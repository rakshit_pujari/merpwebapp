<?php

class Receipt_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }
     /**
     * 
     * @return type
     */
    public function get_all_receipts_for_report(){
        $result = $this->db->query("SELECT 
                                        CONCAT('REC', LPAD(receipt_id, '5', '0')) AS receipt_id,
                                        CONCAT('INV', LPAD(receipt_linked_invoice_id, '5', '0')) AS linked_invoice,
                                        receipt_amount,
                                        receipt_linked_company_display_name as customer_name,
                                        receipt_linked_company_billing_address as customer_billing_address,
                                        receipt_linked_company_billing_state_name as customer_billing_state
                                    FROM
                                        receipt;");
        return $result->result_array();
    }
    

    /**
     * 
     * @param type $invoice_id
     * @return type
     */
    public function get_total_receipt_amount_for_invoice($invoice_id) {
        $result = $this->db->query('Select SUM(receipt_amount) as total_receipt_amount from receipt where receipt_linked_invoice_id = ' . $invoice_id);
        if (isset($result->row_array()['total_receipt_amount'])) {
            return $result->row_array()['total_receipt_amount'];
        }

        return 0;
    }

    /**
     * 
     * @param type $invoice_id
     * @return type
     */
    public function get_receipt_for_invoice($invoice_id) {
        $query = $this->db->get_where('receipt', array('receipt_linked_invoice_id' => $receipt_id));
        return $query->row_array();
    }

    /*
     * Get receipt using receipt_id
     */

    public function get_receipt_with_id($receipt_id) {
        $query = $this->db->get_where('receipt', array('receipt_id' => $receipt_id));
        return $query->row_array();
    }

    /*
     * Retrieve all Credit receipts
     */

    public function get_all_receipts() {
        $result = $this->db->query('SELECT 
                                        *
                                    FROM
                                        receipt
                                    LEFT JOIN
                                        employee ON employee.employee_id = receipt.receipt_record_created_by
                                    ORDER BY
                                        receipt_id DESC;');
        return $result->result_array();
    }

    public function save_receipt($receipt_id, $receipt) {
        try {
            log_message('debug', 'add_receipt. - $receipt = ' . print_r($receipt, 1));
            date_default_timezone_set('Asia/Kolkata');  //PHP server time zone. This won't set mySQl time zone. Needs to be set separately.
            $this->load->model('invoice_model');
            $this->load->model('bank_model');

            //add other data

            if (isset($receipt['receipt_linked_invoice_id'])) {
                $invoice = $this->invoice_model->get_invoice_with_id($receipt['receipt_linked_invoice_id']);

                $receipt['receipt_linked_invoice_due_date'] = $invoice['invoice_due_date'];
                $receipt['receipt_oc_name'] = $invoice['invoice_oc_name'];
                $receipt['receipt_oc_address'] = $invoice['invoice_oc_address'];

                $receipt['receipt_oc_city_name'] = $invoice['invoice_oc_city_name'];
                $receipt['receipt_oc_gstin'] = $invoice['invoice_oc_gstin'];
                $receipt['receipt_oc_pan_number'] = $invoice['invoice_oc_pan_number'];
                $receipt['receipt_oc_logo_path'] = $invoice['invoice_oc_logo_path'];
                $receipt['receipt_oc_district'] = $invoice['invoice_oc_district'];
                $receipt['receipt_oc_state'] = $invoice['invoice_oc_state'];
                $receipt['receipt_oc_contact_number'] = $invoice['invoice_oc_contact_number'];
                $receipt['receipt_oc_gst_supply_state_id'] = $invoice['invoice_oc_gst_supply_state_id'];

                $receipt['receipt_linked_invoice_amount'] = $invoice['invoice_amount'];
                $receipt['receipt_linked_invoice_date'] = $invoice['invoice_date'];

                $receipt['receipt_linked_company_invoice_name'] = $invoice['invoice_linked_company_invoice_name'];
                $receipt['receipt_linked_company_display_name'] = $invoice['invoice_linked_company_display_name'];
                $receipt['receipt_linked_company_billing_address'] = $invoice['invoice_linked_company_billing_address'];
                $receipt['receipt_linked_company_shipping_address'] = $invoice['invoice_linked_company_shipping_address'];
                $receipt['receipt_linked_company_gstin'] = $invoice['invoice_linked_company_gstin'];
                $receipt['receipt_linked_company_contact_number'] = $invoice['invoice_linked_company_contact_number'];

                $receipt_linked_company_gst_supply_state_id = $invoice['invoice_linked_company_gst_supply_state_id'];
                $receipt['receipt_linked_company_gst_supply_state_id'] = $invoice['invoice_linked_company_gst_supply_state_id'];
                $receipt['receipt_place_of_supply'] = $invoice['invoice_place_of_supply'];

                $receipt['receipt_linked_company_billing_state_id'] = $invoice['invoice_linked_company_billing_state_id'];
                $receipt['receipt_linked_company_shipping_state_id'] = $invoice['invoice_linked_company_shipping_state_id'];

                $receipt['receipt_linked_company_billing_state_name'] = $invoice['invoice_linked_company_billing_state_name'];
                $receipt['receipt_linked_company_shipping_state_name'] = $invoice['invoice_linked_company_shipping_state_name'];

                $receipt['receipt_linked_company_payment_term_id'] = $invoice['invoice_linked_company_payment_term_id'];
                $receipt['receipt_linked_company_payment_term_text'] = $invoice['invoice_linked_company_payment_term_name'];
            }
            
            if(!empty($receipt['receipt_debit_coa_id'])){
                $this->load->model('chart_of_accounts_model');
                $receipt['receipt_debit_coa_name'] = $this->chart_of_accounts_model->get_coa_with_id($receipt['receipt_debit_coa_id'])['coa_account_name'];
            }  else {
                $receipt['receipt_debit_coa_id'] = null;
            }

            /* add time component to date if receipt date is today.
             * else
             * add 23:59:59 if the receipt was created on a later date
             */
            if (date('Y-m-d', strtotime($receipt['receipt_date'])) == date("Y-m-d")) {
                $receipt['receipt_date'] = date("Y-m-d H:i:sa");
            } else {
                $receipt['receipt_date'] = date("Y-m-d 23:59:59", strtotime($receipt['receipt_date']));
            }

            if (!empty($receipt['receipt_credit_bank_id'])) {
                $bank = $this->bank_model->get_bank_with_id($receipt['receipt_credit_bank_id']);
                $receipt['receipt_credit_bank_name'] = $bank['bank_name'];
                $receipt['receipt_credit_bank_account_number'] = $bank['bank_account_number'];
            } else {
                $receipt['receipt_credit_bank_id'] = NULL;
                $receipt['receipt_credit_bank_name'] = NULL;
                $receipt['receipt_credit_bank_account_number'] = NULL;
            }
            
            if(empty($receipt['receipt_linked_company_id'])){
                $receipt['receipt_linked_company_id'] = NULL;
            }
            
            $receipt['receipt_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
            
            $this->db->trans_begin();
            $this->db->query('SET time_zone = "+05:30";');
            if ($receipt_id == null) {
                $this->db->insert('receipt', $receipt);
                $receipt_id = $this->db->insert_id();
            } else {
                $this->db->where('receipt_id', $receipt_id);
                $this->db->update('receipt', $receipt);
            }

            if ($receipt_id > 0) {
                $this->db->trans_commit();
            } else {
                log_message('debug', 'ROLLBACK. ' . print_r($this->db->error()));
                $this->db->trans_rollback();
            }

            log_message('debug', 'edit_invoice. - Query = ' . $this->db->last_query());
            return $receipt_id;
        } finally {
            $this->load->model('system_journal_model');
            $this->system_journal_model->calculate_system_journal();
        }
    }

}
