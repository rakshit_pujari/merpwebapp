<?php

class Charge_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    /*
     * Delete Charge
     */

    function delete_charge_by_id($charge_id) {
        log_message('debug', 'delete_charge_by_id. - $id = ' . print_r($charge_id, 1));

        $charge = $this->get_charge_with_id($charge_id);

        $accounting_setting_id = $charge['charge_linked_accounting_setting_id'];
        $this->db->where('charge_id', $charge_id);
        $this->db->delete('charge');
        $this->db->where('as_id', $accounting_setting_id);
        $this->db->delete('accounting_setting');

        log_message('debug', 'delete_charge_by_id. - Query = ' . $this->db->last_query());

        if ($this->db->affected_rows() == '1') {
            log_message('debug', 'delete_charge_by_id. - DELETED ');
            return TRUE;
        } else {
            log_message('debug', 'delete_charge_by_id. - FALIED TO DELETE ');
            return FALSE;
        }
    }

    /*
     * Retrieve all charges
     */

    public function get_all_charges($charge_type = NULL) {
        
        $query = 'SELECT 
                    charge.charge_id,
                    charge.charge_name,
                    charge.charge_type,
                    charge.charge_description,
                    charge.charge_is_default_type,
                    charge.charge_record_creation_time,
                    employee_username
                FROM
                    charge
                LEFT JOIN
                    employee ON employee.employee_id = charge.charge_record_created_by';
        
        if(!empty($charge_type)){
            $query.=' WHERE charge_type = "'.$charge_type.'"';
        }
        
        $result = $this->db->query($query);
        return $result->result_array();
    }
    
    /*
     * Get charge using charge_id
     */

    public function get_charge_with_id($charge_id) {
        $query = $this->db->get_where('charge', array('charge_id' => $charge_id));
        return $query->row_array();
    }


    /**
     * Save a charge
     */
    public function save_charge($data, $charge_id = NULL) {
        log_message('debug', 'save_charge. - $id = ' . print_r($charge_id, 1) . '$data = ' . print_r($data, 1));
        if (isset($data['charge_is_default_type'])) {
            $data['charge_is_default_type'] = 1;
        } else {
            $data['charge_is_default_type'] = 0;
        }
        if($charge_id == NULL){
            $this->db->query('SET time_zone = "+05:30";');
            $accounting_setting = array();            
            if($data['charge_type'] == 'sales'){
                $charge_type = 'invoice';
            } else {
                $charge_type = 'purchase';
            }        
            
            $accounting_setting['as_for'] = $charge_type;
            $accounting_setting['as_field_name'] = strtolower(str_replace(' ', '_', trim($data['charge_name'])));
            $accounting_setting['as_unique_field_name'] = strtolower($charge_type.'_'.str_replace(' ', '_', trim($data['charge_name'])));
            $accounting_setting['as_field_display_name'] = $data['charge_name'];
            $this->db->insert('accounting_setting', $accounting_setting);
            
            $data['charge_linked_accounting_setting_id'] = $this->db->insert_id();
            $charge_id = $this->db->insert('charge', $data);
            
            //INSERT INTO `accounting_setting` (`as_id`,`as_for`,`as_field_name`,`as_unique_field_name`,`as_field_display_name`,`as_debit_account_coa_id`,`as_credit_account_coa_id`) 
                    //VALUES (45,'manufacturing','complete','manufacturing_complete','Completed Order',80,19);
        } else {
            
            $charge = $this->get_charge_with_id($charge_id);

            $accounting_setting_id = $charge['charge_linked_accounting_setting_id'];
            
            $this->db->where('charge_id', $charge_id);
            $this->db->update('charge', $data);
            
            if(!empty($accounting_setting_id)){
                $updated_accounting_setting = array();
                
                if($data['charge_type'] == 'sales'){
                    $charge_type = 'invoice';
                } else {
                    $charge_type = 'purchase';
                }

                $updated_accounting_setting['as_for'] = $charge_type;
                $updated_accounting_setting['as_field_name'] = strtolower(str_replace(' ', '_', trim($data['charge_name'])));
                $updated_accounting_setting['as_unique_field_name'] = strtolower($charge_type.'_'.str_replace(' ', '_', trim($data['charge_name'])));
                $updated_accounting_setting['as_field_display_name'] = $data['charge_name'];

                $this->db->where('as_id', $accounting_setting_id);
                $this->db->update('accounting_setting', $updated_accounting_setting);
            } else {
                throw new Exception('Settings not found for '.$as_unique_field_name.' Query - '.$this->db->last_query());
            }
        }
        
        $response['query'] = $this->db->last_query();
        log_message('debug', 'save_charge. - response = ' . print_r($response, 1));
        return $response;
    } 
}
