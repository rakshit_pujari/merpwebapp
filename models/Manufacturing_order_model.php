<?php

class Manufacturing_order_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_all_manufacturing_orders_for_report(){
        $result = $this->db->query("SELECT 
                                    CONCAT('MFG',
                                    LPAD(manufacturing_order_id, '5', '0')) AS manufacturing_order_id,
                                    manufacturing_order_linked_product_name as product,
                                    manufacturing_order_product_quantity,
                                    manufacturing_order_date,
                                    manufacturing_order_status
                                FROM
                                    manufacturing_order;");
        return $result->result_array();
    }
    
    
    /*
     * Retrieve all draft manufacturing_orders
     */

    public function get_all_draft_manufacturing_orders() {
        $result = $this->db->query('SELECT 
                                        *,
                                        (Select uqc_text from unique_quantity_code where unique_quantity_code.uqc_id = product.product_uqc_id) as uqc_text
                                    FROM
                                        draft_manufacturing_order
                                    LEFT JOIN
                                        product
                                    ON
                                        draft_manufacturing_order.manufacturing_order_linked_product_id = product.product_id
                                    LEFT JOIN
                                        employee ON employee.employee_id = draft_manufacturing_order.manufacturing_order_record_created_by;');
        return $result->result_array();
    }

        /**
     * Get entries for an manufacturing_order
     */
    public function get_draft_manufacturing_order_entries($manufacturing_order_id) {

        $query = 'SELECT 
                    *,
                    IFNULL(SUM(ie_it_product_quantity), 0) AS inventory_quantity,
                    (Select ie_it_product_rate from inventory_entry as c WHERE inventory_entry.ie_it_product_id = c.ie_it_product_id ORDER BY c.ie_id DESC LIMIT 1) as rate_per_unit,
                    (Select uqc_text from unique_quantity_code where unique_quantity_code.uqc_id = product.product_uqc_id) as uqc_text
                    FROM draft_manufacturing_order_entries 
                    LEFT JOIN
                        product
                    ON
                        draft_manufacturing_order_entries.moe_linked_product_id = product.product_id
                    LEFT JOIN
                        inventory_entry ON inventory_entry.ie_it_product_id = product.product_id
                    WHERE
                        moe_linked_manufacturing_order_id = ' . $manufacturing_order_id . ' group by moe_id;';

        $result = $this->db->query($query);
        return $result->result_array();
    }
    
    
    /*
     * Delete ManufacturingOrder
     */
    function delete_draft_manufacturing_order_by_id($manufacturing_order_id) {
        log_message('debug', 'delete_manufacturing_order_by_id. - $id = ' . print_r($manufacturing_order_id, 1));

        $this->db->where('moe_linked_manufacturing_order_id', $manufacturing_order_id);
        $this->db->delete('draft_manufacturing_order_entries');
            
        $this->db->where('manufacturing_order_id', $manufacturing_order_id);
        $this->db->delete('draft_manufacturing_order');

        log_message('debug', 'delete_manufacturing_order_by_id. - Query = ' . $this->db->last_query());

        if ($this->db->affected_rows() == '1') {
            log_message('debug', 'delete_manufacturing_order_by_id. - DELETED ');
            return TRUE;
        } else {
            log_message('debug', 'delete_manufacturing_order_by_id. - FALIED TO DELETE ');
            return FALSE;
        }
    }

    /*
     * Retrieve all manufacturing_orders
     */

    public function get_all_manufacturing_orders() {
        $result = $this->db->query('SELECT 
                                    *,
                                    (Select uqc_text from unique_quantity_code where unique_quantity_code.uqc_id = product.product_uqc_id) as uqc_text
                                FROM
                                    manufacturing_order
                                LEFT JOIN
                                    product
                                ON
                                    manufacturing_order.manufacturing_order_linked_product_id = product.product_id
                                LEFT JOIN
                                    employee ON employee.employee_id = manufacturing_order.manufacturing_order_record_created_by
                                ORDER BY 
                                    manufacturing_order_id DESC;');
        return $result->result_array();
    }

    /**
     * Get entries for an manufacturing_order
     */
    public function get_manufacturing_order_entries($manufacturing_order_id) {

        $query = 'SELECT 
                    *,
                    IFNULL(SUM(ie_it_product_quantity), 0) AS inventory_quantity,
                    (Select ie_it_product_rate from inventory_entry as c WHERE inventory_entry.ie_it_product_id = c.ie_it_product_id ORDER BY c.ie_id DESC LIMIT 1) as rate_per_unit,
                    (Select uqc_text from unique_quantity_code where unique_quantity_code.uqc_id = product.product_uqc_id) as uqc_text
                    FROM manufacturing_order_entries 
                    LEFT JOIN
                        product
                    ON
                        manufacturing_order_entries.moe_linked_product_id = product.product_id
                        LEFT JOIN
                        inventory_entry ON inventory_entry.ie_it_product_id = product.product_id
                    WHERE
                        moe_linked_manufacturing_order_id = ' . $manufacturing_order_id . ' group by moe_id;';

        $result = $this->db->query($query);
        return $result->result_array();
    }

    /*
     * Get manufacturing_order using manufacturing_order_id
     */
    public function get_manufacturing_order_with_id($manufacturing_order_id) {
        $query = $this->db->get_where('manufacturing_order', array('manufacturing_order_id' => $manufacturing_order_id));
        return $query->row_array();
    }
    
    
    /*
     * Get manufacturing_order using manufacturing_order_id
     */

    public function get_draft_manufacturing_order_with_id($manufacturing_order_id) {
        $query = $this->db->get_where('draft_manufacturing_order', array('manufacturing_order_id' => $manufacturing_order_id));
        return $query->row_array();
    }

    /**
     * 
     * @param type $manufacturing_id
     */
    public function mark_manufacturing_order_as_complete($manufacturing_id){
        $this->db->query('SET time_zone = "+05:30";');
        $query = 'Update manufacturing_order SET manufacturing_order_status = "complete", manufacturing_order_completion_date = NOW() where manufacturing_order_id = '.$manufacturing_id;
        $this->db->query($query);
    }
    
    /*
     * Add new manufacturing_order

      0 - Tax
      1 - HSN
      2 - rate
      3 - quantity
      4 - discount

     * IMPORTANT - while editing manufacturing_order entries, always delete old entries and insert edited ones so that new cne_id are assgined. Failure to
     * do so may lead to error while calculating inventory
     */

    
    public function save_manufacturing_order($manufacturing_order_id, $manufacturing_order, $manufacturing_order_entries, $manufacturing_order_action) {

        try {
        if ($manufacturing_order_action == 'previewDraft') {
            $manufacturing_order_action = 'draft';
        }

        log_message('debug', 'add_manufacturing_order. - $manufacturing_order = ' . print_r($manufacturing_order, 1));
        date_default_timezone_set('Asia/Kolkata');  //PHP server time zone. This won't set mySQl time zone. Needs to be set separately.

        $this->db->query('SET time_zone = "+05:30";');

        $this->db->trans_begin();
        $count = 0;
        
        $this->load->model('owner_company_model');
        $this->load->model('product_model');
        $this->load->model('unique_quantity_code_model');
        
        //add other data
        $owner_company = $this->owner_company_model->get_owner_company();

        $manufacturing_order['manufacturing_order_oc_name'] = $owner_company['oc_billing_name'];
        $manufacturing_order['manufacturing_order_oc_address'] = $owner_company['oc_billing_address'];
        $manufacturing_order['manufacturing_order_oc_city_name'] = $owner_company['city_name'];
        if(!empty($owner_company['oc_pincode'])){
            $manufacturing_order['manufacturing_order_oc_city_name'].= ' - '.$owner_company['oc_pincode'];
        }
        $manufacturing_order['manufacturing_order_oc_gstin'] = $owner_company['oc_gst_number'];
        $manufacturing_order['manufacturing_order_oc_pan_number'] = $owner_company['oc_pan_number'];
        $manufacturing_order['manufacturing_order_oc_logo_path'] = $owner_company['oc_logo_path'];
        $manufacturing_order['manufacturing_order_oc_district'] = $owner_company['district'];
        $manufacturing_order['manufacturing_order_oc_state'] = $owner_company['billing_state_name'];
        $manufacturing_order['manufacturing_order_oc_contact_number'] = $owner_company['oc_contact_number'];

        /* add time component to date if manufacturing_order date is today.
         * else
         * add 23:59:59 if the manufacturing_order was created on a later date
         * This timestamp is to determine entry sequence for FIFO inventory
         * To be done for Invoice, manufacturing_order, credit note, debit note, inventory adjustment
         */
        $this->db->query('SET time_zone = "+05:30";');
        $manufacturing_order_date = $manufacturing_order['manufacturing_order_date'];
        if (date('Y-m-d', strtotime($manufacturing_order['manufacturing_order_date'])) == date("Y-m-d")) {
            $manufacturing_order['manufacturing_order_date'] = date("Y-m-d H:i:sa");
        } else {
            $manufacturing_order['manufacturing_order_date'] = date("Y-m-d 23:59:59", strtotime($manufacturing_order_date));
        }
        
        $product = $this->product_model->get_product_with_id($manufacturing_order['manufacturing_order_linked_product_id']);
        $manufacturing_order['manufacturing_order_linked_product_name'] = $product['product_name'];
        
        $manufacturing_order['manufacturing_order_uqc_id'] = $this->product_model->get_product_with_id($manufacturing_order['manufacturing_order_linked_product_id'])['product_uqc_id'];
        $manufacturing_order['manufacturing_order_uqc_text'] = $this->unique_quantity_code_model->get_uqc_with_id($manufacturing_order['manufacturing_order_uqc_id'])['uqc_text'];
                
        if ($manufacturing_order['manufacturing_order_status'] == 'new') {
            $manufacturing_order['manufacturing_order_status'] = $manufacturing_order_action;
            if ($manufacturing_order_action == 'confirm') {
                $this->db->insert('manufacturing_order', $manufacturing_order);
            } else if ($manufacturing_order_action == 'draft') {
                $this->db->insert('draft_manufacturing_order', $manufacturing_order);
            } else {
                throw new Exception('Invalid manufacturing_order action for insert');
            }
            $manufacturing_order_id = $this->db->insert_id();
        } else {
            // user is editing an existing manufacturing_order. 
            if ($manufacturing_order_action == $manufacturing_order['manufacturing_order_status']) {

                if ($manufacturing_order_action == 'draft') {

                    $this->db->where('moe_linked_manufacturing_order_id', $manufacturing_order_id);
                    $this->db->delete('draft_manufacturing_order_entries');

                    $this->db->where('manufacturing_order_id', $manufacturing_order_id);
                    $this->db->update('draft_manufacturing_order', $manufacturing_order);
                } else if ($manufacturing_order_action == 'confirm') {

                    //retain time component if date not edited by user
                    $manufacturing_order_last_copy = $this->get_manufacturing_order_with_id($manufacturing_order_id);
                    if (date('Y-m-d', strtotime($manufacturing_order['manufacturing_order_date'])) == strtotime($manufacturing_order_last_copy['manufacturing_order_date'])) {
                        $manufacturing_order['manufacturing_order_date'] = $manufacturing_order_last_copy['manufacturing_order_date'];
                    }

                    $this->db->where('moe_linked_manufacturing_order_id', $manufacturing_order_id);
                    $this->db->delete('manufacturing_order_entries');

                    $this->db->where('manufacturing_order_id', $manufacturing_order_id);
                    $this->db->update('manufacturing_order', $manufacturing_order);
                } else {
                    throw new Exception('Invalid manufacturing_order action for update');
                }
            } else if ($manufacturing_order['manufacturing_order_status'] == 'draft' && $manufacturing_order_action == 'confirm') {   //confirming a draft manufacturing_order
                //delete draft manufacturing_order
                $this->db->where('moe_linked_manufacturing_order_id', $manufacturing_order_id);
                $this->db->delete('draft_manufacturing_order_entries');
                $this->db->where('manufacturing_order_id', $manufacturing_order_id);
                $this->db->delete('draft_manufacturing_order');
                
                $manufacturing_order['manufacturing_order_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
                $manufacturing_order['manufacturing_order_status'] = $manufacturing_order_action;
                $this->db->insert('manufacturing_order', $manufacturing_order);
                $manufacturing_order_id = $this->db->insert_id();
            } else {
                throw new Exception('Invalid manufacturing_order status and required action');
            }
        }

        if ($manufacturing_order_id > 0) {
            
            $moe['moe_linked_manufacturing_order_id'] = $manufacturing_order_id;

            foreach ($manufacturing_order_entries as $entry_id => $product_info) {
                foreach ($product_info as $product_id => $quantity) {
                    $moe['moe_linked_product_id'] = $product_id;
                    $moe['moe_linked_product_quantity'] = $quantity;
                    
                    $moe['moe_uqc_id'] = $this->product_model->get_product_with_id($moe['moe_linked_product_id'])['product_uqc_id'];
                    $moe['moe_uqc_text'] = $this->unique_quantity_code_model->get_uqc_with_id($moe['moe_uqc_id'])['uqc_text'];
                    
                    $raw_material = $this->product_model->get_product_with_id($moe['moe_linked_product_id']);
                    $moe['moe_linked_product_name'] = $raw_material['product_name'];
        
                    if ($manufacturing_order_action == 'confirm') {
                        $this->db->insert('manufacturing_order_entries', $moe);
                    } else if ($manufacturing_order_action == 'draft') {
                        $this->db->insert('draft_manufacturing_order_entries', $moe);
                    } else {
                        throw new Exception('Invalid manufacturing_order action while inserting manufacturing_order entries');
                    }
                    if ($this->db->insert_id() > 0) {
                        $count++;
                    }
                }
            }
        } else {
            $response['result'] = $this->db->error();
            $response['query'] = $this->db->last_query();
        }

        if ($count > 0) {
            $this->db->trans_commit();
            $response['result'] = "success";
        } else {
            log_message('debug', 'rollback. ' . print_r($response));
            $this->db->trans_rollback();
        }

        log_message('debug', 'save_manufacturing_order. - Query = ' . $this->db->last_query());
        return $manufacturing_order_id;
    } finally {
        $this->load->model('inventory_model');
        $this->inventory_model->evaluate_inventory_figures();
    }
    }

}
