<?php

class State_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    /*
     * Retrieve all states
     */

    public function get_all_states() {
        $result = $this->db->query('SELECT 
										*
										FROM
										state
										WHERE
										state_type = "state"');
        return $result->result_array();
    }

    /*
     * Retrieve all union territories
     */

    public function get_all_union_territories() {
        $result = $this->db->query('SELECT 
										*
										FROM
										state
										WHERE
										state_type = "union_territory"');
        return $result->result_array();
    }

    public function get_state_with_id($id) {
        $query = $this->db->get_where('state', array('state_tin_number' => $id));
        return $query->row_array();
    }

}
