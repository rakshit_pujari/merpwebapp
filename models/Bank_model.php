<?php

class Bank_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    /**
     * Delete bank using its ID
     */
    function deleteBankById($id) {
        log_message('debug', 'deleteBankBy. - $id = ' . print_r($id, 1));

        $this->db->where('bank_id', $id);
        $this->db->delete('bank');

        log_message('debug', 'deleteBankBy. - Query = ' . $this->db->last_query());

        if ($this->db->affected_rows() == '1') {
            log_message('debug', 'deleteBankBy. - BANK DELETED ');
            return TRUE;
        } else {
            log_message('debug', 'deleteBankBy. - FALIED TO DELETE BANK ');
            return FALSE;
        }
    }

    /**
     * Get number of banks in table
     * */
    public function get_bank_count() {
        $query_string = 'SELECT COUNT(bank_id) AS count from bank';
        $result = $this->db->query($query_string);
        return $result->row_array();
    }

    /**
     * Get all banks along with location
     * */
    public function get_all_banks() {
        $result = $this->db->query('SELECT 
                                        bank_id,
                                        bank_name,
                                        bank_account_name,
                                        bank_account_number,
                                        bank_address,
                                        bank_area,
                                        city_name,
                                        employee_name,
                                        bank_record_creation_time,
                                        bank_relationship_manager_name,
                                        bank_contact_number
                                    FROM
                                        bank
                                    LEFT JOIN
                                        employee ON employee.employee_id = bank.bank_record_created_by
                                    LEFT JOIN
                                        location
                                    ON
                                        bank.bank_location_id = location.location_id;');
        return $result->result_array();
    }

    /**
     * Get a bank using ID
     */
    public function get_bank_with_id($id) {
        $query = $this->db->get_where('bank', array('bank_id' => $id));
        return $query->row_array();
    }

    /**
     * Save a bank
     */
    public function save_bank($data, $bank_id = NULL) {
        log_message('debug', 'save_bank. - $id = ' . print_r($bank_id, 1) . '$data = ' . print_r($data, 1));
        if($bank_id == NULL){
            $this->db->query('SET time_zone = "+05:30";');
            $bank_id = $this->db->insert('bank', $data);
        } else {
            $this->db->where('bank_id', $bank_id);
            $this->db->update('bank', $data);
        }
        
        $response['query'] = $this->db->last_query();
        log_message('debug', 'save_bank. - response = ' . print_r($response, 1));
        return $response;
    } 
}
