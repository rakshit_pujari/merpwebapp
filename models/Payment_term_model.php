<?php

class Payment_term_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    
        /*
     * Retrieve all locations
     */

    public function get_non_zero_days_payment_terms() {
        $query = $this->db->query('Select * from payment_term where payment_term_days > 0');
        return $query->result_array();
    }

    
    /*
     * Retrieve all locations
     */

    public function get_all_payment_terms() {
        $query = $this->db->get('payment_term');
        return $query->result_array();
    }

    /*
     * Retrieve payment term using ID
     */

    public function get_payment_term_using_id($payment_term_id) {
        $query = $this->db->get_where('payment_term', array('payment_term_id' => $payment_term_id));
        return $query->row_array();
    }

}
