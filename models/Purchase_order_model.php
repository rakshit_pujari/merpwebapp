<?php

class Purchase_order_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    
    public function get_all_purchase_orders_for_report(){
        $result = $this->db->query("SELECT 
                                    CONCAT('PUR', LPAD(purchase_order_id, '5', '0')) AS purchase_order_id,
                                    purchase_order_linked_company_display_name as vendor_name,
                                    purchase_order_date,
                                    purchase_order_linked_company_billing_address as vendor_billing_address,
                                    purchase_order_linked_company_billing_state_name as vendor_state
                                FROM
                                    purchase_order
                                LEFT JOIN
                                    employee ON employee.employee_id = purchase_order.purchase_order_record_created_by;");
        return $result->result_array();
    }
    
    /*
     * Delete Purchase Order
     */
    function delete_draft_purchase_order_by_id($purchase_order_id) {
        log_message('debug', 'delete_purchase_order_by_id. - $id = ' . print_r($purchase_order_id, 1));

        $this->db->where('po_linked_purchase_order_id', $purchase_order_id);
        $this->db->delete('draft_purchase_order_entries');

        $this->db->where('purchase_order_id', $purchase_order_id);
        $this->db->delete('draft_purchase_order');

        log_message('debug', 'delete_purchase_order_by_id. - Query = ' . $this->db->last_query());

        if ($this->db->affected_rows() == '1') {
            log_message('debug', 'delete_purchase_order_by_id. - DELETED ');
            return TRUE;
        } else {
            log_message('debug', 'delete_purchase_order_by_id. - FALIED TO DELETE ');
            return FALSE;
        }
    }

    /*
     * Retrieve all purchase orders
     */

    public function get_all_purchase_orders() {
        $result = $this->db->query('SELECT 
                                    *
                                FROM
                                        purchase_order
                                LEFT JOIN
                                        employee ON employee.employee_id = purchase_order.purchase_order_record_created_by
                                ORDER BY 
                                purchase_order_id DESC;');
        return $result->result_array();
    }

    /*
     * Retrieve all draft purchase orders
     */

    public function get_all_draft_purchase_orders() {
        $result = $this->db->query('SELECT 
                                        *
                                    FROM
                                        draft_purchase_order
                                    LEFT JOIN
                                        employee ON employee.employee_id = draft_purchase_order.purchase_order_record_created_by;');
        return $result->result_array();
    }

    /**
     * Get entries for an purchase order
     */
    public function get_purchase_order_entries($purchase_order_id) {

        $query = 'SELECT 
                    *
                    FROM purchase_order_entries 
                    WHERE
                    po_linked_purchase_order_id = ' . $purchase_order_id . ' group by po_id;';

        $result = $this->db->query($query);
        return $result->result_array();
    }

    /*
     * Get purchase order using purchase_order_id
     */

    public function get_purchase_order_with_id($purchase_order_id) {
        $query = $this->db->get_where('purchase_order', array('purchase_order_id' => $purchase_order_id));
        return $query->row_array();
    }

    /**
     * Get entries for an purchase order
     */
    public function get_draft_purchase_order_entries($purchase_order_id) {
        $query = 'SELECT 
                    *
                    FROM draft_purchase_order_entries 
                    WHERE
                    po_linked_purchase_order_id = ' . $purchase_order_id . ' group by po_id;';

        $result = $this->db->query($query);
        return $result->result_array();
    }

    /*
     * Get purchase order using purchase_order_id
     */

    public function get_draft_purchase_order_with_id($purchase_order_id) {
        $query = $this->db->get_where('draft_purchase_order', array('purchase_order_id' => $purchase_order_id));
        return $query->row_array();
    }

    /*
     * Add new purchase order

      0 - Tax
      1 - HSN
      2 - rate
      3 - quantity
      4 - discount

     * IMPORTANT - while editing purchase order entries, always delete old entries and insert edited ones so that new cne_id are assgined. Failure to
     * do so may lead to error while calculating inventory
     */

    public function save_purchase_order($purchase_order_id, $purchase_order, $purchase_order_entries, $purchase_order_action) {

        if ($purchase_order_action == 'previewDraft') {
            $purchase_order_action = 'draft';
        }

        log_message('debug', 'add_purchase_order. - $purchase_order = ' . print_r($purchase_order, 1));
        date_default_timezone_set('Asia/Kolkata');  //PHP server time zone. This won't set mySQl time zone. Needs to be set separately.
        $this->load->model('company_model');
        $this->load->model('owner_company_model');
        $this->load->model('payment_term_model');
        $this->load->model('state_model');
        $this->load->model('transporter_model');
        $this->load->model('product_model');
        $this->load->model('unique_quantity_code_model');

        //add other data
        $owner_company = $this->owner_company_model->get_owner_company();

        $purchase_order['purchase_order_oc_name'] = $owner_company['oc_billing_name'];
        $purchase_order['purchase_order_oc_address'] = $owner_company['oc_billing_address'];
        $purchase_order['purchase_order_oc_city_name'] = $owner_company['city_name'];
        if(!empty($owner_company['oc_pincode'])){
            $purchase_order['purchase_order_oc_city_name'].= ' - '.$owner_company['oc_pincode'];
        }
        $purchase_order['purchase_order_oc_gstin'] = $owner_company['oc_gst_number'];
        $purchase_order['purchase_order_oc_pan_number'] = $owner_company['oc_pan_number'];
        $purchase_order['purchase_order_oc_logo_path'] = $owner_company['oc_logo_path'];
        $purchase_order['purchase_order_oc_district'] = $owner_company['district'];
        $purchase_order['purchase_order_oc_state'] = $owner_company['billing_state_name'];
        $purchase_order['purchase_order_oc_contact_number'] = $owner_company['oc_contact_number'];
        //$purchase_order['purchase_order_terms'] = $owner_company['oc_invoice_terms'];
        //$purchase_order['purchase_order_additional_terms'] = $owner_company['oc_additional_terms'];

        $this->db->query('SET time_zone = "+05:30";');

        $purchase_order_date = $purchase_order['purchase_order_date'];

        $payment_term = $this->payment_term_model->get_payment_term_using_id($purchase_order['purchase_order_linked_company_payment_term_id']);
        $purchase_order['purchase_order_linked_company_payment_term_name'] = $payment_term['payment_term_display_text'];

        /* add time component to date if purchase order date is today.
         * else
         * add 23:59:59 if the purchase order was created on a later date
         * This timestamp is to determine entry sequence for FIFO inventory
         * To be done for Invoice, purchase order, credit note, debit note, inventory adjustment
         */
        if (date('Y-m-d', strtotime($purchase_order['purchase_order_date'])) == date("Y-m-d")) {
            $purchase_order['purchase_order_date'] = date("Y-m-d H:i:sa");
        } else {
            $purchase_order['purchase_order_date'] = date("Y-m-d 23:59:59", strtotime($purchase_order_date));
        }

        if (!empty($purchase_order['purchase_order_linked_company_id'])) {
            $linked_company_id = $purchase_order['purchase_order_linked_company_id'];
            $linked_company = $this->company_model->get_company_with_id($linked_company_id);
            $purchase_order['purchase_order_linked_company_contact_number'] = $linked_company['company_contact_number'];
        } else {
            $purchase_order['purchase_order_linked_company_id'] = NULL;
            $purchase_order['purchase_order_linked_company_contact_number'] = NULL;
        }

        if(empty($purchase_order['purchase_order_revision_number'])){
            $purchase_order['purchase_order_revision_number'] = NULL;
        }
        
        if(empty($purchase_order['purchase_order_revision_date'])){
            $purchase_order['purchase_order_revision_date'] = NULL;
        }
        
        $purchase_order['purchase_order_linked_company_billing_state_name'] = $this->state_model->get_state_with_id($purchase_order['purchase_order_linked_company_billing_state_id'])['state_name'];

        $this->db->trans_begin();
        $count = 0;
        $this->db->query('SET time_zone = "+05:30";');
        if ($purchase_order['purchase_order_status'] == 'new') {
            $purchase_order['purchase_order_status'] = $purchase_order_action;
            if ($purchase_order_action == 'confirm') {
                $this->db->insert('purchase_order', $purchase_order);
            } else if ($purchase_order_action == 'draft') {
                $this->db->insert('draft_purchase_order', $purchase_order);
            } else {
                throw new Exception('Invalid purchase order action for insert');
            }
            $purchase_order_id = $this->db->insert_id();
        } else {
            // user is editing an existing purchase order. 
            if ($purchase_order_action == $purchase_order['purchase_order_status']) {

                if ($purchase_order_action == 'draft') {

                    $this->db->where('po_linked_purchase_order_id', $purchase_order_id);
                    $this->db->delete('draft_purchase_order_entries');

                    $this->db->where('purchase_order_id', $purchase_order_id);
                    $this->db->update('draft_purchase_order', $purchase_order);
                } else if ($purchase_order_action == 'confirm') {

                    //retain time component if date not edited by user
                    $purchase_order_last_copy = $this->get_purchase_order_with_id($purchase_order_id);
                    if (date('Y-m-d', strtotime($purchase_order['purchase_order_date'])) == strtotime($purchase_order_last_copy['purchase_order_date'])) {
                        $purchase_order['purchase_order_date'] = $purchase_order_last_copy['purchase_order_date'];
                    }

                    $this->db->where('po_linked_purchase_order_id', $purchase_order_id);
                    $this->db->delete('purchase_order_entries');

                    $this->db->where('purchase_order_id', $purchase_order_id);
                    $this->db->update('purchase_order', $purchase_order);
                } else {
                    throw new Exception('Invalid purchase order action for update');
                }
            } else if ($purchase_order['purchase_order_status'] == 'draft' && $purchase_order_action == 'confirm') {   //confirming a draft purchase order
                //delete draft purchase order
                $this->db->where('po_linked_purchase_order_id', $purchase_order_id);
                $this->db->delete('draft_purchase_order_entries');
                $this->db->where('purchase_order_id', $purchase_order_id);
                $this->db->delete('draft_purchase_order');

                $purchase_order['purchase_order_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
                
                $purchase_order['purchase_order_status'] = $purchase_order_action;
                $this->db->insert('purchase_order', $purchase_order);
                $purchase_order_id = $this->db->insert_id();
            } else {
                throw new Exception('Invalid purchase order status and required action');
            }
        }

        if ($purchase_order_id > 0) {
            $po['po_linked_purchase_order_id'] = $purchase_order_id;

            foreach ($purchase_order_entries as $entry_id => $purchase_order_entry) {
                $po['po_entry_id'] = $entry_id;
                foreach ($purchase_order_entry as $product_id => $product_info) {
                    $po['po_linked_product_id'] = $product_id;

                    $po['po_product_name'] = $product_info[0];
                    $po['po_product_rate'] = $product_info[1];
                    $po['po_product_quantity'] = $product_info[2];
                    //$po['po_product_uqc_id'] = $product_info[3];
                    //$po['po_product_uqc_text'] = $product_info[4];
                    $po['po_product_uqc_id'] = $this->product_model->get_product_with_id($product_id)['product_uqc_id'];
                    $po['po_product_uqc_text'] = $this->unique_quantity_code_model->get_uqc_with_id($po['po_product_uqc_id'])['uqc_text'];

                    if ($purchase_order_action == 'confirm') {
                        $this->db->insert('purchase_order_entries', $po);
                    } else if ($purchase_order_action == 'draft') {
                        $this->db->insert('draft_purchase_order_entries', $po);
                    } else {
                        throw new Exception('Invalid purchase order action while inserting purchase order entries');
                    }
                    if ($this->db->insert_id() > 0) {
                        $count++;
                    }
                }
            }
        } else {
            $response['result'] = $this->db->error();
            $response['query'] = $this->db->last_query();
        }


        if ($count > 0) {
            $this->db->trans_commit();
            $response['Result'] = "Success";
        } else {
            log_message('debug', 'confirm_dispatch. ROLLBACK. ' . print_r($response));
            $this->db->trans_rollback();
        }

        log_message('debug', 'edit_purchase_order. - Query = ' . $this->db->last_query());
        return $purchase_order_id;
    }

}
