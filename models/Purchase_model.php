<?php

class Purchase_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }
    
    /**
     * 
     * @return type
     */
    public function get_all_purchases_for_report(){
        $result = $this->db->query("SELECT 
                                    CONCAT('PUR', LPAD(purchase_id, '5', '0')) AS purchase_id,
                                    purchase_date,
                                    purchase_bill_date,
                                    purchase_linked_company_display_name as vendor_name,
                                    purchase_linked_company_billing_address as vendor_address,
                                    purchase_bill_reference,
                                    purchase_amount,
                                    (purchase_amount
                                    - (SELECT IFNULL(SUM(pap_allocated_amount), 0) FROM purchase_advance_payment WHERE pap_purchase_id = purchase_id)
                                    + (SELECT IFNULL(SUM(IFNULL(cn_amount, 0)), 0) FROM credit_note WHERE cn_linked_purchase_id = purchase_id)
                                    - (SELECT IFNULL(SUM(IFNULL(dn_amount, 0)), 0) FROM debit_note WHERE dn_linked_purchase_id = purchase_id)
                                    - (SELECT IFNULL(SUM(IFNULL(payment_amount, 0)), 0) FROM payment WHERE payment_linked_purchase_id = purchase_id)                                
                                    ) AS payment_balance
                                FROM
                                    purchase
                                LEFT JOIN
                                    employee ON employee.employee_id = purchase.purchase_record_created_by;");
        return $result->result_array();
    }

    /**
     * 
     * @return type
     */
    public function get_all_vendors() {
        /*$result = $this->db->query("SELECT 
                                        IFNULL(purchase_linked_company_id, 0) AS purchase_linked_company_id,
                                        IF(purchase_linked_company_id IS NULL,
                                            'Unregistered',
                                            GROUP_CONCAT(DISTINCT (purchase_linked_company_invoice_name)
                                                SEPARATOR ', ')) AS purchase_linked_company_invoice_name,
                                        IF(purchase_linked_company_id IS NULL,
                                            'Unregistered',
                                            GROUP_CONCAT(DISTINCT (purchase_linked_company_display_name)
                                                SEPARATOR ', ')) AS purchase_linked_company_display_name
                                    FROM
                                        purchase
                                    GROUP BY purchase_linked_company_id;");*/
        $result = $this->db->query('Select purchase_linked_company_display_name, purchase_linked_company_invoice_name, purchase_linked_company_id from purchase GROUP BY purchase_linked_company_display_name;');
        return $result->result_array();
    }

    /**
     * Get all linked documents
     * @param type $purchase_id
     * @return type
     */
    public function get_linked_documents($purchase_id) {
        $result = $this->db->query('select advance_payment_id as document_id, "advance_payment" as document_type from purchase_advance_payment'
                                    . ' LEFT JOIN advance_payment ON advance_payment.advance_payment_id = purchase_advance_payment.pap_advance_payment_id where pap_purchase_id = ' . $purchase_id
                                    . ' UNION '
                                    . 'select cn_id as document_id, "credit_note" as document_type from credit_note where cn_linked_purchase_id = ' . $purchase_id
                                    . ' UNION
                                    select dn_id as document_id, "debit_note" as document_type from debit_note where dn_linked_purchase_id = ' . $purchase_id
                                    . ' UNION
                                    select payment_id as document_id, "payment" as document_type from payment where payment_linked_purchase_id = ' . $purchase_id);
        return $result->result_array();
    }

    /* public function get_purchase_vendor_map(){
      $result = $this->db->query('Select purchase_id, purchase_linked_company_id, purchase_linked_company_invoice_name from purchase where purchase_linked_company_invoice_name!="null";');
      return $result->result_array();
      } */

    public function get_payment_vendor_map($purchase_id = NULL) {

        $query = 'SELECT purchase_id, purchase_amount, purchase_linked_company_id, purchase_linked_company_invoice_name, purchase_linked_company_display_name,
                                (SELECT IFNULL(SUM(IFNULL(cn_amount, 0)), 0) FROM credit_note WHERE cn_linked_purchase_id = purchase_id) AS cn_amount,
                                (SELECT IFNULL(SUM(IFNULL(dn_amount, 0)), 0) FROM debit_note WHERE dn_linked_purchase_id = purchase_id) AS dn_amount,
                                (SELECT IFNULL(SUM(pap_allocated_amount), 0) FROM purchase_advance_payment WHERE pap_purchase_id = purchase_id) as advance_payment_amount,
                                (SELECT IFNULL(SUM(IFNULL(payment_amount, 0)), 0) FROM payment WHERE payment_linked_purchase_id = purchase_id) AS payment_amount,
                                (purchase_amount 
                                - (SELECT IFNULL(SUM(pap_allocated_amount), 0) FROM purchase_advance_payment WHERE pap_purchase_id = purchase_id)
                                + (SELECT IFNULL(SUM(IFNULL(cn_amount, 0)), 0) FROM credit_note WHERE cn_linked_purchase_id = purchase_id)
                                - (SELECT IFNULL(SUM(IFNULL(dn_amount, 0)), 0) FROM debit_note WHERE dn_linked_purchase_id = purchase_id)
                                - (SELECT IFNULL(SUM(IFNULL(payment_amount, 0)), 0) FROM payment WHERE payment_linked_purchase_id = purchase_id)                                
                                ) AS payment_balance
                                FROM purchase';
        if ($purchase_id !== NULL) {
            $query .= ' WHERE purchase_id = ' . $purchase_id;
        }

        $query .= ';';
        $result = $this->db->query($query);
        return $result->result_array();
    }

    /*
     * Delete Purchase
     */

    function delete_draft_purchase_by_id($purchase_id) {
        log_message('debug', 'delete_draft_purchase_by_id. - $id = ' . print_r($purchase_id, 1));

        $this->db->where('pe_linked_purchase_id', $purchase_id);
        $this->db->delete('draft_purchase_entries');
        
        $this->db->where('pce_linked_purchase_id', $purchase_id);
        $this->db->delete('draft_purchase_charge_entries');
        
        $this->db->where('purchase_id', $purchase_id);
        $this->db->delete('draft_purchase');

        log_message('debug', 'delete_draft_purchase_by_id. - Query = ' . $this->db->last_query());

        if ($this->db->affected_rows() == '1') {
            log_message('debug', 'delete_draft_purchase_by_id. - DELETED ');
            return TRUE;
        } else {
            log_message('debug', 'delete_draft_purchase_by_id. - FALIED TO DELETE ');
            return FALSE;
        }
    }

    /*
     * Retrieve all purchases
     */

    public function get_all_purchases() {
        $result = $this->db->query('SELECT 
                                    *,
                                    (purchase_amount
                                - (SELECT IFNULL(SUM(pap_allocated_amount), 0) FROM purchase_advance_payment WHERE pap_purchase_id = purchase_id)
                                + (SELECT IFNULL(SUM(IFNULL(cn_amount, 0)), 0) FROM credit_note WHERE cn_linked_purchase_id = purchase_id)
                                - (SELECT IFNULL(SUM(IFNULL(dn_amount, 0)), 0) FROM debit_note WHERE dn_linked_purchase_id = purchase_id)
                                - (SELECT IFNULL(SUM(IFNULL(payment_amount, 0)), 0) FROM payment WHERE payment_linked_purchase_id = purchase_id)                                
                                ) AS payment_balance
                                FROM
                                    purchase
                                LEFT JOIN
                                    employee ON employee.employee_id = purchase.purchase_record_created_by
                                ORDER BY
                                    purchase_id DESC;');
        return $result->result_array();
    }

    /*
     * Retrieve all draft purchases
     */

    public function get_all_draft_purchases() {
        $result = $this->db->query('SELECT 
                                        *
                                    FROM
                                        draft_purchase
                                    LEFT JOIN
                                        employee ON employee.employee_id = draft_purchase.purchase_record_created_by;');
        return $result->result_array();
    }

    /**
     * Get entries for an purchase
     */
    public function get_purchase_entries($purchase_id) {

        $query = 'SELECT 
                    *,
                    (pe_product_quantity - IFNULL(SUM(dne_product_quantity), 0)) AS debit_note_balance
                    FROM purchase_entries 
                    LEFT JOIN debit_note_entries
                    ON
                    purchase_entries.pe_id = debit_note_entries.dne_linked_pe_id
                    WHERE
                    pe_linked_purchase_id = ' . $purchase_id . ' group by pe_id;';

        $result = $this->db->query($query);
        return $result->result_array();
    }
    
    /**
     * Get entries for an purchase
     */
    public function get_purchase_charge_entries($purchase_id) {
        //$query = $this->db->get_where('purchase_product_entries', array('ipe_linked_purchase_id' => $purchase_id));
        $query = 'SELECT 
                    *
                    FROM purchase_charge_entries 
                    WHERE
                    pce_linked_purchase_id = ' . $purchase_id . ' group by pce_id;';

        $result = $this->db->query($query);
        return $result->result_array();
    }
    
    
    /**
     * Get entries for an purchase
     */
    public function get_draft_purchase_charge_entries($purchase_id) {
        //$query = $this->db->get_where('purchase_product_entries', array('ipe_linked_purchase_id' => $purchase_id));
        $query = 'SELECT 
                    *
                    FROM draft_purchase_charge_entries 
                    WHERE
                    pce_linked_purchase_id = ' . $purchase_id . ' group by pce_id;';

        $result = $this->db->query($query);
        return $result->result_array();
    }


    /*
     * Get purchase using purchase_id
     */

    public function get_purchase_with_id($purchase_id) {
        $query = $this->db->get_where('purchase', array('purchase_id' => $purchase_id));
        return $query->row_array();
    }

    /**
     * Get entries for an purchase
     */
    public function get_draft_purchase_entries($purchase_id) {
        $query = 'SELECT 
                    *
                    FROM draft_purchase_entries 
                    WHERE
                    pe_linked_purchase_id = ' . $purchase_id . ' group by pe_id;';

        $result = $this->db->query($query);
        return $result->result_array();
    }

    /*
     * Get purchase using purchase_id
     */

    public function get_draft_purchase_with_id($purchase_id) {
        $query = $this->db->get_where('draft_purchase', array('purchase_id' => $purchase_id));
        return $query->row_array();
    }

    /*
     * Add new purchase

      0 - Tax
      1 - HSN
      2 - rate
      3 - quantity
      4 - discount

     * IMPORTANT - while editing purchase entries, always delete old entries and insert edited ones so that new cne_id are assgined. Failure to
     * do so may lead to error while calculating inventory
     */

    public function save_purchase($purchase_id, $purchase, $purchase_entries, $purchase_charge_entries, $purchase_action) {

        try {
            if ($purchase_action == 'previewDraft') {
                $purchase_action = 'draft';
            }

            log_message('debug', 'add_purchase. - $purchase = ' . print_r($purchase, 1));
            date_default_timezone_set('Asia/Kolkata');  //PHP server time zone. This won't set mySQl time zone. Needs to be set separately.
            $this->load->model('company_model');
            $this->load->model('owner_company_model');
            $this->load->model('payment_term_model');
            $this->load->model('state_model');
            $this->load->model('transporter_model');
            $this->load->model('unique_quantity_code_model');

            //add other data
            $owner_company = $this->owner_company_model->get_owner_company();

            $purchase['purchase_oc_name'] = $owner_company['oc_billing_name'];
            $purchase['purchase_oc_address'] = $owner_company['oc_billing_address'];
            $purchase['purchase_oc_city_name'] = $owner_company['city_name'];
            $purchase['purchase_oc_gstin'] = $owner_company['oc_gst_number'];
            $purchase['purchase_oc_pan_number'] = $owner_company['oc_pan_number'];
            $purchase['purchase_oc_logo_path'] = $owner_company['oc_logo_path'];
            $purchase['purchase_oc_district'] = $owner_company['district'];
            $purchase['purchase_oc_state'] = $owner_company['billing_state_name'];
            $purchase['purchase_oc_contact_number'] = $owner_company['oc_contact_number'];
            $purchase['purchase_oc_gst_supply_state_id'] = $owner_company['oc_gst_supply_state_id'];
            //$purchase['purchase_terms'] = $owner_company['oc_invoice_terms'];
            //$purchase['purchase_additional_terms'] = $owner_company['oc_additional_terms'];


            $this->db->query('SET time_zone = "+05:30";');

            $purchase_date = $purchase['purchase_date'];

            $payment_term = $this->payment_term_model->get_payment_term_using_id($purchase['purchase_linked_company_payment_term_id']);
            $purchase['purchase_linked_company_payment_term_name'] = $payment_term['payment_term_display_text'];
            $number_of_days_to_due = $payment_term['payment_term_days'];
            $purchase['purchase_due_date'] = date('Y-m-d', strtotime($purchase_date . ' + ' . $number_of_days_to_due . ' days'));

            /* add time component to date if purchase date is today.
             * else
             * add 23:59:59 if the purchase was created on a later date
             * This timestamp is to determine entry sequence for FIFO inventory
             * To be done for Invoice, purchase, credit note, debit note, inventory adjustment
             */
            if (date('Y-m-d', strtotime($purchase['purchase_date'])) == date("Y-m-d")) {
                $purchase['purchase_date'] = date("Y-m-d H:i:sa");
            } else {
                $purchase['purchase_date'] = date("Y-m-d 23:59:59", strtotime($purchase_date));
            } 

            if (!empty($purchase['purchase_linked_company_id'])) {
                $linked_company_id = $purchase['purchase_linked_company_id'];
                $linked_company = $this->company_model->get_company_with_id($linked_company_id);
                //$purchase['purchase_linked_company_gstin'] = $linked_company['company_gst_number'];
                $purchase['purchase_is_reverse_charge_applicable'] = $linked_company['is_reverse_charge_applicable_for_company'];
                $purchase['purchase_linked_company_contact_number'] = $linked_company['company_contact_number'];
            } else {
                $purchase['purchase_linked_company_id'] = NULL;
                //$purchase['purchase_linked_company_gstin'] = NULL;
                $purchase['purchase_is_reverse_charge_applicable'] = NULL;
                $purchase['purchase_linked_company_contact_number'] = NULL;
            }

            if(empty($purchase['purchase_linked_company_gstin'])){
                $purchase['purchase_linked_company_gstin'] = NULL;
            } else {
                $purchase['purchase_linked_company_gstin'] = strtoupper($purchase['purchase_linked_company_gstin']);
            }
            
            $purchase_linked_company_gst_supply_state_id = $purchase['purchase_linked_company_gst_supply_state_id'];
            $purchase['purchase_place_of_supply'] = $this->state_model->get_state_with_id($purchase_linked_company_gst_supply_state_id)['state_name'];

            $purchase['purchase_linked_company_billing_state_name'] = $this->state_model->get_state_with_id($purchase['purchase_linked_company_billing_state_id'])['state_name'];

            $this->db->trans_begin();
            
            $purchase_advance_payment_ids = array();
            if(!empty($purchase['purchase_advance_payment_ids'])){
                $purchase_advance_payment_ids = $purchase['purchase_advance_payment_ids'];
            }
            unset($purchase['purchase_advance_payment_ids']);
            
            $count = 0;
            $this->db->query('SET time_zone = "+05:30";');
            if ($purchase['purchase_status'] == 'new') {
                $purchase['purchase_status'] = $purchase_action;
                if ($purchase_action == 'confirm') {
                    $this->db->insert('purchase', $purchase);
                } else if ($purchase_action == 'draft') {
                    $this->db->insert('draft_purchase', $purchase);
                } else {
                    throw new Exception('Invalid purchase action for insert');
                }
                $purchase_id = $this->db->insert_id();
            } else {
                // user is editing an existing purchase. 
                if ($purchase_action == $purchase['purchase_status']) {

                    if ($purchase_action == 'draft') {

                        $this->db->where('pe_linked_purchase_id', $purchase_id);
                        $this->db->delete('draft_purchase_entries');

                        $this->db->where('pce_linked_purchase_id', $purchase_id);
                        $this->db->delete('draft_purchase_charge_entries');
                        
                        $this->db->where('purchase_id', $purchase_id);
                        $this->db->update('draft_purchase', $purchase);
                    } else if ($purchase_action == 'confirm') {

                        //retain time component if date not edited by user
                        $purchase_last_copy = $this->get_purchase_with_id($purchase_id);
                        if (date('Y-m-d', strtotime($purchase['purchase_date'])) == strtotime($purchase_last_copy['purchase_date'])) {
                            $purchase['purchase_date'] = $purchase_last_copy['purchase_date'];
                        }

                        $this->db->where('pe_linked_purchase_id', $purchase_id);
                        $this->db->delete('purchase_entries');
                       
                        $this->db->where('pce_linked_purchase_id', $purchase_id);
                        $this->db->delete('purchase_charge_entries');                        

                        $this->db->where('purchase_id', $purchase_id);
                        $this->db->update('purchase', $purchase);
                    } else {
                        throw new Exception('Invalid purchase action for update');
                    }
                } else if ($purchase['purchase_status'] == 'draft' && $purchase_action == 'confirm') {   //confirming a draft purchase
                    //delete draft purchase
                    $this->db->where('pe_linked_purchase_id', $purchase_id);
                    $this->db->delete('draft_purchase_entries');
                    
                    $this->db->where('pce_linked_purchase_id', $purchase_id);
                    $this->db->delete('draft_purchase_charge_entries');
                        
                    $this->db->where('purchase_id', $purchase_id);
                    $this->db->delete('draft_purchase');

                    $purchase['purchase_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
                    
                    $purchase['purchase_status'] = $purchase_action;
                    $this->db->insert('purchase', $purchase);
                    $purchase_id = $this->db->insert_id();
                } else {
                    throw new Exception('Invalid purchase status and required action');
                }
            }

            //manage advance payments
            if ($purchase['purchase_status'] == 'draft') {
                  
                $delete_query = 'Delete from purchase_advance_payment where pap_draft_purchase_id = "' . $purchase_id . '"';
                if(sizeof($purchase_advance_payment_ids) > 0){
                    $delete_query.=' AND pap_advance_payment_id NOT IN (' . implode(',', $purchase_advance_payment_ids) . ')';
                }
                
                $this->db->query($delete_query);
                
                foreach ($purchase_advance_payment_ids as $purchase_advance_payment_id) {
                    $purchase_advance_payment_amount = $this->db->query('Select * from '
                                    . 'purchase_advance_payment where pap_draft_purchase_id = "' . $purchase_id .
                                    '" AND pap_advance_payment_id = "' . $purchase_advance_payment_id . '"')->result_array();
                    if (sizeof($purchase_advance_payment_amount) == 0) {
                        $this->db->insert('purchase_advance_payment', array('pap_advance_payment_id' => $purchase_advance_payment_id,
                            'pap_draft_purchase_id' => $purchase_id,
                            'pap_allocated_amount' => 0));
                    }
                }
            } else if ($purchase['purchase_status'] == 'confirm') {
                //delete all unlinked advance payments
                $delete_query = 'Delete from purchase_advance_payment where pap_purchase_id = "' . $purchase_id . '"';
                if(sizeof($purchase_advance_payment_ids) > 0){
                    //$delete_query.=' AND pap_advance_payment_id NOT IN (' . implode(',', $purchase_advance_payment_ids) . ')';
                }
                $this->db->query($delete_query);
                //now check if exisitng advance payments amount against this purchase is equal to the purchase amount
                $advance_payment_amount_against_purchase = $this->db->query('Select SUM(pap_allocated_amount) as advance_payment_amount_against_purchase from purchase_advance_payment where pap_purchase_id = "' . $purchase_id . '"')->row_array()['advance_payment_amount_against_purchase'];

                if (empty($advance_payment_amount_against_purchase)) {
                    $advance_payment_amount_against_purchase = 0;
                }

                $balance_amount = $purchase['purchase_amount'] - $advance_payment_amount_against_purchase;
                
                foreach ($purchase_advance_payment_ids as $purchase_advance_payment_id) {
                    if ($balance_amount > 0) {
                        $advance_payment = $this->advance_payment_model->get_advance_payment_with_id($purchase_advance_payment_id);

                        //calculate amount
                        //part of the advance payment amount that has been used
                        $used_advance_payment_amount = $this->db->query('Select SUM(pap_allocated_amount) as pap_allocated_amount from purchase_advance_payment '
                                        . 'where pap_advance_payment_id = "' . $purchase_advance_payment_id . '"')->row_array()['pap_allocated_amount'];

                        $purchase_advance_payment_amount = $this->db->query('Select SUM(pap_allocated_amount) as pap_allocated_amount from purchase_advance_payment '
                                        . 'where pap_purchase_id = "' . $purchase_id . '" AND pap_advance_payment_id = "' . $purchase_advance_payment_id . '"')->row_array()['pap_allocated_amount'];
                        
                        if (empty($purchase_advance_payment_amount) 
                                || $used_advance_payment_amount > 0) {

                            $available_advance_payment_amount = $advance_payment['advance_payment_amount'] - $used_advance_payment_amount;

                            if ($balance_amount > $available_advance_payment_amount) {
                                $this->db->insert('purchase_advance_payment', array('pap_advance_payment_id' => $purchase_advance_payment_id,
                                    'pap_purchase_id' => $purchase_id,
                                    'pap_allocated_amount' => $available_advance_payment_amount));
                                $balance_amount = $balance_amount - $available_advance_payment_amount;
                            } else {
                                $this->db->insert('purchase_advance_payment', array('pap_advance_payment_id' => $purchase_advance_payment_id,
                                    'pap_purchase_id' => $purchase_id,
                                    'pap_allocated_amount' => $balance_amount));
                                $balance_amount = 0;
                            }
                        } else {
                            $balance_amount = $balance_amount - $purchase_advance_payment_amount;
                        }
                    }
                }
            } else {
                throw new Exception('Invalid purchase status');
            }

            
            //Get all taxes
            $tax_query = $this->db->get('tax');
            $taxes = $tax_query->result_array();
            $tax_id_percent_map = array();
            $tax_id_name_map = array();
            foreach ($taxes as $tax) {
                $tax_id_percent_map[$tax['tax_id']] = $tax['tax_percent'];
                $tax_id_name_map[$tax['tax_id']] = $tax['tax_name'];
            }

            if ($purchase_id > 0) {
                $pe['pe_linked_purchase_id'] = $purchase_id;

                foreach ($purchase_entries as $entry_id => $purchase_entry) {
                    $pe['pe_entry_id'] = $entry_id;
                    foreach ($purchase_entry as $product_id => $product_info) {
                        $pe['pe_linked_product_id'] = $product_id;

                        $pe['pe_product_name'] = $product_info[0];
                        $pe['pe_tax_id_applicable'] = $product_info[1];
                        $pe['pe_tax_percent_applicable'] = $tax_id_percent_map[$product_info[1]]; // fetch tax percent from $tax_id_percent_map
                        $pe['pe_tax_name_applicable'] = $tax_id_name_map[$product_info[1]]; // fetch tax name from $tax_id_name_map
                        $pe['pe_product_hsn_code'] = $product_info[2];
                        $pe['pe_product_rate'] = $product_info[3];
                        $pe['pe_product_quantity'] = $product_info[4];
                        //$pe['pe_product_uqc_id'] = $product_info[5];
                        //$pe['pe_product_uqc_text'] = $product_info[6];
                        $pe['pe_product_uqc_id'] = $this->product_model->get_product_with_id($product_id)['product_uqc_id'];
                        $pe['pe_product_uqc_text'] = $this->unique_quantity_code_model->get_uqc_with_id($pe['pe_product_uqc_id'])['uqc_text'];
                        $pe['pe_discount'] = $product_info[7];

                        if ($purchase_action == 'confirm') {
                            $this->db->insert('purchase_entries', $pe);
                        } else if ($purchase_action == 'draft') {
                            $this->db->insert('draft_purchase_entries', $pe);
                        } else {
                            throw new Exception('Invalid purchase action while inserting purchase entries');
                        }
                        if ($this->db->insert_id() > 0) {
                            $count++;
                        }
                    }
                }
                
                //enter charges
                foreach ($purchase_charge_entries as $charge_entry_id => $purchase_charge_entry) {
                    $pce['pce_entry_id'] = $charge_entry_id;
                    foreach ($purchase_charge_entry as $charge_id => $charge_info) {
                        $pce['pce_charge_id'] = $charge_id;
                        $pce['pce_linked_purchase_id'] = $purchase_id;
                        $pce['pce_charge_name'] = $charge_info[0];  
                        $pce['pce_taxable_amount'] = $charge_info[1];
                        
                        $pce['pce_tax_id_applicable'] = $charge_info[2];
                        $pce['pce_tax_percent_applicable'] = $tax_id_percent_map[$charge_info[2]]; // fetch tax percent from $tax_id_percent_map
                        $pce['pce_tax_name_applicable'] = $tax_id_name_map[$charge_info[2]]; // fetch tax name from $tax_id_name_map
                        
                        if ($purchase_action == 'confirm') {
                            $this->db->insert('purchase_charge_entries', $pce);
                        } else if ($purchase_action == 'draft') {
                            $this->db->insert('draft_purchase_charge_entries', $pce);
                        } else {
                            throw new Exception('Invalid purchase action while inserting purchase entries');
                        }
                    }
                }
                
            } else {
                $response['result'] = $this->db->error();
                $response['query'] = $this->db->last_query();
            }


            if ($count > 0) {
                $this->db->trans_commit();
                $response['Result'] = "Success";
            } else {
                log_message('debug', 'confirm_dispatch. ROLLBACK. ' . print_r($response));
                $this->db->trans_rollback();
            }

            log_message('debug', 'edit_purchase. - Query = ' . $this->db->last_query());
            return $purchase_id;
        } finally {
            $this->load->model('inventory_model');
            $this->inventory_model->evaluate_inventory_figures();
        }
    }

}
