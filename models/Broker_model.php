<?php

class Broker_model extends CI_Model {

    function deleteBrokerById($id) {
        log_message('debug', 'deleteBrokerBy. - $id = ' . print_r($id, 1));

        $this->db->where('broker_id', $id);
        $this->db->delete('broker');

        log_message('debug', 'deleteBrokerBy. - Query = ' . $this->db->last_query());

        if ($this->db->affected_rows() == '1') {
            log_message('debug', 'deleteBrokerBy. - BROKER DELETED ');
            return TRUE;
        } else {
            log_message('debug', 'deleteBrokerBy. - FALIED TO DELETE BROKER ');
            return FALSE;
        }
    }

    public function get_broker_count() {
        $query_string = 'SELECT COUNT(broker_id) AS count from Broker';
        $result = $this->db->query($query_string);
        return $result->row_array();
    }
    
    /**
     * 
     * @return type
     */
    public function get_all_brokers_for_report(){
        $result = $this->db->query('SELECT 
                                        CONCAT("BRK", LPAD(broker_id, "5", "0")) AS broker_id,
                                        broker_name as agent_name, 
                                        broker_address as address,
                                        broker_area as area,
                                        broker_pincode as pincode,
                                        city_name,
                                        district,
                                        state_name,
                                        broker_contact_person_name as contact_person_name,
                                        broker_email_address as email_address,
                                        broker_contact_number as contact_number,
                                        broker_gst_number as gst_number,
                                        broker_commission as commission
                                    FROM
                                        broker
                                    LEFT JOIN
                                        location
                                    ON
                                        broker.broker_location_id = location.location_id
                                    LEFT JOIN
                                        state
                                    ON
                                        location.state_id = state.state_tin_number;');
        return $result->result_array();
    }

    /**
     * 
     * @return type
     */
    public function get_all_brokers() {
        $result = $this->db->query('SELECT 
                                    *
                                    FROM
                                    broker
                                    LEFT JOIN
                                    employee ON employee.employee_id = broker.broker_record_created_by
                                    LEFT JOIN
                                    location
                                    ON
                                    broker.broker_location_id = location.location_id;');
        return $result->result_array();
    }

    public function get_broker_with_id($id) {
        $query = $this->db->get_where('broker', array('broker_id' => $id));
        return $query->row_array();
    }

    /**
     * Save a broker
     */
    public function save_broker($data, $broker_id = NULL) {
        log_message('debug', 'save_broker. - $id = ' . print_r($broker_id, 1) . '$data = ' . print_r($data, 1));
        if($broker_id == NULL){
            $this->db->query('SET time_zone = "+05:30";');
            $broker_id = $this->db->insert('broker', $data);
        } else {
            $this->db->where('broker_id', $broker_id);
            $this->db->update('broker', $data);
        }
        
        $response['query'] = $this->db->last_query();
        log_message('debug', 'save_broker. - response = ' . print_r($response, 1));
        return $response;
    } 
}
