<?php

class Chart_of_accounts_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    /*
     * Retrieve all account types
     */

    public function get_all_account_types() {
        $result = $this->db->query('SELECT * FROM account_type');
        return $result->result_array();
    }

    /*
     * Retrieve account using ID
     */

    public function get_account_type_using_id($at_id) {
        $result = $this->db->query('SELECT * FROM account_type where at_id = ' . $at_id);
        return $result->result_array();
    }

    /*
     * Retrieve all account types
     */

    public function get_all_coa_for_report() {
        $result = $this->db->query("SELECT 
                                        CONCAT('COA', LPAD(coa_id, '5', '0')) AS chart_of_account_id,
                                        coa_account_name as account_name,
                                        coa_account_code as account_code,
                                        coa_account_description as account_description
                                    FROM
                                        chart_of_accounts
                                            LEFT JOIN
                                        account_type ON account_type.at_id = chart_of_accounts.coa_at_id
                                            LEFT JOIN
                                        employee ON employee.employee_id = chart_of_accounts.coa_id");

        return $result->result_array();
    }
    
    /*
     * Retrieve all account types
     */

    public function get_all_coa() {
        $result = $this->db->query('SELECT 
                                        *
                                    FROM
                                        chart_of_accounts
                                            LEFT JOIN
                                        account_type ON account_type.at_id = chart_of_accounts.coa_at_id
                                            LEFT JOIN
                                        employee ON employee.employee_id = chart_of_accounts.coa_id');

        return $result->result_array();
    }

    /**
     * Get all COA's of a particular account type
     * @param type $at_id
     * @return type
     */
    public function get_all_coa_of_account_type($at_id) {
        $result = $this->db->query('SELECT * FROM chart_of_accounts where coa_at_id = ' . $at_id);
        return $result->result_array();
    }

    /**
     * Retrieve coa using ID
     */
    public function get_coa_with_id($coa_id) {
        $result = $this->db->query('SELECT 
                                        *
                                    FROM
                                        chart_of_accounts
                                            WHERE
                                            coa_id = "' . $coa_id . '"');

        return $result->row_array();
    }

    /**
     * 
     * @return type
     */
    public function get_all_parent_coa() {
        $result = $this->db->query('SELECT * FROM chart_of_accounts where coa_subaccount_of_id IS NULL');
        return $result->result_array();
    }

    /**
     * 
     * @return type
     */
    public function get_all_subaccounts() {
        $result = $this->db->query('SELECT * FROM chart_of_accounts where coa_subaccount_of_id IS NOT NULL');
        return $result->result_array();
    }

    /**
     * Save opening balance for all COA
     */
    public function save_coa_opening_balance($data) {
        $coa_ob_date = $data['coa_opening_balance_date'];
        
        $oc_data = array('oc_opening_balance_date' => $coa_ob_date);
        $this->db->update('owner_company', $oc_data);
            
        $coa_opening_balances = $data['coa_ob'];

        foreach ($coa_opening_balances as $coa_id => $coa_ob_data) {

            $coa = array();
            if (isset($coa_ob_data[0])) {
                $coa['coa_opening_balance'] = $coa_ob_data[0];
                $coa['coa_opening_balance_transaction_type'] = 'debit';
            } else if (isset($coa_ob_data[1])) {
                $coa['coa_opening_balance'] = $coa_ob_data[1];
                $coa['coa_opening_balance_transaction_type'] = 'credit';
            } else {
                throw new Exception('Transaction type not defined');
            }

            //$coa['coa_opening_balance_date'] = $coa_ob_date;

            $this->db->where('coa_id', $coa_id);
            $this->db->update('chart_of_accounts', $coa);
        }
        //update opening balance date for opening stock
        $this->load->model('inventory_model');
        $this->inventory_model->update_opening_balance();
    }

    /**
     * Save or edit chart of accounts record
     * @param type $coa_id
     * @param type $data
     */
    public function save_coa($coa_id, $data) {
        //log_message('debug', 'save_coa. - $data = ' . print_r($data));

        try {
            if (!isset($data['coa_at_id'])) {
                if (isset($data['coa_subaccount_of_id'])) {
                    $account = $this->get_coa_with_id($data['coa_subaccount_of_id']);
                    $data['coa_at_id'] = $account['coa_at_id'];
                } else {
                    throw new Exception("Atleast one of 1) Account type OR 2) Parent account type needs to be specified");
                }
            }

            if (!isset($data['coa_subaccount_of_id'])) {
                $data['coa_subaccount_of_id'] = NULL;
            }

            $data['coa_opening_balance_transaction_type'] = 'debit';
            $this->db->query('SET time_zone = "+05:30";');
            if ($coa_id == NULL) {
                $coa_id = $this->db->insert('chart_of_accounts', $data);
            } else {
                $this->db->where('coa_id', $coa_id);
                $this->db->update('chart_of_accounts', $data);
            }

            if ($coa_id > 0) {
                $response['result'] = "success";
            } else {
                $response['result'] = $this->db->error();
                $response['query'] = $this->db->last_query();
            }

            //log_message('debug', 'save_coa. - query = ' . $this->db->last_query());
            //log_message('debug', 'save_coa. - response = ' . print_r($response));
            return $response;
        } finally {
            $this->load->model('system_journal_model');
            $this->system_journal_model->calculate_system_journal();
        }
    }

    /*
     * Delete COA
     */

    function delete_chart_of_account_by_id($coa_id) {
        log_message('debug', 'delete_chart_of_account_by_id. - $id = ' . print_r($coa_id, 1));

        try {
            $this->db->where('coa_id', $coa_id);
            $this->db->delete('chart_of_accounts');

            log_message('debug', 'delete_chart_of_account_by_id. - Query = ' . $this->db->last_query());

            if ($this->db->affected_rows() == '1') {
                log_message('debug', 'delete_chart_of_account_by_id. - DELETED ');
                return TRUE;
            } else {
                log_message('debug', 'delete_chart_of_account_by_id. - FALIED TO DELETE ');
                return FALSE;
            }
        } finally {
            $this->load->model('system_journal_model');
            $this->system_journal_model->calculate_system_journal();
        }
    }

}

?>