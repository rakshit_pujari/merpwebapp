<?php

class Supplementary_invoice_reason_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    /**
      Fetch all supplementary_invoice_reason
     */
    public function get_all_reasons() {
        $query = $this->db->get('supplementary_invoice_reason');
        return $query->result_array();
    }

    /**
      Get supplementary_invoice_reason using ID
     */
    public function get_sir_with_id($sir_id) {
        $query = $this->db->get_where('supplementary_invoice_reason', array('sir_id' => $sir_id));
        return $query->row_array();
    }

}
