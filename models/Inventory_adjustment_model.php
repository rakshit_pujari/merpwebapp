<?php

class Inventory_adjustment_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }
    
    public function get_inventory_adjustment_for_report(){
        $result = $this->db->query('SELECT 
                                    product_name,
                                    ia_reason as reason,
                                    ia_quantity as quantity,
                                    ia_value as value
                                        from inventory_adjustment 
                                    LEFT JOIN
                                        product
                                    ON
                                        inventory_adjustment.ia_product_id = product.product_id');
        return $result->result_array();
    }

    public function delete_inventory_adjustment($ia_id) {
        try {
            $this->db->where('ie_inventory_adjustment_id', $ia_id);
            $this->db->delete('inventory_entry');

            $this->db->where('ia_id', $ia_id);
            $this->db->delete('inventory_adjustment');

            if ($this->db->affected_rows() == '1') {
                log_message('debug', 'deleteLocationBy. - DELETED ');
                return TRUE;
            } else {
                log_message('debug', 'deleteLocationBy. - FALIED TO DELETE ');
                return FALSE;
            }
        } finally {
            $this->load->model('inventory_model');
            $this->inventory_model->evaluate_inventory_figures();
        }
    }

    /*
     * Retrieve all inventory_adjustments
     */

    public function get_all_inventory_adjustments() {
        $result = $this->db->query('SELECT * from 
                                        inventory_adjustment 
                                    LEFT JOIN 
                                        employee
                                    ON
                                        employee.employee_id = inventory_adjustment.ia_record_created_by
                                    LEFT JOIN
                                        product
                                    ON
                                        inventory_adjustment.ia_product_id = product.product_id
                                    LEFT JOIN
                                        unique_quantity_code
                                    ON 
                                        unique_quantity_code.uqc_id = product.product_uqc_id
                                    ORDER BY
                                        ia_id DESC;');
        return $result->result_array();
    }

    /*
     * Get inventory_adjustment using inventory_adjustment_id
     */

    public function get_inventory_adjustment_with_id($inventory_adjustment_id) {
        $query = $this->db->get_where('inventory_adjustment', array('ia_id' => $inventory_adjustment_id));
        return $query->row_array();
    }

    /*
     * Get inventory_adjustment using inventory_adjustment_id
     */

    public function get_inventory_entry_before_ia($inventory_adjustment_id) {
        /*$query = $this->db->query('SELECT
                                        *
                                    FROM
                                        inventory_entry
                                    WHERE
                                        ie_id = ((SELECT 
                                                ie_id
                                            FROM
                                                inventory_entry
                                            WHERE
                                                ie_inventory_adjustment_id = ' . $inventory_adjustment_id . '
                                            ORDER BY ie_id ASC
                                            LIMIT 1) - 1)');*/
        $inventory_adjustment = $this->get_inventory_adjustment_with_id($inventory_adjustment_id);
        
        $query = 'SELECT 
                    *
                FROM
                    inventory_entry
                WHERE
                    ie_it_product_id = '.$inventory_adjustment['ia_product_id'].
                        ' AND ie_id < (SELECT 
                            ie_id
                        FROM
                            inventory_entry
                        WHERE
                            ie_inventory_adjustment_id = '.$inventory_adjustment['ia_id'].' 
                                ORDER BY ie_id ASC LIMIT 1)
                ORDER BY ie_id DESC
                LIMIT 1;';
        //throw new Exception($query);
        $result = $this->db->query($query);
        
        return $result->row_array();
    }

    /*
     * Save inventory adjustment
     */

    public function save_inventory_adjustment($ia_id, $data) {
        log_message('debug', 'save_inventory_adjustment. - $data = ' . print_r($data, 1));

        try {
            $this->db->query('SET time_zone = "+05:30";');
            if ($ia_id == null) {
                $inventory_adjustment_id = $this->db->insert('inventory_adjustment', $data);
            } else {
                $this->db->where('ia_id', $ia_id);
                $this->db->update('inventory_adjustment', $data);
            }

            if ($ia_id > 0) {
                $response['Result'] = "Success";
            } else {
                $response['Result'] = $this->db->error();
                $response['query'] = $this->db->last_query();
            }
            log_message('debug', 'edit_inventory_adjustment. - Query = ' . $this->db->last_query());
            log_message('debug', 'edit_inventory_adjustment. - response = ' . print_r($response, 1));
            return $response;
        } finally {
            $this->load->model('inventory_model');
            $this->inventory_model->evaluate_inventory_figures();
        }
    }

}
