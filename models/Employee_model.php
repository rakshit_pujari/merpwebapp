<?php

class Employee_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }
    
    public function get_employee_rights_list(){
       $query_string = 'Select * from employee_right';
       $er_array = $this->db->query($query_string)->result_array();
       
       $employee_rights = array();
       foreach ($er_array as $er_item) {
           $er_class = $er_item['er_class'];
           $er_action = $er_item['er_action'];
           $er_id = $er_item['er_id'];
           if(!isset($employee_rights[$er_class])){
               $employee_rights[$er_class] = array();
           }
           $employee_rights[$er_class][$er_action] = $er_id;
       }
       
       return $employee_rights; 
    }
    
    /**
     * 
     * @param type $class
     * @param type $action
     * @return type
     */
    public function get_class_module($class, $action){
        $query_string = 'Select er_module from employee_right where er_class = "'.$class.'" and er_action = "'.$action.'"';
        $result = $this->db->query($query_string);
        if($result->num_rows() > 0){
            return $result->row_array()['er_module'];
        } 
        return null;
    }
    
    /**
     * 
     * @return type
     */
    public function get_module_wise_employee_rights_list(){
       $query_string = 'Select * from employee_right';
       $er_array = $this->db->query($query_string)->result_array();
       
       $employee_rights = array();
       foreach ($er_array as $er_item) {
           $er_class = $er_item['er_class'];
           $er_action = $er_item['er_action'];
           $er_module = $er_item['er_module'];
           $er_id = $er_item['er_id'];
           if(!isset($employee_rights[$er_class])){
               $employee_rights[$er_class] = array();
           }
           $employee_rights[$er_module][$er_class][$er_action] = $er_id;
       }
       
       return $employee_rights; 
    }

    /**
     * Login
     * @param type $username
     * @param type $password
     * @return type
     */
    public function get_user($username, $password){
        $query_string = 'Select * from employee where employee_username = "'.$username.'" and employee_password = "'.$password.'"';
        $result = $this->db->query($query_string);
        if($result->num_rows() == 1){
            return $result->row_array();
        } 
        return null;
    }
    /**
     * 
     * @param type $id
     * @return boolean
     */
    function delete_employee_by_id($id) {
        log_message('debug', 'deleteEmployeeBy. - $id = ' . print_r($id, 1));

        $this->db->where('employee_id', $id);
        $this->db->delete('employee');

        log_message('debug', 'deleteEmployeeBy. - Query = ' . $this->db->last_query());

        if ($this->db->affected_rows() == '1') {
            log_message('debug', 'deleteEmployeeBy. - EMPLOYEE DELETED ');
            return TRUE;
        } else {
            log_message('debug', 'deleteEmployeeBy. - FALIED TO DELETE EMPLOYEE ');
            return FALSE;
        }
    }

    /**
     * 
     * @return type
     */
    public function get_employee_count() {
        $query_string = 'SELECT COUNT(employee_id) AS count from employee';
        $result = $this->db->query($query_string);
        return $result->row_array();
    }

    public function get_all_employees_for_report(){
        $result = $this->db->query('SELECT 
                                        CONCAT("EMP", LPAD(employee_id, "5", "0")) AS employee_id,
                                        employee_name,
                                        employee_address,
                                        employee_area,
                                        city_name,
                                        employee_pincode,
                                        state_name,
                                        employee_email_address,
                                        employee_contact_number,
                                        employee_pan_number,
                                        employee_esic_number,
                                        employee_department,
                                        employee_designation,
                                        employee_username,
                                        employee_account_number
                                    FROM
                                        employee
                                    LEFT JOIN
                                        location
                                    ON
                                        employee_location_id = location.location_id
                                    LEFT JOIN
                                        state
                                    ON
                                        location.state_id = state.state_tin_number;');
        return $result->result_array();
    }
    
    /**
     * 
     * @return type
     */
    public function get_all_employees() {
        $result = $this->db->query('SELECT 
                                        employee.employee_id,
                                        employee.employee_name,
                                        employee.employee_address,
                                        employee.employee_area,
                                        employee.employee_location_id,
                                        employee.employee_pincode,
                                        employee.employee_email_address,
                                        employee.employee_contact_number,
                                        employee.employee_pan_number,
                                        employee.employee_esic_number,
                                        employee.employee_department,
                                        employee.employee_designation,
                                        employee.employee_username,
                                        employee.employee_password,
                                        employee.employee_bank_id,
                                        employee.employee_account_number,
                                        employee.employee_record_created_by,
                                        employee.employee_record_creation_time,
                                        t1.employee_name as employee_record_created_by_name,
                                        location_id,
                                        city_name
                                    FROM
                                        employee
                                    LEFT JOIN
                                        location
                                    ON
                                        employee.employee_location_id = location.location_id
                                    LEFT JOIN
                                        employee AS t1
                                    ON
                                        t1.employee_id = employee.employee_record_created_by;');
        return $result->result_array();
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function get_employee_with_id($id) {
        $query = $this->db->get_where('employee', array('employee_id' => $id));
        return $query->row_array();
    }
    
    /**
     * 
     * @param type $employee_id
     * @return array
     */
    public function get_rights_assigned_to_employee($employee_id){
        $employee_rights_array = $this->db->get_where('employee_rights_assignment', array('era_employee_id' => $employee_id))->result_array();
        $assigned_rights = array();
        foreach ($employee_rights_array as $employee_right) {
            array_push($assigned_rights, $employee_right['era_er_id_assigned']);
        }
        return $assigned_rights;
    }


    /**
     * 
     * @param type $employee_id
     * @param type $employee_rights
     */    
    public function save_employee_rights($employee_id, $employee_rights){
        $this->db->where('era_employee_id', $employee_id);
        $this->db->delete('employee_rights_assignment');
        foreach ($employee_rights as $er_id => $assignment_status) {
            $era = array();
            $era['era_employee_id'] = $employee_id;
            $era['era_er_id_assigned'] = $er_id;
            $this->db->insert('employee_rights_assignment', $era);
        }
    }
    
    /**
     * 
     * @param type $id
     * @param type $data
     * @return type
     */
    public function save_employee($employee_id, $data) {
        log_message('debug', 'edit_employee. - $id = ' . print_r($employee_id, 1) . '$data = ' . print_r($data, 1));
      
        if($employee_id == NULL){
            $this->db->query('SET time_zone = "+05:30";');
            $employee_id = $this->db->insert('employee', $data);
        } else {
            $this->db->where('employee_id', $employee_id);
            $this->db->update('employee', $data);
        }

        if ($this->db->affected_rows() > 0 
                || $employee_id > 0) {
            $response['result'] = "success";
        } else {
            $response['result'] = "no records changed";
            $response['query'] = $this->db->last_query();
        }

        $response['query'] = $this->db->last_query();
        log_message('debug', 'edit_employee. - response = ' . print_r($response, 1));
        return $response;
    }
}
