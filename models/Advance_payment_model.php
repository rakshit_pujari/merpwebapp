<?php

class Advance_payment_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    /**
     * 
     * @param type $purchase_status
     * @param type $purchase_id
     * @return type
     * @throws Exception
     */
    public function get_linked_advance_payments_ids($purchase_status, $purchase_id) {
        $query = 'Select pap_advance_payment_id from '
                . 'purchase_advance_payment where ';

        if ($purchase_status == 'draft') {
            $query .= 'pap_draft_purchase_id';
        } else if ($purchase_status == 'confirm') {
            $query .= 'pap_purchase_id';
        } else {
            throw new Exception('Invalid purchase status');
        }

        $query .= ' = "' . $purchase_id . '"';

        $advance_payments = $this->db->query($query)->result_array();

        $advance_payment_ids = array();
        foreach ($advance_payments as $advance_payment) {
            array_push($advance_payment_ids, $advance_payment['pap_advance_payment_id']);
        }

        return $advance_payment_ids;
    }
    
    /**
     * Get advance payments linked to the purchase and any other advance payments linked to that company with surplus balance
     * @param type $company_id
     * @param type $purchase_id
     * @return type
     */
    public function get_linked_and_surplus_balance_advance_payments($company_id, $purchase_id){
        
        $query = 'SELECT 
                    advance_payment_id,
                    advance_payment_linked_company_invoice_name,
                    advance_payment_linked_company_id,
                    advance_payment_record_creation_time,
                    advance_payment_date,
                    advance_payment_amount,
                    pap_purchase_id,
                    advance_payment_payment_mode,
                    balance_advance_payment_amount
                FROM
                    (SELECT 
                        advance_payment_id,
                            advance_payment_linked_company_invoice_name,
                            advance_payment_linked_company_id,
                            advance_payment_record_creation_time,
                            advance_payment_date,
                            advance_payment_amount,
                            pap_purchase_id,
                            advance_payment_payment_mode,
                            advance_payment_amount - IFNULL(SUM(pap_allocated_amount), 0) AS balance_advance_payment_amount
                    FROM
                        advance_payment
                    LEFT JOIN purchase_advance_payment ON advance_payment.advance_payment_id = purchase_advance_payment.pap_advance_payment_id
                    WHERE
                        pap_advance_payment_id IN (Select pap_advance_payment_id from purchase_advance_payment where pap_purchase_id = '.$purchase_id.
                    ') GROUP BY advance_payment_id';
                
                if(!empty($company_id)){
                    $query.= ' UNION ALL SELECT 
                        advance_payment_id,
                            advance_payment_linked_company_invoice_name,
                            advance_payment_linked_company_id,
                            advance_payment_record_creation_time,
                            advance_payment_date,
                            advance_payment_amount,
                            pap_purchase_id,
                            advance_payment_payment_mode,
                            advance_payment_amount - IFNULL(SUM(pap_allocated_amount), 0) AS balance_advance_payment_amount
                    FROM
                        advance_payment
                    LEFT JOIN purchase_advance_payment ON advance_payment.advance_payment_id = purchase_advance_payment.pap_advance_payment_id
                    WHERE
                        advance_payment_linked_company_id = '.$company_id.
                    ' GROUP BY advance_payment_id
                    HAVING (advance_payment_amount > IFNULL(SUM(pap_allocated_amount), 0)) ';
                }
                
                $query.=') AS advance
                    group by
                    advance_payment_id';
        
        $advance_payments = $this->db->query($query)->result_array();
        return $advance_payments;
    }
        /**
     * 
     * @param type $purchase_status
     * @param type $purchase_id
     * @return type
     * @throws Exception
     */
    public function get_linked_advance_payments($purchase_status, $purchase_id) {
        $query = 'Select '
                . 'advance_payment_id,
                    advance_payment_linked_company_invoice_name,
                    advance_payment_linked_company_id,
                    advance_payment_record_creation_time,
                    advance_payment_date,
                    advance_payment_amount,
                    pap_purchase_id,
                    advance_payment_payment_mode,
                    advance_payment_amount - IFNULL(SUM(pap_allocated_amount), 0) AS balance_advance_payment_amount from '
                . 'advance_payment'
                . ' LEFT JOIN purchase_advance_payment ON advance_payment.advance_payment_id = purchase_advance_payment.pap_advance_payment_id'
                . ' where ';

        if ($purchase_status == 'draft') {
            $query .= 'pap_draft_purchase_id';
        } else if ($purchase_status == 'confirm') {
            $query .= 'pap_purchase_id';
        } else {
            throw new Exception('Invalid purchase status');
        }

        $query .= ' = "' . $purchase_id . '" '
                . 'GROUP BY 
                pap_advance_payment_id';
        //throw new Exception($query);
        $advance_payments = $this->db->query($query)->result_array();
        return $advance_payments;
    }
    
        
        /*
     * Retrieve all advance payments
     */
    public function get_advance_payments_with_balance($linked_company_id = NULL) {
        
        $query = 'SELECT
                    advance_payment_id,
                    advance_payment_linked_company_invoice_name,
                    advance_payment_linked_company_id,
                    advance_payment_record_creation_time,
                    advance_payment_date,
                    advance_payment_amount,
                    pap_purchase_id,
                    advance_payment_payment_mode,
                    advance_payment_amount - IFNULL(SUM(pap_allocated_amount), 0) AS balance_advance_payment_amount
                FROM
                    advance_payment
                LEFT JOIN purchase_advance_payment ON advance_payment.advance_payment_id = purchase_advance_payment.pap_advance_payment_id';
        if($linked_company_id != NULL){
            $query.=' WHERE advance_payment_linked_company_id = '.$linked_company_id;
        }
        
        $query.=' GROUP BY
                    advance_payment_id
                HAVING
                    advance_payment_amount > IFNULL(SUM(pap_allocated_amount), 0)
                    ORDER BY 
                    advance_payment_id DESC;';
        
        $result = $this->db->query($query);
        
        $advance_payments = $result->result_array();
        
        foreach ($advance_payments as $key => $advance_payment) {
            $advance_payment['linked_purchases'] = $this->db->query('SELECT 
                                        pap_purchase_id, pap_allocated_amount
                                    FROM
                                        purchase_advance_payment
                                        WHERE pap_advance_payment_id = '.$advance_payment['advance_payment_id'])->result_array();
            
            $advance_payments[$key] = $advance_payment;
        }
        
        return $advance_payments;
    }
    
    /*
     * Get payment using advance_payment_id
     */
    public function get_advance_payment_with_id($advance_payment_id) {
        $query = 'SELECT 
                    *,
                    IFNULL(SUM(pap_allocated_amount), 0) AS advance_payment_total_allocated_amount
                FROM
                    advance_payment
                LEFT JOIN purchase_advance_payment ON advance_payment.advance_payment_id = purchase_advance_payment.pap_advance_payment_id
                WHERE advance_payment_id = '.$advance_payment_id;
                
        $result = $this->db->query($query);
        return $result->row_array();
    }
    
    /**
     * 
     * @return type
     */
    public function get_all_advance_payments_for_report(){
        $query = "SELECT 
                    CONCAT('ADP',
                            LPAD(advance_payment_id, '5', '0')) AS advance_payment_id,
                    advance_payment_linked_company_display_name AS customer_name,
                    advance_payment_date,
                    advance_payment_amount,
                    GROUP_CONCAT(CONCAT('PUR', LPAD(pap_purchase_id, '5', '0'))) AS linked_purchases,
                    GROUP_CONCAT(CONCAT('DBN', LPAD(pap_debit_note_id, '5', '0'))) AS linked_debit_notes,
                    advance_payment_payment_mode,
                    advance_payment_amount - IFNULL(SUM(pap_allocated_amount), 0) AS balance_advance_payment_amount
                FROM
                    advance_payment
                        LEFT JOIN
                    employee ON employee.employee_id = advance_payment.advance_payment_record_created_by
                        LEFT JOIN
                    purchase_advance_payment ON advance_payment.advance_payment_id = purchase_advance_payment.pap_advance_payment_id
                GROUP BY advance_payment_id
                ORDER BY advance_payment_id DESC";
        
        $result = $this->db->query($query);        
        return $result->result_array();
    }

    /*
     * Retrieve all advance payments
     */
    public function get_all_advance_payments() {
        
        $query = 'SELECT 
                    advance_payment_id,
                    advance_payment_linked_company_display_name,
                    advance_payment_linked_company_id,
                    employee_username,
                    advance_payment_record_creation_time,
                    advance_payment_date,
                    advance_payment_amount,
                    pap_purchase_id,
                    pap_debit_note_id,
                    advance_payment_payment_mode,
                    advance_payment_amount - IFNULL(SUM(pap_allocated_amount), 0) AS balance_advance_payment_amount
                FROM
                    advance_payment
                LEFT JOIN employee ON employee.employee_id = advance_payment.advance_payment_record_created_by
                LEFT JOIN purchase_advance_payment ON advance_payment.advance_payment_id = purchase_advance_payment.pap_advance_payment_id';
        
        $query.=' GROUP BY
                    advance_payment_id
                  ORDER BY 
                    advance_payment_id DESC;';
        
        $result = $this->db->query($query);
        
        $advance_payments = $result->result_array();
        
        foreach ($advance_payments as $key => $advance_payment) {
            $advance_payment['linked_purchases'] = $this->db->query('SELECT 
                                        pap_purchase_id, pap_allocated_amount, pap_debit_note_id
                                    FROM
                                        purchase_advance_payment
                                        WHERE pap_advance_payment_id = '.$advance_payment['advance_payment_id'].
                                    '  AND (pap_purchase_id IS NOT NULL OR pap_debit_note_id IS NOT NULL)')->result_array();
            
            $advance_payments[$key] = $advance_payment;
        }
        
        return $advance_payments;
    }

    /**
     * 
     * @param type $advance_payment_id
     * @param type $advance_payment
     * @return type
     */
    public function save_advance_payment($advance_payment_id, $advance_payment) {
        try {
            log_message('debug', 'add_payment. - $advance_payment = ' . print_r($advance_payment, 1));
            date_default_timezone_set('Asia/Kolkata');  //PHP server time zone. This won't set mySQl time zone. Needs to be set separately.

            $this->load->model('owner_company_model');
            $this->load->model('company_model');
            $this->load->model('state_model');
            //add other data
            $owner_company = $this->owner_company_model->get_owner_company();

            $advance_payment['advance_payment_oc_name'] = $owner_company['oc_billing_name'];
            $advance_payment['advance_payment_oc_address'] = $owner_company['oc_billing_address'];
            $advance_payment['advance_payment_oc_city_name'] = $owner_company['city_name'];
            $advance_payment['advance_payment_oc_gstin'] = $owner_company['oc_gst_number'];
            $advance_payment['advance_payment_oc_pan_number'] = $owner_company['oc_pan_number'];
            $advance_payment['advance_payment_oc_logo_path'] = $owner_company['oc_logo_path'];
            $advance_payment['advance_payment_oc_district'] = $owner_company['district'];
            $advance_payment['advance_payment_oc_state'] = $owner_company['billing_state_name'];
            $advance_payment['advance_payment_oc_contact_number'] = $owner_company['oc_contact_number'];
            $advance_payment['advance_payment_oc_gst_supply_state_id'] = $owner_company['oc_gst_supply_state_id'];
            $advance_payment['advance_payment_place_of_supply'] = $owner_company['supply_state_name'];
            
            if (!empty($advance_payment['advance_payment_linked_company_id'])) {
                $linked_company_id = $advance_payment['advance_payment_linked_company_id'];
                $linked_company = $this->company_model->get_company_with_id($linked_company_id);
                $advance_payment['advance_payment_linked_company_invoice_name'] = $linked_company['company_invoice_name'];
                $advance_payment['advance_payment_linked_company_display_name'] = $linked_company['company_display_name'];
                $advance_payment['advance_payment_linked_company_billing_address'] = $linked_company['company_billing_address'];
                $advance_payment['advance_payment_linked_company_shipping_address'] = $linked_company['company_shipping_address'];
                $advance_payment['advance_payment_linked_company_gst_supply_state_id'] = $linked_company['company_gst_supply_state_id'];
                $advance_payment['advance_payment_linked_company_contact_number'] = $linked_company['company_contact_number'];
                $advance_payment['advance_payment_linked_company_gstin'] = $linked_company['company_gst_number'];
                $advance_payment['advance_payment_place_of_supply'] = $this->state_model->get_state_with_id($linked_company['company_gst_supply_state_id'])['state_name'];
            } else {
                throw new Exception('Company ID not given');
            }
            
            if(!empty($advance_payment['advance_payment_credit_coa_id'])){
                $this->load->model('chart_of_accounts_model');
                $advance_payment['advance_payment_credit_coa_name'] = $this->chart_of_accounts_model->get_coa_with_id($advance_payment['advance_payment_credit_coa_id'])['coa_account_name'];
            } else {
                $advance_payment['advance_payment_credit_coa_id'] = null;
            }
            /* add time component to date if payment date is today.
             * else
             * add 23:59:59 if the payment was created on a later date
             */
            if (date('Y-m-d', strtotime($advance_payment['advance_payment_date'])) == date("Y-m-d")) {
                $advance_payment['advance_payment_date'] = date("Y-m-d H:i:sa");
            } else {
                $advance_payment['advance_payment_date'] = date("Y-m-d 23:59:59", strtotime($advance_payment['advance_payment_date']));
            }
            
            
            $this->db->trans_begin();
            $this->db->query('SET time_zone = "+05:30";');
            if ($advance_payment_id == null) {
                $this->db->insert('advance_payment', $advance_payment);
                $advance_payment_id = $this->db->insert_id();
            } else {
                $this->db->where('advance_payment_id', $advance_payment_id);
                $this->db->update('advance_payment', $advance_payment);
            }

            if ($advance_payment_id > 0) {
                $this->db->trans_commit();
            } else {
                log_message('debug', 'ROLLBACK. ' . print_r($this->db->error()));
                $this->db->trans_rollback();
            }

            log_message('debug', 'save_advance_payment. - Query = ' . $this->db->last_query());
            return $advance_payment_id;
        } finally {
            $this->load->model('system_journal_model');
            $this->system_journal_model->calculate_system_journal();
        }
    }
}
