<?php

class Payment_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    /*
     * Get payment using payment_id
     */

    public function get_payment_with_id($payment_id) {
        $query = $this->db->get_where('payment', array('payment_id' => $payment_id));
        return $query->row_array();
    }

    /**
     * 
     * @return type
     */
    public function get_all_payments_for_report(){
        $result = $this->db->query("SELECT 
                                        CONCAT('PAY', LPAD(payment_id, '5', '0')) AS payment_id,
                                        CONCAT('PUR', LPAD(payment_linked_purchase_id, '5', '0')) AS linked_purchase,
                                        payment_amount,
                                        payment_linked_company_display_name as vendor_name,
                                        payment_linked_company_billing_address as vendor_billing_address,
                                        payment_linked_company_billing_state_name as vendor_billing_state
                                    FROM
                                        payment
                                    LEFT JOIN
                                        employee ON employee.employee_id = payment.payment_record_created_by;");
        return $result->result_array();
    }
    /*
     * Retrieve all Credit payments
     */

    public function get_all_payments() {
        $result = $this->db->query('SELECT 
                                            *
                                    FROM
                                            payment
                                    LEFT JOIN
                                            employee ON employee.employee_id = payment.payment_record_created_by
                                    ORDER BY
                                        payment_id DESC;');
        return $result->result_array();
    }

    public function save_payment($payment_id, $payment) {
        try {
            log_message('debug', 'add_payment. - $payment = ' . print_r($payment, 1));
            date_default_timezone_set('Asia/Kolkata');  //PHP server time zone. This won't set mySQl time zone. Needs to be set separately.
            $this->load->model('purchase_model');
            $this->load->model('bank_model');

            //add other data

            if (isset($payment['payment_linked_purchase_id'])) {
                $purchase = $this->purchase_model->get_purchase_with_id($payment['payment_linked_purchase_id']);

                $payment['payment_linked_purchase_due_date'] = $purchase['purchase_due_date'];
                $payment['payment_oc_name'] = $purchase['purchase_oc_name'];
                $payment['payment_oc_address'] = $purchase['purchase_oc_address'];

                $payment['payment_oc_city_name'] = $purchase['purchase_oc_city_name'];
                $payment['payment_oc_gstin'] = $purchase['purchase_oc_gstin'];
                $payment['payment_oc_pan_number'] = $purchase['purchase_oc_pan_number'];
                $payment['payment_oc_logo_path'] = $purchase['purchase_oc_logo_path'];
                $payment['payment_oc_district'] = $purchase['purchase_oc_district'];
                $payment['payment_oc_state'] = $purchase['purchase_oc_state'];
                $payment['payment_oc_contact_number'] = $purchase['purchase_oc_contact_number'];
                $payment['payment_oc_gst_supply_state_id'] = $purchase['purchase_oc_gst_supply_state_id'];

                $payment['payment_linked_purchase_amount'] = $purchase['purchase_amount'];
                $payment['payment_linked_purchase_date'] = $purchase['purchase_date'];

                $payment['payment_linked_company_invoice_name'] = $purchase['purchase_linked_company_invoice_name'];
                $payment['payment_linked_company_display_name'] = $purchase['purchase_linked_company_display_name'];
                $payment['payment_linked_company_billing_address'] = $purchase['purchase_linked_company_billing_address'];
                //$payment['payment_linked_company_shipping_address'] = $purchase['purchase_linked_company_shipping_address'];
                $payment['payment_linked_company_gstin'] = $purchase['purchase_linked_company_gstin'];
                $payment['payment_linked_company_contact_number'] = $purchase['purchase_linked_company_contact_number'];

                $payment_linked_company_gst_supply_state_id = $purchase['purchase_linked_company_gst_supply_state_id'];
                $payment['payment_linked_company_gst_supply_state_id'] = $purchase['purchase_linked_company_gst_supply_state_id'];
                $payment['payment_place_of_supply'] = $purchase['purchase_place_of_supply'];

                $payment['payment_linked_company_billing_state_id'] = $purchase['purchase_linked_company_billing_state_id'];
                //$payment['payment_linked_company_shipping_state_id'] = $purchase['purchase_linked_company_shipping_state_id'];

                $payment['payment_linked_company_billing_state_name'] = $purchase['purchase_linked_company_billing_state_name'];
                //$payment['payment_linked_company_shipping_state_name'] = $purchase['purchase_linked_company_shipping_state_name'];

                $payment['payment_linked_company_payment_term_id'] = $purchase['purchase_linked_company_payment_term_id'];
                $payment['payment_linked_company_payment_term_text'] = $purchase['purchase_linked_company_payment_term_name'];
            }
            
            if(!empty($payment['payment_credit_coa_id'])){
                $this->load->model('chart_of_accounts_model');
                $payment['payment_credit_coa_name'] = $this->chart_of_accounts_model->get_coa_with_id($payment['payment_credit_coa_id'])['coa_account_name'];
            } else {
                $payment['payment_credit_coa_id'] = null;
            }

            /* add time component to date if payment date is today.
             * else
             * add 23:59:59 if the payment was created on a later date
             */
            if (date('Y-m-d', strtotime($payment['payment_date'])) == date("Y-m-d")) {
                $payment['payment_date'] = date("Y-m-d H:i:sa");
            } else {
                $payment['payment_date'] = date("Y-m-d 23:59:59", strtotime($payment['payment_date']));
            }
            
            if(empty($payment['payment_linked_company_id'])){
                $payment['payment_linked_company_id'] = NULL;
            }
            if (!empty($payment['payment_credit_bank_id'])) {
                $bank = $this->bank_model->get_bank_with_id($payment['payment_credit_bank_id']);
                $payment['payment_credit_bank_name'] = $bank['bank_name'];
                $payment['payment_credit_bank_account_number'] = $bank['bank_account_number'];
            } else {
                $payment['payment_credit_bank_id'] = NULL;
                $payment['payment_credit_bank_name'] = NULL;
                $payment['payment_credit_bank_account_number'] = NULL;
            } 
            
            $this->db->trans_begin();
            $this->db->query('SET time_zone = "+05:30";');
            if ($payment_id == null) {
                $this->db->insert('payment', $payment);
                $payment_id = $this->db->insert_id();
            } else {
                $this->db->where('payment_id', $payment_id);
                $this->db->update('payment', $payment);
            }

            if ($payment_id > 0) {
                $this->db->trans_commit();
            } else {
                log_message('debug', 'ROLLBACK. ' . print_r($this->db->error()));
                $this->db->trans_rollback();
            }

            log_message('debug', 'edit_purchase. - Query = ' . $this->db->last_query());
            return $payment_id;
        } finally {
            $this->load->model('system_journal_model');
            $this->system_journal_model->calculate_system_journal();
        }
    }

}
