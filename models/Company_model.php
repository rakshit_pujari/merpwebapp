<?php

class Company_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    function delete_company_by_id($id) {
        log_message('debug', 'deleteCompanyBy. - $id = ' . print_r($id, 1));

        $this->db->where('company_id', $id);
        $this->db->delete('company');

        log_message('debug', 'deleteCompanyBy. - Query = ' . $this->db->last_query());

        if ($this->db->affected_rows() == '1') {
            log_message('debug', 'deleteCompanyBy. - EMPLOYEE DELETED ');
            return TRUE;
        } else {
            log_message('debug', 'deleteCompanyBy. - FALIED TO DELETE EMPLOYEE ');
            return FALSE;
        }
    }

    public function get_company_count() {
        $query_string = 'SELECT COUNT(company_id) AS count from company';
        $result = $this->db->query($query_string);
        return $result->row_array();
    }

    /**
     * 
     * @return type
     */
    public function get_all_companies_for_report(){
        $query = 'SELECT 
                        CONCAT("CMP", LPAD(company_id, "5", "0")) AS company_id,
                        company_display_name as company_name, 
                        company_type,
                        company_billing_address,
                        company_billing_area,
                        billing_location.city_name AS billing_city_name,
                        billing_location.district AS billing_district,
                        billing_state.state_name as billing_state_name,
                        company_shipping_address,
                        company_shipping_area,
                        shipping_location.city_name AS shipping_city_name,
                        shipping_location.district AS shipping_district,
                        shipping_state.state_name as shipping_state_name,
                        company_contact_number,
                        company_pan_number,
                        company_gst_number
                    FROM
                        company
                    LEFT JOIN
                        location AS billing_location
                    ON
                        company.company_billing_location_id = billing_location.location_id
                    LEFT JOIN
                        location AS shipping_location
                    ON
                        company.company_shipping_location_id = shipping_location.location_id
                    LEFT JOIN
                        state as billing_state
                    ON
                        billing_location.state_id = billing_state.state_tin_number
                    LEFT JOIN
                        state as shipping_state
                    ON
                        shipping_location.state_id = shipping_state.state_tin_number;';


        $result = $this->db->query($query);
        return $result->result_array();
    }
    
    public function get_all_companies($company_type = null) {
        $query = 'SELECT 
                    *,
                    billing_location.city_name AS billing_city_name,
                    billing_location.district AS billing_district,
                    billing_location.state_id AS company_billing_state_id,
                    shipping_location.city_name AS shipping_city_name,
                    shipping_location.district AS shipping_district,
                    shipping_location.state_id AS company_shipping_state_id
                        FROM
                    company
                        LEFT JOIN
                    employee
                        ON
                    employee.employee_id = company.company_record_created_by
                        LEFT JOIN
                    location AS billing_location
                        ON
                    company.company_billing_location_id = billing_location.location_id
                        LEFT JOIN
                    location AS shipping_location
                        ON
                    company.company_shipping_location_id = shipping_location.location_id';

        if ($company_type != null) {
            $query .= ' WHERE company_type = "' . $company_type . '"';
        }

        $query .= ';';

        $result = $this->db->query($query);
        return $result->result_array();
    }

    public function get_company_with_id($id) {
        $query = $this->db->get_where('company', array('company_id' => $id));
        return $query->row_array();
    }

    /**
     * Save a company
     */
    public function save_company($data, $company_id = NULL) {
        log_message('debug', 'save_company. - $id = ' . print_r($company_id, 1) . '$data = ' . print_r($data, 1));
        
        if(empty($data['company_payment_term_id'])){
            $data['company_payment_term_id'] = NULL;
        }
        
        if(!empty($data['company_gst_number'])){
            $data['company_gst_number'] = strtoupper($data['company_gst_number']);
        }
        
        if($company_id == NULL){
            $this->db->query('SET time_zone = "+05:30";');
            $company_id = $this->db->insert('company', $data);
        } else {
            $this->db->where('company_id', $company_id);
            $this->db->update('company', $data);
        }
        
        $response['query'] = $this->db->last_query();
        log_message('debug', 'save_company. - response = ' . print_r($response, 1));
        return $response;
    } 
}
