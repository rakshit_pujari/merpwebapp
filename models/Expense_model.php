<?php

class Expense_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }
    
    public function get_all_expenses_for_report(){
        $result = $this->db->query("SELECT 
                                        CONCAT('EXP', LPAD(expense_id, '5', '0')) AS expense_id,
                                        expense_vendor_name as vendor_name,
                                        expense_date,
                                        expense_bill_reference,
                                        expense_amount,
                                        expense_narration as narration
                                    FROM
                                        expense
                                    LEFT JOIN
                                        employee ON employee.employee_id = expense.expense_record_created_by;");
        return $result->result_array();
    }

    /*
     * Delete Expense
     */

    function delete_expense_by_id($id) {
        try {
            log_message('debug', 'delete_expense_by_id. - $id = ' . print_r($id, 1));

            $this->db->where('expense_id', $id);
            $this->db->delete('expense');

            log_message('debug', 'delete_expense_by_id. - Query = ' . $this->db->last_query());

            if ($this->db->affected_rows() == '1') {
                log_message('debug', 'delete_expense_by_id. - DELETED ');
                return TRUE;
            } else {
                log_message('debug', 'delete_expense_by_id. - FALIED TO DELETE ');
                return FALSE;
            }
        } finally {
            $this->load->model('system_journal_model');
            $this->system_journal_model->calculate_system_journal();
        }
    }

    /*
     * Retrieve all expenses
     */

    public function get_all_expenses() {
        $result = $this->db->query('SELECT 
                                            *
                                    FROM
                                            expense
                                    LEFT JOIN
                                            employee ON employee.employee_id = expense.expense_record_created_by
                                    ORDER BY
                                        expense_id DESC;');
        return $result->result_array();
    }

    /*
     * Get expense using expense_id
     */

    public function get_expense_with_id($expense_id) {
        $query = $this->db->get_where('expense', array('expense_id' => $expense_id));
        return $query->row_array();
    }

    /**
     * Save new expense or edit existing expense
     * @param type $expense_id
     * @param type $expense
     * @return type
     */
    public function save_expense($expense_id, $expense) {
        try {
            log_message('debug', 'save_expense. - $expense = ' . print_r($expense, 1));

            $this->db->query('SET time_zone = "+05:30";');

            $expense_date = $expense['expense_date'];

            /* add time component to date if invoice date is today.
             * else
             * add 23:59:59 if the invoice was created on a later date
             * This timestamp is to determine entry sequence for FIFO inventory
             * To be done for Invoice, expense, credit note, debit note, inventory adjustment
             */
            if (date('Y-m-d', strtotime($expense['expense_date'])) == date("Y-m-d")) {
                $expense['expense_date'] = date("Y-m-d H:i:sa");
            } else {
                $expense['expense_date'] = date("Y-m-d 23:59:59", strtotime($expense_date));
            }
            
            if(empty($expense['expense_vendor_id'])){
                $expense['expense_vendor_id'] = NULL;
            }
            
            $this->db->trans_begin();

            $this->db->query('SET time_zone = "+05:30";');

            if ($expense_id == null) {
                $this->db->insert('expense', $expense);
                $expense_id = $this->db->insert_id();
            } else {
                $this->db->where('expense_id', $expense_id);
                $this->db->update('expense', $expense);
            }

            if ($expense_id > 0) {
                $this->db->trans_commit();
                $response['result'] = "success";
                log_message('debug', 'save_expense success.');
            } else {
                $response['result'] = $this->db->error();
                $response['query'] = $this->db->last_query();
                log_message('debug', 'save_expense. ROLLBACK. ' . print_r($response));
                $this->db->trans_rollback();
            }

            log_message('debug', 'save_expense. - Query = ' . $this->db->last_query());
            return $expense_id;
        } finally {
            $this->load->model('system_journal_model');
            $this->system_journal_model->calculate_system_journal();
        }
    }

}
