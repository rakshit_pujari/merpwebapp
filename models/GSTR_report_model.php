<?php

class GSTR_report_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }
    
    
     /**
     * GSTR1 1.4 b2b
     * @param type $start_date
     * @param type $end_date
     * @return type
     */
    public function get_gstr1_1_dot_4_b2b($month = NULL, $year = NULL) {

        if($month == NULL){
            $month = date("m");;
        }
        
        if($year == NULL){
            $year =  date("Y");
        }
        
        $last_date =  date('t',strtotime($month.'/1/'.$year));
        
        $start_date = $year.'-'.$month.'-01';
        $end_date = $year.'-'.$month.'-'.$last_date;
        
        $query = 'SELECT 
                    invoice_linked_company_gstin,
                    invoice_linked_company_invoice_name,
                    CONCAT("INV", LPAD(invoice_id, "5", "0")),
                    DATE(invoice_date),
                    invoice_amount,
                    CONCAT(invoice_linked_company_gst_supply_state_id,
                            "-",
                            invoice_place_of_supply),
                        IF(invoice_is_reverse_charge_applicable = 1, "Y", "N"),
                        /*ROUND(ipe_tax_percent_applicable, 2),*/ "",
                    "Regular",
                    "" as a,
                    "" as b,
                    ROUND(SUM(invoice_taxable_amount), 2)
                FROM
                    invoice_calc
                where 
                    ipe_tax_percent_applicable > 0 
                     AND invoice_linked_company_gstin IS NOT NULL AND invoice_linked_company_gstin != ""
                    AND invoice_date between "'.$start_date.'" AND "'.$end_date.'"
                group by
                    ipe_linked_invoice_id, ipe_tax_id_applicable
				order by
					invoice_id ASC, ipe_tax_percent_applicable ASC';

        $result = $this->db->query($query)->result_array();
        return $result;
    }

    
    /**
     * GSTR1 1.4 b2b
     * @param type $start_date
     * @param type $end_date
     * @return type
     */
    public function get_gstr1_1_dot_4_b2cl($month = NULL, $year = NULL) {

        if($month == NULL){
            $month = date("m");;
        }
        
        if($year == NULL){
            $year =  date("Y");
        }
        
        $last_date =  date('t',strtotime($month.'/1/'.$year));
        
        $start_date = $year.'-'.$month.'-01';
        $end_date = $year.'-'.$month.'-'.$last_date;
        
        $query = 'SELECT 
                    CONCAT("INV", LPAD(invoice_id, "5", "0")),
                    DATE(invoice_date),
                    invoice_amount,
                    CONCAT(invoice_linked_company_gst_supply_state_id,
                            "-",
                            invoice_place_of_supply),
                    ipe_tax_percent_applicable,
                    TRUNCATE(SUM(invoice_taxable_amount), 2),
                            "",
                            ""
                FROM
                    invoice_calc
                where 
                        invoice_amount > 250000 
                        AND ipe_tax_percent_applicable > 0 
                        AND invoice_oc_gst_supply_state_id!=invoice_linked_company_gst_supply_state_id
                        AND (invoice_linked_company_gstin IS NULL
                            OR invoice_linked_company_gstin = "")
                    AND invoice_date between "'.$start_date.'" AND "'.$end_date.'"
                group by
                    ipe_tax_id_applicable, ipe_linked_invoice_id
				order by
					invoice_id ASC';

        $result = $this->db->query($query)->result_array();
        return $result;
    }
    
    /**
     * GSTR1 1.4 b2b
     * @param type $start_date
     * @param type $end_date
     * @return type
     */
    public function get_gstr1_1_dot_4_b2cs($month = NULL, $year = NULL) {

        if($month == NULL){
            $month = date("m");;
        }
        
        if($year == NULL){
            $year =  date("Y");
        }
        
        $last_date =  date('t',strtotime($month.'/1/'.$year));
        
        $start_date = $year.'-'.$month.'-01';
        $end_date = $year.'-'.$month.'-'.$last_date;
        
        $query = 'SELECT 
                    "OE",
                    CONCAT(invoice_linked_company_gst_supply_state_id,
                            "-",
                            invoice_place_of_supply),
                    ipe_tax_percent_applicable,
                    TRUNCATE(SUM(invoice_taxable_amount - IFNULL((Select SUM(cn_taxable_amount) from credit_note_calc where cne_linked_ipe_id = ipe_id), 0) + IFNULL((Select SUM(dn_taxable_amount) from debit_note_calc where dne_linked_ipe_id = ipe_id), 0)), 2),
                    "",
                    ""
                FROM
                    invoice_calc
                WHERE
                    ((invoice_amount <= 250000
                        AND invoice_oc_gst_supply_state_id != invoice_linked_company_gst_supply_state_id)
                        OR invoice_oc_gst_supply_state_id = invoice_linked_company_gst_supply_state_id)
                        AND
                        (invoice_linked_company_gstin IS NULL
                            OR invoice_linked_company_gstin = "")
                    AND (invoice_linked_company_gstin IS NULL
                    OR invoice_linked_company_gstin = "")
                    AND invoice_date between "'.$start_date.'" AND "'.$end_date.'"
                GROUP BY invoice_linked_company_gst_supply_state_id, ipe_tax_id_applicable
                    ORDER BY invoice_linked_company_gst_supply_state_id ASC, ipe_tax_id_applicable ASC';

        $result = $this->db->query($query)->result_array();
        return $result;
    }
    
    
    /**
     * GSTR1 1.4 cdnr
     * @param type $start_date
     * @param type $end_date
     * @return type
     */
    public function get_gstr1_1_dot_4_cdnr($month = NULL, $year = NULL) {

        if($month == NULL){
            $month = date("m");;
        }
        
        if($year == NULL){
            $year =  date("Y");
        }
        
        $last_date =  date('t',strtotime($month.'/1/'.$year));
        
        $start_date = $year.'-'.$month.'-01';
        $end_date = $year.'-'.$month.'-'.$last_date;
        
        $query = 'SELECT 
                    cn_linked_company_gstin,
                    cn_linked_company_invoice_name,
                    CONCAT("INV", LPAD(cn_linked_invoice_id, "5", "0")),
                    DATE(cn_linked_invoice_date),
                    CONCAT("CDN", LPAD(cn_id, "5", "0")),
                    DATE(cn_date) as document_date,
                    "C",
                    CONCAT(cn_sir_text),
                    CONCAT(cn_linked_company_gst_supply_state_id,
                            "-",
                            cn_place_of_supply),
                    SUM(cn_amount),
                    cne_tax_percent_applicable,
                    ROUND(SUM(cn_taxable_amount), 2),
					"",
                    "N"
                FROM
                    credit_note_calc
				where 
                    cn_linked_company_gstin IS NOT NULL AND cn_linked_company_gstin != ""
                    AND cn_linked_invoice_id IS NOT NULL
                    AND cn_date BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                group by
                    cne_tax_id_applicable, cne_linked_credit_note_id
				UNION ALL
                SELECT 
                    dn_linked_company_gstin,
                    dn_linked_company_invoice_name,
                    CONCAT("INV", LPAD(dn_linked_invoice_id, "5", "0")),
                    DATE(dn_linked_invoice_date),
                    CONCAT("DBN", LPAD(dn_id, "5", "0")),
                    DATE(dn_date) as document_date,
                    "D",
                    CONCAT(dn_sir_text),
                    CONCAT(dn_linked_company_gst_supply_state_id,
                            "-",
                            dn_place_of_supply),
                    SUM(dn_amount),
                    dne_tax_percent_applicable,
                    ROUND(SUM(dn_taxable_amount), 2),
					"",
                    "N"
                FROM
                    debit_note_calc
				where 
                    dn_linked_company_gstin IS NOT NULL AND dn_linked_company_gstin != ""
                    AND dn_linked_invoice_id IS NOT NULL
                    AND dn_date BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                group by
                    dne_tax_id_applicable, dne_linked_debit_note_id
                        order by
                    document_date ASC';

        $result = $this->db->query($query)->result_array();
        return $result;
    }
    
      /**
     * GSTR1 1.4 cdnur
     * @param type $start_date
     * @param type $end_date
     * @return type
     */
    public function get_gstr1_1_dot_4_cdnur($month = NULL, $year = NULL) {

        if($month == NULL){
            $month = date("m");;
        }
        
        if($year == NULL){
            $year =  date("Y");
        }
        
        $last_date =  date('t',strtotime($month.'/1/'.$year));
        
        $start_date = $year.'-'.$month.'-01';
        $end_date = $year.'-'.$month.'-'.$last_date;
        
        $query = 'SELECT 
                    "B2CL",
                    CONCAT("CDN", LPAD(cn_id, "5", "0")),
                    DATE(cn_date) as document_date,
                    "C",
                    CONCAT("INV", LPAD(cn_linked_invoice_id, "5", "0")),
                    DATE(cn_linked_invoice_date),
                    CONCAT(cn_sir_text),
                    CONCAT(cn_linked_company_gst_supply_state_id,
                            "-",
                            cn_place_of_supply),
                    SUM(cn_amount),
                    cne_tax_percent_applicable,
                    SUM(cn_taxable_amount),
					"",
                    "N"
                FROM
                    credit_note_calc
				where 
                    cn_linked_company_gstin IS NULL AND cn_linked_company_gstin = ""
                    AND cn_oc_gst_supply_state_id != cn_linked_company_gst_supply_state_id
                    AND cn_linked_invoice_id IS NOT NULL
                    AND cn_amount > 250000
                    AND cn_date BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                group by
                    cne_tax_id_applicable, cne_linked_credit_note_id
				UNION ALL
                SELECT 
                    "B2CL",
                    CONCAT("DBN", LPAD(dn_id, "5", "0")),
                    DATE(dn_date) as document_date,
                    "D",
                    CONCAT("INV", LPAD(dn_linked_invoice_id, "5", "0")),
                    DATE(dn_linked_invoice_date),
                    CONCAT(dn_sir_text),
                    CONCAT(dn_linked_company_gst_supply_state_id,
                            "-",
                            dn_place_of_supply),
                    SUM(dn_amount),
                    dne_tax_percent_applicable,
                    SUM(dn_taxable_amount),
                    "",
                    "N"
                FROM
                    debit_note_calc
				where 
                    (dn_linked_company_gstin IS NULL OR dn_linked_company_gstin = "")
                    AND dn_oc_gst_supply_state_id != dn_linked_company_gst_supply_state_id
                    AND dn_linked_invoice_id IS NOT NULL
                    AND dn_amount > 250000
                    AND dn_date BETWEEN "'.$start_date.'" AND "'.$end_date.'"
                group by
                    dne_tax_id_applicable, dne_linked_debit_note_id
                    	order by
                    document_date ASC';

        $result = $this->db->query($query)->result_array();
        return $result;
    }
    
    /**
     * GSTR1 1.4 exemp
     * @param type $start_date
     * @param type $end_date
     * @return type
     */
    public function get_gstr1_1_dot_4_exemp($month = NULL, $year = NULL) {

        if($month == NULL){
            $month = date("m");
        }
        
        if($year == NULL){
            $year =  date("Y");
        }
        
        $last_date =  date('t',strtotime($month.'/1/'.$year));
        
        $start_date = $year.'-'.$month.'-01';
        $end_date = $year.'-'.$month.'-'.$last_date;
        
        //no need to consider exclusive/inclusive since tax% is 0.. hence both values will be same
        $query_invoice_row1 = 'SELECT 
                        SUM(invoice_taxable_amount) as amount
                    FROM
                        invoice_calc
                    WHERE
                        invoice_linked_company_gst_supply_state_id != invoice_oc_gst_supply_state_id
                            AND invoice_linked_company_gstin IS NOT NULL
                            AND invoice_linked_company_gstin != ""
                            AND ipe_tax_percent_applicable = 0
                            AND invoice_date between "'.$start_date.'" AND "'.$end_date.'"';
       
        
        $query_cn_row1 = 'SELECT 
                        SUM(cn_taxable_amount) as amount
                    FROM
                        credit_note_calc
                    WHERE
                        cn_linked_company_gst_supply_state_id != cn_oc_gst_supply_state_id
                            AND cn_linked_company_gstin IS NOT NULL
                            AND cn_linked_company_gstin != ""
                            AND cne_tax_percent_applicable = 0
                            AND cn_date between "'.$start_date.'" AND "'.$end_date.'"';
        
        $query_dn_row1 = 'SELECT 
                        SUM(dn_taxable_amount) as amount
                    FROM
                        debit_note_calc
                    WHERE
                        dn_linked_company_gst_supply_state_id != dn_oc_gst_supply_state_id
                            AND dn_linked_company_gstin IS NOT NULL
                            AND dn_linked_company_gstin != ""
                            AND dne_tax_percent_applicable = 0
                            AND dn_date between "'.$start_date.'" AND "'.$end_date.'"';
        
        $query_invoice_row2 = 'SELECT 
                        SUM(invoice_taxable_amount) as amount
                    FROM
                        invoice_calc
                    WHERE
                        invoice_linked_company_gst_supply_state_id = invoice_oc_gst_supply_state_id
                            AND invoice_linked_company_gstin IS NOT NULL
                            AND invoice_linked_company_gstin != ""
                            AND ipe_tax_percent_applicable = 0
                            AND invoice_date between "'.$start_date.'" AND "'.$end_date.'"';
        
        $query_cn_row2 = 'SELECT 
                        SUM(cn_taxable_amount) as amount
                    FROM
                        credit_note_calc
                    WHERE
                        cn_linked_company_gst_supply_state_id = cn_oc_gst_supply_state_id
                            AND cn_linked_company_gstin IS NOT NULL
                            AND cn_linked_company_gstin != ""
                            AND cne_tax_percent_applicable = 0
                            AND cn_date between "'.$start_date.'" AND "'.$end_date.'"';
        
        $query_dn_row2 = 'SELECT 
                        SUM(dn_taxable_amount) as amount
                    FROM
                        debit_note_calc
                    WHERE
                        dn_linked_company_gst_supply_state_id = dn_oc_gst_supply_state_id
                            AND dn_linked_company_gstin IS NOT NULL
                            AND dn_linked_company_gstin != ""
                            AND dne_tax_percent_applicable = 0
                            AND dn_date between "'.$start_date.'" AND "'.$end_date.'"';
        
        $query_invoice_row3 = 'SELECT 
                        SUM(invoice_taxable_amount) as amount
                    FROM
                        invoice_calc
                    WHERE
                        invoice_linked_company_gst_supply_state_id != invoice_oc_gst_supply_state_id
                            AND (invoice_linked_company_gstin IS NULL
                            OR invoice_linked_company_gstin = "")
                            AND ipe_tax_percent_applicable = 0
                            AND invoice_date between "'.$start_date.'" AND "'.$end_date.'"';
        
        $query_cn_row3 = 'SELECT 
                        SUM(cn_taxable_amount) as amount
                    FROM
                        credit_note_calc
                    WHERE
                        cn_linked_company_gst_supply_state_id != cn_oc_gst_supply_state_id
                            AND (cn_linked_company_gstin IS NULL
                            OR cn_linked_company_gstin = "")
                            AND cne_tax_percent_applicable = 0
                            AND cn_date between "'.$start_date.'" AND "'.$end_date.'"';
        
        $query_dn_row3 = 'SELECT 
                        SUM(dn_taxable_amount) as amount
                    FROM
                        debit_note_calc
                    WHERE
                        dn_linked_company_gst_supply_state_id != dn_oc_gst_supply_state_id
                            AND (dn_linked_company_gstin IS NULL
                            OR dn_linked_company_gstin = "")
                            AND dne_tax_percent_applicable = 0
                            AND dn_date between "'.$start_date.'" AND "'.$end_date.'"';
        
        $query_invoice_row4 = 'SELECT 
                        SUM(invoice_taxable_amount) as amount
                    FROM
                        invoice_calc
                    WHERE
                        invoice_linked_company_gst_supply_state_id = invoice_oc_gst_supply_state_id
                            AND (invoice_linked_company_gstin IS NULL
                            OR invoice_linked_company_gstin = "")
                            AND ipe_tax_percent_applicable = 0
                            AND invoice_date between "'.$start_date.'" AND "'.$end_date.'"';
        
        $query_cn_row4 = 'SELECT 
                        SUM(cn_taxable_amount) as amount
                    FROM
                        credit_note_calc
                    WHERE
                        cn_linked_company_gst_supply_state_id = cn_oc_gst_supply_state_id
                            AND (cn_linked_company_gstin IS NULL
                            OR cn_linked_company_gstin = "")
                            AND cne_tax_percent_applicable = 0
                            AND cn_date between "'.$start_date.'" AND "'.$end_date.'"';
        
        $query_dn_row4 = 'SELECT 
                        SUM(dn_taxable_amount) as amount
                    FROM
                        debit_note_calc
                    WHERE
                        dn_linked_company_gst_supply_state_id = dn_oc_gst_supply_state_id
                            AND (dn_linked_company_gstin IS NULL
                            OR dn_linked_company_gstin = "")
                            AND dne_tax_percent_applicable = 0
                            AND dn_date between "'.$start_date.'" AND "'.$end_date.'"';

        $result[0] = array($this->db->query($query_invoice_row1)->row_array()['amount'] - $this->db->query($query_cn_row1)->row_array()['amount'] + $this->db->query($query_dn_row1)->row_array()['amount'], "", "");
        $result[1] = array($this->db->query($query_invoice_row2)->row_array()['amount'] - $this->db->query($query_cn_row2)->row_array()['amount'] + $this->db->query($query_dn_row2)->row_array()['amount'], "", "");
        $result[2] = array($this->db->query($query_invoice_row3)->row_array()['amount'] - $this->db->query($query_cn_row3)->row_array()['amount'] + $this->db->query($query_dn_row3)->row_array()['amount'], "", "");
        $result[3] = array($this->db->query($query_invoice_row4)->row_array()['amount'] - $this->db->query($query_cn_row4)->row_array()['amount'] + $this->db->query($query_dn_row4)->row_array()['amount'], "", "");
        
        return $result;
    }


    /**
     * GSTR1 1.4 hsn
     * @param type $start_date
     * @param type $end_date
     * @return type
     */
    public function get_gstr1_1_dot_4_hsn($month = NULL, $year = NULL) {

        if($month == NULL){
            $month = date("m");;
        }
        
        if($year == NULL){
            $year =  date("Y");
        }
        
        $last_date =  date('t',strtotime($month.'/1/'.$year));
        
        $start_date = $year.'-'.$month.'-01';
        $end_date = $year.'-'.$month.'-'.$last_date;
        
        $query_invoice = 'SELECT 
                    *,
                    invoice_taxable_amount,
                    invoice_tax_amount
                    FROM
                invoice_calc
                    WHERE
                invoice_date between "'.$start_date.'" AND "'.$end_date.'"';
        
        

        $invoice_entries = $this->db->query($query_invoice)->result_array();
        
        $result = array();
        foreach ($invoice_entries as $invoice_entry) {
            if(!isset($result[$invoice_entry['ipe_linked_product_id']])){
                $product_entry = array();
                $product_entry[0] = $invoice_entry['ipe_product_hsn_code'];
                $product_entry[1] = $invoice_entry['ipe_product_name'];
                $product_entry[2] = "";
                $product_entry[3] = 0;
                $product_entry[4] = 0;
                $product_entry[5] = 0;
                $product_entry[6] = 0;
                $product_entry[7] = 0;
                $product_entry[8] = 0;
                $product_entry[9] = 0;
                
                $result[$invoice_entry['ipe_linked_product_id']] = $product_entry;
            }
            
            $product_entry = $result[$invoice_entry['ipe_linked_product_id']];
            
            if(strpos($product_entry[1], $invoice_entry['ipe_product_name']) !== false){
                
            } else {
                $product_entry[1] = $product_entry[1].' ,'. $invoice_entry['ipe_product_name']; //quantity
            }
            
            $product_entry[3] = $product_entry[3] + $invoice_entry['ipe_product_quantity']; //quantity
            $product_entry[4] = $product_entry[4] + ($invoice_entry['invoice_taxable_amount'] + $invoice_entry['invoice_tax_amount']); //total_value
            $product_entry[5] = $product_entry[5] + $invoice_entry['invoice_taxable_amount'];    //invoice_taxable_amount
            if($invoice_entry['invoice_linked_company_gst_supply_state_id'] != $invoice_entry['invoice_oc_gst_supply_state_id']){
                $product_entry[6] = $product_entry[6] + $invoice_entry['invoice_tax_amount'];        //invoice_tax_amount
            } else {
                $product_entry[7] = $product_entry[7] + $invoice_entry['invoice_tax_amount'] / 2;
                $product_entry[8] = $product_entry[8] + $invoice_entry['invoice_tax_amount'] / 2;
            }
            $product_entry[9] = $product_entry[9] + 0;
            
            $result[$invoice_entry['ipe_linked_product_id']] = $product_entry;
        }
        
        $query_credit_note = 'SELECT 
                    *,
                    cn_taxable_amount,
                    cn_tax_amount
                        FROM
                credit_note_calc
                    WHERE
                cn_date between "'.$start_date.'" AND "'.$end_date.'"';
        
        

        $cn_entries = $this->db->query($query_credit_note)->result_array();
        
        foreach ($cn_entries as $cn_entry) {
            if(!isset($result[$cn_entry['cne_linked_product_id']])){
                //throw new Exception("HSN Code note found");
                throw new Exception("Product ID for credit note found");
            }
            
            $product_entry = $result[$cn_entry['cne_linked_product_id']];
            
            if(strpos($product_entry[1], $cn_entry['cne_product_name']) !== false){
                
            } else {
                throw New Exception("Product Name not found");
                //$product_entry[1] = $product_entry[1].' ,'. $cn_entry['cne_product_name']; //quantity
            }
            
            $product_entry[3] = $product_entry[3] - $cn_entry['cne_product_quantity']; //quantity
            $product_entry[4] = $product_entry[4] - ($cn_entry['cn_taxable_amount'] + $cn_entry['cn_tax_amount']); //total_value
            $product_entry[5] = $product_entry[5] - $cn_entry['cn_taxable_amount'];    //invoice_taxable_amount
            if($cn_entry['cn_linked_company_gst_supply_state_id'] != $cn_entry['cn_oc_gst_supply_state_id']){
                $product_entry[6] = $product_entry[6] - $cn_entry['cn_tax_amount'];        //invoice_tax_amount
            } else {
                $product_entry[7] = $product_entry[7] - $cn_entry['cn_tax_amount'] / 2;
                $product_entry[8] = $product_entry[8] - $cn_entry['cn_tax_amount'] / 2;
            }
            $product_entry[9] = $product_entry[9] + 0;
            
            $result[$cn_entry['cne_linked_product_id']] = $product_entry;
        }
        
        $query_debit_note = 'SELECT 
                    *
                        FROM
                debit_note_calc
                    WHERE
                dn_date between "'.$start_date.'" AND "'.$end_date.'"';
        
        

        $dn_entries = $this->db->query($query_debit_note)->result_array();
        
        foreach ($dn_entries as $dn_entry) {
            if(!isset($result[$dn_entry['dne_linked_product_id']])){
                //throw new Exception("HSN Code note found");
                throw new Exception("Product ID for debit note found". - $dn_entry['dne_linked_product_id']);
            }
            
            $product_entry = $result[$dn_entry['dne_linked_product_id']];
            
            if(strpos($product_entry[1], $dn_entry['dne_product_name']) !== false){
                
            } else {
                throw New Exception("Product Name not found");
                //$product_entry[1] = $product_entry[1].' ,'. $dn_entry['dne_product_name']; //quantity
            }
            
            $product_entry[3] = $product_entry[3] + $dn_entry['dne_product_quantity']; //quantity
            $product_entry[4] = $product_entry[4] + ($dn_entry['dn_taxable_amount'] + $dn_entry['dn_tax_amount']); //total_value
            $product_entry[5] = $product_entry[5] + $dn_entry['dn_taxable_amount'];    //invoice_taxable_amount
            if($dn_entry['dn_linked_company_gst_supply_state_id'] != $dn_entry['dn_oc_gst_supply_state_id']){
                $product_entry[6] = $product_entry[6] + $dn_entry['dn_tax_amount'];        //invoice_tax_amount
            } else {
                $product_entry[7] = $product_entry[7] + $dn_entry['dn_tax_amount'] / 2;
                $product_entry[8] = $product_entry[8] + $dn_entry['dn_tax_amount'] / 2;
            }
            $product_entry[9] = $product_entry[9] + 0;
            
            $result[$dn_entry['dne_linked_product_id']] = $product_entry;
        }
        
        foreach ($result as $key => $item) {
            $item[4] = number_format($item[4], 2);
            $item[5] = number_format($item[5], 2);
            $item[6] = number_format($item[6], 2);
            $item[7] = number_format($item[7], 2);
            $item[8] = number_format($item[8], 2);
            
            $result[$key] = $item;
        }
        
        return $result;
    }
    
        /**
     * GSTR1 1.4 doc
     * @param type $start_date
     * @param type $end_date
     * @return type
     */
    public function get_gstr1_1_dot_4_doc($month = NULL, $year = NULL) {

        if($month == NULL){
            $month = date("m");
        }
        
        if($year == NULL){
            $year =  date("Y");
        }
        
        $last_date =  date('t',strtotime($month.'/1/'.$year));
        
        $start_date = $year.'-'.$month.'-01';
        $end_date = $year.'-'.$month.'-'.$last_date;
        
        $query_row1 = 'SELECT 
                        "Invoices for outward supply", CONCAT("INV", LPAD(min(invoice_id), 5, 0)), CONCAT("INV", LPAD(max(invoice_id), 5, 0)), count(invoice_id), 0
                    FROM
                        invoice
                    WHERE
                         invoice_date between "'.$start_date.'" AND "'.$end_date.'"';
        
        //purchase from unregistered companies
        $query_row2 = 'SELECT 
                        "Invoices for inward supply from unregistered person", CONCAT("PUR", LPAD(min(purchase_id), 5, 0)), CONCAT("PUR", LPAD(max(purchase_id), 5, 0)), count(purchase_id), 0
                    FROM
                        purchase
                    WHERE
                        purchase_date between "'.$start_date.'" AND "'.$end_date.'"'
                . ' AND (purchase_linked_company_gstin IS NULL '
                . 'OR purchase_linked_company_gstin = "" )'; 
        
        $query_row3 = 'SELECT 
                        "Credit Note", CONCAT("CDN", LPAD(min(cn_id), 5, 0)), CONCAT("CDN", LPAD(max(cn_id), 5, 0)), count(cn_id), 0
                    FROM
                        credit_note
                    WHERE
                         cn_date between "'.$start_date.'" AND "'.$end_date.'"';
        
        /*$query_row4 = 'SELECT 
                        CONCAT("DBN", LPAD(min(dn_id), 5, 0)), CONCAT("DBN", LPAD(max(dn_id), 5, 0)), count(dn_id), 0
                    FROM
                        debit_note
                    WHERE
                        dn_date between "'.$start_date.'" AND "'.$end_date.'"';*/
        
        $query_row5 = 'SELECT 
                        "Receipt Voucher", CONCAT("REC", LPAD(min(receipt_id), 5, 0)), CONCAT("REC", LPAD(max(receipt_id), 5, 0)), count(receipt_id), 0
                    FROM
                        receipt
                    WHERE
                        receipt_date between "'.$start_date.'" AND "'.$end_date.'"';
        
        $query_row6 = 'SELECT 
                        "Receipt Voucher", CONCAT("ADR", LPAD(min(advance_receipt_id), 5, 0)), CONCAT("ADR", LPAD(max(advance_receipt_id), 5, 0)), count(advance_receipt_id), 0
                    FROM
                        advance_receipt
                    WHERE
                        advance_receipt_date between "'.$start_date.'" AND "'.$end_date.'"';
        
        $query_row7 = 'SELECT 
                        "Payment Voucher", CONCAT("PAY", LPAD(min(payment_id), 5, 0)), CONCAT("PAY", LPAD(max(payment_id), 5, 0)), count(payment_id), 0
                    FROM
                        payment
                    WHERE
                        payment_date between "'.$start_date.'" AND "'.$end_date.'"';
        
        $query_row8 = 'SELECT 
                        "Payment Voucher", CONCAT("ADP", LPAD(min(advance_payment_id), 5, 0)), CONCAT("ADP", LPAD(max(advance_payment_id), 5, 0)), count(advance_payment_id), 0
                    FROM
                        advance_payment
                    WHERE
                        advance_payment_date between "'.$start_date.'" AND "'.$end_date.'"';

        $result[0] = $this->db->query($query_row1)->row_array();
        $result[1] = $this->db->query($query_row2)->row_array();
        $result[2] = $this->db->query($query_row3)->row_array();
        //$result[3] = $this->db->query($query_row4)->row_array();
        $result[3] = $this->db->query($query_row5)->row_array();
        $result[4] = $this->db->query($query_row6)->row_array();
        $result[5] = $this->db->query($query_row7)->row_array();
        $result[6] = $this->db->query($query_row8)->row_array();
        
        return $result;
    }
    
      /**
     * GSTR2 1.1 b2b
     * @param type $start_date
     * @param type $end_date
     * @return type
     */
    public function get_gstr2_1_dot_1_b2b($month = NULL, $year = NULL) {

        if($month == NULL){
            $month = date("m");;
        }
        
        if($year == NULL){
            $year =  date("Y");
        }
        
        $last_date =  date('t',strtotime($month.'/1/'.$year));
        
        $start_date = $year.'-'.$month.'-01';
        $end_date = $year.'-'.$month.'-'.$last_date;
      
        $query = 'SELECT 
                    purchase_linked_company_gstin,
                    purchase_bill_reference,
                    DATE(purchase_bill_date),                 
                    purchase_amount,
                    CONCAT(purchase_linked_company_gst_supply_state_id,
                            "-",
                            purchase_place_of_supply),
                        IF(purchase_is_reverse_charge_applicable = 1, "Y", "N"),
                    "Regular",    
                    ROUND(pe_tax_percent_applicable, 2),                    
                    ROUND(SUM(purchase_taxable_amount), 2),
                    if(purchase_oc_gst_supply_state_id = purchase_linked_company_gst_supply_state_id, ROUND(SUM(purchase_tax_amount), 2), 0) as igst,
                    if(purchase_oc_gst_supply_state_id != purchase_linked_company_gst_supply_state_id, ROUND(SUM(purchase_tax_amount/2), 2), 0) as cgst,
                    if(purchase_oc_gst_supply_state_id != purchase_linked_company_gst_supply_state_id, ROUND(SUM(purchase_tax_amount/2), 2), 0) as sgst,
                    "0",
                    if(purchase_is_input_tax_credit_available = 1, "Inputs", "Ineligible"),
                    if(purchase_is_input_tax_credit_available = 1, if(purchase_oc_gst_supply_state_id = purchase_linked_company_gst_supply_state_id, ROUND(SUM(purchase_tax_amount), 2), 0), 0) as availed_itc_igst,
                    if(purchase_is_input_tax_credit_available = 1, if(purchase_oc_gst_supply_state_id != purchase_linked_company_gst_supply_state_id, ROUND(SUM(purchase_tax_amount/2), 2), 0), 0) as availed_itc_cgst,
                    if(purchase_is_input_tax_credit_available = 1, if(purchase_oc_gst_supply_state_id != purchase_linked_company_gst_supply_state_id, ROUND(SUM(purchase_tax_amount/2), 2), 0), 0) as availed_itc_sgst,
                    0  as availed_itc_cess
                FROM
                    purchase_calc
                where 
                    pe_tax_percent_applicable > 0 
                     AND purchase_linked_company_gstin IS NOT NULL AND purchase_linked_company_gstin != ""
                    AND purchase_date between "'.$start_date.'" AND "'.$end_date.'"
                group by
                    pe_linked_purchase_id, pe_tax_id_applicable
				order by
					purchase_id ASC, pe_tax_percent_applicable ASC';

        $result = $this->db->query($query)->result_array();
        return $result;
    }

    
          /**
     * GSTR2 1.1 b2b
     * @param type $start_date
     * @param type $end_date
     * @return type
     */
    public function get_gstr2_1_dot_1_b2bur($month = NULL, $year = NULL) {

        if($month == NULL){
            $month = date("m");;
        }
        
        if($year == NULL){
            $year =  date("Y");
        }
        
        $last_date =  date('t',strtotime($month.'/1/'.$year));
        
        $start_date = $year.'-'.$month.'-01';
        $end_date = $year.'-'.$month.'-'.$last_date;
      
        $query = 'SELECT 
                    purchase_linked_company_invoice_name,
                    CONCAT("PUR", LPAD(purchase_id, "5", "0")),
                    DATE(purchase_bill_date),                 
                    purchase_amount,
                    CONCAT(purchase_linked_company_gst_supply_state_id,
                            "-",
                            purchase_place_of_supply),
					if(purchase_oc_gst_supply_state_id = purchase_linked_company_gst_supply_state_id, "Intra State", "Inter State") as supply_type,
                    ROUND(pe_tax_percent_applicable, 2),                    
                    ROUND(SUM(purchase_taxable_amount), 2),
                    if(purchase_oc_gst_supply_state_id != purchase_linked_company_gst_supply_state_id, ROUND(SUM(purchase_tax_amount), 2), 0) as igst,
                    if(purchase_oc_gst_supply_state_id = purchase_linked_company_gst_supply_state_id, ROUND(SUM(purchase_tax_amount/2), 2), 0) as cgst,
                    if(purchase_oc_gst_supply_state_id = purchase_linked_company_gst_supply_state_id, ROUND(SUM(purchase_tax_amount/2), 2), 0) as sgst,
                    "0",
                    if(purchase_is_input_tax_credit_available = 1, "Inputs", "Ineligible"),
                    if(purchase_is_input_tax_credit_available = 1, if(purchase_oc_gst_supply_state_id != purchase_linked_company_gst_supply_state_id, ROUND(SUM(purchase_tax_amount), 2), 0), 0) as availed_itc_igst,
                    if(purchase_is_input_tax_credit_available = 1, if(purchase_oc_gst_supply_state_id = purchase_linked_company_gst_supply_state_id, ROUND(SUM(purchase_tax_amount/2), 2), 0), 0) as availed_itc_cgst,
                    if(purchase_is_input_tax_credit_available = 1, if(purchase_oc_gst_supply_state_id = purchase_linked_company_gst_supply_state_id, ROUND(SUM(purchase_tax_amount/2), 2), 0), 0) as availed_itc_sgst,
                    0 as availed_itc_cess
                FROM
                    purchase_calc
                where 
                    pe_tax_percent_applicable > 0 
                    AND (purchase_linked_company_gstin IS NULL OR purchase_linked_company_gstin = "")
                    AND purchase_date between "'.$start_date.'" AND "'.$end_date.'"
                group by
                    pe_linked_purchase_id, pe_tax_id_applicable
                order by
                    purchase_id ASC, pe_tax_percent_applicable ASC';

        $result = $this->db->query($query)->result_array();
        return $result;
    }

    /**
     * GSTR2 1.1 b2b
     * @param type $start_date
     * @param type $end_date
     * @return type
     */
    public function get_gstr2_1_dot_1_cdnr($month = NULL, $year = NULL) {

        if($month == NULL){
            $month = date("m");;
        }
        
        if($year == NULL){
            $year =  date("Y");
        }
        
        $last_date =  date('t',strtotime($month.'/1/'.$year));
        
        $start_date = $year.'-'.$month.'-01';
        $end_date = $year.'-'.$month.'-'.$last_date;
      
        $query = 'SELECT 
                    dn_linked_company_gstin,
                    dn_vendor_credit_note_number,
                    DATE(dn_vendor_credit_note_date),   
                    purchase_bill_reference,
                    DATE(purchase_bill_date),  
                    "N",
                    "C",    
                    dn_sir_text,    
                    if(dn_oc_gst_supply_state_id = dn_linked_company_gst_supply_state_id, "Intra State", "Inter State") as supply_type,
                    ROUND(SUM(dn_post_tax_amount), 2),
                    dne_tax_percent_applicable,
                    dn_taxable_amount,
                    if(dn_oc_gst_supply_state_id = dn_linked_company_gst_supply_state_id, ROUND(SUM(dn_tax_amount), 2), 0) as igst,
                    if(dn_oc_gst_supply_state_id != dn_linked_company_gst_supply_state_id, ROUND(SUM(dn_tax_amount/2), 2), 0) as cgst,
                    if(dn_oc_gst_supply_state_id != dn_linked_company_gst_supply_state_id, ROUND(SUM(dn_tax_amount/2), 2), 0) as sgst,
                    "0",
                    if(purchase_is_input_tax_credit_available = 1, "Inputs", "Ineligible"),
                    if(purchase_is_input_tax_credit_available = 1, if(dn_oc_gst_supply_state_id = dn_linked_company_gst_supply_state_id, ROUND(SUM(dn_tax_amount), 2), 0), 0) as availed_itc_igst,
                    if(purchase_is_input_tax_credit_available = 1, if(dn_oc_gst_supply_state_id != dn_linked_company_gst_supply_state_id, ROUND(SUM(dn_tax_amount/2), 2), 0), 0) as availed_itc_cgst,
                    if(purchase_is_input_tax_credit_available = 1, if(dn_oc_gst_supply_state_id != dn_linked_company_gst_supply_state_id, ROUND(SUM(dn_tax_amount/2), 2), 0), 0) as availed_itc_sgst,
                    0 as availed_itc_cess
                FROM
                    debit_note_calc
				LEFT JOIN
					purchase_calc
				ON
					dne_linked_pe_id = pe_id
                where 
                    dne_tax_percent_applicable > 0 
                     AND dn_linked_company_gstin IS NOT NULL AND dn_linked_company_gstin != ""
                    AND dn_date between "'.$start_date.'" AND "'.$end_date.'"
                group by
                    dne_linked_debit_note_id, dne_tax_id_applicable
				order by
					dn_id ASC, dne_tax_percent_applicable ASC';

        $result = $this->db->query($query)->result_array();
        return $result;
    }

   
        /**
     * GSTR2 1.1 cdnur
     * @param type $start_date
     * @param type $end_date
     * @return type
     */
    public function get_gstr2_1_dot_1_cdnur($month = NULL, $year = NULL) {

        if($month == NULL){
            $month = date("m");;
        }
        
        if($year == NULL){
            $year =  date("Y");
        }
        
        $last_date =  date('t',strtotime($month.'/1/'.$year));
        
        $start_date = $year.'-'.$month.'-01';
        $end_date = $year.'-'.$month.'-'.$last_date;
      
        $query = 'SELECT 
                    CONCAT("DBN", LPAD(dne_linked_debit_note_id, "5", "0")),
                    DATE(dn_date),   
                    CONCAT("PUR", LPAD(purchase_id, "5", "0")),
                    DATE(purchase_date),  
					"N",
                    "D",    
                    dn_sir_text,    
                    if(dn_oc_gst_supply_state_id = dn_linked_company_gst_supply_state_id, "Intra State", "Inter State") as supply_type,
                    "B2BUR",
                    ROUND(SUM(dn_post_tax_amount), 2),
                    dne_tax_percent_applicable,
                    dn_taxable_amount,
                    if(dn_oc_gst_supply_state_id != dn_linked_company_gst_supply_state_id, ROUND(SUM(dn_tax_amount), 2), 0) as igst,
                    if(dn_oc_gst_supply_state_id = dn_linked_company_gst_supply_state_id, ROUND(SUM(dn_tax_amount/2), 2), 0) as cgst,
                    if(dn_oc_gst_supply_state_id = dn_linked_company_gst_supply_state_id, ROUND(SUM(dn_tax_amount/2), 2), 0) as sgst,
                    "0",
                    if(purchase_is_input_tax_credit_available = 1, "Inputs", "Ineligible"),
                    if(purchase_is_input_tax_credit_available = 1, if(dn_oc_gst_supply_state_id != dn_linked_company_gst_supply_state_id, ROUND(SUM(dn_tax_amount), 2), 0), 0) as availed_itc_igst,
                    if(purchase_is_input_tax_credit_available = 1, if(dn_oc_gst_supply_state_id = dn_linked_company_gst_supply_state_id, ROUND(SUM(dn_tax_amount/2), 2), 0), 0) as availed_itc_cgst,
                    if(purchase_is_input_tax_credit_available = 1, if(dn_oc_gst_supply_state_id = dn_linked_company_gst_supply_state_id, ROUND(SUM(dn_tax_amount/2), 2), 0), 0) as availed_itc_sgst,
                    0 as availed_itc_cess
                FROM
                    debit_note_calc
                LEFT JOIN
                    purchase_calc
		ON
                    dne_linked_pe_id = pe_id
                where 
                    dne_tax_percent_applicable > 0 
                     AND (dn_linked_company_gstin IS NULL OR dn_linked_company_gstin = "")
                    AND dn_date between "'.$start_date.'" AND "'.$end_date.'"
                group by
                    dne_linked_debit_note_id, dne_tax_id_applicable
				order by
					dn_id ASC, dne_tax_percent_applicable ASC';

        $result = $this->db->query($query)->result_array();
        return $result;
    }

    /**
     * 
     * @param type $month
     * @param type $year
     * @return type
     */
    public function get_gstr2_1_dot_1_exemp($month = NULL, $year = NULL) {
    
        if($month == NULL){
            $month = date("m");;
        }
        
        if($year == NULL){
            $year =  date("Y");
        }
        
        $last_date =  date('t',strtotime($month.'/1/'.$year));
        
        $start_date = $year.'-'.$month.'-01';
        $end_date = $year.'-'.$month.'-'.$last_date;
      
        $interstate_supplies_query = 'SELECT 
                            IFNULL(SUM(expense_amount), 0) as composition_taxable_person
                        FROM
                            expense
                        LEFT JOIN
                            company
                        ON
                            expense.expense_vendor_id = company.company_id
                        WHERE
                            expense_type = "Composition Taxable Supplies"
                                AND expense_date between "'.$start_date.'" AND "'.$end_date.'"
                                AND company_gst_supply_state_id != (Select oc_gst_supply_state_id from owner_company)';
        
        $intrastate_supplies_query = 'SELECT 
                            IFNULL(SUM(expense_amount), 0) as composition_taxable_person
                        FROM
                            expense
                        LEFT JOIN
                            company
                        ON
                            expense.expense_vendor_id = company.company_id
                        WHERE
                            expense_type = "Composition Taxable Supplies"
                                AND expense_date between "'.$start_date.'" AND "'.$end_date.'"
                                AND company_gst_supply_state_id = (Select oc_gst_supply_state_id from owner_company)';
        
        $interstate_nil_rated_supply = 'SELECT 
                                        ROUND(IFNULL(SUM(purchase_taxable_amount), 0), 2) AS nil_rated_supply
                                    FROM
                                        purchase_calc
                                    WHERE
                                        purchase_date between "'.$start_date.'" AND "'.$end_date.'"
                                                    AND pe_tax_percent_applicable = 0
                                            AND purchase_linked_company_gst_supply_state_id != (SELECT 
                                                oc_gst_supply_state_id
                                            FROM
                                                owner_company)';
        
        $intrastate_nil_rated_supply = 'SELECT 
                                        ROUND(IFNULL(SUM(purchase_taxable_amount), 0), 2) AS nil_rated_supply
                                    FROM
                                        purchase_calc
                                    WHERE
                                        purchase_date between "'.$start_date.'" AND "'.$end_date.'"
                                                    AND pe_tax_percent_applicable = 0
                                            AND purchase_linked_company_gst_supply_state_id = (SELECT 
                                                oc_gst_supply_state_id
                                            FROM
                                                owner_company)';
        
        $intrastate_exempted_query = 'SELECT 
                            IFNULL(SUM(expense_amount), 0) as exempted
                        FROM
                            expense
                        LEFT JOIN
                            company
                        ON
                            expense.expense_vendor_id = company.company_id
                        WHERE
                            (expense_type = "GST Exempt" OR expense_type = "Unregistered Dealer Purchase")
                                AND expense_date between "'.$start_date.'" AND "'.$end_date.'"
                                AND company_gst_supply_state_id = (Select oc_gst_supply_state_id from owner_company)';
        
        $interstate_exempted_query = 'SELECT 
                            IFNULL(SUM(expense_amount), 0) as exempted
                        FROM
                            expense
                        LEFT JOIN
                            company
                        ON
                            expense.expense_vendor_id = company.company_id
                        WHERE
                            (expense_type = "GST Exempt" OR expense_type = "Unregistered Dealer Purchase")
                                AND expense_date between "'.$start_date.'" AND "'.$end_date.'"
                                AND company_gst_supply_state_id != (Select oc_gst_supply_state_id from owner_company)';
        
        $intrastate_non_gst_query = 'SELECT 
                            IFNULL(SUM(expense_amount), 0) as non_gst
                        FROM
                            expense
                        LEFT JOIN
                            company
                        ON
                            expense.expense_vendor_id = company.company_id
                        WHERE
                            expense_type = "Non-GST"
                                AND expense_date between "'.$start_date.'" AND "'.$end_date.'"
                                AND company_gst_supply_state_id = (Select oc_gst_supply_state_id from owner_company)';
        
        $interstate_non_gst_query = 'SELECT 
                            IFNULL(SUM(expense_amount), 0) as non_gst
                        FROM
                            expense
                        LEFT JOIN
                            company
                        ON
                            expense.expense_vendor_id = company.company_id
                        WHERE
                            expense_type = "Non-GST"
                                AND expense_date between "'.$start_date.'" AND "'.$end_date.'"
                                AND company_gst_supply_state_id != (Select oc_gst_supply_state_id from owner_company)';
        
        $result = array();
        
        $result[0][0] = $this->db->query($interstate_supplies_query)->row_array()['composition_taxable_person'];
        $result[0][1] = $this->db->query($interstate_nil_rated_supply)->row_array()['nil_rated_supply'];
        $result[0][2] = $this->db->query($interstate_exempted_query)->row_array()['exempted'];
        $result[0][3] = $this->db->query($interstate_non_gst_query)->row_array()['non_gst'];
        
        $result[1][0] = $this->db->query($intrastate_supplies_query)->row_array()['composition_taxable_person'];
        $result[1][1] = $this->db->query($intrastate_nil_rated_supply)->row_array()['nil_rated_supply'];
        $result[1][2] = $this->db->query($intrastate_exempted_query)->row_array()['exempted'];
        $result[1][3] = $this->db->query($intrastate_non_gst_query)->row_array()['non_gst'];
    
        return $result;

    }
    
    /**
     * GSTR3 v3.0 3.1(a)
     * @param type $start_date
     * @param type $end_date
     * @return type
     */
    public function get_gstr3_3_dot_0_31a($month = NULL, $year = NULL) {

        if($month == NULL){
            $month = date("m");;
        }
        
        if($year == NULL){
            $year =  date("Y");
        }
        
        $last_date =  date('t',strtotime($month.'/1/'.$year));
        
        $start_date = $year.'-'.$month.'-01';
        $end_date = $year.'-'.$month.'-'.$last_date;
      
        $query = 'SELECT 
                    ROUND(IFNULL(SUM(invoice_taxable_amount), 0), 2) AS total_taxable_amount,
                    if(invoice_oc_gst_supply_state_id = invoice_linked_company_gst_supply_state_id, ROUND(SUM(invoice_tax_amount), 2), 0) as igst,
                    if(invoice_oc_gst_supply_state_id = invoice_linked_company_gst_supply_state_id, ROUND(SUM(invoice_tax_amount/2), 2), 0) as cgst,
                    if(invoice_oc_gst_supply_state_id = invoice_linked_company_gst_supply_state_id, ROUND(SUM(invoice_tax_amount/2), 2), 0) as sgst,
                    0 as cess
                FROM
                    invoice_calc
                WHERE
                    invoice_linked_company_gstin IS NOT NULL
                        AND invoice_linked_company_gstin != ""
                        AND ipe_tax_percent_applicable > 0
                        AND invoice_date between "'.$start_date.'" AND "'.$end_date.'"';

        $result = $this->db->query($query)->result_array();
        return $result;
    }

    
    /**
     * GSTR3 v3.0 3.1(c)
     * @param type $start_date
     * @param type $end_date
     * @return type
     */
    public function get_gstr3_3_dot_0_31c($month = NULL, $year = NULL) {

        if($month == NULL){
            $month = date("m");;
        }
        
        if($year == NULL){
            $year =  date("Y");
        }
        
        $last_date =  date('t',strtotime($month.'/1/'.$year));
        
        $start_date = $year.'-'.$month.'-01';
        $end_date = $year.'-'.$month.'-'.$last_date;
      
        $query = 'SELECT 
                    ROUND(IFNULL(SUM(invoice_taxable_amount), 0), 2) AS total_taxable_amount,
                    if(invoice_oc_gst_supply_state_id = invoice_linked_company_gst_supply_state_id, ROUND(SUM(invoice_tax_amount), 2), 0) as igst,
                    "" as cgst,
                    "" as sgst,
                    0 as cess
                FROM
                    invoice_calc
                WHERE
                    invoice_linked_company_gstin IS NOT NULL
                        AND invoice_linked_company_gstin != ""
                        AND ipe_tax_percent_applicable = 0
                        AND invoice_date between "'.$start_date.'" AND "'.$end_date.'"';

        $result = $this->db->query($query)->result_array();
        return $result;
    }
    
    
    /**
     * GSTR3 v3.0 3.2
     * @param type $start_date
     * @param type $end_date
     * @return type
     */
    public function get_gstr3_3_dot_0_32($month = NULL, $year = NULL) {

        if($month == NULL){
            $month = date("m");;
        }
        
        if($year == NULL){
            $year =  date("Y");
        }
        
        $last_date =  date('t',strtotime($month.'/1/'.$year));
        
        $start_date = $year.'-'.$month.'-01';
        $end_date = $year.'-'.$month.'-'.$last_date;
      
        $query = 'SELECT 
                    invoice_place_of_supply,
                    ROUND(IFNULL(SUM(invoice_taxable_amount), 0), 2) AS total_taxable_amount,
                    if(invoice_oc_gst_supply_state_id != invoice_linked_company_gst_supply_state_id, ROUND(SUM(invoice_tax_amount), 2), 0) as igst
                FROM
                    invoice_calc
                WHERE
                    (invoice_linked_company_gstin IS NULL
                        OR invoice_linked_company_gstin = "")
                        AND (invoice_oc_gst_supply_state_id != invoice_linked_company_gst_supply_state_id)
                        AND invoice_date between "'.$start_date.'" AND "'.$end_date.'"';

        $result = $this->db->query($query)->result_array();
        return $result;
    } 
    
    /**
     * GSTR2 1.1 b2b
     * @param type $start_date
     * @param type $end_date
     * @return type
     */
    public function get_gstr3_3_dot_0_4a5($month = NULL, $year = NULL) {

        if($month == NULL){
            $month = date("m");;
        }
        
        if($year == NULL){
            $year =  date("Y");
        }
        
        $last_date =  date('t',strtotime($month.'/1/'.$year));
        
        $start_date = $year.'-'.$month.'-01';
        $end_date = $year.'-'.$month.'-'.$last_date;
      
        $query = 'SELECT 
                    if(purchase_is_input_tax_credit_available = 1, if(purchase_oc_gst_supply_state_id = purchase_linked_company_gst_supply_state_id, ROUND(SUM(purchase_tax_amount), 2), 0), 0) as itc_igst,
                    if(purchase_is_input_tax_credit_available = 1, if(purchase_oc_gst_supply_state_id != purchase_linked_company_gst_supply_state_id, ROUND(SUM(purchase_tax_amount/2), 2), 0), 0) as itc_cgst,
                    if(purchase_is_input_tax_credit_available = 1, if(purchase_oc_gst_supply_state_id != purchase_linked_company_gst_supply_state_id, ROUND(SUM(purchase_tax_amount/2), 2), 0), 0) as itc_sgst,
                    0 as itc_cess
                FROM
                    purchase_calc
                where 
                    pe_tax_percent_applicable > 0 
                    AND purchase_linked_company_gstin IS NOT NULL AND purchase_linked_company_gstin != ""
                    AND purchase_date between "'.$start_date.'" AND "'.$end_date.'"
                group by
                    pe_linked_purchase_id, pe_tax_id_applicable
				order by
					purchase_id ASC, pe_tax_percent_applicable ASC';

        $result = $this->db->query($query)->result_array();
        return $result;
    }

}
?>