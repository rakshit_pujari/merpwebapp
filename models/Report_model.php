<?php

class Report_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    /**
     * 
     * @return type
     */
    public function get_customer_performance_report($start_date = NULL, $end_date = NULL) {

        $query = 'SELECT 
                    invoice_linked_company_id,
                    invoice_linked_company_display_name AS customer_name,
                    COUNT(DISTINCT(invoice_id)) AS number_of_invoices,
                    FORMAT(SUM(invoice_taxable_amount), 2) AS invoice_amount,
                    FORMAT((SELECT 
                                IFNULL(SUM(cn_taxable_amount), 0)
                            FROM
                                credit_note_calc
                            WHERE
                                credit_note_calc.cn_linked_invoice_id = invoice_calc.invoice_id),
                        2) AS credit_note_amount,
                    (SUM(invoice_taxable_amount) - (SELECT 
                                IFNULL(SUM(cn_taxable_amount), 0)
                            FROM
                                credit_note_calc
                            WHERE
                                credit_note_calc.cn_linked_invoice_id = invoice_calc.invoice_id) + (SELECT 
                                IFNULL(SUM(dn_taxable_amount), 0)
                            FROM
                                debit_note_calc
                            WHERE
                                debit_note_calc.dn_linked_invoice_id = invoice_calc.invoice_id)) AS net_sales
                FROM
                    invoice_calc
                        LEFT JOIN
                    debit_note_calc ON debit_note_calc.dn_linked_invoice_id = invoice_calc.invoice_id';

        if ($start_date != NULL && $end_date != NULL) {
            $query .= ' WHERE invoice_date BETWEEN "' . $start_date . '" AND "' . $end_date . '"';
        }

        $query .= ' GROUP BY invoice_linked_company_id, invoice_linked_company_display_name;';

        $result = $this->db->query($query)->result_array();

        foreach ($result as $key => $result_item) {

            if (isset($result_item['invoice_linked_company_id'])) {
                $cogs_query = 'SELECT 
                                    ABS(SUM(ie_it_product_rate * ie_it_product_quantity)) as cogs
                                FROM
                                    inventory_entry
                                WHERE
                                    ie_invoice_id IN (Select invoice_id from invoice where invoice_linked_company_id = ' . $result_item['invoice_linked_company_id'] . ')';
                $result_item['COGS_amount'] = $this->db->query($cogs_query)->row_array()['cogs'];
            } else {
                $cogs_query = 'SELECT 
                                    ABS(SUM(ie_it_product_rate * ie_it_product_quantity)) as cogs
                                FROM
                                    inventory_entry
                                WHERE
                                    ie_invoice_id IN (Select invoice_id from invoice where invoice_linked_company_display_name = "' . $result_item['customer_name'] . '")';
                $result_item['COGS_amount'] = $this->db->query($cogs_query)->row_array()['cogs'];
            }

            $result_item['gross_profit'] = doubleval($result_item['net_sales']) - doubleval($result_item['COGS_amount']);
            $result_item['gross_margin'] = number_format(doubleval($result_item['gross_profit']) / doubleval($result_item['net_sales']) * 100, 2) . ' %';
            //$result_item['gross_margin'] = number_format($result_item['gross_margin'], 2) . ' %';
            //$result_item['gross_profit'] = '₹ '.number_format($result_item['gross_profit'], 2);
            $result_item['gross_profit'] = '₹ ' . number_format($result_item['gross_profit'], 2);
            $result_item['COGS_amount'] = '₹ ' . number_format($result_item['COGS_amount'], 2);
            $result_item['net_sales'] = '₹ ' . number_format($result_item['net_sales'], 2);
            $result_item['invoice_amount'] = '₹ ' . $result_item['invoice_amount'];
            $result_item['credit_note_amount'] = '₹ ' . $result_item['credit_note_amount'];

            unset($result_item['invoice_linked_company_id']);
            $result[$key] = $result_item;
        }

        return $result;
    }

    /**
     * 
     * @return type
     */
    public function get_product_sales_report($start_date = NULL, $end_date = NULL) {

        $query = 'SELECT 
                    ipe_product_name AS product_name,
                    SUM(ipe_product_quantity) as quantity,
                    CONCAT("₹ ", FORMAT(SUM(invoice_taxable_amount), 2)) AS sales_amount,
                    CONCAT("₹ ", FORMAT(SUM(invoice_taxable_amount) / SUM(ipe_product_quantity), 2)) AS average_selling_rate
                FROM
                    invoice_calc
                    LEFT JOIN
                product ON invoice_calc.ipe_linked_product_id = product.product_id';

        if ($start_date != NULL && $end_date != NULL) {
            $query .= ' WHERE invoice_date BETWEEN "' . $start_date . '" AND "' . $end_date . '"';
        }

        $query .= ' GROUP BY ipe_linked_product_id , ipe_product_name;';

        $result = $this->db->query($query);
        return $result->result_array();
    }

    /**
     * 
     * @return type
     */
    public function get_purchase_summary($start_date = NULL, $end_date = NULL) {

        $query = 'SELECT 
                    purchase_linked_company_display_name AS vendor_name,
                    CONCAT("PUR", LPAD(purchase_id, "5", "0")) AS invoice_number,
                    date(purchase_date) as invoice_date,
                    (DATEDIFF(NOW(), purchase_due_date) + 1) AS overdue_days,
                    purchase_amount AS invoice_amount,
                    (SELECT 
                            IFNULL(SUM(debit_note.dn_amount), 0)
                        FROM
                            debit_note
                        WHERE
                            debit_note.dn_linked_purchase_id = purchase_id) AS debit_note_amount,
                    (SELECT 
                            IFNULL(SUM(payment.payment_amount), 0)
                        FROM
                            payment
                        WHERE
                            payment.payment_linked_purchase_id = purchase_id) AS amount_paid,
                    IFNULL(purchase_amount - (SELECT 
                            IFNULL(SUM(debit_note.dn_amount), 0)
                        FROM
                            debit_note
                        WHERE
                            debit_note.dn_linked_purchase_id = purchase_id) - (SELECT 
                            IFNULL(SUM(payment.payment_amount), 0)
                        FROM
                            payment
                        WHERE
                            payment.payment_linked_purchase_id = purchase_id), 0) AS balance_due
                FROM
                    purchase';

        if ($start_date != NULL && $end_date != NULL) {
            $query .= ' WHERE purchase_date BETWEEN "' . $start_date . '" AND "' . $end_date . '"';
        }

        $result = $this->db->query($query)->result_array();
        
        foreach ($result as $key => $result_item) {
            $result_item['invoice_amount'] = '₹ ' . number_format($result_item['invoice_amount'], 2);
            $result_item['debit_note_amount'] = '₹ ' . number_format($result_item['debit_note_amount'], 2);
            $result_item['amount_paid'] = '₹ ' . number_format($result_item['amount_paid'], 2);
            $result_item['balance_due'] = '₹ ' . number_format($result_item['balance_due'], 2);
            $result[$key] = $result_item;
        }
        
        return $result;
    }

    /**
     * 
     * @return type
     */
    public function get_broker_commission($start_date = NULL, $end_date = NULL) {

        $query = 'SELECT 
                    broker_name as agent_name,
                    company_display_name AS customer_name,
                    CONCAT("INV", LPAD(invoice_id, "5", "0")) AS invoice_number,
                    DATE(invoice_date) AS invoice_date,
                    (SELECT 
                            (at_amount - at_discount_amount)
                        FROM
                            invoice_accounting_transactions
                        WHERE
                            at_document_id = invoice_id
                                AND at_document_type = "invoice") AS invoice_amount,
                    IFNULL((SELECT 
                            SUM(at_amount - at_discount_amount)
                        FROM
                            cn_accounting_transactions
                        WHERE
                            at_document_id IN (SELECT 
                                    cne_linked_credit_note_id
                                FROM
                                    credit_note_entries
                                WHERE
                                    cne_linked_ipe_id IN (SELECT 
                                            ipe_id
                                        FROM
                                            invoice_product_entries
                                        WHERE
                                            ipe_linked_invoice_id = invoice_id))), 0) AS credit_note_amount,

                        ((SELECT 
                            (at_amount - at_discount_amount)
                        FROM
                            invoice_accounting_transactions
                        WHERE
                            at_document_id = invoice_id
                                AND at_document_type = "invoice") - IFNULL((SELECT 
                            SUM(at_amount - at_discount_amount)
                        FROM
                            cn_accounting_transactions
                        WHERE
                            at_document_id IN (SELECT 
                                    cne_linked_credit_note_id
                                FROM
                                    credit_note_entries
                                WHERE
                                    cne_linked_ipe_id IN (SELECT 
                                            ipe_id
                                        FROM
                                            invoice_product_entries
                                        WHERE
                                            ipe_linked_invoice_id = invoice_id))), 0)) AS net_amount,
                                        CONCAT(invoice_linked_broker_commission, " %") AS agent_commission,

                        (((SELECT 
                            (at_amount - at_discount_amount)
                        FROM
                            invoice_accounting_transactions
                        WHERE
                            at_document_id = invoice_id
                                AND at_document_type = "invoice") - IFNULL((SELECT 
                            SUM(at_amount - at_discount_amount)
                        FROM
                            cn_accounting_transactions
                        WHERE
                            at_document_id IN (SELECT 
                                    cne_linked_credit_note_id
                                FROM
                                    credit_note_entries
                                WHERE
                                    cne_linked_ipe_id IN (SELECT 
                                            ipe_id
                                        FROM
                                            invoice_product_entries
                                        WHERE
                                            ipe_linked_invoice_id = invoice_id))),0)) * invoice_linked_broker_commission / 100) AS commission_amount
                FROM
                    invoice
                        LEFT JOIN
                    broker ON invoice.invoice_linked_broker_id = broker.broker_id
                        LEFT JOIN
                    company ON invoice.invoice_linked_company_id = company.company_id
                WHERE
                    broker_id IS NOT NULL';

        if ($start_date != NULL && $end_date != NULL) {
            $query .= ' AND invoice_date BETWEEN "' . $start_date . '" AND "' . $end_date . '"';
        }


        $result = $this->db->query($query);
        $broker_array = $result->result_array();

        foreach ($broker_array as $key => $broker) {
            if ($broker['commission_amount'] <= 0) {
                //unset($broker_array[$key]);
            }
            $broker['invoice_amount'] = '₹ ' .number_format($broker['invoice_amount'], 2);
            $broker['credit_note_amount'] = '₹ ' .number_format($broker['credit_note_amount'], 2);
            $broker['net_amount'] = '₹ ' .number_format($broker['net_amount'], 2);
            $broker['commission_amount'] = '₹ ' .number_format($broker['commission_amount'], 2);        
          
            $broker_array[$key] = $broker;
        }

        return array_values($broker_array);
    }

    /**
     * 
     * @param type $start_date
     * @param type $end_date
     * @return type
     */
    public function get_customer_aging($start_date = NULL, $end_date = NULL) {
        $query = 'SELECT invoice_id, invoice_amount, invoice_linked_company_display_name, invoice_date, invoice_linked_company_id, date(invoice_date) as invoice_date,
                        (SELECT IFNULL(SUM(IFNULL(cn_amount, 0)), 0) FROM credit_note WHERE cn_linked_invoice_id = invoice_id) AS cn_amount,
                        (SELECT IFNULL(SUM(IFNULL(dn_amount, 0)), 0) FROM debit_note WHERE dn_linked_invoice_id = invoice_id) AS dn_amount,
                        (SELECT IFNULL(SUM(IFNULL(receipt_amount, 0)), 0) FROM receipt WHERE receipt_linked_invoice_id = invoice_id) AS receipt_amount,
                        (invoice_amount 
                        - (SELECT IFNULL(SUM(IFNULL(cn_amount, 0)), 0) FROM credit_note WHERE cn_linked_invoice_id = invoice_id)
                        + (SELECT IFNULL(SUM(IFNULL(dn_amount, 0)), 0) FROM debit_note WHERE dn_linked_invoice_id = invoice_id)
                        - (SELECT IFNULL(SUM(IFNULL(receipt_amount, 0)), 0) FROM receipt WHERE receipt_linked_invoice_id = invoice_id)                                
                        ) AS receipt_balance
                        FROM invoice';

        if ($start_date != NULL && $end_date != NULL) {
            $query .= ' WHERE invoice_date BETWEEN "' . $start_date . '" AND "' . $end_date . '"';
        }
        $query .= ' ORDER BY invoice_linked_company_display_name ASC;';
        $result = $this->db->query($query);
        $customers = $result->result_array();

        $customer_array = array();
        date_default_timezone_set('Asia/Kolkata');
        foreach ($customers as $customer) {

            if (!isset($customer['invoice_linked_company_id'])) {
                $customer['invoice_linked_company_id'] = $customer['invoice_linked_company_display_name'];
            }

            if (!isset($customer_array[$customer['invoice_linked_company_id']])) {
                $customer_info = array();
                $customer_info['customer_name'] = $customer['invoice_linked_company_display_name'];

                $customer_info['0_to_30_days'] = 0;
                $customer_info['31_to_60_days'] = 0;
                $customer_info['61_to_90_days'] = 0;
                $customer_info['91_to_120_days'] = 0;
                $customer_info['121_and_more_days'] = 0;
                $customer_info['total_due'] = 0;
                $customer_array[$customer['invoice_linked_company_id']] = $customer_info;
            }

            $customer_info = $customer_array[$customer['invoice_linked_company_id']];

            $date_now = new DateTime("now");
            $date_invoice = new DateTime($customer['invoice_date']);
            $date_diff = $date_invoice->diff($date_now)->days;

            if ($date_diff < 30) {
                $customer_info['0_to_30_days'] = $customer_info['0_to_30_days'] + $customer['receipt_balance'];
            } else if ($date_diff < 60) {
                $customer_info['31_to_60_days'] = $customer_info['31_to_60_days'] + $customer['receipt_balance'];
            } else if ($date_diff < 90) {
                $customer_info['61_to_90_days'] = $customer_info['61_to_90_days'] + $customer['receipt_balance'];
            } else if ($date_diff < 120) {
                $customer_info['91_to_120_days'] = $customer_info['91_to_120_days'] + $customer['receipt_balance'];
            } else {
                $customer_info['121_and_more_days'] = $customer_info['121_and_more_days'] + $customer['receipt_balance'];
            }

            $customer_info['total_due'] = $customer_info['total_due'] + $customer['receipt_balance'];


            $customer_array[$customer['invoice_linked_company_id']] = $customer_info;
        }

        foreach ($customer_array as $customer_id => $customer) {

            $formatted_customer = $customer;
            $formatted_customer['0_to_30_days'] = '₹ ' . number_format($customer['0_to_30_days']);
            $formatted_customer['31_to_60_days'] = '₹ ' . number_format($customer['31_to_60_days']);
            $formatted_customer['61_to_90_days'] = '₹ ' . number_format($customer['61_to_90_days']);
            $formatted_customer['91_to_120_days'] = '₹ ' . number_format($customer['91_to_120_days']);
            $formatted_customer['121_and_more_days'] = '₹ ' . number_format($customer['121_and_more_days']);

            $formatted_customer['total_due'] = '₹ ' . number_format($customer['total_due']);

            $customer_array[$customer_id] = $formatted_customer;
        }

        return array_values($customer_array);
    }

    /**
     * 
     * @param type $start_date
     * @param type $end_date
     * @return type
     */
    public function get_vendor_aging($start_date = NULL, $end_date = NULL) {
        $query = 'SELECT purchase_id, purchase_amount, purchase_linked_company_display_name, purchase_date, purchase_linked_company_id, date(purchase_date) as purchase_date,
                        (SELECT IFNULL(SUM(IFNULL(cn_amount, 0)), 0) FROM credit_note WHERE cn_linked_purchase_id = purchase_id) AS cn_amount,
                        (SELECT IFNULL(SUM(IFNULL(dn_amount, 0)), 0) FROM debit_note WHERE dn_linked_purchase_id = purchase_id) AS dn_amount,
                        (SELECT IFNULL(SUM(IFNULL(payment_amount, 0)), 0) FROM payment WHERE payment_linked_purchase_id = purchase_id) AS payment_amount,
                        (purchase_amount 
                        + (SELECT IFNULL(SUM(IFNULL(cn_amount, 0)), 0) FROM credit_note WHERE cn_linked_purchase_id = purchase_id)
                        - (SELECT IFNULL(SUM(IFNULL(dn_amount, 0)), 0) FROM debit_note WHERE dn_linked_purchase_id = purchase_id)
                        - (SELECT IFNULL(SUM(IFNULL(payment_amount, 0)), 0) FROM payment WHERE payment_linked_purchase_id = purchase_id)                                
                        ) AS payment_balance
                        FROM purchase'; 

        if ($start_date != NULL && $end_date != NULL) {
            $query .= ' WHERE purchase_date BETWEEN "' . $start_date . '" AND "' . $end_date . '"';
        }
        $query .= ' ORDER BY purchase_linked_company_display_name ASC;';
        $result = $this->db->query($query);
        $vendors = $result->result_array();

        $vendor_array = array();

        foreach ($vendors as $vendor) {

            if (!isset($vendor['purchase_linked_company_id'])) {
                $vendor['purchase_linked_company_id'] = $vendor['purchase_linked_company_display_name'];
            }

            if (!isset($vendor_array[$vendor['purchase_linked_company_id']])) {
                $vendor_info = array();
                $vendor_info['vendor_name'] = $vendor['purchase_linked_company_display_name'];

                $vendor_info['0_to_30_days'] = 0;
                $vendor_info['31_to_60_days'] = 0;
                $vendor_info['61_to_90_days'] = 0;
                $vendor_info['91_to_120_days'] = 0;
                $vendor_info['121_and_more_days'] = 0;
                $vendor_info['total_due'] = 0;
                $vendor_array[$vendor['purchase_linked_company_id']] = $vendor_info;
            }
            $vendor_info = $vendor_array[$vendor['purchase_linked_company_id']];

            $date_now = new DateTime("now");
            $date_purchase = new DateTime($vendor['purchase_date']);
            $date_diff = $date_now->diff($date_purchase)->days;

            if ($date_diff < 30) {
                $vendor_info['0_to_30_days'] = $vendor_info['0_to_30_days'] + $vendor['payment_balance'];
            } else if ($date_diff < 60) {
                $vendor_info['31_to_60_days'] = $vendor_info['31_to_60_days'] + $vendor['payment_balance'];
            } else if ($date_diff < 90) {
                $vendor_info['61_to_90_days'] = $vendor_info['61_to_90_days'] + $vendor['payment_balance'];
            } else if ($date_diff < 120) {
                $vendor_info['91_to_120_days'] = $vendor_info['91_to_120_days'] + $vendor['payment_balance'];
            } else {
                $vendor_info['121_and_more_days'] = $vendor_info['121_and_more_days'] + $vendor['payment_balance'];
            }

            $vendor_info['total_due'] = $vendor_info['total_due'] + $vendor['payment_balance'];
            $vendor_array[$vendor['purchase_linked_company_id']] = $vendor_info;
        }
        
        foreach ($vendor_array as $vendor_id => $vendor) {

            $formatted_vendor = $vendor;
            $formatted_vendor['0_to_30_days'] = '₹ ' . number_format($vendor['0_to_30_days']);
            $formatted_vendor['31_to_60_days'] = '₹ ' . number_format($vendor['31_to_60_days']);
            $formatted_vendor['61_to_90_days'] = '₹ ' . number_format($vendor['61_to_90_days']);
            $formatted_vendor['91_to_120_days'] = '₹ ' . number_format($vendor['91_to_120_days']);
            $formatted_vendor['121_and_more_days'] = '₹ ' . number_format($vendor['121_and_more_days']);

            $formatted_vendor['total_due'] = '₹ ' . number_format($vendor['total_due']);

            $vendor_array[$vendor_id] = $formatted_vendor;
        }

        return array_values($vendor_array);
    }

    /**
     * 
     * @param type $start_date
     * @param type $end_date
     * @return string
     */
    public function get_broker_summary($start_date = NULL, $end_date = NULL) {

        $query = 'SELECT 
                    broker_name as agent_name,
                    (SELECT 
                            COUNT(invoice_id)
                        FROM
                            invoice
                        WHERE
                            invoice_linked_broker_id = broker_id) AS invoice_count,
                    (SELECT 
                            SUM(at_amount - at_discount_amount)
                        FROM
                            invoice_accounting_transactions
                        WHERE
                            at_document_id IN (SELECT 
                                    invoice_id
                                FROM
                                    invoice
                                WHERE
                                    invoice_linked_broker_id = broker_id)) AS invoice_amount,
                    (SELECT 
                            SUM(at_amount - at_discount_amount)
                        FROM
                            cn_accounting_transactions
                        WHERE
                            at_document_id IN (SELECT 
                                    cn_id
                                FROM
                                    credit_note
                                WHERE
                                    cn_linked_broker_id = broker_id)) AS credit_note_amount,
                    ((SELECT 
                            SUM(at_broker_commission)
                        FROM
                            invoice_accounting_transactions
                        WHERE
                            at_document_id IN (SELECT 
                                    invoice_id
                                FROM
                                    invoice
                                WHERE
                                    invoice_linked_broker_id = broker_id)) - (SELECT 
                            SUM(at_broker_commission)
                        FROM
                            cn_accounting_transactions
                        WHERE
                            at_document_id IN (SELECT 
                                    cn_id
                                FROM
                                    credit_note
                                WHERE
                                    cn_linked_broker_id = broker_id))) AS agent_commission
                        FROM
                            invoice
                                LEFT JOIN
                            broker ON invoice.invoice_linked_broker_id = broker.broker_id
                        WHERE
                            invoice_linked_broker_id IS NOT NULL';

        if ($start_date != NULL && $end_date != NULL) {
            $query .= ' AND invoice_date BETWEEN "' . $start_date . '" AND "' . $end_date . '"';
        }
        $query .= ' GROUP BY broker_id';

        $result = $this->db->query($query);
        $broker_array = $result->result_array();

        $i = 0;
        foreach ($broker_array as $broker) {
            $formatted_broker = $broker;
            $formatted_broker['invoice_amount'] = '₹ ' . number_format($broker['invoice_amount'], 2);
            $formatted_broker['credit_note_amount'] = '₹ ' . number_format($broker['credit_note_amount'], 2);
            $formatted_broker['agent_commission'] = '₹ ' . number_format($broker['agent_commission'], 2);

            $broker_array[$i] = $formatted_broker;
            $i++;
        }

        return $broker_array;
    }

    /**
     * 
     * @return type
     */
    public function get_invoice_summary($start_date = NULL, $end_date = NULL) {

        $query = 'SELECT 
                    invoice_linked_company_display_name AS customer_name,
                    CONCAT("INV", LPAD(invoice_id, "5", "0")) AS invoice_number,
                    date(invoice_date) as invoice_date,
                    date(invoice_due_date) AS due_date,
                    (DATEDIFF(NOW(), invoice_due_date) + 1) AS overdue_days,
                    invoice_amount,
                    (SELECT 
                            SUM(credit_note.cn_amount)
                        FROM
                            credit_note
                        WHERE
                            credit_note.cn_linked_invoice_id = invoice_id) AS credit_note_amount,
                    (SELECT 
                            SUM(receipt.receipt_amount)
                        FROM
                            receipt
                        WHERE
                            receipt.receipt_linked_invoice_id = invoice_id) AS amount_paid
                FROM
                    invoice';

        if ($start_date != NULL && $end_date != NULL) {
            $query .= ' WHERE invoice_date BETWEEN "' . $start_date . '" AND "' . $end_date . '"';
        }

        $result = $this->db->query($query)->result_array();

        foreach ($result as $key => $result_item) {
            $result_item['balance_due'] = doubleval($result_item['invoice_amount']) - (doubleval($result_item['credit_note_amount']) + doubleval($result_item['amount_paid']));
            $result_item['balance_due'] = '₹ ' . number_format($result_item['balance_due'], 2);
            $result_item['invoice_amount'] = '₹ ' . number_format($result_item['invoice_amount'], 2);
            $result_item['credit_note_amount'] = '₹ ' . number_format($result_item['credit_note_amount'], 2);
            $result_item['amount_paid'] = '₹ ' . number_format($result_item['amount_paid'], 2);
            $result[$key] = $result_item;
        }

        return $result;
    }

    /**
     * 
     * @param type $start_date
     * @param type $end_date
     * @return type
     */
    public function get_vendor_purchase_report($start_date = NULL, $end_date = NULL) {

        $query = 'SELECT 
                    purchase_linked_company_display_name as vendor_name,
                    COUNT(purchase_id) AS number_of_purchases,
                    CONCAT("₹ ", FORMAT(SUM(purchase_amount), 2)) AS total_purchase_value
                FROM
                    purchase';

        if ($start_date != NULL && $end_date != NULL) {
            $query .= ' WHERE purchase_date BETWEEN "' . $start_date . '" AND "' . $end_date . '"';
        }

        $query .= ' GROUP BY purchase_linked_company_id , purchase_linked_company_display_name;';

        $result = $this->db->query($query);
        return $result->result_array();
    }

    /**
     * 
     * @param type $start_date
     * @param type $end_date
     * @return type
     */
    public function get_customer_sales_report($start_date = NULL, $end_date = NULL) {

        $query = 'SELECT 
                    invoice_linked_company_display_name as customer_name,
                    COUNT(invoice_id) AS number_of_invoices,
                    CONCAT("₹ ", SUM(invoice_amount)) AS total_invoice_value,
                    GROUP_CONCAT(employee_name SEPARATOR ", ") as linked_employee
                FROM
                    invoice
                LEFT JOIN
                    employee
                ON
                    employee.employee_id = invoice.invoice_linked_employee_id';

        if ($start_date != NULL && $end_date != NULL) {
            $query .= ' WHERE invoice_date BETWEEN "' . $start_date . '" AND "' . $end_date . '"';
        }

        $query .= ' GROUP BY invoice_linked_company_display_name;';

        $result = $this->db->query($query);
        return $result->result_array();
    }

    /**
     * 
     * @return type
     */
    public function get_product_purchase_report($start_date = NULL, $end_date = NULL) {

        $query = 'SELECT 
                    pe_product_name as product_name,
                    count(purchase_id) as number_of_bills,
                    pe_product_quantity as quantity,
                    CONCAT("₹ ", FORMAT(ROUND(SUM(pe_product_rate * pe_product_quantity) / pe_product_quantity, 2), 2)) AS average_purchase_cost,
                    CONCAT("₹ ", FORMAT(SUM(pe_product_rate * pe_product_quantity), 0)) AS purchase_amount
                FROM
                    purchase_entries
                        LEFT JOIN
                    purchase ON purchase_entries.pe_linked_purchase_id = purchase.purchase_id';

        if ($start_date != NULL && $end_date != NULL) {
            $query .= ' WHERE purchase_date BETWEEN "' . $start_date . '" AND "' . $end_date . '"';
        }

        $query .= ' GROUP BY pe_linked_product_id , pe_product_name;';

        $result = $this->db->query($query);
        return $result->result_array();
    }

    /**
     * For inventory overview page
     * @return type
     */
    public function get_inventory_summary($start_date = NULL, $end_date = NULL) {
        date_default_timezone_set('Asia/Kolkata');
        if ($start_date == NULL) {
            $start_date = '2000-01-01';
        }
        if ($end_date == NULL) {
            $end_date = date("Y-m-d");
        }
        $query = 'SELECT 
                    product_name,
                    uqc_text as unit_of_measurement,
                    transaction_type,
                    (SELECT 
                            ie_stock_on_hand
                        FROM
                            inventory_entry
                        WHERE
                            ie_it_product_id = product_id
                                AND date(ie_it_entry_datetime) >= "' . $start_date . '"
                        ORDER BY ie_id ASC
                        LIMIT 1) AS opening_stock,
                    (SELECT 
                            SUM(pe_product_quantity)
                        FROM
                            purchase_calc
                                WHERE
                                    pe_linked_product_id = product_id
                                        AND date(purchase_date) >= "' . $start_date . '"
                                        AND date(purchase_date) <= "' . $end_date . '") AS purchases,
                    IFNULL((SELECT 
                            SUM(dne_product_quantity)
                        FROM
                            debit_note_calc
                                WHERE
                                    dne_linked_product_id = product_id
                                        AND date(dn_date) >= "' . $start_date . '"
                                        AND date(dn_date) <= "' . $end_date . '"), 0) AS debit_note,
                    (SELECT 
                            SUM(ipe_product_quantity)
                        FROM
                            invoice_calc
                                WHERE
                                    ipe_linked_product_id = product_id
                                        AND date(invoice_date) >= "' . $start_date . '"
                                        AND date(invoice_date) <= "' . $end_date . '") AS sales,
                    IFNULL((SELECT 
                            SUM(cne_product_quantity)
                        FROM
                            credit_note_calc
                                WHERE
                                    cne_linked_product_id = product_id
                                        AND date(cn_date) >= "' . $start_date . '"
                                        AND date(cn_date) <= "' . $end_date . '"), 0) AS credit_note,
                    IFNULL((SELECT 
                            SUM(ie_it_product_quantity)
                        FROM
                            inventory_entry
                        WHERE
                            ie_it_product_id = product_id
                            AND ie_inventory_adjustment_id IS NOT NULL 
                                AND date(ie_it_entry_datetime) >= "' . $start_date . '"
                                    AND date(ie_it_entry_datetime) <= "' . $end_date . '"), 0) AS inventory_adjustment,
                    (SELECT 
                            ie_stock_on_hand
                        FROM
                            inventory_entry
                        WHERE
                            ie_it_product_id = product_id
                        ORDER BY ie_id DESC
                        LIMIT 1) AS stock_available,
                    (SELECT 
                            ie_it_product_rate
                        FROM
                            inventory_entry
                        WHERE
                            ie_it_product_id = product_id
                        ORDER BY ie_id DESC
                        LIMIT 1) AS unit_rate
                FROM
                    product
                LEFT JOIN
                    unique_quantity_code 
                ON
                    unique_quantity_code.uqc_id = product.product_uqc_id';

        //throw new Exception($query);
        $products = $this->db->query($query)->result_array();
        foreach ($products as $key => $product) {
            if($product['opening_stock'] == 0 &&
                    $product['purchases'] == 0 &&
                    $product['sales'] == 0 &&
                    $product['stock_available'] == 0){
                unset($products[$key]);
                continue;
            }
            $formatted_product = $product;
            //$formatted_product['purchases'] = '₹ ' . number_format($product['purchases'], 2);
            //$formatted_product['sales'] = '₹ ' . number_format($product['sales'], 2);
            $formatted_product['unit_rate'] = '₹ ' . number_format($product['unit_rate'], 2);

            $products[$key] = $formatted_product;
        }

        return $products;
    }

    /*
     * Retrieve all expenses
     */

    public function get_expense_summary($start_date = NULL, $end_date = NULL) {
        $query = 'SELECT 
                    date(expense_date) AS date,
                    c2.coa_account_name as expense_type,
                    expense_narration AS description,
                    expense_vendor_name AS vendor_name,
                    expense_bill_reference AS bill_reference_number,
                    c1.coa_account_name as payment_account,
                    CONCAT("₹ ", FORMAT(expense_amount, 2)) as expense_amount
                FROM
                    expense
                        LEFT JOIN
                    employee ON employee.employee_id = expense.expense_record_created_by
                                LEFT JOIN
                        chart_of_accounts AS c1 ON c1.coa_id = expense.expense_payment_account_coa_id
                        LEFT JOIN
                        chart_of_accounts AS c2 ON c2.coa_id = expense.expense_account_coa_id';

        if ($start_date != NULL && $end_date != NULL) {
            $query .= ' WHERE expense_date BETWEEN "' . $start_date . '" AND "' . $end_date . '"';
        }

        $query .= ' group by expense_id;';

        $result = $this->db->query($query);
        return $result->result_array();
    }

    /**
     * Fetch all transactions 
     * @return type
     */
    public function get_transactions_history($start_date = NULL, $end_date = NULL) {

        $query = 'SELECT 
                    date(sj_date) as transaction_date,
                    sj_document_id_text as document_reference,
                    /*sj_document_type as document_type,*/
                    coa_account_name as account_name,
                    GROUP_CONCAT(sj_narration
                        SEPARATOR ", ") AS narration,
                    FORMAT(IF(sj_transaction_type = "debit", SUM(sj_amount), NULL), 2) as debit,
                    FORMAT(IF(sj_transaction_type = "credit", SUM(sj_amount), NULL), 2) as credit
                FROM
                    system_journal
                        LEFT JOIN
                    chart_of_accounts ON sj_account_coa_id = chart_of_accounts.coa_id 
                    WHERE sj_amount > 0 ';

        if ($start_date != NULL && $end_date != NULL) {
            $query .= ' && sj_date BETWEEN "' . $start_date . '" AND "' . $end_date . '"';
        }

        $query .= ' GROUP BY CASE
                    WHEN sj_field_name ="inventory_effect" THEN 1  
                    ELSE 0
                    END, sj_account_coa_id , sj_document_id_text , sj_transaction_type 
                    ORDER BY sj_date, sj_transaction_type DESC;';
        $result = $this->db->query($query)->result_array();
        return $result;
    }

}
