<?php

class System_journal_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }


    /**
     * Fetch all transactions 
     * @return type
     */
    public function get_system_transactions_for_entity($entity_type, $entity_id) {
        //$this->calculate_system_journal();
        //$query = 'Select * from system_journal left join chart_of_accounts ON sj_account_coa_id = chart_of_accounts.coa_id';
        $query = 'SELECT 
                    sj_date,
                    broker_name,
                    employee_name,
                    transporter_name,
                    sj_linked_broker_id,
                    sj_linked_company_id,
                    sj_linked_employee_id,
                    sj_linked_transporter_id,
                    sj_account_coa_id,
                    company_display_name,
                    sj_document_type,
                    sj_document_type,
                    sj_document_id,
                    sj_document_id_text,
                    coa_account_name,
                    sj_transaction_type,
                    GROUP_CONCAT(sj_narration
                        SEPARATOR ", ") AS sj_narration,
                    SUM(sj_amount) AS sj_amount
                FROM
                    system_journal
                        LEFT JOIN
                    chart_of_accounts ON sj_account_coa_id = chart_of_accounts.coa_id
                        LEFT JOIN
                    broker ON sj_linked_broker_id = broker.broker_id
                        LEFT JOIN
                    company ON sj_linked_company_id = company.company_id
                        LEFT JOIN
                    employee ON sj_linked_employee_id = employee.employee_id
                        LEFT JOIN
                    transporter ON sj_linked_transporter_id = transporter.transporter_id ';
        
        if($entity_type == 'broker'){
            $query.='WHERE sj_linked_broker_id = '.$entity_id;
        } else if($entity_type == 'company'){
            $query.='WHERE sj_linked_company_id = '.$entity_id;
        } else if($entity_type == 'employee'){
            $query.='WHERE sj_linked_employee_id = '.$entity_id;
        } else if($entity_type == 'transporter'){
            $query.='WHERE sj_linked_transporter_id = '.$entity_id;
        } else {
            throw new Exception('Unknown entity type');
        }
        
        $query.=' GROUP BY CASE
            WHEN sj_field_name ="inventory_effect" THEN 1  
            ELSE 0
         END, sj_account_coa_id , sj_document_id_text , sj_transaction_type 
                ORDER BY sj_date, sj_transaction_type DESC';
        $result = $this->db->query($query)->result_array();
        return $result;
    }
    
    public function get_system_transactions_for_report() {

        $query = 'SELECT 
                    sj_date as date,
                    company_display_name,
                    employee_name,
                    broker_name,
                    transporter_name,
                    sj_document_type as document_type,
                    sj_document_id_text as document_id,
                    coa_account_name as chart_of_account_name,
                    sj_transaction_type as transaction_type,
                    GROUP_CONCAT(sj_narration
                        SEPARATOR ", ") AS narration,
                    SUM(sj_amount) AS amount
                FROM
                    system_journal
                        LEFT JOIN
                    chart_of_accounts ON sj_account_coa_id = chart_of_accounts.coa_id
                        LEFT JOIN
                    broker ON sj_linked_broker_id = broker.broker_id
                        LEFT JOIN
                    company ON sj_linked_company_id = company.company_id
                        LEFT JOIN
                    employee ON sj_linked_employee_id = employee.employee_id
                        LEFT JOIN
                    transporter ON sj_linked_transporter_id = transporter.transporter_id
                GROUP BY CASE
            WHEN sj_field_name ="inventory_effect" THEN 1  
            ELSE 0
         END, sj_account_coa_id , sj_document_id_text , sj_transaction_type 
                ORDER BY sj_date DESC, sj_transaction_type DESC';
        $result = $this->db->query($query)->result_array();
        return $result;
    }
    
    
    /**
     * Fetch all transactions 
     * @return type
     */
    public function get_system_transactions() {
        $this->calculate_system_journal();
        //$query = 'Select * from system_journal left join chart_of_accounts ON sj_account_coa_id = chart_of_accounts.coa_id';
        $query = 'SELECT 
                    sj_date,
                    sj_linked_broker_id,
                    sj_linked_company_id,
                    sj_linked_employee_id,
                    sj_linked_transporter_id,
                    company_display_name,
                    employee_name,
                    broker_name,
                    transporter_name,
                    sj_document_type,
                    sj_document_type,
                    sj_document_id,
                    sj_document_id_text,
                    coa_account_name,
                    sj_account_coa_id,
                    sj_transaction_type,
                    GROUP_CONCAT(sj_narration
                        SEPARATOR ", ") AS sj_narration,
                    SUM(sj_amount) AS sj_amount
                FROM
                    system_journal
                        LEFT JOIN
                    chart_of_accounts ON sj_account_coa_id = chart_of_accounts.coa_id
                        LEFT JOIN
                    broker ON sj_linked_broker_id = broker.broker_id
                        LEFT JOIN
                    company ON sj_linked_company_id = company.company_id
                        LEFT JOIN
                    employee ON sj_linked_employee_id = employee.employee_id
                        LEFT JOIN
                    transporter ON sj_linked_transporter_id = transporter.transporter_id
                GROUP BY CASE
            WHEN sj_field_name ="inventory_effect" THEN 1  
            ELSE 0
         END, sj_account_coa_id , sj_document_id_text , sj_transaction_type 
                ORDER BY sj_date DESC, sj_transaction_type DESC';
        $result = $this->db->query($query)->result_array();
        return $result;
    }

    /**
     * @return type
     */
    public function get_ledger($coa_id = NULL, $entity_type = NULL, $entity_id = NULL) {
        //$this->calculate_system_journal();
        if ($coa_id == NULL) {
            $query = 'SELECT DISTINCT
                        (sj_account_coa_id),
                        coa_account_name,
                        coa_subaccount_of_id,
                        coa_opening_balance,
                        coa_opening_balance_transaction_type,
                        at_name,
                        (SELECT 
                                SUM(sj_amount)
                            FROM
                                system_journal
                            WHERE
                                sj_transaction_type = "credit"
                                    && sj_account_coa_id = SJ.sj_account_coa_id) AS credit_balance,
                        (SELECT 
                                SUM(sj_amount)
                            FROM
                                system_journal
                            WHERE
                                sj_transaction_type = "debit"
                                    && sj_account_coa_id = SJ.sj_account_coa_id) AS debit_balance
                        FROM
                            system_journal AS SJ
                        LEFT JOIN
                            chart_of_accounts
                        ON
                            SJ.sj_account_coa_id = chart_of_accounts.coa_id
                        LEFT JOIN
                            account_type
                        ON
                            chart_of_accounts.coa_at_id = account_type.at_id
                        GROUP BY sj_account_coa_id
                        ORDER BY sj_account_coa_id ASC';
        } else {
            /** 
             * $query = 'Select * from system_journal LEFT JOIN chart_of_accounts ON system_journal.sj_account_coa_id = chart_of_accounts.coa_id WHERE sj_account_coa_id =' . $coa_id;
             * 
             * new query to resolve issue #127
             */
            
            switch($entity_type){
                case "broker":
                    $sj_linked_entity_id_field = 'sj_linked_broker_id';
                    $display_field_name = 'broker_name';

                    $left_join_table = 'broker';
                    $left_join_table_id_field = 'broker_id';
                    break;
                case "company":
                    $sj_linked_entity_id_field = 'sj_linked_company_id';
                    $display_field_name = 'company_display_name';

                    $left_join_table = 'company';
                    $left_join_table_id_field = 'company_id';
                    break;
                case "employee":
                    $sj_linked_entity_id_field = 'sj_linked_employee_id';
                    $display_field_name = 'employee_name';

                    $left_join_table = 'employee';
                    $left_join_table_id_field = 'employee_id';
                    break;
                case "transporter":
                    $sj_linked_entity_id_field = 'sj_linked_transporter_id';
                    $display_field_name = 'transporter_name';

                    $left_join_table = 'transporter';
                    $left_join_table_id_field = 'transporter_id';
                    break;
                //default:
                    //throw new Exception("Unknown entity type");
            }

            if($entity_id==NULL 
                    || $entity_id == 'unregistered'){
                $query = 'Select *,GROUP_CONCAT(sj_narration
                            SEPARATOR ", ") AS sj_narration,
                            sj_linked_broker_id,
                            sj_linked_company_id,
                            sj_linked_employee_id,
                            sj_linked_transporter_id,
                            broker_name,
                            company_display_name,
                            employee_name,
                            transporter_name,
                            SUM(sj_amount) AS sj_amount from system_journal 
                        LEFT JOIN chart_of_accounts ON system_journal.sj_account_coa_id = chart_of_accounts.coa_id
                        LEFT JOIN broker ON system_journal.sj_linked_broker_id = broker.broker_id
                        LEFT JOIN company ON system_journal.sj_linked_company_id = company.company_id
                        LEFT JOIN employee ON system_journal.sj_linked_employee_id = employee.employee_id
                        LEFT JOIN transporter ON system_journal.sj_linked_transporter_id = transporter.transporter_id
                        WHERE sj_account_coa_id =' . $coa_id;
                        
                if($entity_id == 'unregistered'){
                    $query.=' AND '.$sj_linked_entity_id_field.' IS NULL';
                }
                
                //throw new Exception($query);
            } else {
                
                $query = 'Select *,GROUP_CONCAT(sj_narration
                        SEPARATOR ", ") AS sj_narration,
                        '.$sj_linked_entity_id_field.',
                        '.$display_field_name.',
                        SUM(sj_amount) AS sj_amount from system_journal 
                        LEFT JOIN chart_of_accounts ON system_journal.sj_account_coa_id = chart_of_accounts.coa_id
                        LEFT JOIN 
                        '.$left_join_table.' ON '.$sj_linked_entity_id_field.' = '.$left_join_table.'.'.$left_join_table_id_field.' 
                        WHERE sj_account_coa_id =' . $coa_id;
                
                $query.=' AND '.$sj_linked_entity_id_field.' =' . $entity_id;
            
            }
            
            $query.=' GROUP BY CASE
                        WHEN sj_field_name ="inventory_effect" THEN 1  
                        ELSE 0
                        END, sj_account_coa_id , sj_document_id_text , sj_transaction_type 
                        ORDER BY sj_date, sj_transaction_type DESC';
        }
        
        $result = $this->db->query($query)->result_array();
        //throw new Exception(print_r($query));
        return $result;
    }
    
    /**
     * 
     * @param type $coa_id
     * @return type
     */
    public function ledger_account_total_for_entity($entity_type, $coa_id){
        
        switch($entity_type){
            case "broker":
                $sj_linked_entity_id_field = 'sj_linked_broker_id';
                $display_field_name = 'broker_name';
                
                $left_join_table = 'broker';
                $left_join_table_id_field = 'broker_id';
                break;
            case "company":
                $sj_linked_entity_id_field = 'sj_linked_company_id';
                $display_field_name = 'company_display_name';
                
                $left_join_table = 'company';
                $left_join_table_id_field = 'company_id';
                break;
            case "employee":
                $sj_linked_entity_id_field = 'sj_linked_employee_id';
                $display_field_name = 'employee_name';
                
                $left_join_table = 'employee';
                $left_join_table_id_field = 'employee_id';
                break;
            case "transporter":
                $sj_linked_entity_id_field = 'sj_linked_transporter_id';
                $display_field_name = 'transporter_name';
                
                $left_join_table = 'transporter';
                $left_join_table_id_field = 'transporter_id';
                break;
            default:
                throw new Exception("Unknown entity type");
        }
            
        $query = 'SELECT 
                    coa_account_name,
                    '.$sj_linked_entity_id_field.',
                    SUM(debit_balance) AS debit_balance,
                    SUM(credit_balance) AS credit_balance,'
                    .$display_field_name.
                ' FROM
                    (SELECT 
                        coa_account_name,
                            IFNULL('.$sj_linked_entity_id_field.', 0) AS '.$sj_linked_entity_id_field.',
                            SUM(sj_amount) AS debit_balance,
                            0 AS credit_balance,'
                    .$display_field_name.
                    ' FROM
                        system_journal
                    LEFT JOIN '.$left_join_table.' ON system_journal.'.$sj_linked_entity_id_field.' = '.$left_join_table.'.'.$left_join_table_id_field.'
                     LEFT JOIN chart_of_accounts ON system_journal.sj_account_coa_id = chart_of_accounts.coa_id
                    WHERE
                        sj_account_coa_id = '.$coa_id.
                           ' AND sj_transaction_type = "debit"
                    GROUP BY '.$sj_linked_entity_id_field.' UNION SELECT 
                        coa_account_name,
                            IFNULL('.$sj_linked_entity_id_field.', 0) AS '.$sj_linked_entity_id_field.',
                            0 AS debit_balance,
                            SUM(sj_amount) AS credit_balance,'
                    .$display_field_name.
                ' FROM
                        system_journal
                    LEFT JOIN '.$left_join_table.' ON system_journal.'.$sj_linked_entity_id_field.' = '.$left_join_table.'.'.$left_join_table_id_field.'
                     LEFT JOIN chart_of_accounts ON system_journal.sj_account_coa_id = chart_of_accounts.coa_id
                    WHERE
                        sj_account_coa_id = '.$coa_id.
                           ' AND sj_transaction_type = "credit"
                    GROUP BY '.$sj_linked_entity_id_field.') AS temp
                GROUP BY '.$sj_linked_entity_id_field.'
                ORDER BY '.$display_field_name.' ASC';
        
        $result = $this->db->query($query)->result_array();
        return $result;
    }

    /**
     * Triggers - Inventory evaluation( covers invoice, purchase, CN, DN ), payment save, receipt save, expense save/delete
     * @throws Exception
     */
    public function calculate_system_journal() {
        $this->db->query('SET time_zone = "+05:30";');
        /**
         * Delete all entries
         */
        $delete_query = 'Delete from system_journal WHERE sj_id >= 1';
        $this->db->query($delete_query);

        /**
         * Reset auto increment
         */
        $this->db->query('Alter table system_journal AUTO_INCREMENT = 1');


        /**
         * Add opening balance
         */
        $coa_query = 'Select * from chart_of_accounts';
        $coas = $this->db->query($coa_query)->result_array();

        $opening_balance_date = $this->db->query('Select * from owner_company')->row_array()['oc_opening_balance_date'];
        foreach ($coas as $coa) {
            $system_journal = array();
            $system_journal['sj_narration'] = 'Opening Balance For ' . $coa['coa_account_name'];
            $system_journal['sj_field_name'] = 'Opening Balance';
            $system_journal['sj_amount'] = $coa['coa_opening_balance'];
            $system_journal['sj_document_type'] = 'coa_opening_balance';
            $system_journal['sj_document_id'] = $coa['coa_id'];
            $system_journal['sj_document_id_text'] = 'COA' . str_pad($coa['coa_id'], 5, '0', STR_PAD_LEFT);
            //$system_journal['sj_date'] = $coa['coa_opening_balance_date'];
            $system_journal['sj_date'] = $opening_balance_date;

            $system_journal['sj_transaction_type'] = $coa['coa_opening_balance_transaction_type'];
            $system_journal['sj_account_coa_id'] = $coa['coa_id'];
            //if ($coa['coa_opening_balance'] > 0)
                $this->db->insert('system_journal', $system_journal);
        }
        
        $this->load->model('owner_company_model');
        
        $payment_receivables = array_merge($this->owner_company_model->get_all_receivables(), $this->owner_company_model->get_all_payables());
        foreach ($payment_receivables as $payment_receivable) {
            
            if(empty($payment_receivable['pr_opening_balance_transaction_type']))
                continue;
            
            $system_journal = array();
            $system_journal['sj_narration'] = 'Opening Balance For ' . $payment_receivable['pr_entity_name'];
            $system_journal['sj_field_name'] = 'Opening Balance';
            $system_journal['sj_amount'] = $payment_receivable['pr_opening_balance'];
            $system_journal['sj_document_type'] = $payment_receivable['pr_entity_type'];
            $system_journal['sj_document_id'] = $payment_receivable['pr_entity_id'];
            $system_journal['sj_document_id_text'] = $payment_receivable['pr_entity_prefix'] . str_pad($payment_receivable['pr_entity_id'], 5, '0', STR_PAD_LEFT);
            $system_journal['sj_date'] = $opening_balance_date; 
            
            if($payment_receivable['pr_entity_type'] == 'broker'){
                $system_journal['sj_linked_broker_id'] = $payment_receivable['pr_entity_id'];
            } else if($payment_receivable['pr_entity_type'] == 'company'){
                $system_journal['sj_linked_company_id'] = $payment_receivable['pr_entity_id'];
            } else if($payment_receivable['pr_entity_type'] == 'employee'){
                $system_journal['sj_linked_employee_id'] = $payment_receivable['pr_entity_id'];
            } else if($payment_receivable['pr_entity_type'] == 'transporter'){
                $system_journal['sj_linked_transporter_id'] = $payment_receivable['pr_entity_id'];
            }

            $system_journal['sj_transaction_type'] = $payment_receivable['pr_opening_balance_transaction_type'];
            if($payment_receivable['pr_entity_type_display'] == 'customer'){
                $system_journal['sj_account_coa_id'] = $this->owner_company_model->get_accounting_settings_for_field('general_receivables')['debit_account_coa_id'];
            } else {
                $system_journal['sj_account_coa_id'] = $this->owner_company_model->get_accounting_settings_for_field('general_payables')['debit_account_coa_id'];
            }
            
            $this->db->insert('system_journal', $system_journal);
        }

        /**
         * Populate
         */
        $query = 'Select * from accounting_transactions';
        $result = $this->db->query($query);
        $accounting_transactions = $result->result_array();

        foreach ($accounting_transactions as $accounting_transaction) {

            $transaction_type = $accounting_transaction['at_document_type'];
            $at_linked_company_id = $accounting_transaction['at_linked_company_id'];
            $at_document_id_text = $accounting_transaction['at_document_id_text'];
            $at_document_id = $accounting_transaction['at_document_id'];
            $at_tradeable_amount = $accounting_transaction['at_tradeable_amount'];
            $at_non_tradeable_amount = $accounting_transaction['at_non_tradeable_amount'];
            $at_discount_amount = $accounting_transaction['at_discount_amount'];
            $at_transport_charges = $accounting_transaction['at_transport_charges'];
            $at_tax = $accounting_transaction['at_tax'];
            $at_tax_type = $accounting_transaction['at_tax_type'];
            $at_broker_commission = $accounting_transaction['at_broker_commission'];
            $at_adjustments = $accounting_transaction['at_adjustments'];
            $at_tradeable_inventory_effect = $accounting_transaction['at_tradeable_inventory_effect'];
            $at_non_tradeable_inventory_effect = $accounting_transaction['at_non_tradeable_inventory_effect'];
            $at_date = $accounting_transaction['at_date'];

            switch ($transaction_type) {
                case 'invoice':
                case 'credit_note':
                case 'purchase':
                case 'debit_note':
                    if($transaction_type == 'purchase' || $transaction_type == 'debit_note'){
                        $this->make_system_journal_entry($transaction_type, $at_linked_company_id, 'tradeable_amount', $at_document_id_text, $at_document_id, $at_tradeable_amount, $at_date);
                        $this->make_system_journal_entry($transaction_type, $at_linked_company_id, 'raw_material_amount', $at_document_id_text, $at_document_id, $at_non_tradeable_amount, $at_date);
                        $this->make_system_journal_entry($transaction_type, $at_linked_company_id, 'raw_materials_inventory', $at_document_id_text, $at_document_id, $at_non_tradeable_inventory_effect, $at_date);
                    } else {
                        $this->make_system_journal_entry($transaction_type, $at_linked_company_id, 'amount', $at_document_id_text, $at_document_id, $at_tradeable_amount, $at_date);
                    }
                    $this->make_system_journal_entry($transaction_type, $at_linked_company_id, 'discount', $at_document_id_text, $at_document_id, $at_discount_amount, $at_date);
                    $this->make_system_journal_entry($transaction_type, $at_linked_company_id, 'transport_charges', $at_document_id_text, $at_document_id, $at_transport_charges, $at_date);
                    if ($at_tax_type == 'intrastate') {
                        $this->make_system_journal_entry($transaction_type, $at_linked_company_id, 'SGST', $at_document_id_text, $at_document_id, $at_tax / 2, $at_date);
                        $this->make_system_journal_entry($transaction_type, $at_linked_company_id, 'CGST', $at_document_id_text, $at_document_id, $at_tax / 2, $at_date);
                    } else if ($at_tax_type == 'interstate') {
                        $this->make_system_journal_entry($transaction_type, $at_linked_company_id, 'IGST', $at_document_id_text, $at_document_id, $at_tax, $at_date);
                    } else {
                        throw new Exception('tax_type not defined');
                    }
                    if (isset($at_broker_commission)) {
                        $this->make_system_journal_entry($transaction_type, $at_linked_company_id, 'broker_commission', $at_document_id_text, $at_document_id, $at_broker_commission, $at_date);
                    }
                    $this->make_system_journal_entry($transaction_type, $at_linked_company_id, 'adjustment', $at_document_id_text, $at_document_id, $at_adjustments, $at_date);
                    $this->make_system_journal_entry($transaction_type, $at_linked_company_id, 'tradeable_inventory_effect', $at_document_id_text, $at_document_id, $at_tradeable_inventory_effect, $at_date);
                    
                    break;
                case 'payment':
                case 'receipt':
                    $this->make_system_journal_entry($transaction_type, $at_linked_company_id, 'amount', $at_document_id_text, $at_document_id, $at_tradeable_amount, $at_date);
                    break;
                case 'manufacturing_raw_material':
                    $this->make_system_journal_entry('manufacturing', $at_linked_company_id, 'confirm', $at_document_id_text, $at_document_id, $at_non_tradeable_inventory_effect, $at_date, 'Inventory Effect');
                    break;
                case 'manufacturing_finished_goods':
                    $this->make_system_journal_entry('manufacturing', $at_linked_company_id, 'complete', $at_document_id_text, $at_document_id, $at_non_tradeable_inventory_effect, $at_date, 'Inventory Effect');
                    break;
                case 'advance_payment':
                case 'advance_receipt':
                        //make_system_journal_entry($as_for, $as_linked_company_id, $as_field_name, $as_document_id_text, $as_document_id, $amount, $date)
                    $as_query = 'Select * from accounting_setting WHERE as_for = "' . $transaction_type . '" && as_field_name = "amount"';
                    $as_result = $this->db->query($as_query)->row_array();
                    
                    if($transaction_type == 'advance_payment'){
                        $query = 'Select * from advance_payment WHERE advance_payment_id = "' . $at_document_id . '"';
                    } else if($transaction_type == 'advance_receipt'){
                        $query = 'Select * from advance_receipt WHERE advance_receipt_id = "' . $at_document_id . '"';
                    } else {
                        throw new Exception('Invalid transaction type');
                    }
                    
                    $result = $this->db->query($query)->row_array();

                    $system_journal = array();
                    $system_journal['sj_narration'] = 'Amount';
                    $system_journal['sj_field_name'] = 'amount';
                    $system_journal['sj_linked_company_id'] = $at_linked_company_id;
                    $system_journal['sj_amount'] = abs($at_tradeable_amount);
                    $system_journal['sj_document_type'] = $transaction_type;
                    $system_journal['sj_document_id'] = $at_document_id;
                    $system_journal['sj_document_id_text'] = $at_document_id_text;
                    $system_journal['sj_date'] = $at_date;

                    $system_journal['sj_transaction_type'] = 'debit';
                    if($transaction_type == 'advance_payment'){
                        $system_journal['sj_account_coa_id'] = $as_result['as_debit_account_coa_id'];
                    } else {
                        $system_journal['sj_account_coa_id'] = $result['advance_receipt_debit_coa_id'];
                    }
                    $this->db->insert('system_journal', $system_journal);

                    $system_journal['sj_transaction_type'] = 'credit';
                    if($transaction_type == 'advance_payment'){
                        $system_journal['sj_account_coa_id'] = $result['advance_payment_credit_coa_id']; 
                    } else {
                        $system_journal['sj_account_coa_id'] = $as_result['as_credit_account_coa_id'];
                    }
                    
                    $this->db->insert('system_journal', $system_journal);
                    break;
                case 'inventory_adjustment':
                    
                    $system_journal['sj_narration'] = 'Inventory Adjustment - '.$at_document_id_text;
                    $system_journal['sj_linked_company_id'] = $at_linked_company_id;
                    $system_journal['sj_amount'] = abs($at_tradeable_amount);
                    $system_journal['sj_document_type'] = $transaction_type;
                    $system_journal['sj_document_id'] = $at_document_id;
                    $system_journal['sj_document_id_text'] = $at_document_id_text;
                    $system_journal['sj_date'] = $at_date;

                    if($at_tradeable_amount > 0){
                        $system_journal['sj_transaction_type'] = 'credit';
                    } else {
                        $system_journal['sj_transaction_type'] = 'debit';
                    }
                    
                    $this->load->model('owner_company_model');
                    //COGS account
                    $system_journal['sj_account_coa_id'] = $this->owner_company_model->get_accounting_settings_for_field('general_cogs')['debit_account_coa_id'];
                    $this->db->insert('system_journal', $system_journal);
                    
                    if($at_tradeable_amount > 0){
                        $system_journal['sj_transaction_type'] = 'debit';
                    } else {
                        $system_journal['sj_transaction_type'] = 'credit';
                    }
                    //inventory account
                    $system_journal['sj_account_coa_id'] = $this->owner_company_model->get_accounting_settings_for_field('coa_opening_balance_trade_inventory')['debit_account_coa_id'];
                    $this->db->insert('system_journal', $system_journal);
                    
                    break;
                case 'expense':
                    $expense_query = 'Select * from expense WHERE expense_id = "' . $at_document_id . '"';
                    $expense_result = $this->db->query($expense_query)->row_array();

                    $expense_journal = array();
                    $expense_journal['sj_account_coa_id'] = $expense_result['expense_account_coa_id'];
                    //$expense_journal['sj_credit_account_coa_id'] = $expense_result['expense_payment_account_coa_id'];
                    $expense_journal['sj_linked_company_id'] = $expense_result['expense_vendor_id'];
                    $expense_journal['sj_transaction_type'] = 'debit';
                    $expense_journal['sj_narration'] = 'Expense ' . $at_document_id_text;
                    $expense_journal['sj_amount'] = $at_tradeable_amount;
                    $expense_journal['sj_document_type'] = 'expense';
                    $expense_journal['sj_document_id'] = $at_document_id;
                    $expense_journal['sj_document_id_text'] = $at_document_id_text;
                    $expense_journal['sj_date'] = $at_date;
                    $this->db->insert('system_journal', $expense_journal);
                    
                    $expense_journal['sj_account_coa_id'] = $expense_result['expense_payment_account_coa_id'];
                    $expense_journal['sj_transaction_type'] = 'credit';
                    $this->db->insert('system_journal', $expense_journal);

                    break;
                case 'journal':

                    $journal_query = 'Select * from journal_entries where je_id = "' . $at_document_id . '"';
                    $journal_entry = $this->db->query($journal_query)->row_array();

                    $system_journal = array();

                    $system_journal['sj_narration'] = $journal_entry['je_description'];
                    
                    $system_journal['sj_linked_broker_id'] = $journal_entry['je_linked_broker_id'];
                    $system_journal['sj_linked_company_id'] = $journal_entry['je_linked_company_id'];
                    $system_journal['sj_linked_employee_id'] = $journal_entry['je_linked_employee_id'];
                    $system_journal['sj_linked_transporter_id'] = $journal_entry['je_linked_transporter_id'];
                    
                    $system_journal['sj_amount'] = $journal_entry['je_amount'];
                    $system_journal['sj_document_type'] = $transaction_type;
                    $system_journal['sj_document_id'] = $journal_entry['je_linked_journal_id'];
                    $system_journal['sj_document_id_text'] = 'JRN' . str_pad($journal_entry['je_linked_journal_id'], 5, '0', STR_PAD_LEFT);
                    $system_journal['sj_date'] = $at_date;

                    $system_journal['sj_transaction_type'] = $journal_entry['je_transaction_type'];
                    $system_journal['sj_account_coa_id'] = $journal_entry['je_linked_coa_id'];
                    $this->db->insert('system_journal', $system_journal);
                    break;
                
                case 'sales_charges':
                case 'purchase_charges':
                case 'credit_note_charges':
                case 'debit_note_charges':
                    $charge_query = 'Select * from charge where charge_id = "' . $at_document_id . '"';
                    $charge_entry = $this->db->query($charge_query)->row_array();
                    
                    $as_query = 'Select * from accounting_setting WHERE as_id = "' . $charge_entry['charge_linked_accounting_setting_id'] . '"';
                    $as_result = $this->db->query($as_query)->row_array();

                    if(empty($as_result['as_debit_account_coa_id']) || empty($as_result['as_debit_account_coa_id'])){
                        throw new Exception($as_query);
                    }
                    $system_journal = array();

                    $system_journal['sj_narration'] = ucwords(str_replace('_', ' ', $transaction_type.' '));                  
                    $system_journal['sj_linked_company_id'] = $at_linked_company_id;
                    $system_journal['sj_amount'] = $at_tradeable_amount;
                    $system_journal['sj_document_type'] = $transaction_type;
                    $system_journal['sj_document_id'] = $at_document_id;
                    $system_journal['sj_document_id_text'] = $at_document_id_text;
                    $system_journal['sj_date'] = $at_date;

                    $system_journal['sj_transaction_type'] = 'debit';
                    $system_journal['sj_account_coa_id'] = $as_result['as_debit_account_coa_id'];
                    $this->db->insert('system_journal', $system_journal);
                    
                    $system_journal['sj_transaction_type'] = 'credit';
                    $system_journal['sj_account_coa_id'] = $as_result['as_credit_account_coa_id'];
                    $this->db->insert('system_journal', $system_journal);
                    
                    
                    if($transaction_type == 'sales_charges'){
                        $as_for = 'invoice';
                    } else if($transaction_type == 'purchase_charges'){
                        $as_for = 'purchase';
                    } else if($transaction_type == 'credit_note_charges'){
                        $as_for = 'credit_note';
                    } else if($transaction_type == 'debit_note_charges'){
                        $as_for = 'debit_note';
                    } else {
                       throw new Exception('Invalid charge'); 
                    }
                    //taxes
                    if ($at_tax_type == 'intrastate') {
                        $this->make_system_journal_entry($as_for, $at_linked_company_id, 'SGST', $at_document_id_text, $at_document_id, $at_tax / 2, $at_date);
                        $this->make_system_journal_entry($as_for, $at_linked_company_id, 'CGST', $at_document_id_text, $at_document_id, $at_tax / 2, $at_date);
                    } else if ($at_tax_type == 'interstate') {
                        $this->make_system_journal_entry($as_for, $at_linked_company_id, 'IGST', $at_document_id_text, $at_document_id, $at_tax, $at_date);
                    } else {
                        throw new Exception('tax_type not defined');
                    }
                    break;
            }
        }
    }
    
    
    
    /**
     * In system journal entry, credit account and debit account details are taken from account_setting table
     * 
     * @param type $as_for
     * @param type $as_field_name
     * @param type $as_document_id_text
     * @param type $as_document_id
     * @param type $amount
     * @param type $date
     */
    public function make_system_journal_entry($as_for, $as_linked_company_id, $as_field_name, 
            $as_document_id_text, $as_document_id, $amount, $date, $narration = NULL) {
        if ($amount == 0)
            return;

        $as_query = 'Select * from accounting_setting WHERE as_for = "' . $as_for . '" && as_field_name = "' . $as_field_name . '"';
        $as_result = $this->db->query($as_query)->row_array();

         
        $system_journal = array();
        if($narration == NULL){
            $system_journal['sj_narration'] = ucwords(str_replace('_', ' ', $as_for.' '.$as_field_name));
        } else {
            $system_journal['sj_narration'] = $narration;
        }
        $system_journal['sj_field_name'] = $as_field_name;
        $system_journal['sj_linked_company_id'] = $as_linked_company_id;
        $system_journal['sj_amount'] = abs($amount);
        $system_journal['sj_document_type'] = $as_for;
        $system_journal['sj_document_id'] = $as_document_id;
        $system_journal['sj_document_id_text'] = $as_document_id_text;
        $system_journal['sj_date'] = $date;

        if(!empty($as_result['as_debit_account_coa_id'])){
            $system_journal['sj_transaction_type'] = 'debit';
            $system_journal['sj_account_coa_id'] = $as_result['as_debit_account_coa_id'];
            $this->db->insert('system_journal', $system_journal);
        } else {
            throw new Exception('No debit COA ID for '.$as_for.' AND '.$as_field_name);
        }

        if(!empty($as_result['as_credit_account_coa_id'])){
            $system_journal['sj_transaction_type'] = 'credit';
            $system_journal['sj_account_coa_id'] = $as_result['as_credit_account_coa_id'];
            $this->db->insert('system_journal', $system_journal);
        } else {
            throw new Exception('No credit COA ID for '.$as_for.' AND '.$as_field_name);
        }
        
    }

}
