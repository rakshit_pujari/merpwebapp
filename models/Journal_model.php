<?php

class Journal_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    /*
     * Delete Journal
     */

    function delete_journal_by_id($id) {
        log_message('debug', 'deleteJournalBy. - $id = ' . print_r($id, 1));
        try {
            $this->db->trans_begin();

            $this->db->where('je_linked_journal_id', $id);
            $this->db->delete('journal_entries');

            $this->db->where('journal_id', $id);
            $this->db->delete('journal');

            log_message('debug', 'deleteJournalBy. - Query = ' . $this->db->last_query());

            if ($this->db->affected_rows() == '1') {
                log_message('debug', 'deleteJournalBy. - DELETED ');
                $this->db->trans_commit();
                return TRUE;
            } else {
                log_message('debug', 'deleteJournalBy. - FALIED TO DELETE ');
                $this->db->trans_rollback();
                return FALSE;
            }
        } finally {
            $this->load->model('system_journal_model');
            $this->system_journal_model->calculate_system_journal();
        }
    }

    /*
     * Retrieve all journals
     */

    public function get_all_journals_for_report() {
        $result = $this->db->query("SELECT 
                                       CONCAT('JRN', LPAD(je_linked_journal_id, '5', '0')) AS journal_id,
                                       je_amount as amount,
                                       je_transaction_type as transaction_type,
                                       journal_narration,
                                       journal_date
                                    FROM
                                        journal_entries
                                    LEFT JOIN
                                        journal ON journal.journal_id = journal_entries.je_linked_journal_id;");
        return $result->result_array();
    }
    
    /*
     * Retrieve all journals
     */

    public function get_all_journals() {
        $result = $this->db->query('SELECT 
                                        *
                                    FROM
                                        journal
                                    LEFT JOIN
                                        employee ON employee.employee_id = journal.journal_record_created_by;');
        return $result->result_array();
    }

    /**
     * For journal landing page
     * @return type
     */
    public function get_all_journals_with_entry_details() {
        $result = $this->db->query('SELECT 
                                        journal_id,
                                        journal_date,
                                        journal_narration,
                                        account.coa_id AS account_coa_id,
                                        account.coa_account_name AS account_name,
                                        possible_parent_account.coa_id AS possible_parent_coa_id,
                                        possible_parent_account.coa_account_name AS possible_parent_name,
                                        je_entry_id,
                                        je_linked_coa_id,
                                        je_description,
                                        je_amount,
                                        je_transaction_type,
                                        (SELECT 
                                                COUNT(*)
                                            FROM
                                                journal_entries
                                            WHERE
                                                je_linked_journal_id = journal.journal_id) AS journal_row_count
                                    FROM
                                        journal_entries
                                            LEFT JOIN
                                        journal ON journal.journal_id = journal_entries.je_linked_journal_id
                                            LEFT JOIN
                                        chart_of_accounts AS account ON account.coa_id = journal_entries.je_linked_coa_id
                                            LEFT JOIN
                                        chart_of_accounts AS possible_parent_account ON possible_parent_account.coa_id = account.coa_subaccount_of_id
                                    ORDER BY journal_id');
        return $result->result_array();
    }

    /**
     * Get entries for an journal
     */
    public function get_journal_entries($journal_id) {
        //$query = $this->db->get_where('journal_product_entries', array('je_linked_journal_id' => $journal_id));
        $query = 'SELECT 
                    je_entry_id,
                    je_linked_coa_id,
                    coa_subaccount_of_id,
                    je_description,
                    je_amount,
                    je_linked_broker_id,
                    je_linked_company_id,
                    je_linked_employee_id,
                    je_linked_transporter_id,
                    je_transaction_type
                FROM
                    journal_entries
                        LEFT JOIN
                    chart_of_accounts ON chart_of_accounts.coa_id = journal_entries.je_linked_coa_id WHERE je_linked_journal_id = ' . $journal_id;
        $result = $this->db->query($query);
        return $result->result_array();
    }

    /*
     * Get journal using journal_id
     */

    public function get_journal_with_id($journal_id) {
        $query = $this->db->get_where('journal', array('journal_id' => $journal_id));
        return $query->row_array();
    }

    /*
     * Add new journal

     */

    public function save_journal($journal_id, $journal, $journal_entries) {
        log_message('debug', 'save_journal. - $journal = ' . print_r($journal, 1));

        try {
            $this->db->query('SET time_zone = "+05:30";');
            date_default_timezone_set('Asia/Kolkata');
            /* add time component to date if journal date is today.
             * else
             * add 23:59:59 if the journal was created on a later date
             * This timestamp is to determine entry sequence for FIFO inventory
             * To be done for Journal, purchase, credit note, debit note, inventory adjustment
             */
            if (date('Y-m-d', strtotime($journal['journal_date'])) == date("Y-m-d")) {
                $journal['journal_date'] = date("Y-m-d H:i:sa");
            } else {
                $journal['journal_date'] = date("Y-m-d 23:59:59", strtotime($journal['journal_date']));
            }

            $this->db->trans_begin();

            if ($journal_id == null) {
                $this->db->insert('journal', $journal);
                $journal_id = $this->db->insert_id();
            } else {
                $this->db->where('journal_id', $journal_id);
                $this->db->update('journal', $journal);

                $this->db->where('je_linked_journal_id', $journal_id);
                $this->db->delete('journal_entries');
            }

            $count = 0;
            if ($journal_id > 0) {
                $je['je_linked_journal_id'] = $journal_id;
                $row = 1;
                foreach ($journal_entries as $journal_entry) {
                    //$je['je_entry_id'] = $journal_entry[0];
                    $je['je_entry_id'] = $row;
                    $row++;
                    $je['je_linked_coa_id'] = $journal_entry[1];
                    $je['je_description'] = $journal_entry[2];

                    $je['je_linked_broker_id'] = NULL;
                    $je['je_linked_company_id'] = NULL;
                    $je['je_linked_employee_id'] = NULL;
                    $je['je_linked_transporter_id'] = NULL;
                    
                    if(!empty($journal_entry[5])){
                        $linked_entity = explode("_", $journal_entry[5]);
                        if($linked_entity[0] == 'broker'){
                            $je['je_linked_broker_id'] = $linked_entity[1];
                        } else if($linked_entity[0] == 'company'){
                            $je['je_linked_company_id'] = $linked_entity[1];
                        } else if($linked_entity[0] == 'employee'){
                            $je['je_linked_employee_id'] = $linked_entity[1];
                        } else if($linked_entity[0] == 'transporter'){
                            $je['je_linked_transporter_id'] = $linked_entity[1];
                        } else {
                            throw new Exception('Unknown linked entity');
                        }
                    } 
                    
                    if (isset($journal_entry[3])) {
                        $je['je_transaction_type'] = 'debit';
                        $je['je_amount'] = $journal_entry[3];
                    } else if (isset($journal_entry[4])) {
                        $je['je_transaction_type'] = 'credit';
                        $je['je_amount'] = $journal_entry[4];
                    } else {
                        throw new Exception("Amount for journal entry not found");
                    }
                    if ($je['je_amount'] > 0)
                        $this->db->insert('journal_entries', $je);

                    if ($this->db->insert_id() > 0) {
                        $count++;
                    }
                }
            } else {
                $response['result'] = $this->db->error();
                $response['query'] = $this->db->last_query();
            }


            if ($count > 0) {
                $this->db->trans_commit();
                $response['result'] = "success";
            } else {
                log_message('debug', 'save_journal. ROLLBACK. ' . print_r($response));
                $this->db->trans_rollback();
            }

            log_message('debug', 'save_journal. - Query = ' . $this->db->last_query());
            return $response;
        } finally {
            $this->load->model('system_journal_model');
            $this->system_journal_model->calculate_system_journal();
        }
    }

}
