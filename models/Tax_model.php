<?php

class Tax_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }
    
    public $GSTINFORMAT_REGEX = "/[0-9]{2}[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9A-Za-z]{1}[Z]{1}[0-9a-zA-Z]{1}/";
    public $GSTN_CODEPOINT_CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	/*
	* THIS IS PARENT METHOT TO CALL 
	* YOU JUST NEED TO PASS GST NUMBER IN IT
	* boolen check_my_gst_number ( String $param1)
	* Return boolen 1 if it is valid else 0 
	*/
    public function check_my_gst_number($param1){
    	if($this->validategstin($param1)){
            return(1);
    	} else {
            return(0);
    	}
    }
    
    public function validategstin($gstNumber){
        if(preg_match($this->GSTINFORMAT_REGEX, $gstNumber)){
            if($this->gst_error_check($gstNumber) == $gstNumber){
                    return(1);
            }else{
                    return(0);
            }
        } else {
            return(0);
        }
    }
    	
    
    public function gst_error_check($gst_number){
    	$input =  str_split($gst_number);
    	$inputChars = $input;
    	unset($inputChars[14]); 
    	
    	$factor = 2;
		$sum = 0;
		$checkCodePoint = 0;
		$cpChars = str_split($this->GSTN_CODEPOINT_CHARS);
		$mod = count($cpChars);
		for ($i = count($inputChars) - 1; $i >= 0; $i--) {
				$codePoint = -1;
				for ($j = 0; $j < count($cpChars); $j++) {
					if ($cpChars[$j] == $inputChars[$i]) {
						$codePoint = $j;
					}
				}
				
				$digit = $factor * $codePoint;
				$factor = ($factor == 2) ? 1 : 2;
				$x =  ($digit % $mod).'<br>';
				#echo $x;
				$digit = (int) ($digit / $mod) + (int) ($digit % $mod);
				$sum += $digit;
			}
			$checkCodePoint = ($mod - ($sum % $mod)) % $mod;
			$inputChars = implode('', $inputChars);
    	return($inputChars.$cpChars[$checkCodePoint]);
    }

    /*
     * Retrieve all intrastate tax
     */

    public function get_intrastate_taxes() {
        $query = $this->db->get_where('tax', array('tax_sale_condition' => 'intrastate'));
        return $query->result_array();
    }

    /**
      Fetch all taxes
     */
    public function get_all_taxes() {
        $query = $this->db->get('tax');
        return $query->result_array();
    }

    /*
     * Retrieve all interstate tax
     */

    public function get_interstate_taxes() {
        $query = $this->db->get_where('tax', array('tax_sale_condition' => 'interstate'));
        return $query->result_array();
    }

    /**
      Get tax using ID
     */
    public function get_tax_with_id($tax_id) {
        $query = $this->db->get_where('tax', array('tax_id' => $tax_id));
        return $query->row_array();
    }

}
