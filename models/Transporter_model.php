<?php

class Transporter_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    /**
     * Delete transporter using its ID
     */
    function deleteTransporterById($id) {
        log_message('debug', 'deleteTransporterBy. - $id = ' . print_r($id, 1));

        $this->db->where('transporter_id', $id);
        $this->db->delete('transporter');

        log_message('debug', 'deleteTransporterBy. - Query = ' . $this->db->last_query());

        if ($this->db->affected_rows() == '1') {
            log_message('debug', 'deleteTransporterBy. - Transporter Deleted ');
            return TRUE;
        } else {
            log_message('debug', 'deleteTransporterBy. - Failed to delete Transporter ');
            return FALSE;
        }
    }

    /**
     * Get number of transporters in table
     * */
    public function get_transporter_count() {
        $query_string = 'SELECT COUNT(transporter_id) AS count from transporter';
        $result = $this->db->query($query_string);
        return $result->row_array();
    }

    /**
     * Get all transporters along with location
     * */
    public function get_all_transporters() {
        $result = $this->db->query('SELECT 
										*
										FROM
										transporter
										LEFT JOIN
										employee ON employee.employee_id = transporter.transporter_record_created_by
										LEFT JOIN
										location
										ON
										transporter.transporter_location_id = location.location_id;');
        return $result->result_array();
    }
    
        /**
     * Get all transporters along with location
     * */
    public function get_all_transporters_for_report() {
        $result = $this->db->query('SELECT 
                                        CONCAT("TRN", LPAD(transporter_id, "5", "0")) AS transporter_id,
                                        transporter_name,
                                        transporter_billing_address,
                                        transporter_correspondence_address as correspondence_address,
                                        transporter_area as area,
                                        transporter_pincode as pincode,
                                        transporter_email_address as email_address,
                                        transporter_contact_person_name as contact_person,
                                        transporter_contact_number,
                                        transporter_pan_number,
                                        transporter_gst_number
                                    FROM
                                        transporter
                                    LEFT JOIN
                                        location
                                    ON
                                        transporter.transporter_location_id = location.location_id;');
        return $result->result_array();
    }

    /**
     * Get a transporter using ID
     */
    public function get_transporter_with_id($id) {
        $query = $this->db->get_where('transporter', array('transporter_id' => $id));
        return $query->row_array();
    }

    /**
     * Save a transporter
     */
    public function save_transporter($data, $transporter_id = NULL) {
        log_message('debug', 'save_transporter. - $id = ' . print_r($transporter_id, 1) . '$data = ' . print_r($data, 1));
        if ($transporter_id == NULL) {
            $this->db->query('SET time_zone = "+05:30";');
            $transporter_id = $this->db->insert('transporter', $data);
        } else {
            $this->db->where('transporter_id', $transporter_id);
            $this->db->update('transporter', $data);
        }

        $response['query'] = $this->db->last_query();
        log_message('debug', 'save_transporter. - response = ' . print_r($response, 1));
        return $response;
    }

}
