<?php

class Unique_quantity_code_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }


    /**
      Fetch all codes
     */
    public function get_all_uqc() {
        $query = $this->db->get('unique_quantity_code');
        return $query->result_array();
    }
    
    /**
     * 
     * @return type
     */
    public function get_all_active_uqc() {
        $query = $this->db->get_where('unique_quantity_code', array('uqc_active' => 'active'));
        return $query->result_array();
    }

    /**
     * 
     * @param type $uqc_id
     * @return type
     */
    public function get_uqc_with_id($uqc_id) {
        $query = $this->db->get_where('unique_quantity_code', array('uqc_id' => $uqc_id));
        return $query->row_array();
    }
}
