<?php

class Inventory_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    /**
     * For inventory overview page
     * @return type
     */
    public function get_overview_for_report($product_id = NULL) {
        $this->evaluate_inventory_figures();

        if ($product_id == NULL) {
            $query = 'SELECT 
                    product_id,
                    product_name,
                    product_code,
                    (Select ie_it_product_rate from inventory_entry as c WHERE a.ie_it_product_id = c.ie_it_product_id ORDER BY c.ie_id DESC LIMIT 1) as rate_per_unit,
                    (Select uqc_text from unique_quantity_code where unique_quantity_code.uqc_id = product.product_uqc_id) as uqc_text,
                    hsn_code,
                    SUM(ie_it_product_quantity) as product_quantity,
                    (Select ie_inventory_asset_value from inventory_entry as b WHERE a.ie_it_product_id = b.ie_it_product_id ORDER BY b.ie_id DESC LIMIT 1) AS inventory_asset_value
                FROM
                    inventory_entry as a
                        LEFT JOIN
                    product ON a.ie_it_product_id = product.product_id
                    where product_type = "goods"';
        
            $query .= ' GROUP BY ie_it_product_id';
        } else {
            $query = "Select 
                        ie_it_document_id_text as document,
                        ie_it_product_rate as product_rate,
                        ie_it_product_quantity as quantity,
                        ie_stock_on_hand as stock_on_hand,
                        ie_inventory_asset_value as inventory_asset_value,
                        ie_fifo_reference as fifo_reference,
                        ie_it_entry_datetime as date
                     from inventory_entry";
            $query .= ' where ie_it_product_id = ' . $product_id;
            $query .= '  ORDER BY ie_id ASC';   
        }

        

        $result = $this->db->query($query);
        return $result->result_array();
    }
    
    /**
     * For inventory overview page
     * @return type
     */
    public function get_overview($product_id = NULL) {
        $this->evaluate_inventory_figures();

        $query = 'SELECT 
                    product_id,
                    product_name,
                    product_source,
                    transaction_type,
                    product_image_path,
                    product_code,
                    (Select ie_it_product_rate from inventory_entry as c WHERE a.ie_it_product_id = c.ie_it_product_id ORDER BY c.ie_id DESC LIMIT 1) as rate_per_unit,
                    (Select uqc_text from unique_quantity_code where unique_quantity_code.uqc_id = product.product_uqc_id) as uqc_text,
                    hsn_code,
                    SUM(ie_it_product_quantity) as product_quantity,
                    (Select SUM(moe_linked_product_quantity) 
                        FROM 
                    manufacturing_order_entries 
                        LEFT JOIN
                    manufacturing_order ON manufacturing_order_entries.moe_linked_manufacturing_order_id = manufacturing_order.manufacturing_order_id 
                        WHERE 
                    manufacturing_order.manufacturing_order_status = "confirm" and moe_linked_product_id = product_id) as work_in_progress,
                    (Select ie_inventory_asset_value from inventory_entry as b WHERE a.ie_it_product_id = b.ie_it_product_id ORDER BY b.ie_id DESC LIMIT 1) AS inventory_asset_value
                FROM
                    inventory_entry as a
                        LEFT JOIN
                    product ON a.ie_it_product_id = product.product_id
                    where product_type = "goods"';
        if ($product_id != NULL) {
            $query .= ' AND ie_it_product_id = ' . $product_id;
        }

        $query .= ' GROUP BY ie_it_product_id';

        $result = $this->db->query($query);
        return $result->result_array();
    }

    /**
     * Get all inventory transactions
     */
    public function get_inventory_transactions($product_id = null) {
        //$this->evaluate_inventory_figures();
        $query = 'SELECT 
                    *
                FROM
                    inventory_entry
                        LEFT JOIN
                    product ON inventory_entry.ie_it_product_id = product.product_id
                        LEFT JOIN
                    unique_quantity_code ON unique_quantity_code.uqc_id = product_uqc_id';

        if ($product_id != null) {
            $query .= ' where ie_it_product_id = ' . $product_id;
        }

        $query .= '  ORDER BY ie_id ASC';

        $result = $this->db->query($query);
        return $result->result_array();
    }

    /**
     * Fetch opening stock for a product
     * @param type $product_id
     * @return type
     */
    public function get_product_opening_balance($product_id) {
        $result = $this->db->query('SELECT * FROM product_opening_balance where pob_product_id = ' . $product_id);
        return $result->row_array();
    }

    /**
     * 
     * @param type $data
     * @throws Exception
     */
    public function update_opening_balance($data) {
        try {
            $pobs = $data['pob'];

            $opening_stock_value = 0;
            foreach ($pobs as $product_id => $pob_data) {

                $pob = array();
                $pob['pob_opening_balance'] = $pob_data[0];
                $pob['pob_unit_rate'] = $pob_data[2];
                if (isset($pob_data[0])) {
                    $pob['pob_opening_balance'] = $pob_data[0];
                } else {
                    $pob['pob_opening_balance'] = 0;
                }
                if (isset($pob_data[2])) {
                    $pob['pob_unit_rate'] = $pob_data[2];
                } else {
                    $pob['pob_unit_rate'] = 0;
                }

                $opening_stock_value += $pob['pob_opening_balance'] * $pob['pob_unit_rate'];

                //$pob['pob_date'] = $pob_date;

                $this->db->where('pob_product_id', $product_id);
                $q = $this->db->get('product_opening_balance');

                if ($q->num_rows() > 0) {
                    $this->db->where('pob_product_id', $product_id);
                    $this->db->update('product_opening_balance', $pob);
                } else {
                    $pob['pob_product_id '] = $product_id;
                    $this->db->insert('product_opening_balance', $pob);
                }
            }

            $this->update_adjustment_account();
            
        } finally {
            $this->evaluate_inventory_figures();
        }
    }

    /**
     * 
     * @param type $opening_stock_value
     */
    public function update_adjustment_account() {
        try {
            
            $opening_stock_value = $this->db->query('select SUM(pob_opening_balance * pob_unit_rate) as balance from product_opening_balance')->row_array()['balance'];
            
            $as_query = 'Select * from accounting_setting WHERE as_for = "coa_opening_balance" && as_field_name = "inventory"';
            $as_result = $this->db->query($as_query)->row_array();

            $inventory_account_coa_id = $as_result['as_debit_account_coa_id'];

            $coa = array();
            $coa['coa_opening_balance'] = $opening_stock_value;
            $coa['coa_opening_balance_transaction_type'] = 'debit';
            $this->db->where('coa_id', $inventory_account_coa_id);
            $this->db->update('chart_of_accounts', $coa);

            //update opening adjustment
            // 1. Set opening balance adjustment as 0
            // 2. Find balance amount
            //3. Update opening balance adjustment

            $ob_adj_query = 'Select * from accounting_setting WHERE as_for = "coa_opening_balance" && as_field_name = "adjustment"';
            $ob_adj_result = $this->db->query($ob_adj_query)->row_array();
            $adjustment_account_coa_id = $ob_adj_result['as_debit_account_coa_id'];


            //1.
            $adj_zero_coa = array();
            $adj_zero_coa['coa_opening_balance'] = 0;
            $adj_zero_coa['coa_opening_balance_transaction_type'] = 'debit';
            $this->db->where('coa_id', $adjustment_account_coa_id);
            $this->db->update('chart_of_accounts', $adj_zero_coa);

            //2.
            $coa_balance_query = "SELECT DISTINCT
                                IFNULL((SELECT 
                                        SUM(debit_balance_table.coa_opening_balance) AS debit_balance
                                    FROM
                                        chart_of_accounts AS debit_balance_table
                                    WHERE
                                        coa_opening_balance_transaction_type = 'debit'),
                                0) - IFNULL((SELECT 
                                        SUM(credit_balance_table.coa_opening_balance)
                                    FROM
                                        chart_of_accounts AS credit_balance_table
                                    WHERE
                                        coa_opening_balance_transaction_type = 'credit'),
                                0) AS balance
                            FROM
                                chart_of_accounts";

            $coa_balance = $this->db->query($coa_balance_query)->row_array()['balance'];
            
            $broker_balance_query = "SELECT DISTINCT
                                IFNULL((SELECT 
                                        SUM(debit_balance_table.broker_opening_balance) AS debit_balance
                                    FROM
                                        broker AS debit_balance_table
                                    WHERE
                                        broker_opening_balance_transaction_type = 'debit'),
                                0) - IFNULL((SELECT 
                                        SUM(credit_balance_table.broker_opening_balance)
                                    FROM
                                        broker AS credit_balance_table
                                    WHERE
                                        broker_opening_balance_transaction_type = 'credit'),
                                0) AS balance
                            FROM
                                broker";

            $broker_balance = $this->db->query($broker_balance_query)->row_array()['balance'];
            
            $company_balance_query = "SELECT DISTINCT
                                IFNULL((SELECT 
                                        SUM(debit_balance_table.company_opening_balance) AS debit_balance
                                    FROM
                                        company AS debit_balance_table
                                    WHERE
                                        company_opening_balance_transaction_type = 'debit'),
                                0) - IFNULL((SELECT 
                                        SUM(credit_balance_table.company_opening_balance)
                                    FROM
                                        company AS credit_balance_table
                                    WHERE
                                        company_opening_balance_transaction_type = 'credit'),
                                0) AS balance
                            FROM
                                company";

            $company_balance = $this->db->query($company_balance_query)->row_array()['balance'];
            
            $employee_balance_query = "SELECT DISTINCT
                                IFNULL((SELECT 
                                        SUM(debit_balance_table.employee_opening_balance) AS debit_balance
                                    FROM
                                        employee AS debit_balance_table
                                    WHERE
                                        employee_opening_balance_transaction_type = 'debit'),
                                0) - IFNULL((SELECT 
                                        SUM(credit_balance_table.employee_opening_balance)
                                    FROM
                                        employee AS credit_balance_table
                                    WHERE
                                        employee_opening_balance_transaction_type = 'credit'),
                                0) AS balance
                            FROM
                                employee";

            $employee_balance = $this->db->query($employee_balance_query)->row_array()['balance'];
            
            $transporter_balance_query = "SELECT DISTINCT
                                IFNULL((SELECT 
                                        SUM(debit_balance_table.transporter_opening_balance) AS debit_balance
                                    FROM
                                        transporter AS debit_balance_table
                                    WHERE
                                        transporter_opening_balance_transaction_type = 'debit'),
                                0) - IFNULL((SELECT 
                                        SUM(credit_balance_table.transporter_opening_balance)
                                    FROM
                                        transporter AS credit_balance_table
                                    WHERE
                                        transporter_opening_balance_transaction_type = 'credit'),
                                0) AS balance
                            FROM
                                transporter";

            $transporter_balance = $this->db->query($transporter_balance_query)->row_array()['balance'];
            
            $net_balance = $coa_balance + $broker_balance + $company_balance + $employee_balance + $transporter_balance;

            //3.
            $adj_coa = array();
            $adj_coa['coa_opening_balance'] = abs($net_balance);

            if ($net_balance > 0) {
                $adj_coa['coa_opening_balance_transaction_type'] = 'credit';
            } else {
                $adj_coa['coa_opening_balance_transaction_type'] = 'debit';
            }

            $this->db->where('coa_id', $adjustment_account_coa_id);
            $this->db->update('chart_of_accounts', $adj_coa);
        } finally {
            $this->load->model('inventory_model');
            $this->inventory_model->evaluate_inventory_figures();
        }
    }

    /**
     * Fetch opening stock for a product
     * @param type $product_id
     * @return type
     */
    public function get_product_opening_balance_for_all_goods() {
        $result = $this->db->query('SELECT 
                                    *
                                FROM
                                    product
                                        LEFT JOIN
                                    product_opening_balance ON product.product_id = product_opening_balance.pob_product_id
                                    LEFT JOIN
                                        unique_quantity_code 
                                    ON
                                        unique_quantity_code.uqc_id = product.product_uqc_id
                                 where product_type = "goods"');
        return $result->result_array();
    }

    /**
     * Get the first entry from inventory_transactions view upto which data in inventory_entry table matches data from view inventory_transactions
     * Returns empty array if all product id, quantity, rate and entry datetime of all rows in both table matches else returns the datetime
     */
    public function is_inventory_upto_date() {
        $query = 'SELECT 
                    it_entry_datetime
                FROM
                    inventory_transactions
                        LEFT JOIN
                    inventory_entry ON (inventory_entry.ie_it_product_id = inventory_transactions.it_product_id
                        AND inventory_entry.ie_it_entry_datetime = inventory_transactions.it_entry_datetime
                        
                        /*AND inventory_entry.ie_it_product_quantity = inventory_transactions.it_product_quantity
                        AND inventory_entry.ie_it_product_rate = inventory_transactions.it_product_rate*/
                        

                        AND 
                        (inventory_entry.ie_invoice_id = inventory_transactions.it_document_id
                        OR inventory_entry.ie_purchase_id = inventory_transactions.it_document_id
                        OR inventory_entry.ie_credit_note_id = inventory_transactions.it_document_id
                        OR inventory_entry.ie_debit_note_id = inventory_transactions.it_document_id
                        OR inventory_entry.ie_pob_id = inventory_transactions.it_document_id)
                        AND 
                        (inventory_entry.ie_ipe_id = inventory_transactions.it_entry_id
                        OR inventory_entry.ie_pe_id = inventory_transactions.it_entry_id
                        OR inventory_entry.ie_cne_id = inventory_transactions.it_entry_id
                        OR inventory_entry.ie_dne_id = inventory_transactions.it_entry_id
                        OR inventory_entry.ie_pob_id = inventory_transactions.it_document_id))/* no entry id in case of pob */
                WHERE
                    inventory_entry.ie_id IS NULL
                ORDER BY it_entry_datetime ASC
                LIMIT 1';
        $result = $this->db->query($query);
        $inventory_entries = $result->row_array();

        if (sizeOf($inventory_entries) == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Calculate inventory figures based on inventory transaction view - ver 2
     */
    public function evaluate_inventory_figures() {
        try {

            if ($this->is_inventory_upto_date()) {
                //return; 
            }
            /**
             * Creating FIFO IN Queue
             */
            $IN_queue_query = 'Select * from inventory_transactions where it_document_type = "purchase"  '
                    . '|| it_document_type = "product_creation" || it_document_type = "manufacturing_order_finished_products"';
            $IN_inventory_transactions = $this->db->query($IN_queue_query)->result_array();
            //product added in collectively is a batch. Example - 10 products added for purchase invoice PUR00045 is 1 batch. 50 products for credit note is another batch.

            $invoice_entry_batch_map = array();
            $purchase_entry_batch_map = array();

            $fifo_in_product_batch_map = array();    // to sort this IN queue product wise
            foreach ($IN_inventory_transactions as $IN_inventory_transaction) {
                $product_id = $IN_inventory_transaction['it_product_id'];
                if (!isset($fifo_in_product_batch_map[$product_id])) { //create product batch array for current product ID if it does not exist
                    $product_batch_array = array();
                    $fifo_in_product_batch_map[$product_id] = $product_batch_array;
                }

                $product_batch_array = $fifo_in_product_batch_map[$product_id];  //retrieve batch for current product ID

                $batch = array();    // current batch
                $batch['quantity'] = $IN_inventory_transaction['it_product_quantity'];
                $batch['rate_per_unit'] = $IN_inventory_transaction['it_product_rate'];
                $batch['document_id_text'] = $IN_inventory_transaction['it_document_id_text'];
                $batch['entry_id'] = $IN_inventory_transaction['it_entry_id'];
                $batch['entry_datetime'] = $IN_inventory_transaction['it_entry_datetime'];

                array_push($product_batch_array, $batch);    //push current batch to product batch array
                $fifo_in_product_batch_map[$product_id] = $product_batch_array;

                if ($IN_inventory_transaction['it_document_type'] == 'purchase') {
                    /**
                     * Store FIFO IN batch for current purchase entry ( pe_id ). To be used in case debit note is issued against this purchase in future. 
                     * The stored rate will be used for FIFO Out calculations
                     */
                    $purchase_entry_batch_map[$IN_inventory_transaction['it_entry_id']] = (sizeof($product_batch_array) - 1);
                }
            }

            /**
             * Delete all entries
             */
            $delete_query = 'Delete from inventory_entry WHERE ie_id >= 1';
            $this->db->query($delete_query);

            /**
             * Reset auto increment
             */
            $this->db->query('Alter table inventory_entry AUTO_INCREMENT = 1');

            /**
             * Populate
             */
            $query = 'Select * from inventory_transactions';
            $result = $this->db->query($query);
            $inventory_transactions = $result->result_array();

            $product_wise_stock = array();

            $product_inventory_asset_value_map = array();
            foreach ($inventory_transactions as $inventory_transaction) {
                $inventory_entry = array();
                $inventory_entry['ie_it_document_id_text'] = $inventory_transaction['it_document_id_text'];

                $product_id = $inventory_transaction['it_product_id'];
                $inventory_entry['ie_it_product_id'] = $product_id;

                $inventory_entry['ie_it_entry_datetime'] = $inventory_transaction['it_entry_datetime'];

                $document_type = $inventory_transaction['it_document_type'];
                if ($document_type == 'purchase') {
                    $inventory_entry['ie_purchase_id'] = $inventory_transaction['it_document_id'];
                    $inventory_entry['ie_pe_id'] = $inventory_transaction['it_entry_id'];
                } else if ($document_type == 'debit_note') {
                    $inventory_entry['ie_debit_note_id'] = $inventory_transaction['it_document_id'];
                    $inventory_entry['ie_dne_id'] = $inventory_transaction['it_entry_id'];
                } else if ($document_type == 'invoice') {
                    $inventory_entry['ie_invoice_id'] = $inventory_transaction['it_document_id'];
                    $inventory_entry['ie_ipe_id'] = $inventory_transaction['it_entry_id'];
                } else if ($document_type == 'credit_note') {
                    $inventory_entry['ie_credit_note_id'] = $inventory_transaction['it_document_id'];
                    $inventory_entry['ie_cne_id'] = $inventory_transaction['it_entry_id'];
                } else if ($document_type == 'inventory_adjustment') {
                    $inventory_entry['ie_inventory_adjustment_id'] = $inventory_transaction['it_document_id'];
                    // no entry id in case of inventory adjustment
                } else if ($document_type == 'product_creation') {
                    $inventory_entry['ie_pob_id'] = $inventory_transaction['it_document_id'];
                    // no entry id in case of opening balance
                } else if ($document_type == 'manufacturing_order_raw_materials') {
                    $inventory_entry['ie_manufacturing_order_id'] = $inventory_transaction['it_document_id'];
                    $inventory_entry['ie_moe_id'] = $inventory_transaction['it_entry_id'];
                } else if ($document_type == 'manufacturing_order_finished_products') {
                    $inventory_entry['ie_manufacturing_order_id'] = $inventory_transaction['it_document_id'];
                    //no entry id for finished products
                }

                $product_batch_array = array();
                if (isset($fifo_in_product_batch_map[$product_id])) {
                    $product_batch_array = $fifo_in_product_batch_map[$product_id];  //retrieve batch for current product ID created in step 1
                }


                if (($document_type == 'invoice' 
                        || $document_type == 'debit_note' 
                        || $document_type == 'inventory_adjustment'
                        || $document_type == 'manufacturing_order_raw_materials' ) 
                        && sizeof($product_batch_array) > 0) {
                    $current_rate = $product_batch_array[(sizeof($product_batch_array) - 1)]['rate_per_unit'];  //default is last rate
                } else if($document_type == 'manufacturing_order_finished_products') {
                    $current_rate_query = 'Select ABS(SUM(ie_it_product_quantity * ie_it_product_rate)) as cogs '
                            . 'from inventory_entry where ie_manufacturing_order_id = '.$inventory_transaction['it_document_id']. 
                            ' AND ie_moe_id IS NOT NULL';
                    $current_rate = ($this->db->query($current_rate_query)->row_array()['cogs'])/$inventory_transaction['it_product_quantity'];
                    
                    //FOLLOWING 2 LINES ARE EXPERIMENTAL
                    $this->db->query('Update inventory_entry SET ie_it_product_rate = '.$current_rate.' WHERE LEFT(ie_fifo_reference, 8) LIKE "'.$inventory_transaction['it_document_id_text'].'"');
                    //$this->db->query('Update inventory_entry SET ie_inventory_asset_value = (ie_it_product_rate * ie_stock_on_hand) WHERE LEFT(ie_fifo_reference, 8) LIKE "'.$inventory_transaction['it_document_id_text'].'"');
                    
                    foreach($product_batch_array as $batch_number => $product_batch){
                        if($product_batch['document_id_text'] == $inventory_transaction['it_document_id_text']){
                            $product_batch['rate_per_unit'] = $current_rate;
                            $product_batch_array[$batch_number] = $product_batch;
                            $fifo_in_product_batch_map[$product_id] = $product_batch_array;
                        }
                    }
                    //throw new Exception($current_rate_query);
                } else {
                    $current_rate = $inventory_transaction['it_product_rate'];
                }

                $transaction_quantity = $inventory_transaction['it_product_quantity'];
                if ($document_type == 'inventory_adjustment' && isset($transaction_quantity)) {
                    $transaction_quantity = $transaction_quantity - $product_wise_stock[$product_id];
                }

                $batch_counter = -1;
                $stresstest = 0;
                $inventory_adjustment_valuation_reversal = 0;
                do {
                    if ($document_type == 'credit_note') {

                        $batch_data_array = array();
                        if (isset($invoice_entry_batch_map[$inventory_transaction['it_linked_document_entry_id']])) {
                            $batch_data_array = $invoice_entry_batch_map[$inventory_transaction['it_linked_document_entry_id']];
                        }
                        $inventory_entry['ie_fifo_reference'] = 'INV' . str_pad($inventory_transaction['it_linked_document_id'], 5, "0", STR_PAD_LEFT) . " > ";
                        $available_quantity = 0;
                        if (sizeof($batch_data_array) > 0) {
                            $batch_counter = $batch_data_array[0]['batch_count'];
                            $available_quantity = $batch_data_array[0]['quantity'];
                            if ($available_quantity > abs($transaction_quantity)) {
                                $batch_data_array[0]['quantity'] = $available_quantity - abs($transaction_quantity);
                                $inventory_entry['ie_it_product_quantity'] = $transaction_quantity;
                                $transaction_quantity = 0;
                            } else {
                                $batch_data_array[0]['quantity'] = 0;
                                $inventory_entry['ie_it_product_quantity'] = $available_quantity;
                                $transaction_quantity = (abs($transaction_quantity) - $available_quantity);
                                array_shift($batch_data_array);
                            }
                            $invoice_entry_batch_map[$inventory_transaction['it_linked_document_entry_id']] = $batch_data_array;
                        } else {
                            $inventory_entry['ie_it_product_quantity'] = $transaction_quantity;
                            $transaction_quantity = 0;
                        }
                        if ($batch_counter < 0) {
                            //throw new Exception('$batch_counter = '.$batch_counter.' '.$product_id.' '.print_r($batch_data_array). ' size = '.sizeof($batch_data_array). ' Credit note entry = '. $inventory_transaction['it_linked_document_entry_id']);
                            $batch_counter = sizeof($product_batch_array) - 1;
                        }
                        //$product_batch_array[$batch_counter]['quantity'] = $product_batch_array[$batch_counter]['quantity'] + $inventory_transaction['it_product_quantity'];//changed on 29/01/18
                        $product_batch_array[$batch_counter]['quantity'] = $product_batch_array[$batch_counter]['quantity'] + $inventory_entry['ie_it_product_quantity'];
                        $fifo_in_product_batch_map[$product_id] = $product_batch_array;
                        $current_rate = $product_batch_array[$batch_counter]['rate_per_unit'];

                        $inventory_entry['ie_fifo_reference'] = $inventory_entry['ie_fifo_reference'] . $product_batch_array[$batch_counter]['document_id_text'];
                        $inventory_entry['ie_it_product_rate'] = $current_rate;
                    } else {//if ($document_type == 'invoice' || $document_type == 'debit_note') {    //special case in which FIFO sequence needs to be considered
                        //find first non - empty batch
                        if (sizeof($product_batch_array) > 0
                                //&& (!isset($inventory_transaction['it_product_quantity']) || $inventory_transaction['it_product_quantity'] < 0)) {
                                && (!isset($transaction_quantity) || $transaction_quantity < 0 || abs($inventory_adjustment_valuation_reversal) > 0)) {
                            $batch_counter = 0;
                            while ($product_batch_array[$batch_counter]['quantity'] <= 0 && $batch_counter < sizeof($product_batch_array)) {
                                $batch_counter++;
                                if ($batch_counter == sizeof($product_batch_array)) {
                                    $batch_counter = -1;
                                    break;
                                }
                                if ($document_type == 'inventory_adjustment') {
                                    if (strtotime($product_batch_array[$batch_counter]['entry_datetime']) > strtotime($inventory_transaction['it_entry_datetime'])) {
                                        $batch_counter = -1;
                                        break;
                                    }
                                }
                            }
                        }

                        if ($document_type == 'debit_note') {
                            /**
                             * Retrieve FIFO batch at which product came IN from FIFO queue
                             */
                            if (isset($purchase_entry_batch_map[$inventory_transaction['it_linked_document_entry_id']])) {
                                $batch_counter = $purchase_entry_batch_map[$inventory_transaction['it_linked_document_entry_id']];
                            }
                        }

                        if ($document_type == 'inventory_adjustment') {
                            $ia_quantity = $inventory_transaction['it_product_quantity'];
                            $ia_value = $inventory_transaction['it_product_rate'];

                            if ($ia_quantity != null && $ia_value == null) { //adjustment by quantity
                                //do nothing
                            } else if ($ia_quantity == null && $ia_value != null) {    //adjustment by value
                                if ($batch_counter >= 0) {//exhaust all batches for current product
                                    $transaction_quantity = $product_batch_array[$batch_counter]['quantity'] * (-1);
                                    $inventory_adjustment_valuation_reversal = $inventory_adjustment_valuation_reversal + $transaction_quantity;
                                } else {
                                    // means all batches have been exhausted. now reverse
                                    $transaction_quantity = $inventory_adjustment_valuation_reversal * (-1);
                                    if ($transaction_quantity != 0) {
                                        $current_rate = ($ia_value / $transaction_quantity);
                                    }
                                    $inventory_adjustment_valuation_reversal = 0;
                                }
                            }
                            if ($transaction_quantity > 0) {
                                $batch = array();    // current batch
                                $batch['quantity'] = $transaction_quantity;
                                $batch['rate_per_unit'] = $current_rate;
                                $batch['document_id_text'] = $inventory_transaction['it_document_id_text'];
                                $batch['entry_id'] = $inventory_transaction['it_entry_id'];
                                $batch['entry_datetime'] = $inventory_transaction['it_entry_datetime'];

                                array_push($product_batch_array, $batch);    //push current batch to product batch array
                                $fifo_in_product_batch_map[$product_id] = $product_batch_array;
                            }
                        }

                        if ($batch_counter >= 0) {

                            $product_batch = $product_batch_array[$batch_counter];
                            $available_quantity = $product_batch['quantity'];
                            $current_rate = $product_batch['rate_per_unit'];

                            $inventory_entry['ie_fifo_reference'] = $product_batch['document_id_text'];
                            /**
                             * Performing FIFO calculations
                             */
                            if ($available_quantity >= abs($transaction_quantity) || $available_quantity == 0) {
                                $product_batch['quantity'] = $available_quantity - abs($transaction_quantity); //reduce used up quantity from FIFO queue
                                $product_batch_array[$batch_counter] = $product_batch;
                                $inventory_entry['ie_it_product_quantity'] = $transaction_quantity;
                                $transaction_quantity = 0;
                            } else {
                                $transaction_quantity = (abs($transaction_quantity) - $available_quantity) * (-1);
                                $inventory_entry['ie_it_product_quantity'] = $available_quantity * (-1);
                                //array_shift($product_batch_array);  //remove batch from FIFO queue
                                $product_batch['quantity'] = 0;
                                $product_batch_array[$batch_counter] = $product_batch;
                            }

                            $fifo_in_product_batch_map[$product_id] = $product_batch_array;
                            $inventory_entry['ie_it_product_rate'] = $current_rate;
                        } else {
                            $inventory_entry['ie_it_product_quantity'] = $transaction_quantity;
                            $inventory_entry['ie_it_product_rate'] = $current_rate;
                            $transaction_quantity = 0;
                        }

                        if ($document_type == 'invoice') {
                            /**
                             * Store FIFO out batch for current invoice entry ( ipe_id ). To be used in case Credit note is issued against this invoice in future. 
                             * The stored batch will be used for FIFO In calculations
                             */
                            $batch_data = array();
                            $batch_data['quantity'] = abs($inventory_entry['ie_it_product_quantity']);
                            if ($batch_counter < 0) {
                                $batch_data['batch_count'] = sizeof($product_batch_array) - 1;
                            } else {
                                $batch_data['batch_count'] = $batch_counter;
                            }

                            $batch_data_array = array();
                            if (isset($invoice_entry_batch_map[$inventory_transaction['it_entry_id']])) {
                                $batch_data_array = $invoice_entry_batch_map[$inventory_transaction['it_entry_id']];
                            }
                            array_push($batch_data_array, $batch_data);
                            $invoice_entry_batch_map[$inventory_transaction['it_entry_id']] = $batch_data_array;
                        }
                    }


                    /**
                     *  Calculating stock on hand
                     */
                    if (!isset($product_wise_stock[$product_id])) {
                        $product_wise_stock[$product_id] = $inventory_entry['ie_it_product_quantity'];
                        $inventory_entry['ie_stock_on_hand'] = $product_wise_stock[$product_id];
                    } else {
                        $inventory_entry['ie_stock_on_hand'] = $product_wise_stock[$product_id] + $inventory_entry['ie_it_product_quantity'];
                        $product_wise_stock[$product_id] = $inventory_entry['ie_stock_on_hand'];
                    }

                    /**
                     * Calculating inventory asset value
                     */
                    $inventory_asset_value = 0;

                    if (!isset($product_inventory_asset_value_map[$product_id])) {
                        $product_inventory_asset_value_map[$product_id] = $inventory_entry['ie_it_product_quantity'] * $inventory_entry['ie_it_product_rate'];
                    } else {
                        $product_inventory_asset_value_map[$product_id] = $product_inventory_asset_value_map[$product_id] +
                                $inventory_entry['ie_it_product_quantity'] * $inventory_entry['ie_it_product_rate'];
                    }
                    $inventory_asset_value = $product_inventory_asset_value_map[$product_id];
                    //$inventory_entry['ie_inventory_asset_value'] = $inventory_asset_value;
                    $inventory_entry['ie_inventory_asset_value'] = 0;

                    $this->db->insert('inventory_entry', $inventory_entry);

                    $stresstest++;
                } while (abs($transaction_quantity) > 0 || abs($inventory_adjustment_valuation_reversal) > 0);
                ////while ($stresstest < 2000);
            }
            
            $this->evaluate_inventory_asset_valuation();
        } finally {
            $this->load->model('system_journal_model');
            $this->system_journal_model->calculate_system_journal();
        }
    }
    
    
    public function evaluate_inventory_asset_valuation(){
        
        $query = 'Select * from inventory_entry';
        $result = $this->db->query($query);
        $inventory_entries = $result->result_array();

        $product_inventory_asset_value_map = array();
        foreach ($inventory_entries as $inventory_entry) {
            $product_id = $inventory_entry['ie_it_product_id'];
            if (!isset($product_inventory_asset_value_map[$product_id])) {
                $product_inventory_asset_value_map[$product_id] = $inventory_entry['ie_it_product_quantity'] * $inventory_entry['ie_it_product_rate'];
            } else {
                $product_inventory_asset_value_map[$product_id] = $product_inventory_asset_value_map[$product_id] +
                        $inventory_entry['ie_it_product_quantity'] * $inventory_entry['ie_it_product_rate'];
            }
            $inventory_asset_value = $product_inventory_asset_value_map[$product_id];
            
            $updated_inventory_entry['ie_inventory_asset_value'] = $inventory_asset_value;

            $this->db->where('ie_id', $inventory_entry['ie_id']);
            $this->db->update('inventory_entry', $updated_inventory_entry);
        }
    }

}

?>