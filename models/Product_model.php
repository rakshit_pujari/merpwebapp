<?php

class Product_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    /**
     * Delete product using ID
     */
    function delete_product_by_id($id) {

        log_message('debug', 'delete_product_by_id. - $id = ' . print_r($id, 1));

        try {
            $this->db->trans_begin();

            $this->db->where('pob_product_id', $id);
            $this->db->delete('product_opening_balance');

            $this->db->where('product_id', $id);
            $this->db->delete('product');

            log_message('debug', 'deleteProductBy. - Query = ' . $this->db->last_query());

            if ($this->db->affected_rows() == '1') {
                $this->db->trans_commit();
                log_message('debug', 'deleteProductBy. - Product deleted ');
                return TRUE;
            } else {
                $this->db->trans_rollback();
                log_message('debug', 'deleteProductBy. - Failed');
                return FALSE;
            }
        } finally {
            $this->load->model('inventory_model');
            $this->inventory_model->evaluate_inventory_figures();
        }
    }

    /**
     * Get on activated products
     */
    public function get_activated_products($transaction_type = null) {
        $query = 'SELECT
                    *,
                    (Select uqc_text from unique_quantity_code where unique_quantity_code.uqc_id = product.product_uqc_id) as uqc_text
                  FROM
                    product
                  WHERE product_status = "active"';

        if ($transaction_type != null) {
            $query .= ' && FIND_IN_SET("' . $transaction_type . '", transaction_type)';
        }

        $query .= ';';
        $result = $this->db->query($query);
        return $result->result_array();
    }

    /**
     * Get on activated products
     */
    public function get_activated_products_with_inventory_status($transaction_type = null) {
        $query = 'SELECT 
                    *, IFNULL(SUM(ie_it_product_quantity), 0) AS inventory_quantity 
                FROM
                    product
                LEFT JOIN
                    unique_quantity_code ON unique_quantity_code.uqc_id = product.product_uqc_id
                LEFT JOIN
                    inventory_entry ON inventory_entry.ie_it_product_id = product.product_id
                WHERE
                    product_status = "active"';

        if ($transaction_type != null) {
            $query .= ' && FIND_IN_SET("' . $transaction_type . '", transaction_type)';
        }

        $query .= ' GROUP BY product_id;';
        $result = $this->db->query($query);
        return $result->result_array();
    }

    /**
     * Get all products
     */
    public function get_all_products() {
        $result = $this->db->query('SELECT *
                                        FROM
                                    product
                                        LEFT JOIN
                                    employee ON employee.employee_id = product.product_record_created_by;');
        return $result->result_array();
    }
    
        /**
     * Get all products
     */
    public function get_all_products_for_report() {
        $result = $this->db->query('SELECT
                                            CONCAT("PRD", LPAD(product_id, "5", "0")) AS product_id,
                                          product_name,
                                          product_type,
                                          transaction_type,
                                          product_code,
                                          product_category,
                                          hsn_code,
                                          product_status,
                                          purchase_price,
                                          selling_price
                                        FROM
                                    product;');
        return $result->result_array();
    }

    /**
     * Get all products
     */
    public function get_all_products_with_inventory_status() {
        $result = $this->db->query('SELECT *,
                                        (Select ie_stock_on_hand from inventory_entry where inventory_entry.ie_it_product_id = product.product_id ORDER BY ie_id DESC LIMIT 1) AS product_quantity
                                        FROM
                                    product
                                        LEFT JOIN
                                    unique_quantity_code ON unique_quantity_code.uqc_id = product.product_uqc_id
                                        LEFT JOIN
                                    employee ON employee.employee_id = product.product_record_created_by;');
        return $result->result_array();
    }

    /**
     * Get products using ID
     */
    public function get_product_with_id($product_id) {
        //$query = $this->db->get_where('product', array('product_id' => $id));
        $result = $this->db->query('SELECT *
                                        FROM
                                    product
                                        LEFT JOIN
                                    unique_quantity_code ON unique_quantity_code.uqc_id = product.product_uqc_id
                                        LEFT JOIN
                                    employee ON employee.employee_id = product.product_record_created_by
                                    WHERE product_id = '. $product_id .';');
        return $result->row_array();
    }

    /**
     * Add new product
     */
    public function save_product($product_id, $data) {
        log_message('debug', 'add_product. - $data = ' . print_r($data, 1));

        $transaction_string = $data['transaction_type'][0];
        if (sizeof($data['transaction_type']) == 2) {
            $transaction_string = $data['transaction_type'][0] . ',' . $data['transaction_type'][1];
        }
        $data['transaction_type'] = $transaction_string;

        $this->db->query('SET time_zone = "+05:30";');

        if ($product_id == null) {
            $this->db->insert('product', $data);
            $product_id = $this->db->insert_id();
        } else {
            $this->db->where('product_id', $product_id);
            $this->db->update('product', $data);
        }

        if ($data['product_type'] == 'goods') {
            $pob = $this->db->query('SELECT * FROM product_opening_balance where pob_product_id = "' . $product_id . '"')->result_array();
            if (sizeof($pob) == 0) {
                $product_opening_balance = array();
                $product_opening_balance['pob_opening_balance'] = 0; //$data['opening_stock'];
                $product_opening_balance['pob_unit_rate'] = 0; //$data['rate_per_unit
                $product_opening_balance['pob_product_id'] = $product_id;
                $this->db->insert('product_opening_balance', $product_opening_balance);
            }
        } else {
            //delete any opening balance in case user has edit product type of existing product from 'goods' to 'service'
            $this->db->where('pob_product_id', $product_id);
            $this->db->delete('product_opening_balance');
        }

        if ($product_id > 0) {
            $response['result'] = "success";
            $response['id'] = $product_id;
        } else {
            $response['result'] = $this->db->error();
            $response['query'] = $this->db->last_query();
            $response['id'] = -1;
        }

        log_message('debug', 'add_product. - Query = ' . $this->db->last_query());

        log_message('debug', 'add_product. - response = ' . print_r($response, 1));
        return $response;
    }

}
