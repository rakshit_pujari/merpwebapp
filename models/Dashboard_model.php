<?php

class Dashboard_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    /**
     * Reference for generating week range taken from stackoverflow - https://stackoverflow.com/questions/2157282/generate-days-from-date-range
     * @param type $amount_field
     * @param type $date_field
     * @param type $table_name
     * @return type
     */
    public function get_week_wise_query($amount_field, $date_field, $table_name, $interval = NULL) {
        if ($interval == NULL) {
            $interval = 10;
        }
        return 'SELECT 
                t1.reference_week, t1.reference_year, t1.weekyear, IFNULL(' . $amount_field . ', 0) as ' . $amount_field . ' 
            FROM
                ((SELECT 
                    YEAR(a.Date) AS reference_year,
                        WEEK(a.Date) AS reference_week,
                        CONCAT(LPAD(WEEK(a.Date), 2, 0), YEAR(a.Date)) AS weekyear
                FROM
                    (SELECT 
                    CURDATE() - INTERVAL (a.a + (10 * b.a) + (100 * c.a)) DAY AS Date
                FROM
                    (SELECT 0 AS a UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) AS a
                CROSS JOIN (SELECT 0 AS a UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) AS b
                CROSS JOIN (SELECT 0 AS a UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) AS c) a
                WHERE
                    a.Date >= (CURDATE() - INTERVAL ' . $interval . ' WEEK)
                GROUP BY weekyear) t1
                LEFT JOIN (SELECT 
                    CONCAT(LPAD(WEEK(' . $date_field . '), 2, 0), YEAR(' . $date_field . ')) AS weekyear,
                        SUM(' . $amount_field . ') AS ' . $amount_field . '
                FROM
                    ' . $table_name . '
                WHERE
                    ' . $date_field . ' >= (CURDATE() - INTERVAL ' . $interval . ' WEEK)
                GROUP BY weekyear) t2 ON t1.weekyear = t2.weekyear)
            ORDER BY reference_year ASC , reference_week ASC';
    }

    /**
     * Returns result. Combines common week.
     * @param type $amount_field
     * @param type $date_field
     * @param type $table_name
     * @param int $interval
     * @return type
     */
    public function get_week_wise_query_result($amount_field, $date_field, $table_name, $interval = NULL) {
        if ($interval == NULL) {
            $interval = 11;
        }
        $query = 'SELECT 
                t1.reference_week as reference_week, t1.reference_year as reference_year, t1.weekyear as weekyear, IFNULL(' . $amount_field . ', 0) as ' . $amount_field . ' 
            FROM
                ((SELECT 
                    YEAR(a.Date) AS reference_year,
                        WEEK(a.Date) AS reference_week,
                        CONCAT(LPAD(WEEK(a.Date), 2, 0), YEAR(a.Date)) AS weekyear
                FROM
                    (SELECT 
                    CURDATE() - INTERVAL (a.a + (10 * b.a) + (100 * c.a)) DAY AS Date
                FROM
                    (SELECT 0 AS a UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) AS a
                CROSS JOIN (SELECT 0 AS a UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) AS b
                CROSS JOIN (SELECT 0 AS a UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) AS c) a
                WHERE
                    a.Date >= (CURDATE() - INTERVAL ' . $interval . ' WEEK)
                GROUP BY weekyear) t1
                LEFT JOIN (SELECT 
                    CONCAT(LPAD(WEEK(' . $date_field . '), 2, 0), YEAR(' . $date_field . ')) AS weekyear,
                        SUM(' . $amount_field . ') AS ' . $amount_field . '
                FROM
                    ' . $table_name . '
                WHERE
                    ' . $date_field . ' >= (CURDATE() - INTERVAL ' . $interval . ' WEEK)
                GROUP BY weekyear) t2 ON t1.weekyear = t2.weekyear)
            ORDER BY reference_year ASC , reference_week ASC';

        $result = $this->db->query($query)->result_array();

        if (sizeof($result) == $interval + 2) {
            /**
             * meaning same week has appeared twice. 
             * For example 53rd week of 2017 and 00th week of 2018 are the same week
             * However they appear twice in the result
             * Happens at year end
             */
            foreach ($result as $key => $result_item) {
                if (isset($result[$key + 1])) {
                    if ($result[$key]['reference_year'] != $result[$key + 1]['reference_year']) {
                        $result[$key][$amount_field] = $result[$key][$amount_field] + $result[$key + 1][$amount_field];
                        unset($result[$key + 1]);
                        break;
                    }
                }
            }
        }

        $result = array_values($result); 
        return $result;
    }

    /**
     * 
     */
    public function get_sales_history() {
        $this->db->query('SET time_zone = "+05:30";');
        $interval = 11;
        //$query_invoice = $this->get_week_wise_query('invoice_amount', 'invoice_date', 'invoice', $interval);
        //$query_cn = $this->get_week_wise_query('cn_amount', 'cn_date', 'credit_note', $interval);

        //$sales_history = $this->db->query($query_invoice)->result_array();
        $sales_history = $this->get_week_wise_query_result('invoice_amount', 'invoice_date', 'invoice', $interval);
        $cn_history = $this->get_week_wise_query_result('cn_amount', 'cn_date', 'credit_note', $interval);

        $sales_amount_array = array();
        foreach ($sales_history as $sales_history_item) {
            array_push($sales_amount_array, (int) $sales_history_item['invoice_amount']);
        }

        $cn_amount_array = array();
        foreach ($cn_history as $cn_item) {
            array_push($cn_amount_array, (int) $cn_item['cn_amount']);
        }

        $net_sales_array = array();
        for ($i = 0; $i < sizeof($sales_history); $i++) {
            array_push($net_sales_array, ((int) $sales_history[$i]['invoice_amount']) - ((int) isset($cn_history[$i]['cn_amount']) ? $cn_history[$i]['cn_amount'] : 0));
        }

        $result = array();
        $result['sales_amount_history'] = $sales_amount_array;
        $result['cn_amount_history'] = $cn_amount_array;
        $result['net_sales_amount_history'] = $net_sales_array;
        return $result;
    }

    /**
     * 
     * @return array
     */
    public function get_purchase_history() {
        $this->db->query('SET time_zone = "+05:30";');
        //$query_purchase = $this->get_week_wise_query('purchase_amount', 'purchase_date', 'purchase');
        //$query_dn = $this->get_week_wise_query('dn_amount', 'dn_date', 'debit_note');

        //$purchase_history = $this->db->query($query_purchase)->result_array();
        //$dn_history = $this->db->query($query_dn)->result_array();
        
        $purchase_history =  $this->get_week_wise_query_result('purchase_amount', 'purchase_date', 'purchase');
        $dn_history = $this->get_week_wise_query_result('dn_amount', 'dn_date', 'debit_note');
        
        $purchase_amount_array = array();
        foreach ($purchase_history as $purchase_history_item) {
            array_push($purchase_amount_array, (int) $purchase_history_item['purchase_amount']);
        }

        $dn_amount_array = array();
        foreach ($dn_history as $dn_item) {
            array_push($dn_amount_array, (int) $dn_item['dn_amount']);
        }

        $net_purchase_array = array();
        for ($i = 0; $i < sizeof($purchase_history); $i++) {
            array_push($net_purchase_array, ((int) $purchase_history[$i]['purchase_amount']) - ((int) isset($dn_history[$i]['dn_amount']) ? $dn_history[$i]['dn_amount'] : 0));
        }

        $result = array();
        $result['purchase_amount_history'] = $purchase_amount_array;
        $result['dn_amount_history'] = $dn_amount_array;
        $result['net_purchase_amount_history'] = $net_purchase_array;
        return $result;
    }

    /**
     * Get weekly profit status for dashboard
     */
    public function get_weekly_profit() {
        $this->db->query('SET time_zone = "+05:30";');


        $revenue_history = array();
        $expense_history = array();
        $profit_history = array();

        $yearweeks = $this->db->query('SELECT 
                                        CONCAT(YEAR(a.Date), LPAD(WEEK(a.Date), 2, 0)) AS yearweek
                                    FROM
                                        (SELECT 
                                            CURDATE() - INTERVAL (a.a + (10 * b.a) + (100 * c.a)) DAY AS Date
                                        FROM
                                            (SELECT 0 AS a UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) AS a
                                        CROSS JOIN (SELECT 0 AS a UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) AS b
                                        CROSS JOIN (SELECT 0 AS a UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) AS c) a
                                    WHERE
                                        a.Date >= (CURDATE() - INTERVAL 6 WEEK)
                                    GROUP BY yearweek
                                    ORDER BY yearweek DESC')->result_array();
        //set all keys
        foreach ($yearweeks as $yearweek) {
            $revenue_history[$yearweek['yearweek']] = 0;
            $expense_history[$yearweek['yearweek']] = 0;
            $profit_history[$yearweek['yearweek']] = 0;
        }

        $query = "SELECT 
                    at_name,
                    sj_transaction_type,
                    sj_amount,
                    CONCAT(YEAR(sj_date), LPAD(WEEK(sj_date), 2, 0)) AS yearweek
                FROM
                    system_journal
                        LEFT JOIN
                    chart_of_accounts ON chart_of_accounts.coa_id = system_journal.sj_account_coa_id
                        LEFT JOIN
                    account_type ON account_type.at_id = chart_of_accounts.coa_at_id
                WHERE
                    (at_name = 'Revenue'
                        OR at_name = 'Expense')
                        AND sj_date >= (CURDATE() - INTERVAL 6 WEEK)
                        ORDER BY yearweek DESC";

        $pnl_transactions = $this->db->query($query)->result_array();
        foreach ($pnl_transactions as $pnl_transaction) {
            if ($pnl_transaction['at_name'] == 'Revenue') {
                //revenue is a credit account
                $revenue_history[$pnl_transaction['yearweek']] = $revenue_history[$pnl_transaction['yearweek']] +
                        ($pnl_transaction['sj_transaction_type'] == 'credit' ? $pnl_transaction['sj_amount'] : ( -1 * $pnl_transaction['sj_amount']));
            } else if ($pnl_transaction['at_name'] == 'Expense') {
                //expense is a debit account
                $expense_history[$pnl_transaction['yearweek']] = $expense_history[$pnl_transaction['yearweek']] -
                        ($pnl_transaction['sj_transaction_type'] == 'debit' ? $pnl_transaction['sj_amount'] : ( -1 * $pnl_transaction['sj_amount']));
            } else {
                throw new Exception('Unknown account type');
            }

            $profit_history[$pnl_transaction['yearweek']] = $profit_history[$pnl_transaction['yearweek']] +
                    ($pnl_transaction['sj_transaction_type'] == 'credit' ? $pnl_transaction['sj_amount'] : ( -1 * $pnl_transaction['sj_amount']));
        }

        ksort($revenue_history, 1); //SORT_NUMERIC
        ksort($expense_history, 1); //SORT_NUMERIC
        ksort($profit_history, 1); //SORT_NUMERIC
        //round off
        foreach ($yearweeks as $yearweek) {
            $revenue_history[$yearweek['yearweek']] = round($revenue_history[$yearweek['yearweek']], 2);
            $expense_history[$yearweek['yearweek']] = round($expense_history[$yearweek['yearweek']], 2);
            $profit_history[$yearweek['yearweek']] = round($profit_history[$yearweek['yearweek']], 2);
        }


        $revenue_history = array_values($revenue_history);
        $expense_history = array_values($expense_history);
        $profit_history = array_values($profit_history);

        $result = array();
        $result['revenue_history'] = $revenue_history;
        $result['expense_history'] = $expense_history;
        $result['net_profit_history'] = $profit_history;

        return $result;
    }

    /**
     * 
     * @return type
     */
    public function get_current_fy_profit_figures() {
        $query_revenue = "SELECT 
                    SUM(IF(sj_transaction_type = 'credit',
                        sj_amount,
                        (- 1 * sj_amount))) AS revenue
                FROM
                    system_journal
                        LEFT JOIN
                    chart_of_accounts ON chart_of_accounts.coa_id = system_journal.sj_account_coa_id
                        LEFT JOIN
                    account_type ON account_type.at_id = chart_of_accounts.coa_at_id
                WHERE
                    at_name = 'Revenue'
                        AND sj_date >= IF(MONTH(CURDATE()) >= 4,
                        STR_TO_DATE(CONCAT(YEAR(CURDATE()), ',4', ',1'),
                                '%Y,%m,%d'),
                        STR_TO_DATE(CONCAT(YEAR(CURDATE()) - 1, ',4', ',1'),
                                '%Y,%m,%d'))";

        $query_expense = "SELECT 
                    SUM(IF(sj_transaction_type = 'debit',
                        sj_amount,
                        (- 1 * sj_amount))) AS expense
                FROM
                    system_journal
                        LEFT JOIN
                    chart_of_accounts ON chart_of_accounts.coa_id = system_journal.sj_account_coa_id
                        LEFT JOIN
                    account_type ON account_type.at_id = chart_of_accounts.coa_at_id
                WHERE
                    at_name = 'Expense'
                        AND sj_date >= IF(MONTH(CURDATE()) >= 4,
                        STR_TO_DATE(CONCAT(YEAR(CURDATE()), ',4', ',1'),
                                '%Y,%m,%d'),
                        STR_TO_DATE(CONCAT(YEAR(CURDATE()) - 1, ',4', ',1'),
                                '%Y,%m,%d'))";

        $current_fy_revenue = $this->db->query($query_revenue)->row_array()['revenue'];
        $current_fy_expense = $this->db->query($query_expense)->row_array()['expense'];
        $current_fy_profit = $current_fy_revenue - $current_fy_expense;

        $result = array();
        $result['revenue'] = $current_fy_revenue;
        $result['expense'] = $current_fy_expense;
        $result['profit'] = $current_fy_profit;

        return $result;
    }

    /**
     * 
     * @return type
     */
    public function get_current_fy_doc_figures() {
        //set current financial year begin date
        $this->db->query("SET @fy_begin_date = 
                IF(MONTH(CURDATE()) >= 4,
                    STR_TO_DATE(CONCAT(YEAR(CURDATE()), ',4', ',1'),
                            '%Y,%m,%d'),
                    STR_TO_DATE(CONCAT(YEAR(CURDATE()) - 1, ',4', ',1'),
                            '%Y,%m,%d'));");

        $current_fy_sales = $this->db->query("SELECT SUM(invoice_amount) as invoice_amount FROM invoice WHERE invoice_date >= @fy_begin_date;")->row_array()['invoice_amount'];
        $current_fy_cn = $this->db->query("SELECT SUM(cn_amount) as cn_amount FROM credit_note WHERE cn_date >= @fy_begin_date;")->row_array()['cn_amount'];
        $current_fy_purchase = $this->db->query("SELECT SUM(purchase_amount) as purchase_amount FROM purchase WHERE purchase_date >= @fy_begin_date;")->row_array()['purchase_amount'];
        $current_fy_dn = $this->db->query("SELECT SUM(dn_amount) as dn_amount FROM debit_note WHERE dn_date >= @fy_begin_date;")->row_array()['dn_amount'];

        $net_sales = $current_fy_sales - $current_fy_cn;
        $net_purchase = $current_fy_purchase - $current_fy_dn;

        $result = array();
        $result['sales'] = $current_fy_sales;
        $result['cn'] = $current_fy_cn;
        $result['purchase'] = $current_fy_purchase;
        $result['dn'] = $current_fy_dn;
        $result['net_sales'] = $net_sales;
        $result['net_purchase'] = $net_purchase;

        return $result;
    }

}
