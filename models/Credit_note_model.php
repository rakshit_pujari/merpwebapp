<?php

class Credit_note_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }
    
       /**
     * 
     * @return type
     */
    public function get_all_credit_notes_for_report(){
        $result = $this->db->query("SELECT 
                                        CONCAT('CDN', LPAD(cn_id, '5', '0')) AS credit_note_id,
                                        cn_linked_company_display_name as customer_name,
                                        CONCAT('INV', LPAD(cn_linked_invoice_id, '5', '0')) AS linked_invoice_id,
                                        cn_date as credit_note_date,
                                        cn_linked_company_billing_address as customer_billing_address,
                                        cn_amount as credit_note_amount
                                    FROM
                                        credit_note
                                    LEFT JOIN
                                        invoice
                                    ON
                                        credit_note.cn_linked_purchase_id = invoice.invoice_id   
                                    LEFT JOIN
                                        employee ON employee.employee_id = credit_note.cn_record_created_by
                                    ORDER BY
                                        cn_date;");
        return $result->result_array();
    }

    public function get_all_customers() {
        $result = $this->db->query('Select DISTINCT(cn_linked_company_invoice_name), '
                . 'cn_linked_company_id from credit_note where cn_linked_company_invoice_name!="null";');
        return $result->result_array();
    }

    /*
     * Delete Credit note
     */
    function delete_draft_credit_note_by_id($credit_note_id) {
        log_message('debug', 'delete_draft_credit_note_by_id. - $id = ' . print_r($credit_note_id, 1));

        $this->db->where('cne_linked_credit_note_id', $credit_note_id);
        $this->db->delete('draft_credit_note_entries');

        $this->db->where('cnce_linked_credit_note_id', $credit_note_id);
        $this->db->delete('draft_credit_note_charge_entries');
        
        $this->db->where('cn_id', $credit_note_id);
        $this->db->delete('draft_credit_note');

        log_message('debug', 'delete_draft_credit_note_by_id. - Query = ' . $this->db->last_query());

        if ($this->db->affected_rows() == '1') {
            log_message('debug', 'delete_draft_credit_note_by_id. - DELETED ');
            return TRUE;
        } else {
            log_message('debug', 'delete_draft_credit_note_by_id. - FALIED TO DELETE ');
            return FALSE;
        }
    }

    /*
     * Retrieve all Credit notes
     */

    public function get_all_credit_notes() {
        $result = $this->db->query('SELECT 
                                        *
                                    FROM
                                        credit_note
                                    LEFT JOIN
                                        invoice
                                    ON
                                        credit_note.cn_linked_invoice_id = invoice.invoice_id
                                    LEFT JOIN
                                        employee ON employee.employee_id = credit_note.cn_record_created_by
                                    ORDER BY 
                                        cn_id DESC;');
        return $result->result_array();
    }

    /**
     * Get entries for a credit note
     */
    public function get_credit_note_entries($cn_id) {
        $query = $this->db->get_where('credit_note_entries', array('cne_linked_credit_note_id' => $cn_id));
        return $query->result_array();
    }

    /*
     * Retrieve all draft credit_notes
     */

    public function get_all_draft_credit_notes() {
        $result = $this->db->query('SELECT 
                                        *
                                    FROM
                                        draft_credit_note
                                    LEFT JOIN
                                        invoice
                                    ON
                                        draft_credit_note.cn_linked_invoice_id = invoice.invoice_id
                                    LEFT JOIN
                                        employee ON employee.employee_id = draft_credit_note.cn_record_created_by;');
        return $result->result_array();
    }

    /**
     * Get entries for an credit_note
     */
    public function get_draft_credit_note_entries($credit_note_id) {

        $query = 'SELECT 
                    *
                    FROM draft_credit_note_entries 
                    WHERE
                    cne_linked_credit_note_id = ' . $credit_note_id . ' group by cne_id;';

        $result = $this->db->query($query);
        return $result->result_array();
    }

        /**
     * Get entries for an credit_note
     */
    public function get_credit_note_charge_entries($credit_note_id) {
        $query = 'SELECT 
                        *
                    FROM credit_note_charge_entries 
                        WHERE
                    cnce_linked_credit_note_id = ' . $credit_note_id . ' group by cnce_id;';

        $result = $this->db->query($query);
        return $result->result_array();
    }
    
    
    /**
     * Get entries for an credit_note
     */
    public function get_draft_credit_note_charge_entries($credit_note_id) {
        $query = 'SELECT 
                        *
                    FROM draft_credit_note_charge_entries 
                        WHERE
                    cnce_linked_credit_note_id = ' . $credit_note_id . ' group by cnce_id;';

        $result = $this->db->query($query);
        return $result->result_array();
    }

    
    /*
     * Get credit_note using credit_note_id
     */

    public function get_draft_credit_note_with_id($credit_note_id) {
        $query = $this->db->get_where('draft_credit_note', array('cn_id' => $credit_note_id));
        return $query->row_array();
    }

    /*
     * Get credit note using credit_note_id
     */

    public function get_credit_note_with_id($credit_note_id) {
        $query = $this->db->get_where('credit_note', array('cn_id' => $credit_note_id));
        return $query->row_array();
    }

    /*
     * Add new credit_note

      0 - Tax
      1 - HSN
      2 - rate
      3 - quantity
      4 - discount
     * 
     * 
     * IMPORTANT - while editing credit note entries. Always delete old entries and insert edited ones so that new cne_id are assgined. Failure to
     * do so may lead to error while calculating inventory

     */

    public function save_credit_note($credit_note_id, $credit_note, $credit_note_entries, $credit_note_charge_entries, $credit_note_action) {

        try {
            if ($credit_note_action == 'previewDraft') {
                $credit_note_action = 'draft';
            }

            log_message('debug', 'add_credit_note. - $credit_note = ' . print_r($credit_note, 1));
            date_default_timezone_set('Asia/Kolkata');  //PHP server time zone. This won't set mySQl time zone. Needs to be set separately.
            $credit_against = '';
            $credit_note['cn_linked_invoice_id'] = null;
            $credit_note['cn_linked_purchase_id'] = null;
            if ($credit_note['credit_against'] == 'sales') {
                $credit_note['cn_linked_invoice_id'] = $credit_note['cn_linked_supplementary_invoice_id'];
                $credit_against = 'sales';
            } else if ($credit_note['credit_against'] == 'purchase') {
                $credit_note['cn_linked_purchase_id'] = $credit_note['cn_linked_supplementary_invoice_id'];
                $credit_against = 'purchase';
            }
            unset($credit_note['cn_linked_supplementary_invoice_id']);
            unset($credit_note['credit_against']);

            if ($credit_note['cn_write_off_stock'] == 'yes') {
                $credit_note['cn_write_off_stock'] = 1;
            } else if ($credit_note['cn_write_off_stock'] == 'no') {
                $credit_note['cn_write_off_stock'] = 0;
            }

            
            if (empty($credit_note['cn_linked_employee_id'])) {
                $credit_note['cn_linked_employee_id'] = NULL;
            }
            
            if (empty($credit_note['cn_linked_broker_id'])) {
                $credit_note['cn_linked_broker_id'] = NULL;
            }            
            
            $this->load->model('company_model');
            $this->load->model('owner_company_model');
            $this->load->model('payment_term_model');
            $this->load->model('state_model');
            $this->load->model('transporter_model');

            $this->load->model('supplementary_invoice_reason_model');

            //add other data
            $owner_company = $this->owner_company_model->get_owner_company();

            $credit_note['cn_oc_name'] = $owner_company['oc_billing_name'];
            $credit_note['cn_oc_address'] = $owner_company['oc_billing_address'];
            $credit_note['cn_oc_city_name'] = $owner_company['city_name'];
            $credit_note['cn_oc_gstin'] = $owner_company['oc_gst_number'];
            $credit_note['cn_oc_pan_number'] = $owner_company['oc_pan_number'];
            $credit_note['cn_oc_logo_path'] = $owner_company['oc_logo_path'];
            $credit_note['cn_oc_district'] = $owner_company['district'];
            $credit_note['cn_oc_state'] = $owner_company['billing_state_name'];
            $credit_note['cn_oc_contact_number'] = $owner_company['oc_contact_number'];
            $credit_note['cn_oc_gst_supply_state_id'] = $owner_company['oc_gst_supply_state_id'];
            //$credit_note['cn_terms'] = $owner_company['oc_invoice_terms'];
            //$credit_note['cn_additional_terms'] = $owner_company['oc_additional_terms'];

            $credit_note['cn_sir_text'] = $this->supplementary_invoice_reason_model->get_sir_with_id($credit_note['cn_sir_id'])['sir_text'];

            if ($credit_against == 'sales') {
                $this->load->model('invoice_model');
                $credit_note['cn_linked_invoice_date'] = $this->invoice_model->get_invoice_with_id($credit_note['cn_linked_invoice_id'])['invoice_date'];
            } else if ($credit_against == 'purchase') {
                $this->load->model('purchase_model');
                $credit_note['cn_linked_invoice_date'] = $this->purchase_model->get_purchase_with_id($credit_note['cn_linked_purchase_id'])['purchase_date'];
            }

            if (empty($credit_note['cn_linked_transporter_id'])) {
                $credit_note['cn_linked_transporter_id'] = NULL;
                $credit_note['cn_linked_transporter_name'] = NULL;
            } else {
                $transporter_id = $credit_note['cn_linked_transporter_id'];
                $credit_note['cn_linked_transporter_name'] = $this->transporter_model->get_transporter_with_id($transporter_id)['transporter_name'];
            }

            $this->db->query('SET time_zone = "+05:30";');

            $credit_note_date = $credit_note['cn_date'];

            $payment_term = $this->payment_term_model->get_payment_term_using_id($credit_note['cn_linked_company_payment_term_id']);
            $credit_note['cn_linked_company_payment_term_name'] = $payment_term['payment_term_display_text'];

            /* add time component to date if invoice date is today.
             * else
             * add 23:59:59 if the invoice was created on a later date
             * This timestamp is to determine entry sequence for FIFO inventory
             * To be done for Invoice, purchase, credit note, debit note, inventory adjustment
             */
            if (date('Y-m-d', strtotime($credit_note['cn_date'])) == date("Y-m-d")) {
                $credit_note['cn_date'] = date("Y-m-d H:i:sa");
            } else {
                $credit_note['cn_date'] = date("Y-m-d 23:59:59", strtotime($credit_note['cn_date']));
            }

            if (!empty($credit_note['cn_linked_company_id']) && $credit_note['cn_linked_company_id']!='null') {
                $linked_company_id = $credit_note['cn_linked_company_id'];
                $linked_company = $this->company_model->get_company_with_id($linked_company_id);
                $credit_note['cn_linked_company_display_name'] = $linked_company['company_display_name'];
                $credit_note['cn_linked_company_gstin'] = $linked_company['company_gst_number'];
                $credit_note['cn_is_reverse_charge_applicable'] = $linked_company['is_reverse_charge_applicable_for_company'];
                $credit_note['cn_linked_company_contact_number'] = $linked_company['company_contact_number'];
            } else {
                $credit_note['cn_linked_company_id'] = NULL;
                $credit_note['cn_linked_company_display_name'] = NULL;
                $credit_note['cn_linked_company_gstin'] = NULL;
                $credit_note['cn_is_reverse_charge_applicable'] = NULL;
                $credit_note['cn_linked_company_contact_number'] = NULL;
            }

            $credit_note_linked_company_gst_supply_state_id = $credit_note['cn_linked_company_gst_supply_state_id'];
            $credit_note['cn_place_of_supply'] = $this->state_model->get_state_with_id($credit_note_linked_company_gst_supply_state_id)['state_name'];

            $credit_note['cn_linked_company_billing_state_name'] = $this->state_model->get_state_with_id($credit_note['cn_linked_company_billing_state_id'])['state_name'];
            if (!empty($credit_note['cn_linked_company_shipping_state_id'])){
                $credit_note['cn_linked_company_shipping_state_name'] = $this->state_model->get_state_with_id($credit_note['cn_linked_company_shipping_state_id'])['state_name'];
            } else {
                $credit_note['cn_linked_company_shipping_state_id'] = NULL;
                $credit_note['cn_linked_company_shipping_state_name'] = NULL;
            }

            $this->db->trans_begin();
            $count = 0;
            $this->db->query('SET time_zone = "+05:30";');
            if ($credit_note['cn_status'] == 'new') {  //creating new credit_note
                $credit_note['cn_status'] = $credit_note_action;
                if ($credit_note_action == 'confirm') {
                    $this->db->insert('credit_note', $credit_note);
                } else if ($credit_note_action == 'draft') {
                    $this->db->insert('draft_credit_note', $credit_note);
                } else {
                    throw new Exception('Invalid credit_note action for insert');
                }
                $credit_note_id = $this->db->insert_id();
            } else {        // user is editing an existing credit_note. 
                if ($credit_note_action == $credit_note['cn_status']) {

                    if ($credit_note_action == 'draft') {

                        $this->db->where('cne_linked_credit_note_id', $credit_note_id);
                        $this->db->delete('draft_credit_note_entries');
                        
                        $this->db->where('cnce_linked_credit_note_id', $credit_note_id);
                        $this->db->delete('draft_credit_note_charge_entries');

                        $this->db->where('cn_id', $credit_note_id);
                        $this->db->update('draft_credit_note', $credit_note);

                    } else if ($credit_note_action == 'confirm') {

                        //retain time component if date not edited by user
                        $credit_note_last_copy = $this->get_credit_note_with_id($credit_note_id);
                        if (date('Y-m-d', strtotime($credit_note['cn_date'])) == strtotime($credit_note_last_copy['cn_date'])) {
                            $credit_note['cn_date'] = $credit_note_last_copy['cn_date'];
                        }

                        $this->db->where('cne_linked_credit_note_id', $credit_note_id);
                        $this->db->delete('credit_note_entries');
                        
                        $this->db->where('cnce_linked_credit_note_id', $credit_note_id);
                        $this->db->delete('credit_note_charge_entries');

                        $this->db->where('cn_id', $credit_note_id);
                        $this->db->update('credit_note', $credit_note);

                    } else {
                        throw new Exception('Invalid credit_note action for update');
                    }
                } else if ($credit_note['cn_status'] == 'draft' && $credit_note_action == 'confirm') {   //confirming a draft credit_note
                    //delete draft credit_note
                    $this->db->where('cne_linked_credit_note_id', $credit_note_id);
                    $this->db->delete('draft_credit_note_entries');
                    
                    $this->db->where('cnce_linked_credit_note_id', $credit_note_id);
                    $this->db->delete('draft_credit_note_charge_entries');
                    
                    $this->db->where('cn_id', $credit_note_id);
                    $this->db->delete('draft_credit_note');

                    $credit_note['cn_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
                    
                    $credit_note['cn_status'] = $credit_note_action;
                    $this->db->insert('credit_note', $credit_note);
                    $credit_note_id = $this->db->insert_id();
                } else {
                    throw new Exception('Invalid credit_note status and required action');
                }
            }
            
            if ($credit_note['cn_status'] == 'confirm') {
                
                $delete_query = 'Delete from invoice_advance_receipt where iar_credit_note_id = '.$credit_note_id;
                $this->db->query($delete_query);
                //now check if exisitng advance receipts amount against this invoice is equal to the invoice amount
                $advance_receipts_against_invoice = $this->db->query('Select * '
                        . 'from invoice_advance_receipt where iar_invoice_id = "' . $credit_note['cn_linked_invoice_id'] . '"'
                        //. ' AND iar_credit_note_id IS NULL '
                        //. ' ORDER BY iar_advance_receipt_id DESC'
                        . '')->result_array();

                $balance_amount = $credit_note['cn_amount'];
                foreach ($advance_receipts_against_invoice as $advance_receipt) {
                    if ($balance_amount > 0) {
                        if ($balance_amount > $advance_receipt['iar_allocated_amount']) {
                                $this->db->insert('invoice_advance_receipt', array('iar_advance_receipt_id' => $advance_receipt['iar_advance_receipt_id'],
                                    'iar_credit_note_id' => $credit_note_id,
                                    'iar_allocated_amount' => $advance_receipt['iar_allocated_amount'] * (-1)));
                                $balance_amount = $balance_amount - $advance_receipt['iar_allocated_amount'];
                            } else {
                                $this->db->insert('invoice_advance_receipt', array('iar_advance_receipt_id' => $advance_receipt['iar_advance_receipt_id'],
                                    'iar_credit_note_id' => $credit_note_id,
                                    'iar_allocated_amount' => $balance_amount * (-1)));
                                $balance_amount = 0;
                            }
                    }
                }
            }
            //Get all taxes
            $tax_query = $this->db->get('tax');
            $taxes = $tax_query->result_array();
            $tax_id_percent_map = array();
            $tax_id_name_map = array();
            foreach ($taxes as $tax) {
                $tax_id_percent_map[$tax['tax_id']] = $tax['tax_percent'];
                $tax_id_name_map[$tax['tax_id']] = $tax['tax_name'];
            }

            if ($credit_note_id > 0) {
                $cne['cne_linked_credit_note_id'] = $credit_note_id;

                foreach ($credit_note_entries as $entry_id => $credit_note_entry) {
                    $cne['cne_entry_id'] = $entry_id;
                    foreach ($credit_note_entry as $product_id => $product_info) {
                        $cne['cne_linked_product_id'] = $product_id;

                        $cne['cne_product_name'] = $product_info[0];
                        $cne['cne_product_hsn_code'] = $product_info[1];
                        $cne['cne_tax_id_applicable'] = $product_info[2];
                        $cne['cne_tax_percent_applicable'] = $tax_id_percent_map[$product_info[2]]; // fetch tax percent from $tax_id_percent_map
                        $cne['cne_tax_name_applicable'] = $tax_id_name_map[$product_info[2]]; // fetch tax name from $tax_id_name_map
                        $cne['cne_product_rate'] = $product_info[3];
                        $cne['cne_product_quantity'] = $product_info[4];
                        $cne['cne_product_uqc_id'] = $product_info[5];
                        $cne['cne_product_uqc_text'] = $product_info[6];
                        $cne['cne_discount'] = $product_info[7];
                        if ($credit_against == 'sales') {
                            $cne['cne_linked_ipe_id'] = $product_info[8];
                        } else if ($credit_against == 'purchase') {
                            $cne['cne_linked_pe_id'] = $product_info[8];
                        }

                        if ($credit_note_action == 'confirm') {
                            $this->db->insert('credit_note_entries', $cne);
                        } else if ($credit_note_action == 'draft') {
                            $this->db->insert('draft_credit_note_entries', $cne);
                        } else {
                            throw new Exception('Invalid credit_note action while inserting credit_note entries');
                        }
                        if ($this->db->insert_id() > 0) {
                            $count++;
                        }
                    }
                }
                
                                //enter charges
                foreach ($credit_note_charge_entries as $charge_entry_id => $credit_note_charge_entry) {
                    $pce['cnce_entry_id'] = $charge_entry_id;
                    foreach ($credit_note_charge_entry as $charge_id => $charge_info) {
                        $pce['cnce_charge_id'] = $charge_id;
                        $pce['cnce_linked_credit_note_id'] = $credit_note_id;
                        $pce['cnce_charge_name'] = $charge_info[0];  
                        $pce['cnce_taxable_amount'] = $charge_info[1];
                        
                        $pce['cnce_tax_id_applicable'] = $charge_info[2];
                        $pce['cnce_tax_percent_applicable'] = $tax_id_percent_map[$charge_info[2]]; // fetch tax percent from $tax_id_percent_map
                        $pce['cnce_tax_name_applicable'] = $tax_id_name_map[$charge_info[2]]; // fetch tax name from $tax_id_name_map
                        
                        if ($credit_note_action == 'confirm') {
                            $this->db->insert('credit_note_charge_entries', $pce);
                        } else if ($credit_note_action == 'draft') {
                            $this->db->insert('draft_credit_note_charge_entries', $pce);
                        } else {
                            throw new Exception('Invalid credit_note action while inserting credit_note entries');
                        }
                    }
                }
            } else {
                $response['result'] = $this->db->error();
                $response['query'] = $this->db->last_query();
            }


            if ($count > 0) {
                $this->db->trans_commit();
                $response['result'] = "Success";
            } else {
                log_message('debug', 'confirm_dispatch. ROLLBACK. ' . print_r($response));
                $this->db->trans_rollback();
            }

            log_message('debug', 'edit_credit_note. - Query = ' . $this->db->last_query());
            return $credit_note_id;
        } finally {
            $this->load->model('inventory_model');
            $this->inventory_model->evaluate_inventory_figures();
        }
    }

}
