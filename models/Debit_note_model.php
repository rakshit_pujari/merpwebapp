<?php

class Debit_note_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

       /**
     * 
     * @return type
     */
    public function get_all_debit_notes_for_report(){
        $result = $this->db->query("SELECT 
                                        CONCAT('DBN', LPAD(dn_id, '5', '0')) AS debit_note_id,
                                        dn_linked_company_display_name as vendor_name,
                                        dn_order_reference as order_reference,
                                        CONCAT('PUR', LPAD(dn_linked_purchase_id, '5', '0')) AS linked_purchase_order_id,
                                        dn_date as debit_note_date,
                                        dn_linked_company_billing_address as vendor_billing_address,
                                        dn_amount as debit_note_amount
                                    FROM
                                            debit_note
                                    LEFT JOIN
                                        purchase
                                    ON
                                        debit_note.dn_linked_purchase_id = purchase.purchase_id   
                                    LEFT JOIN
                                            employee ON employee.employee_id = debit_note.dn_record_created_by
                                    ORDER BY
                                        dn_date;");
        return $result->result_array();
    }
    
    public function get_all_customers() {
        $result = $this->db->query('Select DISTINCT(dn_linked_company_invoice_name), dn_linked_company_id from debit_note where dn_linked_company_invoice_name!="null";');
        return $result->result_array();
    }

    /*
     * Delete Purchase
     */

    function delete_draft_debit_note_by_id($debit_note_id) {
        log_message('debug', 'delete_draft_debit_note_by_id. - $id = ' . print_r($debit_note_id, 1));

        $this->db->where('dne_linked_debit_note_id', $debit_note_id);
        $this->db->delete('draft_debit_note_entries');
        
        $this->db->where('dnce_linked_debit_note_id', $debit_note_id);
        $this->db->delete('draft_debit_note_charge_entries');
        
        $this->db->where('dn_id', $debit_note_id);
        $this->db->delete('draft_debit_note');

        log_message('debug', 'delete_draft_debit_note_by_id. - Query = ' . $this->db->last_query());

        if ($this->db->affected_rows() == '1') {
            log_message('debug', 'delete_draft_debit_note_by_id. - DELETED ');
            return TRUE;
        } else {
            log_message('debug', 'delete_draft_debit_note_by_id. - FALIED TO DELETE ');
            return FALSE;
        }
    }
    /*
     * Retrieve all Debit notes
     */

    public function get_all_debit_notes() {
        $result = $this->db->query('SELECT 
                                            *
                                    FROM
                                            debit_note
                                    LEFT JOIN
                                        purchase
                                    ON
                                        debit_note.dn_linked_purchase_id = purchase.purchase_id   
                                    LEFT JOIN
                                            employee ON employee.employee_id = debit_note.dn_record_created_by
                                    ORDER BY
                                        dn_id DESC;');
        return $result->result_array();
    }

    /**
     * Get entries for a debit note
     */
    public function get_debit_note_entries($dn_id) {
        $query = $this->db->get_where('debit_note_entries', array('dne_linked_debit_note_id' => $dn_id));
        return $query->result_array();
    }

    /*
     * Get debit note using debit_note_id
     */

    public function get_debit_note_with_id($debit_note_id) {
        $query = $this->db->get_where('debit_note', array('dn_id' => $debit_note_id));
        return $query->row_array();
    }

    /*
     * Retrieve all draft debit_notes
     */

    public function get_all_draft_debit_notes() {
        $result = $this->db->query('SELECT 
                                        *
                                    FROM
                                        draft_debit_note
                                    LEFT JOIN
                                        purchase
                                    ON
                                        draft_debit_note.dn_linked_purchase_id = purchase.purchase_id    
                                    LEFT JOIN
                                        employee ON employee.employee_id = draft_debit_note.dn_record_created_by;');
        return $result->result_array();
    }

    /**
     * Get entries for an debit_note
     */
    public function get_draft_debit_note_entries($debit_note_id) {

        $query = 'SELECT 
                    *
                    FROM draft_debit_note_entries 
                    WHERE
                    dne_linked_debit_note_id = ' . $debit_note_id . ' group by dne_id;';

        $result = $this->db->query($query);
        return $result->result_array();
    }
    
            /**
     * Get entries for an debit_note
     */
    public function get_debit_note_charge_entries($debit_note_id) {
        $query = 'SELECT 
                        *
                    FROM debit_note_charge_entries 
                        WHERE
                    dnce_linked_debit_note_id = ' . $debit_note_id . ' group by dnce_id;';

        $result = $this->db->query($query);
        return $result->result_array();
    }
    
    
    /**
     * Get entries for an debit_note
     */
    public function get_draft_debit_note_charge_entries($debit_note_id) {
        $query = 'SELECT 
                        *
                    FROM draft_debit_note_charge_entries 
                        WHERE
                    dnce_linked_debit_note_id = ' . $debit_note_id . ' group by dnce_id;';

        $result = $this->db->query($query);
        return $result->result_array();
    }


    /*
     * Get debit_note using debit_note_id
     */

    public function get_draft_debit_note_with_id($debit_note_id) {
        $query = $this->db->get_where('draft_debit_note', array('dn_id' => $debit_note_id));
        return $query->row_array();
    }

    /*
     * Add new debit_note

      0 - Tax
      1 - HSN
      2 - rate
      3 - quantity
      4 - discount

     * * IMPORTANT - while editing debit note entries. Always delete old entries and insert edited ones so that new dne_id are assgined. Failure to
     * do so may lead to error while calculating inventory

     */

    public function save_debit_note($debit_note_id, $debit_note, $debit_note_entries, $debit_note_charge_entries, $debit_note_action) {

        try {
            if ($debit_note_action == 'previewDraft') {
                $debit_note_action = 'draft';
            }

            log_message('debug', 'add_debit_note. - $debit_note = ' . print_r($debit_note, 1));
            date_default_timezone_set('Asia/Kolkata');  //PHP server time zone. This won't set mySQl time zone. Needs to be set separately.
            $debit_against = '';
            $debit_note['dn_linked_invoice_id'] = null;
            $debit_note['dn_linked_purchase_id'] = null;
            if ($debit_note['debit_against'] == 'sales') {
                $debit_note['dn_linked_invoice_id'] = $debit_note['dn_linked_supplementary_invoice_id'];
                $debit_against = 'sales';
            } else if ($debit_note['debit_against'] == 'purchase') {
                $debit_note['dn_linked_purchase_id'] = $debit_note['dn_linked_supplementary_invoice_id'];
                $debit_against = 'purchase';
            }
            unset($debit_note['dn_linked_supplementary_invoice_id']);
            unset($debit_note['debit_against']);

            /* if ($debit_note['dn_write_off_stock'] == 'yes') {
              $debit_note['dn_write_off_stock'] = 1;
              } else if ($debit_note['dn_write_off_stock'] == 'no') {
              $debit_note['dn_write_off_stock'] = 0;
              } */

            $this->load->model('company_model');
            $this->load->model('owner_company_model');
            $this->load->model('payment_term_model');
            $this->load->model('state_model');
            $this->load->model('transporter_model');

            $this->load->model('supplementary_invoice_reason_model');

            //add other data
            $owner_company = $this->owner_company_model->get_owner_company();

            $debit_note['dn_oc_name'] = $owner_company['oc_billing_name'];
            $debit_note['dn_oc_address'] = $owner_company['oc_billing_address'];
            $debit_note['dn_oc_city_name'] = $owner_company['city_name'];
            $debit_note['dn_oc_gstin'] = $owner_company['oc_gst_number'];
            $debit_note['dn_oc_pan_number'] = $owner_company['oc_pan_number'];
            $debit_note['dn_oc_logo_path'] = $owner_company['oc_logo_path'];
            $debit_note['dn_oc_district'] = $owner_company['district'];
            $debit_note['dn_oc_state'] = $owner_company['billing_state_name'];
            $debit_note['dn_oc_contact_number'] = $owner_company['oc_contact_number'];
            $debit_note['dn_oc_gst_supply_state_id'] = $owner_company['oc_gst_supply_state_id'];
            //$debit_note['dn_terms'] = $owner_company['oc_invoice_terms'];
            //$debit_note['dn_additional_terms'] = $owner_company['oc_additional_terms'];

            $debit_note['dn_sir_text'] = $this->supplementary_invoice_reason_model->get_sir_with_id($debit_note['dn_sir_id'])['sir_text'];

            if (empty($debit_note['dn_linked_employee_id'])) {
                $debit_note['dn_linked_employee_id'] = NULL;
            }
            
            if (empty($debit_note['dn_linked_transporter_id'])) {
                $debit_note['dn_linked_transporter_id'] = NULL;
            } else {
                $transporter_id = $debit_note['dn_linked_transporter_id'];
                $debit_note['dn_linked_transporter_name'] = $this->transporter_model->get_transporter_with_id($transporter_id)['transporter_name'];
            }
            
            if ($debit_against == 'sales') {
                $this->load->model('invoice_model');
                $debit_note['dn_linked_invoice_date'] = $this->invoice_model->get_invoice_with_id($debit_note['dn_linked_invoice_id'])['invoice_date'];
            } else if ($debit_against == 'purchase') {
                $this->load->model('purchase_model');
                $debit_note['dn_linked_invoice_date'] = $this->purchase_model->get_purchase_with_id($debit_note['dn_linked_purchase_id'])['purchase_date'];
            }


            $this->db->query('SET time_zone = "+05:30";');

            $debit_note_date = $debit_note['dn_date'];

            $payment_term = $this->payment_term_model->get_payment_term_using_id($debit_note['dn_linked_company_payment_term_id']);
            $debit_note['dn_linked_company_payment_term_name'] = $payment_term['payment_term_display_text'];

            /* add time component to date if invoice date is today.
             * else
             * add 23:59:59 if the invoice was created on a later date
             * This timestamp is to determine entry sequence for FIFO inventory
             * To be done for Invoice, purchase, debit note, debit note, inventory adjustment
             */
            if (date('Y-m-d', strtotime($debit_note['dn_date'])) == date("Y-m-d")) {
                $debit_note['dn_date'] = date("Y-m-d H:i:sa");
            } else {
                $debit_note['dn_date'] = date("Y-m-d 23:59:59", strtotime($debit_note['dn_date']));
            }

            if (!empty($debit_note['dn_linked_company_id']) && $debit_note['dn_linked_company_id']!='null') {
                $linked_company_id = $debit_note['dn_linked_company_id'];
                $linked_company = $this->company_model->get_company_with_id($linked_company_id);
                $debit_note['dn_linked_company_display_name'] = $linked_company['company_display_name'];
                $debit_note['dn_linked_company_gstin'] = $linked_company['company_gst_number'];
                $debit_note['dn_linked_company_pan_number'] = $linked_company['company_pan_number'];
                $debit_note['dn_is_reverse_charge_applicable'] = $linked_company['is_reverse_charge_applicable_for_company'];
                $debit_note['dn_linked_company_contact_number'] = $linked_company['company_contact_number'];
            } else {
                $debit_note['dn_linked_company_id']= NULL;
                $debit_note['dn_linked_company_display_name']= NULL;
                $debit_note['dn_linked_company_gstin']= NULL;
                $debit_note['dn_is_reverse_charge_applicable']= NULL;
                $debit_note['dn_linked_company_contact_number']= NULL;
            }

            $debit_note_linked_company_gst_supply_state_id = $debit_note['dn_linked_company_gst_supply_state_id'];
            $debit_note['dn_place_of_supply'] = $this->state_model->get_state_with_id($debit_note_linked_company_gst_supply_state_id)['state_name'];

            $debit_note['dn_linked_company_billing_state_name'] = $this->state_model->get_state_with_id($debit_note['dn_linked_company_billing_state_id'])['state_name'];
            if (!empty($debit_note['dn_linked_company_shipping_state_id'])){
                $debit_note['dn_linked_company_shipping_state_name'] = $this->state_model->get_state_with_id($debit_note['dn_linked_company_shipping_state_id'])['state_name'];
            } else {
                $debit_note['dn_linked_company_shipping_state_id'] = NULL;
            }  

            $this->db->trans_begin();
            $count = 0;
            $this->db->query('SET time_zone = "+05:30";');
            if ($debit_note['dn_status'] == 'new') {  //creating new debit_note
                $debit_note['dn_status'] = $debit_note_action;
                if ($debit_note_action == 'confirm') {
                    $this->db->insert('debit_note', $debit_note);
                } else if ($debit_note_action == 'draft') {
                    $this->db->insert('draft_debit_note', $debit_note);
                } else {
                    throw new Exception('Invalid debit_note action for insert');
                }
                $debit_note_id = $this->db->insert_id();
            } else {        // user is editing an existing debit_note. 
                if ($debit_note_action == $debit_note['dn_status']) {

                    if ($debit_note_action == 'draft') {

                        $this->db->where('dne_linked_debit_note_id', $debit_note_id);
                        $this->db->delete('draft_debit_note_entries');
                        
                        $this->db->where('dnce_linked_debit_note_id', $debit_note_id);
                        $this->db->delete('draft_debit_note_charge_entries');

                        $this->db->where('dn_id', $debit_note_id);
                        $this->db->update('draft_debit_note', $debit_note);

                    } else if ($debit_note_action == 'confirm') {

                        //retain time component if date not edited by user
                        $debit_note_last_copy = $this->get_debit_note_with_id($debit_note_id);
                        if (date('Y-m-d', strtotime($debit_note['dn_date'])) == strtotime($debit_note_last_copy['dn_date'])) {
                            $debit_note['dn_date'] = $debit_note_last_copy['dn_date'];
                        }

                        $this->db->where('dne_linked_debit_note_id', $debit_note_id);
                        $this->db->delete('debit_note_entries');
                        
                        $this->db->where('dnce_linked_debit_note_id', $debit_note_id);
                        $this->db->delete('debit_note_charge_entries');

                        $this->db->where('dn_id', $debit_note_id);
                        $this->db->update('debit_note', $debit_note);

                    } else {
                        throw new Exception('Invalid debit_note action for update');
                    }
                } else if ($debit_note['dn_status'] == 'draft' && $debit_note_action == 'confirm') {   //confirming a draft debit_note
                    //delete draft debit_note
                    $this->db->where('dne_linked_debit_note_id', $debit_note_id);
                    $this->db->delete('draft_debit_note_entries');
                    
                    $this->db->where('dnce_linked_debit_note_id', $debit_note_id);
                    $this->db->delete('draft_debit_note_charge_entries');
        
                    $this->db->where('dn_id', $debit_note_id);
                    $this->db->delete('draft_debit_note');

                    $debit_note['dn_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
                    
                    $debit_note['dn_status'] = $debit_note_action;
                    $this->db->insert('debit_note', $debit_note);
                    $debit_note_id = $this->db->insert_id();
                } else {
                    throw new Exception('Invalid debit_note status and required action');
                }
            }
            
            if ($debit_note['dn_status'] == 'confirm') {
                
                $delete_query = 'Delete from purchase_advance_payment where pap_debit_note_id = '.$debit_note_id;
                $this->db->query($delete_query);
                //now check if exisitng advance payments amount against this purchase is equal to the purchase amount
                $advance_payments_against_purchase = $this->db->query('Select * '
                        . 'from purchase_advance_payment where pap_purchase_id = "' . $debit_note['dn_linked_purchase_id'] . '"'
                        //. ' AND pap_debit_note_id IS NULL '
                        //. ' ORDER BY pap_advance_payment_id DESC'
                        . '')->result_array();

                $balance_amount = $debit_note['dn_amount'];
                foreach ($advance_payments_against_purchase as $advance_payment) {
                    if ($balance_amount > 0) {
                        if ($balance_amount > $advance_payment['pap_allocated_amount']) {
                                $this->db->insert('purchase_advance_payment', array('pap_advance_payment_id' => $advance_payment['pap_advance_payment_id'],
                                    'pap_debit_note_id' => $debit_note_id,
                                    'pap_allocated_amount' => $advance_payment['pap_allocated_amount'] * (-1)));
                                $balance_amount = $balance_amount - $advance_payment['pap_allocated_amount'];
                            } else {
                                $this->db->insert('purchase_advance_payment', array('pap_advance_payment_id' => $advance_payment['pap_advance_payment_id'],
                                    'pap_debit_note_id' => $debit_note_id,
                                    'pap_allocated_amount' => $balance_amount * (-1)));
                                $balance_amount = 0;
                            }
                    }
                }
            }
            
            //Get all taxes
            $tax_query = $this->db->get('tax');
            $taxes = $tax_query->result_array();
            $tax_id_percent_map = array();
            $tax_id_name_map = array();
            foreach ($taxes as $tax) {
                $tax_id_percent_map[$tax['tax_id']] = $tax['tax_percent'];
                $tax_id_name_map[$tax['tax_id']] = $tax['tax_name'];
            }

            if ($debit_note_id > 0) {
                $dne['dne_linked_debit_note_id'] = $debit_note_id;

                foreach ($debit_note_entries as $entry_id => $debit_note_entry) {
                    $dne['dne_entry_id'] = $entry_id;
                    foreach ($debit_note_entry as $product_id => $product_info) {
                        $dne['dne_linked_product_id'] = $product_id;

                        $dne['dne_product_name'] = $product_info[0];
                        $dne['dne_product_hsn_code'] = $product_info[1];
                        $dne['dne_tax_id_applicable'] = $product_info[2];
                        $dne['dne_tax_percent_applicable'] = $tax_id_percent_map[$product_info[2]]; // fetch tax percent from $tax_id_percent_map
                        $dne['dne_tax_name_applicable'] = $tax_id_name_map[$product_info[2]]; // fetch tax name from $tax_id_name_map
                        $dne['dne_product_rate'] = $product_info[3];
                        $dne['dne_product_quantity'] = $product_info[4];
                        $dne['dne_product_uqc_id'] = $product_info[5];
                        $dne['dne_product_uqc_text'] = $product_info[6];
                        $dne['dne_discount'] = $product_info[7];
                        if ($debit_against == 'sales') {
                            $dne['dne_linked_ipe_id'] = $product_info[8];
                        } else if ($debit_against == 'purchase') {
                            $dne['dne_linked_pe_id'] = $product_info[8];
                        }

                        if ($debit_note_action == 'confirm') {
                            $this->db->insert('debit_note_entries', $dne);
                        } else if ($debit_note_action == 'draft') {
                            $this->db->insert('draft_debit_note_entries', $dne);
                        } else {
                            throw new Exception('Invalid debit_note action while inserting debit_note entries');
                        }
                        if ($this->db->insert_id() > 0) {
                            $count++;
                        }
                    }
                }
                
                foreach ($debit_note_charge_entries as $charge_entry_id => $debit_note_charge_entry) {
                    $pce['dnce_entry_id'] = $charge_entry_id;
                    foreach ($debit_note_charge_entry as $charge_id => $charge_info) {
                        $pce['dnce_charge_id'] = $charge_id;
                        $pce['dnce_linked_debit_note_id'] = $debit_note_id;
                        $pce['dnce_charge_name'] = $charge_info[0];  
                        $pce['dnce_taxable_amount'] = $charge_info[1];
                        
                        $pce['dnce_tax_id_applicable'] = $charge_info[2];
                        $pce['dnce_tax_percent_applicable'] = $tax_id_percent_map[$charge_info[2]]; // fetch tax percent from $tax_id_percent_map
                        $pce['dnce_tax_name_applicable'] = $tax_id_name_map[$charge_info[2]]; // fetch tax name from $tax_id_name_map
                        
                        if ($debit_note_action == 'confirm') {
                            $this->db->insert('debit_note_charge_entries', $pce);
                        } else if ($debit_note_action == 'draft') {
                            $this->db->insert('draft_debit_note_charge_entries', $pce);
                        } else {
                            throw new Exception('Invalid debit_note action while inserting debit_note entries');
                        }
                    }
                }
                
            } else {
                $response['result'] = $this->db->error();
                $response['query'] = $this->db->last_query();
            }


            if ($count > 0) {
                $this->db->trans_commit();
                $response['result'] = "Success";
            } else {
                log_message('debug', 'confirm_dispatch. ROLLBACK. ' . print_r($response));
                $this->db->trans_rollback();
            }

            log_message('debug', 'edit_debit_note. - Query = ' . $this->db->last_query());
            return $debit_note_id;
        } finally {
            $this->load->model('inventory_model');
            $this->inventory_model->evaluate_inventory_figures();
        }
    }

}
