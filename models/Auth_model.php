<?php

class Auth_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function is_session_valid(){
        $current_session_token = $this->session->userdata('token');
        
        if((!empty($current_session_token) 
                && $this->session->userdata('employee')['employee_id'] > 0)){
            
            $query = 'select * from employee_login where employee_session_token = "'.$current_session_token.'" AND employee_logout_time IS NULL;';
            $result = $this->db->query($query)->result_array();
            return (sizeof($result) == 1);
        }      
        return false;
    }
    
    public function record_session_info($user){
        $this->db->query('SET time_zone = "+05:30";');

        //close open sessions
        $query = 'update employee_login SET  employee_logout_time = NOW() where employee_logout_time IS NULL;';
        $this->db->query($query);
        
        $session = array();
        $session['employee_id'] = $user['employee_id'];
        $session['employee_session_token'] = $this->get_token(20);
        $session['employee_session_ip'] = $_SERVER['REMOTE_ADDR'];
        $session['employee_session_user_agent'] = $_SERVER['HTTP_USER_AGENT'];
        $this->db->insert('employee_login', $session);

        $this->session->set_userdata('employee', $user);
        $this->session->set_userdata('token', $session['employee_session_token']);
    }
    
    
    public function logout_user(){
        
        $this->db->query('SET time_zone = "+05:30";');
        $session = array();
        $this->db->set('employee_logout_time', 'NOW()', FALSE);
        $this->db->where('employee_session_token', $this->session->userdata('token'));
        $this->db->update('employee_login', $session);
            
        $this->session->set_userdata('employee', '');
        $this->session->set_userdata('oc', '');
        
        $this->session->unset_userdata('employee');
        $this->session->unset_userdata('oc');       
    }
    
    function crypto_rand_secure($min, $max){
        $range = $max - $min;
        if ($range < 1) return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd > $range);
        return $min + $rnd;
    }

    public function get_token($length){
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->crypto_rand_secure(0, $max-1)];
        }

        return $token;
    }

   
}
