<?php

class Quotation_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_all_quotations_for_report(){
        $result = $this->db->query("SELECT 
                                    CONCAT('QTN',
                                    LPAD(quotation_id, '5', '0')) AS quotation_id,
                                    quotation_linked_company_display_name as customer,
                                    quotation_date,
                                    quotation_linked_company_billing_address as customer_billing_address,
                                    quotation_linked_company_billing_state_name as customer_state
                                FROM
                                    quotation;");
        return $result->result_array();
    }
    /*
     * Delete Quotation
     */

    function delete_draft_quotation_by_id($quotation_id) {
        log_message('debug', 'delete_draft_quotation_by_id. - $id = ' . print_r($quotation_id, 1));

        $this->db->where('qe_linked_quotation_id', $quotation_id);
        $this->db->delete('draft_quotation_entries');

        $this->db->where('quotation_id', $quotation_id);
        $this->db->delete('draft_quotation');

        log_message('debug', 'deleteQuotationBy. - Query = ' . $this->db->last_query());

        if ($this->db->affected_rows() == '1') {
            log_message('debug', 'delete_draft_quotation_by_id. - DELETED ');
            return TRUE;
        } else {
            log_message('debug', 'delete_draft_quotation_by_id. - FALIED TO DELETE ');
            return FALSE;
        }
    }

    /*
     * Retrieve all quotations
     */

    public function get_all_quotations() {
        $result = $this->db->query('SELECT 
                                    *
                                FROM
                                        quotation
                                LEFT JOIN
                                        employee ON employee.employee_id = quotation.quotation_record_created_by
                                ORDER BY
                                    quotation_id DESC;');
        return $result->result_array();
    }

    /*
     * Retrieve all draft quotations
     */

    public function get_all_draft_quotations() {
        $result = $this->db->query('SELECT 
                                        *
                                    FROM
                                        draft_quotation
                                    LEFT JOIN
                                        employee ON employee.employee_id = draft_quotation.quotation_record_created_by;');
        return $result->result_array();
    }

    /**
     * Get entries for an quotation
     */
    public function get_quotation_entries($quotation_id) {

        $query = 'SELECT 
                    *
                    FROM quotation_entries 
                    WHERE
                    qe_linked_quotation_id = ' . $quotation_id . ' group by qe_id;';

        $result = $this->db->query($query);
        return $result->result_array();
    }

    /*
     * Get quotation using quotation_id
     */

    public function get_quotation_with_id($quotation_id) {
        $query = $this->db->get_where('quotation', array('quotation_id' => $quotation_id));
        return $query->row_array();
    }

    /**
     * Get entries for an quotation
     */
    public function get_draft_quotation_entries($quotation_id) {
        $query = 'SELECT 
                    *
                    FROM draft_quotation_entries 
                    WHERE
                    qe_linked_quotation_id = ' . $quotation_id . ' group by qe_id;';

        $result = $this->db->query($query);
        return $result->result_array();
    }

    /*
     * Get quotation using quotation_id
     */

    public function get_draft_quotation_with_id($quotation_id) {
        $query = $this->db->get_where('draft_quotation', array('quotation_id' => $quotation_id));
        return $query->row_array();
    }

    /*
     * Add new quotation

      0 - Tax
      1 - HSN
      2 - rate
      3 - quantity
      4 - discount

     * IMPORTANT - while editing quotation entries, always delete old entries and insert edited ones so that new cne_id are assgined. Failure to
     * do so may lead to error while calculating inventory
     */

    public function save_quotation($quotation_id, $quotation, $quotation_entries, $quotation_action) {

        if ($quotation_action == 'previewDraft') {
            $quotation_action = 'draft';
        }

        log_message('debug', 'add_quotation. - $quotation = ' . print_r($quotation, 1));
        date_default_timezone_set('Asia/Kolkata');  //PHP server time zone. This won't set mySQl time zone. Needs to be set separately.
        $this->load->model('company_model');
        $this->load->model('owner_company_model');
        $this->load->model('payment_term_model');
        $this->load->model('state_model');
        $this->load->model('transporter_model');

        //add other data
        $owner_company = $this->owner_company_model->get_owner_company();

        $quotation['quotation_oc_name'] = $owner_company['oc_billing_name'];
        $quotation['quotation_oc_address'] = $owner_company['oc_billing_address'];
        $quotation['quotation_oc_city_name'] = $owner_company['city_name'];
        if(!empty($owner_company['oc_pincode'])){
            $quotation['quotation_oc_city_name'].= ' - '.$owner_company['oc_pincode'];
        }
        $quotation['quotation_oc_gstin'] = $owner_company['oc_gst_number'];
        $quotation['quotation_oc_pan_number'] = $owner_company['oc_pan_number'];
        $quotation['quotation_oc_logo_path'] = $owner_company['oc_logo_path'];
        $quotation['quotation_oc_district'] = $owner_company['district'];
        $quotation['quotation_oc_state'] = $owner_company['billing_state_name'];
        $quotation['quotation_oc_contact_number'] = $owner_company['oc_contact_number'];
        
        //$quotation['quotation_terms'] = $owner_company['oc_invoice_terms'];
        //$quotation['quotation_additional_terms'] = $owner_company['oc_additional_terms'];

        $this->db->query('SET time_zone = "+05:30";');

        $quotation_date = $quotation['quotation_date'];

        $payment_term = $this->payment_term_model->get_payment_term_using_id($quotation['quotation_linked_company_payment_term_id']);
        $quotation['quotation_linked_company_payment_term_name'] = $payment_term['payment_term_display_text'];

        /* add time component to date if quotation date is today.
         * else
         * add 23:59:59 if the quotation was created on a later date
         * This timestamp is to determine entry sequence for FIFO inventory
         * To be done for Invoice, quotation, credit note, debit note, inventory adjustment
         */
        if (date('Y-m-d', strtotime($quotation['quotation_date'])) == date("Y-m-d")) {
            $quotation['quotation_date'] = date("Y-m-d H:i:sa");
        } else {
            $quotation['quotation_date'] = date("Y-m-d 23:59:59", strtotime($quotation_date));
        }

        if (!empty($quotation['quotation_linked_company_id'])) {
            $linked_company_id = $quotation['quotation_linked_company_id'];
            $linked_company = $this->company_model->get_company_with_id($linked_company_id);
            $quotation['quotation_linked_company_contact_number'] = $linked_company['company_contact_number'];
        } else {
            $quotation['quotation_linked_company_id'] = NULL;
            $quotation['quotation_linked_company_contact_number'] = NULL;
        }

        if(empty($quotation['quotation_revision_number'])){
            $quotation['quotation_revision_number'] = NULL;
        }

        if(empty($quotation['quotation_revision_date'])){
            $quotation['quotation_revision_date'] = NULL;
        }

        $quotation['quotation_linked_company_billing_state_name'] = $this->state_model->get_state_with_id($quotation['quotation_linked_company_billing_state_id'])['state_name'];

        $this->db->trans_begin();
        $count = 0;
        $this->db->query('SET time_zone = "+05:30";');
        if ($quotation['quotation_status'] == 'new') {
            $quotation['quotation_status'] = $quotation_action;
            if ($quotation_action == 'confirm') {
                $this->db->insert('quotation', $quotation);
            } else if ($quotation_action == 'draft') {
                $this->db->insert('draft_quotation', $quotation);
            } else {
                throw new Exception('Invalid quotation action for insert');
            }
            $quotation_id = $this->db->insert_id();
        } else {
            // user is editing an existing quotation. 
            if ($quotation_action == $quotation['quotation_status']) {

                if ($quotation_action == 'draft') {

                    $this->db->where('qe_linked_quotation_id', $quotation_id);
                    $this->db->delete('draft_quotation_entries');

                    $this->db->where('quotation_id', $quotation_id);
                    $this->db->update('draft_quotation', $quotation);
                } else if ($quotation_action == 'confirm') {

                    //retain time component if date not edited by user
                    $quotation_last_copy = $this->get_quotation_with_id($quotation_id);
                    if (date('Y-m-d', strtotime($quotation['quotation_date'])) == strtotime($quotation_last_copy['quotation_date'])) {
                        $quotation['quotation_date'] = $quotation_last_copy['quotation_date'];
                    }

                    $this->db->where('qe_linked_quotation_id', $quotation_id);
                    $this->db->delete('quotation_entries');

                    $this->db->where('quotation_id', $quotation_id);
                    $this->db->update('quotation', $quotation);
                } else {
                    throw new Exception('Invalid quotation action for update');
                }
            } else if ($quotation['quotation_status'] == 'draft' && $quotation_action == 'confirm') {   //confirming a draft quotation
                //delete draft quotation
                $this->db->where('qe_linked_quotation_id', $quotation_id);
                $this->db->delete('draft_quotation_entries');
                $this->db->where('quotation_id', $quotation_id);
                $this->db->delete('draft_quotation');
                
                $quotation['quotation_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
                $quotation['quotation_status'] = $quotation_action;
                $this->db->insert('quotation', $quotation);
                $quotation_id = $this->db->insert_id();
            } else {
                throw new Exception('Invalid quotation status and required action');
            }
        }

        if ($quotation_id > 0) {
            $qe['qe_linked_quotation_id'] = $quotation_id;

            foreach ($quotation_entries as $entry_id => $quotation_entry) {
                $qe['qe_entry_id'] = $entry_id;
                foreach ($quotation_entry as $product_id => $product_info) {
                    $qe['qe_linked_product_id'] = $product_id;

                    $qe['qe_product_name'] = $product_info[0];
                    $qe['qe_product_rate'] = $product_info[1];
                    $qe['qe_product_quantity'] = $product_info[2];
                    $qe['qe_product_uqc_id'] = $product_info[3];
                    $qe['qe_product_uqc_text'] = $product_info[4];

                    if ($quotation_action == 'confirm') {
                        $this->db->insert('quotation_entries', $qe);
                    } else if ($quotation_action == 'draft') {
                        $this->db->insert('draft_quotation_entries', $qe);
                    } else {
                        throw new Exception('Invalid quotation action while inserting quotation entries');
                    }
                    if ($this->db->insert_id() > 0) {
                        $count++;
                    }
                }
            }
        } else {
            $response['result'] = $this->db->error();
            $response['query'] = $this->db->last_query();
        }


        if ($count > 0) {
            $this->db->trans_commit();
            $response['Result'] = "Success";
        } else {
            log_message('debug', 'confirm_dispatch. ROLLBACK. ' . print_r($response));
            $this->db->trans_rollback();
        }

        log_message('debug', 'edit_quotation. - Query = ' . $this->db->last_query());
        return $quotation_id;
    }

}
