<?php

class Owner_company_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }
    
    /**
     * 
     * @param type $pr_opening_balances
     * @throws Exception
     */
    public function save_pr_opening_balance($pr_opening_balances){
        
        foreach ($pr_opening_balances as $entity_class_id => $pr_entity_wise_opening_balances) {
            foreach ($pr_entity_wise_opening_balances as $pr_entity_id => $pr_balance_amount) {
                
                $data = array();
                if(!empty($pr_balance_amount[0])){ 
                    $transaction_type = 'debit';
                    $amount = $pr_balance_amount[0];
                } else if(!empty($pr_balance_amount[1])){
                    $transaction_type = 'credit';
                    $amount = $pr_balance_amount[1];
                } else {
                    $transaction_type = NULL;
                    $amount = NULL;
                }
                
                //throw new Exception(print_r($pr_opening_balances));
                switch ($entity_class_id){
                    case 0: //broker
                        $data['broker_opening_balance'] = $amount;
                        $data['broker_opening_balance_transaction_type'] = $transaction_type;
                        $id_column_name = 'broker_id';
                        $table_name = 'broker';
                        break;
                    case 1: //company
                        $data['company_opening_balance'] = $amount;
                        $data['company_opening_balance_transaction_type'] = $transaction_type;
                        $id_column_name = 'company_id';
                        $table_name = 'company';
                        break;
                    case 2: //employee
                        $data['employee_opening_balance'] = $amount;
                        $data['employee_opening_balance_transaction_type'] = $transaction_type;
                        $id_column_name = 'employee_id';
                        $table_name = 'employee';
                        break;
                    case 3: //transporter
                        $data['transporter_opening_balance'] = $amount;
                        $data['transporter_opening_balance_transaction_type'] = $transaction_type;
                        $id_column_name = 'transporter_id';
                        $table_name = 'transporter';
                        break;

                    default:
                        throw new Exception('Unknown entity type');
                }
                
                $this->db->where($id_column_name, $pr_entity_id);
                $this->db->update($table_name, $data);
            }
        }
        
        /*adjust trade receivables and payables
            $broker_balance_query = "SELECT DISTINCT
                                IFNULL((SELECT 
                                        SUM(debit_balance_table.broker_opening_balance) AS debit_balance
                                    FROM
                                        broker AS debit_balance_table
                                    WHERE
                                        broker_opening_balance_transaction_type = 'debit'),
                                0) - IFNULL((SELECT 
                                        SUM(credit_balance_table.broker_opening_balance)
                                    FROM
                                        broker AS credit_balance_table
                                    WHERE
                                        broker_opening_balance_transaction_type = 'credit'),
                                0) AS balance
                            FROM
                                broker";

            $broker_balance = $this->db->query($broker_balance_query)->row_array()['balance'];
            
            $customer_balance_query = "SELECT DISTINCT
                                IFNULL((SELECT 
                                        SUM(debit_balance_table.company_opening_balance) AS debit_balance
                                    FROM
                                        company AS debit_balance_table
                                    WHERE
                                        company_opening_balance_transaction_type = 'debit'
                                        AND company_type = 'customer'),
                                0) - IFNULL((SELECT 
                                        SUM(credit_balance_table.company_opening_balance)
                                    FROM
                                        company AS credit_balance_table
                                    WHERE
                                        company_opening_balance_transaction_type = 'credit'
                                        AND company_type = 'customer'),
                                0) AS balance
                            FROM
                                company";

            $customer_balance = $this->db->query($customer_balance_query)->row_array()['balance'];
            
            $vendor_balance_query = "SELECT DISTINCT
                                IFNULL((SELECT 
                                        SUM(debit_balance_table.company_opening_balance) AS debit_balance
                                    FROM
                                        company AS debit_balance_table
                                    WHERE
                                        company_opening_balance_transaction_type = 'debit'
                                        AND company_type = 'vendor'),
                                0) - IFNULL((SELECT 
                                        SUM(credit_balance_table.company_opening_balance)
                                    FROM
                                        company AS credit_balance_table
                                    WHERE
                                        company_opening_balance_transaction_type = 'credit'
                                        AND company_type = 'vendor'),
                                0) AS balance
                            FROM
                                company";

            $vendor_balance = $this->db->query($vendor_balance_query)->row_array()['balance'];
            
            $employee_balance_query = "SELECT DISTINCT
                                IFNULL((SELECT 
                                        SUM(debit_balance_table.employee_opening_balance) AS debit_balance
                                    FROM
                                        employee AS debit_balance_table
                                    WHERE
                                        employee_opening_balance_transaction_type = 'debit'),
                                0) - IFNULL((SELECT 
                                        SUM(credit_balance_table.employee_opening_balance)
                                    FROM
                                        employee AS credit_balance_table
                                    WHERE
                                        employee_opening_balance_transaction_type = 'credit'),
                                0) AS balance
                            FROM
                                employee";

            $employee_balance = $this->db->query($employee_balance_query)->row_array()['balance'];
            
            $transporter_balance_query = "SELECT DISTINCT
                                IFNULL((SELECT 
                                        SUM(debit_balance_table.transporter_opening_balance) AS debit_balance
                                    FROM
                                        transporter AS debit_balance_table
                                    WHERE
                                        transporter_opening_balance_transaction_type = 'debit'),
                                0) - IFNULL((SELECT 
                                        SUM(credit_balance_table.transporter_opening_balance)
                                    FROM
                                        transporter AS credit_balance_table
                                    WHERE
                                        transporter_opening_balance_transaction_type = 'credit'),
                                0) AS balance
                            FROM
                                transporter";

            $transporter_balance = $this->db->query($transporter_balance_query)->row_array()['balance'];
            
            $receivables_query = 'Select * from accounting_setting WHERE as_for = "general" && as_field_name = "receivables"';
            $receivables_result = $this->db->query($receivables_query)->row_array();
            $receivables_account_coa_id = $receivables_result['as_debit_account_coa_id'];
            
            $receivables_coa = array();
            $receivables_coa['coa_opening_balance'] = abs($customer_balance);

            if ($customer_balance < 0) {
                $receivables_coa['coa_opening_balance_transaction_type'] = 'credit';
            } else {
                $receivables_coa['coa_opening_balance_transaction_type'] = 'debit';
            }

            $this->db->where('coa_id', $receivables_account_coa_id);
            $this->db->update('chart_of_accounts', $receivables_coa);
            
            //payables
            $net_payable = $vendor_balance + $broker_balance + $transporter_balance + $employee_balance;
            $payables_query = 'Select * from accounting_setting WHERE as_for = "general" && as_field_name = "payables"';
            $payables_result = $this->db->query($payables_query)->row_array();
            $payables_account_coa_id = $payables_result['as_debit_account_coa_id'];
            
            $payables_coa = array();
            $payables_coa['coa_opening_balance'] = abs($net_payable);

            if ($net_payable < 0) {
                $payables_coa['coa_opening_balance_transaction_type'] = 'credit';
            } else {
                $payables_coa['coa_opening_balance_transaction_type'] = 'debit';
            }

            $this->db->where('coa_id', $payables_account_coa_id);
            $this->db->update('chart_of_accounts', $payables_coa); */
            
            $this->load->model('inventory_model');
            $this->inventory_model->update_adjustment_account();
    }
    
    /**
     * 
     * @return type
     */
    public function get_all_payables(){
        $query = "SELECT 
                    broker_id AS pr_entity_id,
                    broker_name AS pr_entity_name,
                    'broker' AS pr_entity_type,
                    'broker' AS pr_entity_type_display,
                    0 AS pr_entity_class_id,
                    'BRK' AS pr_entity_prefix,
                    broker_opening_balance AS pr_opening_balance,
                    broker_opening_balance_transaction_type AS pr_opening_balance_transaction_type
                FROM
                    broker 
                UNION ALL SELECT 
                    company_id AS pr_entity_id,
                    company_display_name AS pr_entity_name,
                    'company' AS pr_entity_type,
                    'customer' AS pr_entity_type_display,
                    1 AS pr_entity_class_id,
                    'COM' AS pr_entity_prefix,
                    company_opening_balance AS pr_opening_balance,
                    company_opening_balance_transaction_type AS pr_opening_balance_transaction_type
                FROM
                    company
                WHERE
                    company_type = 'vendor' 
                UNION ALL SELECT 
                    employee_id AS pr_entity_id,
                    employee_name AS pr_entity_name,
                    'employee' AS pr_entity_type,
                    'employee' AS pr_entity_type_display,
                    2 AS pr_entity_class_id,
                    'EMP' AS pr_entity_prefix,
                    employee_opening_balance AS pr_opening_balance,
                    employee_opening_balance_transaction_type AS pr_opening_balance_transaction_type
                FROM
                    employee 
                UNION ALL SELECT 
                    transporter_id AS pr_entity_id,
                    transporter_name AS pr_entity_name,
                    'transporter' AS pr_entity_type,
                    'transporter' AS pr_entity_type_display,
                    3 AS pr_entity_class_id,
                    'TRN' AS pr_entity_prefix,
                    transporter_opening_balance AS pr_opening_balance,
                    transporter_opening_balance_transaction_type AS pr_opening_balance_transaction_type
                FROM
                    transporter";
        
        $result = $this->db->query($query);
        return $result->result_array();
    }
    
    /**
     * 
     * @return type
     */
    public function get_all_receivables(){
        $query = "SELECT 
                    company_id AS pr_entity_id,
                    company_display_name AS pr_entity_name,
                    'company' AS pr_entity_type,
                    'customer' AS pr_entity_type_display,
                    'COM' AS pr_entity_prefix,
                    1 AS pr_entity_class_id,
                    company_opening_balance AS pr_opening_balance,
                    company_opening_balance_transaction_type AS pr_opening_balance_transaction_type
                FROM
                    company
                WHERE
                    company_type = 'customer'";
        
        $result = $this->db->query($query);
        return $result->result_array();
    }

    /*
     * Retrieve owner_company
     */

    public function get_owner_company() {
        $result = $this->db->query('SELECT 				*,
                                        billing_state.state_name AS billing_state_name,
                                        supply_state.state_name AS supply_state_name
                                    FROM
                                        owner_company
                                    LEFT JOIN
                                        location AS billing_location
                                    ON 
                                        billing_location.location_id = owner_company.oc_location_id
                                    LEFT JOIN
                                        state AS billing_state
                                    ON 
                                        billing_state.state_tin_number = billing_location.state_id
                                    LEFT JOIN
                                        state AS supply_state
                                    ON 
                                        supply_state.state_tin_number = owner_company.oc_gst_supply_state_id;');
        return $result->row_array();
    }

    /**
     * 
     * @param type $accounting_settings
     * @throws Exception
     */
    public function update_accounting_preferences($accounting_settings) {
        try {
            foreach ($accounting_settings as $as_id => $coa_ids) {
                foreach ($coa_ids as $key => $coa_id) {
                    $accounting_setting = array();
                    $this->db->where('as_id', $as_id);
                    if ($key == 0) {
                        $accounting_setting['as_debit_account_coa_id'] = $coa_id;
                    } else if ($key == 1) {
                        $accounting_setting['as_credit_account_coa_id'] = $coa_id;
                    } else {
                        throw new Exception('Found value other than Credit/Debit - ' . $key);
                    }
                    $this->db->update('accounting_setting', $accounting_setting);
                }
            }
        } finally {
            $this->load->model('system_journal_model');
            $this->system_journal_model->calculate_system_journal();
        }
    }

    /*
     * Edit owner_company using owner_company_id
     */

    public function edit_owner_company($data) {

        log_message('debug', 'edit_owner_company. - $data = ' . print_r($data, 1));

        $this->db->query('SET time_zone = "+05:30";');
        $this->db->where('oc_id', '1');
        $this->db->update('owner_company', $data);

        log_message('debug', 'edit_owner_company. - Query = ' . $this->db->last_query());

        if ($this->db->affected_rows() > 0) {
            $response['Result'] = "Success";
        } else {
            $response['Result'] = "No Records Updated";
        }
        $response['query'] = $this->db->last_query();
        log_message('debug', 'edit_owner_company. - response = ' . print_r($response, 1));
        return $response;
    }

    /*
     * Add new owner_company
     */

    public function add_owner_company($data) {
        log_message('debug', 'add_owner_company. - $data = ' . print_r($data, 1));

        $this->db->query('SET time_zone = "+05:30";');
        if ($this->db->insert('owner_company', $data)) {
            $response['Result'] = "Success";
        } else {
            $response['Result'] = $this->db->error();
            $response['query'] = $this->db->last_query();
        }
        log_message('debug', 'edit_owner_company. - Query = ' . $this->db->last_query());
        log_message('debug', 'edit_owner_company. - response = ' . print_r($response, 1));
        return $response;
    }

    /**
     * 
     * @param type $as_for
     * @return type
     */
    public function get_accounting_settings_for($as_for) {
        $result = $this->db->query('SELECT 
                                        as_id,
                                        as_field_display_name,
                                        debit_coa.coa_id AS debit_account_coa_id,
                                        debit_coa.coa_account_name AS debit_account,
                                        credit_coa.coa_id AS credit_account_coa_id,
                                        credit_coa.coa_account_name AS credit_account
                                    FROM
                                        accounting_setting
                                            LEFT JOIN
                                        chart_of_accounts AS debit_coa ON debit_coa.coa_id = accounting_setting.as_debit_account_coa_id
                                            LEFT JOIN
                                        chart_of_accounts AS credit_coa ON credit_coa.coa_id = accounting_setting.as_credit_account_coa_id
                                            WHERE 
                                    as_for = "' . $as_for . '"');
        return $result->result_array();
    }

    /**
     * 
     * @param type $field_name
     * @return type
     */
    public function get_accounting_settings_for_field($field_name) {
        $result = $this->db->query('SELECT 
                                        as_id,
                                        as_field_display_name,
                                        debit_coa.coa_id AS debit_account_coa_id,
                                        debit_coa.coa_account_name AS debit_account,
                                        credit_coa.coa_id AS credit_account_coa_id,
                                        credit_coa.coa_account_name AS credit_account
                                    FROM
                                        accounting_setting
                                            LEFT JOIN
                                        chart_of_accounts AS debit_coa ON debit_coa.coa_id = accounting_setting.as_debit_account_coa_id
                                            LEFT JOIN
                                        chart_of_accounts AS credit_coa ON credit_coa.coa_id = accounting_setting.as_credit_account_coa_id
                                            WHERE 
                                    as_unique_field_name = "' . $field_name . '"');
        return $result->row_array();
    }

}
