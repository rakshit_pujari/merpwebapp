<?php

class Location_model extends CI_Model {

    public function __construct() {
        //$this->db = $this->load->database('elevate', TRUE);
        $this->load->database();
    }

    /*
     * Delete Location
     */

    function delete_location_by_id($id) {
        log_message('debug', 'deleteLocationBy. - $id = ' . print_r($id, 1));

        $this->db->where('location_id', $id);
        $this->db->delete('location');

        log_message('debug', 'deleteLocationBy. - Query = ' . $this->db->last_query());

        if ($this->db->affected_rows() == '1') {
            log_message('debug', 'deleteLocationBy. - DELETED ');
            return TRUE;
        } else {
            log_message('debug', 'deleteLocationBy. - FALIED TO DELETE ');
            return FALSE;
        }
    }

    /*
     * Retrieve all locations
     */

    public function get_all_locations() {
        $result = $this->db->query('SELECT 
                                        location.location_id,
                                        location.city_name,
                                        location.district,
                                        location.state_id,
                                        location.location_record_creation_time,
                                        employee_username,
                                        state_name
                                FROM
                                        location
                                LEFT JOIN
                                        employee ON employee.employee_id = location.location_record_created_by
                                LEFT JOIN
                                        state ON state.state_tin_number = location.state_id;');
        return $result->result_array();
    }
    
       /*
     * Retrieve all locations
     */

    public function get_all_locations_for_report() {
        $result = $this->db->query('SELECT 
                                        CONCAT("LOC", LPAD(location_id, "5", "0")) AS location_id,
                                        city_name,
                                        district,
                                        state_name
                                FROM
                                        location
                                LEFT JOIN
                                        state ON state.state_tin_number = location.state_id;');
        return $result->result_array();
    }

    /*
     * Get location using location_id
     */

    public function get_location_with_id($location_id) {
        $query = $this->db->get_where('location', array('location_id' => $location_id));
        return $query->row_array();
    }


    /**
     * Save a location
     */
    public function save_location($data, $location_id = NULL) {
        log_message('debug', 'save_location. - $id = ' . print_r($location_id, 1) . '$data = ' . print_r($data, 1));
        if($location_id == NULL){
            $this->db->query('SET time_zone = "+05:30";');
            $location_id = $this->db->insert('location', $data);
        } else {
            $this->db->where('location_id', $location_id);
            $this->db->update('location', $data);
        }
        
        $response['query'] = $this->db->last_query();
        log_message('debug', 'save_location. - response = ' . print_r($response, 1));
        return $response;
    } 
}
