<?php

class Invoice_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }
    
    public function get_all_invoices_for_report(){
        $result = $this->db->query("SELECT 
                                    CONCAT('INV', LPAD(invoice_id, '5', '0')) AS invoice_id,
                                    invoice_date,
                                    invoice_linked_company_display_name as customer_name,
                                    invoice_linked_company_billing_address as customer_address,
                                    invoice_amount,
                                    (invoice_amount 
                                    - (SELECT IFNULL(SUM(iar_allocated_amount), 0) FROM invoice_advance_receipt WHERE iar_invoice_id = invoice_id)
                                    - (SELECT IFNULL(SUM(IFNULL(cn_amount, 0)), 0) FROM credit_note WHERE cn_linked_invoice_id = invoice_id)
                                    + (SELECT IFNULL(SUM(IFNULL(dn_amount, 0)), 0) FROM debit_note WHERE dn_linked_invoice_id = invoice_id)
                                    - (SELECT IFNULL(SUM(IFNULL(receipt_amount, 0)), 0) FROM receipt WHERE receipt_linked_invoice_id = invoice_id)                                
                                    ) AS receipt_balance
                                FROM
                                    invoice;");
        return $result->result_array();
    }

    /**
     * Get all linked documents
     * @param type $invoice_id
     * @return type
     */
    public function get_linked_documents($invoice_id) {
        $result = $this->db->query('select advance_receipt_id as document_id, "advance_receipt" as document_type from invoice_advance_receipt'
                                    . ' LEFT JOIN advance_receipt ON advance_receipt.advance_receipt_id = invoice_advance_receipt.iar_advance_receipt_id where iar_invoice_id = ' . $invoice_id
                                    . ' UNION'
                                    . ' select cn_id as document_id, "credit_note" as document_type from credit_note where cn_linked_invoice_id = ' . $invoice_id
                                    . ' UNION
                                    select dn_id as document_id, "debit_note" as document_type from debit_note where dn_linked_invoice_id = ' . $invoice_id
                                    . ' UNION
                                    select receipt_id as document_id, "receipt" as document_type from receipt where receipt_linked_invoice_id = ' . $invoice_id);
        return $result->result_array();
    }


    /**
     * 
     * @param type $invoice_id
     * @return type
     */
    public function get_invoice_customer_map($invoice_id = NULL) {
        //$result = $this->db->query('Select invoice_id, invoice_linked_company_id, invoice_amount, invoice_linked_company_invoice_name from invoice where invoice_linked_company_invoice_name!="null";');
        $query = 'SELECT invoice_id, invoice_date, invoice_amount, invoice_linked_company_id, invoice_linked_company_invoice_name, invoice_linked_company_display_name,
                                (SELECT IFNULL(SUM(IFNULL(cn_amount, 0)), 0) FROM credit_note WHERE cn_linked_invoice_id = invoice_id) AS cn_amount,
                                (SELECT IFNULL(SUM(IFNULL(dn_amount, 0)), 0) FROM debit_note WHERE dn_linked_invoice_id = invoice_id) AS dn_amount,
                                (SELECT IFNULL(SUM(iar_allocated_amount), 0) FROM invoice_advance_receipt WHERE iar_invoice_id = invoice_id) as advance_receipt_amount,
                                (SELECT IFNULL(SUM(IFNULL(receipt_amount, 0)), 0) FROM receipt WHERE receipt_linked_invoice_id = invoice_id) AS receipt_amount,
                                (invoice_amount 
                                    - (SELECT IFNULL(SUM(iar_allocated_amount), 0) FROM invoice_advance_receipt WHERE iar_invoice_id = invoice_id)
                                    - (SELECT IFNULL(SUM(IFNULL(cn_amount, 0)), 0) FROM credit_note WHERE cn_linked_invoice_id = invoice_id)
                                    + (SELECT IFNULL(SUM(IFNULL(dn_amount, 0)), 0) FROM debit_note WHERE dn_linked_invoice_id = invoice_id)
                                    - (SELECT IFNULL(SUM(IFNULL(receipt_amount, 0)), 0) FROM receipt WHERE receipt_linked_invoice_id = invoice_id)                                
                                    ) AS receipt_balance
                                FROM invoice';
        if ($invoice_id !== NULL) {
            $query .= ' WHERE invoice_id = ' . $invoice_id;
        }

        $query .= ';';
        $result = $this->db->query($query);
        return $result->result_array();
    }

    /**
     * 
     * @return type
     */
    public function get_all_customers() {
        //$result = $this->db->query('Select DISTINCT(invoice_linked_company_invoice_name), invoice_linked_company_display_name, invoice_linked_company_id from invoice;');
        /* $result = $this->db->query("SELECT 
          invoice_linked_company_display_name,
          invoice_linked_company_invoice_name,
          IFNULL(invoice_linked_company_id, 0) AS invoice_linked_company_id
          FROM
          invoice
          GROUP BY invoice_linked_company_display_name"); */

        $result = $this->db->query('Select invoice_linked_company_display_name, invoice_linked_company_invoice_name, invoice_linked_company_id from invoice GROUP BY invoice_linked_company_display_name;');
        return $result->result_array();
    }

    /*
     * Delete Invoice
     */

    function delete_draft_invoice_by_id($invoice_id) {
        log_message('debug', 'deleteInvoiceBy. - $id = ' . print_r($invoice_id, 1));

        $this->db->where('ipe_linked_invoice_id', $invoice_id);
        $this->db->delete('draft_invoice_product_entries');
              
        $this->db->where('ice_linked_invoice_id', $invoice_id);
        $this->db->delete('draft_invoice_charge_entries');

        $this->db->where('invoice_id', $invoice_id);
        $this->db->delete('draft_invoice');

        log_message('debug', 'deleteInvoiceBy. - Query = ' . $this->db->last_query());

        if ($this->db->affected_rows() == '1') {
            log_message('debug', 'deleteInvoiceBy. - DELETED ');
            return TRUE;
        } else {
            log_message('debug', 'deleteInvoiceBy. - FALIED TO DELETE ');
            return FALSE;
        }
    }

    /*
     * Retrieve all invoices
     */

    public function get_all_invoices() {
        $result = $this->db->query('SELECT 
                                        *,
                                            (invoice_amount 
                                    - (SELECT IFNULL(SUM(iar_allocated_amount), 0) FROM invoice_advance_receipt WHERE iar_invoice_id = invoice_id)
                                    - (SELECT IFNULL(SUM(IFNULL(cn_amount, 0)), 0) FROM credit_note WHERE cn_linked_invoice_id = invoice_id)
                                    + (SELECT IFNULL(SUM(IFNULL(dn_amount, 0)), 0) FROM debit_note WHERE dn_linked_invoice_id = invoice_id)
                                    - (SELECT IFNULL(SUM(IFNULL(receipt_amount, 0)), 0) FROM receipt WHERE receipt_linked_invoice_id = invoice_id)                                
                                    ) AS receipt_balance
                                    FROM
                                        invoice
                                    LEFT JOIN
                                        employee ON employee.employee_id = invoice.invoice_record_created_by
                                    ORDER BY invoice_id DESC;');
        return $result->result_array();
    }

    /*
     * Retrieve all invoices
     */

    public function get_all_draft_invoices() {
        $result = $this->db->query('SELECT 
                                        *
                                    FROM
                                        draft_invoice
                                    LEFT JOIN
                                        employee ON employee.employee_id = draft_invoice.invoice_record_created_by;');
        return $result->result_array();
    }

    /**
     * Get entries for an invoice
     */
    public function get_invoice_entries($invoice_id) {
        //$query = $this->db->get_where('invoice_product_entries', array('ipe_linked_invoice_id' => $invoice_id));
        $query = 'SELECT 
                    *,
                    (ipe_product_quantity - IFNULL(SUM(cne_product_quantity), 0)) AS credit_note_balance
                    FROM invoice_product_entries 
                    LEFT JOIN credit_note_entries
                    ON
                    invoice_product_entries.ipe_id = credit_note_entries.cne_linked_ipe_id
                    WHERE
                    ipe_linked_invoice_id = ' . $invoice_id . ' group by ipe_id;';

        $result = $this->db->query($query);
        return $result->result_array();
    }
    
        /**
     * Get entries for an invoice
     */
    public function get_invoice_charge_entries($invoice_id) {
        //$query = $this->db->get_where('invoice_product_entries', array('ipe_linked_invoice_id' => $invoice_id));
        $query = 'SELECT 
                    *
                    FROM invoice_charge_entries 
                    WHERE
                    ice_linked_invoice_id = ' . $invoice_id . ' group by ice_id;';

        $result = $this->db->query($query);
        return $result->result_array();
    }
    
    
        /**
     * Get entries for an invoice
     */
    public function get_draft_invoice_charge_entries($invoice_id) {
        //$query = $this->db->get_where('invoice_product_entries', array('ipe_linked_invoice_id' => $invoice_id));
        $query = 'SELECT 
                    *
                    FROM draft_invoice_charge_entries 
                    WHERE
                    ice_linked_invoice_id = ' . $invoice_id . ' group by ice_id;';

        $result = $this->db->query($query);
        return $result->result_array();
    }

    /*
     * Get invoice using invoice_id
     */

    public function get_invoice_with_id($invoice_id) {
        $query = $this->db->get_where('invoice', array('invoice_id' => $invoice_id));
        return $query->row_array();
    }

    /**
     * Get entries for an invoice
     */
    public function get_draft_invoice_entries($invoice_id) {
        //$query = $this->db->get_where('invoice_product_entries', array('ipe_linked_invoice_id' => $invoice_id));
        $query = 'SELECT 
                    *
                    FROM draft_invoice_product_entries 
                    WHERE
                    ipe_linked_invoice_id = ' . $invoice_id . ' group by ipe_id;';

        $result = $this->db->query($query);
        return $result->result_array();
    }

    /*
     * Get invoice using invoice_id
     */

    public function get_draft_invoice_with_id($invoice_id) {
        $query = $this->db->get_where('draft_invoice', array('invoice_id' => $invoice_id));
        return $query->row_array();
    }

    /**
      Perform all calculations here itself before sending to client to avoid code related to calculation on different views on clients -  example : on draft page, preview page, android app.
     */
    public function get_entire_invoice_data($invoice_id) {
        $invoice = $this->get_invoice_with_id($invoice_id);
        $invoice_entries = $this->get_invoice_entries($invoice_id);
        $total_discount = 0;
        $total_taxable_amount = 0;

        $tax_amount_map = array();
        foreach ($invoice_entries as $key => $invoice_entry) {
            //1
            $discount = $invoice_entry['ipe_discount'] * $invoice_entry['ipe_product_rate'] / 100;
            $total_discount = $total_discount + ( $discount * $invoice_entry['ipe_product_quantity']);

            //2
            if ($invoice['invoice_tax_type'] == 'inclusive') {
                $taxable_amount = $invoice_entry['ipe_product_quantity'] * ( $invoice_entry['ipe_product_rate'] - $discount ) / ( 1 + $invoice_entry['ipe_tax_percent_applicable'] / 100);
            } else if ($invoice['invoice_tax_type'] == 'exclusive') {
                $taxable_amount = $invoice_entry['ipe_product_quantity'] * ( $invoice_entry['ipe_product_rate'] - $discount );
            }

            $total_taxable_amount = $total_taxable_amount + $taxable_amount;

            $ipe_tax_percent_applicable = $invoice_entry['ipe_tax_percent_applicable'];
            $tax_amount = $taxable_amount * $invoice_entry['ipe_tax_percent_applicable'] / 100;

            if (!isset($tax_amount_map[$ipe_tax_percent_applicable])) {
                $tax_amount_map[$ipe_tax_percent_applicable] = $tax_amount;
            } else {
                $stored_tax = $tax_amount_map[$ipe_tax_percent_applicable];
                $tax_amount_map[$ipe_tax_percent_applicable] = $stored_tax + $tax_amount;
            }

            $invoice_entry['ipe_taxable_amount'] = $taxable_amount;
            $invoice_entry['ipe_tax_amount'] = $tax_amount;
            $invoice_entry['ipe_discount_per_item'] = $discount;

            $invoice_entries[$key] = $invoice_entry;
        }

        $invoice['total_discount'] = $total_discount;
        $invoice['total_taxable_amount'] = $total_taxable_amount;
        $invoice['tax_amount_map'] = $tax_amount_map;

        $invoice_data['invoice'] = $invoice;
        $invoice_data['invoice_entries'] = $invoice_entries;

        return $invoice_data;
    }

    /*
     * Add new invoice

      0 - Tax
      1 - HSN
      2 - rate
      3 - quantity
      4 - discount
     * 
     * IMPORTANT - while editing invoice entries, always delete old entries and insert edited ones so that new cne_id are assgined. Failure to
     * do so may lead to error while calculating inventory

     */

    public function save_invoice($invoice_id, $invoice, $invoice_entries, $invoice_charge_entries, $invoice_action) {

        try {
            if ($invoice_action == 'previewDraft') {
                $invoice_action = 'draft';
            }

            log_message('debug', 'add_invoice. - $invoice = ' . print_r($invoice, 1));

            date_default_timezone_set('Asia/Kolkata');  //PHP server time zone. This won't set mySQl time zone. Needs to be set separately.

            $this->load->model('company_model');
            $this->load->model('owner_company_model');
            $this->load->model('payment_term_model');
            $this->load->model('state_model');
            $this->load->model('transporter_model');
            $this->load->model('advance_receipt_model');
            $this->load->model('unique_quantity_code_model');

            //add other data
            $owner_company = $this->owner_company_model->get_owner_company();

            $invoice['invoice_oc_name'] = $owner_company['oc_billing_name'];
            $invoice['invoice_oc_address'] = $owner_company['oc_billing_address'];
            $invoice['invoice_oc_city_name'] = $owner_company['city_name'];
            $invoice['invoice_oc_gstin'] = $owner_company['oc_gst_number'];
            $invoice['invoice_oc_pan_number'] = $owner_company['oc_pan_number'];
            $invoice['invoice_oc_logo_path'] = $owner_company['oc_logo_path'];
            $invoice['invoice_oc_district'] = $owner_company['district'];
            $invoice['invoice_oc_state'] = $owner_company['billing_state_name'];
            $invoice['invoice_oc_contact_number'] = $owner_company['oc_contact_number'];
            $invoice['invoice_oc_gst_supply_state_id'] = $owner_company['oc_gst_supply_state_id'];
            //$invoice['invoice_terms'] = $owner_company['oc_invoice_terms'];
            //$invoice['invoice_additional_terms'] = $owner_company['oc_additional_terms'];


            if (!empty($invoice['invoice_linked_transporter_id'])) {
                $transporter_id = $invoice['invoice_linked_transporter_id'];
                $invoice['invoice_linked_transporter_name'] = $this->transporter_model->get_transporter_with_id($transporter_id)['transporter_name'];
            } else {
                $invoice['invoice_linked_transporter_id'] = NULL;
                $invoice['invoice_linked_transporter_name'] = '';
            }

            if (empty($invoice['invoice_linked_broker_id'])) {
                $invoice['invoice_linked_broker_id'] = NULL;
            }

            if (empty($invoice['invoice_linked_employee_id'])) {
                $invoice['invoice_linked_employee_id'] = NULL;
            }

            $this->db->query('SET time_zone = "+05:30";');

            $payment_term = $this->payment_term_model->get_payment_term_using_id($invoice['invoice_linked_company_payment_term_id']);
            $invoice['invoice_linked_company_payment_term_name'] = $payment_term['payment_term_display_text'];
            $number_of_days_to_due = $payment_term['payment_term_days'];
            $invoice['invoice_due_date'] = date('Y-m-d', strtotime($invoice['invoice_date'] . ' + ' . $number_of_days_to_due . ' days'));

            /* add time component to date if invoice date is today.
             * else
             * add 23:59:59 if the invoice was created on a later date
             * This timestamp is to determine entry sequence for FIFO inventory
             * To be done for Invoice, purchase, credit note, debit note, inventory adjustment
             */
            if (date('Y-m-d', strtotime($invoice['invoice_date'])) == date("Y-m-d")) {
                $invoice['invoice_date'] = date("Y-m-d H:i:sa");
            } else {
                $invoice['invoice_date'] = date("Y-m-d 23:59:59", strtotime($invoice['invoice_date']));
            }

            if (!empty($invoice['invoice_linked_company_id'])) {
                $linked_company_id = $invoice['invoice_linked_company_id'];
                $linked_company = $this->company_model->get_company_with_id($linked_company_id);
                //$invoice['invoice_linked_company_gstin'] = $linked_company['company_gst_number'];
                $invoice['invoice_is_reverse_charge_applicable'] = $linked_company['is_reverse_charge_applicable_for_company'];
                $invoice['invoice_linked_company_contact_number'] = $linked_company['company_contact_number'];
            } else {
                $invoice['invoice_linked_company_id'] = NULL;
                //$invoice['invoice_linked_company_gstin'] = NULL;
                $invoice['invoice_is_reverse_charge_applicable'] = NULL;
                $invoice['invoice_linked_company_contact_number'] = NULL;
            }

            if(empty($invoice['invoice_linked_company_gstin'])){
                $invoice['invoice_linked_company_gstin'] = NULL;
            } else {
                $invoice['invoice_linked_company_gstin'] = strtoupper($invoice['invoice_linked_company_gstin']);
            }
            $invoice_linked_company_gst_supply_state_id = $invoice['invoice_linked_company_gst_supply_state_id'];
            $invoice['invoice_place_of_supply'] = $this->state_model->get_state_with_id($invoice_linked_company_gst_supply_state_id)['state_name'];

            $invoice['invoice_linked_company_billing_state_name'] = $this->state_model->get_state_with_id($invoice['invoice_linked_company_billing_state_id'])['state_name'];
            $invoice['invoice_linked_company_shipping_state_name'] = $this->state_model->get_state_with_id($invoice['invoice_linked_company_shipping_state_id'])['state_name'];

            $this->db->trans_begin();

            $invoice_advance_receipt_ids = array();
            if(!empty($invoice['invoice_advance_receipt_ids'])){
                $invoice_advance_receipt_ids = $invoice['invoice_advance_receipt_ids'];
            }
            unset($invoice['invoice_advance_receipt_ids']);

            $count = 0;
            $this->db->query('SET time_zone = "+05:30";');
            if ($invoice['invoice_status'] == 'new') {  //creating new invoice
                $invoice['invoice_status'] = $invoice_action;
                if ($invoice_action == 'confirm') {
                    $this->db->insert('invoice', $invoice);
                } else if ($invoice_action == 'draft') {
                    $this->db->insert('draft_invoice', $invoice);
                } else {
                    throw new Exception('Invalid invoice action for insert');
                }
                $invoice_id = $this->db->insert_id();
            } else {        // user is editing an existing invoice. 
                if ($invoice_action == $invoice['invoice_status']) {

                    if ($invoice_action == 'draft') {

                        $this->db->where('ipe_linked_invoice_id', $invoice_id);
                        $this->db->delete('draft_invoice_product_entries');
                        
                        $this->db->where('ice_linked_invoice_id', $invoice_id);
                        $this->db->delete('draft_invoice_charge_entries');

                        $this->db->where('invoice_id', $invoice_id);
                        $this->db->update('draft_invoice', $invoice);
                    } else if ($invoice_action == 'confirm') {

                        //retain time component if date not edited by user
                        $invoice_last_copy = $this->get_invoice_with_id($invoice_id);
                        if (date('Y-m-d', strtotime($invoice['invoice_date'])) == strtotime($invoice_last_copy['invoice_date'])) {
                            $invoice['invoice_date'] = $invoice_last_copy['invoice_date'];
                        }

                        $this->db->where('ipe_linked_invoice_id', $invoice_id);
                        $this->db->delete('invoice_product_entries');
                        
                        $this->db->where('ice_linked_invoice_id', $invoice_id);
                        $this->db->delete('invoice_charge_entries');

                        $this->db->where('invoice_id', $invoice_id);
                        $this->db->update('invoice', $invoice);
                    } else {
                        throw new Exception('Invalid invoice action for update');
                    }
                } else if ($invoice['invoice_status'] == 'draft' && $invoice_action == 'confirm') {   //confirming a draft invoice
                    //delete advance receipt associations
                    $this->db->where('iar_draft_invoice_id', $invoice_id);
                    $this->db->delete('invoice_advance_receipt');
                    //delete draft invoice
                    $this->db->where('ipe_linked_invoice_id', $invoice_id);
                    $this->db->delete('draft_invoice_product_entries');
                    
                    $this->db->where('ice_linked_invoice_id', $invoice_id);
                    $this->db->delete('draft_invoice_charge_entries');
                    
                    $this->db->where('invoice_id', $invoice_id);
                    $this->db->delete('draft_invoice');

                    $invoice['invoice_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];

                    $invoice['invoice_status'] = $invoice_action;
                    $this->db->insert('invoice', $invoice);
                    $invoice_id = $this->db->insert_id();
                } else {
                    throw new Exception('Invalid invoice status and required action');
                }
            }

            //manage advance receipts
            if ($invoice['invoice_status'] == 'draft') {
                  
                $delete_query = 'Delete from invoice_advance_receipt where iar_draft_invoice_id = "' . $invoice_id . '"';
                if(sizeof($invoice_advance_receipt_ids) > 0){
                    $delete_query.=' AND iar_advance_receipt_id NOT IN (' . implode(',', $invoice_advance_receipt_ids) . ')';
                }
                
                $this->db->query($delete_query);
                
                foreach ($invoice_advance_receipt_ids as $invoice_advance_receipt_id) {
                    $invoice_advance_receipt_amount = $this->db->query('Select * from '
                                    . 'invoice_advance_receipt where iar_draft_invoice_id = "' . $invoice_id .
                                    '" AND iar_advance_receipt_id = "' . $invoice_advance_receipt_id . '"')->result_array();
                    if (sizeof($invoice_advance_receipt_amount) == 0) {
                        $this->db->insert('invoice_advance_receipt', array('iar_advance_receipt_id' => $invoice_advance_receipt_id,
                            'iar_draft_invoice_id' => $invoice_id,
                            'iar_allocated_amount' => 0));
                    }
                }
            } else if ($invoice['invoice_status'] == 'confirm') {
                //delete all unlinked advance receipts
                $delete_query = 'Delete from invoice_advance_receipt where iar_invoice_id = "' . $invoice_id . '"';
                if(sizeof($invoice_advance_receipt_ids) > 0){
                    //$delete_query.=' AND iar_advance_receipt_id NOT IN (' . implode(',', $invoice_advance_receipt_ids) . ')';
                }
                $this->db->query($delete_query);
                //now check if exisitng advance receipts amount against this invoice is equal to the invoice amount
                $advance_receipt_amount_against_invoice = $this->db->query('Select SUM(iar_allocated_amount) as advance_receipt_amount_against_invoice from invoice_advance_receipt where iar_invoice_id = "' . $invoice_id . '"')->row_array()['advance_receipt_amount_against_invoice'];

                if (empty($advance_receipt_amount_against_invoice)) {
                    $advance_receipt_amount_against_invoice = 0;
                }

                $balance_amount = $invoice['invoice_amount'] - $advance_receipt_amount_against_invoice;
                
                foreach ($invoice_advance_receipt_ids as $invoice_advance_receipt_id) {
                    if ($balance_amount > 0) {
                        $advance_receipt = $this->advance_receipt_model->get_advance_receipt_with_id($invoice_advance_receipt_id);

                        //calculate amount
                        //part of the advance receipt amount that has been used
                        $used_advance_receipt_amount = $this->db->query('Select SUM(iar_allocated_amount) as iar_allocated_amount from invoice_advance_receipt '
                                        . 'where iar_advance_receipt_id = "' . $invoice_advance_receipt_id . '"')->row_array()['iar_allocated_amount'];

                        $invoice_advance_receipt_amount = $this->db->query('Select SUM(iar_allocated_amount) as iar_allocated_amount from invoice_advance_receipt '
                                        . 'where iar_invoice_id = "' . $invoice_id . '" AND iar_advance_receipt_id = "' . $invoice_advance_receipt_id . '"')->row_array()['iar_allocated_amount'];
                        
                        if (empty($invoice_advance_receipt_amount) 
                                || $used_advance_receipt_amount > 0) {

                            $available_advance_receipt_amount = $advance_receipt['advance_receipt_amount'] - $used_advance_receipt_amount;

                            if ($balance_amount > $available_advance_receipt_amount) {
                                $this->db->insert('invoice_advance_receipt', array('iar_advance_receipt_id' => $invoice_advance_receipt_id,
                                    'iar_invoice_id' => $invoice_id,
                                    'iar_allocated_amount' => $available_advance_receipt_amount));
                                $balance_amount = $balance_amount - $available_advance_receipt_amount;
                            } else {
                                $this->db->insert('invoice_advance_receipt', array('iar_advance_receipt_id' => $invoice_advance_receipt_id,
                                    'iar_invoice_id' => $invoice_id,
                                    'iar_allocated_amount' => $balance_amount));
                                $balance_amount = 0;
                            }
                        } else {
                            $balance_amount = $balance_amount - $invoice_advance_receipt_amount;
                        }
                    }
                }
            } else {
                throw new Exception('Invalid invoice status');
            }

            //Get all taxes
            $tax_query = $this->db->get('tax');
            $taxes = $tax_query->result_array();
            $tax_id_percent_map = array();
            $tax_id_name_map = array();
            foreach ($taxes as $tax) {
                $tax_id_percent_map[$tax['tax_id']] = $tax['tax_percent'];
                $tax_id_name_map[$tax['tax_id']] = $tax['tax_name'];
            }

            if ($invoice_id > 0) {
                $ipe['ipe_linked_invoice_id'] = $invoice_id;

                foreach ($invoice_entries as $entry_id => $invoice_entry) {
                    $ipe['ipe_entry_id'] = $entry_id;
                    foreach ($invoice_entry as $product_id => $product_info) {
                        $ipe['ipe_linked_product_id'] = $product_id;

                        $ipe['ipe_product_name'] = $product_info[0];
                        $ipe['ipe_tax_id_applicable'] = $product_info[1];
                        $ipe['ipe_tax_percent_applicable'] = $tax_id_percent_map[$product_info[1]]; // fetch tax percent from $tax_id_percent_map
                        $ipe['ipe_tax_name_applicable'] = $tax_id_name_map[$product_info[1]]; // fetch tax name from $tax_id_name_map
                        $ipe['ipe_product_hsn_code'] = $product_info[2];
                        $ipe['ipe_product_rate'] = $product_info[3];
                        $ipe['ipe_product_quantity'] = $product_info[4];
                        //$ipe['ipe_product_uqc_id'] = $product_info[5];
                        //$ipe['ipe_product_uqc_text'] = $product_info[6];
                        $ipe['ipe_product_uqc_id'] = $this->product_model->get_product_with_id($product_id)['product_uqc_id'];
                        $ipe['ipe_product_uqc_text'] = $this->unique_quantity_code_model->get_uqc_with_id($ipe['ipe_product_uqc_id'])['uqc_text'];
                        $ipe['ipe_discount'] = $product_info[7];

                        if ($invoice_action == 'confirm') {
                            $this->db->insert('invoice_product_entries', $ipe);
                        } else if ($invoice_action == 'draft') {
                            $this->db->insert('draft_invoice_product_entries', $ipe);
                        } else {
                            throw new Exception('Invalid invoice action while inserting invoice entries');
                        }
                        if ($this->db->insert_id() > 0) {
                            $count++;
                        }
                    }
                }
                
                //enter charges
                foreach ($invoice_charge_entries as $charge_entry_id => $invoice_charge_entry) {
                    $ice['ice_entry_id'] = $charge_entry_id;
                    foreach ($invoice_charge_entry as $charge_id => $charge_info) {
                        $ice['ice_charge_id'] = $charge_id;
                        $ice['ice_linked_invoice_id'] = $invoice_id;
                        $ice['ice_charge_name'] = $charge_info[0];  
                        $ice['ice_taxable_amount'] = $charge_info[1];
                        
                        $ice['ice_tax_id_applicable'] = $charge_info[2];
                        $ice['ice_tax_percent_applicable'] = $tax_id_percent_map[$charge_info[2]]; // fetch tax percent from $tax_id_percent_map
                        $ice['ice_tax_name_applicable'] = $tax_id_name_map[$charge_info[2]]; // fetch tax name from $tax_id_name_map
                        
                        if ($invoice_action == 'confirm') {
                            $this->db->insert('invoice_charge_entries', $ice);
                        } else if ($invoice_action == 'draft') {
                            $this->db->insert('draft_invoice_charge_entries', $ice);
                        } else {
                            throw new Exception('Invalid invoice action while inserting invoice entries');
                        }
                    }
                }
            } else {
                $response['result'] = $this->db->error();
                $response['query'] = $this->db->last_query();
            }


            if ($count > 0) {
                $this->db->trans_commit();
                $response['result'] = "success";
            } else {
                log_message('debug', 'confirm_dispatch. ROLLBACK. ' . print_r($response));
                $this->db->trans_rollback();
            }

            log_message('debug', 'edit_invoice. - Query = ' . $this->db->last_query());
            return $invoice_id;
        } finally {
            $this->load->model('inventory_model');
            $this->inventory_model->evaluate_inventory_figures();
        }
    }

}
