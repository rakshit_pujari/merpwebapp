<?php

class Advance_receipt_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    /**
     * 
     * @return type
     */
    public function get_all_advance_receipts_for_report(){
        $query = "SELECT 
                    CONCAT('ADR',
                            LPAD(advance_receipt_id, '5', '0')) AS advance_receipt_id,
                    advance_receipt_linked_company_display_name AS customer_name,
                    advance_receipt_date,
                    advance_receipt_amount,
                    GROUP_CONCAT(CONCAT('PUR', LPAD(iar_invoice_id, '5', '0'))) AS linked_invoices,
                    GROUP_CONCAT(CONCAT('DBN', LPAD(iar_credit_note_id, '5', '0'))) AS linked_credit_notes,
                    advance_receipt_receipt_mode,
                    advance_receipt_amount - IFNULL(SUM(iar_allocated_amount), 0) AS balance_advance_receipt_amount
                FROM
                    advance_receipt
                        LEFT JOIN
                    employee ON employee.employee_id = advance_receipt.advance_receipt_record_created_by
                        LEFT JOIN
                    invoice_advance_receipt ON advance_receipt.advance_receipt_id = invoice_advance_receipt.iar_advance_receipt_id
                GROUP BY advance_receipt_id
                ORDER BY advance_receipt_id DESC";
        
        $result = $this->db->query($query);        
        return $result->result_array();
    }
    
    /**
     * 
     * @param type $invoice_status
     * @param type $invoice_id
     * @return type
     * @throws Exception
     */
    public function get_linked_advance_receipts_ids($invoice_status, $invoice_id) {
        $query = 'Select iar_advance_receipt_id from '
                . 'invoice_advance_receipt where ';

        if ($invoice_status == 'draft') {
            $query .= 'iar_draft_invoice_id';
        } else if ($invoice_status == 'confirm') {
            $query .= 'iar_invoice_id';
        } else {
            throw new Exception('Invalid invoice status');
        }

        $query .= ' = "' . $invoice_id . '"';

        $advance_receipts = $this->db->query($query)->result_array();

        $advance_receipt_ids = array();
        foreach ($advance_receipts as $advance_receipt) {
            array_push($advance_receipt_ids, $advance_receipt['iar_advance_receipt_id']);
        }

        return $advance_receipt_ids;
    }
    
    /**
     * Get advance receipts linked to the invoice and any other advance receipts linked to that company with surplus balance
     * @param type $company_id
     * @param type $invoice_id
     * @return type
     */
    public function get_linked_and_surplus_balance_advance_receipts($company_id, $invoice_id){
        
        $query = 'SELECT 
                    advance_receipt_id,
                    advance_receipt_linked_company_invoice_name,
                    advance_receipt_linked_company_id,
                    advance_receipt_record_creation_time,
                    advance_receipt_date,
                    advance_receipt_amount,
                    iar_invoice_id,
                    advance_receipt_receipt_mode,
                    balance_advance_receipt_amount
                FROM
                    (SELECT 
                        advance_receipt_id,
                            advance_receipt_linked_company_invoice_name,
                            advance_receipt_linked_company_id,
                            advance_receipt_record_creation_time,
                            advance_receipt_date,
                            advance_receipt_amount,
                            iar_invoice_id,
                            advance_receipt_receipt_mode,
                            advance_receipt_amount - IFNULL(SUM(iar_allocated_amount), 0) AS balance_advance_receipt_amount
                    FROM
                        advance_receipt
                    LEFT JOIN invoice_advance_receipt ON advance_receipt.advance_receipt_id = invoice_advance_receipt.iar_advance_receipt_id
                    WHERE
                        iar_advance_receipt_id IN (Select iar_advance_receipt_id from invoice_advance_receipt where iar_invoice_id = '.$invoice_id.
                    ') GROUP BY advance_receipt_id UNION ALL SELECT 
                        advance_receipt_id,
                            advance_receipt_linked_company_invoice_name,
                            advance_receipt_linked_company_id,
                            advance_receipt_record_creation_time,
                            advance_receipt_date,
                            advance_receipt_amount,
                            iar_invoice_id,
                            advance_receipt_receipt_mode,
                            advance_receipt_amount - IFNULL(SUM(iar_allocated_amount), 0) AS balance_advance_receipt_amount
                    FROM
                        advance_receipt
                    LEFT JOIN invoice_advance_receipt ON advance_receipt.advance_receipt_id = invoice_advance_receipt.iar_advance_receipt_id
                    WHERE
                        advance_receipt_linked_company_id = '.$company_id.
                    ' GROUP BY advance_receipt_id
                    HAVING (advance_receipt_amount > IFNULL(SUM(iar_allocated_amount), 0))) AS advance
                    group by
                    advance_receipt_id';
        
        $advance_receipts = $this->db->query($query)->result_array();
        return $advance_receipts;
    }
        /**
     * 
     * @param type $invoice_status
     * @param type $invoice_id
     * @return type
     * @throws Exception
     */
    public function get_linked_advance_receipts($invoice_status, $invoice_id) {
        $query = 'Select '
                . 'advance_receipt_id,
                    advance_receipt_linked_company_invoice_name,
                    advance_receipt_linked_company_id,
                    advance_receipt_record_creation_time,
                    advance_receipt_date,
                    advance_receipt_amount,
                    iar_invoice_id,
                    advance_receipt_receipt_mode,
                    advance_receipt_amount - IFNULL(SUM(iar_allocated_amount), 0) AS balance_advance_receipt_amount from '
                . 'advance_receipt'
                . ' LEFT JOIN invoice_advance_receipt ON advance_receipt.advance_receipt_id = invoice_advance_receipt.iar_advance_receipt_id'
                . ' where ';

        if ($invoice_status == 'draft') {
            $query .= 'iar_draft_invoice_id';
        } else if ($invoice_status == 'confirm') {
            $query .= 'iar_invoice_id';
        } else {
            throw new Exception('Invalid invoice status');
        }

        $query .= ' = "' . $invoice_id . '" '
                . 'GROUP BY 
                iar_advance_receipt_id';
        //throw new Exception($query);
        $advance_receipts = $this->db->query($query)->result_array();
        return $advance_receipts;
    }
    
        
        /*
     * Retrieve all advance receipts
     */
    public function get_advance_receipts_with_balance($linked_company_id = NULL) {
        
        $query = 'SELECT
                    advance_receipt_id,
                    advance_receipt_linked_company_invoice_name,
                    advance_receipt_linked_company_id,
                    advance_receipt_record_creation_time,
                    advance_receipt_date,
                    advance_receipt_amount,
                    iar_invoice_id,
                    advance_receipt_receipt_mode,
                    advance_receipt_amount - IFNULL(SUM(iar_allocated_amount), 0) AS balance_advance_receipt_amount
                FROM
                    advance_receipt
                LEFT JOIN invoice_advance_receipt ON advance_receipt.advance_receipt_id = invoice_advance_receipt.iar_advance_receipt_id';
        if($linked_company_id != NULL){
            $query.=' WHERE advance_receipt_linked_company_id = '.$linked_company_id;
        }
        
        $query.=' GROUP BY
                    advance_receipt_id
                HAVING
                    advance_receipt_amount > IFNULL(SUM(iar_allocated_amount), 0)
                    ORDER BY 
                    advance_receipt_id DESC;';
        
        $result = $this->db->query($query);
        
        $advance_receipts = $result->result_array();
        
        foreach ($advance_receipts as $key => $advance_receipt) {
            $advance_receipt['linked_invoices'] = $this->db->query('SELECT 
                                        iar_invoice_id, iar_allocated_amount
                                    FROM
                                        invoice_advance_receipt
                                        WHERE iar_advance_receipt_id = '.$advance_receipt['advance_receipt_id'])->result_array();
            
            $advance_receipts[$key] = $advance_receipt;
        }
        
        return $advance_receipts;
    }
    
    /*
     * Get receipt using advance_receipt_id
     */
    public function get_advance_receipt_with_id($advance_receipt_id) {
        $query = 'SELECT 
                    *,
                    IFNULL(SUM(iar_allocated_amount), 0) AS advance_receipt_total_allocated_amount
                FROM
                    advance_receipt
                LEFT JOIN invoice_advance_receipt ON advance_receipt.advance_receipt_id = invoice_advance_receipt.iar_advance_receipt_id
                WHERE advance_receipt_id = '.$advance_receipt_id;
                
        $result = $this->db->query($query);
        return $result->row_array();
    }

    /*
     * Retrieve all advance receipts
     */
    public function get_all_advance_receipts() {
        
        $query = 'SELECT 
                    advance_receipt_id,
                    advance_receipt_linked_company_display_name,
                    advance_receipt_linked_company_id,
                    employee_username,
                    advance_receipt_record_creation_time,
                    advance_receipt_date,
                    advance_receipt_amount,
                    iar_invoice_id,
                    iar_credit_note_id,
                    advance_receipt_receipt_mode,
                    advance_receipt_amount - IFNULL(SUM(iar_allocated_amount), 0) AS balance_advance_receipt_amount
                FROM
                    advance_receipt
                LEFT JOIN employee ON employee.employee_id = advance_receipt.advance_receipt_record_created_by
                LEFT JOIN invoice_advance_receipt ON advance_receipt.advance_receipt_id = invoice_advance_receipt.iar_advance_receipt_id';

        
        $query.=' GROUP BY
                    advance_receipt_id
                  ORDER BY 
                    advance_receipt_id DESC;';
        
        $result = $this->db->query($query);
        
        $advance_receipts = $result->result_array();
        
        foreach ($advance_receipts as $key => $advance_receipt) {
            $advance_receipt['linked_invoices'] = $this->db->query('SELECT 
                                        iar_invoice_id, iar_allocated_amount, iar_credit_note_id
                                    FROM
                                        invoice_advance_receipt
                                        WHERE iar_advance_receipt_id = '.$advance_receipt['advance_receipt_id'].
                                    '  AND (iar_invoice_id IS NOT NULL OR iar_credit_note_id IS NOT NULL)')->result_array();
            
            $advance_receipts[$key] = $advance_receipt;
        }
        
        return $advance_receipts;
    }


    /**
     * 
     * @param type $advance_receipt_id
     * @param type $advance_receipt
     * @return type
     */
    public function save_advance_receipt($advance_receipt_id, $advance_receipt) {
        try {
            log_message('debug', 'add_receipt. - $advance_receipt = ' . print_r($advance_receipt, 1));
            date_default_timezone_set('Asia/Kolkata');  //PHP server time zone. This won't set mySQl time zone. Needs to be set separately.

            $this->load->model('owner_company_model');
            $this->load->model('company_model');
            $this->load->model('state_model');
            //add other data
            $owner_company = $this->owner_company_model->get_owner_company();

            $advance_receipt['advance_receipt_oc_name'] = $owner_company['oc_billing_name'];
            $advance_receipt['advance_receipt_oc_address'] = $owner_company['oc_billing_address'];
            $advance_receipt['advance_receipt_oc_city_name'] = $owner_company['city_name'];
            $advance_receipt['advance_receipt_oc_gstin'] = $owner_company['oc_gst_number'];
            $advance_receipt['advance_receipt_oc_pan_number'] = $owner_company['oc_pan_number'];
            $advance_receipt['advance_receipt_oc_logo_path'] = $owner_company['oc_logo_path'];
            $advance_receipt['advance_receipt_oc_district'] = $owner_company['district'];
            $advance_receipt['advance_receipt_oc_state'] = $owner_company['billing_state_name'];
            $advance_receipt['advance_receipt_oc_contact_number'] = $owner_company['oc_contact_number'];
            $advance_receipt['advance_receipt_oc_gst_supply_state_id'] = $owner_company['oc_gst_supply_state_id'];
            $advance_receipt['advance_receipt_place_of_supply'] = $owner_company['supply_state_name'];
            
            if (!empty($advance_receipt['advance_receipt_linked_company_id'])) {
                $linked_company_id = $advance_receipt['advance_receipt_linked_company_id'];
                $linked_company = $this->company_model->get_company_with_id($linked_company_id);
                $advance_receipt['advance_receipt_linked_company_invoice_name'] = $linked_company['company_invoice_name'];
                $advance_receipt['advance_receipt_linked_company_display_name'] = $linked_company['company_display_name'];
                $advance_receipt['advance_receipt_linked_company_billing_address'] = $linked_company['company_billing_address'];
                $advance_receipt['advance_receipt_linked_company_shipping_address'] = $linked_company['company_shipping_address'];
                $advance_receipt['advance_receipt_linked_company_gst_supply_state_id'] = $linked_company['company_gst_supply_state_id'];
                $advance_receipt['advance_receipt_linked_company_contact_number'] = $linked_company['company_contact_number'];
                $advance_receipt['advance_receipt_linked_company_gstin'] = $linked_company['company_gst_number'];
                $advance_receipt['advance_receipt_place_of_supply'] = $this->state_model->get_state_with_id($linked_company['company_gst_supply_state_id'])['state_name'];
            } else {
                throw new Exception('Company ID not given');
            }
            
            if(!empty($advance_receipt['advance_receipt_debit_coa_id'])){
                $this->load->model('chart_of_accounts_model');
                $advance_receipt['advance_receipt_debit_coa_name'] = $this->chart_of_accounts_model->get_coa_with_id($advance_receipt['advance_receipt_debit_coa_id'])['coa_account_name'];
            }  else {
                $advance_receipt['advance_receipt_debit_coa_id'] = null;
            }
            /* add time component to date if receipt date is today.
             * else
             * add 23:59:59 if the receipt was created on a later date
             */
            if (date('Y-m-d', strtotime($advance_receipt['advance_receipt_date'])) == date("Y-m-d")) {
                $advance_receipt['advance_receipt_date'] = date("Y-m-d H:i:sa");
            } else {
                $advance_receipt['advance_receipt_date'] = date("Y-m-d 23:59:59", strtotime($advance_receipt['advance_receipt_date']));
            }
            
            
            $this->db->trans_begin();
            $this->db->query('SET time_zone = "+05:30";');
            if ($advance_receipt_id == null) {
                $this->db->insert('advance_receipt', $advance_receipt);
                $advance_receipt_id = $this->db->insert_id();
            } else {
                $this->db->where('advance_receipt_id', $advance_receipt_id);
                $this->db->update('advance_receipt', $advance_receipt);
            }

            if ($advance_receipt_id > 0) {
                $this->db->trans_commit();
            } else {
                log_message('debug', 'ROLLBACK. ' . print_r($this->db->error()));
                $this->db->trans_rollback();
            }

            log_message('debug', 'save_advance_receipt. - Query = ' . $this->db->last_query());
            return $advance_receipt_id;
        } finally {
            $this->load->model('system_journal_model');
            $this->system_journal_model->calculate_system_journal();
        }
    }
}
