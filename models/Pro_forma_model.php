<?php

class Pro_forma_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_all_pro_formas_for_report(){
        $result = $this->db->query("SELECT 
                                        CONCAT('PRF', LPAD(pro_forma_id, '5', '0')) AS pro_forma_id,
                                        pro_forma_linked_company_display_name as customer_name,
                                        pro_forma_linked_company_billing_address as customer_billing_address,
                                        pro_forma_linked_company_billing_state_name as billing_state_name,
                                        pro_forma_date,
                                        pro_forma_amount
                                    FROM
                                        pro_forma;");
        return $result->result_array();
    }
    /**
     * 
     * @return type
     */
    public function get_all_customers() {
        $result = $this->db->query('Select pro_forma_linked_company_display_name, pro_forma_linked_company_invoice_name, pro_forma_linked_company_id from pro_forma GROUP BY pro_forma_linked_company_display_name;');
        return $result->result_array();
    }

    /*
     * Delete Pro Forma
     */

    function delete_draft_pro_forma_by_id($pro_forma_id) {
        log_message('debug', 'delete Pro Forma By. - $id = ' . print_r($pro_forma_id, 1));

        $this->db->where('pfe_linked_pro_forma_id', $pro_forma_id);
        $this->db->delete('draft_pro_forma_entries');
        
        $this->db->where('pfce_linked_pro_forma_id', $pro_forma_id);
        $this->db->delete('draft_pro_forma_charge_entries');
        
        $this->db->where('pro_forma_id', $pro_forma_id);
        $this->db->delete('draft_pro_forma');

        log_message('debug', 'delete Pro Forma By. - Query = ' . $this->db->last_query());

        if ($this->db->affected_rows() == '1') {
            log_message('debug', 'delete Pro Forma By. - DELETED ');
            return TRUE;
        } else {
            log_message('debug', 'delete Pro Forma By. - FALIED TO DELETE ');
            return FALSE;
        }
    }

    /*
     * Retrieve all pro formas
     */

    public function get_all_pro_formas() {
        $result = $this->db->query('SELECT 
                                        *
                                    FROM
                                        pro_forma
                                    LEFT JOIN
                                        employee ON employee.employee_id = pro_forma.pro_forma_record_created_by
                                    ORDER BY
                                        pro_forma_id DESC;');
        return $result->result_array();
    }

    /*
     * Retrieve all pro formas
     */

    public function get_all_draft_pro_formas() {
        $result = $this->db->query('SELECT 
                                        *
                                    FROM
                                        draft_pro_forma
                                    LEFT JOIN
                                        employee ON employee.employee_id = draft_pro_forma.pro_forma_record_created_by;');
        return $result->result_array();
    }

    /**
     * Get entries for an pro forma
     */
    public function get_pro_forma_entries($pro_forma_id) {
        //$query = $this->db->get_where('pro_forma_entries', array('pfe_linked_pro_forma_id' => $pro_forma_id));
        $query = 'SELECT 
                    *
                    FROM pro_forma_entries 
                    WHERE
                    pfe_linked_pro_forma_id = ' . $pro_forma_id . ' group by pfe_id;';

        $result = $this->db->query($query);
        return $result->result_array();
    }

        
    /**
     * Get entries for an pro_forma
     */
    public function get_pro_forma_charge_entries($pro_forma_id) {
        //$query = $this->db->get_where('pro_forma_product_entries', array('ipe_linked_pro_forma_id' => $pro_forma_id));
        $query = 'SELECT 
                    *
                    FROM pro_forma_charge_entries 
                WHERE
                    pfce_linked_pro_forma_id = ' . $pro_forma_id . ' group by pfce_id;';

        $result = $this->db->query($query);
        return $result->result_array();
    }
    
    
    /**
     * Get entries for an pro_forma
     */
    public function get_draft_pro_forma_charge_entries($pro_forma_id) {
        //$query = $this->db->get_where('pro_forma_product_entries', array('ipe_linked_pro_forma_id' => $pro_forma_id));
        $query = 'SELECT 
                    *
                    FROM draft_pro_forma_charge_entries 
                WHERE
                    pfce_linked_pro_forma_id = ' . $pro_forma_id . ' group by pfce_id;';

        $result = $this->db->query($query);
        return $result->result_array();
    }

    
    /*
     * Get pro forma using pro_forma_id
     */

    public function get_pro_forma_with_id($pro_forma_id) {
        $query = $this->db->get_where('pro_forma', array('pro_forma_id' => $pro_forma_id));
        return $query->row_array();
    }

    /**
     * Get entries for an pro forma
     */
    public function get_draft_pro_forma_entries($pro_forma_id) {
        //$query = $this->db->get_where('pro_forma_entries', array('pfe_linked_pro_forma_id' => $pro_forma_id));
        $query = 'SELECT 
                    *
                    FROM draft_pro_forma_entries 
                    WHERE
                    pfe_linked_pro_forma_id = ' . $pro_forma_id . ' group by pfe_id;';

        $result = $this->db->query($query);
        return $result->result_array();
    }

    /*
     * Get pro forma using pro_forma_id
     */

    public function get_draft_pro_forma_with_id($pro_forma_id) {
        $query = $this->db->get_where('draft_pro_forma', array('pro_forma_id' => $pro_forma_id));
        return $query->row_array();
    }

    /**
      Perform all calculations here itself before sending to client to avoid code related to calculation on different views on clients -  example : on draft page, preview page, android app.
     */
    public function get_entire_pro_forma_data($pro_forma_id) {
        $pro_forma = $this->get_pro_forma_with_id($pro_forma_id);
        $pro_forma_entries = $this->get_pro_forma_entries($pro_forma_id);
        $total_discount = 0;
        $total_taxable_amount = 0;

        $tax_amount_map = array();
        foreach ($pro_forma_entries as $key => $pro_forma_entry) {
            //1
            $discount = $pro_forma_entry['pfe_discount'] * $pro_forma_entry['pfe_product_rate'] / 100;
            $total_discount = $total_discount + ( $discount * $pro_forma_entry['pfe_product_quantity']);

            //2
            if ($pro_forma['pro_forma_tax_type'] == 'inclusive') {
                $taxable_amount = $pro_forma_entry['pfe_product_quantity'] * ( $pro_forma_entry['pfe_product_rate'] - $discount ) / ( 1 + $pro_forma_entry['pfe_tax_percent_applicable'] / 100);
            } else if ($pro_forma['pro_forma_tax_type'] == 'exclusive') {
                $taxable_amount = $pro_forma_entry['pfe_product_quantity'] * ( $pro_forma_entry['pfe_product_rate'] - $discount );
            }

            $total_taxable_amount = $total_taxable_amount + $taxable_amount;

            $pfe_tax_percent_applicable = $pro_forma_entry['pfe_tax_percent_applicable'];
            $tax_amount = $taxable_amount * $pro_forma_entry['pfe_tax_percent_applicable'] / 100;

            if (!isset($tax_amount_map[$pfe_tax_percent_applicable])) {
                $tax_amount_map[$pfe_tax_percent_applicable] = $tax_amount;
            } else {
                $stored_tax = $tax_amount_map[$pfe_tax_percent_applicable];
                $tax_amount_map[$pfe_tax_percent_applicable] = $stored_tax + $tax_amount;
            }

            $pro_forma_entry['pfe_taxable_amount'] = $taxable_amount;
            $pro_forma_entry['pfe_tax_amount'] = $tax_amount;
            $pro_forma_entry['pfe_discount_per_item'] = $discount;

            $pro_forma_entries[$key] = $pro_forma_entry;
        }

        $pro_forma['total_discount'] = $total_discount;
        $pro_forma['total_taxable_amount'] = $total_taxable_amount;
        $pro_forma['tax_amount_map'] = $tax_amount_map;

        $pro_forma_data['pro_forma'] = $pro_forma;
        $pro_forma_data['pro_forma_entries'] = $pro_forma_entries;

        return $pro_forma_data;
    }

    /*
     * Add new pro forma

      0 - Tax
      1 - HSN
      2 - rate
      3 - quantity
      4 - discount
     * 
     * IMPORTANT - while editing pro forma entries, always delete old entries and insert edited ones so that new cne_id are assgined. Failure to
     * do so may lead to error while calculating inventory

     */

    public function save_pro_forma($pro_forma_id, $pro_forma, $pro_forma_entries, $pro_forma_charge_entries, $pro_forma_action) {

        try {
            if ($pro_forma_action == 'previewDraft') {
                $pro_forma_action = 'draft';
            }

            log_message('debug', 'add_pro_forma. - $pro_forma = ' . print_r($pro_forma, 1));

            date_default_timezone_set('Asia/Kolkata');  //PHP server time zone. This won't set mySQl time zone. Needs to be set separately.

            $this->load->model('company_model');
            $this->load->model('owner_company_model');
            $this->load->model('payment_term_model');
            $this->load->model('state_model');
            $this->load->model('transporter_model');

            //add other data
            $owner_company = $this->owner_company_model->get_owner_company();

            $pro_forma['pro_forma_oc_name'] = $owner_company['oc_billing_name'];
            $pro_forma['pro_forma_oc_address'] = $owner_company['oc_billing_address'];
            $pro_forma['pro_forma_oc_city_name'] = $owner_company['city_name'];
            if(!empty($owner_company['oc_pincode'])){
                $pro_forma['pro_forma_oc_city_name'].= ' - '.$owner_company['oc_pincode'];
            }
            $pro_forma['pro_forma_oc_gstin'] = $owner_company['oc_gst_number'];
            $pro_forma['pro_forma_oc_bank_details'] = $owner_company['oc_bank_details'];
            $pro_forma['pro_forma_oc_pan_number'] = $owner_company['oc_pan_number'];
            $pro_forma['pro_forma_oc_logo_path'] = $owner_company['oc_logo_path'];
            $pro_forma['pro_forma_oc_district'] = $owner_company['district'];
            $pro_forma['pro_forma_oc_state'] = $owner_company['billing_state_name'];
            $pro_forma['pro_forma_oc_contact_number'] = $owner_company['oc_contact_number'];
            $pro_forma['pro_forma_oc_gst_supply_state_id'] = $owner_company['oc_gst_supply_state_id'];
            /*if(isset($owner_company['oc_pro_forma_terms'])){
                $pro_forma['pro_forma_terms'] = $owner_company['oc_pro_forma_terms'];
            } else {
                $pro_forma['pro_forma_terms'] = NULL;
            }*/
            //$pro_forma['pro_forma_additional_terms'] = $owner_company['oc_additional_terms'];


            if (!empty($pro_forma['pro_forma_linked_transporter_id'])) {
                $transporter_id = $pro_forma['pro_forma_linked_transporter_id'];
                $pro_forma['pro_forma_linked_transporter_name'] = $this->transporter_model->get_transporter_with_id($transporter_id)['transporter_name'];
            } else {
                $pro_forma['pro_forma_linked_transporter_id'] = NULL;
                $pro_forma['pro_forma_linked_transporter_name'] = '';
            }
            
            if (empty($pro_forma['pro_forma_linked_broker_id'])) {
                $pro_forma['pro_forma_linked_broker_id'] = NULL;
            } 
            
            if (empty($pro_forma['pro_forma_linked_employee_id'])) {
                $pro_forma['pro_forma_linked_employee_id'] = NULL;
            } 

            $this->db->query('SET time_zone = "+05:30";');

            $payment_term = $this->payment_term_model->get_payment_term_using_id($pro_forma['pro_forma_linked_company_payment_term_id']);
            $pro_forma['pro_forma_linked_company_payment_term_name'] = $payment_term['payment_term_display_text'];
            $number_of_days_to_due = $payment_term['payment_term_days'];
            $pro_forma['pro_forma_due_date'] = date('Y-m-d', strtotime($pro_forma['pro_forma_date'] . ' + ' . $number_of_days_to_due . ' days'));

            /* add time component to date if pro forma date is today.
             * else
             * add 23:59:59 if the pro forma was created on a later date
             * This timestamp is to determine entry sequence for FIFO inventory
             * To be done for Pro Forma, purchase, credit note, debit note, inventory adjustment
             */
            if (date('Y-m-d', strtotime($pro_forma['pro_forma_date'])) == date("Y-m-d")) {
                $pro_forma['pro_forma_date'] = date("Y-m-d H:i:sa");
            } else {
                $pro_forma['pro_forma_date'] = date("Y-m-d 23:59:59", strtotime($pro_forma['pro_forma_date']));
            }

            if (!empty($pro_forma['pro_forma_linked_company_id'])) {
                $linked_company_id = $pro_forma['pro_forma_linked_company_id'];
                $linked_company = $this->company_model->get_company_with_id($linked_company_id);
                $pro_forma['pro_forma_linked_company_gstin'] = $linked_company['company_gst_number'];
                $pro_forma['pro_forma_is_reverse_charge_applicable'] = $linked_company['is_reverse_charge_applicable_for_company'];
                $pro_forma['pro_forma_linked_company_contact_number'] = $linked_company['company_contact_number'];
            } else {
                $pro_forma['pro_forma_linked_company_id'] = NULL;
                $pro_forma['pro_forma_linked_company_gstin'] = NULL;
                $pro_forma['pro_forma_is_reverse_charge_applicable'] = NULL;
                $pro_forma['pro_forma_linked_company_contact_number'] = NULL;
            }
            
            if(empty($pro_forma['pro_forma_revision_number'])){
                $pro_forma['pro_forma_revision_number'] = NULL;
            }

            if(empty($pro_forma['pro_forma_revision_date'])){
                $pro_forma['pro_forma_revision_date'] = NULL;
            }

            $pro_forma_linked_company_gst_supply_state_id = $pro_forma['pro_forma_linked_company_gst_supply_state_id'];
            $pro_forma['pro_forma_place_of_supply'] = $this->state_model->get_state_with_id($pro_forma_linked_company_gst_supply_state_id)['state_name'];

            $pro_forma['pro_forma_linked_company_billing_state_name'] = $this->state_model->get_state_with_id($pro_forma['pro_forma_linked_company_billing_state_id'])['state_name'];
            $pro_forma['pro_forma_linked_company_shipping_state_name'] = $this->state_model->get_state_with_id($pro_forma['pro_forma_linked_company_shipping_state_id'])['state_name'];


            $this->db->trans_begin();
            $count = 0;
            $this->db->query('SET time_zone = "+05:30";');
            if ($pro_forma['pro_forma_status'] == 'new') {  //creating new pro forma
                $pro_forma['pro_forma_status'] = $pro_forma_action;
                if ($pro_forma_action == 'confirm') {
                    $this->db->insert('pro_forma', $pro_forma);
                } else if ($pro_forma_action == 'draft') {
                    $this->db->insert('draft_pro_forma', $pro_forma);
                } else {
                    throw new Exception('Invalid pro forma action for insert');
                }
                $pro_forma_id = $this->db->insert_id();
            } else {        // user is editing an existing pro forma
                if ($pro_forma_action == $pro_forma['pro_forma_status']) {

                    if ($pro_forma_action == 'draft') {

                        $this->db->where('pfe_linked_pro_forma_id', $pro_forma_id);
                        $this->db->delete('draft_pro_forma_entries');

                        $this->db->where('pfce_linked_pro_forma_id', $pro_forma_id);
                        $this->db->delete('draft_pro_forma_charge_entries');
        
                        $this->db->where('pro_forma_id', $pro_forma_id);
                        $this->db->update('draft_pro_forma', $pro_forma);

                    } else if ($pro_forma_action == 'confirm') {

                        //retain time component if date not edited by user
                        $pro_forma_last_copy = $this->get_pro_forma_with_id($pro_forma_id);
                        if (date('Y-m-d', strtotime($pro_forma['pro_forma_date'])) == strtotime($pro_forma_last_copy['pro_forma_date'])) {
                            $pro_forma['pro_forma_date'] = $pro_forma_last_copy['pro_forma_date'];
                        }

                        $this->db->where('pfe_linked_pro_forma_id', $pro_forma_id);
                        $this->db->delete('pro_forma_entries');
                        
                        $this->db->where('pfce_linked_pro_forma_id', $pro_forma_id);
                        $this->db->delete('pro_forma_charge_entries');

                        $this->db->where('pro_forma_id', $pro_forma_id);
                        $this->db->update('pro_forma', $pro_forma);

                    } else {
                        throw new Exception('Invalid pro forma action for update');
                    }
                } else if ($pro_forma['pro_forma_status'] == 'draft' && $pro_forma_action == 'confirm') {   //confirming a draft pro forma
                    //delete draft pro forma
                    $this->db->where('pfe_linked_pro_forma_id', $pro_forma_id);
                    $this->db->delete('draft_pro_forma_entries');
                    
                    $this->db->where('pfce_linked_pro_forma_id', $pro_forma_id);
                    $this->db->delete('draft_pro_forma_charge_entries');
        
                    $this->db->where('pro_forma_id', $pro_forma_id);
                    $this->db->delete('draft_pro_forma');

                    $pro_forma['pro_forma_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
                    $pro_forma['pro_forma_status'] = $pro_forma_action;
                    $this->db->insert('pro_forma', $pro_forma);
                    $pro_forma_id = $this->db->insert_id();
                } else {
                    throw new Exception('Invalid pro forma status and required action');
                }
            }

            //Get all taxes
            $tax_query = $this->db->get('tax');
            $taxes = $tax_query->result_array();
            $tax_id_percent_map = array();
            $tax_id_name_map = array();
            foreach ($taxes as $tax) {
                $tax_id_percent_map[$tax['tax_id']] = $tax['tax_percent'];
                $tax_id_name_map[$tax['tax_id']] = $tax['tax_name'];
            }

            if ($pro_forma_id > 0) {
                $pfe['pfe_linked_pro_forma_id'] = $pro_forma_id;

                foreach ($pro_forma_entries as $entry_id => $pro_forma_entry) {
                    $pfe['pfe_entry_id'] = $entry_id;
                    foreach ($pro_forma_entry as $product_id => $product_info) {
                        $pfe['pfe_linked_product_id'] = $product_id;

                        $pfe['pfe_product_name'] = $product_info[0];
                        $pfe['pfe_tax_id_applicable'] = $product_info[1];
                        $pfe['pfe_tax_percent_applicable'] = $tax_id_percent_map[$product_info[1]]; // fetch tax percent from $tax_id_percent_map
                        $pfe['pfe_tax_name_applicable'] = $tax_id_name_map[$product_info[1]]; // fetch tax name from $tax_id_name_map
                        $pfe['pfe_product_hsn_code'] = $product_info[2];
                        $pfe['pfe_product_rate'] = $product_info[3];
                        $pfe['pfe_product_quantity'] = $product_info[4];
                        $pfe['pfe_product_uqc_id'] = $product_info[5];
                        $pfe['pfe_product_uqc_text'] = $product_info[6];
                        $pfe['pfe_discount'] = $product_info[7];

                        if ($pro_forma_action == 'confirm') {
                            $this->db->insert('pro_forma_entries', $pfe);
                        } else if ($pro_forma_action == 'draft') {
                            $this->db->insert('draft_pro_forma_entries', $pfe);
                        } else {
                            throw new Exception('Invalid pro forma action while inserting pro forma entries');
                        }
                        if ($this->db->insert_id() > 0) {
                            $count++;
                        }
                    }
                }
                
                                
                //enter charges
                foreach ($pro_forma_charge_entries as $charge_entry_id => $pro_forma_charge_entry) {
                    $pfce['pfce_entry_id'] = $charge_entry_id;
                    foreach ($pro_forma_charge_entry as $charge_id => $charge_info) {
                        $pfce['pfce_charge_id'] = $charge_id;
                        $pfce['pfce_linked_pro_forma_id'] = $pro_forma_id;
                        $pfce['pfce_charge_name'] = $charge_info[0];  
                        $pfce['pfce_taxable_amount'] = $charge_info[1];
                        
                        $pfce['pfce_tax_id_applicable'] = $charge_info[2];
                        $pfce['pfce_tax_percent_applicable'] = $tax_id_percent_map[$charge_info[2]]; // fetch tax percent from $tax_id_percent_map
                        $pfce['pfce_tax_name_applicable'] = $tax_id_name_map[$charge_info[2]]; // fetch tax name from $tax_id_name_map
                        
                        if ($pro_forma_action == 'confirm') {
                            $this->db->insert('pro_forma_charge_entries', $pfce);
                        } else if ($pro_forma_action == 'draft') {
                            $this->db->insert('draft_pro_forma_charge_entries', $pfce);
                        } else {
                            throw new Exception('Invalid proforma action while inserting proforma entries');
                        }
                    }
                }
            } else {
                $response['result'] = $this->db->error();
                $response['query'] = $this->db->last_query();
            }


            if ($count > 0) {
                $this->db->trans_commit();
                $response['result'] = "success";
            } else {
                log_message('debug', 'confirm_dispatch. ROLLBACK. ' . print_r($response));
                $this->db->trans_rollback();
            }

            log_message('debug', 'edit_pro_forma. - Query = ' . $this->db->last_query());
            return $pro_forma_id;
        } finally {
            $this->load->model('inventory_model');
            $this->inventory_model->evaluate_inventory_figures();
        }
    }
}
