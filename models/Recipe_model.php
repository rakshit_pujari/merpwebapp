<?php

class Recipe_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }
    
    /**
     * Get on activated products
     */
    public function get_activated_products_without_recipe($transaction_type = null) {
        $query = "SELECT 
                    *
                FROM
                    product
                LEFT JOIN
                    unique_quantity_code
                ON
                    product.product_uqc_id = unique_quantity_code.uqc_id
                LEFT OUTER JOIN
                    recipe ON recipe.recipe_linked_product_id = product.product_id
                WHERE
                    recipe.recipe_linked_product_id IS NULL AND
                    product_status = 'active'";

        if ($transaction_type != null) {
            $query .= ' && FIND_IN_SET("' . $transaction_type . '", transaction_type)';
        }

        $query .= ';';
        $result = $this->db->query($query);
        return $result->result_array();
    }

    /**
     * 
     * @param type $product_id
     * @return type
     */
    public function get_recipe_for_product($product_id){
        $query = $this->db->get_where('recipe', array('recipe_linked_product_id' => $product_id));
        return $query->row_array();  
    }
    
    /**
     * 
     * @return type
     */
    public function get_all_recipes_for_report(){
        $result = $this->db->query("SELECT 
                                    CONCAT('BOM',
                                    LPAD(recipe_id, '5', '0')) AS recipe_id,
                                    product_name,
                                    recipe_product_quantity
                                    FROM
                                        recipe
                                    LEFT JOIN
                                        product
                                    ON
                                        recipe.recipe_linked_product_id = product.product_id
                                    LEFT JOIN
                                        employee ON employee.employee_id = recipe.recipe_record_created_by;");
        return $result->result_array();
    }
    
    /*
     * Delete Recipe
     */
    function delete_recipe_by_id($recipe_id) {
        log_message('debug', 'delete_recipe_by_id. - $id = ' . print_r($recipe_id, 1));

        $this->db->where('re_linked_recipe_id', $recipe_id);
        $this->db->delete('recipe_entries');
            
        $this->db->where('recipe_id', $recipe_id);
        $this->db->delete('recipe');

        log_message('debug', 'delete_recipe_by_id. - Query = ' . $this->db->last_query());

        if ($this->db->affected_rows() == '1') {
            log_message('debug', 'delete_recipe_by_id. - DELETED ');
            return TRUE;
        } else {
            log_message('debug', 'delete_recipe_by_id. - FALIED TO DELETE ');
            return FALSE;
        }
    }

    /*
     * Retrieve all recipes
     */
    public function get_all_recipes() {
        $result = $this->db->query('SELECT 
                                    *,
                                    (Select uqc_text from unique_quantity_code where unique_quantity_code.uqc_id = product.product_uqc_id) as uqc_text
                                FROM
                                    recipe
                                LEFT JOIN
                                    product
                                ON
                                    recipe.recipe_linked_product_id = product.product_id
                                LEFT JOIN
                                    employee ON employee.employee_id = recipe.recipe_record_created_by;');
        return $result->result_array();
    }

    /**
     * Get entries for an recipe
     */
    public function get_recipe_entries($recipe_id) {

        $query = 'SELECT 
                    *,
                    (Select uqc_text from unique_quantity_code where unique_quantity_code.uqc_id = product.product_uqc_id) as uqc_text
                    FROM recipe_entries 
                    LEFT JOIN
                        product
                    ON
                        recipe_entries.re_linked_product_id = product.product_id
                    WHERE
                    re_linked_recipe_id = ' . $recipe_id . ' group by re_id;';

        $result = $this->db->query($query);
        return $result->result_array();
    }

        /**
     * Get entries for an recipe
     */
    public function get_recipe_entries_with_inventory_status($recipe_id) {

        $query = 'SELECT 
                    *,
                    IFNULL(SUM(ie_it_product_quantity), 0) AS inventory_quantity,
                    (Select ie_it_product_rate from inventory_entry as c WHERE inventory_entry.ie_it_product_id = c.ie_it_product_id ORDER BY c.ie_id DESC LIMIT 1) as rate_per_unit,
                    (Select uqc_text from unique_quantity_code where unique_quantity_code.uqc_id = product.product_uqc_id) as uqc_text
                    FROM recipe_entries 
                    LEFT JOIN
                        product
                    ON
                        recipe_entries.re_linked_product_id = product.product_id
                    LEFT JOIN
                        inventory_entry ON inventory_entry.ie_it_product_id = product.product_id
                    WHERE
                    re_linked_recipe_id = ' . $recipe_id . ' group by re_id;';

        $result = $this->db->query($query);
        return $result->result_array();
    }

    
    /*
     * Get recipe using recipe_id
     */

    public function get_recipe_with_id($recipe_id) {
        $query = $this->db->get_where('recipe', array('recipe_id' => $recipe_id));
        return $query->row_array();
    }


    /*
     * Add new recipe

      0 - Tax
      1 - HSN
      2 - rate
      3 - quantity
      4 - discount

     * IMPORTANT - while editing recipe entries, always delete old entries and insert edited ones so that new cne_id are assgined. Failure to
     * do so may lead to error while calculating inventory
     */

    public function save_recipe($recipe_id = NULL, $recipe, $recipe_entries) {

        log_message('debug', 'add_recipe. - $recipe = ' . print_r($recipe, 1));
        date_default_timezone_set('Asia/Kolkata');  //PHP server time zone. This won't set mySQl time zone. Needs to be set separately.

        $this->db->query('SET time_zone = "+05:30";');

        $this->db->trans_begin();
        $count = 0;

        if ($recipe_id == NULL) {
            $this->db->insert('recipe', $recipe);
            $recipe_id = $this->db->insert_id();
        } else {
            $this->db->where('re_linked_recipe_id', $recipe_id);
            $this->db->delete('recipe_entries');

            $this->db->where('recipe_id', $recipe_id);
            $this->db->update('recipe', $recipe);
        }

        if ($recipe_id > 0) {
            
            $re['re_linked_recipe_id'] = $recipe_id;

            foreach ($recipe_entries as $entry_id => $product_info) {
                foreach ($product_info as $product_id => $quantity) {
                    $re['re_linked_product_id'] = $product_id;
                    $re['re_linked_product_quantity'] = $quantity;
                    $this->db->insert('recipe_entries', $re);
                    if ($this->db->insert_id() > 0) {
                        $count++;
                    }
                }
            }
        } else {
            $response['result'] = $this->db->error();
            $response['query'] = $this->db->last_query();
        }

        if ($count > 0) {
            $this->db->trans_commit();
            $response['result'] = "success";
        } else {
            log_message('debug', 'rollback. ' . print_r($response));
            $this->db->trans_rollback();
        }

        log_message('debug', 'save_recipe. - Query = ' . $this->db->last_query());
        return $recipe_id;
    }

}
