<?php

class Expense extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('expense_model');
        $this->load->helper('url_helper');
        $this->load->model('company_model');
        $this->load->model('owner_company_model');
        $this->load->model('tax_model');
        $this->load->model('chart_of_accounts_model');        
    }

    /**
     * Landing page
     */
    public function all() {
        //check if user is allowed view access
        if (!$this->is_access_granted('expense', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        
        $data['expenses'] = $this->expense_model->get_all_expenses();
        $this->load->view('landing_expense', $data);
    }

    /**
     * Expense form
     * @param type $id
     * @return type
     */
    public function view($expense_type, $id = NULL) {
        
        //check if user is allowed view access
        if (!$this->is_access_granted('expense', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }

        $this->load->helper('form');
        
        $form_data['oc_state_id'] = $this->owner_company_model->get_owner_company()['oc_gst_supply_state_id'];
        $form_data['expense_accounts'] = $this->chart_of_accounts_model->get_all_coa_of_account_type(3);
        $form_data['current_accounts'] = $this->chart_of_accounts_model->get_all_coa_of_account_type(4);
        
        if ($expense_type == 'new') {
            $this->load->view('expense', $form_data);
        } else {
            if ($id != NULL) {
                if($expense_type == 'confirm'){
                    $data['expense'] = $this->expense_model->get_expense_with_id($id);
                }
                
                if (empty($data['expense'])) {
                    redirect('web/master#!/expense/all', 'refresh');
                    return;
                }
                
                $this->load->view('expense', array_merge($data, $form_data));
            } else {
                throw new Exception('Invoice ID not provided');
            }
        }
    }

    /**
     * 
     * @param type $id
     */
    public function save($expense_id = NULL) {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->helper('date');
        
        $expense = $this->input->post();
        if($expense_id == NULL){
            $expense['expense_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
        }
        
        date_default_timezone_set('Asia/Kolkata');
        $current_time = now('Asia/Kolkata');

        if (!empty($_FILES['expense_image_path']['name'])) {
            $file_name_array = explode(".", $_FILES['expense_image_path']['name']);
            //$file_type = $file_name_array[1];
            $file_type = $file_name_array[sizeof($file_name_array) - 1]; 
            $file_location = "/uploads/expense/" . $current_time . "." . $file_type;
            $expense['expense_image_path'] = $file_location;
        }

        $expense_id = $this->expense_model->save_expense($expense_id, $expense);
        //print_r($result);

        if (empty($_FILES['expense_image_path']['name'])) {
            redirect('web/master#!/expense/all', 'refresh');
            return;
        }


        $uploadPath = './uploads/expense/';
        if (!file_exists($uploadPath)) {
            mkdir($uploadPath, 0777, true);
        }

        //$new_name = $productId;
        //Image
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'png|jpg|bmp|jpeg';
        $config['max_size'] = 102400;
        $config['max_width'] = 102400;
        $config['max_height'] = 76800;
        $config['file_name'] = $current_time;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('expense_image_path')) {
            $error = array('error' => $this->upload->display_errors());
            print_r($error);
        } else {
            $data = array('upload_data' => $this->upload->data());
        }
        redirect('web/master#!/expense/all', 'refresh');
    }

    /**
     * 
     * @param type $expense_id
     */
    public function delete($expense_id) {
        if (!$this->is_access_granted('expense', 'delete')) {
            return;
        }
        if ($this->input->server('REQUEST_METHOD') == 'DELETE') {
            if ($this->expense_model->delete_expense_by_id($expense_id) == false) {
                echo "failed";
            } else {
                echo "success";
            }
        }
    }
}
