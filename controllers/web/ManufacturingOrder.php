<?php

class ManufacturingOrder extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('manufacturing_order_model');
        $this->load->helper('url_helper');
        $this->load->model('product_model');
        $this->load->model('recipe_model');
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function delete($draft_manufacturing_order_id) {
        if (!$this->is_access_granted('manufacturing_order', 'delete')) {
            return;
        }
        if ($this->input->server('REQUEST_METHOD') == 'DELETE') {
            if ($this->manufacturing_order_model->delete_draft_manufacturing_order_by_id($draft_manufacturing_order_id) == false) {
                echo "failed";
            } else {
                echo "success";
            }
        }
    }

    /**
     * 
     * @param type $manufacturing_order_id
     * @return type
     */
    public function complete($manufacturing_order_id) {
        if (!$this->is_access_granted('manufacturing_order', 'save')) {
            return;
        }

        $this->manufacturing_order_model->mark_manufacturing_order_as_complete($manufacturing_order_id);
    }

    /**
     * 
     * @return type
     */
    public function all() {
        //check if user is allowed view access
        if (!$this->is_access_granted('manufacturing_order', 'view')) {
            return;
        }

        $data['manufacturing_orders'] = array_merge($this->manufacturing_order_model->get_all_manufacturing_orders(), $this->manufacturing_order_model->get_all_draft_manufacturing_orders());
        $this->load->view('landing_manufacturing_order', $data);
    }

    /**
     * 
     * @param type $parameter
     * @param type $parameter_id
     */
    public function json($parameter = NULL, $parameter_id = NULL) {
        if ($parameter != null) {
            if ($parameter == 'customer') {
                $data['customers'] = $this->manufacturing_order_model->get_all_customers();
            }
            if ($parameter == 'manufacturing_order_customer_map') {
                $data['manufacturing_order_customer_map'] = $this->manufacturing_order_model->get_payment_customer_map();
            }
            if ($parameter == 'manufacturing_order' && $parameter_id != null) {
                $data['manufacturing_order'] = $this->manufacturing_order_model->get_manufacturing_order_with_id($parameter_id);
                $data['manufacturing_order_entries'] = $this->manufacturing_order_model->get_manufacturing_order_entries($parameter_id);
            }
        } else {
            $data['manufacturing_orders'] = $this->manufacturing_order_model->get_all_manufacturing_orders();
        }

        echo json_encode($data);
    }

    
        /**
     * 
     * @param type $id
     * @return type
     */
    public function preview($manufacturing_order_type, $manufacturing_order_id) {
        //check if user is allowed view access
        if (!$this->is_access_granted('manufacturing_order', 'view')) {
            return;
        }
        
        if ($manufacturing_order_id == null) {
            return;
        }

    if($manufacturing_order_type == 'confirm' || $manufacturing_order_type == 'complete'){
            $data['manufacturing_order'] = $this->manufacturing_order_model->get_manufacturing_order_with_id($manufacturing_order_id);
            $data['manufacturing_order_entries'] = $this->manufacturing_order_model->get_manufacturing_order_entries($manufacturing_order_id);
        } else if($manufacturing_order_type == 'draft'){
            $data['manufacturing_order'] = $this->manufacturing_order_model->get_draft_manufacturing_order_with_id($manufacturing_order_id);
            $data['manufacturing_order_entries'] = $this->manufacturing_order_model->get_draft_manufacturing_order_entries($manufacturing_order_id);
        }
        
        $this->load->view('preview_manufacturing_order', $data);
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function view($manufacturing_order_type, $manufacturing_order_id = NULL) {

        //check if user is allowed view access
        if (!$this->is_access_granted('manufacturing_order', 'view')) {
            return;
        }

        $this->load->helper('form');
        //$form_data['products'] = $this->product_model->get_activated_products_with_inventory_status('sales');
        $form_data['recipes'] = $this->recipe_model->get_all_recipes();

        if ($manufacturing_order_type == 'new') {
            $this->load->view('manufacturing_order', $form_data);
        } else {
            if ($manufacturing_order_id != NULL) {
                if($manufacturing_order_type == 'confirm'){
                    $data['manufacturing_order'] = $this->manufacturing_order_model->get_manufacturing_order_with_id($manufacturing_order_id);
                    $data['manufacturing_order_entries'] = $this->manufacturing_order_model->get_manufacturing_order_entries($manufacturing_order_id);
                } else if($manufacturing_order_type == 'draft'){
                    $data['manufacturing_order'] = $this->manufacturing_order_model->get_draft_manufacturing_order_with_id($manufacturing_order_id);
                    $data['manufacturing_order_entries'] = $this->manufacturing_order_model->get_draft_manufacturing_order_entries($manufacturing_order_id);
                }
                
                $data['linked_recipe'] = $this->recipe_model->get_recipe_for_product($data['manufacturing_order']['manufacturing_order_linked_product_id']);
                
                $this->load->view('manufacturing_order', array_merge($data, $form_data));
            } else {
                throw new Exception('ID not provided');
            }
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function save($manufacturing_order_id = NULL) {
        $this->load->helper('form');
        $manufacturing_order = $this->input->post();
        $manufacturing_order_entries = $manufacturing_order['manufacturing_order_entries'];

        if ($manufacturing_order_id == NULL) {
            $manufacturing_order['manufacturing_order_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
        }

        unset($manufacturing_order['manufacturing_order_entries']);

        if ($_POST['transaction'] == 'draft' || $_POST['transaction'] == 'previewDraft' || $_POST['transaction'] == 'confirm') {
            $action = $_POST['transaction'];
            unset($manufacturing_order['transaction']);

            $manufacturing_order_id = $this->manufacturing_order_model->save_manufacturing_order($manufacturing_order_id, $manufacturing_order, $manufacturing_order_entries, $action);
            echo $manufacturing_order_id;
            
        } else {
            echo "ERROR - Action not defined";
        }
        
    }

}
