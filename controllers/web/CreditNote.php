<?php

class CreditNote extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('credit_note_model');
        $this->load->helper('url_helper');
        $this->load->model('employee_model');
        $this->load->model('broker_model');
        $this->load->model('state_model');
        $this->load->model('transporter_model');
        $this->load->model('product_model');
        $this->load->model('company_model');
        $this->load->model('owner_company_model');
        $this->load->model('payment_term_model');
        $this->load->model('tax_model');
        $this->load->model('charge_model');
        $this->load->model('supplementary_invoice_reason_model');
    }

    /**
     * Landing page
     * @return type
     */
    public function all() {
        
        //check if user is allowed view access
        if (!$this->is_access_granted('credit_note', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        
        $data['credit_notes'] = array_merge($this->credit_note_model->get_all_credit_notes(), $this->credit_note_model->get_all_draft_credit_notes());
        $this->load->view('landing_credit_note', $data);
    }

    /**
     * 
     */
    public function json() {
        $data['credit_notes'] = $this->credit_note_model->get_all_credit_notes();
        echo json_encode($data);
    }

    /**
     * 
     * @param type $credit_note_type
     * @param type $credit_note_id
     * @return type
     */
    public function preview($credit_note_type, $credit_note_id) {
        
        //check if user is allowed view access
        if (!$this->is_access_granted('credit_note', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        
        if ($credit_note_id == null) {
            redirect('web/master#!/CreditNote/all', 'refresh');
            return;
        }

        if($credit_note_type == 'confirm'){
            $data['credit_note'] = $this->credit_note_model->get_credit_note_with_id($credit_note_id);
            $data['credit_note_entries'] = $this->credit_note_model->get_credit_note_entries($credit_note_id);
            $data['credit_note_charge_entries'] = $this->credit_note_model->get_credit_note_charge_entries($credit_note_id);
        } else if($credit_note_type == 'draft'){
            $data['credit_note'] = $this->credit_note_model->get_draft_credit_note_with_id($credit_note_id);
            $data['credit_note_entries'] = $this->credit_note_model->get_draft_credit_note_entries($credit_note_id);
            $data['credit_note_charge_entries'] = $this->credit_note_model->get_draft_credit_note_charge_entries($credit_note_id);
        }
        
        $this->load->view('preview_credit_note', $data);
    }

    /**
     * 
     * @param type $credit_note_type
     * @param type $credit_note_id
     * @return type
     * @throws Exception
     */
    public function view($credit_note_type, $credit_note_id = NULL) {
        
        //check if user is allowed view access
        if (!$this->is_access_granted('credit_note', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('cn_linked_supplementary_invoice_id', 'ID', 'required');

        $form_data['employees'] = $this->employee_model->get_all_employees();
        $form_data['transporters'] = $this->transporter_model->get_all_transporters();
        $form_data['brokers'] = $this->broker_model->get_all_brokers();
        $form_data['states'] = $this->state_model->get_all_states();
        $form_data['charges'] = $this->charge_model->get_all_charges('sales');
        $form_data['products'] = $this->product_model->get_all_products();
        $form_data['union_territories'] = $this->state_model->get_all_union_territories();
        $form_data['payment_terms'] = $this->payment_term_model->get_all_payment_terms();
        $form_data['interstate_taxes'] = $this->tax_model->get_interstate_taxes();
        $form_data['intrastate_taxes'] = $this->tax_model->get_intrastate_taxes();
        $form_data['oc_state_id'] = $this->owner_company_model->get_owner_company()['oc_gst_supply_state_id'];
        $form_data['supplementary_invoice_reasons'] = $this->supplementary_invoice_reason_model->get_all_reasons();

        if ($credit_note_type == 'new') {
            $this->load->view('credit_note', $form_data);
        } else {
            if ($credit_note_id != NULL) {
                if($credit_note_type == 'confirm'){
                    $data['credit_note'] = $this->credit_note_model->get_credit_note_with_id($credit_note_id);
                    $data['credit_note_entries'] = $this->credit_note_model->get_credit_note_entries($credit_note_id);
                    $data['credit_note_charge_entries'] = $this->credit_note_model->get_credit_note_charge_entries($credit_note_id);
                } else if($credit_note_type == 'draft'){
                    $data['credit_note'] = $this->credit_note_model->get_draft_credit_note_with_id($credit_note_id);
                    $data['credit_note_entries'] = $this->credit_note_model->get_draft_credit_note_entries($credit_note_id);
                    $data['credit_note_charge_entries'] = $this->credit_note_model->get_draft_credit_note_charge_entries($credit_note_id);
                }
                
                if (empty($data['credit_note'])) {
                    redirect('web/master#!/CreditNote/all', 'refresh');
                    return;
                }
                
                $this->load->view('credit_note', array_merge($data, $form_data));
            } else {
                throw new Exception('Invoice ID not provided');
            }
        }
    }

    
    /**
     * 
     * @param type $credit_note_id
     * @return type
     */
    public function save($credit_note_id = NULL) {
        $this->load->helper('form');

        $credit_note = $this->input->post();
        $credit_note_entries = $credit_note['credit_note_entries'];

        if($credit_note_id == NULL){
            $credit_note['cn_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
        }
        
        $credit_note_charge_entries = array();
        if(isset($credit_note['charge_entries'])){
            $credit_note_charge_entries = $credit_note['charge_entries'];
        }
        
        unset($credit_note['credit_note_entries']);
        unset($credit_note['charge_entries']);
        if (!isset($credit_note['cn_linked_company_id'])) {
            $credit_note['cn_linked_company_id'] = null;
        }

        if ($_POST['transaction'] == 'draft' 
                || $_POST['transaction'] == 'previewDraft' 
                || $_POST['transaction'] == 'confirm') {
            
            $action = $_POST['transaction'];
            unset($credit_note['transaction']);

            $credit_note_id = $this->credit_note_model->save_credit_note($credit_note_id, $credit_note, $credit_note_entries, $credit_note_charge_entries, $action);
            
            if($action == 'confirm'){
                //redirect('web/master#!/CreditNote/preview/confirm/'.$credit_note_id);
            } else if($action == 'draft') {
                //redirect('web/master#!/CreditNote/all');
            }  else if($action == 'previewDraft') {
                //redirect('web/master#!/CreditNote/preview/draft/'.$credit_note_id);
            }
            
            echo $credit_note_id;
            
        } else {
            echo "ERROR - Action not defined";
        }
    }

    /**
     * 
     * @param type $credit_note_id
     * @return type
     */
    public function delete($credit_note_type, $credit_note_id) {
        if (!$this->is_access_granted('credit_note', 'delete')) {
            return;
        }
        if ($this->input->server('REQUEST_METHOD') == 'DELETE') {
            if($credit_note_type == 'draft'){
                if ($this->credit_note_model->delete_draft_credit_note_by_id($credit_note_id)) {
                    echo "success";
                } else {
                    echo "failed";
                }
            }
        }
    }
}
