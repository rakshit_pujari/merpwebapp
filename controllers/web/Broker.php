<?php

class Broker extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('location_model');
        $this->load->model('broker_model');
        $this->load->helper('url_helper');
        
        //check if user is allowed view access
        if (!$this->is_access_granted('broker', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
    }

    /**
     * Landing Page
     */
    public function all() {
        $data['brokers'] = $this->broker_model->get_all_brokers();
        $this->load->view('landing_broker', $data);
    }


    /**
     * 
     * @param type $broker_id
     */
    public function view($broker_id = NULL) {
        $this->load->helper('form');
        $data['locations'] = $this->location_model->get_all_locations();
        if ($broker_id == NULL) { 
            $this->load->view('broker', $data);
        } else {
            $data['broker'] = $this->broker_model->get_broker_with_id($broker_id);
            $this->load->view('broker', $data);
        }
    } 

    /**
     * 
     * @param type $broker_id
     * @return type
     */
    public function save($broker_id = NULL) {
        //check if user is allowed view access
        if (!$this->is_access_granted('broker', 'save')) {
            return;
        }
        
        $data = $this->input->post();
        if($broker_id == NULL){
            $data['broker_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
        }
        $this->broker_model->save_broker($data, $broker_id);
    }
    
    public function delete($id) {
        if (!$this->is_access_granted('broker', 'delete')) {
            return;
        }
        if ($this->input->server('REQUEST_METHOD') == 'DELETE') {
            if ($this->broker_model->deleteBrokerById($id) == false) {
                echo "failed";
            } else {
                echo "success";
            }
        }
    }

}
