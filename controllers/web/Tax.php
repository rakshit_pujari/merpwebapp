<?php

class Tax extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('tax_model');
        $this->load->helper('url_helper');
    }

    public function json() {
        $data['interstate_taxes'] = $this->tax_model->get_interstate_taxes();
        $data['intrastate_taxes'] = $this->tax_model->get_intrastate_taxes();
        
        echo json_encode($data);
    }
    
    public function gst($param, $gstin_to_validate){
        
        if($param == 'validate'){
            if($this->tax_model->check_my_gst_number($gstin_to_validate) == 0){
                echo 'invalid';
            } else if($this->tax_model->check_my_gst_number($gstin_to_validate) == 1){
                echo 'valid';
            } else {
                echo 'error';
            }
        }
    }

}
