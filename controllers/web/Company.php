<?php

class Company extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('location_model');
        $this->load->model('company_model');
        $this->load->helper('url_helper');
        $this->load->model('state_model');
        $this->load->model('payment_term_model');
    }

    /**
     * 
     * @return type
     */
    public function all() {

        //check if user is allowed view access
        if (!$this->is_access_granted('company', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        $data['companies'] = $this->company_model->get_all_companies();
        $this->load->view('landing_company', $data);
    }

    /**
     * 
     * @param type $company_type
     */
    public function json($company_type = NULL) {
        $data['companies'] = $this->company_model->get_all_companies($company_type);
        echo json_encode($data);
    }

    /**
     * 
     * @param type $company_id
     * @return type
     */
    public function view($company_id = NULL) {

        //check if user is allowed view access
        if (!$this->is_access_granted('company', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }

        $this->load->helper('form');

        $data['locations'] = $this->location_model->get_all_locations();
        $data['states'] = $this->state_model->get_all_states();
        $data['payment_terms'] = $this->payment_term_model->get_all_payment_terms();
        $data['union_territories'] = $this->state_model->get_all_union_territories();

        if ($company_id == NULL) {
            $this->load->view('company', $data);
        } else {
            $data['company'] = $this->company_model->get_company_with_id($company_id);
            $this->load->view('company', $data);
        }
    }

    /**
     * 
     * @param type $company_id
     */
    public function save($company_id = NULL) {
        //check if user is allowed view access
        if (!$this->is_access_granted('company', 'save')) {
            return;
        }
        
        $data = $this->input->post();
        if (isset($data['is_reverse_charge_applicable_for_company'])) {
            $data['is_reverse_charge_applicable_for_company'] = 1;
        } else {
            $data['is_reverse_charge_applicable_for_company'] = 0;
        }
        if($company_id == NULL){
            $data['company_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
        }
        $this->company_model->save_company($data, $company_id);
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function delete($id) {
        if (!$this->is_access_granted('company', 'delete')) {
            return;
        }
        if ($this->input->server('REQUEST_METHOD') == 'DELETE') {
            if ($this->company_model->delete_company_by_id($id) == false) {
                echo "failed";
            } else {
                echo "success";
            }
        }
    }
}
