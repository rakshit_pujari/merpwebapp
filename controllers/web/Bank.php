<?php

class Bank extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('location_model');
        $this->load->model('bank_model');
        $this->load->helper('url_helper');

        //check if user is allowed view access
        if (!$this->is_access_granted('bank', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
    }

    /**
     * Landing page
     */
    public function all() {
        $data['banks'] = $this->bank_model->get_all_banks();
        $this->load->view('landing_bank', $data);
    }

    /**
     * 
     * @param type $bank_id
     */
    public function view($bank_id = NULL) {
        $this->load->helper('form');
        $data['locations'] = $this->location_model->get_all_locations();
        if ($bank_id == NULL) { 
            $this->load->view('bank', $data);
        } else {
            $data['bank'] = $this->bank_model->get_bank_with_id($bank_id);
            $this->load->view('bank', $data);
        }
    } 

    /**
     * 
     * @param type $bank_id
     * @return type
     */
    public function save($bank_id = NULL) {
        //check if user is allowed view access
        if (!$this->is_access_granted('bank', 'save')) {
            return;
        }
        
        $data = $this->input->post();
        if($bank_id == NULL){
            $data['bank_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
        }
        
        $this->bank_model->save_bank($data, $bank_id);
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function delete($id) {
        if (!$this->is_access_granted('bank', 'delete')) {
            return;
        }
        if ($this->input->server('REQUEST_METHOD') == 'DELETE') {
            if ($this->bank_model->deleteBankById($id) == false) {
                echo "failed";
            } else {
                echo "success";
            }
        }
    }

}
