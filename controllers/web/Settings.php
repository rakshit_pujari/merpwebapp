<?php

class Settings extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('location_model');
        $this->load->model('state_model');
        $this->load->model('owner_company_model');
        $this->load->model('chart_of_accounts_model');
        $this->load->helper('url_helper');
        $this->load->helper('date');
    }
    
        
    public function changelog($id = NULL) {
        $this->load->view('changelog');
    }
    

    /**
     * 
     */
    public function accounting($action = NULL) {

        $this->load->helper('form');
        $this->load->library('form_validation');
        
        if ($action == 'save') {
            $post_data = $this->input->post()['accounting_settings'];
            $this->owner_company_model->update_accounting_preferences($post_data);
            redirect('web/master#!/settings/accounting', 'refresh');
        } else {
            
            $data['chart_of_accounts'] = $this->chart_of_accounts_model->get_all_coa();

            $data['invoice_accounting_settings'] = $this->owner_company_model->get_accounting_settings_for('invoice');
            $data['credit_note_accounting_settings'] = $this->owner_company_model->get_accounting_settings_for('credit_note');
            $data['purchase_accounting_settings'] = $this->owner_company_model->get_accounting_settings_for('purchase');
            $data['debit_note_accounting_settings'] = $this->owner_company_model->get_accounting_settings_for('debit_note');
            $data['payment_accounting_settings'] = array_merge($this->owner_company_model->get_accounting_settings_for('payment'),
                    $this->owner_company_model->get_accounting_settings_for('advance_payment'));
            $data['receipt_accounting_settings'] = array_merge($this->owner_company_model->get_accounting_settings_for('receipt'),
                    $this->owner_company_model->get_accounting_settings_for('advance_receipt'));
            
            $data['manufacturing_accounting_settings'] = $this->owner_company_model->get_accounting_settings_for('manufacturing');
            
            $coa_opening_balance_settings = $this->owner_company_model->get_accounting_settings_for('coa_opening_balance');
            //$expense_settings = $this->owner_company_model->get_accounting_settings_for('expense');
            $corporate_settings = $this->owner_company_model->get_accounting_settings_for('general');

            $data['general_settings'] = array_merge($coa_opening_balance_settings, $corporate_settings);

            $this->load->view('accounting_settings', $data);
        }
    }
   
    /**
     * 
     * @return type
     */
    public function profile() {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters('<div class="error" style="display:none;width:82%;padding:10px;margin-top:20px;border: 1px solid #FF0000">', '</div>');
        $this->form_validation->set_rules('oc_name', 'Company Name', 'required');

        if ($this->form_validation->run() == FALSE) {
            $data['locations'] = $this->location_model->get_all_locations();
            $data['states'] = $this->state_model->get_all_states();
            $data['union_territories'] = $this->state_model->get_all_union_territories();
            $data['owner_company'] = $this->owner_company_model->get_owner_company();

            $this->load->view('profile', $data);
        } else {
            $post_data = $this->input->post();

            if (isset($post_data['oc_receive_push_notification'])) {
                $post_data['oc_receive_push_notification'] = 1;
            } else {
                $post_data['oc_receive_push_notification'] = 0;
            }

            //save image

            date_default_timezone_set('Asia/Kolkata');
            $current_time = now('Asia/Kolkata');
            unset($post_data['oc_logo_path']);
            if (!empty($_FILES['oc_logo_path']['name'])) {
                $file_name_array = explode(".", $_FILES['oc_logo_path']['name']);
                $file_type = $file_name_array[1];
                $file_location = "/uploads/oc_files/logo/" . $current_time . "." . $file_type;

                $uploadPath = './uploads/oc_files/logo';


                if (!file_exists($uploadPath)) {
                    mkdir($uploadPath, 0777, true);
                }

                //Image
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'png|jpg|bmp|jpeg';
                $config['max_size'] = 102400;
                $config['max_width'] = 102400;
                $config['max_height'] = 76800;
                $config['file_name'] = $current_time;
                $config['overwrite'] = TRUE;

                $this->load->library('upload', $config);


                if (!$this->upload->do_upload('oc_logo_path')) {
                    $error = array('error' => $this->upload->display_errors());
                    print_r($error);
                    return;
                } else {
                    $post_data['oc_logo_path'] = $file_location;
                }
            }

            $this->owner_company_model->edit_owner_company($post_data);
            //print_r($post_data);
            redirect('web/master#!/settings/profile', 'refresh');
        }
    }

}
