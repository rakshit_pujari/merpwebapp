<?php

class Recipe extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('recipe_model');
        $this->load->helper('url_helper');
        $this->load->model('product_model');
    }

        /**
     * 
     * @param type $id
     * @return type
     */
    public function delete($recipe_id) {
        if (!$this->is_access_granted('recipe', 'delete')) {
            return;
        }
        if ($this->input->server('REQUEST_METHOD') == 'DELETE') {
            if ($this->recipe_model->delete_recipe_by_id($recipe_id) == false) {
                echo "failed";
            } else {
                echo "success";
            }
        }
    }

    
    /**
     * 
     * @return type
     */
    public function all() {
                //check if user is allowed view access
        if (!$this->is_access_granted('recipe', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        
        $data['recipes'] = array_merge($this->recipe_model->get_all_recipes());
        $this->load->view('landing_recipe', $data);
    }

    /**
     * 
     * @param type $parameter
     * @param type $parameter_id
     */
    public function json($parameter_id = NULL) {
        if ($parameter_id != null) {
            if ($parameter_id != null) {
                $data['recipe'] = $this->recipe_model->get_recipe_with_id($parameter_id);
                //$data['recipe_entries'] = $this->recipe_model->get_recipe_entries($parameter_id);
                $data['recipe_entries'] = $this->recipe_model->get_recipe_entries_with_inventory_status($parameter_id);
            }
        } else {
            $data['recipes'] = $this->recipe_model->get_all_recipes();
        }

        echo json_encode($data);
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function preview($recipe_type, $recipe_id) {
        //check if user is allowed view access
        if (!$this->is_access_granted('recipe', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        if ($recipe_id == null) {
            redirect('web/master#!/recipe/all', 'refresh');
            return;
        }

        if($recipe_type == 'confirm'){
            $data['recipe'] = $this->recipe_model->get_recipe_with_id($recipe_id);
            $data['recipe_entries'] = $this->recipe_model->get_recipe_entries($recipe_id);
        } else if($recipe_type == 'draft'){
            $data['recipe'] = $this->recipe_model->get_draft_recipe_with_id($recipe_id);
            $data['recipe_entries'] = $this->recipe_model->get_draft_recipe_entries($recipe_id);
        }
        
        $this->load->view('preview_recipe', $data);
    }

    /**
     * 
     * @param type $recipe_id
     * @return type
     */
    public function view($recipe_id) {
        
        //check if user is allowed view access
        if (!$this->is_access_granted('recipe', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        
        $this->load->helper('form');
        //$form_data['products'] = $this->product_model->get_activated_products_with_inventory_status('sales');
        $form_data['raw_materials'] = $this->product_model->get_activated_products('purchase');
        
        if ($recipe_id == 'new') {
            $form_data['finished_products'] = $this->recipe_model->get_activated_products_without_recipe('sales');
            $this->load->view('recipe', $form_data);
        } else {
            if ($recipe_id != NULL) {
                $data['recipe'] = $this->recipe_model->get_recipe_with_id($recipe_id);
                $form_data['finished_products'] = array_merge($this->recipe_model->get_activated_products_without_recipe('sales'), 
                        array($this->product_model->get_product_with_id($data['recipe']['recipe_linked_product_id'])));
                $data['recipe_entries'] = $this->recipe_model->get_recipe_entries($recipe_id);
                if (empty($data['recipe'])) {
                    redirect('web/master#!/recipe/all', 'refresh');
                    return;
                }
                $this->load->view('recipe', array_merge($data, $form_data));
            } else {
                throw new Exception('Recipe ID not provided');
            }
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function save($recipe_id = NULL) {
        $this->load->helper('form');
        $recipe = $this->input->post();
        $recipe_entries = $recipe['recipe_entries'];

        if($recipe_id == NULL){
            $recipe['recipe_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
        }
        
        unset($recipe['recipe_entries']);

        $recipe_id = $this->recipe_model->save_recipe($recipe_id, $recipe, $recipe_entries);
        echo $recipe_id;

    }

}
