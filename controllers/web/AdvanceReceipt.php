<?php

class AdvanceReceipt extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('advance_receipt_model');
        $this->load->model('company_model');
        $this->load->helper('url_helper');
        $this->load->model('chart_of_accounts_model');
        $this->load->model('owner_company_model');
    }

    /**
     * 
     * @return type
     */
    public function all() {
        //check if user is allowed view access
        if (!$this->is_access_granted('advance_receipt', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }

        $data['advance_receipts'] = $this->advance_receipt_model->get_all_advance_receipts();
        $this->load->view('landing_advance_receipt', $data);
    }
    
    /**
     * 
     * @param type $linked_company_id
     */
    public function json($linked_company_id = NULL) {
        $data['advance_receipts'] = $this->advance_receipt_model->get_advance_receipts_with_balance($linked_company_id); 
        echo json_encode($data);
    }

    /**
     * 
     * @param type $advance_receipt_id
     * @return type
     */
    public function preview($advance_receipt_id = NULL) {
        //check if user is allowed view access
        if (!$this->is_access_granted('advance_receipt', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        if ($advance_receipt_id != NULL) {
            $data['advance_receipt'] = $this->advance_receipt_model->get_advance_receipt_with_id($advance_receipt_id);

            if (empty($data['advance_receipt'])) {
                redirect('web/master#!/AdvanceReceipt/all');
                return;
            }
            $this->load->view('preview_advance_receipt', array_merge($data));
        }
    }

    /**
     * 
     * @param type $advance_receipt_id
     * @return type
     */
    public function view($advance_receipt_id = NULL) {
        //check if user is allowed view access
        if (!$this->is_access_granted('advance_receipt', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        $this->load->helper('form');
        
        $form_data['customers'] = $this->company_model->get_all_companies('customer');
        $form_data['chart_of_accounts'] = $this->chart_of_accounts_model->get_all_coa();

        if ($advance_receipt_id != NULL) {
            $data['advance_receipt'] = $this->advance_receipt_model->get_advance_receipt_with_id($advance_receipt_id);
            $this->load->view('advance_receipt', array_merge($data, $form_data));
        } else {
            $form_data['advance_receipt_setting'] = $this->owner_company_model->get_accounting_settings_for_field('advance_receipt_amount');
            //print_r($form_data['advance_receipt_setting']);
            $this->load->view('advance_receipt', $form_data);
        }
    }

    /**
     * 
     * @param type $advance_receipt_id
     */
    public function save($advance_receipt_id = NULL) {
        
        //check if user is allowed view access
        if (!$this->is_access_granted('advance_receipt', 'save')) {
            return;
        }
        
        $advance_receipt = $this->input->post();
        unset($advance_receipt['transaction']);
        if($advance_receipt_id == NULL){
            $advance_receipt['advance_receipt_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
        }
        
        $advance_receipt_id = $this->advance_receipt_model->save_advance_receipt($advance_receipt_id, $advance_receipt);
        echo $advance_receipt_id;
    }

}
