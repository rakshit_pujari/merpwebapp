<?php

class Quotation extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('quotation_model');
        $this->load->helper('url_helper');
        $this->load->model('employee_model');
        $this->load->model('state_model');
        $this->load->model('transporter_model');
        $this->load->model('product_model');
        $this->load->model('company_model');
        $this->load->model('owner_company_model');
        $this->load->model('payment_term_model');
        $this->load->model('tax_model');
    }

      /**
     * 
     * @param type $id
     * @return type
     */
    public function delete($quotation_id) {
        if (!$this->is_access_granted('quotation', 'delete')) {
            return;
        }
        if ($this->input->server('REQUEST_METHOD') == 'DELETE') {
            
            if ($this->quotation_model->delete_draft_quotation_by_id($quotation_id)) {
                echo "success";
            } else {
                echo "failed";
            }

        }
    }
  
    
    /**
     * 
     * @return type
     */
    public function all() {
                //check if user is allowed view access
        if (!$this->is_access_granted('quotation', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        
        $data['quotations'] = array_merge($this->quotation_model->get_all_quotations(), $this->quotation_model->get_all_draft_quotations());
        $this->load->view('landing_quotation', $data);
    }

    /**
     * 
     * @param type $parameter
     * @param type $parameter_id
     */
    public function json($parameter = NULL, $parameter_id = NULL) {
        if ($parameter != null) {
            if ($parameter == 'customer') {
                $data['customers'] = $this->quotation_model->get_all_customers();
            }
            if ($parameter == 'quotation_customer_map') {
                $data['quotation_customer_map'] = $this->quotation_model->get_payment_customer_map();
            }
            if ($parameter == 'quotation' && $parameter_id != null) {
                $data['quotation'] = $this->quotation_model->get_quotation_with_id($parameter_id);
                $data['quotation_entries'] = $this->quotation_model->get_quotation_entries($parameter_id);
            }
        } else {
            $data['quotations'] = $this->quotation_model->get_all_quotations();
        }

        echo json_encode($data);
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function preview($quotation_type, $quotation_id) {
        //check if user is allowed view access
        if (!$this->is_access_granted('quotation', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        if ($quotation_id == null) {
            redirect('web/master#!/quotation/all', 'refresh');
            return;
        }

        if($quotation_type == 'confirm'){
            $data['quotation'] = $this->quotation_model->get_quotation_with_id($quotation_id);
            $data['quotation_entries'] = $this->quotation_model->get_quotation_entries($quotation_id);
        } else if($quotation_type == 'draft'){
            $data['quotation'] = $this->quotation_model->get_draft_quotation_with_id($quotation_id);
            $data['quotation_entries'] = $this->quotation_model->get_draft_quotation_entries($quotation_id);
        }
        
        $this->load->view('preview_quotation', $data);
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function view($quotation_type, $id = NULL) {
        
        //check if user is allowed view access
        if (!$this->is_access_granted('quotation', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('quotation_linked_company_display_name', 'Company Name', 'required');

        $form_data['employees'] = $this->employee_model->get_all_employees();
        $form_data['transporters'] = $this->transporter_model->get_all_transporters();
        $form_data['states'] = $this->state_model->get_all_states();
        $form_data['products'] = $this->product_model->get_activated_products_with_inventory_status('sales');
        $form_data['union_territories'] = $this->state_model->get_all_union_territories();
        $form_data['payment_terms'] = $this->payment_term_model->get_non_zero_days_payment_terms();
        $form_data['interstate_taxes'] = $this->tax_model->get_interstate_taxes();
        $form_data['intrastate_taxes'] = $this->tax_model->get_intrastate_taxes();
        $form_data['oc_state_id'] = $this->owner_company_model->get_owner_company()['oc_gst_supply_state_id'];
        $form_data['oc_quotation_terms'] = $this->owner_company_model->get_owner_company()['oc_quotation_terms'];
        
        if ($quotation_type == 'new') {
            $this->load->view('quotation', $form_data);
        } else {
            if ($id != NULL) {
                if($quotation_type == 'confirm'){
                    $data['quotation'] = $this->quotation_model->get_quotation_with_id($id);
                    $data['quotation_entries'] = $this->quotation_model->get_quotation_entries($id);
                } else if($quotation_type == 'draft'){
                    $data['quotation'] = $this->quotation_model->get_draft_quotation_with_id($id);
                    $data['quotation_entries'] = $this->quotation_model->get_draft_quotation_entries($id);
                }
                
                if (empty($data['quotation'])) {
                    redirect('web/master#!/quotation/all', 'refresh');
                    return;
                }
                $this->load->view('quotation', array_merge($data, $form_data));
            } else {
                throw new Exception('Quotation ID not provided');
            }
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function save($quotation_id = NULL) {
        $this->load->helper('form');
        $this->load->helper('date');

        $quotation = $this->input->post();
        $quotation_entries = $quotation['quotation_entries'];

        if($quotation_id == NULL){
            $quotation['quotation_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
        }
        
        unset($quotation['quotation_entries']);
        if (!isset($quotation['quotation_linked_company_id'])) {
            $quotation['quotation_linked_company_id'] = null;
        }

        if ($_POST['transaction'] == 'draft' || $_POST['transaction'] == 'previewDraft' 
                || $_POST['transaction'] == 'confirm') {
            $action = $_POST['transaction'];
            unset($quotation['transaction']);

            date_default_timezone_set('Asia/Kolkata');
            $current_time = now('Asia/Kolkata');

            if (!empty($_FILES['quotation_image_path']['name'])) {
                $file_name_array = explode(".", $_FILES['quotation_image_path']['name']);
                $file_type = $file_name_array[1];
                $file_location = "/uploads/quotation/". $current_time . "." . $file_type;
                $quotation['quotation_image_path'] = $file_location;
            }


            $quotation_id = $this->quotation_model->save_quotation($quotation_id, $quotation, $quotation_entries, $action);
            
            if (!empty($_FILES['quotation_image_path']['name'])) {
                $uploadPath = './uploads/quotation/';
                if (!file_exists($uploadPath)) {
                    mkdir($uploadPath, 0777, true);
                }

                //$new_name = $productId;
                //Image
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = '*';
                $config['max_size'] = 102400;
                $config['max_width'] = 102400;
                $config['max_height'] = 76800;
                $config['file_name'] = $current_time;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('quotation_image_path')) {
                    $error = array('error' => $this->upload->display_errors());
                    print_r($error);
                } else {
                    $data = array('upload_data' => $this->upload->data());
                }
            }


            if($action == 'confirm'){
                //redirect('web/master#!/quotation/preview/confirm/'.$quotation_id);
            } else if($action == 'draft'){
                //redirect('web/master#!/quotation/all', 'refresh');
            } else if($action == 'previewDraft'){
                //redirect('web/master#!/quotation/preview/draft/'.$quotation_id);
            }
            
            echo $quotation_id;
        } else {
            echo "ERROR - Action not defined";
        }
    }

}
