<?php

class PurchaseOrder extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('purchase_order_model');
        $this->load->helper('url_helper');
        $this->load->model('employee_model');
        $this->load->model('state_model');
        $this->load->model('transporter_model');
        $this->load->model('product_model');
        $this->load->model('company_model');
        $this->load->model('owner_company_model');
        $this->load->model('payment_term_model');
        $this->load->model('tax_model');
    }

    
    /**
     * 
     * @param type $purchase_order_id
     * @return type
     */
    public function delete($purchase_order_id) {
        if (!$this->is_access_granted('purchase_order', 'delete')) {
            return;
        }
        if ($this->input->server('REQUEST_METHOD') == 'DELETE') {
            if ($this->purchase_order_model->delete_draft_purchase_order_by_id($purchase_order_id) == false) {
                echo "failed";
            } else {
                echo "success";
            }
        }
    }
    /**
     * 
     * @return type
     */
    public function all() {
                //check if user is allowed view access
        if (!$this->is_access_granted('purchase_order', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        
        $data['purchase_orders'] = array_merge($this->purchase_order_model->get_all_purchase_orders(), $this->purchase_order_model->get_all_draft_purchase_orders());
        $this->load->view('landing_purchase_order', $data);
    }

    /**
     * 
     * @param type $parameter
     * @param type $parameter_id
     */
    public function json($parameter = NULL, $parameter_id = NULL) {
        if ($parameter != null) {
            if ($parameter == 'customer') {
                $data['customers'] = $this->purchase_order_model->get_all_customers();
            }
            if ($parameter == 'purchase_order_customer_map') {
                $data['purchase_order_customer_map'] = $this->purchase_order_model->get_payment_customer_map();
            }
            if ($parameter == 'purchase_order' && $parameter_id != null) {
                $data['purchase_order'] = $this->purchase_order_model->get_purchase_order_with_id($parameter_id);
                $data['purchase_order_entries'] = $this->purchase_order_model->get_purchase_order_entries($parameter_id);
            }
        } else {
            $data['purchase_orders'] = $this->purchase_order_model->get_all_purchase_orders();
        }

        echo json_encode($data);
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function preview($purchase_order_type, $purchase_order_id) {
        //check if user is allowed view access
        if (!$this->is_access_granted('purchase_order', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        if ($purchase_order_id == null) {
            redirect('web/master#!/PurchaseOrder/all', 'refresh');
            return;
        }

        if($purchase_order_type == 'confirm'){
            $data['purchase_order'] = $this->purchase_order_model->get_purchase_order_with_id($purchase_order_id);
            $data['purchase_order_entries'] = $this->purchase_order_model->get_purchase_order_entries($purchase_order_id);
        } else if($purchase_order_type == 'draft'){
            $data['purchase_order'] = $this->purchase_order_model->get_draft_purchase_order_with_id($purchase_order_id);
            $data['purchase_order_entries'] = $this->purchase_order_model->get_draft_purchase_order_entries($purchase_order_id);
        }
        
        $this->load->view('preview_purchase_order', $data);
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function view($purchase_order_type, $id = NULL) {
        
        //check if user is allowed view access
        if (!$this->is_access_granted('purchase_order', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('purchase_order_linked_company_display_name', 'Company Name', 'required');

        $form_data['employees'] = $this->employee_model->get_all_employees();
        $form_data['transporters'] = $this->transporter_model->get_all_transporters();
        $form_data['states'] = $this->state_model->get_all_states();
        $form_data['products'] = $this->product_model->get_activated_products_with_inventory_status('purchase');
        $form_data['union_territories'] = $this->state_model->get_all_union_territories();
        $form_data['payment_terms'] = $this->payment_term_model->get_non_zero_days_payment_terms();
        $form_data['interstate_taxes'] = $this->tax_model->get_interstate_taxes();
        $form_data['intrastate_taxes'] = $this->tax_model->get_intrastate_taxes();
        $form_data['oc_state_id'] = $this->owner_company_model->get_owner_company()['oc_gst_supply_state_id'];
        $form_data['oc_purchase_terms'] = $this->owner_company_model->get_owner_company()['oc_purchase_terms'];
        
        if ($purchase_order_type == 'new') {
            $this->load->view('purchase_order', $form_data);
        } else {
            if ($id != NULL) {
                if($purchase_order_type == 'confirm'){
                    $data['purchase_order'] = $this->purchase_order_model->get_purchase_order_with_id($id);
                    $data['purchase_order_entries'] = $this->purchase_order_model->get_purchase_order_entries($id);
                    
                } else if($purchase_order_type == 'draft'){
                    $data['purchase_order'] = $this->purchase_order_model->get_draft_purchase_order_with_id($id);
                    $data['purchase_order_entries'] = $this->purchase_order_model->get_draft_purchase_order_entries($id);
                }
                
                if (empty($data['purchase_order'])) {
                    redirect('web/master#!/PurchaseOrder/all', 'refresh');
                    return;
                }
                $this->load->view('purchase_order', array_merge($data, $form_data));
            } else {
                throw new Exception('Purchase Order ID not provided');
            }
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function save($purchase_order_id = NULL) {
        $this->load->helper('form');
        $this->load->helper('date');

        $purchase_order = $this->input->post();
        $purchase_order_entries = $purchase_order['purchase_order_entries'];

        if($purchase_order_id == NULL){
            $purchase_order['purchase_order_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
        }
        
        unset($purchase_order['purchase_order_entries']);
        if (!isset($purchase_order['purchase_order_linked_company_id'])) {
            $purchase_order['purchase_order_linked_company_id'] = null;
        }

        if ($_POST['transaction'] == 'draft' || $_POST['transaction'] == 'previewDraft' 
                || $_POST['transaction'] == 'confirm') {
            $action = $_POST['transaction'];
            unset($purchase_order['transaction']);

            date_default_timezone_set('Asia/Kolkata');
            $current_time = now('Asia/Kolkata');

            if (!empty($_FILES['purchase_order_image_path']['name'])) {
                $file_name_array = explode(".", $_FILES['purchase_order_image_path']['name']);
                $file_type = $file_name_array[1];
                $file_location = "/uploads/PurchaseOrder/". $current_time . "." . $file_type;
                $purchase_order['purchase_order_image_path'] = $file_location;
            }


            $purchase_order_id = $this->purchase_order_model->save_purchase_order($purchase_order_id, $purchase_order, $purchase_order_entries, $action);
            
            if (!empty($_FILES['purchase_order_image_path']['name'])) {
                $uploadPath = './uploads/PurchaseOrder/';
                if (!file_exists($uploadPath)) {
                    mkdir($uploadPath, 0777, true);
                }

                //$new_name = $productId;
                //Image
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = '*';
                $config['max_size'] = 102400;
                $config['max_width'] = 102400;
                $config['max_height'] = 76800;
                $config['file_name'] = $current_time;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('purchase_order_image_path')) {
                    $error = array('error' => $this->upload->display_errors());
                    print_r($error);
                } else {
                    $data = array('upload_data' => $this->upload->data());
                }
            }


            if($action == 'confirm'){
                //redirect('web/master#!/PurchaseOrder/preview/confirm/'.$purchase_order_id);
            } else if($action == 'draft'){
                //redirect('web/master#!/PurchaseOrder/all', 'refresh');
            } else if($action == 'previewDraft'){
                //redirect('web/master#!/PurchaseOrder/preview/draft/'.$purchase_order_id);
            }
            
            echo $purchase_order_id;
        } else {
            echo "ERROR - Action not defined";
        }
    }

}
