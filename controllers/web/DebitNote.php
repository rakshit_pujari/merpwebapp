<?php

class DebitNote extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('debit_note_model');
        $this->load->helper('url_helper');
        $this->load->model('employee_model');
        $this->load->model('broker_model');
        $this->load->model('state_model');
        $this->load->model('transporter_model');
        $this->load->model('product_model');
        $this->load->model('company_model');
        $this->load->model('owner_company_model');
        $this->load->model('payment_term_model');
        $this->load->model('tax_model');
        $this->load->model('charge_model');
        $this->load->model('supplementary_invoice_reason_model');
    }

    /**
     * Landing page
     * @return type
     */
    public function all() {
                //check if user is allowed view access
        if (!$this->is_access_granted('debit_note', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        
        $data['debit_notes'] = array_merge($this->debit_note_model->get_all_debit_notes(), $this->debit_note_model->get_all_draft_debit_notes());
        $this->load->view('landing_debit_note', $data);
    }

    /**
     * 
     */
    public function json() {
        $data['debit_notes'] = $this->debit_note_model->get_all_debit_notes();
        echo json_encode($data);
    }
    
    /**
     * 
     * @param type $debit_note_type
     * @param type $debit_note_id
     * @return type
     */
    public function preview($debit_note_type, $debit_note_id) {
        //check if user is allowed view access
        if (!$this->is_access_granted('debit_note', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        
        if ($debit_note_id == null) {
            redirect('web/master#!/DebitNote/all', 'refresh');
            return;
        }

        if($debit_note_type == 'confirm'){
            $data['debit_note'] = $this->debit_note_model->get_debit_note_with_id($debit_note_id);
            $data['debit_note_entries'] = $this->debit_note_model->get_debit_note_entries($debit_note_id);
            $data['debit_note_charge_entries'] = $this->debit_note_model->get_debit_note_charge_entries($debit_note_id);
        } else {
            $data['debit_note'] = $this->debit_note_model->get_draft_debit_note_with_id($debit_note_id);
            $data['debit_note_entries'] = $this->debit_note_model->get_draft_debit_note_entries($debit_note_id);
            $data['debit_note_charge_entries'] = $this->debit_note_model->get_draft_debit_note_charge_entries($debit_note_id);
        }
        
        $this->load->view('preview_debit_note', $data);
    }

    /**
     * 
     * @param type $debit_note_type
     * @param type $debit_note_id
     * @return type
     * @throws Exception
     */
    public function view($debit_note_type, $debit_note_id = NULL) {
        
        //check if user is allowed view access
        if (!$this->is_access_granted('debit_note', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('dn_linked_supplementary_invoice_id', 'ID', 'required');

        $form_data['employees'] = $this->employee_model->get_all_employees();
        $form_data['transporters'] = $this->transporter_model->get_all_transporters();
        $form_data['brokers'] = $this->broker_model->get_all_brokers();
        $form_data['states'] = $this->state_model->get_all_states();
        $form_data['products'] = $this->product_model->get_all_products();
        $form_data['charges'] = $this->charge_model->get_all_charges('purchase');
        $form_data['union_territories'] = $this->state_model->get_all_union_territories();
        $form_data['payment_terms'] = $this->payment_term_model->get_all_payment_terms();
        $form_data['interstate_taxes'] = $this->tax_model->get_interstate_taxes();
        $form_data['intrastate_taxes'] = $this->tax_model->get_intrastate_taxes();
        $form_data['oc_state_id'] = $this->owner_company_model->get_owner_company()['oc_gst_supply_state_id'];
        $form_data['supplementary_invoice_reasons'] = $this->supplementary_invoice_reason_model->get_all_reasons();

        if ($debit_note_type == 'new') {
            $this->load->view('debit_note', $form_data);
        } else {
            if ($debit_note_id != NULL) {
                if($debit_note_type == 'confirm'){
                    $data['debit_note'] = $this->debit_note_model->get_debit_note_with_id($debit_note_id);
                    $data['debit_note_entries'] = $this->debit_note_model->get_debit_note_entries($debit_note_id);
                    $data['debit_note_charge_entries'] = $this->debit_note_model->get_debit_note_charge_entries($debit_note_id);
                } else if($debit_note_type == 'draft'){
                    $data['debit_note'] = $this->debit_note_model->get_draft_debit_note_with_id($debit_note_id);
                    $data['debit_note_entries'] = $this->debit_note_model->get_draft_debit_note_entries($debit_note_id);
                    $data['debit_note_charge_entries'] = $this->debit_note_model->get_draft_debit_note_charge_entries($debit_note_id);
                } else {
                    throw new Exception('Debit note status not given');
                }
               
                $this->load->view('debit_note', array_merge($data, $form_data));
            } else {
                throw new Exception('Invoice ID not provided');
            }
        }
    }

     
    /**
     * 
     * @param type $debit_note_id
     * @return type
     */
    public function save($debit_note_id = NULL) {
        $this->load->helper('form');

        $debit_note = $this->input->post();
        $debit_note_entries = $debit_note['debit_note_entries'];

        if($debit_note_id == NULL){
            $debit_note['dn_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
        }
                
        $debit_note_charge_entries = array();
        if(isset($debit_note['charge_entries'])){
            $debit_note_charge_entries = $debit_note['charge_entries'];
        }
        
        unset($debit_note['debit_note_entries']);
        unset($debit_note['charge_entries']);
        if (!isset($debit_note['dn_linked_company_id'])) {
            $debit_note['dn_linked_company_id'] = null;
        }

        if ($_POST['transaction'] == 'draft' || $_POST['transaction'] == 'previewDraft' 
                || $_POST['transaction'] == 'confirm') {
            $action = $_POST['transaction'];
            unset($debit_note['transaction']);

            $debit_note_id = $this->debit_note_model->save_debit_note($debit_note_id, $debit_note, 
                    $debit_note_entries, $debit_note_charge_entries, $action);
            
            if($action == 'confirm'){
                //redirect('web/master#!/DebitNote/preview/confirm/'.$debit_note_id, 'refresh');
            } else if($action == 'draft'){
                //redirect('web/master#!/DebitNote/all', 'refresh');
            } else if($action == 'previewDraft'){
                //redirect('web/master#!/DebitNote/preview/draft/'.$debit_note_id);
            }
            
            echo $debit_note_id;
            
        } else {
            echo "ERROR - Action not defined";
        }
    }
    
    /**
     * 
     * @param type $debit_note_id
     * @return type
     */
    public function delete($debit_note_type, $debit_note_id) {
        if (!$this->is_access_granted('debit_note', 'delete')) {
            return;
        }
        if ($this->input->server('REQUEST_METHOD') == 'DELETE') {
            if($debit_note_type == 'draft'){
                if ($this->debit_note_model->delete_draft_debit_note_by_id($debit_note_id)) {
                    echo "success";
                } else {
                    echo "failed";
                }
            }
        }
    }

}
