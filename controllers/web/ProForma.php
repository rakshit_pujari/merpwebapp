<?php

class ProForma extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('pro_forma_model');
        $this->load->helper('url_helper');
        $this->load->model('employee_model');
        $this->load->model('broker_model');
        $this->load->model('state_model');
        $this->load->model('transporter_model');
        $this->load->model('product_model');
        $this->load->model('company_model');
        $this->load->model('owner_company_model');
        $this->load->model('payment_term_model');
        $this->load->model('charge_model');
        $this->load->model('tax_model');
    }

    /**
     * 
     * @return type
     */
    public function all() {

        //check if user is allowed view access
        if (!$this->is_access_granted('pro_forma', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }

        $data['pro_formas'] = array_merge($this->pro_forma_model->get_all_pro_formas(), $this->pro_forma_model->get_all_draft_pro_formas());
        $this->load->view('landing_pro_forma', $data);
    }

    /**
     * 
     * @param type $parameter
     * @param type $parameter_id
     */
    public function json($parameter = NULL, $parameter_id = NULL) {
        if ($parameter != null) {
            if ($parameter == 'customer') {
                $data['customers'] = $this->pro_forma_model->get_all_customers();
            }
            if ($parameter == 'pro_forma_customer_map') {
                $data['pro_forma_customer_map'] = $this->pro_forma_model->get_pro_forma_customer_map();
            }
            if ($parameter == 'pro_forma' && $parameter_id != null) {
                $data['pro_forma'] = $this->pro_forma_model->get_pro_forma_with_id($parameter_id);
                $data['pro_forma_entries'] = $this->pro_forma_model->get_pro_forma_entries($parameter_id);
            }

            if ($parameter == 'pro_forma_data' && $parameter_id != null) {
                $data['pro_forma_data'] = $this->pro_forma_model->get_entire_pro_forma_data($parameter_id);
            }
        } else {
            $data['pro_formas'] = $this->pro_forma_model->get_all_pro_formas();
        }
        echo json_encode($data);
    }

    /**
     * 
     * @param type $pro_forma_id
     * @return type
     */
    public function preview($pro_forma_type, $pro_forma_id) {

        //check if user is allowed view access
        if (!$this->is_access_granted('pro_forma', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }

        if ($pro_forma_id == null) {
            redirect('web/master#!/ProForma/all', 'refresh');
            return;
        }
        
        if($pro_forma_type == 'confirm'){
            $data['pro_forma'] = $this->pro_forma_model->get_pro_forma_with_id($pro_forma_id);
            $data['pro_forma_entries'] = $this->pro_forma_model->get_pro_forma_entries($pro_forma_id);
            $data['pro_forma_charge_entries'] = $this->pro_forma_model->get_pro_forma_charge_entries($pro_forma_id);
            //$data['receipt_balance'] = $this->pro_forma_model->get_pro_forma_customer_map($pro_forma_id)[0]['receipt_balance'];
            //$data['linked_documents'] = $this->pro_forma_model->get_linked_documents($pro_forma_id);
        } else if($pro_forma_type == 'draft'){
            $data['pro_forma'] = $this->pro_forma_model->get_draft_pro_forma_with_id($pro_forma_id);
            $data['pro_forma_entries'] = $this->pro_forma_model->get_draft_pro_forma_entries($pro_forma_id);
            $data['pro_forma_charge_entries'] = $this->pro_forma_model->get_draft_pro_forma_charge_entries($pro_forma_id);
        }
        $this->load->view('preview_pro_forma', $data);
    }

    /**
     * 
     * @param type $pro_forma_id
     * @return type
     */
    public function view($pro_forma_type, $pro_forma_id = NULL) {   //endpoint used for 1) displaying new pro forma form 2) displaying existing pro forma ( draft & confirm ) 3) creating new pro forma
        //check if user is allowed view access
        if (!$this->is_access_granted('pro_forma', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }

        $this->load->helper('form');
        
        $form_data['employees'] = $this->employee_model->get_all_employees();
        $form_data['transporters'] = $this->transporter_model->get_all_transporters();
        $form_data['brokers'] = $this->broker_model->get_all_brokers();
        $form_data['states'] = $this->state_model->get_all_states();
        $form_data['products'] = $this->product_model->get_activated_products_with_inventory_status('sales');
        $form_data['charges'] = $this->charge_model->get_all_charges('sales');
        $form_data['union_territories'] = $this->state_model->get_all_union_territories();
        $form_data['payment_terms'] = $this->payment_term_model->get_all_payment_terms();
        $form_data['interstate_taxes'] = $this->tax_model->get_interstate_taxes();
        $form_data['intrastate_taxes'] = $this->tax_model->get_intrastate_taxes();
        
        $owner_company = $this->owner_company_model->get_owner_company();
        $form_data['oc_state_id'] = $owner_company['oc_gst_supply_state_id'];
        $form_data['oc_terms'] = $owner_company['oc_pro_forma_terms'];
        $form_data['oc_sales_default_tax_type'] = $owner_company['oc_sales_default_tax_type'];

        if ($pro_forma_type == 'new') {
            $this->load->view('pro_forma', $form_data);
        } else {
            if ($pro_forma_id != NULL) {
                if($pro_forma_type == 'confirm'){
                    $data['pro_forma'] = $this->pro_forma_model->get_pro_forma_with_id($pro_forma_id);
                    $data['pro_forma_entries'] = $this->pro_forma_model->get_pro_forma_entries($pro_forma_id);
                    $data['pro_forma_charge_entries'] = $this->pro_forma_model->get_pro_forma_charge_entries($pro_forma_id);
                } else if($pro_forma_type == 'draft'){
                    $data['pro_forma'] = $this->pro_forma_model->get_draft_pro_forma_with_id($pro_forma_id);
                    $data['pro_forma_entries'] = $this->pro_forma_model->get_draft_pro_forma_entries($pro_forma_id);
                    $data['pro_forma_charge_entries'] = $this->pro_forma_model->get_draft_pro_forma_charge_entries($pro_forma_id);
                }
                
                if (empty($data['pro_forma'])) {
                    redirect('web/master#!/ProForma/all', 'refresh');
                    return;
                }
                $this->load->view('pro_forma', array_merge($data, $form_data));
            } else {
                throw new Exception('Pro Forma ID not provided');
            }
        }
    }

    /**
     * 
     * @param type $pro_forma_id
     * @return type
     */
    public function save($pro_forma_id = NULL) {
        
        $this->load->helper('form');

        $pro_forma = $this->input->post();
        $pro_forma_entries = $pro_forma['pro_forma_entries'];
        $pro_forma_charge_entries = array();
        if(isset($pro_forma['charge_entries'])){
            $pro_forma_charge_entries = $pro_forma['charge_entries'];
        }
        if($pro_forma_id == NULL){
            $pro_forma['pro_forma_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
        }
        
        unset($pro_forma['pro_forma_entries']);
        unset($pro_forma['charge_entries']);
        if (!isset($pro_forma['pro_forma_linked_company_id'])) {
            $pro_forma['pro_forma_linked_company_id'] = null;
        }

        if ($_POST['transaction'] == 'draft' || $_POST['transaction'] == 'previewDraft' 
                || $_POST['transaction'] == 'confirm') {
            $action = $_POST['transaction'];
            unset($pro_forma['transaction']);

            $pro_forma_id = $this->pro_forma_model->save_pro_forma($pro_forma_id, $pro_forma, $pro_forma_entries, $pro_forma_charge_entries, $action);
            
            if($action == 'confirm'){
                //redirect('web/master#!/ProForma/preview/confirm/'.$pro_forma_id, 'refresh');
            } else if($action == 'draft'){
                //redirect('web/master#!/ProForma/all', 'refresh');
            } else if($action == 'previewDraft'){
                //redirect('web/master#!/ProForma/preview/draft/'.$pro_forma_id, 'refresh');
            }
            
            echo $pro_forma_id;
        } else {
            echo "ERROR - Action not defined";
        }
    }
    
    /**
     * 
     * @param type $pro_forma_id
     * @return type
     */
    public function delete($pro_forma_type, $pro_forma_id) {
        if (!$this->is_access_granted('pro_forma', 'delete')) {
            return;
        }
        if ($this->input->server('REQUEST_METHOD') == 'DELETE') {
            if($pro_forma_type == 'draft'){
                if ($this->pro_forma_model->delete_draft_pro_forma_by_id($pro_forma_id)) {
                    echo "success";
                } else {
                    echo "failed";
                }
            }
        }
    }

}
