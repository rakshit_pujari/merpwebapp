<?php

class OC extends Access_controller {

    public function __construct() {
        parent::__construct();  
        $this->load->model('owner_company_model');
        $this->load->helper('url_helper');
    }

    public function state() {
        $data['state_id'] = $this->owner_company_model->get_owner_company()['oc_gst_supply_state_id'];
        echo json_encode($data);
    }

    /**
     * Set opening balance for owner company
     */
    public function opening($param) {
        if (!$this->is_access_granted('chart_of_accounts', 'view')) {
            return;
        }
        
        $this->load->helper('form');
        if ($param == 'save') {
            if ($this->is_access_granted('chart_of_accounts', 'save')) {
                $post_data = $this->input->post();
                $this->owner_company_model->save_pr_opening_balance($post_data['pr_ob']);
                echo "success";
            } else {
                echo "access not granted";
            }
        } else {
            
            $data['oc_data'] = $this->owner_company_model->get_owner_company();
            
            if($param == 'payables'){
                $data['accounts'] = $this->owner_company_model->get_all_payables();
            } else if($param == 'receivables'){
                $data['accounts'] = $this->owner_company_model->get_all_receivables();
            } else {
                throw new Exception('Unknown entity type');
            }
            
            $data['title'] = $param;
            
            $this->load->view('landing_opening_payables_receivables', $data);
        }
    }
}
