<?php

class Invoice extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('invoice_model');
        $this->load->helper('url_helper');
        $this->load->model('employee_model');
        $this->load->model('broker_model');
        $this->load->model('state_model');
        $this->load->model('transporter_model');
        $this->load->model('product_model');
        $this->load->model('company_model');
        $this->load->model('owner_company_model');
        $this->load->model('payment_term_model');
        $this->load->model('tax_model');
        $this->load->model('charge_model');
        $this->load->model('advance_receipt_model');
    }

    /**
     * 
     * @return type
     */
    public function all() {

        //check if user is allowed view access
        if (!$this->is_access_granted('invoice', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }

        $data['invoices'] = array_merge($this->invoice_model->get_all_invoices(), $this->invoice_model->get_all_draft_invoices());
        $this->load->view('landing_invoice', $data);
    }

    /**
     * 
     * @param type $parameter
     * @param type $parameter_id
     */
    public function json($parameter = NULL, $parameter_id = NULL) {
        if ($parameter != null) {
            if ($parameter == 'customer') {
                $data['customers'] = $this->invoice_model->get_all_customers();
            }
            if ($parameter == 'invoice_customer_map') {
                $data['invoice_customer_map'] = $this->invoice_model->get_invoice_customer_map();
            }
            if ($parameter == 'invoice' && $parameter_id != null) {
                $data['invoice'] = $this->invoice_model->get_invoice_with_id($parameter_id);
                $data['invoice_entries'] = $this->invoice_model->get_invoice_entries($parameter_id);
            }

            if ($parameter == 'invoice_data' && $parameter_id != null) {
                $data['invoice_data'] = $this->invoice_model->get_entire_invoice_data($parameter_id);
            }
        } else {
            $data['invoices'] = $this->invoice_model->get_all_invoices();
        }
        echo json_encode($data);
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function preview($invoice_type, $invoice_id) {

        //check if user is allowed view access
        if (!$this->is_access_granted('invoice', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }

        if ($invoice_id == null) {
            redirect('web/master#!/invoice/all', 'refresh');
            return;
        }
        
        if($invoice_type == 'confirm'){
            $data['invoice'] = $this->invoice_model->get_invoice_with_id($invoice_id);
            $data['invoice_entries'] = $this->invoice_model->get_invoice_entries($invoice_id);
            $data['receipt_balance'] = $this->invoice_model->get_invoice_customer_map($invoice_id)[0]['receipt_balance'];
            $data['linked_documents'] = $this->invoice_model->get_linked_documents($invoice_id);
            $data['invoice_charge_entries'] = $this->invoice_model->get_invoice_charge_entries($invoice_id);
        } else if($invoice_type == 'draft'){
            $data['invoice'] = $this->invoice_model->get_draft_invoice_with_id($invoice_id);
            $data['invoice_entries'] = $this->invoice_model->get_draft_invoice_entries($invoice_id);
            $data['invoice_charge_entries'] = $this->invoice_model->get_draft_invoice_charge_entries($invoice_id);
        }
        $this->load->view('preview_invoice', $data);
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function view($invoice_type, $id = NULL) {   //endpoint used for 1) displaying new invoice form 2) displaying existing invoice ( draft & confirm ) 3) creating new invoice
        //check if user is allowed view access
        if (!$this->is_access_granted('invoice', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }

        $this->load->helper('form');
        
        $form_data['employees'] = $this->employee_model->get_all_employees();
        $form_data['transporters'] = $this->transporter_model->get_all_transporters();
        $form_data['brokers'] = $this->broker_model->get_all_brokers();
        $form_data['states'] = $this->state_model->get_all_states();
        $form_data['charges'] = $this->charge_model->get_all_charges('sales');
        $form_data['products'] = $this->product_model->get_activated_products_with_inventory_status('sales');
        $form_data['union_territories'] = $this->state_model->get_all_union_territories();
        $form_data['payment_terms'] = $this->payment_term_model->get_all_payment_terms();
        $form_data['interstate_taxes'] = $this->tax_model->get_interstate_taxes();
        $form_data['intrastate_taxes'] = $this->tax_model->get_intrastate_taxes();
        
        $owner_company = $this->owner_company_model->get_owner_company();
        $form_data['oc_state_id'] = $owner_company['oc_gst_supply_state_id'];
        $form_data['oc_terms'] = $owner_company['oc_invoice_terms'];
        $form_data['oc_sales_default_tax_type'] = $owner_company['oc_sales_default_tax_type'];

        if ($invoice_type == 'new') {
            $form_data['advance_receipts'] = $this->advance_receipt_model->get_advance_receipts_with_balance(); 
            $this->load->view('invoice', $form_data);
        } else {
            if ($id != NULL) {
                
                if($invoice_type == 'confirm'){
                    $form_data['linked_advance_receipt_ids'] = $this->advance_receipt_model->get_linked_advance_receipts_ids('confirm', $id); 
                    $data['invoice'] = $this->invoice_model->get_invoice_with_id($id);
                    $data['invoice_entries'] = $this->invoice_model->get_invoice_entries($id);
                    $data['invoice_charge_entries'] = $this->invoice_model->get_invoice_charge_entries($id);
                } else if($invoice_type == 'draft'){
                    $form_data['linked_advance_receipt_ids'] = $this->advance_receipt_model->get_linked_advance_receipts_ids('draft', $id); 
                    $data['invoice'] = $this->invoice_model->get_draft_invoice_with_id($id);
                    $data['invoice_entries'] = $this->invoice_model->get_draft_invoice_entries($id);
                    $data['invoice_charge_entries'] = $this->invoice_model->get_draft_invoice_charge_entries($id);
                }
                
                if(isset($data['invoice']['invoice_linked_company_id'])){
                    $form_data['advance_receipts'] = $this->advance_receipt_model->get_linked_and_surplus_balance_advance_receipts($data['invoice']['invoice_linked_company_id'], $id);
                } else {
                    $form_data['advance_receipts'] = array();
                }
                        /*array_merge($this->advance_receipt_model->get_linked_advance_receipts($invoice_type, $id),
                            $this->advance_receipt_model->get_advance_receipts_with_balance($data['invoice']['invoice_linked_company_id'])); */
                
                
                $this->load->view('invoice', array_merge($data, $form_data));
                
            } else {
                throw new Exception('Invoice ID not provided');
            }
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function save($invoice_id = NULL) {
        
        $this->load->helper('form');

        $invoice = $this->input->post();
        $invoice_entries = $invoice['invoice_entries'];
        $invoice_charge_entries = array();
        if(isset($invoice['charge_entries'])){
            $invoice_charge_entries = $invoice['charge_entries'];
        }
        //print_r($invoice);return;
        if($invoice_id == NULL){
            $invoice['invoice_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
        }
        
        unset($invoice['invoice_entries']);
        unset($invoice['charge_entries']);
        if (!isset($invoice['invoice_linked_company_id'])) {
            $invoice['invoice_linked_company_id'] = null;
        }

        if ($_POST['transaction'] == 'draft' || $_POST['transaction'] == 'previewDraft' 
                || $_POST['transaction'] == 'confirm') {
            $action = $_POST['transaction'];
            unset($invoice['transaction']);

            $invoice_id = $this->invoice_model->save_invoice($invoice_id, $invoice, $invoice_entries, $invoice_charge_entries, $action);
            
            if($action == 'confirm'){
                //redirect('web/master#!/invoice/preview/confirm/'.$invoice_id, 'refresh');
            } else if($action == 'draft'){
                //redirect('web/master#!/invoice/all', 'refresh');
            } else if($action == 'previewDraft'){
                //redirect('web/master#!/invoice/preview/draft/'.$invoice_id, 'refresh');
            }
            
            echo $invoice_id;
        } else {
            echo "ERROR - Action not defined";
        }
    }
    
    /**
     * 
     * @param type $id
     * @return type
     */
    public function delete($invoice_type, $invoice_id) {
        if (!$this->is_access_granted('invoice', 'delete')) {
            return;
        }
        if ($this->input->server('REQUEST_METHOD') == 'DELETE') {
            if($invoice_type == 'draft'){
                if ($this->invoice_model->delete_draft_invoice_by_id($invoice_id)) {
                    echo "success";
                } else {
                    echo "failed";
                }
            }
        }
    }

}
