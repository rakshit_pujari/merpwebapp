<?php


class Report extends Access_controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->helper('url_helper');
        $this->load->library('excel');
        $this->load->library('spreadsheet');
        $this->load->model('report_model');
        $this->load->model('GSTR_report_model');
    }
    
    public function gst(){
        $this->load->view('gst');
    }
    
    /**
     * 
     */
    public function all() {
        $this->load->view('landing_report');
    }
    
    /**
     * 
     * @param type $report_type
     * @param type $start_date
     * @param type $end_date
     */
    public function view($report_type, $start_date = NULL, $end_date = NULL){
        $data['report_type'] = $report_type;
        switch($report_type){
            case 'invoice':
                $data['report_items'] = $this->report_model->get_customer_performance_report($start_date, $end_date);
                $data['title'] = 'Customer Performance Summary';                
                $data['icon'] = 'icon-person';  
                break;
            case 'product_sales':
                $data['report_items'] = $this->report_model->get_product_sales_report($start_date, $end_date);
                $data['title'] = 'Product Sales';                
                $data['icon'] = 'icon-person';  
                break;
            case 'purchase_summary':
                $data['report_items'] = $this->report_model->get_purchase_summary($start_date, $end_date);
                $data['title'] = 'Purchase Summary';                
                $data['icon'] = 'icon-person';  
                break;
            case 'invoice_summary':
                $data['report_items'] = $this->report_model->get_invoice_summary($start_date, $end_date);
                $data['title'] = 'Invoice Summary';                
                $data['icon'] = 'icon-person';  
                break;
            case 'vendor_purchase':
                $data['report_items'] = $this->report_model->get_vendor_purchase_report($start_date, $end_date);
                $data['title'] = 'Vendor Purchase Report';                
                $data['icon'] = 'icon-person';  
                break;
            case 'customer_sales':
                $data['report_items'] = $this->report_model->get_customer_sales_report($start_date, $end_date);
                $data['title'] = 'Customer Invoice Report';                
                $data['icon'] = 'icon-person';  
                break;
            case 'product_purchase':
                $data['report_items'] = $this->report_model->get_product_purchase_report($start_date, $end_date);
                $data['title'] = 'Product Purchase Report';                
                $data['icon'] = 'icon-person';  
                break;
            case 'inventory':
                $data['report_items'] = $this->report_model->get_inventory_summary($start_date, $end_date);
                $data['title'] = 'Inventory Summary';                
                $data['icon'] = 'icon-stack';  
                break;
            case 'expense':
                $data['report_items'] = $this->report_model->get_expense_summary($start_date, $end_date);
                $data['title'] = 'Expense Summary';                
                $data['icon'] = 'icon-stack';  
                break;
            case 'transactions':
                $data['report_items'] = $this->report_model->get_transactions_history($start_date, $end_date);
                $data['title'] = 'Transactions History';
                $data['icon'] = 'icon-stack';
                break;
            case 'broker_commission':
                $data['report_items'] = $this->report_model->get_broker_commission($start_date, $end_date);
                $data['title'] = 'Agent Commission';
                $data['icon'] = 'icon-stack';  
                break;
            case 'broker_summary':
                $data['report_items'] = $this->report_model->get_broker_summary($start_date, $end_date);
                $data['title'] = 'Agent Summary';
                $data['icon'] = 'icon-stack';  
                break;
            case 'customer_ageing':
                $data['report_items'] = $this->report_model->get_customer_aging($start_date, $end_date);
                $data['title'] = 'Customer Ageing';
                $data['icon'] = 'icon-stack';  
                break;
            case 'vendor_ageing':
                $data['report_items'] = $this->report_model->get_vendor_aging($start_date, $end_date);
                $data['title'] = 'Vendor Ageing';
                $data['icon'] = 'icon-stack';  
                break;
            default:
                return;
        }
        
        $this->load->view('report', $data);
    }
    
    /**
     * 
     * @param type $report_type
     * @param type $parameter_1
     * @param type $parameter_2    
     */
    public function download($report_type, $parameter_1 = NULL, $parameter_2 = NULL){
        switch($report_type){
            case 'invoice':
                $report_items = $this->report_model->get_customer_performance_report($parameter_1, $parameter_2);
                $title = 'Customer Sales';                 
                break;
            case 'product_sales':
                $report_items = $this->report_model->get_product_sales_report($parameter_1, $parameter_2);
                $title = 'Product Sales';                
                break;
            case 'purchase_summary':
                $report_items = $this->report_model->get_purchase_summary($parameter_1, $parameter_2);
                $title = 'Purchase Summary';                
                break;
            case 'invoice_summary':
                $report_items = $this->report_model->get_invoice_summary($parameter_1, $parameter_2);
                $title = 'Invoice Summary';                
                break;
            case 'vendor_purchase':
                $report_items = $this->report_model->get_vendor_purchase_report($parameter_1, $parameter_2);
                $title = 'Vendor Purchase Report';
                break;
            case 'customer_sales':
                $report_items = $this->report_model->get_customer_sales_report($parameter_1, $parameter_2);
                $title = 'Customer Invoice Report';                
                break;
            case 'product_purchase':
                $report_items = $this->report_model->get_product_purchase_report($parameter_1, $parameter_2);
                $title = 'Product Purchase Report';                
                break;
            case 'inventory':
                $report_items = $this->report_model->get_inventory_summary($parameter_1, $parameter_2);
                $title = 'Inventory Summary';
                break;
            case 'expense':
                $report_items = $this->report_model->get_expense_summary($parameter_1, $parameter_2);
                $title = 'Expense Summary';
                break;
            case 'transactions':
                $report_items = $this->report_model->get_transactions_history($parameter_1, $parameter_2);
                $title = 'Transactions History';
                break;
            case 'broker_commission':
                $report_items = $this->report_model->get_broker_commission($parameter_1, $parameter_2);
                $title = 'Agent Commission';
                break;
            case 'broker_summary':
                $report_items = $this->report_model->get_broker_summary($parameter_1, $parameter_2);
                $title = 'Agent Summary';
                break;
            case 'customer_ageing':
                $report_items = $this->report_model->get_customer_aging($parameter_1, $parameter_2);
                $title = 'Customer Aging'; 
                break;
            case 'vendor_ageing':
                $report_items = $this->report_model->get_vendor_aging($parameter_1, $parameter_2);
                $title = 'Vendor Aging'; 
                break;
            case 'bank':
                $this->load->model('bank_model');
                $report_items = $this->bank_model->get_all_banks();
                $title = 'Banks';
                break;
            case 'broker':
                $this->load->model('broker_model');
                $report_items = $this->broker_model->get_all_brokers_for_report();
                $title = 'Agents';
                break;
            case 'company':
                $this->load->model('company_model');
                $report_items = $this->company_model->get_all_companies_for_report();
                $title = 'Companies';
                break;
            case 'employee':
                $this->load->model('employee_model');
                $report_items = $this->employee_model->get_all_employees_for_report();
                $title = 'Employees';
                break;
            case 'location':
                $this->load->model('location_model');
                $report_items = $this->location_model->get_all_locations_for_report();
                $title = 'Locations';
                break;
            case 'product':
                $this->load->model('product_model');
                $report_items = $this->product_model->get_all_products_for_report();
                $title = 'Products';
                break;
            case 'transporter':
                $this->load->model('transporter_model');
                $report_items = $this->transporter_model->get_all_transporters_for_report();
                $title = 'Transporters';
                break;
            case 'advance_payment':
                $this->load->model('advance_payment_model');
                $report_items = $this->advance_payment_model->get_all_advance_payments_for_report();
                $title = 'Advance Payments';
                break;
            case 'advance_receipt':
                $this->load->model('advance_receipt_model');
                $report_items = $this->advance_receipt_model->get_all_advance_receipts_for_report();
                $title = 'Advance Receipts';
                break;
            case 'payment':
                $this->load->model('payment_model');
                $report_items = $this->payment_model->get_all_payments_for_report();
                $title = 'Payments';
                break;
            case 'receipt':
                $this->load->model('receipt_model');
                $report_items = $this->receipt_model->get_all_receipts_for_report();
                $title = 'Receipts';
                break;
            case 'quotation':
                $this->load->model('quotation_model');
                $report_items = $this->quotation_model->get_all_quotations_for_report();
                $title = 'Quotations';
                break;
            case 'pro_forma':
                $this->load->model('pro_forma_model');
                $report_items = $this->pro_forma_model->get_all_pro_formas_for_report();
                $title = 'Pro Forma';
                break;
            case 'expense_1':
                $this->load->model('expense_model');
                $report_items = $this->expense_model->get_all_expenses_for_report();
                $title = 'Advance Payments';
                break;
            case 'purchase_order':
                $this->load->model('purchase_order_model');
                $report_items = $this->purchase_order_model->get_all_purchase_orders_for_report();
                $title = 'Purchase Orders';
                break;
            case 'purchase':
                $this->load->model('purchase_model');
                $report_items = $this->purchase_model->get_all_purchases_for_report();
                $title = 'Purchase Invoices';
                break;
            
            case 'sales_invoice':
                $this->load->model('invoice_model');
                $report_items = $this->invoice_model->get_all_invoices_for_report();
                $title = 'Sales Invoices';
                break;
            
            case 'debit_note':
                $this->load->model('debit_note_model');
                $report_items = $this->debit_note_model->get_all_debit_notes_for_report();
                $title = 'Debit Notes';
                break;
            
            case 'credit_note':
                $this->load->model('credit_note_model');
                $report_items = $this->credit_note_model->get_all_credit_notes_for_report();
                $title = 'Credit Notes';
                break;
            
            case 'ledger':
                $this->load->model('system_journal_model');
                $report_items = $this->system_journal_model->get_ledger($parameter_1);
                $title = 'Ledger';
                break;
            case 'inventory_overview':
                $this->load->model('inventory_model');
                $report_items = $this->inventory_model->get_overview_for_report($parameter_1);
                $title = 'Inventory Overview';
                break;
            case 'inventory_adjustment':
                $this->load->model('inventory_adjustment_model');
                $report_items = $this->inventory_adjustment_model->get_inventory_adjustment_for_report();
                $title = 'Inventory Adjustment';
                break;
            case 'chart_of_accounts':
                $this->load->model('chart_of_accounts_model');
                $report_items = $this->chart_of_accounts_model->get_all_coa_for_report();
                $title = 'Chart Of Accounts';
                break;
            case 'system_transaction':
                $this->load->model('system_journal_model');
                $report_items = $this->system_journal_model->get_system_transactions_for_report();
                $title = 'System Transactions';
                break;
            case 'journal':
                $this->load->model('journal_model');
                $report_items = $this->journal_model->get_all_journals_for_report();
                $title = 'Journals';
                break;
            case 'ManufacturingOrder':
                $this->load->model('manufacturing_order_model');
                $report_items = $this->manufacturing_order_model->get_all_manufacturing_orders_for_report();
                $title = 'Manufacturing Orders';
                break;
            case 'recipe':
                $this->load->model('recipe_model');
                $report_items = $this->recipe_model->get_all_recipes_for_report();
                $title = 'Bill Of Materials';
                break;
        }
        
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle($title);
        
        $worksheet = $this->excel->getActiveSheet();
        $worksheet->setCellValue('A1', 'Sr No');
        
        //set heading
        $column = 'B';    
        $fontsize = 12;
        $fontbold = true;
        $worksheet->getStyle('A1')->getFont()->setSize($fontsize)->setBold($fontbold);
        foreach ($report_items[0] as $key => $value) { 
            $heading =  ucwords(str_replace('_', ' ', $key));
            $worksheet->setCellValue($column.'1', $heading);
            $worksheet->getColumnDimension($column)->setAutoSize(true);
            $worksheet->getStyle($column.'1')->getFont()->setSize($fontsize)->setBold($fontbold);
            $column++;
        }
        
        $row = 2;
        foreach($report_items as $report_item){
            $worksheet->setCellValue('A'.$row, ($row - 1));
            $column = 'B';  
            foreach ($report_item as $key => $value) { 
                $worksheet->setCellValue($column.$row, $value);
                $column++;
            }
            $row++;
        }
        
        $filename=$report_type.'.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        ob_end_clean(); //VERY VERY IMPORTANT
        $objWriter->save('php://output');    
    }
    
    /**
     * 
     * @param string $version
     * @return type
     * @throws Exception
     */
    public function gstr1($version = NULL, $month = NULL, $year = NULL){ // $version = 1.4
 
        if($version == NULL){
            $version = '1.4';
        }
        $file = './assets/gst_report/ver'.$version.'/GSTR1.xls';
        //$file = './assets/gst_report/ver1.4/GSTR1_V1.4.xls';
        try {
            $objPHPExcel = PHPExcel_IOFactory::load($file);
           // $this->excel->load('/assets/gst_report/ver1.4/GSTR1_V1.4.xls');
        } catch(Exception $e){
            echo "Version not found";
            return;
        }

        //b2b
        $b2b_items = $this->GSTR_report_model->get_gstr1_1_dot_4_b2b($month, $year);
        
        $this->load_entries($objPHPExcel, 2, $b2b_items, 'A', 5);
        
        //b2cl
        $b2cl_items = $this->GSTR_report_model->get_gstr1_1_dot_4_b2cl($month, $year);
        $this->load_entries($objPHPExcel, 4, $b2cl_items, 'A', 5);        
       
        //bs2cs
        $b2cs_items = $this->GSTR_report_model->get_gstr1_1_dot_4_b2cs($month, $year); 
        $this->load_entries($objPHPExcel, 6, $b2cs_items, 'A', 5);        
        
        //cdnr
        $cdnr_items = $this->GSTR_report_model->get_gstr1_1_dot_4_cdnr($month, $year);  
        $this->load_entries($objPHPExcel, 8, $cdnr_items, 'A', 5);        
        
        //cdnur
        $cdnur_items = $this->GSTR_report_model->get_gstr1_1_dot_4_cdnur($month, $year);
        $this->load_entries($objPHPExcel, 10, $cdnur_items, 'A', 5);          

        //exemp
        $exemp_items = $this->GSTR_report_model->get_gstr1_1_dot_4_exemp($month, $year);  
        $this->load_entries($objPHPExcel, 18, $exemp_items, 'B', 5);          
        
        //hsn
        $hsn_items = $this->GSTR_report_model->get_gstr1_1_dot_4_hsn($month, $year);  
        $this->load_entries($objPHPExcel, 19, $hsn_items, 'A', 5);
        
        //doc
        $doc_items = $this->GSTR_report_model->get_gstr1_1_dot_4_doc($month, $year);  
        $this->load_entries($objPHPExcel, 20, $doc_items, 'B', 5);
        
        $filename='GSTR1_'.$version.'.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type 
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
        ob_end_clean(); //VERY VERY IMPORTANT
        $objWriter->save('php://output');    
    }
    
     
    /**
     * 
     * @param string $version
     * @return type
     * @throws Exception
     */
    public function gstr1_xlsx($version = NULL, $month = NULL, $year = NULL){ // $version = 1.4
        
        //set_time_limit(300);
        
        if($version == NULL){
            $version = '1.4';
        }
        //$file = './assets/gst_report/ver'.$version.'/GSTR1_Color.xlsx';
        $file = './assets/gst_report/ver'.$version.'/gstr1_revised_23022018_r1.xlsx';
        try {
            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);
           // $this->excel->load('/assets/gst_report/ver1.4/GSTR1_V1.4.xls');
        } catch(Exception $e){
            echo "Version not found";
            return;
        }
        
        $styleArray = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000'),
                ),
            ),
        );
        //$spreadsheet->getDefaultStyle()->applyFromArray($styleArray);

        //b2b
        $b2b_items = $this->GSTR_report_model->get_gstr1_1_dot_4_b2b($month, $year);
        
        $this->load_entries_xlsx($spreadsheet, 1, $b2b_items, 'A', 5);
        
        //b2cl
        $b2cl_items = $this->GSTR_report_model->get_gstr1_1_dot_4_b2cl($month, $year);
        $this->load_entries_xlsx($spreadsheet, 3, $b2cl_items, 'A', 5);        
       
        //bs2cs
        $b2cs_items = $this->GSTR_report_model->get_gstr1_1_dot_4_b2cs($month, $year); 
        $this->load_entries_xlsx($spreadsheet, 5, $b2cs_items, 'A', 5);        
        
        //cdnr
        $cdnr_items = $this->GSTR_report_model->get_gstr1_1_dot_4_cdnr($month, $year);  
        $this->load_entries_xlsx($spreadsheet, 7, $cdnr_items, 'A', 5);        
        
        //cdnur
        $cdnur_items = $this->GSTR_report_model->get_gstr1_1_dot_4_cdnur($month, $year);
        $this->load_entries_xlsx($spreadsheet, 9, $cdnur_items, 'A', 5);          

        //exemp
        $exemp_items = $this->GSTR_report_model->get_gstr1_1_dot_4_exemp($month, $year);  
        $this->load_entries_xlsx($spreadsheet, 17, $exemp_items, 'B', 5);          
        
        //hsn
        $hsn_items = $this->GSTR_report_model->get_gstr1_1_dot_4_hsn($month, $year);  
        $this->load_entries_xlsx($spreadsheet, 18, $hsn_items, 'A', 5);
        
        //doc
        $doc_items = $this->GSTR_report_model->get_gstr1_1_dot_4_doc($month, $year);  
        $this->load_entries_xlsx($spreadsheet, 19, $doc_items, 'A', 5);
        
        $filename='GSTR1_'.$version.'.xlsx'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type 
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //$objWriter = PHPExcel_IOFactory::createWriter($spreadsheet, 'Excel5');  
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        ob_end_clean(); //VERY VERY IMPORTANT
        $writer->save('php://output');    
    }
   
     /**
     * 
     * @param string $version
     * @return type
     * @throws Exception
     */
    public function gstr2_xlsx($version = NULL, $month = NULL, $year = NULL){ // $version = 1.1
        
        //set_time_limit(300);
        
        if($version == NULL){
            $version = '1.1';
        }
        
        $file = './assets/gst_report/gstr2/ver'.$version.'/gstr2_01032018_r3.xlsx';
        try {
            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);
        } catch(Exception $e){
            echo "Version not found at ".$file;
            return;
        }

        //b2b
        $b2b_items = $this->GSTR_report_model->get_gstr2_1_dot_1_b2b($month, $year);
        $this->load_entries_xlsx($spreadsheet, 1, $b2b_items, 'A', 5);
        
        //b2bur
        $b2bur_items = $this->GSTR_report_model->get_gstr2_1_dot_1_b2bur($month, $year);
        $this->load_entries_xlsx($spreadsheet, 2, $b2bur_items, 'A', 5);        
       
        //imps
        //$imps_items = $this->GSTR_report_model->get_gstr2_1_dot_4_imps($month, $year); 
        //$this->load_entries_xlsx($spreadsheet, 5, $imps_items, 'A', 5);        
        
        //impg
        //$impg_items = $this->GSTR_report_model->get_gstr2_1_dot_4_impg($month, $year); 
        //$this->load_entries_xlsx($spreadsheet, 5, $impg_items, 'A', 5);        
        
        //cdnr
        $cdnr_items = $this->GSTR_report_model->get_gstr2_1_dot_1_cdnr($month, $year);  
        $this->load_entries_xlsx($spreadsheet, 5, $cdnr_items, 'A', 5);        
        
        //cdnur
        $cdnur_items = $this->GSTR_report_model->get_gstr2_1_dot_1_cdnur($month, $year);
        $this->load_entries_xlsx($spreadsheet, 6, $cdnur_items, 'A', 5);               
        
        //exemp
        $exemp_items = $this->GSTR_report_model->get_gstr2_1_dot_1_exemp($month, $year);  
        $this->load_entries_xlsx($spreadsheet, 9, $exemp_items, 'B', 5);
        
        $filename='GSTR2_'.$version.'.xlsx'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type 
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache 
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        ob_end_clean(); //VERY VERY IMPORTANT
        $writer->save('php://output');    
    }
   
     /**
     * 
     * @param string $version
     * @return type
     * @throws Exception
     */
    public function gstr3_xlsx($version = NULL, $month = NULL, $year = NULL){ // $version = 1.1
        
        //set_time_limit(300);
        
        if($version == NULL){
            $version = '1.1';
        }
        
        $file = './assets/gst_report/gstr3/ver3.0/gstr3_31032018_r1.xlsx';
        try {
            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);
        } catch(Exception $e){
            echo "Version not found at ".$file;
            return;
        }

        $items_31a = $this->GSTR_report_model->get_gstr3_3_dot_0_31a($month, $year);
        $this->load_entries_xlsx($spreadsheet, 1, $items_31a, 'C', 11);
        
        $items_31c = $this->GSTR_report_model->get_gstr3_3_dot_0_31c($month, $year);
        $this->load_entries_xlsx($spreadsheet, 1, $items_31c, 'C', 13);          
        
        $items_32 = $this->GSTR_report_model->get_gstr3_3_dot_0_32($month, $year);  
        $this->load_entries_xlsx($spreadsheet, 1, $items_32, 'B', 79);        
        
        $items_4a5 = $this->GSTR_report_model->get_gstr3_3_dot_0_4a5($month, $year);
        $this->load_entries_xlsx($spreadsheet, 1, $items_4a5, 'C', 26);               
        
        $filename='GSTR3_'.$version.'.xlsx'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type 
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache 
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        ob_end_clean(); //VERY VERY IMPORTANT
        $writer->save('php://output');    
    }
   
    
     /**
     * 
     * @param type $objPHPExcel
     * @param type $entries
     * @param type $start_row
     * @param type $start_column
     */
    private function load_entries(&$objPHPExcel, $sheet, $entries, $start_column, $start_row){
        $objPHPExcel->setActiveSheetIndex($sheet);
        $worksheet = $objPHPExcel->getActiveSheet(); 
        $row = $start_row;
        foreach($entries as $entry){
                $column = $start_column;  
                foreach ($entry as $key => $value) { 
                    $worksheet->setCellValue($column.$row, $value);
                    $column++;
                }
                $row++;
        }  
    }
    
    /**
     * 
     * @param type $spreadsheet
     * @param type $entries
     * @param type $start_row
     * @param type $start_column
     */
    private function load_entries_xlsx(&$spreadsheet, $sheet, $entries, $start_column, $start_row){
        $spreadsheet->setActiveSheetIndex($sheet);
        $worksheet = $spreadsheet->getActiveSheet(); 
        $row = $start_row;
        foreach($entries as $entry){
                $column = $start_column;  
                foreach ($entry as $key => $value) { 
                    $worksheet->setCellValue($column.$row, $value);
                    $column++;
                }
                $row++;
        }  
    }
}

?>