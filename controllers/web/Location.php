<?php

class Location extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('location_model');
        $this->load->helper('url_helper');
        $this->load->model('state_model');
    }

    /**
     * For location landing page
     * @return type
     */
    public function all() {

        //check if user is allowed view access
        if (!$this->is_access_granted('location', 'view')) {
            redirect('web/master/', 'refresh');
            return;
        }

        $data['locations'] = $this->location_model->get_all_locations();
        $this->load->view('landing_location', $data);
    }

    /**
     * 
     */
    public function json() {
        $data['locations'] = $this->location_model->get_all_locations();
        echo json_encode($data);
    }

    /**
     * 
     * @param type $location_id
     */
    public function view($location_id = NULL) {
        $this->load->helper('form');
        
        $data['states'] = $this->state_model->get_all_states();
        $data['union_territories'] = $this->state_model->get_all_union_territories();
        
        if ($location_id == NULL) {
            $this->load->view('location', $data);
        } else {
            $data['location'] = $this->location_model->get_location_with_id($location_id);
            $this->load->view('location', $data);
        }
    }

    /**
     * 
     * @param type $location_id
     * @return type
     */
    public function save($location_id = NULL) {
        //check if user is allowed view access
        if (!$this->is_access_granted('location', 'save')) {
            return;
        }

        $data = $this->input->post();
        if ($location_id == NULL) {
            $data['location_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
        }

        $this->location_model->save_location($data, $location_id);
    }

    /**
     * 
     * @param type $location_id
     */
    public function delete($location_id) {
        if (!$this->is_access_granted('location', 'delete')) {
            return;
        }
        if ($this->input->server('REQUEST_METHOD') == 'DELETE') {
            if ($this->location_model->delete_location_by_id($location_id) == false) {
                echo "failed";
            } else {
                echo "success";
            }
        }
    }

}
