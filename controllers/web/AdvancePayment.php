<?php

class AdvancePayment extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('advance_payment_model');
        $this->load->model('company_model');
        $this->load->helper('url_helper');
        $this->load->model('chart_of_accounts_model');
        $this->load->model('owner_company_model');
    }

    /**
     * 
     * @return type
     */
    public function all() {
        //check if user is allowed view access
        if (!$this->is_access_granted('advance_payment', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }

        $data['advance_payments'] = $this->advance_payment_model->get_all_advance_payments();
        $this->load->view('landing_advance_payment', $data);
    }

    
        /**
     * 
     * @param type $linked_company_id
     */
    public function json($linked_company_id = NULL) {
        $data['advance_payments'] = $this->advance_payment_model->get_advance_payments_with_balance($linked_company_id); 
        echo json_encode($data);
    }

    
    /**
     * 
     * @param type $advance_payment_id
     * @return type
     */
    public function preview($advance_payment_id = NULL) {
        //check if user is allowed view access
        if (!$this->is_access_granted('advance_payment', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        if ($advance_payment_id != NULL) {
            $data['advance_payment'] = $this->advance_payment_model->get_advance_payment_with_id($advance_payment_id);

            if (empty($data['advance_payment'])) {
                redirect('web/master#!/AdvancePayment/all');
                return;
            }
            $this->load->view('preview_advance_payment', array_merge($data));
        }
    }

    /**
     * 
     * @param type $advance_payment_id
     * @return type
     */
    public function view($advance_payment_id = NULL) {
        //check if user is allowed view access
        if (!$this->is_access_granted('advance_payment', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        $this->load->helper('form');
        
        $form_data['vendors'] = $this->company_model->get_all_companies('vendor');
        $form_data['chart_of_accounts'] = $this->chart_of_accounts_model->get_all_coa();

        if ($advance_payment_id != NULL) {
            $data['advance_payment'] = $this->advance_payment_model->get_advance_payment_with_id($advance_payment_id);
            $this->load->view('advance_payment', array_merge($data, $form_data));
        } else {
            $form_data['advance_payment_setting'] = $this->owner_company_model->get_accounting_settings_for_field('advance_payment_amount');
            $this->load->view('advance_payment', $form_data);
        }
    }

    /**
     * 
     * @param type $advance_payment_id
     */
    public function save($advance_payment_id = NULL) {
        
        //check if user is allowed view access
        if (!$this->is_access_granted('advance_payment', 'save')) {
            return;
        }
        
        $advance_payment = $this->input->post();
        unset($advance_payment['transaction']);
        if($advance_payment_id == NULL){
            $advance_payment['advance_payment_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
        }
        
        $advance_payment_id = $this->advance_payment_model->save_advance_payment($advance_payment_id, $advance_payment);
        echo $advance_payment_id;
    }

}
