<?php

class Transporter extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('location_model');
        $this->load->model('transporter_model');
        $this->load->helper('url_helper');
        $this->load->model('state_model');
        $this->load->model('payment_term_model');
    }

    /**
     * 
     */
    public function all() {

        //check if user is allowed view access
        if (!$this->is_access_granted('transporter', 'view')) {
            redirect('web/master/');
            return;
        }
        $data['transporters'] = $this->transporter_model->get_all_transporters();
        $this->load->view('landing_transporter', $data);
    }

    /**
     * 
     * @param type $transporter_id
     */
    public function view($transporter_id = NULL) {

        //check if user is allowed view access
        if (!$this->is_access_granted('transporter', 'view')) {
            redirect('web/master/');
            return;
        }

        $this->load->helper('form');
        $data['states'] = $this->state_model->get_all_states();
        $data['union_territories'] = $this->state_model->get_all_union_territories();
        $data['payment_terms'] = $this->payment_term_model->get_all_payment_terms();
        $data['locations'] = $this->location_model->get_all_locations();
        if ($transporter_id == NULL) {
            $this->load->view('transporter', $data);
        } else {
            $data['transporter'] = $this->transporter_model->get_transporter_with_id($transporter_id);
            $this->load->view('transporter', $data);
        }
    }

    /**
     * 
     * @param type $transporter_id
     */
    public function save($transporter_id = NULL) {
        //check if user is allowed view access
        if (!$this->is_access_granted('transporter', 'save')) {
            return;
        }

        $data = $this->input->post();
        if($transporter_id == NULL){
            $data['transporter_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
        }
        
        if (isset($data['is_reverse_charge_applicable_for_transporter'])) {
            $data['is_reverse_charge_applicable_for_transporter'] = 1;
        } else {
            $data['is_reverse_charge_applicable_for_transporter'] = 0;
        }
        
        $this->transporter_model->save_transporter($data, $transporter_id);
    }

    /**
     * 
     * @param type $transporter_id
     */
    public function delete($transporter_id) {
        if (!$this->is_access_granted('transporter', 'delete')) {
            return;
        }
        if ($this->input->server('REQUEST_METHOD') == 'DELETE') {
            if ($this->transporter_model->deleteTransporterById($transporter_id) == false) {
                echo "failed";
            } else {
                echo "success";
            }
        }
    }

}
