<?php

class Master extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('state_model');
        $this->load->helper('url_helper');
    }

    
    public function index($id = NULL) {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['states'] = $this->state_model->get_all_states();
        $data['union_territories'] = $this->state_model->get_all_union_territories();

        $this->load->view('master', $data);
    }

    /**
     * Get subdomain string
     */
    public function subdomain(){
        $subdomain_arr = explode('.', $_SERVER['HTTP_HOST'], 2);
        $subdomain_name = $subdomain_arr[0];
        echo print_r($subdomain_arr);   
    }
    
    public function online() {
        /*$time = time();
        for($i = 0; $i < 100000; $i++){
            $this->state_model->get_all_states();
        }
        $diff = time() - $time;
        echo 'success '.$diff;*/
        echo 'success';
    }
    
    public function error(){
        $data = $this->input->post();
        
        $data['error_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
        $this->load->database();
        date_default_timezone_set('Asia/Kolkata');
        $this->db->query('SET time_zone = "+05:30";');
        
        $this->db->insert('error_log', $data);
    }
    
    /**
     * Error landing page
     */
    public function error_log(){
        $this->load->database();
        
        $query = 'Select *  from error_log left join employee on employee.employee_id = error_log.error_record_created_by order by error_record_creation_time DESC;';
        $data['errors'] = $this->db->query($query)->result_array();
        
        $this->load->view('landing_error', $data);
    }
    
    /**
     * To check if bank, transporter, company etc names are unique to avoid error 
     */
    public function unique(){
        $data = $this->input->post();
        $user_entered_data = $data['user_entered_data'];
        $entity_class_name = $data['entity_class_name'];
        $field_name = $data['field_name'];
        $entity_id = $data['entity_id'];
        $entity_id_field_name = $data['entity_id_field_name'];
        //echo(print_r($data)); 
        $this->load->database();
        $query = 'Select COUNT(*) as existing_row_count from '.$entity_class_name.' where '.$field_name.'="'.$user_entered_data.'"';
        
        if(trim($entity_id)!=''){
            $query.=' AND '.$entity_id_field_name.'!='.$entity_id;
        }

        $result = $this->db->query($query);
        if($result->row_array()['existing_row_count'] == 0){
            echo "success";
        } else {
            echo "failed";
        }
    }
}
