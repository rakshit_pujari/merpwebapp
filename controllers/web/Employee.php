<?php

class Employee extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('location_model');
        $this->load->model('employee_model');
        $this->load->model('bank_model');
        $this->load->helper('url_helper');
    }
    
    /**
     * Employee rights module
     */
    public function rights($action, $employee_id = NULL){
        
        //check if user is allowed view access
        if (!$this->is_access_granted('employee_rights', 'save')) {
            redirect('web/master#!dashboard/one');
            return;
        }
        
        if($action == 'all'){
            $data['employees'] = $this->employee_model->get_all_employees();
            $this->load->view('landing_employee_rights', $data);
        } else if($action == 'view' && $employee_id!=NULL) {
            $this->load->helper('form');
            $data['employee'] = $this->employee_model->get_employee_with_id($employee_id);
            $data['employee_rights'] = $this->employee_model->get_employee_rights_list();
            $data['employee_rights_assigned'] = $this->employee_model->get_rights_assigned_to_employee($employee_id);
            $this->load->view('employee_rights', $data);
        } else if($action == 'save' && $employee_id!=NULL) {
            $employee_rights = array();
            if(isset($this->input->post()['employee_rights'])){
                $employee_rights = $this->input->post()['employee_rights'];
            }
            $this->employee_model->save_employee_rights($employee_id, $employee_rights);
            redirect('web/master#!/employee/rights/all', 'refresh');
        }
    }

    /**
     * For landing page
     */
    public function all() {
        
        //check if user is allowed view access
        if (!$this->is_access_granted('employee', 'view')) {
            redirect('web/master#!/dashboard/one');
            return;
        }
        
        $data['employees'] = $this->employee_model->get_all_employees();
        $this->load->view('landing_employee', $data);
    }

    /**
     * 
     * @param type $id
     */
    public function view($id = NULL) {
        
        //check if user is allowed view access
        if (!$this->is_access_granted('employee', 'view')) {
            redirect('web/master/');
            return;
        }
        
        $this->load->helper('form');
        $data['employee_rights'] = $this->employee_model->get_employee_rights_list();
        $data['locations'] = $this->location_model->get_all_locations();
        $data['banks'] = $this->bank_model->get_all_banks();
        if ($id == NULL) {
            $this->load->view('employee', $data);
        } else {
            $data['employee'] = $this->employee_model->get_employee_with_id($id);
            $this->load->view('employee', $data);
        }
    }

    /**
     * 
     * @param type $employee_id
     */
    public function save($employee_id = NULL) {
        //check if user is allowed view access
        if (!$this->is_access_granted('employee', 'save')) {
            return;
        }
        
        $this->load->library('form_validation');
        $this->load->helper('date');
        $data = $this->input->post();

        if($employee_id == NULL){
            $data['employee_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
        }

        if (trim($data['employee_username']) == "") {
            $data['employee_username'] = NULL;
        }

        unset($data['employee_password_repeat']);
        
        date_default_timezone_set('Asia/Kolkata');
        $current_time = now('Asia/Kolkata');

        if (!empty($_FILES['employee_image_path']['name'])) {
            $file_name_array = explode(".", $_FILES['employee_image_path']['name']);
            //$file_type = $file_name_array[1];
            $file_type = $file_name_array[sizeof($file_name_array) - 1]; 
            $file_location = "/uploads/employee/" . $current_time . "." . $file_type;
            $data['employee_image_path'] = $file_location;
        }
        
        $this->employee_model->save_employee($employee_id, $data);

        //print_r($result);

        if (empty($_FILES['employee_image_path']['name'])) {
            redirect('web/master#!/employee/all', 'refresh');
            return;
        }


        $uploadPath = './uploads/employee/';
        if (!file_exists($uploadPath)) {
            mkdir($uploadPath, 0777, true);
        }

        //$new_name = $productId;
        //Image
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'png|jpg|bmp|jpeg';
        $config['max_size'] = 102400;
        $config['max_width'] = 102400;
        $config['max_height'] = 76800;
        $config['file_name'] = $current_time;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('employee_image_path')) {
            $error = array('error' => $this->upload->display_errors());
            print_r($error);
        } else {
            $data = array('upload_data' => $this->upload->data());
        }

        redirect('web/master#!/employee/all', 'refresh');
    }

    /**
     * 
     * @param type $id
     */
    public function delete($id) {
        if (!$this->is_access_granted('employee', 'delete')) {
            return;
        }
        if ($this->input->server('REQUEST_METHOD') === 'DELETE') {
            if ($this->employee_model->delete_employee_by_id($id) == false) {
                echo "failed";
            } else {
                echo "success";
            }
        }
    }

}
