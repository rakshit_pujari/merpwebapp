<?php

class Dashboard extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url_helper');
        $this->load->model('dashboard_model');
        
    }

   /**
    * Dashboard 1
    */
   public function one(){
       $data['fy_revenue_expense_figures'] = $this->dashboard_model->get_current_fy_profit_figures();
       $data['fy_doc_figures'] = $this->dashboard_model->get_current_fy_doc_figures();
       $this->load->view('dashboard_1', $data);
   }
   
   /**
    * Sales history widget
    */
   public function sales_history(){
       $data['sales_history'] = $this->dashboard_model->get_sales_history();
       echo json_encode($data);
   }
   
   
   /**
    * Purchase history widget
    */
   public function purchase_history(){
       $data['purchase_history'] = $this->dashboard_model->get_purchase_history();
       echo json_encode($data);
   }
   
   /**
    * 
    */
   public function profit_history(){
       $data['profit_history'] = $this->dashboard_model->get_weekly_profit();
       echo json_encode($data);
   }
   
}
