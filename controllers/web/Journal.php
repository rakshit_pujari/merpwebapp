<?php

class Journal extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('journal_model');
        $this->load->model('chart_of_accounts_model');
        
        $this->load->model('broker_model');
        $this->load->model('company_model');
        $this->load->model('employee_model');
        $this->load->model('transporter_model');        
    }

    /**
     * Journals landing page
     */
    public function all() {

        //check if user is allowed view access
        if (!$this->is_access_granted('journal', 'view')) {
            return;
        }

        $data['journals'] = $this->journal_model->get_all_journals_with_entry_details();
        $this->load->view('landing_journal', $data);
    }
    
    /**
     * 
     * @param type $parameter
     */
    public function json($parameter){
        if($parameter == 'entities'){
            $data['brokers'] = $this->broker_model->get_all_brokers();
            $data['companies'] = $this->company_model->get_all_companies();
            $data['employees'] = $this->employee_model->get_all_employees();
            $data['transporters'] = $this->transporter_model->get_all_transporters();
        }
        echo json_encode($data);
    }

    /**
     * Journal form
     * @param type $journal_id
     * @return type
     */
    public function view($journal_id = NULL) {

        //check if user is allowed view access
        if (!$this->is_access_granted('journal', 'view')) {
            return;
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['brokers'] = $this->broker_model->get_all_brokers();
        $data['companies'] = $this->company_model->get_all_companies();
        $data['employees'] = $this->employee_model->get_all_employees();
        $data['transporters'] = $this->transporter_model->get_all_transporters();
        
        if ($journal_id != NULL) {
            $data['journal'] = $this->journal_model->get_journal_with_id($journal_id);
            $data['journal_entries'] = $this->journal_model->get_journal_entries($journal_id);

            $data['parents'] = $this->chart_of_accounts_model->get_all_parent_coa();
            $data['subaccounts'] = $this->chart_of_accounts_model->get_all_subaccounts();

            $this->load->view('journal', $data);
        } else {
            $this->load->view('journal', $data);
        }
    }

    /**
     * Edit existing record
     * @param type $journal_id
     */
    public function save($journal_id = NULL) {
        //check if user is allowed view access
        if (!$this->is_access_granted('journal', 'save')) {
            return;
        }
        
        $journal = $this->input->post();
        $journal_entries = $journal['journal_entries'];

        if ($journal_id == NULL) {
            $journal['journal_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
        }

        unset($journal['journal_entries']);
        $this->journal_model->save_journal($journal_id, $journal, $journal_entries);
        redirect('web/master#!/journal/all', 'refresh');
    }

    /**
     * 
     * @param type $journal_id
     */
    public function delete($journal_id) {

        //check if user is allowed view access
        if (!$this->is_access_granted('journal', 'delete')) {
            return;
        }


        if ($this->input->server('REQUEST_METHOD') == 'DELETE') {
            if ($this->journal_model->delete_journal_by_id($journal_id) == false) {
                echo "failed";
            } else {
                echo "success";
            }
        }
    }

}
