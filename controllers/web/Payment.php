<?php

class Payment extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('payment_model');
        $this->load->helper('url_helper');
        $this->load->model('purchase_model');
        $this->load->model('chart_of_accounts_model');
    }

    public function all() {
        //check if user is allowed view access
        if (!$this->is_access_granted('payment', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }

        $data['payments'] = $this->payment_model->get_all_payments();
        $this->load->view('landing_payment', $data);
    }

    public function preview($payment_id = NULL) {
        //check if user is allowed view access
        if (!$this->is_access_granted('payment', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        if ($payment_id != NULL) {
            $data['payment'] = $this->payment_model->get_payment_with_id($payment_id);

            if (empty($data['payment'])) {
                redirect('web/master#!/Payment/all', 'refresh');
                return;
            }
            $this->load->view('preview_payment', array_merge($data));
        }
    }

    /**
     * 
     * @param type $payment_id
     * @return type
     */
    public function view($payment_id = NULL) {
        //check if user is allowed view access
        if (!$this->is_access_granted('payment', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        $this->load->helper('form');
        
        $form_data['purchase_customers'] = $this->purchase_model->get_all_vendors();
        $form_data['coas'] = $this->chart_of_accounts_model->get_all_coa();

        if ($payment_id != NULL) {
            $data['payment'] = $this->payment_model->get_payment_with_id($payment_id);
            $purchase_id = $data['payment']['payment_linked_purchase_id'];
            $data['purchase_vendor_map'] = $this->purchase_model->get_payment_vendor_map($purchase_id);
            $this->load->view('payment', array_merge($data, $form_data));
        } else {
            $form_data['payment_setting'] = $this->owner_company_model->get_accounting_settings_for_field('payment_amount');
            //print_r($form_data);
            $this->load->view('payment', $form_data);
        }
    }

    /**
     * 
     * @param type $payment_id
     */
    public function save($payment_id = NULL) {
        
        //check if user is allowed view access
        if (!$this->is_access_granted('payment', 'save')) {
            return;
        }
        
        $payment = $this->input->post();
        unset($payment['transaction']);
        if($payment_id == NULL){
            $payment['payment_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
        }
        
        $payment_id = $this->payment_model->save_payment($payment_id, $payment);
        echo $payment_id;
        //redirect('web/master#!/Payment/preview/' . $payment_id, 'refresh');
    }

}
