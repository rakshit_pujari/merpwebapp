<?php

class COA extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('chart_of_accounts_model');
        $this->load->model('owner_company_model');
        $this->load->helper('url_helper');
    }

    /**
     * Chart of accounts landing page
     */
    public function all() {
        if (!$this->is_access_granted('chart_of_accounts', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        $data['chart_of_accounts'] = $this->chart_of_accounts_model->get_all_coa();
        $this->load->view('landing_chart_of_accounts', $data);
    }
    
    
    /**
     * Set opening balance for chart of accounts
     */
    public function opening($action = NULL) {
        if (!$this->is_access_granted('chart_of_accounts', 'view')) {
            //redirect('web/master/dashboard/one');
            return;
        }
        
        $this->load->helper('form');
        if ($action == 'save') {
            if ($this->is_access_granted('chart_of_accounts', 'save')) {
                $post_data = $this->input->post();
                $this->chart_of_accounts_model->save_coa_opening_balance($post_data);
                echo "success";
            } else {
                echo "access not granted";
            }
            redirect('web/master#!/COA/opening', 'refresh');
        } else {
            $data['chart_of_accounts'] = $this->chart_of_accounts_model->get_all_coa();
            $data['oc_data'] = $this->owner_company_model->get_owner_company();
            $this->load->view('landing_chart_of_accounts_opening_balance', $data);
        }
    }
    
    /**
     * Get all chart of accounts as JSON
     */
    public function json($coa_type = NULL) {
        if($coa_type == NULL){
            $data['coa'] = $this->chart_of_accounts_model->get_all_coa();
        } else if($coa_type == 'parent'){
            $data['coa'] = $this->chart_of_accounts_model->get_all_parent_coa();
        } else if($coa_type == 'subaccount'){
            $data['coa'] = $this->chart_of_accounts_model->get_all_subaccounts();
        }
        echo json_encode($data);
    }
    
    /**
     * 
     * @param type $coa_id
     */
    public function view($coa_id = NULL) {
        if (!$this->is_access_granted('chart_of_accounts', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        $this->load->helper('form');

        $data['account_types'] = $this->chart_of_accounts_model->get_all_account_types();
        $data['parent_accounts'] = $this->chart_of_accounts_model->get_all_parent_coa();

        if ($coa_id == NULL) { //CREATE
            $this->load->view('chart_of_account', $data);
        } else { //VIEW
            $data['coa'] = $this->chart_of_accounts_model->get_coa_with_id($coa_id);
            $this->load->view('chart_of_account', $data);
        }
    }

    /**
     * 
     * @param type $id
     */
    public function save($coa_id = NULL) {
        $data = $this->input->post();
        if($coa_id == NULL){
            $data['coa_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
        }        
        $this->chart_of_accounts_model->save_coa($coa_id, $data);
        redirect('web/master#!/COA/all');
    }

    /**
     * 
     * @param type $coa_id
     */
    public function delete($coa_id) {
        if (!$this->is_access_granted('chart_of_accounts', 'view')) {
            return;
        }
        if ($this->input->server('REQUEST_METHOD') == 'DELETE') {
            if ($this->chart_of_accounts_model->delete_chart_of_account_by_id($coa_id) == false) {
                echo "failed";
            } else {
                echo "success";
            }
        }
    }

}
