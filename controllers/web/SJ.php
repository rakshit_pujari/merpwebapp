<?php

class SJ extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('system_journal_model');
        $this->load->model('owner_company_model');
        $this->load->helper('url_helper');
    }

    /**
     * Transactions
     */
    public function transactions(){
        
        //check if user is allowed view access
        if (!$this->is_access_granted('system_transactions', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        
        $data['transactions'] = $this->system_journal_model->get_system_transactions();
        $this->load->view('landing_transactions', $data);
    }
    
    /**
     * Ledger
     * @param type $coa_id
     */
    public function ledger($coa_id = NULL, $entity_type = NULL, $entity_id = NULL){
        
        //check if user is allowed view access
        if (!$this->is_access_granted('ledger', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        $data['coa_id'] = $coa_id;
        $data['entity_id'] = $entity_id;
        $data['entity_type'] = $entity_type;
        
        $data['invoice_amount_account_coa_id'] = $this->owner_company_model->get_accounting_settings_for_field('invoice_amount')['debit_account_coa_id'];
        $data['purchase_amount_account_coa_id'] = $this->owner_company_model->get_accounting_settings_for_field('purchase_amount')['credit_account_coa_id'];

        //case 1 - all accounts total
        //case 2 - a specific account total for all entities
        //case 3 - a specific account break up
        //case 4 - a specific account break up related for a entity
        if($coa_id == NULL){
            //case 1
            $data['ledger_entries'] = $this->system_journal_model->get_ledger();
            $this->load->view('landing_ledger', $data);
        } else {
            if($entity_id == 'all'){
                //case 2
                $data['entities'] = $this->system_journal_model->ledger_account_total_for_entity($entity_type, $coa_id);
                $this->load->view('landing_ledger_entity', $data);
            } else {
                //case 3,4
                $data['transactions'] = $this->system_journal_model->get_ledger($coa_id, $entity_type, $entity_id);
                $this->load->view('landing_ledger_account', $data);
            }
        }
    }
    
    /**
     * Ledger
     * @param type $company_id
     */
    public function entity($entity_type, $entity_id){
        
        //check if user is allowed view access
        if (!$this->is_access_granted('ledger', 'view')) {
            return;
        }
        
        $data['transactions'] = $this->system_journal_model->get_system_transactions_for_entity($entity_type, $entity_id);
        $this->load->view('landing_transactions', $data);
    }
    
    /**
     * Trial Balance
     */
    public function trial_balance(){
        
        //check if user is allowed view access
        if (!$this->is_access_granted('trial_balance', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        
        
        $data['ledger_entries'] = $this->system_journal_model->get_ledger();
        $this->load->view('landing_trial_balance', $data);
    }
   
    /**
     * Profit & Loss Statement
     */
    public function pnl(){ 
        
        //check if user is allowed view access
        if (!$this->is_access_granted('profit_and_loss', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        
        
        $data['ledger_entries'] = $this->system_journal_model->get_ledger();
        $data['corporate_tax'] = $this->owner_company_model->get_accounting_settings_for_field('corporate_tax');
        $this->load->view('landing_pnl', $data);
    }
    
    /**
     * Balance Sheet
     */
    public function bs(){ 
        
        //check if user is allowed view access
        if (!$this->is_access_granted('balance_sheet', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        
        $data['ledger_entries'] = $this->system_journal_model->get_ledger();
        $this->load->view('landing_balance_sheet', $data);
    }
    
    /**
     * Cash Flow Statement
     */
    public function cfs(){ 
        
        //check if user is allowed view access
        if (!$this->is_access_granted('cash_flow', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        
        $data['ledger_entries'] = $this->system_journal_model->get_ledger();
        $data['cash_account_coa_id'] = $this->owner_company_model->get_accounting_settings_for_field('general_cash');
        $data['coa_opening_balance_adjustment'] = $this->owner_company_model->get_accounting_settings_for_field('coa_opening_balance_adjustment');
        $this->load->view('landing_cash_flow', $data);
    }
}