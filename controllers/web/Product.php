<?php

class Product extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('product_model');
        $this->load->model('inventory_model');
        $this->load->model('owner_company_model');
        $this->load->model('tax_model');
        $this->load->model('unique_quantity_code_model');
        $this->load->helper('url_helper');
        $this->load->helper('date');
    }

    /**
     * 
     */
    public function all() {

        //check if user is allowed view access
        if (!$this->is_access_granted('product', 'view')) {
            redirect('web/master/');
            return;
        }

        $data['products'] = $this->product_model->get_all_products_with_inventory_status();
        $this->load->view('landing_product', $data);
    }

    /**
     * Set opening balance for product
     */
    public function opening($action = NULL) {
        $this->load->helper('form');
        
        if ($action == 'save') {
            $post_data = $this->input->post();
            //print_r($post_data); 
            $this->inventory_model->update_opening_balance($post_data);
            redirect('web/master#!/COA/opening', 'refresh');
        } else {
            $data['products'] = $this->inventory_model->get_product_opening_balance_for_all_goods();
            $data['oc_data'] = $this->owner_company_model->get_owner_company();
            $this->load->view('landing_product_opening_stock', $data);
        }
    }

    /**
     * 
     * @param type $transaction_type
     */
    public function json($transaction_type = NULL) {
        $data['products'] = $this->product_model->get_activated_products_with_inventory_status($transaction_type);
        echo json_encode($data);
    }

    /**
     * 
     */
    public function image() {
        $data['result'] = "success";
        echo json_encode($data);
    }

    /**
     * 
     * @param type $product_id
     * @return type
     */
    public function view($product_id = NULL) {

        //check if user is allowed view access
        if (!$this->is_access_granted('product', 'view')) {
            redirect('web/master/');
            return;
        }

        $this->load->helper('form');
        $data['interstate_taxes'] = $this->tax_model->get_interstate_taxes();
        $data['intrastate_taxes'] = $this->tax_model->get_intrastate_taxes();

        $data['unique_quantity_codes'] = $this->unique_quantity_code_model->get_all_active_uqc();

        if ($product_id == NULL) {
            $this->load->view('product', $data);
        } else {
            $data['product'] = $this->product_model->get_product_with_id($product_id);
            $data['product_opening_balance'] = $this->inventory_model->get_product_opening_balance($product_id);
            //print_r($data);
            $this->load->view('product', $data);
        }
    }

    /**
     * 
     * @param type $product_id
     * @return type
     */
    public function save($product_id = NULL) {

        $data = $this->input->post();
        
        if($product_id == NULL){
            $data['product_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
        }
        
        if (isset($data['product_status'])) {
            $data['product_status'] = 'active';
        } else {
            $data['product_status'] = 'deactivated';
        }

        unset($data['product_image_path']);
        date_default_timezone_set('Asia/Kolkata');
        $current_time = now('Asia/Kolkata');

        if (!empty($_FILES['product_image_path']['name'])) {
            $file_name_array = explode(".", $_FILES['product_image_path']['name']);
            $file_type = $file_name_array[1];
            $file_location = "/uploads/product/" . $current_time . "." . $file_type;
            $data['product_image_path'] = $file_location;
        }

        $this->product_model->save_product($product_id, $data);
        //print_r($result);

        if (empty($_FILES['product_image_path']['name'])) {
            redirect('web/master#!/product/all', 'refresh');
            return;
        }


        $uploadPath = './uploads/product/';
        if (!file_exists($uploadPath)) {
            mkdir($uploadPath, 0777, true);
        }

        //$new_name = $productId;
        //Image
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'png|jpg|bmp|jpeg';
        $config['max_size'] = 102400;
        $config['max_width'] = 102400;
        $config['max_height'] = 76800;
        $config['file_name'] = $current_time;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('product_image_path')) {
            $error = array('error' => $this->upload->display_errors());
            print_r($error);
        } else {
            $data = array('upload_data' => $this->upload->data());
        }
    }

    /**
     * 
     * @param type $product_id
     * @return type
     */
    public function delete($product_id) {
        if (!$this->is_access_granted('product', 'delete')) {
            return;
        }
        if ($this->input->server('REQUEST_METHOD') == 'DELETE') {
            if ($this->product_model->delete_product_by_id($product_id) == false) {
                echo "failed";
            } else {
                echo "success";
            }
        }
    }

}
