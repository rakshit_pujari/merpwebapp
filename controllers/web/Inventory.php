<?php

class Inventory extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('inventory_model');
        $this->load->model('product_model');
        $this->load->model('inventory_adjustment_model');
    }

    /**
     * Inventory landing page
     */
    public function overview() {
        if (!$this->is_access_granted('inventory_overview', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        $data['inventory_overview'] = $this->inventory_model->get_overview();
        $this->load->view('landing_inventory', $data);
    }

    /**
     * Inventory page for a product
     * @param type $product_id
     */
    public function product($product_id) {
        if (!$this->is_access_granted('inventory_overview', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        $data['inventory_products'] = $this->inventory_model->get_inventory_transactions($product_id);
        $data['product'] = $this->product_model->get_product_with_id($product_id);
        $data['product_opening_balance'] = $this->inventory_model->get_product_opening_balance($product_id);
        $this->load->view('inventory_transactions_product', $data);
    }

    /**
     * For performing inventory adjustment by value or quantity
     * @param type $parameter
     * @param type $ia_id
     */
    public function adjustment($parameter = null, $ia_id = null) {
        
        if (!$this->is_access_granted('inventory_adjustment', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        
        $this->load->helper('form');
        $this->load->library('form_validation');

        if ($parameter != null) {
            if ($parameter == 'view') {
                $form_data['products'] = $this->inventory_model->get_overview();
                if ($ia_id == NULL) { //CREATE
                    $this->load->view('inventory_adjustment', $form_data);
                } else {//VIEW
                    $data['inventory_adjustment'] = $this->inventory_adjustment_model->get_inventory_adjustment_with_id($ia_id);
                    $data['stock'] = $this->inventory_adjustment_model->get_inventory_entry_before_ia($ia_id);
                    //print_r($data);
                    $this->load->view('inventory_adjustment', array_merge($form_data, $data));
                }
            } else if ($parameter == 'save') {
                $data = $this->input->post();
                if($ia_id == null){
                    $data['ia_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
                }
                $this->inventory_adjustment_model->save_inventory_adjustment($ia_id, $data);
                redirect('web/master#!/Inventory/adjustment/', 'refresh');
            } else if ($parameter == 'delete') {
                if ($this->input->server('REQUEST_METHOD') == 'DELETE') {
                    if ($this->inventory_adjustment_model->delete_inventory_adjustment($ia_id) == false) {
                        echo "failed";
                    } else {
                        echo "success";
                    }
                }
                
            }
        } else {
            $data['inventory_adjustments'] = $this->inventory_adjustment_model->get_all_inventory_adjustments();
            $this->load->view('landing_inventory_adjustment', $data);
        }
    }

}

?>