<?php

class Charge extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('charge_model');
        $this->load->helper('url_helper');
    }

    /**
     * For charge landing page
     * @return type
     */
    public function all() {

        //check if user is allowed view access
        if (!$this->is_access_granted('charge', 'view')) {
            redirect('web/master/', 'refresh');
            return;
        }

        $data['charges'] = $this->charge_model->get_all_charges();
        $this->load->view('landing_charge', $data);
    }

    /**
     * 
     */
    public function json($charge_type = NULL) {
        $data['charges'] = $this->charge_model->get_all_charges($charge_type);
        echo json_encode($data);
    }

    /**
     * 
     * @param type $charge_id
     */
    public function view($charge_id = NULL) {
        $this->load->helper('form');
        if ($charge_id == NULL) {
            $this->load->view('charge');
        } else {
            $data['charge'] = $this->charge_model->get_charge_with_id($charge_id);
            $this->load->view('charge', $data);
        }
    }

    /**
     * 
     * @param type $charge_id
     * @return type
     */
    public function save($charge_id = NULL) {
        //check if user is allowed view access
        if (!$this->is_access_granted('charge', 'save')) {
            return;
        }

        $data = $this->input->post();
        if ($charge_id == NULL) {
            $data['charge_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
        }

        $this->charge_model->save_charge($data, $charge_id);
    }

    /**
     * 
     * @param type $charge_id
     */
    public function delete($charge_id) {
        if (!$this->is_access_granted('charge', 'delete')) {
            return;
        }
        if ($this->input->server('REQUEST_METHOD') == 'DELETE') {
            if ($this->charge_model->delete_charge_by_id($charge_id) == false) {
                echo "failed";
            } else {
                echo "success";
            }
        }
    }

}
