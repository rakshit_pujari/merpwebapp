<?php

class Purchase extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('purchase_model');
        $this->load->helper('url_helper');
        $this->load->model('employee_model');
        $this->load->model('state_model');
        $this->load->model('transporter_model');
        $this->load->model('product_model');
        $this->load->model('company_model');
        $this->load->model('owner_company_model');
        $this->load->model('payment_term_model');
        $this->load->model('tax_model');
        $this->load->model('charge_model');
        $this->load->model('advance_payment_model');
    }

    /**
     * 
     * @param type $purchase_id
     * @return type
     */
    public function delete($purchase_type, $purchase_id) {
        if (!$this->is_access_granted('purchase', 'delete')) {
            return;
        }
        if ($this->input->server('REQUEST_METHOD') == 'DELETE') {
            if($purchase_type == 'draft'){
                if ($this->purchase_model->delete_draft_purchase_by_id($purchase_id)) {
                    echo "success";
                } else {
                    echo "failed";
                }
            }
        }
    }
    
    /**
     * 
     * @return type
     */
    public function all() {
                //check if user is allowed view access
        if (!$this->is_access_granted('purchase', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        
        $data['purchases'] = array_merge($this->purchase_model->get_all_purchases(), $this->purchase_model->get_all_draft_purchases());
        $this->load->view('landing_purchase', $data);
    }

    /**
     * 
     * @param type $parameter
     * @param type $parameter_id
     */
    public function json($parameter = NULL, $parameter_id = NULL) {
        if ($parameter != null) {
            if ($parameter == 'vendor') {
                $data['vendors'] = $this->purchase_model->get_all_vendors();
            }
            if ($parameter == 'purchase_vendor_map') {
                $data['purchase_vendor_map'] = $this->purchase_model->get_payment_vendor_map();
            }
            if ($parameter == 'purchase' && $parameter_id != null) {
                $data['purchase'] = $this->purchase_model->get_purchase_with_id($parameter_id);
                $data['purchase_entries'] = $this->purchase_model->get_purchase_entries($parameter_id);
            }
        } else {
            $data['purchases'] = $this->purchase_model->get_all_purchases();
        }

        echo json_encode($data);
    }

    /**
     * 
     * @param type $purchase_id
     * @return type
     */
    public function preview($purchase_type, $purchase_id) {
        //check if user is allowed view access
        if (!$this->is_access_granted('purchase', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        if ($purchase_id == null) {
            redirect('web/master#!/purchase/all', 'refresh');
            return;
        }

        if($purchase_type == 'confirm'){
            $data['purchase'] = $this->purchase_model->get_purchase_with_id($purchase_id);
            $data['purchase_entries'] = $this->purchase_model->get_purchase_entries($purchase_id);
            $data['payment_balance'] = $this->purchase_model->get_payment_vendor_map($purchase_id)[0]['payment_balance'];
            $data['linked_documents'] = $this->purchase_model->get_linked_documents($purchase_id);
            $data['purchase_charge_entries'] = $this->purchase_model->get_purchase_charge_entries($purchase_id);
        } else if($purchase_type == 'draft'){
            $data['purchase'] = $this->purchase_model->get_draft_purchase_with_id($purchase_id);
            $data['purchase_entries'] = $this->purchase_model->get_draft_purchase_entries($purchase_id);
            $data['purchase_charge_entries'] = $this->purchase_model->get_draft_purchase_charge_entries($purchase_id);
        }
        
        $this->load->view('preview_purchase', $data);
    }

    /** 
     * 
     * @param type $purchase_id
     * @return type
     */
    public function view($purchase_type, $purchase_id = NULL) {
        
        //check if user is allowed view access
        if (!$this->is_access_granted('purchase', 'view')) {
            redirect('web/master/dashboard/one');
            return;
        }
        
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('purchase_linked_company_display_name', 'Company Name', 'required');

        $form_data['employees'] = $this->employee_model->get_all_employees();
        $form_data['transporters'] = $this->transporter_model->get_all_transporters();
        $form_data['states'] = $this->state_model->get_all_states();
        $form_data['charges'] = $this->charge_model->get_all_charges('purchase');
        $form_data['products'] = $this->product_model->get_activated_products_with_inventory_status('purchase');
        $form_data['union_territories'] = $this->state_model->get_all_union_territories();
        $form_data['payment_terms'] = $this->payment_term_model->get_all_payment_terms();
        $form_data['interstate_taxes'] = $this->tax_model->get_interstate_taxes();
        $form_data['intrastate_taxes'] = $this->tax_model->get_intrastate_taxes();

        $owner_company = $this->owner_company_model->get_owner_company();
        $form_data['oc_state_id'] = $owner_company['oc_gst_supply_state_id'];
        $form_data['oc_terms'] = $owner_company['oc_purchase_terms'];
        $form_data['oc_purchase_default_tax_type'] = $owner_company['oc_purchase_default_tax_type'];

        if ($purchase_type == 'new') {
            $form_data['advance_payments'] = $this->advance_payment_model->get_advance_payments_with_balance(); 
            $this->load->view('purchase', $form_data);
        } else {
            if ($purchase_id != NULL) {
                if($purchase_type == 'confirm'){
                    $form_data['linked_advance_payment_ids'] = $this->advance_payment_model->get_linked_advance_payments_ids('confirm', $purchase_id); 
                    $data['purchase'] = $this->purchase_model->get_purchase_with_id($purchase_id);
                    $data['purchase_entries'] = $this->purchase_model->get_purchase_entries($purchase_id);
                    $data['purchase_charge_entries'] = $this->purchase_model->get_purchase_charge_entries($purchase_id);
                } else if($purchase_type == 'draft'){
                    $form_data['linked_advance_payment_ids'] = $this->advance_payment_model->get_linked_advance_payments_ids('draft', $purchase_id); 
                    $data['purchase'] = $this->purchase_model->get_draft_purchase_with_id($purchase_id);
                    $data['purchase_entries'] = $this->purchase_model->get_draft_purchase_entries($purchase_id);
                    $data['purchase_charge_entries'] = $this->purchase_model->get_draft_purchase_charge_entries($purchase_id);
                }
                $form_data['advance_payments'] = $this->advance_payment_model->get_linked_and_surplus_balance_advance_payments($data['purchase']['purchase_linked_company_id'], $purchase_id);
                
                $this->load->view('purchase', array_merge($data, $form_data));
            } else {
                throw new Exception('Purchase ID not provided');
            }
        }
    }

    /**
     * 
     * @param type $purchase_id
     * @return type
     */
    public function save($purchase_id = NULL) {
        $this->load->helper('form');
        $this->load->helper('date');

        $purchase = $this->input->post();
        $purchase_entries = $purchase['purchase_entries'];
        $purchase_charge_entries = array();
        if(isset($purchase['charge_entries'])){
            $purchase_charge_entries = $purchase['charge_entries'];
        }
        if($purchase_id == NULL){
            $purchase['purchase_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
        }
        
        unset($purchase['purchase_entries']);
        unset($purchase['charge_entries']);
        if (!isset($purchase['purchase_linked_company_id'])) {
            $purchase['purchase_linked_company_id'] = null;
        }

        if ($_POST['transaction'] == 'draft' || $_POST['transaction'] == 'previewDraft' 
                || $_POST['transaction'] == 'confirm') {
            $action = $_POST['transaction'];
            unset($purchase['transaction']);

            date_default_timezone_set('Asia/Kolkata');
            $current_time = now('Asia/Kolkata');

            if (!empty($_FILES['purchase_image_path']['name'])) {
                $file_name_array = explode(".", $_FILES['purchase_image_path']['name']);
                $file_type = $file_name_array[1];
                $file_location = "/uploads/purchase/". $current_time . "." . $file_type;
                $purchase['purchase_image_path'] = $file_location;
            }


            $purchase_id = $this->purchase_model->save_purchase($purchase_id, $purchase, $purchase_entries, $purchase_charge_entries, $action);
            
            if (!empty($_FILES['purchase_image_path']['name'])) {
                $uploadPath = './uploads/purchase/';
                if (!file_exists($uploadPath)) {
                    mkdir($uploadPath, 0777, true);
                }

                //$new_name = $productId;
                //Image
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = '*';
                $config['max_size'] = 102400;
                $config['max_width'] = 102400;
                $config['max_height'] = 76800;
                $config['file_name'] = $current_time;

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('purchase_image_path')) {
                    $error = array('error' => $this->upload->display_errors());
                    print_r($error);
                } else {
                    $data = array('upload_data' => $this->upload->data());
                }
            }


            if($action == 'confirm'){
                //redirect('web/master#!/purchase/preview/confirm/'.$purchase_id);
            } else if($action == 'draft'){
                //redirect('web/master#!/purchase/all', 'refresh');
            } else if($action == 'previewDraft'){
                //redirect('web/master#!/purchase/preview/draft/'.$purchase_id);
            }
            
            echo $purchase_id;
        } else {
            echo "ERROR - Action not defined";
        }
    }

}
