<?php

class Login extends Parent_controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url_helper');
        $this->load->library('session');
        $this->load->model('employee_model');
        $this->load->model('owner_company_model');
        $this->load->model('auth_model');
    }

    public function index() {
        $this->load->helper('form');
        $this->load->view('login');
        /*$subdomain_arr = explode('.', $_SERVER['HTTP_HOST'], 2);
        $subdomain_name = $subdomain_arr[0];
        $owner_company = $this->owner_company_model->get_owner_company();
        if(!empty($owner_company['oc_id'])){
            $this->session->set_userdata('oc', $owner_company);
            $this->load->view('login');
        } else {
            echo 'Company not found';
        }*/
    }

    public function authentication() {

        $username = $this->input->post('username');
        $password = $this->input->post('password');
        
        $user = $this->employee_model->get_user($username, $password);
        $owner_company = $this->owner_company_model->get_owner_company();
        
        if(!isset($owner_company)){
            $this->session->set_flashdata('error_notification', 'Company Details Not Found. Please Contact System Admin.');
            redirect('web/login/', 'refresh');
            return;
        }
        
        if($owner_company['oc_application_status'] != 'active'){
            $this->session->set_flashdata('error_notification', 'Your Subscription Is Inactive. Please Contact System Admin.');
            redirect('web/login/', 'refresh');
            return;
        }
        
        date_default_timezone_set('Asia/Kolkata');
        $date_now = new DateTime("now");
        $date_subscription_end = new DateTime($owner_company['oc_subscription_end_date']);
        $date_diff = (int) $date_now->diff($date_subscription_end)->format("%r%a");
        if($date_diff <= -15){
            $this->session->set_flashdata('error_notification', 'Your Subscription Has Expired On '.date("d F Y", strtotime($owner_company['oc_subscription_end_date'])).'. Please Contact System Admin For Renewal.');
            redirect('web/login/', 'refresh');
            return;
        } else if($date_diff <= 0){
            $this->session->set_flashdata('error_notification', 'Your Subscription Has Expired On '.date("d F Y", strtotime($owner_company['oc_subscription_end_date'])).'. Please Contact System Admin For Renewal.');
        } else if($date_diff <= 5){
            $this->session->set_flashdata('error_notification', 'Your Subscription Is About To End In '.$date_diff.' days');
        }
               
        $employee_id = $user['employee_id'];
        
        if ($employee_id > 0) {
            $this->auth_model->record_session_info($user);
            redirect('web/master#!/dashboard/one', 'refresh');
        } else {
            $this->session->set_flashdata('error_notification', 'Invalid Username or Password');
            redirect('web/login/', 'refresh');
        }
    }

    public function signout() {
       $this->auth_model->logout_user();
        redirect('web/login/', 'refresh');
    }

}
