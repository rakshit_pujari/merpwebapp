<?php

class Receipt extends Access_controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('receipt_model');
        $this->load->helper('url_helper');
        $this->load->model('invoice_model');
        $this->load->model('chart_of_accounts_model');
    }

    /**
     * For landing page
     */
    public function all() {
        $data['receipts'] = $this->receipt_model->get_all_receipts();
        $this->load->view('landing_receipt', $data);
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function preview($id = NULL) {
        if ($id != NULL) {
            $data['receipt'] = $this->receipt_model->get_receipt_with_id($id);

            if (empty($data['receipt'])) {
                redirect('web/master#!/Receipt/all', 'refresh');
                return;
            }
            $this->load->view('preview_receipt', array_merge($data));
        }
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function view($id = NULL) {
        $this->load->helper('form');

        $form_data['invoice_customers'] = $this->invoice_model->get_all_customers();
        $form_data['coas'] = $this->chart_of_accounts_model->get_all_coa();

        if ($id != NULL) {
            $data['receipt'] = $this->receipt_model->get_receipt_with_id($id);
            $invoice_id = $data['receipt']['receipt_linked_invoice_id'];
            $data['invoice_customer_map'] = $this->invoice_model->get_invoice_customer_map($invoice_id);
            $this->load->view('receipt', array_merge($data, $form_data));
        } else {
            $form_data['receipt_setting'] = $this->owner_company_model->get_accounting_settings_for_field('receipt_amount');
            $this->load->view('receipt', $form_data);
        }
    }

    /**
     * 
     * @param type $receipt_id
     */
    public function save($receipt_id = NULL) {
        $this->load->helper('form');

        if ($receipt_id == NULL) {
            $receipt['receipt_record_created_by'] = $this->session->userdata('employee_id')['employee_id'];
        }
        $receipt = $this->input->post();
        unset($receipt['transaction']);
        $receipt_id = $this->receipt_model->save_receipt($receipt_id, $receipt);
        echo $receipt_id;
        //redirect('web/master#!/Receipt/preview/' . $receipt_id, 'refresh');
    }

}
