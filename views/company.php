

<!-- Page header -->
<div class="page-header">
    <div ng-controller="formController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/company/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Master</span> - Company</h4>
        </div>
        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!/company/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!"><i class="icon-home2 position-left"></i> Master</a></li>
            <li><a href="#!/company/all"><i class=" icon-office position-left"></i> Company</a></li>
            <li class="active"><?php
                if (isset($company))
                    echo 'COM' . str_pad($company['company_id'], 5, "0", STR_PAD_LEFT);
                else
                    echo "New";
                ?></li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Form validation -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Company</h5>

        </div>


        <div class="panel-body">
            <p class="content-group-lg">A Company is a business entity which can be a Customer as well as Vendor. For accurate GST reporting, you are advised to add the GSTIN of the New Company.  </p>
            <?php echo validation_errors(); ?>
            <?php
            $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id'=>'merpForm');
            if (isset($company))
                echo form_open('web/company/save/' . $company['company_id'], $attributes);
            else
                echo form_open('web/company/save', $attributes);
            ?>
            <fieldset class="content-group">
                <legend class="text-bold">Basic Details</legend>					
                <div class="col-lg-12">							
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="company_invoice_name">Invoice Name <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex ="1" required name="company_invoice_name" type="text" value = "<?php if (isset($company)) echo $company['company_invoice_name']; ?>" tabindex = "1" placeholder="Company Name to be displayed on Invoice">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="company_display_name">Display Name <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" 
                                   onblur ="checkIfUnique(this, 'company', 'company_display_name', '<? if(isset($company)) echo $company['company_id']; ?>', 'company_id')"
                                   required name="company_display_name" type="text" value = "<?php
                            if (set_value('company_display_name') != null)
                                echo set_value('company_display_name');
                            else if (isset($company))
                                echo $company['company_display_name'];
                            ?>" tabindex = "2" placeholder="Enter Display Name. Must be unique.">
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Company Type </label>
                        <div class="col-lg-9">
                            <select class="select form-control" name="company_type" tabindex = "3" data-placeholder="Select a State...">
                                <option value = "customer" <?php if (isset($company)) if ($company['company_type'] == 'customer') echo 'selected'; ?>>Customer</option>
                                <option value = "vendor" <?php if (isset($company)) if ($company['company_type'] == 'vendor') echo 'selected'; ?>>Vendor</option>
                            </select>
                        </div>
                    </div>
                </div>
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Billing Details</legend>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="company_billing_address">Billing Address </label>
                        <div class="col-lg-9">
                            <input class="form-control" name="company_billing_address" type="text" value = "<?php if (isset($company)) echo $company['company_billing_address']; ?>" tabindex = "4" placeholder="Enter Billing Address">
                        </div>
                    </div>

                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="company_billing_area">Area</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="company_billing_area" type="text" value = "<?php if (isset($company)) echo $company['company_billing_area']; ?>" tabindex = "5" placeholder = "Enter area">
                        </div>
                    </div>		
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="city">City <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select required class="form-control select2" name="company_billing_location_id" id="city" tabindex = "6" onChange = "refreshLocationDistrictAndState()" >
                                        <option value = "">Choose a location</option>
                                        <?php
                                        foreach ($locations as $location) {
                                            ?>
                                            <option district = "<?php echo $location['district'] ?>" state = "<?php echo $location['state_name'] ?>" 
                                            <?php
                                            if (set_value('company_billing_location_id') == $location['location_id'])
                                                echo "selected";
                                            else if (isset($company)) {
                                                if ($company['company_billing_location_id'] == $location['location_id'])
                                                    echo "selected";
                                            }
                                            ?> value="<? echo $location['location_id']; ?>"><? echo $location['city_name'] ?>
                                            </option>
                                            <?php
                                        }
                                        ?>
                                    </select>	
                        </div>
                    </div>	

                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="district">District</label>
                        <div class="col-lg-9">
                            <input class="form-control" id="district" type="text" readonly>
                        </div>
                    </div>							

                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="state">State <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" id="state" type="text" readonly>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="company_billing_pincode">Pin Code </label>
                        <div class="col-lg-9">
                            <input class="form-control" name ="company_billing_pincode" type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' tabindex = "7"
                                   value = "<?php if (isset($company)) echo $company['company_billing_pincode'];?>">
                        </div>
                    </div>
                </div>
            </fieldset>


            <fieldset class="content-group">
                <legend class="text-bold">Shipping Details</legend>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="company_shipping_address">Shipping Address</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="company_shipping_address" type="text" value = "<?php if (isset($company)) echo $company['company_shipping_address']; ?>" tabindex = "7" placeholder="Enter Shipping Address">
                        </div>
                    </div>

                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="company_area">Area</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="company_shipping_area" type="text" value = "<?php if (isset($company)) echo $company['company_shipping_area']; ?>" tabindex = "8" placeholder = "Enter Shipping Area">
                        </div>
                    </div>		
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="city">City <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <!--<div class="input-group">
                                    <span class="input-group-btn" style = "vertical-align:top"><button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#modal_default">Add <i class="icon-city position-right"></i></button></span>
                                    <div class="multi-select-full">-->
                            <select required class="form-control select2" name="company_shipping_location_id" id="secondaryFieldCity" tabindex = "9" onChange = "refreshSecondaryLocationDistrictAndState()" >
                                <option value = "">Choose a location</option>
                                <?php
                                foreach ($locations as $location) {
                                    ?>
                                    <option district = "<?php echo $location['district'] ?>" state = "<?php echo $location['state_name'] ?>" 
                                    <?php
                                    if (set_value('company_shipping_location_id') == $location['location_id'])
                                        echo "selected";
                                    else if (isset($company)) {
                                        if ($company['company_shipping_location_id'] == $location['location_id'])
                                            echo "selected";
                                    }
                                    ?> value="<? echo $location['location_id']; ?>"><? echo $location['city_name'] ?>
                                    </option>
                                    <?php
                                }
                                ?>
                            </select>
                            <!--</div>-->
                            <!--</div>	-->
                        </div>
                    </div>	

                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="district">District</label>
                        <div class="col-lg-9">
                            <input class="form-control" id="secondaryFieldDistrict" type="text" readonly>
                        </div>
                    </div>							
                </div>
                <div class="col-lg-12">							
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="state">State <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" id="secondaryFieldState" type="text" readonly>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="company_shipping_pincode">Pin Code </label>
                        <div class="col-lg-9">
                            <input class="form-control" name ="company_shipping_pincode" type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' tabindex = "10"
                                   value = "<?php if (isset($company)) echo $company['company_shipping_pincode'];?>">
                        </div>
                    </div>
                </div>
            </fieldset>


            <fieldset class="content-group">
                <legend class="text-bold">Contact Details</legend>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="company_contact_person_name">Contact Person </label>
                        <div class="col-lg-9">
                            <input class="form-control" name="company_contact_person_name" type="text" value = "<?php if (isset($company)) echo $company['company_contact_person_name']; ?>"  tabindex = "10">
                        </div>
                    </div>


                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="company_email_address">Email Address</label><span id = "emailAddressError" style = "color:red; display:none;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Please check email address format !</span>
                        <div class="col-lg-9">
                            <input class="form-control" name="company_email_address" type="text" value = "<?php if (isset($company)) echo $company['company_email_address']; ?>" tabindex = "11">
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="company_contact_number">Contact Number </label>
                        <div class="col-lg-9">
                            <input class="form-control" name="company_contact_number" 
                                   type="text" 
                                   value = "<?php if (isset($company)) echo $company['company_contact_number']; ?>" onkeypress='return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 32 || event.charCode == 44' tabindex = "12">
                        </div>
                    </div>	
                </div>
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Compliance & Payment Details</legend>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="company_pan_number">PAN Number</label>
                        <div class="col-lg-9">
                            <input class="form-control" style="text-transform:uppercase" name="company_pan_number" type="text" value = "<?php if (isset($company)) echo $company['company_pan_number']; ?>" tabindex = "13" placeholder = "Example - ABCC8787C">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">GST Supply State<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select required class="select2 form-control" name="company_gst_supply_state_id" tabindex="14">
                                <option value = "">Choose a state</option>
                                <optgroup label="States">
                                    <?php
                                    foreach ($states as $state) {
                                        ?>
                                        <option 
                                        <?php
                                        if (set_value('company_gst_supply_state_id') == $state['state_tin_number'])
                                            echo "selected";
                                        else if (isset($company)) {
                                            if ($company['company_gst_supply_state_id'] == $state['state_tin_number'])
                                                echo "selected";
                                        }
                                        ?> value="<? echo $state['state_tin_number']; ?>"><? echo $state['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </optgroup>
                                <optgroup label = "Union Territories">
                                    <?php
                                    foreach ($union_territories as $union_territory) {
                                        ?>
                                        <option 
                                        <?php
                                        if (set_value('company_gst_supply_state_id') == $union_territory['state_tin_number'])
                                            echo "selected";
                                        else if (isset($company)) {
                                            if ($company['company_gst_supply_state_id'] == $union_territory['state_tin_number'])
                                                echo "selected";
                                        }
                                        ?> value="<? echo $union_territory['state_tin_number']; ?>"><? echo $union_territory['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>

                                </optgroup>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="company_gst_number">GST Number<br/><a href = 'https://services.gst.gov.in/services/searchtp' target = 'blank_'> Verify GST#</a></label>
                        <div class="col-lg-9">
                            <input class="form-control" 
                                   onblur ="verifyGstin(this, 'company', 'company_gst_number', '<? if(isset($company)) echo $company['company_id']; ?>', 'company_id')"
                                   onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9@]+/, '')" style="text-transform:uppercase" id = "company_gst_number" name="company_gst_number" type="text" value = "<?php if (isset($company)) echo $company['company_gst_number']; ?>" tabindex = "15" placeholder = "Should be 15 characters">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Payment Terms </label>
                        <div class="col-lg-9">
                            <select class="form-control select" name="company_payment_term_id" tabindex = "16">
                                <?
                                foreach ($payment_terms as $payment_term) {
                                    ?>
                                    <option 
                                    <?
                                    if (isset($company)) {
                                        if ($company['company_payment_term_id'] == $payment_term['payment_term_id'])
                                            echo "selected";
                                    }
                                    ?> value="<? echo $payment_term['payment_term_id']; ?>"><? echo $payment_term['payment_term_display_text'] ?>
                                    </option>
                                    <?
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Reverse Charge </label>
                        <div class="col-lg-9">
                            <div class="checkbox checkbox-switch">
                                <label>
                                    <input type="checkbox" tabindex = "17" name="is_reverse_charge_applicable_for_company" data-on-text="Yes" data-off-text="No" class="switch" <?php if (isset($company)) if ($company['is_reverse_charge_applicable_for_company'] == 1) echo "checked"; ?>>
                                    Applicable
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>

            <div class="text-right">
                <a href="#!/company/all"><button class="btn btn-default">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                <? 
                if($this->session->userdata('access_controller')->is_access_granted('company', 'save')) { ?>
                <button onclick="submitForm('company');" id ="buttonSubmit" class="btn btn-primary">Save<i class="icon-arrow-right14 position-right"></i></button>
                <? } ?>
            </div>
            </form>

        </div>

        </section>
    </div>	
    <!-- /form validation -->
    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->

