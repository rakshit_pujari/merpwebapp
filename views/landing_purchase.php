

<!-- Page header -->
<div class="page-header">

    <div ng-controller="dataTableController" ng-init="load(2)"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Purchase</span> - Invoice</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!purchase/view/new" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-list-alt  position-left" style = "font-size:22px;color:#26A69A !important"></i> <span>Add Purchase</span></a>
                <a href="report/download/purchase" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-home2 position-left"></i>Purchase</li>
            <li class="active"><i class="icon-city position-left"></i>Purchase Invoice</li>
        </ul>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Purchase archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Purchase Invoice</h6>
            <div class="heading-elements">
                </ul>
            </div>
        </div>

        <table class="table masterDataTable">
            <thead>
                <tr>
                    <th>Purchase ID</th>
                    <th>Vendor Name</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th>Payment Status</th>
                    <th>Amount</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($purchases as $purchase): ?>
                    <tr>
                        <td>
                            <? if ($purchase['purchase_status'] == 'confirm') { ?>
                                PUR<?php echo str_pad($purchase['purchase_id'], 5, "0", STR_PAD_LEFT); ?>
                            <? } else { ?>
                                DRAFT
                            <? } ?>
                        </td>
                        <td>
                            <h6 class="no-margin">
                                <? if ($purchase['purchase_status'] == 'confirm') { ?>
                                    <a href="#!purchase/preview/confirm/<? echo $purchase['purchase_id']; ?>">
                                    <? } else { ?>
                                        <a href="#!purchase/view/<? echo $purchase['purchase_status']; ?>/<? echo $purchase['purchase_id']; ?>">
                                        <? } ?><span><?php echo $purchase['purchase_linked_company_display_name']; ?></span></a>
                                    <small class="display-block text-muted">Created by <?php echo $purchase['employee_username']; ?> 
                                        on <?php echo $purchase['purchase_record_creation_time']; ?></small>
                            </h6>
                        </td>
                        <td><?php echo explode(" ", $purchase['purchase_date'])[0]; ?></td>
                        <td><?php
                            if ($purchase['purchase_status'] == 'confirm')
                                echo '<span class="label bg-blue"><span>' . $purchase['purchase_status'] . '</span></span>';
                            else
                                echo '<span class="label bg-danger"><span>' . $purchase['purchase_status'] . '</span></span>';
                            ?></td>
                        <td><?php
                            if (isset($purchase['payment_balance'])) {
                                if ($purchase['payment_balance'] <= 0)
                                    echo '<span class="label bg-success"><span>PAID</span></span>';
                                else if ($purchase['payment_balance'] >= $purchase['purchase_amount'])
                                    echo '<span class="label bg-danger"><span>UNPAID</span></span>';
                                //else if ($purchase['payment_balance'] < 0)
                                  //  echo '<span class="label bg-success"><span>₹ ' . number_format(abs($purchase['payment_balance'])) . ' OVERPAID</span></span>';
                                else
                                    echo '<span class="label bg-orange"><span>₹ ' . number_format($purchase['payment_balance']) . ' PENDING</span></span>';
                            }
                            
                            ?></td>
                        <td>₹ <?php echo number_format($purchase['purchase_amount'], 2); ?></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li><? if ($purchase['purchase_status'] == 'confirm') { ?>
                                        <a href="#!purchase/preview/confirm/<? echo $purchase['purchase_id']; ?>">
                                        <? } else { ?>
                                            <a href="#!purchase/view/<? echo $purchase['purchase_status']; ?>/<? echo $purchase['purchase_id']; ?>">
                                            <? } ?>
                                            <i class="icon-file-eye"></i></a></li>
                                            <? if ($this->session->userdata('access_controller')->is_access_granted('purchase', 'delete')
                                                && $purchase['purchase_status'] == 'draft') { 
                                            ?>
                                            <li><a href='javascript: void(0)' onclick = 'showDeleteDialog("purchase", <?php echo $purchase['purchase_id']; ?>)'><i class="glyphicon glyphicon-trash"></i></a></li>
                                        <? } ?>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td>Purchase ID</td>
                    <td>Date</td>
                    <td>Vendor Name</td>
                    <th>Status</th>
                    <th>Payment Status</th>
                    <td>Amount</td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->