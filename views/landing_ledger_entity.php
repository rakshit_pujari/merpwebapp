

<!-- Page header -->
<div class="page-header">

    <div ng-controller="noGroupDataTableController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Accounting</span> - Ledger</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <!--<a href="report/download/ledger" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>-->
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-calculator2 position-left"></i>Accounting</li>
            <li><a href="#!/SJ/ledger/"><i class="icon-book2 position-left"></i>Ledger</a></li>
            <? if (isset($companies[0])){ ?>
            <li <? if(!isset($entity_id)) echo 'class="active"'; ?>>
                <? if(isset($entity_id)) echo '<a href="#!/SJ/ledger/'.$coa_id.'">'; ?>
                    <? echo $entities[0]['coa_account_name']; ?>
                <? if(isset($entity_id)) echo '</a>'; ?>
            </li>
            <? } ?>
            <? if(isset($entity_id)) echo '<li class="active">All</li>'; ?>
        </ul>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Ledgers archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Ledger</h6>
            <div class="heading-elements">
                </ul>
            </div>
        </div>

        <table class="table noGroupDataTable">
            <thead>
                <tr>
                    <th>Sr</th>
                    <th>Company Name</th>
                    <th>Debit</th>
                    <th>Credit</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $count = 0;
                    $debit_total = 0;
                    $credit_total = 0;
                    foreach ($entities as $entity) {
                        
                    $count++;    
                        ?>
                <tr>
                    <td>
                        <?
                        echo $count;
                        ?>
                        
                    </td>
                    <td>
                        <?
                        if (isset($entity['sj_linked_broker_id'])){
                            $sj_linked_entity_id_field = 'sj_linked_broker_id';
                            $display_field_name = 'broker_name';
                        } else if(isset($entity['sj_linked_company_id'])){
                            $sj_linked_entity_id_field = 'sj_linked_company_id';
                            $display_field_name = 'company_display_name';
                        } else if(isset($entity['sj_linked_employee_id'])){
                            $sj_linked_entity_id_field = 'sj_linked_employee_id';
                            $display_field_name = 'employee_name';
                        } else if(isset($entity['sj_linked_transporter_id'])){ 
                           $sj_linked_entity_id_field = 'sj_linked_transporter_id';
                           $display_field_name = 'transporter_name';
                        }                        
                        ?>
                            
                        <a href="#!SJ/ledger/<? echo $coa_id; ?>/<? echo $entity_type; ?>/<? if($entity[$sj_linked_entity_id_field]!=0)
                            echo $entity[$sj_linked_entity_id_field];
                        else 
                            echo "unregistered";?>"><span>
                        <?
                        if (isset($entity[$display_field_name])) {
                            echo $entity[$display_field_name];
                        } else {
                            echo "Unregistered";
                        }
                        ?>
                        </span></a>
                    </td>
                    <td>
                        <?
                        if($entity['debit_balance'] > $entity['credit_balance']){
                            $debit_total+=($entity['debit_balance'] - $entity['credit_balance']);
                            echo '₹ '. number_format(($entity['debit_balance'] - $entity['credit_balance']), 2);
                        }
                        ?>
                    </td>
                    <td>
                        <?
                        if($entity['debit_balance'] < $entity['credit_balance']){
                            $credit_total+=($entity['credit_balance'] - $entity['debit_balance']);
                            echo '₹ '. number_format(($entity['credit_balance'] - $entity['debit_balance']), 2);
                        }
                        ?>
                    </td>
                    </tr>
                    <? } ?>
            </tbody>
            <tfoot>
                <tr>
                    <th></th>
                    <th>Total</th>
                    <th><span style="color:<? if(round($debit_total, 2) == round($credit_total, 2)) echo '#00AA00'; else echo '#FF0000';?>"><? echo '₹ '. number_format($debit_total, 2); ?></span></th>
                    <th><span style="color:<? if(round($debit_total, 2) == round($credit_total, 2)) echo '#00AA00'; else echo '#FF0000';?>"><? echo '₹ '. number_format($credit_total, 2); ?></span></th>
                </tr>
                <tr>
                    <th></th>
                    <th>Balance</th>
                    <th><? if($debit_total > $credit_total){
                        echo '₹ '.number_format(abs($debit_total - $credit_total), 2).' (DR)';
                    } ?></th>
                    <th><? if($debit_total < $credit_total) {
                        echo '₹ '.number_format(abs($debit_total - $credit_total), 2).' (CR)';
                    } ?></th>
                </tr>
            </tfoot>
            
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->