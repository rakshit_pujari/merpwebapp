
<div ng-controller="recipeController" ng-init="load()"></div>
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/recipe/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Manufacturing</span> - Bill Of Materials (Recipe)</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!/recipe/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!"><i class="icon-home2 position-left"></i>Manufacturing</a></li>
            <li><a href="#!/recipe/all"><i class="glyphicon glyphicon-list-alt  position-left"></i>Bill Of Materials (Recipe)</a></li>
            <li class="active"><?php
                if (isset($recipe)) {
                    echo 'BOM' . str_pad($recipe['recipe_id'], 5, "0", STR_PAD_LEFT);
                } else {
                    echo "New";
                }
                ?></li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Form validation -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Recipe</h5>

        </div>


        <div class="panel-body">
            <?php
            $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id' => 'merpForm');
            if (isset($recipe))
                echo form_open_multipart('web/recipe/save/' . $recipe['recipe_id'], $attributes);
            else
                echo form_open_multipart('web/recipe/save', $attributes);
            ?>
            <p class="content-group-lg">A Bill of Material (Recipe) is a list of items (ingredients) required for manufacturing a product. Only one BOM (Recipe) can be maintained for one unique Product SKU / item.</p>

            <fieldset class="content-group">
                <legend class="text-bold">Details</legend>	
                <div class="form-group col-lg-12">	
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Product <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="form-control select2" name="recipe_linked_product_id" id = "recipe_linked_product_id" tabindex = "1" required>
                                <option value = "">Choose a product</option>
                                <?
                                foreach ($finished_products as $finished_product) {
                                    ?>
                                    <option 
                                    <?php
                                    if (isset($recipe)) {
                                        if ($recipe['recipe_linked_product_id'] == $finished_product['product_id'])
                                            echo "selected";
                                    }
                                    ?> value="<? echo $finished_product['product_id']; ?>"><? echo $finished_product['product_name']. ' ( '. $finished_product['uqc_text']. ' )'; ?>
                                    </option>
                                    <?
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Quantity <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name = "recipe_product_quantity" type="number" required min ="1"
                                   value = "<?php if (isset($recipe)) echo $recipe['recipe_product_quantity']; ?>">

                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Notes</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name = "recipe_notes" type="text"
                                   value = "<?php if (isset($recipe)) echo $recipe['recipe_notes']; ?>">

                        </div>
                    </div>
                </div>
                    
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Product Details</legend>
                <table class="table recipeDatatable">
                    <thead>
                        <tr>
                            <th>Sr No</th>
                            <th>Product</th>
                            <th>Quantity</th>
                            <th>UQC</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 0;
                        if (isset($recipe_entries))
                            foreach ($recipe_entries as $recipe_entry) {
                                $i++;
                                ?> 
                                <tr>
                                    <td><? echo $i; ?></td>

                                    <td>
                                        <select class="dtSelect2" onchange = "updateRecipeProducts(this, true);">
                                            <? foreach ($raw_materials as $raw_material) { ?>
                                                <option value="<? echo $raw_material['product_id']; ?>" <? if ($recipe_entry['re_linked_product_id'] == $raw_material['product_id']) echo "selected"; ?> >
                                                    <? echo $raw_material['product_name']; ?>
                                                </option>
                                            <? } ?> 

                                        </select>
                                    </td>		
                                    <td>
                                        <input name = "recipe_entries[<? echo $i; ?>][<? echo $recipe_entry['re_linked_product_id']; ?>]"
                                               class = "form-control" type = "number" min = 0 step = 0.01 value = "<? echo $recipe_entry['re_linked_product_quantity']; ?>"></input>
                                    </td>
                                    
                                    <td>
                                        <? echo $recipe_entry['uqc_text']; ?>
                                    </td>

                                    <td>
                                        <?
                                        if ($i != 1)
                                            echo '<a href="javascript: void(0)" onclick = "deleteRecipeRow(this);refreshAndReIndexEntireRecipeTable();"><i class="glyphicon glyphicon-trash"></i></a>';
                                        ?>
                                    </td>
                                </tr>

                                    <?
                                }
                            ?>

                    </tbody>
                    <tfoot>
                        <tr>
                            <td>Sr No</td>
                            <td>Product</td>
                            <td>Quantity</td>
                            <td>UQC</td>
                            <td class="text-center">Actions</td>
                        </tr>
                    </tfoot>
                </table>
                <div class="form-group col-lg-12">
                    <a onclick = "updateRecipeProducts(null);" class="btn btn-link btn-float has-text text-success"><span>+Add New Row</span></a>
                </div>	


                <div class="form-group col-lg-6">
                </div>		
            </fieldset>

            <div class="text-right">
                <a href="#!/recipe/all"><button class="btn btn-default">Cancel<i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                <? if ($this->session->userdata('access_controller')->is_access_granted('recipe', 'save')) { ?>
                    <button onclick="submitForm('recipe')" id = "buttonSaveConfirmRecipe" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                <? } ?>
            </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->