

<!-- Page header -->
<div class="page-header">

    <div ng-controller="dataTableController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Master</span> - Location</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!location/view" class="btn btn-link btn-float has-text"><i class="icon-city text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Add Location</span></a>
                <a href="report/download/location" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-home2 position-left"></i>Master</li>
            <li class="active"><i class="icon-city position-left"></i>Location</li>
        </ul>

    </div>
</div>
<!-- /page header -->



<!-- Content area -->
<div class="content">

    <!-- Invoice archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Location</h6>
            <div class="heading-elements">
            </div>
        </div>

        <table class="table masterDataTable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>City</th>
                    <th>District</th>
                    <th>State</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($locations as $location): ?>
                    <tr>
                        <td>LOC<?php echo str_pad($location['location_id'], 5, "0", STR_PAD_LEFT); ?></td>
                        <td>
                            <h6 class="no-margin">
                                <a href="#!location/view/<?php echo $location['location_id']; ?>"><span><?php echo $location['city_name']; ?></span></a>
                                <small class="display-block text-muted">Created by <?php echo $location['employee_username']; ?> 
                                    on <?php echo $location['location_record_creation_time']; ?></small>
                            </h6>
                        </td>
                        <td><?php echo $location['district']; ?></td>
                        <td><?php echo $location['state_name']; ?></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li><a href="#!location/view/<?php echo $location['location_id']; ?>"><i class="icon-file-eye"></i></a></li>
                                <? 
                                if($this->session->userdata('access_controller')->is_access_granted('location', 'delete')) { ?>
                                <li><a href='javascript: void(0)' onclick = 'showDeleteDialog("location", <?php echo $location['location_id']; ?>)'><i class="glyphicon glyphicon-trash"></i></a></li>
                                <? } 
                                ?>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td>ID</td>
                    <td>City</td>
                    <td>District</td>
                    <td>State</td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->