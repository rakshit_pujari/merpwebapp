<div style = "padding-top:20px;padding-left:20px;padding-right:20px;">
    <?php echo validation_errors('  <div style = "border-color: #ebccd1;background-color:#f2dede; color:#a94442" class="alert alert-danger alert-dismissable"><a style = "padding-bottom:10px" href="#" class="close" data-dismiss="alert" aria-label="close">×</a>', '</div>'); ?>
</div>
<div ng-controller="formController" ng-init="load('4')"></div>
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/charge/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Master</span> - Charge</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!/charge/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!"><i class="icon-home2 position-left"></i> Master</a></li>
            <li><a href="#!/charge/all"><i class="icon-cash4 position-left"></i> Charge</a></li>
            <li class="active"><?php
                if (isset($charge))
                    echo 'CRG' . str_pad($charge['charge_id'], 5, "0", STR_PAD_LEFT);
                else
                    echo "New";
                ?></li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Form validation -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Charge</h5>

        </div>


        <div class="panel-body">
            <p class="content-group-lg">A Charge is the place of business operations. Charge details entered here will be linked with Other Masters, Sales, Purchase, Accounts and Reports. </p>
            <?php echo validation_errors(); ?>
            <?php
            $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id'=>'merpForm');
            if (isset($charge))
                echo form_open('web/charge/save/' . $charge['charge_id'], $attributes);
            else
                echo form_open('web/charge/save', $attributes);
            ?>
            <fieldset class="content-group">
                <legend class="text-bold">Basic inputs</legend>
                <div class="form-group col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Charge Name <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="1" 
                                   onblur ="checkIfUnique(this, 'charge', 'charge_name', '<? if(isset($charge)) echo $charge['charge_id']; ?>', 'charge_id')"
                                   name="charge_name" type="text" required="required" value = "<?php if (isset($charge)) echo $charge['charge_name']; ?>" placeholder="Enter Charge Name">
                        </div>
                    </div>							
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Charge Type<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <label class="radio-inline">
                                <input type = "radio" name = "charge_type" value = "sales"
                                <?
                                if (isset($charge)) {
                                    if ($charge['charge_type'] == 'sales')
                                        echo 'checked ';
                                } else {
                                    echo "checked";
                                }
                                ?>>
                                Sales
                            </label>

                            <label class="radio-inline">
                                <input type = "radio" name = "charge_type" value = "purchase" 
                                <?
                                if (isset($charge)) {
                                    if ($charge['charge_type'] == 'purchase'){
                                        echo 'checked ';
                                    }
                                }
                                ?>>
                                Purchase
                            </label>
                        </div>		
                    </div>
                </div>
                <div class="form-group col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Description</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="3" 
                                   name="charge_description" type="text" value = "<?php if (isset($charge)) echo $charge['charge_description']; ?>" placeholder="Enter Description">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Default Charge </label>
                        <div class="col-lg-9">
                            <div class="checkbox checkbox-switch">
                                <label>
                                    <input type="checkbox" name="charge_is_default_type" data-on-text="Yes" data-off-text="No" class="switch" <?php
                                    if (isset($charge)) {
                                        if ($charge['charge_is_default_type'] == 1)
                                            echo "checked";
                                    }
                                    ?>>
                                    
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>

            <div class="text-right">
                <a href="#!/charge/all"><button class="btn btn-default">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                <? 
                if($this->session->userdata('access_controller')->is_access_granted('charge', 'save')) { ?>
                <button onclick="submitForm('charge')" id = "buttonSubmit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                <? } ?>
            </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->