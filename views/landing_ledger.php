

<!-- Page header -->
<div class="page-header">

    <div ng-controller="noGroupDataTableController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Accounting</span> - Ledger</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!SJ/ledger/<? echo $invoice_amount_account_coa_id; ?>/company/all" class="btn btn-link btn-float has-text"><i class="fa fa-smile-o" style = "font-size:22px;color:#26A69A !important"></i> <span>Customer Receivables</span></a>
                <a href="#!SJ/ledger/<? echo $purchase_amount_account_coa_id; ?>/company/all" class="btn btn-link btn-float has-text"><i class="fa fa-shopping-basket" style = "font-size:22px;color:#26A69A !important"></i> <span>Vendor Payables</span></a>
                <!--<a href="report/download/ledger" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>-->
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-calculator2 position-left"></i>Accounting</li>
            <li class="active"><i class="icon-book2 position-left"></i>Ledger</li>
        </ul>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Ledgers archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Ledger</h6>
            <div class="heading-elements">
                </ul>
            </div>
        </div>

        <table class="table noGroupDataTable">
            <thead>
                <tr>
                    <th>Sr</th>
                    <th>Account Name</th>
                    <th>Account Type</th>
                    <th>Debit</th>
                    <th>Credit</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $count = 0;
                    $debit_total = 0;
                    $credit_total = 0;
                    foreach ($ledger_entries as $ledger_entry): 
                    
                        if($ledger_entry['credit_balance'] > 0 || $ledger_entry['debit_balance'] > 0){
                        $count++;    
                        ?>
                <tr>
                    <td>
                        <?
                        echo $count;
                        ?>
                        
                    </td>
                    <td>
                        <a href="#!SJ/ledger/<?php echo $ledger_entry['sj_account_coa_id']; ?>"><span>
                        <?
                        echo $ledger_entry['coa_account_name'];
                        ?>
                        </span></a>
                        
                    </td>
                    <td>
                        <?
                        echo $ledger_entry['at_name'];
                        ?>
                        
                    </td>
                    <td>
                        <?
                        if($ledger_entry['debit_balance'] > $ledger_entry['credit_balance']){
                            $debit_total+=($ledger_entry['debit_balance'] - $ledger_entry['credit_balance']);
                            echo '₹ '. number_format(($ledger_entry['debit_balance'] - $ledger_entry['credit_balance']), 2);
                        }
                        ?>
                    </td>
                    <td>
                        <?
                        if($ledger_entry['debit_balance'] < $ledger_entry['credit_balance']){
                            $credit_total+=($ledger_entry['credit_balance'] - $ledger_entry['debit_balance']);
                            echo '₹ '. number_format(($ledger_entry['credit_balance'] - $ledger_entry['debit_balance']), 2);
                        }
                        ?>
                    </td>
                    </tr>
                <?
                    } 
                    endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th></th>
                    <th></th>
                    <th>Total</th>
                    <th><span style="color:<? if(round($debit_total, 2) == round($credit_total, 2)) echo '#00AA00'; else echo '#FF0000';?>"><? echo '₹ '. number_format($debit_total, 2); ?></span></th>
                    <th><span style="color:<? if(round($debit_total, 2) == round($credit_total, 2)) echo '#00AA00'; else echo '#FF0000';?>"><? echo '₹ '. number_format($credit_total, 2); ?></span></th>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->