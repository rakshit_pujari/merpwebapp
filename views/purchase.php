
<div ng-controller="purchaseController" ng-init="load()"></div>
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/purchase/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Purchase</span> - Purchase Invoice</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!/purchase/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!"><i class="icon-home2 position-left"></i>Purchase </a></li>
            <li><a href="#!/purchase/all"><i class="glyphicon glyphicon-list-alt  position-left"></i> Purchase Invoice</a></li>
            <li class="active"><?php
                if (isset($purchase)) {
                    if ($purchase['purchase_status'] == 'confirm') {
                        echo 'PUR' . str_pad($purchase['purchase_id'], 5, "0", STR_PAD_LEFT);
                    } else {
                        echo "DRAFT";
                    }
                } else {
                    echo "New";
                }
                ?></li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Form validation -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Purchase</h5>

        </div>


        <div class="panel-body">
            <?php
            $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id'=>'merpDocForm');
            if (isset($purchase))
                echo form_open_multipart('web/purchase/save/' . $purchase['purchase_id'], $attributes);
            else
                echo form_open_multipart('web/purchase/save', $attributes);
            ?>
            <p class="content-group-lg">A Purchase Invoice is a formal proof of purchase of Products or Services. Book all Purchases to ensure accurate Accounting & GST reporting.</p>

            <input name="purchase_status" type="text" readonly style = "display:none"
                   value = "<?php if (isset($purchase)) echo $purchase['purchase_status'];
            else echo "new"; ?>">

            <fieldset class="content-group">
                <legend class="text-bold">Vendor Details</legend>
                <div class="form-group col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Vendor Name <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control vendor_names" tabindex="1" name="purchase_linked_company_display_name" type="text" required onkeydown = "resetPurchaseLinkedCompanyId();"
                                   value = "<?php if (isset($purchase)) echo $purchase['purchase_linked_company_display_name']; ?>">
                        </div>
                    </div>		
                    <div class="form-group col-lg-6 has-feedback has-feedback-left">
                        <label class="control-label col-lg-3">Company ID</label>
                        <div class="col-lg-9">
                            <input class="form-control" style = "font-size:12px;padding-left:40px;" name="purchase_linked_company_id" type="text" readonly
                                   value = "<?php if (isset($purchase)) echo $purchase['purchase_linked_company_id']; ?>">
                            <div class="form-control-feedback" style = "left:10px">
                                <span style = "font-size:12px;padding-left:7px;"> COMP</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Invoice Name <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" id = "" tabindex="2" name="purchase_linked_company_invoice_name" type="text"
                                   value = "<?php if (isset($purchase)) echo $purchase['purchase_linked_company_invoice_name']; ?>"required>
                        </div>
                    </div>	
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Date</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name="purchase_date" id ="purchase_date_picker"
                                   value = "<?php if (isset($purchase)) echo explode(" ", $purchase['purchase_date'])[0]; ?>" required>
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Billing Address</label>
                        <div class="col-lg-9">
                            <textarea rows = "3" class="form-control" tabindex="2" name="purchase_linked_company_billing_address" ><?php if (isset($purchase)) echo $purchase['purchase_linked_company_billing_address']; ?></textarea>
                        </div>
                    </div>	

                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Billing State <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="select2 form-control" name="purchase_linked_company_billing_state_id" id = "purchase_linked_company_billing_state_id" onchange = "updateTaxesInPurchaseTable(null, true); refreshAndReIndexEntirePurchaseTable(true); calculatePurchaseNumbers();" required>
                                <option value = "">Choose a state</option>
                                <optgroup label="States">
                                    <?php
                                    foreach ($states as $state) {
                                        ?>
                                        <option  
                                        <?php
                                        if (isset($purchase)) {
                                            if ($purchase['purchase_linked_company_billing_state_id'] == $state ['state_tin_number'])
                                                echo
                                                "selected";
                                        }
                                        ?> 
                                            value="<? echo $state['state_tin_number']; ?>"><? echo $state['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </optgroup>
                                <optgroup label = "Union Territories">
                                    <?php
                                    foreach ($union_territories as $union_territory) {
                                        ?>
                                        <option 
                                        <?php
                                        if (isset($purchase)) {
                                            if ($purchase['purchase_linked_company_billing_state_id'] == $union_territory['state_tin_number'])
                                                echo "selected";
                                        }
                                        ?> 																	
                                            value="<? echo $union_territory['state_tin_number']; ?>"><? echo $union_territory['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>

                                </optgroup>
                            </select>
                        </div>
                    </div>	

                </div>
                <div class="form-group col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Contact Person<br/>( Billing )</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="" name="purchase_billing_contact_person_name" type="text"
                                   value = "<?php if (isset($purchase)) echo $purchase['purchase_billing_contact_person_name']; ?>">
                        </div>
                    </div>	
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Contact Number<br/>( Billing )</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="" name="purchase_billing_contact_number"
                                   value = "<?php if (isset($purchase)) echo $purchase['purchase_billing_contact_number']; ?>">
                        </div>
                    </div>
                </div>
                
                <div class="form-group col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">GST Number <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="" name="purchase_linked_company_gstin" type="text" style="text-transform:uppercase" required
                                   value = "<?php if (isset($purchase)) echo $purchase['purchase_linked_company_gstin']; ?>">
                        </div>
                    </div>	
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">PAN Number</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="" name="purchase_linked_company_pan_number"
                                   value = "<?php if (isset($purchase)) echo $purchase['purchase_linked_company_pan_number']; ?>">
                        </div>
                    </div>
                </div>
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Other Details</legend>	
                <div class="form-group col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">GST Supply State<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="select2 form-control" name="purchase_linked_company_gst_supply_state_id" id = "purchase_linked_company_gst_supply_state_id" onchange = "updateTaxesInPurchaseTable(); refreshAndReIndexEntirePurchaseTable(true); calculatePurchaseNumbers();" required>
                                <option value = "">Choose a state</option>
                                <optgroup label="States">
                                    <?php
                                    foreach ($states as $state) {
                                        ?>
                                        <option 
                                        <?php
                                        if (isset($purchase)) {
                                            if ($purchase['purchase_linked_company_gst_supply_state_id'] == $state['state_tin_number'])
                                                echo "selected";
                                        } else if (isset($oc_state_id)) {
                                            if ($oc_state_id == $state['state_tin_number'])
                                                echo "selected";
                                        }
                                        ?> value="<? echo $state['state_tin_number']; ?>"><? echo $state['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </optgroup>
                                <optgroup label = "Union Territories">
                                    <?php
                                    foreach ($union_territories as $union_territory) {
                                        ?>
                                        <option 
                                        <?php
                                        if (isset($purchase)) {
                                            if ($purchase['purchase_linked_company_gst_supply_state_id'] == $union_territory['state_tin_number'])
                                                echo "selected";
                                        }
                                        ?> value="<? echo $union_territory['state_tin_number']; ?>"><? echo $union_territory['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>

                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Tax Type</label>
                        <div class="col-lg-9">
                            <select class="form-control select" name="purchase_tax_type" id = "tax_type" onchange = "updateTaxesInPurchaseTable(); refreshAndReIndexEntirePurchaseTable(); calculatePurchaseNumbers();">
                                <option value = "exclusive" <? if (isset($purchase)) { 
                                    if ($purchase['purchase_tax_type'] == 'exclusive') 
                                        echo "selected"; 
                                    } else if($oc_purchase_default_tax_type == 'exclusive'){ 
                                        echo "selected";
                                    }  ?>>Exclusive
                                </option>

                                <option value = "inclusive" <? if (isset($purchase)) { 
                                    if ($purchase['purchase_tax_type'] == 'inclusive') 
                                        echo "selected"; 
                                    } else if($oc_purchase_default_tax_type == 'inclusive'){ 
                                        echo "selected";
                                    } ?>>Inclusive
                                </option>		
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Payment Terms</label>
                        <div class="col-lg-9">
                            <select class="form-control select" name="purchase_linked_company_payment_term_id" id = "purchase_linked_company_payment_term_id" tabindex = "10">
                                <?
                                foreach ($payment_terms as $payment_term) {
                                    ?>
                                    <option 
                                    <?php
                                    if (set_value('purchase_linked_company_payment_term_id') == $payment_term['payment_term_id'])
                                        echo "selected";

                                    else if (isset($purchase)) {
                                        if ($purchase['purchase_linked_company_payment_term_id'] == $payment_term['payment_term_id'])
                                            echo "selected";
                                    }
                                    ?> value="<? echo $payment_term['payment_term_id']; ?>"><? echo $payment_term['payment_term_display_text'] ?>
                                    </option>
                                    <?
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Input Tax Credit Available</label>
                        <div class="col-lg-9">
                            <select class="form-control select" name="purchase_is_input_tax_credit_available" tabindex = "10">
                                <option value ="1" <?php if (isset($purchase)) if($purchase['purchase_is_input_tax_credit_available'] == 1) echo "selected" ?>>Yes</option>
                                <option value ="0" <?php if (isset($purchase)) if($purchase['purchase_is_input_tax_credit_available'] == 0) echo "selected" ?>>No</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Bill Reference <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name="purchase_bill_reference" type="text"
                                   value = "<?php if (isset($purchase)) echo $purchase['purchase_bill_reference']; ?>" required>

                        </div>
                    </div>	
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Bill Date <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name="purchase_bill_date" id ="purchase_bill_date_picker"
                                   value = "<?php if (isset($purchase)) echo explode(" ", $purchase['purchase_bill_date'])[0]; ?>" required>
                        </div>
                    </div>
                </div>
                
                <div class="form-group col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="col-lg-3 control-label">Attach Bill</label>
                        <div class="col-lg-9">
                            <input type="file" name = "purchase_image_path" id = "purchase_image_path" 
                                   class="file-input-preview" data-show-remove="false"  data-dismiss="fileupload" data-show-upload="false" 
                                   data-image-path = "<?php if (isset($purchase)) echo $purchase['purchase_image_path'] ?>" 
                                   data-image-name = "<?php if (isset($purchase)) echo $purchase['purchase_bill_reference'] ?>">
                            <span class="help-block">Maximum file upload size is <code>100 KB.</code> </span>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                            <label class="control-label col-lg-3">Amount Paid In Advance</label>
                            <div class="multi-select-full  col-lg-9">
                                <select class="multiselect-select-all-filtering" multiple="multiple" name = "purchase_advance_payment_ids[]" id = "purchase_advance_payment_ids">
                                    <?
                                    if(isset($purchase))
                                    foreach($advance_payments as $advance_payment){
                                        ?>
                                        <option value="<? echo $advance_payment['advance_payment_id']; ?>" 
                                                <? if(isset($linked_advance_payment_ids)) if(in_array($advance_payment['advance_payment_id'], $linked_advance_payment_ids)) echo 'selected'; ?>>
                                                <? echo 'ADR' . str_pad($advance_payment['advance_payment_id'], 5, "0", STR_PAD_LEFT). ' ( Balance -  ₹ '.$advance_payment['balance_advance_payment_amount'].' )';?></option>
                                        <?
                                    }
                                    ?>
                                </select>
                        </div>
                    </div>
                    
                </div>

                <div class ="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Purchase Terms</label>
                        <div class="col-lg-9">
                            <textarea name = "purchase_terms" class="form-control" rows="5"><?php if (isset($purchase)) 
                                                     echo $purchase['purchase_terms']; 
                                                  else 
                                                      echo $oc_terms; ?>
                            </textarea>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Notes</label>
                        <div class="col-lg-9">
                            <textarea name = "purchase_notes" class="form-control" rows="5">
                                            <? if (isset($purchase)) echo $purchase['purchase_notes']; ?>
                            </textarea>
                        </div>
                    </div>
                </div>
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Product Details</legend>
                <table class="table purchaseDatatable">
                    <thead>
                        <tr>
                            <th>Sr No</th>
                            <th>Product</th>
                            <th>HSN/SAC code</th>
                            <th>Tax</th>
                            <th>Rate</th>
                            <th>Quantity</th>
                            <th>Discount ( % )</th>
                            <th>Amount</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($purchase_entries))
                            foreach ($purchase_entries as $purchase_entry) {
                                ?> 
                                <tr>
                                    <td><? echo $purchase_entry['pe_entry_id']; ?></td>

                                    <td>
                                        <select class="dtSelect2" onchange = "updatePurchaseProducts(this, true);calculatePurchaseNumbers();">
                                            <? foreach ($products as $product) { ?>
                                                <option value="<? echo $product['product_id']; ?>" <? if ($purchase_entry['pe_linked_product_id'] == $product['product_id']) echo "selected"; ?> ><? echo $product['product_name']; ?></option>

                                            <? } ?> 

                                        </select>
                                        <input name = "purchase_entries[<? echo $purchase_entry['pe_entry_id']; ?>][<? echo $purchase_entry['pe_linked_product_id']; ?>][0]" 
                                               value = "<? echo $purchase_entry['pe_product_name']; ?>" style = "display:none" ></input>
                                    </td>		

                                    <td><a href = "master#!/product/hsn/<? echo $purchase_entry['pe_product_hsn_code']; ?>"><? echo $purchase_entry['pe_product_hsn_code']; ?></a>
                                        <input name = "purchase_entries[<? echo $purchase_entry['pe_entry_id']; ?>][<? echo $purchase_entry['pe_linked_product_id']; ?>][2]" 
                                               value = "<? echo $purchase_entry['pe_product_hsn_code']; ?>" style = "display:none" ></input>
                                    </td>

                                    <td>
                                        <select name = "purchase_entries[<? echo $purchase_entry['pe_entry_id']; ?>][<? echo $purchase_entry['pe_linked_product_id']; ?>][1]"  class="form-control dtSelect"  onchange = "calculatePurchaseNumbers()">
                                            <?
                                            $taxes = $intrastate_taxes;
                                            if ($purchase['purchase_linked_company_billing_state_id'] != $purchase['purchase_linked_company_gst_supply_state_id']) {
                                                $taxes = $interstate_taxes;
                                            }

                                            foreach ($taxes as $tax) {
                                                ?>
                                                <option 
                                                <?php
                                                if ($purchase_entry['pe_tax_id_applicable'] == $tax['tax_id'])
                                                    echo "selected";
                                                ?> value="<? echo $tax['tax_id']; ?>" tax_value = "<? echo $tax['tax_percent']; ?>"><? echo $tax['tax_name'] ?>
                                                </option>

                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>

                                    <td>
                                        <input name = "purchase_entries[<? echo $purchase_entry['pe_entry_id']; ?>][<? echo $purchase_entry['pe_linked_product_id']; ?>][3]"
                                               class = "form-control" type = "number" step = 0.01 value = "<? echo $purchase_entry['pe_product_rate']; ?>" onchange = "calculatePurchaseNumbers()"></input>
                                    </td>

                                    <td>
                                        <input name = "purchase_entries[<? echo $purchase_entry['pe_entry_id']; ?>][<? echo $purchase_entry['pe_linked_product_id']; ?>][4]"
                                               id = "quantity" class = "form-control" type = "number" min = 0 step = 0.01 value = "<? echo $purchase_entry['pe_product_quantity']; ?>" onchange = "calculatePurchaseNumbers()"></input>
                                        <input name = "purchase_entries[<? echo $purchase_entry['pe_entry_id']; ?>][<? echo $purchase_entry['pe_linked_product_id']; ?>][5]" value = "<? echo $purchase_entry['pe_product_uqc_id']; ?>" style = "display:none" ></input>
                                        <input name = "purchase_entries[<? echo $purchase_entry['pe_entry_id']; ?>][<? echo $purchase_entry['pe_linked_product_id']; ?>][6]" value = "<? echo $purchase_entry['pe_product_uqc_text']; ?>" style = "display:none" ></input>
                                    </td>

                                    <td>
                                        <input name = "purchase_entries[<? echo $purchase_entry['pe_entry_id']; ?>][<? echo $purchase_entry['pe_linked_product_id']; ?>][7]"
                                               min = 0 max = 100 id = "discount" class = "form-control" type = "number" step = 0.01 value = "<? echo $purchase_entry['pe_discount']; ?>" onchange = "calculatePurchaseNumbers()"></input>
                                    </td>

                                    <td>
                                        0
                                    </td>

                                    <td>
                                        <?
                                        if ($purchase_entry['pe_entry_id'] != 1)
                                            echo '<a href="javascript: void(0)" onclick = "deletePurchaseRow(this);refreshAndReIndexEntirePurchaseTable();calculatePurchaseNumbers();"><i class="glyphicon glyphicon-trash"></i></a>';
                                        ?>
                                    </td>
                                </tr>

                                <?
                            }
                        ?>

                    </tbody>
                    <tfoot>
                        <tr>
                            <td>Sr No</td>
                            <td>Product</td>
                            <td>HSN/SAC code</td>
                            <td>Tax</td>
                            <td>Rate</td>
                            <td>Quantity</td>
                            <td>Discount ( % )</td>
                            <td>Amount</td>
                            <td class="text-center">Actions</td>
                        </tr>
                    </tfoot>
                </table>
                <div class="form-group col-lg-12">
                    <a onclick = "updatePurchaseProducts(null);calculatePurchaseNumbers();" class="btn btn-link btn-float has-text text-success"><span>+Add New Row</span></a>
                </div>	

                <div class="form-group col-lg-8">
                    <fieldset class="content-group">
                    <legend class="text-bold">Additional Charges</legend>
                        <table class="table" id = "purchaseChargesDatatable">
                        <thead>
                            <tr>
                                <th>Charge</th>
                                <th>Taxable Amount</th>
                                <th>Tax</th>
                                <th>Amount</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody> 
                            <?php
                        if (isset($purchase_charge_entries))
                            foreach ($purchase_charge_entries as $purchase_charge_entry) {
                                ?>
                                <tr>
                                    <td>
                                        <select class="dtSelect2" onchange = "updatePurchaseCharges(this);calculatePurchaseNumbers();">
                                            <? foreach ($charges as $charge) { ?>
                                                <option value="<? echo $charge['charge_id']; ?>" <? 
                                                    if ($purchase_charge_entry['pce_charge_id'] == $charge['charge_id']){
                                                        $charge_name = $charge['charge_name'];  //capture this in case charge name has changed
                                                        echo "selected"; 
                                                    } ?>>
                                                    <? echo $charge['charge_name']; ?></option>

                                            <? } ?>
                                        </select>
                                        <input name = "charge_entries[<? echo $purchase_charge_entry['pce_entry_id']; ?>][<? echo $purchase_charge_entry['pce_charge_id']; ?>][0]" 
                                               value = "<? 
                                               
                                               if(!empty($charge_name)) 
                                                        echo $charge_name; 
                                                    else 
                                                        echo $purchase_charge_entry['pce_charge_name']; ?>" style = "display:none" ></input>
                                    </td>		

                                    <td>
                                        <input name = "charge_entries[<? echo $purchase_charge_entry['pce_entry_id']; ?>][<? echo $purchase_charge_entry['pce_charge_id']; ?>][1]" value = "<? echo $purchase_charge_entry['pce_taxable_amount']; ?>" 
                                               class = "form-control" type = "number" min = "0.01" step ="0.01" style = "min-width:100px" onchange = "calculatePurchaseNumbers();"></input>
                                    </td>
                                    <td>
                                        <select name = "charge_entries[<? echo $purchase_charge_entry['pce_entry_id']; ?>][<? echo $purchase_charge_entry['pce_charge_id']; ?>][2]"  
                                                class="form-control dtSelect"  onchange = "calculatePurchaseNumbers()">
                                            <?
                                            $taxes = $intrastate_taxes;
                                            
                                            if ($oc_state_id != $purchase['purchase_linked_company_gst_supply_state_id']) {
                                                $taxes = $interstate_taxes;
                                            }

                                            foreach ($taxes as $tax) {
                                                ?>
                                                <option 
                                                <?php
                                                if ($purchase_charge_entry['pce_tax_id_applicable'] == $tax['tax_id'])
                                                    echo "selected";
                                                ?> value="<? echo $tax['tax_id']; ?>" tax_value = "<? echo $tax['tax_percent']; ?>"><? echo $tax['tax_name'] ?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>


                                    <td style = "white-space: nowrap;">
                                        0
                                    </td>

                                    <td>
                                        <a href="javascript: void(0)" onclick = "deletePurchaseChargeRow(this);refreshAndReIndexEntirePurchaseTable();calculatePurchaseNumbers();"><i class="glyphicon glyphicon-trash"></i></a>
                                    </td>
                                </tr>

                                <?
                            }
                        ?>
                        </tbody>
                        </table>
                        <a onclick = "updatePurchaseCharges(null);calculatePurchaseNumbers();" class="btn btn-link btn-float has-text text-success"><span>+Add New Charge</span></a>
                    </fieldset>
                </div>
                <div class="form-group col-lg-4">
                    <div class="form-group col-lg-12">
                        <label class="control-label col-lg-6" id = "subTotalLabel">Error !</label>
                        <div class="col-lg-6">
                            <h6 class = "no-margin text-bold"><span id = "totalPurchaseSubTotalAmount"></span><h6>
                                    </div>
                                    </div>	

                                    <div id = "taxDiv">

                                    </div>

                                    <div class="form-group col-lg-12 has-feedback has-feedback-left" style = "display:none" >
                                        <label class="control-label col-lg-6 ">Transport Charges</label>
                                        <div class="col-lg-6">
                                            <input class="form-control" name = "purchase_transport_charges" id = "transportationCharges" type="number" min = 0 step = 0.01 onchange = "calculatePurchaseNumbers()" 
                                                   value = "<?php
                                                   if (isset($purchase) && trim($purchase['purchase_transport_charges']) != "") {
                                                       echo $purchase['purchase_transport_charges'];
                                                   } else {
                                                       echo "0";
                                                   }
                                                   ?>">
                                            <div class="form-control-feedback" style = "left:10px">
                                                <span style = "font-size:16px;"> ₹</span>
                                            </div>
                                        </div>
                                    </div>	

                                    <div class="form-group col-lg-12  has-feedback has-feedback-left" style = "margin-top:15px;">
                                        <label class="control-label col-lg-6">Adjustments</label>
                                        <div class="col-lg-6">
                                            <input class="form-control" name = "purchase_adjustments" id = "adjustments" type="number" step = 0.01 onchange = "calculatePurchaseNumbers()" 
                                                   value = "<?php
                                                   if (isset($purchase) && trim($purchase['purchase_adjustments']) != "") {
                                                       echo $purchase['purchase_adjustments'];
                                                   } else {
                                                       echo "0";
                                                   }
                                                   ?>">
                                            <div class="form-control-feedback" style = "left:10px">
                                                <span style = "font-size:16px;"> ₹</span>
                                            </div>
                                        </div>
                                    </div>	

                                    <div class="form-group col-lg-12">
                                        <label class="control-label col-lg-6">Purchase Total</label>
                                        <div class="col-lg-6">
                                            <h6 class = "no-margin text-bold"><span id = "purchaseTotal"></span><h6>
                                                    <input type = "text" name = "purchase_amount" id = "purchaseAmount" style = "display:none">
                                                    </div>
                                                    </div>	
                                                    </div>	
                                                    </fieldset>

                                                    <div class="text-right">
                                                        <a href="#!/purchase/all"><button class="btn btn-default">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                                                        <? if ($this->session->userdata('access_controller')->is_access_granted('purchase', 'save')) { ?>
                                                        <? if (!isset($purchase) || $purchase['purchase_status'] == 'draft') { ?>
                                                                <button onclick="submitDocForm('purchase', 'draft')" id = "buttonSaveDraftPurchase" name="transaction" value="draft" class="btn btn-default">Save as draft <i class="icon-reload-alt position-right"></i></button>
                                                                <button onclick="submitDocForm('purchase', 'previewDraft')" name="transaction" value="previewDraft" class="btn btn-default">Preview<i class="icon-copy position-right"></i></button>
                                                            <? } ?>
                                                            <button onclick="confirmAndContinueForDocForm('purchase', 'confirm')" id = "buttonSaveConfirmPurchase" name="transaction" value="confirm" class="btn btn-primary">Confirm <i class="icon-arrow-right14 position-right"></i></button>
                                                        <? } ?>
                                                    </div>
                                                    </form>
                                                    </div>
                                                    </div>
                                                    <!-- /form validation -->

                                                    <!-- Footer -->
                                                    <div class="footer text-muted">
                                                        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
                                                    </div>
                                                    <!-- /footer -->