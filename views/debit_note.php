
<div ng-controller="debitNoteController" ng-init="load()"></div>
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/debitNote/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Purchase</span> - Debit Note</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!/debitNote/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!"><i class="icon-home2 position-left"></i>Purchase</a></li>
            <li><a href="#!/debitNote/all"><i class="icon-city position-left"></i>Debit Note</a></li>
            <li class="active"><?php
                if(isset($debit_note)){                    
                    if($debit_note['dn_status'] == 'confirm'){
                        echo 'DBN' . str_pad($debit_note['dn_id'], 5, "0", STR_PAD_LEFT);
                    } else {
                        echo "DRAFT";
                    }
                } else {
                    echo "New";
                }
                ?></li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Form validation -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Debit Note</h5>

        </div>


        <div class="panel-body">
            <?php
            $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id'=>'merpDocForm');
            if (isset($debit_note))
                echo form_open('web/DebitNote/save/' . $debit_note['dn_id'], $attributes);
            else
                echo form_open('web/DebitNote/save', $attributes);
            ?>
            <p class="content-group-lg">A Debit Note is a formal document of return of goods issued to Vendors. Book all returns to ensure accurate Accounting & GST reporting.</p>

            <input name="dn_status" type="text" readonly style = "display:none"
                                   value = "<?php if (isset($debit_note)) echo $debit_note['dn_status']; else echo "new"; ?>">
            
            <fieldset class="content-group">
                <legend class="text-bold">Company Details</legend>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Debit Against <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            
                            <label class="radio-inline">
                                <input type="radio" value = "purchase" name="debit_against" id = "debit_against_purchase" onchange = "clearDebitNoteForm();toggleElementsForDebitNote();refreshCustomerListForDebitNote(this);updateCustomerIdForDebitNoteForm();refreshInvoiceListForDebitNote();calculateDebitNoteNumbers();" 
                                <?php
                                if (isset($debit_note)) {
                                    if (isset($debit_note['dn_linked_purchase_id']))
                                        echo 'checked';
                                    if (isset($debit_note['dn_linked_invoice_id']) || isset($debit_note['dn_linked_purchase_id']))
                                        echo ' disabled';
                                } else
                                    echo "checked";
                                ?>>
                                Purchase
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="debit_against" id = "debit_against_sales" onchange = "clearDebitNoteForm();toggleElementsForDebitNote();refreshCustomerListForDebitNote(this);updateCustomerIdForDebitNoteForm();refreshInvoiceListForDebitNote();calculateDebitNoteNumbers()" value = "sales" <?php
                                if (isset($debit_note)) {
                                    if (isset($debit_note['dn_linked_invoice_id']))
                                        echo 'checked';
                                    if (isset($debit_note['dn_linked_invoice_id']) || isset($debit_note['dn_linked_purchase_id']))
                                        echo ' disabled';
                                } 
                                ?> disabled>
                                Sales
                            </label>

                            <?
                            if (isset($debit_note['dn_linked_invoice_id']) || isset($debit_note['dn_linked_purchase_id'])) {
                                if (isset($debit_note['dn_linked_invoice_id'])) {
                                    $debit_against = 'sales';
                                } else if (isset($debit_note['dn_linked_purchase_id'])) {
                                    $debit_against = 'purchase';
                                }

                                echo '<input name="debit_against" style = "display:none" type="text" readonly value = "' . $debit_against . '">';
                            };
                            ?>
                        </div>		
                    </div>	
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Company Name <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <?
                            if (isset($debit_note)) {
                                echo '<input class="form-control" type="text" readonly
														value = "' . $debit_note['dn_linked_company_invoice_name'] . '">';
                            } else {
                                echo '<select class="select2 form-control" id = "debit_note_linked_company_name" onchange = "clearDebitNoteForm();updateCustomerIdForDebitNoteForm();refreshInvoiceListForDebitNote();calculateDebitNoteNumbers();">
														<option value = "">Choose an company</option>
													</select>';
                            }
                            ?>

                        </div>
                    </div>

                </div>
                <div class = "col-lg-12">
                    <div class="form-group col-lg-6 has-feedback has-feedback-left">
                        <label class="control-label col-lg-3">Company ID</label>
                        <div class="col-lg-9">
                            <input class="form-control" style = "font-size:12px;padding-left:40px;" id = "debit_note_linked_company_id" name="dn_linked_company_id" type="text" readonly
                                   value = "<?php if (isset($debit_note)) echo $debit_note['dn_linked_company_id']; ?>">
                            <div class="form-control-feedback" style = "left:10px">
                                <span style = "font-size:12px;padding-left:7px;"> COMP</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Invoice Number <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <?
                            if (isset($debit_note)) {
                                if (isset($debit_note['dn_linked_invoice_id'])) {
                                    $dn_linked_supplementary_invoice_id = $debit_note['dn_linked_invoice_id'];
                                    $dn_linked_supplementary_invoice_id_text = 'INV' . str_pad($dn_linked_supplementary_invoice_id, 5, "0", STR_PAD_LEFT);
                                } else if (isset($debit_note['dn_linked_purchase_id'])) {
                                    $dn_linked_supplementary_invoice_id = $debit_note['dn_linked_purchase_id'];
                                    $dn_linked_supplementary_invoice_id_text = 'PUR' . str_pad($dn_linked_supplementary_invoice_id, 5, "0", STR_PAD_LEFT);
                                }

                                echo '<input class="form-control" type="text" readonly value = "' . $dn_linked_supplementary_invoice_id_text . '">
													<input name="dn_linked_supplementary_invoice_id" style = "display:none" type="text" readonly value = "' . $dn_linked_supplementary_invoice_id . '">';
                            } else {
                                echo '<select class="select2 form-control" name="dn_linked_supplementary_invoice_id" id = "debit_note_linked_invoice_id" onchange = "clearDebitNoteForm();loadInvoiceForDebitNoteForm();calculateDebitNoteNumbers();" required>
																
													</select>';
                            }
                            ?>
                        </div>
                    </div>


                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Invoice Name <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" id = "" tabindex="2" name="dn_linked_company_invoice_name" type="text"
                                   value = "<?php if (isset($debit_note)) echo $debit_note['dn_linked_company_invoice_name']; ?>" required readonly>
                        </div>
                    </div>	
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Date</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name="dn_date" id = "datePicker"
                                   value = "<?php if (isset($debit_note)) echo explode(" ", $debit_note['dn_date'])[0]; ?>">
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Billing Address</label>
                        <div class="col-lg-9">
                            <textarea rows = "3" class="form-control" tabindex="2" name="dn_linked_company_billing_address" readonly><?php if (isset($debit_note)) echo $debit_note['dn_linked_company_billing_address']; ?></textarea>
                        </div>
                    </div>	

                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Shipping Address</label>
                        <div class="col-lg-9">
                            <textarea rows = "3" class="form-control" tabindex="2" name="dn_linked_company_shipping_address" ><?php if (isset($debit_note)) echo $debit_note['dn_linked_company_shipping_address']; ?></textarea>
                        </div>
                    </div>	
                </div>
                <div class="col-lg-12">										
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Billing State</label>
                        <div class="col-lg-9">
                            <select class="select2 form-control" name="dn_linked_company_billing_state_id" id = "debit_note_linked_company_billing_state_id">
                                <option value = "">Choose a state</option>
                                <optgroup label="States">
                                    <?php
                                    foreach ($states as $state) {
                                        ?>
                                        <option  
                                        <?php
                                        if (isset($debit_note)) {
                                            if ($debit_note['dn_linked_company_billing_state_id'] == $state ['state_tin_number'])
                                                echo
                                                "selected";
                                        }
                                        ?> 
                                            value="<? echo $state['state_tin_number']; ?>"><? echo $state['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </optgroup>
                                <optgroup label = "Union Territories">
                                    <?php
                                    foreach ($union_territories as $union_territory) {
                                        ?>
                                        <option 
                                        <?php
                                        if (isset($debit_note)) {
                                            if ($debit_note['dn_linked_company_billing_state_id'] == $union_territory['state_tin_number'])
                                                echo "selected";
                                        }
                                        ?> 																	
                                            value="<? echo $union_territory['state_tin_number']; ?>"><? echo $union_territory['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>

                                </optgroup>
                            </select>
                        </div>
                    </div>	

                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Shipping State</label>
                        <div class="col-lg-9">
                            <select class="select2 form-control" name="dn_linked_company_shipping_state_id" id = "debit_note_linked_company_shipping_state_id">
                                <option value = "">Choose a state</option>
                                <optgroup label="States">
                                    <?php
                                    foreach ($states as $state) {
                                        ?>
                                        <option  
                                        <?php
                                        if (isset($debit_note)) {
                                            if ($debit_note['dn_linked_company_shipping_state_id'] == $state['state_tin_number'])
                                                echo "selected";
                                        }
                                        ?> 
                                            value="<? echo $state['state_tin_number']; ?>"><? echo $state['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </optgroup>
                                <optgroup label = "Union Territories">
                                    <?php
                                    foreach ($union_territories as $union_territory) {
                                        ?>
                                        <option 
                                        <?php
                                        if (isset($debit_note)) {
                                            if ($debit_note['dn_linked_company_shipping_state_id'] == $union_territory['state_tin_number'])
                                                echo "selected";
                                        }
                                        ?> 																	
                                            value="<? echo $union_territory['state_tin_number']; ?>"><? echo $union_territory['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>

                                </optgroup>
                            </select>
                        </div>
                    </div>												
                </div>
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Other Details</legend>	
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">GST Supply State<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="select2 form-control" name="dn_linked_company_gst_supply_state_id" id = "debit_note_linked_company_gst_supply_state_id" required
                                    onchange = "updateTaxesInDebitNoteTable(); calculateDebitNoteNumbers();">
                                <option value = "">Choose a state</option>
                                <optgroup label="States">
                                    <?php
                                    foreach ($states as $state) {
                                        ?>
                                        <option 
                                        <?php
                                        if (isset($debit_note)) {
                                            if ($debit_note['dn_linked_company_gst_supply_state_id'] == $state['state_tin_number'])
                                                echo "selected";
                                        }
                                        ?> value="<? echo $state['state_tin_number']; ?>"><? echo $state['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </optgroup>
                                <optgroup label = "Union Territories">
                                    <?php
                                    foreach ($union_territories as $union_territory) {
                                        ?>
                                        <option 
                                        <?php
                                        if (isset($debit_note)) {
                                            if ($debit_note['dn_linked_company_gst_supply_state_id'] == $union_territory['state_tin_number'])
                                                echo "selected";
                                        }
                                        ?> value="<? echo $union_territory['state_tin_number']; ?>"><? echo $union_territory['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>

                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Tax Type</label>
                        <div class="col-lg-9">
                            <select class="form-control select" name="dn_tax_type" id = "debit_note_tax_type" onchange = "updateTaxesInDebitNoteTable(); calculateDebitNoteNumbers();">
                                <option value = "exclusive" <?php if (isset($debit_note)) if ($debit_note['dn_tax_type'] == 'exclusive') echo "selected"; ?>>Exclusive</option>
                                <option value = "inclusive" <?php if (isset($debit_note)) if ($debit_note['dn_tax_type'] == 'inclusive') echo "selected"; ?>>Inclusive</option>													
                            </select>
                        </div>
                    </div>
                </div>
                <!--<div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Broker</label>
                        <div class="col-lg-9">
                            <select class="form-control select2" id = "debit_note_linked_broker_id" name="dn_linked_broker_id" tabindex="3" onchange = "updateBrokerCommissionForDebitNoteForm();">
                                <option value = "">Choose a broker</option>
                                <?php foreach ($brokers as $broker): ?> 
                                    <option data-commission = "<?php echo $broker['broker_commission'] ?>" value = "<? echo $broker['broker_id']; ?>" 
                                            <?php if (isset($debit_note)) if ($debit_note['dn_linked_broker_id'] == $broker['broker_id']) echo "selected"; ?>><? echo $broker['broker_name']; ?></option>
                                        <?php endforeach; ?>
                            </select>
                        </div>
                    </div>	
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Broker Commission</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name = "dn_linked_broker_commission" id="broker_commission" type="number" step = 0.01
                                   value = "<?php if (isset($debit_note)) echo $debit_note['dn_linked_broker_commission']; ?>">
                        </div>
                    </div>	
                </div>-->
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Transporter</label>
                        <div class="col-lg-9">
                            <select class="form-control select2" id = "tra nsporter_id" name="dn_linked_transporter_id">
                                <option value = "">Choose a transporter</option>
                                <?php foreach ($transporters as $transporter): ?>
                                    <option 
                                    <?php
                                    if (isset($debit_note)) {
                                        if ($debit_note['dn_linked_transporter_id'] == $transporter['transporter_id'])
                                            echo "selected";
                                    }
                                    ?> 
                                        value = "<? echo $transporter['transporter_id']; ?>"><? echo $transporter['transporter_name']; ?></option>
                                    <?php endforeach; ?>
                            </select>
                        </div>
                    </div>	
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">LR Number</label>
                        <div class="col-lg-9">
                            <input class="form-control" name = "dn_lr_number" type="text"
                                   value = "<?php if (isset($debit_note)) echo $debit_note['dn_lr_number']; ?>">
                        </div>
                    </div>	
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Payment Terms</label>
                            <div class="col-lg-9">
                                <select class="form-control select" name="dn_linked_company_payment_term_id" id = "debit_note_linked_company_payment_term_id" tabindex = "10">
                                    <?
                                    foreach ($payment_terms as $payment_term) {
                                        ?>
                                        <option 
                                        <?php
                                        if (set_value('debit_note_linked_company_payment_term_id') == $payment_term['payment_term_id'])
                                            echo "selected";
                                        else if (isset($debit_note)) {
                                            if ($debit_note['dn_linked_company_payment_term_id'] == $payment_term['payment_term_id'])
                                                echo "selected";
                                        }
                                        ?> value="<? echo $payment_term['payment_term_id']; ?>"><? echo $payment_term['payment_term_display_text'] ?>
                                        </option>
                                        <?
                                    }
                                    ?>
                                </select>
                            </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Order Reference</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name="dn_order_reference" type="text"
                                   value = "<?php if (isset($debit_note)) echo $debit_note['dn_order_reference']; ?>">
                        </div>
                    </div>	
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Notes</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name = "dn_notes" type="text"
                                   value = "<?php if (isset($debit_note)) echo $debit_note['dn_notes']; ?>">
                        </div>
                    </div>

                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Linked Employee</label>
                        <div class="col-lg-9">
                            <select class="form-control select2" name="dn_linked_employee_id" tabindex="3">
                                <option value = "">Choose a employee</option>
                                <?php foreach ($employees as $employee): ?>
                                    <option  
                                    <?php
                                    if (isset($debit_note)) {
                                        if ($debit_note['dn_linked_employee_id'] == $employee['employee_id'])
                                            echo "selected";
                                    }
                                    ?> value  = "<? echo $employee['employee_id']; ?>"><? echo $employee['employee_name']; ?></option>
                                    <?php endforeach; ?>
                            </select>
                        </div>
                    </div>	
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Return Reason <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="form-control select2" name="dn_sir_id" required>
                                <option value = "">Choose Reason For Return</option>
                                <?php foreach ($supplementary_invoice_reasons as $supplementary_invoice_reason): ?>
                                    <option 
                                    <?php
                                    if (isset($debit_note)) {
                                        if ($debit_note['dn_sir_id'] == $supplementary_invoice_reason['sir_id'])
                                            echo "selected";
                                    }
                                    ?> 
                                        value = "<? echo $supplementary_invoice_reason['sir_id']; ?>"><? echo $supplementary_invoice_reason['sir_text']; ?></option>
                                    <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Vendor Credit Note Date <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name="dn_vendor_credit_note_date" id = "vendorCnDatePicker"
                                   value = "<?php if (isset($debit_note)) echo explode(" ", $debit_note['dn_vendor_credit_note_date'])[0]; ?>" required>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Vendor Credit Note Number <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name="dn_vendor_credit_note_number"
                                   value = "<?php if (isset($debit_note)) echo $debit_note['dn_vendor_credit_note_number']; ?>">
                        </div>
                    </div>
                </div>
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Product Details</legend>
                <table class="table debitNoteDatatable">
                    <thead>
                        <tr>
                            <th>Sr No</th>
                            <th>Product</th>
                            <th>HSN/SAC code</th>
                            <th>Tax</th>
                            <th>Rate</th>
                            <th>Quantity</th>
                            <th>Discount ( % )</th>
                            <th>Amount</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $counter = 1;
                        if (isset($debit_note_entries))
                            foreach ($debit_note_entries as $debit_note_entry) {
                                ?>
                                <tr>
                                    <td><? echo $counter; 
                                    $counter++; ?></td>

                                    <td>
                                        <? echo $debit_note_entry['dne_product_name']; ?>
                                        <input style = "display:none" name = "debit_note_entries[<? echo $debit_note_entry['dne_entry_id']; ?>][<? echo $debit_note_entry['dne_linked_product_id']; ?>][0]" type = "text" value = "<? echo $debit_note_entry['dne_product_name']; ?>" readonly></input>
                                    </td>		

                                    <td>
                                        <a href = "master#!/product/hsn/<? echo $debit_note_entry['dne_product_hsn_code']; ?>"><? echo $debit_note_entry['dne_product_hsn_code']; ?></a>
                                        <input name = "debit_note_entries[<? echo $debit_note_entry['dne_entry_id']; ?>][<? echo $debit_note_entry['dne_linked_product_id']; ?>][1]" 
                                               value = "<? echo $debit_note_entry['dne_product_hsn_code']; ?>" style = "display:none" readonly></input>
                                    </td>

                                    <td>
                                        <input style = "display:none" name = "debit_note_entries[<? echo $debit_note_entry['dne_entry_id']; ?>][<? echo $debit_note_entry['dne_linked_product_id']; ?>][2]" tax_value = '<? echo $debit_note_entry['dne_tax_percent_applicable']; ?>' class = "form-control" type = "text" value = "<? echo $debit_note_entry['dne_tax_id_applicable']; ?>" readonly></input>
                                        <input class = "form-control" type = "text" value = "<? echo $debit_note_entry['dne_tax_name_applicable']; ?>" readonly></input>

                                    </td>

                                    <td>
                                        <input name = "debit_note_entries[<? echo $debit_note_entry['dne_entry_id']; ?>][<? echo $debit_note_entry['dne_linked_product_id']; ?>][3]"
                                               class = "form-control" type = "number" step = 0.01 value = "<? echo $debit_note_entry['dne_product_rate']; ?>" onchange = "calculateDebitNoteNumbers()" readonly></input>
                                    </td>

                                    <td>
                                        <input name = "debit_note_entries[<? echo $debit_note_entry['dne_entry_id']; ?>][<? echo $debit_note_entry['dne_linked_product_id']; ?>][4]"
                                               id = "quantity" class = "form-control" type = "number" step = 0.01 value = "<? echo $debit_note_entry['dne_product_quantity']; ?>" min = 0  onchange = "calculateDebitNoteNumbers()"></input>
                                        <input name = "debit_note_entries[<? echo $debit_note_entry['dne_entry_id']; ?>][<? echo $debit_note_entry['dne_linked_product_id']; ?>][5]" value = "<? echo $debit_note_entry['dne_product_uqc_id']; ?>" style = "display:none" ></input>
                                        <input name = "debit_note_entries[<? echo $debit_note_entry['dne_entry_id']; ?>][<? echo $debit_note_entry['dne_linked_product_id']; ?>][6]" value = "<? echo $debit_note_entry['dne_product_uqc_text']; ?>" style = "display:none" ></input>
                                    </td>

                                    <td>
                                        <input name = "debit_note_entries[<? echo $debit_note_entry['dne_entry_id']; ?>][<? echo $debit_note_entry['dne_linked_product_id']; ?>][7]"
                                               min = 0 max = 100 id = "discount" class = "form-control" type = "number" step = 0.01 value = "<? echo $debit_note_entry['dne_discount']; ?>" onchange = "calculateDebitNoteNumbers()" readonly></input>
                                    </td>

                                    <td>
                                        0
                                    </td>

                                    <td>
                                        <?
                                        if($debit_against == 'sales'){
                                            $entry_id = $debit_note_entry['dne_linked_ipe_id'];
                                        } else if($debit_against == 'purchase'){
                                            $entry_id = $debit_note_entry['dne_linked_pe_id'];
                                        } 
                                        
                                        if(!isset($entry_id) || $entry_id == NULL || $entry_id == ""){
                                            throw new Exception('Entry ID not defined');
                                        }
                                        echo '<input style = "display:none" name = "debit_note_entries[' . $debit_note_entry['dne_entry_id'] . '][' . $debit_note_entry['dne_linked_product_id'] . '][8]" type = "text"'
                                                . ' value = "' . $entry_id . '" readonly></input><a href="javascript: void(0)" onclick = "deleteDebitNoteRow(this);calculateDebitNoteNumbers();"><i class="glyphicon glyphicon-trash"></i></a>';
                                        ?>
                                    </td>
                                </tr>

                                <?php
                            }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>Sr No</td>
                            <td>Product</td>
                            <td>HSN/SAC code</td>
                            <td>Tax</td>
                            <td>Rate</td>
                            <td>Quantity</td>
                            <td>Discount ( % )</td>
                            <td>Amount</td>
                            <td class="text-center">Actions</td>
                        </tr>
                    </tfoot>
                </table>
                
                <br/><br/>
                <div class="form-group col-lg-8">
                    <fieldset class="content-group">
                    <legend class="text-bold">Additional Charges</legend>
                        <table class="table" id = "debitNoteChargesDatatable">
                        <thead>
                            <tr>
                                <th>Charge</th>
                                <th>Taxable Amount</th>
                                <th>Tax</th>
                                <th>Amount</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody> 
                        <?php
                        if (isset($debit_note_charge_entries))
                            foreach ($debit_note_charge_entries as $debit_note_charge_entry) {
                                ?>
                                <tr>
                                    <td>
                                        <select class="form-control select2" onchange = "updateDebitNoteCharges(this);calculateDebitNoteNumbers();">
                                            <? foreach ($charges as $charge) { ?>
                                                <option value="<? echo $charge['charge_id']; ?>" <? 
                                                    if ($debit_note_charge_entry['dnce_charge_id'] == $charge['charge_id']){
                                                        $charge_name = $charge['charge_name'];  //capture this in case charge name has changed
                                                        echo "selected"; 
                                                    } ?>>
                                                    <? echo $charge['charge_name']; ?></option>

                                            <? } ?>
                                        </select>
                                        <input name = "charge_entries[<? echo $debit_note_charge_entry['dnce_entry_id']; ?>][<? echo $debit_note_charge_entry['dnce_charge_id']; ?>][0]" 
                                               value = "<? 
                                               
                                                if(!empty($charge_name)) 
                                                    echo $charge_name; 
                                                else 
                                                    echo $debit_note_charge_entry['dnce_charge_name']; ?>" style = "display:none" ></input>
                                    </td>		

                                    <td>
                                        <input name = "charge_entries[<? echo $debit_note_charge_entry['dnce_entry_id']; ?>][<? echo $debit_note_charge_entry['dnce_charge_id']; ?>][1]" value = "<? echo $debit_note_charge_entry['dnce_taxable_amount']; ?>" 
                                               class = "form-control" type = "number" min = "0.01" step ="0.01" style = "min-width:100px" onchange = "calculateDebitNoteNumbers();"></input>
                                    </td>
                                    <td>
                                        <select name = "charge_entries[<? echo $debit_note_charge_entry['dnce_entry_id']; ?>][<? echo $debit_note_charge_entry['dnce_charge_id']; ?>][2]"  
                                                class="form-control dtSelect"  onchange = "calculateDebitNoteNumbers()">
                                            <?
                                            $taxes = $intrastate_taxes;
                                            
                                            if ($oc_state_id != $debit_note['debit_note_linked_company_gst_supply_state_id']) {
                                                $taxes = $interstate_taxes;
                                            }

                                            foreach ($taxes as $tax) {
                                                ?>
                                                <option 
                                                <?php
                                                if ($debit_note_charge_entry['dnce_tax_id_applicable'] == $tax['tax_id'])
                                                    echo "selected";
                                                ?> value="<? echo $tax['tax_id']; ?>" tax_value = "<? echo $tax['tax_percent']; ?>"><? echo $tax['tax_name'] ?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>


                                    <td style = "white-space: nowrap;">
                                        0
                                    </td>

                                    <td>
                                        <a href="javascript: void(0)" onclick = "deleteDebitNoteChargeRow(this);calculateDebitNoteNumbers();"><i class="glyphicon glyphicon-trash"></i></a>
                                    </td>
                                </tr>

                                <?
                            }
                        ?>
                        </tbody>
                        </table>
                        <a onclick = "updateDebitNoteCharges(null);calculateDebitNoteNumbers();" class="btn btn-link btn-float has-text text-success"><span>+Add New Charge</span></a>
                    </fieldset>
                </div>
                <div class="form-group col-lg-4">
                    <div class="form-group col-lg-12">
                        <label class="control-label col-lg-6" id = "subTotalLabel"></label>
                        <div class="col-lg-6">
                            <h6 class = "no-margin text-bold"><span id = "totalDebitNoteSubTotalAmount"></span><h6>
                                    </div>
                                    </div>	

                                    <div id = "taxDiv">

                                    </div>

                                    <div class="form-group col-lg-12 has-feedback has-feedback-left" style = "display:none">
                                        <label class="control-label col-lg-6 ">Transport Charges</label>
                                        <div class="col-lg-6">
                                            <input class="form-control" name = "dn_transport_charges" id = "transportationCharges" type="number" min = 0 step = 0.01 onkeyup = "calculateDebitNoteNumbers()" 
                                                   value = "<?php
                                                   if (isset($debit_note) && trim($debit_note['dn_transport_charges']) != "") {
                                                       echo $debit_note['dn_transport_charges'];
                                                   } else {
                                                       echo "0";
                                                   }
                                                   ?>">
                                            <div class="form-control-feedback" style = "left:10px">
                                                <span style = "font-size:16px;"> ₹</span>
                                            </div>
                                        </div>
                                    </div>	

                                    <div class="form-group col-lg-12  has-feedback has-feedback-left" style = "margin-top:15px;">
                                        <label class="control-label col-lg-6">Adjustments</label>
                                        <div class="col-lg-6">
                                            <input class="form-control" name = "dn_adjustments" id = "adjustments" type="number" step = 0.01 onkeyup = "calculateDebitNoteNumbers()" 
                                                   value = "<?php
                                                   if (isset($debit_note) && trim($debit_note['dn_adjustments']) != "") {
                                                       echo $debit_note['dn_adjustments'];
                                                   } else {
                                                       echo "0";
                                                   }
                                                   ?>">
                                            <div class="form-control-feedback" style = "left:10px">
                                                <span style = "font-size:16px;"> ₹</span>
                                            </div>
                                        </div>
                                    </div>	

                                    <div class="form-group col-lg-12">
                                        <label class="control-label col-lg-6">Debit Note Total</label>
                                        <div class="col-lg-6">
                                            <h6 class = "no-margin text-bold"><span id = "debitNoteTotal"></span><h6>
                                                    <input type = "text" name = "dn_amount" id = "debitNoteAmount" style = "display:none">
                                                    </div>
                                                    </div>	
                                                    </div>	
                                                    </fieldset>

                                                    <div class="text-right">
                                                        <a href="#!/DebitNote/all"><button class="btn btn-default">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                                                        <? if($this->session->userdata('access_controller')->is_access_granted('debit_note', 'save')) { ?>
                                                        <? if(!isset($debit_note) || $debit_note['dn_status'] == 'draft') {?>
                                                        <button onclick="submitDocForm('debit_note', 'draft')" id = "buttonSaveDraftDn" name="transaction" value="draft" class="btn btn-default">Save as draft <i class="icon-reload-alt position-right"></i></button>
                                                        <button onclick="submitDocForm('debit_note', 'previewDraft')" id = "buttonPreviewDraftDn" name="transaction" value="previewDraft" class="btn btn-default">Preview<i class="icon-copy position-right"></i></button>
                                                        <? } ?>
                                                        <button onclick="confirmAndContinueForDocForm('debit_note', 'confirm')" id = "buttonSaveConfirmDn" name="transaction" value="confirm" class="btn btn-primary">Confirm <i class="icon-arrow-right14 position-right"></i></button>
                                                        <? } ?>
                                                    </div>
                                                    </form>
                                                    </div>
                                                    </div>
                                                    <!-- /form validation -->

                                                    <!-- Footer -->
                                                    <div class="footer text-muted">
                                                        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
                                                    </div>
                                                    <!-- /footer -->