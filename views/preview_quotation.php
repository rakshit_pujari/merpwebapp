<?php

function getIndianCurrency($number) {
    $decimal = round($number - ($no = floor($number)), 2) * 100;
    $hundred = null;
    $digits_length = strlen($no);
    $i = 0;
    $str = array();
    $words = array(0 => '', 1 => 'one', 2 => 'two',
        3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
        7 => 'seven', 8 => 'eight', 9 => 'nine',
        10 => 'ten', 11 => 'eleven', 12 => 'twelve',
        13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
        16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
        19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
        40 => 'forty', 50 => 'fifty', 60 => 'sixty',
        70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
    $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
    while ($i < $digits_length) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number] . ' ' . $digits[$counter] . $plural . ' ' . $hundred : $words[floor($number / 10) * 10] . ' ' . $words[$number % 10] . ' ' . $digits[$counter] . $plural . ' ' . $hundred;
        } else
            $str[] = null;
    }
    $Rupees = implode('', array_reverse($str));
    //$paise = ($decimal) ? " and " . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    $paise = '';
    if ($decimal < 100) {
        $paise = ($decimal) ? " and " . ($words[((int) ($decimal / 10)) * 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    }
    return ($Rupees ? $Rupees . ' ' : '') . $paise;
}
?>

<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/quotation/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Sales</span> - Quotation</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                    <!--<a href="#!/quotation/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>-->
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-city position-left"></i>Sales</li>
            <li><a href="#!/quotation/all"><i class="icon-city position-left"></i>Quotation</a></li>
            <? if ($quotation['quotation_status'] == 'confirm') { ?>
                <li class="active"><?php if (isset($quotation)) echo 'QTN' . str_pad($quotation['quotation_id'], 5, "0", STR_PAD_LEFT); ?></li>
            <? } else { ?>
                <li class="active"><?php if (isset($quotation)) echo 'DRAFT'; ?></li>
            <? } ?>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <a href="#!/quotation/all"><button class="btn btn-default">Back<i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
    <a href="#!/quotation/view/<? echo $quotation['quotation_status'] . '/' . $quotation['quotation_id']; ?>"><button class="btn btn-default" 
                                                                                                             <? if (isset($linked_documents)) if (sizeof($linked_documents) > 0) echo "disabled title = 'Cannot be edited because this invoice is linked to other documents'" ?>>Edit <i class="icon-pencil3 position-right"></i></button></a>

    <button onClick="window.print();" class="btn btn-primary">Print <i class="icon-arrow-right14 position-right"></i></button>
    <!-- Form validation -->
    <div class="panel-flat">
        <div class="panel panel-body col-md-offset-3"  id = "section-to-print" style="width: 210mm;min-height: 297mm;padding-left:40px;padding-right:40px;">

            <span id = "quotationHeader">Quotation</span>
            <hr>

            <table class = "quotationTable">
                <tr>
                    <td class = "logo_container" colspan = "2" rowspan = "2">
                        <img class = "logo" src="<? echo $quotation['quotation_oc_logo_path']; ?>"></img>
                    </td>
                    <td style="vertical-align: top">
                        <span class = "quotationField">Quotation date</span>												
                        <br/>
                        <span class = "quotationFieldValue"><? echo explode(" ", $quotation['quotation_date'])[0]; ?></span>
                    </td>
                    <td style="vertical-align: top">
                        <span class = "quotationField">Revision Number</span>												
                        <br/>
                        <span class = "quotationFieldValue"><? echo empty($quotation['quotation_revision_number']) ? ' - ' : $quotation['quotation_revision_number']; ?></span>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top">
                        <span class = "quotationField">Quotation No.</span>												
                        <br/>
                        <? if ($quotation['quotation_status'] == 'confirm') { ?>
                            <span class = "quotationFieldValue">QTN<? echo str_pad($quotation['quotation_id'], 5, "0", STR_PAD_LEFT); ?></span>
                        <? } else { ?>
                            <span class = "quotationFieldValue">DRAFT</span>
                        <? } ?>

                    </td>
                    <td style="vertical-align: top">
                        <span class = "quotationField">Revision Date</span>												
                        <br/>
                        <span class = "quotationFieldValue"><? echo empty(explode(" ", $quotation['quotation_revision_date'])[0]) ? ' - ' : explode(" ", $quotation['quotation_revision_date'])[0]; ?></span>
                    </td>
                </tr>
                <tr>
                    <td colspan = "2" rowspan = "2">
                        <span class = "quotationFieldValue"><? echo $quotation['quotation_oc_name']; ?></span><br/>
                        <span class = "quotationField">
                            <? echo $quotation['quotation_oc_address']; ?><br/>
                            <? echo $quotation['quotation_oc_city_name']; ?><br/>
                            <? echo $quotation['quotation_oc_district'].', '.$quotation['quotation_oc_state']; ?><br/>
                            GSTIN: <? echo $quotation['quotation_oc_gstin']; ?><br/>
                            PAN: <? echo $quotation['quotation_oc_pan_number']; ?>

                        </span>
                    </td>
                    <td style="vertical-align: top">
                        <span class = "quotationField">Validity</span>												
                        <br/>
                        <span class = "quotationFieldValue"><? echo $quotation['quotation_linked_company_payment_term_name']; ?></span>
                    </td>
                    
                </tr>
            </table>
            <table class = "quotationTable gray-border">

                <tr>
                    <td>Customer Details</td>
                </tr>
                <tr>
                    <td>
                        Kind Attention :<? echo $quotation['quotation_linked_company_invoice_name']; ?><br/>
                        Name:<span class = "quotationFieldValue"><? echo $quotation['quotation_linked_company_display_name']; ?></span><br/>
                        Address:<? echo $quotation['quotation_linked_company_billing_address']; ?><br/>
                        State:<? echo $quotation['quotation_linked_company_billing_state_name']; ?><br/>
                        Contact details:<? echo $quotation['quotation_linked_company_contact_number']; ?><br/>
                    </td>
                </tr>
            </table>
            <hr>
            <table class = "productList quotationTable">
                <thead class = "grayTBack">
                    <tr>
                        <td width='25px'>Sr</td>
                        <td width='150px'>Item Description</td>
                        <td>Rate / Item</td>
                        <td>Qty</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($quotation_entries as $quotation_entry) {
                        ?>
                        <tr>
                            <td><? echo $quotation_entry['qe_entry_id']; ?></td>

                            <td><? echo $quotation_entry['qe_product_name']; ?></td>		


                            <td>₹ <? echo number_format($quotation_entry['qe_product_rate'], 2); ?></td>
                            <td><? echo number_format($quotation_entry['qe_product_quantity'], 2) . ' ' . $quotation_entry['qe_product_uqc_text']; ?></td>						  
                        </tr>

                        <?php
                    }
                    ?>
                </tbody>
                <tfoot class = "grayTBack">
                    <tr style="height:20px">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>

            <table class = "quotationTable">
                <tr>
                    <td colspan = "6" style = "text-align:left;">
                        <br/>Terms & Conditions<br>
                        <textarea class = "termsText" readonly>&#13;&#10;<? echo $quotation['quotation_terms']; ?></textarea>
                    <td colspan = "2">Authorized Signatory<br/><br/><br/><span style = "font-weight:800;"><? echo $quotation['quotation_oc_name']; ?></span></td>
                    <td></td>
                </tr>
            </table>
            <footer style = "position:absolute;bottom:0;right:0;padding-right:20px;padding-bottom:15px;font-size:11px;font-style:italic;">
                Powered By Quant ERP
            </footer>
        </div>

    </div>


    <style>
        @media print {
            body * {
                visibility: hidden;
            }
            #section-to-print, #section-to-print * {
                visibility: visible;
                overflow:hidden;
            }
            #section-to-print {
                position: absolute;
                left: 0;
                top: 0;
                size: auto;   /* auto is the initial value */
                margin: 0;
                border: initial;
                border-radius: initial;
                width: initial;
                min-height: initial;
                box-shadow: initial;
                background: initial;
                page-break-after: always;
            }
            .grayTBack {
                visibility: visible;
                background-color: #ccc!important;
                color: black;
                font-weight:700;
                -webkit-print-color-adjust: exact;
                border: solid #ccc!important;
            }
            
            .gray-border{
                border: 1px solid #ccc;
                overflow:visible!important;
                -webkit-print-color-adjust: exact;
            }
        }
        @page 
        {
            size:  auto;   /* auto is the initial value */
            margin: 0mm;  /* this affects the margin in the printer settings */
        }
        .termsText{
            font-size: 10px;
        }
        .productList{
            border: 1px solid #FFFFFF;
            text-align: center;
            font-size: 11px;
        }
        .termsText{
            font-size: 10px;
            min-height: 12em;
            border:none;
            resize: none;
            overflow:hidden;
            width:100px;
            width: 375px;
            text-align: justify;
        }
        .whiteTBack {
            background-color: #FFFFFF;
            color: black;
            font-weight:700;
        }
        .grayTBack {
            background-color: #ccc;
            color: black;
            font-weight:700;
        }
        .quotationTable {
            font-size: 11px;
            width: 100%;
            max-width: 100%;
            table-layout: fixed;
            font-family:"arial";
            margin-bottom:5px; 
            padding:10px;
        }
        .gray-border{
            border: 1px solid #ccc;
            border-collapse: collapse;
        }

        .logo {
            max-width:40mm; max-height: 20mm;
            text-align: center;
        }
        .logo_container {
            /*text-align: center;*/
        }
        #quotationHeader {
            font-size: 20px;
            font:"arial";
            width: 100%;
            text-align: center;
            font-family:"arial";
            font-weight: bold;
            display: block;
        }
        hr {
            border: 3px solid #000000;
            margin-top:3px;
        }
        .quotationField {
            font-size: 11px;	
        }
        .quotationFieldValue {
            font-weight: bold;
            font-size: 12px;
        }
        td {
            padding:3px;
        }
        hr {
            margin-bottom: 5px;
        }
    </style>