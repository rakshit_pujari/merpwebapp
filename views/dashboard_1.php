

<!-- Content area -->
<div class="content">
    <div ng-controller="dashboardController" ng-init="load()"></div>
    <? if ($this->session->userdata('access_controller')->is_access_granted('profit_and_loss', 'view')) { ?>
        <div class="row">
            <div class="col-lg-12">
                <!-- Tornado with negative stack -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">Performance Overview</h5>
                    </div>

                    <div class="panel-body">
                        <div class="chart-container">
                            <div class="chart has-fixed-height" id="tornado_bars_negative"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <? } ?>

    <? if ($this->session->userdata('access_controller')->is_access_granted('profit_and_loss', 'view')) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-sm-6 col-md-3">
                        <div class="panel panel-body panel-body-accent">
                            <div class="media no-margin">
                                <div class="media-left media-middle">
                                    <i class="icon-plus3 icon-3x text-success-400"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="no-margin text-semibold"><? echo '₹ ' . number_format($fy_revenue_expense_figures['revenue'], 2) ?></h3>
                                    <span class="text-uppercase text-size-mini text-muted">current fy net revenue</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <div class="panel panel-body">
                            <div class="media no-margin">
                                <div class="media-left media-middle">
                                    <i class="icon-minus3 icon-3x text-danger-400"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="no-margin text-semibold"><? echo '₹ ' . number_format($fy_revenue_expense_figures['expense'], 2) ?></h3>
                                    <span class="text-uppercase text-size-mini text-muted">current fy net expense</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <div class="panel panel-body">
                            <div class="media no-margin">
                                <div class="media-body">
                                    <h3 class="no-margin text-semibold"><? echo '₹ ' . number_format($fy_revenue_expense_figures['profit'], 2) ?></h3>
                                    <span class="text-uppercase text-size-mini text-muted">current fy net profit</span>
                                </div>

                                <div class="media-right media-middle">
                                    <i class="fa fa-inr fa-4x text-blue-400"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                    <a href="#!SJ/pnl">
                        <div class="col-sm-6 col-md-3">
                            <div class="panel panel-body">
                                <div class="media no-margin">
                                    <div class="media-body">
                                        <h3 class="no-margin text-semibold"></h3>
                                        <span class="text-uppercase text-size-mini text-muted">view profit & loss statement</span>
                                    </div>

                                    <div class="media-right media-middle">
                                        <i class="icon-table icon-3x text-indigo-400"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    <? } ?>

    <?
    if ($this->session->userdata('access_controller')->is_access_granted('invoice', 'view') ||
            $this->session->userdata('access_controller')->is_access_granted('credit_note', 'view')) {
        ?>
        <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">Sales History</h5>
                    </div>
                    <div class="panel-body">
                        <div class="chart-container">
                            <div class="chart has-fixed-height" id="line_bar_sales"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <? } ?>
    <?
    if ($this->session->userdata('access_controller')->is_access_granted('invoice', 'view') ||
            $this->session->userdata('access_controller')->is_access_granted('credit_note', 'view')) {
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-sm-6 col-md-3">
                        <div class="panel panel-body panel-body-accent">
                            <div class="media no-margin">
                                <div class="media-left media-middle">
                                    <i class="fa fa-inr fa-4x text-success-400"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="no-margin text-semibold"><? echo '₹ ' . number_format($fy_doc_figures['sales'], 2) ?></h3>
                                    <span class="text-uppercase text-size-mini text-muted">total sales</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <div class="panel panel-body">
                            <div class="media no-margin">
                                <div class="media-left media-middle">
                                    <i class="icon-undo2 icon-3x text-indigo-400"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="no-margin text-semibold"><? echo '₹ ' . number_format($fy_doc_figures['cn'], 2) ?></h3>
                                    <span class="text-uppercase text-size-mini text-muted">credit notes</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <div class="panel panel-body">
                            <div class="media no-margin">
                                <div class="media-body">
                                    <h3 class="no-margin text-semibold"><? echo '₹ ' . number_format($fy_doc_figures['net_sales'], 2) ?></h3>
                                    <span class="text-uppercase text-size-mini text-muted">net sales</span>
                                </div>

                                <div class="media-right media-middle">
                                    <i class="icon-cash4 icon-3x text-blue-400"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="#!invoice/view/new">
                        <div class="col-sm-6 col-md-3">
                            <div class="panel panel-body">
                                <div class="media no-margin">
                                    <div class="media-body">
                                        <h3 class="no-margin text-semibold">CREATE</h3>
                                        <span class="text-uppercase text-size-mini text-muted">new invoice</span>
                                    </div>

                                    <div class="media-right media-middle">
                                        <i class="glyphicon glyphicon-list-alt fa-3x text-grey-400"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    <? } ?>

    <?
    if ($this->session->userdata('access_controller')->is_access_granted('purchase', 'view') ||
            $this->session->userdata('access_controller')->is_access_granted('debit_note', 'view')) {
        ?>
        <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">Purchase History</h5>
                    </div>
                    <div class="panel-body">
                        <div class="chart-container">
                            <div class="chart has-fixed-height" id="line_bar_purchase"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <? } ?>

    <?
    if ($this->session->userdata('access_controller')->is_access_granted('purchase', 'view') ||
            $this->session->userdata('access_controller')->is_access_granted('debit_note', 'view')) {
        ?>`
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-sm-6 col-md-3">
                        <div class="panel panel-body panel-body-accent">
                            <div class="media no-margin">
                                <div class="media-left media-middle">
                                    <i class="icon-cart-add2 icon-3x text-success-400"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="no-margin text-semibold"><? echo '₹ ' . number_format($fy_doc_figures['purchase'], 2) ?></h3>
                                    <span class="text-uppercase text-size-mini text-muted">total purchase</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <div class="panel panel-body">
                            <div class="media no-margin">
                                <div class="media-left media-middle">
                                    <i class="icon-cart-remove icon-3x text-indigo-400"></i>
                                </div>

                                <div class="media-body text-right">
                                    <h3 class="no-margin text-semibold"><? echo '₹ ' . number_format($fy_doc_figures['dn'], 2) ?></h3>
                                    <span class="text-uppercase text-size-mini text-muted">debit notes</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <div class="panel panel-body">
                            <div class="media no-margin">
                                <div class="media-body">
                                    <h3 class="no-margin text-semibold"><? echo '₹ ' . number_format($fy_doc_figures['net_purchase'], 2) ?></h3>
                                    <span class="text-uppercase text-size-mini text-muted">net purchase</span>
                                </div>

                                <div class="media-right media-middle">
                                    <i class="icon-cart2 icon-3x text-blue-400"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                    <a href="#!purchase/view/new">
                        <div class="col-sm-6 col-md-3">
                            <div class="panel panel-body">
                                <div class="media no-margin">
                                    <div class="media-body">
                                        <h3 class="no-margin text-semibold">CREATE</h3>
                                        <span class="text-uppercase text-size-mini text-muted">new purchase invoice</span>
                                    </div>

                                    <div class="media-right media-middle">
                                        <i class="glyphicon glyphicon-list-alt fa-3x text-danger-400"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    <? } ?>
    <div class = "row">
        <div class="col-sm-6 col-lg-3">
            <div class="panel has-scroll">
                <div class="datepicker no-border"></div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3">
            <!--<div class="panel panel-body panel-body-accent">
                <div class="media no-margin">
                    <div class="media-left media-middle">
                        <i class="fa fa-lightbulb-o fa-4x text-success-400"></i>
                    </div>

                    <div class="media-body text-right">
                        <h3 class="no-margin text-semibold">Quant</h3>
                        <span class="text-uppercase text-size-mini text-muted">Designed & Developed by 1Qubit</span>
                    </div>
                </div>
            </div>-->
            <!-- Simple stats with thumbnail -->
            <div class="panel">
                <a href="https://quanterp.com" target="blank_">
                    <img src="/assets/images/quant_logo.png" class="img-responsive" alt="">
                </a>

                <div class="panel-body">
                    <div class="row text-center">
                        <div class="col-xs-12">
                            <span class="text-muted text-size-small">By</span>
                            <h5 class="text-semibold no-margin">1Qubit Technologies</h5>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->