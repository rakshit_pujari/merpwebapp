
<div ng-controller="purchaseOrderController" ng-init="load()"></div>
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/PurchaseOrder/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Sales</span> - Purchase Order</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!/PurchaseOrder/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!"><i class="icon-home2 position-left"></i>Purchase Order </a></li>
            <li><a href="#!/PurchaseOrder/all"><i class="glyphicon glyphicon-list-alt  position-left"></i> Purchase Order</a></li>
            <li class="active"><?php
                if (isset($purchase_order)) {
                    if ($purchase_order['purchase_order_status'] == 'confirm') {
                        echo 'POR' . str_pad($purchase_order['purchase_order_id'], 5, "0", STR_PAD_LEFT);
                    } else {
                        echo "DRAFT";
                    }
                } else {
                    echo "New";
                }
                ?></li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Form validation -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Purchase Order</h5>

        </div>


        <div class="panel-body">
            <?php
            $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id' => 'merpDocForm');
            if (isset($purchase_order))
                echo form_open_multipart('web/PurchaseOrder/save/' . $purchase_order['purchase_order_id'], $attributes);
            else
                echo form_open_multipart('web/PurchaseOrder/save', $attributes);
            ?>
            <p class="content-group-lg">A Purchase Order is a confirmation document offered to the Vendor for supply of Goods. It is a good practice to preview the order before confirming it.</p>

            <input name="purchase_order_status" type="text" readonly style = "display:none"
                   value = "<?php if (isset($purchase_order))
                echo $purchase_order['purchase_order_status'];
            else
                echo "new";
            ?>">

            <fieldset class="content-group">
                <legend class="text-bold">Vendor Details</legend>
                <div class="form-group col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Vendor Name <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control vendor_names" tabindex="1" name="purchase_order_linked_company_display_name" type="text" required onkeydown = "resetPurchaseOrderLinkedCompanyId();"
                                   value = "<?php if (isset($purchase_order)) echo $purchase_order['purchase_order_linked_company_display_name']; ?>">
                        </div>
                    </div>		
                    <div class="form-group col-lg-6 has-feedback has-feedback-left">
                        <label class="control-label col-lg-3">Company ID</label>
                        <div class="col-lg-9">
                            <input class="form-control" style = "font-size:12px;padding-left:40px;" name="purchase_order_linked_company_id" type="text" readonly
                                   value = "<?php if (isset($purchase_order)) echo $purchase_order['purchase_order_linked_company_id']; ?>">
                            <div class="form-control-feedback" style = "left:10px">
                                <span style = "font-size:12px;padding-left:7px;"> COMP</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Kind Attention <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" id = "" tabindex="2" name="purchase_order_linked_company_invoice_name" type="text"
                                   value = "<?php if (isset($purchase_order)) echo $purchase_order['purchase_order_linked_company_invoice_name']; ?>"required>
                        </div>
                    </div>	
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Date</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name="purchase_order_date" id ="purchase_order_date_picker"
                                   value = "<?php if (isset($purchase_order)) echo explode(" ", $purchase_order['purchase_order_date'])[0]; ?>" required>
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Billing Address</label>
                        <div class="col-lg-9">
                            <textarea rows = "3" class="form-control" tabindex="2" name="purchase_order_linked_company_billing_address" ><?php if (isset($purchase_order)) echo $purchase_order['purchase_order_linked_company_billing_address']; ?></textarea>
                        </div>
                    </div>	

                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Billing State <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="select2 form-control" name="purchase_order_linked_company_billing_state_id" id = "purchase_order_linked_company_billing_state_id" required>
                                <option value = "">Choose a state</option>
                                <optgroup label="States">
                                    <?php
                                    foreach ($states as $state) {
                                        ?>
                                        <option  
                                        <?php
                                        if (isset($purchase_order)) {
                                            if ($purchase_order['purchase_order_linked_company_billing_state_id'] == $state ['state_tin_number'])
                                                echo
                                                "selected";
                                        }
                                        ?> 
                                            value="<? echo $state['state_tin_number']; ?>"><? echo $state['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </optgroup>
                                <optgroup label = "Union Territories">
                                    <?php
                                    foreach ($union_territories as $union_territory) {
                                        ?>
                                        <option 
                                        <?php
                                        if (isset($purchase_order)) {
                                            if ($purchase_order['purchase_order_linked_company_billing_state_id'] == $union_territory['state_tin_number'])
                                                echo "selected";
                                        }
                                        ?> 																	
                                            value="<? echo $union_territory['state_tin_number']; ?>"><? echo $union_territory['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>

                                </optgroup>
                            </select>
                        </div>
                    </div>	

                </div>
                <div class="form-group col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Revision Number</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name="purchase_order_revision_number" type="text"
                                   value = "<?php if (isset($purchase_order)) echo $purchase_order['purchase_order_revision_number']; ?>">

                        </div>
                    </div>	
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Revision Date</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name="purchase_order_revision_date" id ="purchase_order_revision_date"
                                   value = "<?php if (isset($purchase_order)) echo explode(" ", $purchase_order['purchase_order_revision_date'])[0]; ?>">
                        </div>
                    </div>
                </div>
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Other Details</legend>	
                <div class="form-group col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Purchase Terms & Conditions</label>
                        <div class="col-lg-9">
                            <textarea rows = "3" class="form-control" tabindex="2" name="purchase_order_terms" ><?php if (isset($purchase_order)) echo $purchase_order['purchase_order_terms']; else echo $oc_purchase_terms; ?></textarea>
                        </div>
                    </div>	
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Validity</label>
                        <div class="col-lg-9">
                            <select class="form-control select" name="purchase_order_linked_company_payment_term_id" id = "purchase_order_linked_company_payment_term_id" tabindex = "10">
                                <?
                                foreach ($payment_terms as $payment_term) {
                                    ?>
                                    <option 
                                    <?php
                                    if (set_value('purchase_order_linked_company_payment_term_id') == $payment_term['payment_term_id'])
                                        echo "selected";

                                    else if (isset($purchase_order)) {
                                        if ($purchase_order['purchase_order_linked_company_payment_term_id'] == $payment_term['payment_term_id'])
                                            echo "selected";
                                    }
                                    ?> value="<? echo $payment_term['payment_term_id']; ?>"><? echo $payment_term['payment_term_display_text'] ?>
                                    </option>
                                    <?
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                </div>

                <div class="form-group col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Notes</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name = "purchase_order_notes" type="text"
                                   value = "<?php if (isset($purchase_order)) echo $purchase_order['purchase_order_notes']; ?>">

                        </div>
                    </div>

                </div>
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Product Details</legend>
                <table class="table purchaseOrderDatatable">
                    <thead>
                        <tr>
                            <th>Sr No</th>
                            <th>Product</th>
                            <th>Rate</th>
                            <th>Quantity</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($purchase_order_entries)){
                            foreach ($purchase_order_entries as $purchase_order_entry) {
                                ?> 
                                <tr>
                                    <td><? echo $purchase_order_entry['po_entry_id']; ?></td>

                                    <td>
                                        <select class="dtSelect2" onchange = "updatePurchaseOrderProducts(this, true);">
                                            <? foreach ($products as $product) { ?>
                                                <option value="<? echo $product['product_id']; ?>" <? if ($purchase_order_entry['po_linked_product_id'] == $product['product_id']) echo "selected"; ?> ><? echo $product['product_name']; ?></option>
                                            <? } ?> 

                                        </select>
                                        <input name = "purchase_order_entries[<? echo $purchase_order_entry['po_entry_id']; ?>][<? echo $purchase_order_entry['po_linked_product_id']; ?>][0]" 
                                               value = "<? echo $purchase_order_entry['po_product_name']; ?>" style = "display:none" ></input>
                                    </td>		

                                    <td>
                                        <input name = "purchase_order_entries[<? echo $purchase_order_entry['po_entry_id']; ?>][<? echo $purchase_order_entry['po_linked_product_id']; ?>][1]"
                                               class = "form-control" type = "number" step = 0.01 min = 0.01 value = "<? echo $purchase_order_entry['po_product_rate']; ?>"></input>
                                    </td>

                                    <td>
                                        <input name = "purchase_order_entries[<? echo $purchase_order_entry['po_entry_id']; ?>][<? echo $purchase_order_entry['po_linked_product_id']; ?>][2]"
                                               id = "quantity" class = "form-control" type = "number" min = 0.01 step = 0.01 value = "<? echo $purchase_order_entry['po_product_quantity']; ?>"></input>
                                        <input name = "purchase_order_entries[<? echo $purchase_order_entry['po_entry_id']; ?>][<? echo $purchase_order_entry['po_linked_product_id']; ?>][3]" value = "<? echo $purchase_order_entry['po_product_uqc_id']; ?>" style = "display:none" ></input>
                                        <input name = "purchase_order_entries[<? echo $purchase_order_entry['po_entry_id']; ?>][<? echo $purchase_order_entry['po_linked_product_id']; ?>][4]" value = "<? echo $purchase_order_entry['po_product_uqc_text']; ?>" style = "display:none" ></input>
                                    </td>

                                    <td>
                                        <?
                                        if ($purchase_order_entry['po_entry_id'] != 1)
                                            echo '<a href="javascript: void(0)" onclick = "deletePurchaseOrderRow(this);refreshAndReIndexEntirePurchaseOrderTable();"><i class="glyphicon glyphicon-trash"></i></a>';
                                        ?>
                                    </td>
                                </tr>

                                    <?
                                }
                        }
                            ?>

                    </tbody>
                    <tfoot>
                        <tr>
                            <td>Sr No</td>
                            <td>Product</td>
                            <td>Rate</td>
                            <td>Quantity</td>
                            <td class="text-center">Actions</td>
                        </tr>
                    </tfoot>
                </table>
                <div class="form-group col-lg-12">
                    <a onclick = "updatePurchaseOrderProducts(null);calculatePurchaseOrderNumbers();" class="btn btn-link btn-float has-text text-success"><span>+Add New Row</span></a>
                </div>	


                <div class="form-group col-lg-6">
                </div>		
            </fieldset>

            <div class="text-right">
                <a href="#!/PurchaseOrder/all"><button class="btn btn-default">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                <? if ($this->session->userdata('access_controller')->is_access_granted('purchase_order', 'save')) { ?>
                    <? if (!isset($purchase_order) || $purchase_order['purchase_order_status'] == 'draft') { ?>
                        <button onclick="submitDocForm('purchase_order', 'draft')" id = "buttonSaveDraftPurchaseOrder" name="transaction" value="draft" class="btn btn-default">Save as draft <i class="icon-reload-alt position-right"></i></button>
                        <button onclick="submitDocForm('purchase_order', 'previewDraft')" name="transaction" value="previewDraft" class="btn btn-default">Preview<i class="icon-copy position-right"></i></button>
    <? } ?>
                    <button onclick="confirmAndContinueForDocForm('purchase_order', 'confirm')" id = "buttonSaveConfirmPurchaseOrder" name="transaction" value="confirm" class="btn btn-primary">Confirm <i class="icon-arrow-right14 position-right"></i></button>
<? } ?>
            </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->