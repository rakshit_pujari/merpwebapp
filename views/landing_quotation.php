

<!-- Page header -->
<div class="page-header">

    <div ng-controller="dataTableController" ng-init="load(2)"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Sales</span> - Quotation</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!quotation/view/new" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-list-alt  position-left" style = "font-size:22px;color:#26A69A !important"></i> <span>Add Quotation</span></a>
                <a href="report/download/quotation" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-home2 position-left"></i>Sales</li>
            <li class="active"><i class="icon-city position-left"></i>Quotation</li>
        </ul>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Quotation archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Quotation</h6>
            <div class="heading-elements">
                </ul>
            </div>
        </div>

        <table class="table masterDataTable">
            <thead>
                <tr>
                    <th>Quotation ID</th>
                    <th>Customer Name</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($quotations as $quotation): ?>
                    <tr>
                        <td>
                            <? if ($quotation['quotation_status'] == 'confirm') { ?>
                                QTN<?php echo str_pad($quotation['quotation_id'], 5, "0", STR_PAD_LEFT); ?>
                            <? } else { ?>
                                DRAFT
                            <? } ?>
                        </td>
                        <td>
                            <h6 class="no-margin">
                                <? if ($quotation['quotation_status'] == 'confirm') { ?>
                                    <a href="#!quotation/preview/confirm/<? echo $quotation['quotation_id']; ?>">
                                    <? } else { ?>
                                        <a href="#!quotation/view/<? echo $quotation['quotation_status']; ?>/<? echo $quotation['quotation_id']; ?>">
                                        <? } ?><span><?php echo $quotation['quotation_linked_company_display_name']; ?></span></a>
                                    <small class="display-block text-muted">Created by <?php echo $quotation['employee_name']; ?> 
                                        on <?php echo $quotation['quotation_record_creation_time']; ?></small>
                            </h6>
                        </td>
                        <td><?php echo explode(" ", $quotation['quotation_date'])[0]; ?></td>
                        <td><?php
                            if ($quotation['quotation_status'] == 'confirm')
                                echo '<span class="label bg-blue"><span>' . $quotation['quotation_status'] . '</span></span>';
                            else
                                echo '<span class="label bg-danger"><span>' . $quotation['quotation_status'] . '</span></span>';
                            ?></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li><? if ($quotation['quotation_status'] == 'confirm') { ?>
                                        <a href="#!quotation/preview/confirm/<? echo $quotation['quotation_id']; ?>">
                                        <? } else { ?>
                                            <a href="#!quotation/view/<? echo $quotation['quotation_status']; ?>/<? echo $quotation['quotation_id']; ?>">
                                            <? } ?>
                                            <i class="icon-file-eye"></i></a></li>
                                            <? if ($this->session->userdata('access_controller')->is_access_granted('quotation', 'delete')
                                                && $quotation['quotation_status'] == 'draft') { 
                                            ?>
                                            <li><a href='javascript: void(0)' onclick = 'showDeleteDialog("quotation", <?php echo $quotation['quotation_id']; ?>)'><i class="glyphicon glyphicon-trash"></i></a></li>
                                        <? } ?>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td>Quotation ID</td>
                    <td>Date</td>
                    <td>Customer Name</td>
                    <th>Status</th>
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->