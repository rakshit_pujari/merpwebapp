<?php

function getIndianCurrency($number) {
    $decimal = round($number - ($no = floor($number)), 2) * 100;
    $hundred = null;
    $digits_length = strlen($no);
    $i = 0;
    $str = array();
    $words = array(0 => '', 1 => 'one', 2 => 'two',
        3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
        7 => 'seven', 8 => 'eight', 9 => 'nine',
        10 => 'ten', 11 => 'eleven', 12 => 'twelve',
        13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
        16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
        19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
        40 => 'forty', 50 => 'fifty', 60 => 'sixty',
        70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
    $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
    while ($i < $digits_length) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number] . ' ' . $digits[$counter] . $plural . ' ' . $hundred : $words[floor($number / 10) * 10] . ' ' . $words[$number % 10] . ' ' . $digits[$counter] . $plural . ' ' . $hundred;
        } else
            $str[] = null;
    }
    $Rupees = implode('', array_reverse($str));
    //$paise = ($decimal) ? " and " . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    $paise = '';
    if ($decimal < 100) {
        $paise = ($decimal) ? " and " . ($words[((int) ($decimal / 10)) * 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    }
    return ($Rupees ? $Rupees . ' ' : '') . $paise;
}
?>

<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/PurchaseOrder/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Purchase</span> - Purchase Order</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                    <!--<a href="#!/PurchaseOrder/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>-->
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-home2 position-left"></i>Purchase</li>
            <li><a href="#!/PurchaseOrder/all"><i class="icon-city position-left"></i> Purchase Order</a></li>
            <? if ($purchase_order['purchase_order_status'] == 'confirm') { ?>
                <li class="active"><?php if (isset($purchase_order)) echo 'POR' . str_pad($purchase_order['purchase_order_id'], 5, "0", STR_PAD_LEFT); ?></li>
            <? } else { ?>
                <li class="active"><?php if (isset($purchase_order)) echo 'DRAFT'; ?></li>
            <? } ?>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <a href="#!/PurchaseOrder/all"><button class="btn btn-default">Back<i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
    <a href="#!/PurchaseOrder/view/<? echo $purchase_order['purchase_order_status'] . '/' . $purchase_order['purchase_order_id']; ?>"><button class="btn btn-default" 
                                                                                                             <? if (isset($linked_documents)) if (sizeof($linked_documents) > 0) echo "disabled title = 'Cannot be edited because this invoice is linked to other documents'" ?>>Edit <i class="icon-pencil3 position-right"></i></button></a>

    <button onClick="window.print();" class="btn btn-primary">Print <i class="icon-arrow-right14 position-right"></i></button>
    <!-- Form validation -->
    <div class="panel-flat">
        <div class="panel panel-body col-md-offset-3"  id = "section-to-print" style="width: 210mm;min-height: 297mm;padding-left:40px;padding-right:40px;">

            <span id = "purchaseOrderHeader">Purchase Order</span>
            <hr>

            <table class = "purchaseOrderTable">
                <tr>
                    <td class = "logo_container" colspan = "2" rowspan = "2">
                        <img class = "logo" src="<? echo $purchase_order['purchase_order_oc_logo_path']; ?>"></img>
                    </td>
                    <td style="vertical-align: top">
                        <span class = "purchaseOrderField">Purchase Order date</span>												
                        <br/>
                        <span class = "purchaseOrderFieldValue"><? echo explode(" ", $purchase_order['purchase_order_date'])[0] ; ?></span>
                    </td>
                    <td style="vertical-align: top">
                        <span class = "purchaseOrderField">Revision Number</span>												
                        <br/>
                        <span class = "purchaseOrderFieldValue"><? echo empty($purchase_order['purchase_order_revision_number']) ? ' - ' : $purchase_order['purchase_order_revision_number']; ?></span>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top">
                        <span class = "purchaseOrderField">Purchase Order No.</span>												
                        <br/>
                        <? if ($purchase_order['purchase_order_status'] == 'confirm') { ?>
                            <span class = "purchaseOrderFieldValue">POR<? echo str_pad($purchase_order['purchase_order_id'], 5, "0", STR_PAD_LEFT); ?></span>
                        <? } else { ?>
                            <span class = "purchaseOrderFieldValue">DRAFT</span>
                        <? } ?>

                    </td>
                    <td style="vertical-align: top">
                        <span class = "purchaseOrderField">Revision Date</span>												
                        <br/>
                        <span class = "purchaseOrderFieldValue"><? echo empty(explode(" ", $purchase_order['purchase_order_revision_date'])[0]) ? ' - ' : explode(" ", $purchase_order['purchase_order_revision_date'])[0]; ?></span>
                    </td>
                </tr>
                <tr>
                    <td colspan = "2" rowspan = "2">
                        <span class = "purchaseOrderFieldValue"><? echo $purchase_order['purchase_order_oc_name']; ?></span><br/>
                        <span class = "purchaseOrderField">
                            <? echo $purchase_order['purchase_order_oc_address']; ?><br/>
                            <? echo $purchase_order['purchase_order_oc_city_name']; ?><br/>
                            <? echo $purchase_order['purchase_order_oc_district'].', '.$purchase_order['purchase_order_oc_state']; ?><br/>
                            GSTIN: <? echo $purchase_order['purchase_order_oc_gstin']; ?><br/>
                            PAN: <? echo $purchase_order['purchase_order_oc_pan_number']; ?>

                        </span>
                    </td>
                    <td style="vertical-align: top">
                        <span class = "purchaseOrderField">Validity</span>												
                        <br/>
                        <span class = "purchaseOrderFieldValue"><? echo $purchase_order['purchase_order_linked_company_payment_term_name']; ?></span>
                    </td>
                    
                </tr>
            </table>
            <table class = "purchaseOrderTable gray-border">

                <tr>
                    <td>Vendor Details</td>
                </tr>
                <tr>
                    <td>Kind Attention : <? echo $purchase_order['purchase_order_linked_company_invoice_name']; ?><br/>
                        Name:<span class = "purchaseOrderFieldValue"><? echo $purchase_order['purchase_order_linked_company_display_name']; ?></span><br/>
                        Address:<? echo $purchase_order['purchase_order_linked_company_billing_address']; ?><br/>
                        State:<? echo $purchase_order['purchase_order_linked_company_billing_state_name']; ?><br/>
                        Contact details:<? echo $purchase_order['purchase_order_linked_company_contact_number']; ?><br/>
                    </td>
                </tr>
            </table>
            <hr>
            <table class = "productList purchaseOrderTable">
                <thead class = "grayTBack">
                    <tr>
                        <td width='25px'>Sr</td>
                        <td width='150px'>Item Description</td>
                        <td>Rate / Item</td>
                        <td>Qty</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($purchase_order_entries as $purchase_order_entry) {
                        ?>
                        <tr>
                            <td><? echo $purchase_order_entry['po_entry_id']; ?></td>

                            <td><? echo $purchase_order_entry['po_product_name']; ?></td>		


                            <td>₹ <? echo number_format($purchase_order_entry['po_product_rate'], 2); ?></td>
                            <td><? echo number_format($purchase_order_entry['po_product_quantity'], 2) . ' ' . $purchase_order_entry['po_product_uqc_text']; ?></td>						  
                        </tr>

                        <?php
                    }
                    ?>
                </tbody>
                <tfoot class = "grayTBack">
                    <tr style="height:20px">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>

            <table class = "purchaseOrderTable">
                <tr>
                    <td colspan = "6" style = "text-align:left;">
                        <br/><br/>Terms & Conditions
                        <textarea class = "termsText" readonly>&#13;&#10;<? echo $purchase_order['purchase_order_terms']; ?></textarea>
                    <td colspan = "3">Authorized Signatory<br/><br/><br/><br/><span style = "font-weight:800;"><? echo $purchase_order['purchase_order_oc_name']; ?></span></td>
                </tr>
            </table>
            <footer style = "position:absolute;bottom:0;right:0;padding-right:20px;padding-bottom:15px;font-size:11px;font-style:italic;">
                Powered By Quant ERP
            </footer>
        </div>
        

    </div>


    <style>
        @media print {
            body * {
                visibility: hidden;
            }
            #section-to-print, #section-to-print * {
                visibility: visible;
                overflow:hidden;
            }
            #section-to-print {
                position: absolute;
                left: 0;
                top: 0;
                size: auto;   /* auto is the initial value */
                margin: 0;
                border: initial;
                border-radius: initial;
                width: initial;
                min-height: initial;
                box-shadow: initial;
                background: initial;
                page-break-after: always;
            }
            .grayTBack {
                visibility: visible;
                background-color: #ccc!important;
                color: black;
                font-weight:700;
                -webkit-print-color-adjust: exact;
                border: solid #ccc!important;
            }
            
            .gray-border{
                border: 1px solid #ccc;
                overflow:visible!important;
                -webkit-print-color-adjust: exact;
            }
        }
        @page 
        {
            size:  auto;   /* auto is the initial value */
            margin: 0mm;  /* this affects the margin in the printer settings */
        }
        .termsText{
            font-size: 10px;
            min-height: 12em;
            border:none;
            resize: none;
            overflow:hidden;
            width:100px;
            width: 375px;
            text-align: justify;
        }
        .productList{
            border: 1px solid #FFFFFF;
            text-align: center;
            font-size: 11px;
        }
        .whiteTBack {
            background-color: #FFFFFF;
            color: black;
            font-weight:700;
        }
        .grayTBack {
            background-color: #ccc;
            color: black;
            font-weight:700;
        }
        .purchaseOrderTable {
            font-size: 11px;
            width: 100%;
            max-width: 100%;
            table-layout: fixed;
            font-family:"arial";
            margin-bottom:5px; 
            padding:10px;
        }
        .gray-border{
            border: 1px solid #ccc;
            border-collapse: collapse;
        }

        .logo {
            max-width:40mm; max-height: 20mm;
            text-align: center;
        }
        .logo_container {
            /*text-align: center;*/
        }
        #purchaseOrderHeader {
            font-size: 20px;
            font:"arial";
            width: 100%;
            text-align: center;
            font-family:"arial";
            font-weight: bold;
            display: block;
        }
        hr {
            border: 3px solid #000000;
            margin-top:3px;
        }
        .purchaseOrderField {
            font-size: 11px;	
        }
        .purchaseOrderFieldValue {
            font-weight: bold;
            font-size: 12px;
        }
        td {
            padding:3px;
        }
        hr {
            margin-bottom: 5px;
        }
    </style>