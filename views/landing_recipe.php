

<!-- Page header -->
<div class="page-header">

    <div ng-controller="dataTableController" ng-init="load(3)"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Manufacturing</span> - Bill of Materials (Recipe)</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!recipe/view/new" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-list-alt  position-left" style = "font-size:22px;color:#26A69A !important"></i> <span>Add Bill Of Material</span></a>
                <a href="report/download/recipe" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-home2 position-left"></i>Manufacturing</li>
            <li class="active"><i class="icon-city position-left"></i>Bill Of Materials (Recipe)</li>
        </ul>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Recipe archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Bill Of Materials (Recipe)</h6>
            <div class="heading-elements">
                </ul>
            </div>
        </div>

        <table class="table masterDataTable">
            <thead>
                <tr>
                    <th>Recipe ID</th>
                    <th>Product Name</th>
                    <th>Recipe Quantity</th>
                    <th>Product Category</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($recipes as $recipe): ?>
                    <tr>
                        <td>
                            BOM<? echo str_pad($recipe['recipe_id'], 5, "0", STR_PAD_LEFT); ?>                                
                        </td>
                        <td>
                            <h6 class="no-margin">
                                <a href="#!recipe/view/<? echo $recipe['recipe_id']; ?>">
                                <span><?php echo $recipe['product_name']; ?></span></a>
                                <small class="display-block text-muted">Created by <?php echo $recipe['employee_name']; ?> 
                                    on <?php echo $recipe['recipe_record_creation_time']; ?></small>
                            </h6>
                        </td>
                        <td>
                            <?php echo $recipe['recipe_product_quantity']. ' '. $recipe['uqc_text']; ?>
                        </td>
                        <td>
                            <?php echo $recipe['product_category']; ?>
                        </td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li>
                                    <a href="#!recipe/view/<? echo $recipe['recipe_id']; ?>">
                                    <i class="icon-file-eye"></i></a>
                                    <? 
                                    if($this->session->userdata('access_controller')->is_access_granted('recipe', 'delete')) { ?>
                                    <li><a href='javascript: void(0)' onclick = 'showDeleteDialog("recipe", <?php echo $recipe['recipe_id']; ?>)'><i class="glyphicon glyphicon-trash"></i></a></li>
                                    <? } ?>
                                </li>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th>Recipe ID</th>
                    <th>Product Name</th>
                    <th>Recipe Quantity</th>
                    <th>Product Category</th>
                    <th class="text-center">Actions</th>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->