

<!-- Page header -->
<div class="page-header">

    <div ng-controller="dataTableController" ng-init="load(3)"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Manufacturing</span> - Manufacturing Order</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!ManufacturingOrder/view/new" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-list-alt  position-left" style = "font-size:22px;color:#26A69A !important"></i> <span>Add Manufacturing Order</span></a>
                <a href="report/download/ManufacturingOrder" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="fa fa-gavel position-left"></i>Manufacturing</li>
            <li class="active"><i class="fa fa-gears position-left"></i>Manufacturing Order</li>
        </ul>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Manufacturing Order archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Manufacturing Order</h6>
            <div class="heading-elements">
                </ul>
            </div>
        </div>

        <table class="table masterDataTable">
            <thead>
                <tr>
                    <th>Manufacturing Order ID</th>
                    <th>Product Name</th>
                    <th>Order Quantity</th>
                    <th>Order Date</th>
                    <th>Status</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($manufacturing_orders as $manufacturing_order): ?>
                    <tr>
                        <td>
                            <? if ($manufacturing_order['manufacturing_order_status'] == 'confirm' || $manufacturing_order['manufacturing_order_status'] == 'complete') { ?>
                                MFG<?php echo str_pad($manufacturing_order['manufacturing_order_id'], 5, "0", STR_PAD_LEFT); ?>
                            <? } else { ?>
                                DRAFT
                            <? } ?>
                        </td>
                        <td>
                            <h6 class="no-margin">
                                <? if ($manufacturing_order['manufacturing_order_status'] == 'confirm' || $manufacturing_order['manufacturing_order_status'] == 'complete') { ?>
                                    <a href="#!ManufacturingOrder/preview/<? echo $manufacturing_order['manufacturing_order_status']; ?>/<? echo $manufacturing_order['manufacturing_order_id']; ?>">
                                    <? } else { ?>
                                        <a href="#!ManufacturingOrder/view/<? echo $manufacturing_order['manufacturing_order_status']; ?>/<? echo $manufacturing_order['manufacturing_order_id']; ?>">
                                        <? } ?><span><?php echo $manufacturing_order['manufacturing_order_linked_product_name']; ?></span></a>
                                    <small class="display-block text-muted">Created by <?php echo $manufacturing_order['employee_name']; ?> 
                                        on <?php echo $manufacturing_order['manufacturing_order_record_creation_time']; ?></small>
                            </h6>
                        </td>
                        <td>
                            <?php echo $manufacturing_order['manufacturing_order_product_quantity']. ' '. $manufacturing_order['uqc_text']; ?>
                        </td>
                        <td>
                            <?php echo explode(" ", $manufacturing_order['manufacturing_order_date'])[0]; ?>
                        </td>
                        <td>
                            <?
                                if($manufacturing_order['manufacturing_order_status'] == 'complete'){
                                    echo '<span class="label bg-success"><span>COMPLETE</span></span>';
                                } else if($manufacturing_order['manufacturing_order_status'] == 'confirm'){
                                    echo '<span class="label bg-blue"><span>CONFIRM</span></span>';
                                } else if ($manufacturing_order['manufacturing_order_status'] == 'draft'){
                                    echo '<span class="label bg-danger"><span>DRAFT</span></span>';
                                }
                            ?>
                        </td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li>
                                    <a href="#!ManufacturingOrder/view/<? echo $manufacturing_order['manufacturing_order_id']; ?>">
                                    <i class="icon-file-eye"></i></a>
                                    <? 
                                    if($manufacturing_order['manufacturing_order_status'] == 'draft'){
                                    if($this->session->userdata('access_controller')->is_access_granted('manufacturing_order', 'delete')) { ?>
                                        <li><a href='javascript: void(0)' onclick = 'showDeleteDialog("ManufacturingOrder", <?php echo $manufacturing_order['manufacturing_order_id']; ?>)'><i class="glyphicon glyphicon-trash"></i></a></li>
                                    <? } 
                                    } ?>
                                </li>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th>Manufacturing Order ID</th>
                    <th>Product Name</th>
                    <th>Order Quantity</th>
                    <th>Order Date</th>
                    <th>Status</th>
                    <th class="text-center">Actions</th>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->