

<!-- Page header -->
<div class="page-header">

    <div ng-controller="noGroupDataTableController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Accounting</span> - Transactions</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="report/download/system_transaction" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-calculator2 position-left"></i>Accounting</li>
            <li class="active"><i class="icon-book2 position-left"></i>Transactions</li>
        </ul>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Transactions archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Transactions</h6>
            <div class="heading-elements">
                </ul>
            </div>
        </div>

        <table class="table noGroupDataTable">
            <thead>
                <tr>
                    <th style="width:200px">Date</th>
                    <th style="width:300px">Transactions ID</th>
                    <th style="width:250px">Linked Entity</th>
                    <th style="width:250px">Account Name</th>
                    <th>Debit</th>
                    <th>Credit</th>
                    <th>Description</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $current_journal_id = '';
                    foreach ($transactions as $transaction): 
                        if($transaction['sj_amount'] > 0) {?>
                <tr>
                    <td>
                        <?
                        echo $transaction['sj_date'];
                        ?>
                    </td>
                    <td>
                        <?
                        //echo $transaction['sj_document_id_text'];
                        if ($transaction['sj_document_type'] == 'invoice') {
                                echo 'Tax Invoice - <a href = "master#!/invoice/preview/confirm/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'credit_note') {
                                echo 'Credit Note - <a href = "master#!/CreditNote/view/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'purchase') {
                                echo 'Purchase - <a href = "master#!/purchase/view/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'debit_note') {
                                echo 'Debit Note - <a href = "master#!/DebitNote/view/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'journal') {
                                echo 'Journal - <a href = "master#!/journal/view/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'coa_opening_balance') {
                                echo 'Opening Balance - <a href = "master#!/COA/view/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'receipt') {
                                echo 'Receipt - <a href = "master#!/receipt/preview/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'manufacturing') {
                                echo 'Manufacturing Order - <a href = "master#!/ManufacturingOrder/preview/confirm/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'advance_receipt') {
                                echo 'Advance Receipt - <a href = "master#!/AdvanceReceipt/preview/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'payment') {
                                echo 'Payment - <a href = "master#!/payment/preview/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'advance_payment') {
                                echo 'Advance Payment - <a href = "master#!/AdvancePayment/preview/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'expense') {
                                echo 'Expense - <a href = "master#!/expense/view/confirm/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'inventory_adjustment') {
                                echo 'Inventory Adjustment - <a href = "master#!/Inventory/adjustment/view/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            }  else if ($transaction['sj_document_type'] == 'broker') {
                                echo 'Opening Balance for Broker - <a href = "master#!/broker/view/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'company') {
                                echo 'Opening Balance for Company - <a href = "master#!/company/view/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'employee') {
                                echo 'Opening Balance for Employee - <a href = "master#!/employee/view/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'transporter') {
                                echo 'Opening Balance for Transporter - <a href = "master#!/transporter/view/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'sales_charges') {
                                echo 'Tax Invoice - <a href = "master#!/invoice/preview/confirm/' . str_replace('INV', '', $transaction['sj_document_id_text']) . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'purchase_charges') {
                                echo 'Purchase - <a href = "master#!/purchase/preview/confirm/' . str_replace('PUR', '', $transaction['sj_document_id_text']) . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'credit_note_charges') {
                                echo 'Credit Note - <a href = "master#!/CreditNote/preview/confirm/' . str_replace('CDN', '', $transaction['sj_document_id_text']) . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'debit_note_charges') {
                                echo 'Debit Note - <a href = "master#!/DebitNote/preview/confirm/' . str_replace('DBN', '', $transaction['sj_document_id_text']) . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else {
                                echo ucwords($transaction['sj_document_type']) .' - '.'<span style="color:#4CAF50">' . $transaction['sj_document_id_text'] . '</span>';
                            }
                        ?>
                    </td>
                    <td>
                        <?
                        if (!empty($transaction['sj_linked_broker_id'])){ ?>
                            Broker - <a href = "master#!/SJ/entity/broker/<? echo $transaction['sj_linked_broker_id']; ?>"><? echo $transaction['broker_name']; ?></a>
                        <? } else if (!empty($transaction['sj_linked_company_id'])){ ?>
                            Company - <a href = "master#!/SJ/entity/company/<? echo $transaction['sj_linked_company_id']; ?>"><? echo $transaction['company_display_name']; ?></a>
                        <? } else if (!empty($transaction['sj_linked_employee_id'])){ ?>
                            Employee - <a href = "master#!/SJ/entity/employee/<? echo $transaction['sj_linked_employee_id']; ?>"><? echo $transaction['employee_name']; ?></a>
                        <? } else if (!empty($transaction['sj_linked_transporter_id'])){ ?>
                            Transporter - <a href = "master#!/SJ/entity/transporter/<? echo $transaction['sj_linked_transporter_id']; ?>"><? echo $transaction['transporter_name']; ?></a>
                        <? }                        
                        ?>
                    </td>
                    <td>
                        <a href ="master#!/SJ/ledger/<? echo $transaction['sj_account_coa_id']; ?>">
                        <?
                        echo $transaction['coa_account_name'];
                        ?>
                        </a>
                    </td>
                    <td>
                        <?
                        if($transaction['sj_transaction_type'] == 'debit'){
                            echo '₹ '. number_format($transaction['sj_amount'], 2);
                        }
                        ?>
                    </td>
                    <td>
                        <?
                        if($transaction['sj_transaction_type'] == 'credit'){
                            echo '₹ '.number_format($transaction['sj_amount'], 2);
                        }
                        ?>
                    </td>
                    
                    <td>
                        <? echo $transaction['sj_narration']; ?>
                    </td>
                    </tr>
                        <?}
                        endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th>Date</th>
                    <th>Transactions ID</th>
                    <th>Company</th>
                    <th>Account Name</th>
                    <th>Debit</th>
                    <th>Credit</th>
                    <th>Description</th>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->