
<div ng-controller="journalController" ng-init="load()"></div>
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/journal/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Accounting</span> - Journal</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!/journal/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!"><i class="icon-calculator2 position-left"></i> Accounting</a></li>
            <li><a href="#!/journal/all"><i class="icon-book2 position-left"></i> Journal</a></li>
            <li class="active"><?php
                if (isset($journal))
                    echo 'JRN' . str_pad($journal['journal_id'], 5, "0", STR_PAD_LEFT);
                else
                    echo "New";
                ?></li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Form validation -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Journal</h5>

        </div>


        <div class="panel-body">
            <?php
            $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id'=>'merpForm');
            if (isset($journal))
                echo form_open('web/journal/save/' . $journal['journal_id'], $attributes);
            else
                echo form_open('web/journal/save', $attributes);
            ?>
            <p class="content-group-lg">The Journal includes all non-system created accounting transactions of the business. Account Balances can be adjusted based on manual transactions.</p>

            <fieldset class="content-group">
                <legend class="text-bold">Narration</legend>

                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Date</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="1" name="journal_date" id = "datePicker" required="required"
                                   value = "<?php if (isset($journal)) echo explode(" ", $journal['journal_date'])[0]; ?>">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Narration <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="3" name = "journal_narration" type="text"
                                   value = "<?php if (isset($journal)) echo $journal['journal_narration']; ?>" required>
                        </div>
                    </div>
                </div>
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Entries</legend>
                <table class="table journalDatatable">
                    <thead>
                        <tr>
                            <th>Sr</th>
                            <th>Account Name</th>
                            <th>Sub-account</th>
                            <th>Description</th>
                            <th>Debit (Dr)</th>
                            <th>Credit (Cr)</th>
                            <th>Linked Entity</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $counter = 0;
                        if (isset($journal_entries))
                            foreach ($journal_entries as $journal_entry) {
                                ?>
                                <tr>
                                    <td>
                                        <? echo ($counter + 1); ?>
                                        <input name = "journal_entries[<? echo $counter; ?>][0]" type = "number" value = "<? echo $journal_entry['je_entry_id']; ?>" style="display:none" readonly></input>
                                    </td>
                                    <td>
                                        <select onchange = "updateJournalRows(this);validateJournalEntry();" required class = "form-control dtSelect2">
                                            <? foreach ($parents as $parent) { ?>
                                                <option value = "<? echo $parent['coa_id']; ?>" 
                                                <?
                                                $parent_account_id = $journal_entry['je_linked_coa_id'];
                                                if (isset($journal_entry['coa_subaccount_of_id'])) {
                                                    $parent_account_id = $journal_entry['coa_subaccount_of_id'];
                                                }
                                                if ($parent_account_id == $parent['coa_id'])
                                                    echo "selected";
                                                ?>><? echo $parent['coa_account_name']; ?></option>   
                                                    <? }
                                                    ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select onchange = "validateJournalEntry();" name = "journal_entries[<? echo $counter; ?>][1]" required class = "form-control dtSelect2">
                                            <option value = "<? echo $parent_account_id; ?>">None</option>
                                            <?
                                            foreach ($subaccounts as $subaccount) {
                                                if ($subaccount['coa_subaccount_of_id'] != $parent_account_id)
                                                    continue;
                                                ?>
                                                <option value = "<? echo $subaccount['coa_id']; ?>"
                                                <? if ($journal_entry['je_linked_coa_id'] == $subaccount['coa_id'])
                                                    echo " selected ";
                                                ?>>
                                                <? echo $subaccount['coa_account_name']; ?> 
                                                </option>  
                                                <?
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    
                                    <td>
                                        <input name = "journal_entries[<? echo $counter; ?>][2]" class = "form-control" type = "text" 
                                               value="<? echo $journal_entry['je_description']; ?>"></input>
                                    </td>
                                    <td>
                                        <input name = "journal_entries[<? echo $counter; ?>][3]" class = "form-control" 
                                               type = "number" step = 0.01 onchange = "invalidate(this, 3); validateJournalEntry()"    
                                               value="<?
                                               if ($journal_entry['je_transaction_type'] == 'debit') {
                                                   echo $journal_entry['je_amount'];
                                               }
                                               ?>" 
                                               <?
                                               if ($journal_entry['je_transaction_type'] != 'debit') {
                                                   echo " disabled";
                                               }
                                               ?>
                                               ></input></td>
                                    <td>
                                        <input name = "journal_entries[<? echo $counter; ?>][4]" class = "form-control" 
                                               type = "number" step = 0.01 onchange = "invalidate(this, 4); validateJournalEntry()"    
                                               value="<?
                                               if ($journal_entry['je_transaction_type'] == 'credit') {
                                                   echo $journal_entry['je_amount'];
                                               }
                                               ?>" 
                                               <?
                                               if ($journal_entry['je_transaction_type'] != 'credit') {
                                                   echo " disabled";
                                               }
                                               ?>
                                               ></input></td>
                                    <td>
                                        <select name = "journal_entries[<? echo $counter; ?>][5]" class = "form-control dtSelect2">
                                            <option value = "">None</option>
                                            <optgroup label = "Brokers">
                                                <?
                                                foreach($brokers as $broker){
                                                ?>
                                                <option value ="<? echo 'broker_'.$broker['broker_id']; ?>" 
                                                        <? if(isset($journal_entry['je_linked_broker_id'])){
                                                            if($journal_entry['je_linked_broker_id'] == $broker['broker_id'])
                                                                echo "selected";
                                                        }
                                                        ?>
                                                        ><? echo $broker['broker_name']; ?></option>
                                                <? 
                                                }
                                                ?>
                                            </optgroup>
                                            <optgroup label = "Companies">
                                                <?
                                                foreach($companies as $company){
                                                ?>
                                                <option value ="<? echo 'company_'.$company['company_id']; ?>" 
                                                        <? if(isset($journal_entry['je_linked_company_id'])){
                                                            if($journal_entry['je_linked_company_id'] == $company['company_id'])
                                                                echo "selected";
                                                        }
                                                        ?>
                                                        ><? echo $company['company_display_name']; ?></option>
                                                <? 
                                                }
                                                ?>
                                            </optgroup>
                                            <optgroup label = "Employees">
                                                <?
                                                foreach($employees as $employee){
                                                ?>
                                                <option value ="<? echo 'employee_'.$employee['employee_id']; ?>" 
                                                        <? if(isset($journal_entry['je_linked_employee_id'])){
                                                            if($journal_entry['je_linked_employee_id'] == $employee['employee_id'])
                                                                echo "selected";
                                                        }
                                                        ?> 
                                                        ><? echo $employee['employee_name']; ?></option>
                                                <? 
                                                }
                                                ?>
                                            </optgroup>
                                            <optgroup label = "Transporters">
                                                <?
                                                foreach($transporters as $transporter){
                                                ?>
                                                <option value ="<? echo 'transporter_'.$transporter['transporter_id']; ?>" 
                                                        <? if(isset($journal_entry['je_linked_transporter_id'])){
                                                            if($journal_entry['je_linked_transporter_id'] == $transporter['transporter_id'])
                                                                echo "selected";
                                                        }
                                                        ?> 
                                                        ><? echo $transporter['transporter_name']; ?></option>
                                                <? 
                                                }
                                                ?>
                                            </optgroup>
                                        </select>
                                    </td>
                                    <td>
                                        <?
                                        echo '<a href="javascript: void(0)" onclick = "deleteJournalRow(this);"><i class="glyphicon glyphicon-trash"></i></a>';
                                        ?>
                                    </td>
                                </tr>

                                <?
                                $counter++;
                            }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Sr</th>
                            <th>Account Name</th>
                            <th>Sub-account</th>
                            <th>Description</th>
                            <th>Debit (Dr)</th>
                            <th>Credit (Cr)</th>
                            <th>Linked Entity</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </tfoot>
                </table>
                <div class="form-group col-lg-12">
                    <a onclick = "updateJournalRows(null); validateJournalEntry();" class="btn btn-link btn-float has-text text-success"><span>+Add New Row</span></a>
                </div>										

                <div class="form-group col-lg-6">
                </div>	
            </fieldset>

            <div class="text-right">
                <a href="#!/journal/all"><button class="btn btn-default">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                <? if ($this->session->userdata('access_controller')->is_access_granted('journal', 'save')) { ?>
                <button onclick="submitForm('journal');" id = "buttonSaveConfirmJournal" class="btn btn-primary">Confirm <i class="icon-arrow-right14 position-right"></i></button>
                <? } ?>
            </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->