

<!-- Page header -->
<div class="page-header">
    <div ng-controller="formController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/broker/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Master</span> - Agent</h4>
        </div>
        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!/broker/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!"><i class="icon-home2 position-left"></i> Master</a></li>
            <li><a href="#!/broker/all"><i class="icon-man position-left"></i> Agent</a></li>
            <li class="active"><?php
                if (isset($broker))
                    echo 'BRK' . str_pad($broker['broker_id'], 5, "0", STR_PAD_LEFT);
                else
                    echo "New";
                ?></li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Form validation -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Agent</h5>
            <div class="heading-elements">
        
            </div>
        </div>


        <div class="panel-body">
            <p class="content-group-lg">An Agent is a business partner who helps in promoting Sales in exchange of a commission. Agent details entered here will be linked with Customer Master, Sales, Accounts and Reports. </p>
            <?php echo validation_errors(); ?>
            <?php
            $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id'=>'merpForm');
            if (isset($broker))
                echo form_open('web/broker/save/' . $broker['broker_id'], $attributes);
            else
                echo form_open('web/broker/save', $attributes);
            ?>
            <fieldset class="content-group">
                <legend class="text-bold">Basic details</legend>	
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="name">Agent Name <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" onblur ="checkIfUnique(this, 'broker', 'broker_name', '<? if(isset($broker)) echo $broker['broker_id']; ?>', 'broker_id')" required name="broker_name" style="text-transform: capitalize;"
                                   placeholder="Enter broker name. Must be unique." type="text" value = "<?php if (isset($broker)) echo $broker['broker_name']; ?>" tabindex = "1" placeholder="Enter Agent Name">
                        </div> 
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="broker_invoice_name">Invoice Name <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" required name="broker_invoice_name" style="text-transform: capitalize;" type="text" value = "<?php if (isset($broker)) echo $broker['broker_invoice_name']; ?>" tabindex = "2" placeholder="Enter broker name as it should appear on Invoice">
                        </div>
                    </div>
                </div>
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Location details</legend>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="broker_address">Address</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="broker_address" type="text" value = "<?php if (isset($broker)) echo $broker['broker_address']; ?>" tabindex = "3" placeholder="Enter address">
                        </div>
                    </div>


                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="broker_area">Area</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="broker_area" type="text" value = "<?php if (isset($broker)) echo $broker['broker_area']; ?>" tabindex = "4" placeholder = "Enter area">
                        </div>
                    </div>		
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="broker_pincode">Pincode</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="broker_pincode" type="text" value = "<?php if (isset($broker)) echo $broker['broker_pincode']; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' tabindex = "5">
                        </div>
                    </div>	
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="broker_location_id">City <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select required class="form-control select2" name="broker_location_id" id="city" onChange = "refreshLocationDistrictAndState()" tabindex = "6">
                                        <option>Choose a location</option>
                                        <?php
                                        foreach ($locations as $location) {
                                            ?>
                                            <option district = "<?php echo $location['district'] ?>" state = "<?php echo $location['state_name'] ?>" 
                                            <?php
                                            if (set_value('bank_location_id') == $location['location_id'])
                                                echo "selected";
                                            else if (isset($broker)) {
                                                if ($broker['broker_location_id'] == $location['location_id'])
                                                    echo "selected";
                                            }
                                            ?> value="<? echo $location['location_id']; ?>"><? echo $location['city_name'] ?>
                                            </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                        </div>
                    </div>	
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="district">District</label>
                        <div class="col-lg-9">
                            <input class="form-control" id="district" type="text" readonly>
                        </div>
                    </div>							

                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="state">State</label>
                        <div class="col-lg-9">
                            <input class="form-control" id="state" type="text" readonly>
                        </div>
                    </div>
                </div>
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Contact details</legend>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="broker_contact_person_name">Contact Person</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="broker_contact_person_name" style="text-transform: capitalize;" type="text" value = "<?php if (isset($broker)) echo $broker['broker_contact_person_name']; ?>"  tabindex = "7">
                        </div>
                    </div>


                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="broker_email_address">Email Address</label><span id = "emailAddressError" style = "color:red; display:none;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Please check email address format !</span>
                        <div class="col-lg-9">
                            <input class="form-control" name="broker_email_address" type="text" value = "<?php if (isset($broker)) echo $broker['broker_email_address']; ?>" tabindex = "8">
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="broker_contact_number">Contact Number</label>
                        <div class="col-lg-9">
                            <input class="form-control format-phone-number" name="broker_contact_number" type="text" value = "<?php if (isset($broker)) echo $broker['broker_contact_number']; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' tabindex = "9">
                        </div>
                    </div>
                </div>
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Other details</legend>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="broker_gst_number">GST Number <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9@]+/, '')" required name="broker_gst_number" type="text" value = "<?php if (isset($broker)) echo $broker['broker_gst_number']; ?>" tabindex = "10" placeholder = "Should be 15 characters">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Commission</label>
                        <div class="col-lg-9">
                            <div class="input-group">
                                <input type="text" name="broker_commission" value = "<?php if (isset($broker)) echo $broker['broker_commission']; ?>" class="touchspin-postfix" placeholder="Commission in percentage" tabindex = "11">
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>

            <div class="text-right">
                <a href="#!/broker/all"><button class="btn btn-default" tabindex = "12">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                <? 
                if($this->session->userdata('access_controller')->is_access_granted('broker', 'save')) { ?>
                <button onclick="submitForm('agent', 'broker/all');" id = "buttonSubmit" class="btn btn-primary" tabindex = "14"> Save <i class="icon-arrow-right14 position-right"></i></button>
                <? } ?>
            </div>
            </form>

        </div>

        </section>
    </div>	
    <!-- /form validation -->
    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->
