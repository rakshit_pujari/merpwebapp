
<!-- Page header -->
<div class="page-header">
    <div ng-controller="gstReportController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Report - Goods & Services Tax (GST) Return</span></h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">

            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-stack4 position-left"></i> Report</li>
            <li class="active"><i class="fa fa-google position-left"></i>GST</li>
        </ul>
    </div>
</div>

<!-- Content area -->
<div class="content">

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-body">
                <div class="col-lg-9">
                    <div class="col-lg-6">
                        <label class="control-label col-lg-3">Choose Month</label>
                        <div class="col-lg-9">
                            <select class="form-control select" id = "gstrReportMonth">
                                <option value = '1' <? if (date('m') == '01') echo 'selected'; ?>>January</option>
                                <option value = '2' <? if (date('m') == '02') echo 'selected'; ?>>February</option>
                                <option value = '3' <? if (date('m') == '03') echo 'selected'; ?>>March</option>
                                <option value = '4' <? if (date('m') == '04') echo 'selected'; ?>>April</option>
                                <option value = '5' <? if (date('m') == '05') echo 'selected'; ?>>May</option>
                                <option value = '6' <? if (date('m') == '06') echo 'selected'; ?>>June</option>
                                <option value = '7' <? if (date('m') == '07') echo 'selected'; ?>>July</option>
                                <option value = '8' <? if (date('m') == '08') echo 'selected'; ?>>August</option>
                                <option value = '9' <? if (date('m') == '09') echo 'selected'; ?>>September</option>
                                <option value = '10' <? if (date('m') == '10') echo 'selected'; ?>>October</option>
                                <option value = '11' <? if (date('m') == '11') echo 'selected'; ?>>November</option>
                                <option value = '12'<? if (date('m') == '12') echo 'selected'; ?>>December</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <label class="col-lg-3">Choose Year</label>
                        <div class="col-lg-9">
                            <select class="form-control select" id = "gstrReportYear">
                                <option value = '<? echo date('Y', strtotime('-1 year'))?>'><? echo date('Y', strtotime('-1 year'))?></option>
                                <option value = '<? echo date('Y')?>' selected><? echo date('Y')?></option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="text-right">
                        <button onclick = 'downloadAllGSTReports()' class="btn btn-primary">Download All Reports <i class="glyphicon glyphicon-download-alt position-right"></i></button>
                    </div>
                </div> 
            </div>
        </div>
    </div>
    <h3 class="media-heading text-bold">&nbsp;GSTR-1</h3><br/>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-body">
                <div class="media no-margin stack-media-on-mobile">
                    <div class="media-left media-middle">
                        <i class="icon-file-text2 text-success-400 icon-3x no-edge-top"></i>
                    </div>
                    <div class="media-body">
                        <h6 class="media-heading text-semibold">GSTR-1</h6>
                        Summary of the Monthly Outward Supplies (Sales) of the business
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <!-- Simple inline block with icon and button -->
            <div class="panel panel-body">
                <div class="media no-margin stack-media-on-mobile">
                    <div class="media-left media-middle">
                        <i class="icon-file-download icon-3x text-muted no-edge-top"></i>
                    </div>

                    <div class="media-body">
                        <h6 class="media-heading text-semibold">Deadline - 10th Of Every Month</h6>
                        <span class = "text-muted">Next Return Date : </span><span class = "text-success-400 text-bold"><?
                            date_default_timezone_set('Asia/Kolkata');
                            if (date('d') >= 10) {
                                echo '10th ' . date('F Y', strtotime('first day +1 month'));
                            } else {
                                echo '10th ' . date('F Y');
                            }
                            ?>
                        </span>
                    </div>

                    <div class="media-right media-middle">
                        <a onclick = 'downloadGSTR1()' class="btn btn-primary"><i class="icon-file-download position-left"></i> Download</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
        <h3 class="media-heading text-bold">&nbsp;GSTR-2A</h3><br/>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-body">
                <div class="media no-margin stack-media-on-mobile">
                    <div class="media-left media-middle">
                        <i class="icon-file-text2 text-danger-400 icon-3x no-edge-top"></i>
                    </div>
                    <div class="media-body">
                        <h6 class="media-heading text-semibold">GSTR-2A</h6>
                        Summary of the Monthly Inward Supplies (Purchase) of the business
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <!-- Simple inline block with icon and button -->
            <div class="panel panel-body">
                <div class="media no-margin stack-media-on-mobile">
                    <div class="media-left media-middle">
                        <i class="icon-file-download icon-3x text-muted no-edge-top"></i>
                    </div>

                    <div class="media-body">
                        <h6 class="media-heading text-semibold">Deadline - 15th Of Every Month</h6>
                        <span class = "text-muted">Next Return Date : </span><span class = "text-danger-400 text-bold"><?
                            date_default_timezone_set('Asia/Kolkata');
                            if (date('d') >= 15) {
                                echo '15th ' . date('F Y', strtotime('+1 month'));
                            } else {
                                echo '15th ' . date('F Y');
                            }
                            ?>
                        </span>
                    </div>

                    <div class="media-right media-middle">
                        <a onclick = 'downloadGSTR2A()' class="btn btn-primary"><i class="icon-file-download position-left"></i> Download</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <h3 class="media-heading text-bold">&nbsp;GSTR-3B</h3><br/>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-body">
                <div class="media no-margin stack-media-on-mobile">
                    <div class="media-left media-middle">
                        <i class="icon-file-text2 text-blue-400 icon-3x no-edge-top"></i>
                    </div>
                    <div class="media-body">
                        <h6 class="media-heading text-semibold">GSTR-3B</h6>
                        Monthly Return Summary of the Inward Supplies (Purchase) and Outward Supplies (Sales)
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <!-- Simple inline block with icon and button -->
            <div class="panel panel-body">
                <div class="media no-margin stack-media-on-mobile">
                    <div class="media-left media-middle">
                        <i class="icon-file-download icon-3x text-muted no-edge-top"></i>
                    </div>

                    <div class="media-body">
                        <h6 class="media-heading text-semibold">Deadline - 20th Of Every Month</h6>
                        <span class = "text-muted">Next Return Date : </span><span class = "text-blue-400 text-bold"><?
                            date_default_timezone_set('Asia/Kolkata');
                            if (date('d') >= 20) {
                                echo '20th ' . date('F Y', strtotime('+1 month'));
                            } else {
                                echo '20th ' . date('F Y');
                            }
                            ?>
                        </span>
                    </div>

                    <div class="media-right media-middle">
                        <a onclick = 'downloadGSTR3B()' class="btn btn-primary"><i class="icon-file-download position-left"></i> Download</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->