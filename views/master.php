<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Quant</title>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>assets/css/core.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>assets/css/components.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>assets/css/colors.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
        <script src="https://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>


        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.5/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.5/angular-route.js"></script>

        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/loaders/pace.min.js"></script>
        <!--<script type="text/javascript" src="<?php echo base_url() ?>assets/js/core/libraries/jquery.min.js"></script>-->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/core/libraries/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/notifications/bootbox.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/loaders/blockui.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/echarts/3.8.5/echarts-en.common.js"></script>-->
        <!-- File upload -->
        <!-- ECharts import -->
        <!--<script src="https://echarts.baidu.com/build/dist/echarts-all.js"></script>-->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/uploaders/fileinput/plugins/purify.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/uploaders/fileinput/plugins/sortable.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/uploaders/fileinput/fileinput.min.js"></script>
        <!-- / File upload -->


        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/forms/validation/validate.min.js"></script>
        <script type="text/javascript" src="https://ajax.microsoft.com/ajax/jquery.validate/1.7/additional-methods.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/forms/inputs/touchspin.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/core/libraries/jquery_ui/interactions.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/tables/datatables/extensions/select.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/tables/datatables/extensions/scroller.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/forms/styling/switch.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/forms/styling/switchery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/forms/styling/uniform.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/notifications/sweet_alert.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/forms/inputs/formatter.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/forms/inputs/typeahead/handlebars.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/core/app.js"></script>

        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/utils.js"></script>

        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/master_form_initialization.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/invoice_form_initialization.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/purchase_form_initialization.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/quotation_form_initialization.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/recipe_form_initialization.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/manufacturing_order_form_initialization.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/credit_note_initialization.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/debit_note_initialization.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/receipt_initialization.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/advance_payment_initialization.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/advance_receipt_initialization.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/payment_initialization.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/settings_form_initialization.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/inventory_adjustment.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/journal_form_initialization.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/expense_form_initialization.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/coa_ob_initialization.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/payment_receipt_ob_initialization.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/pob_initialization.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/uploader_bootstrap.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/data_table.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/delete_modal.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/report_initialization.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/pro_forma_initialization.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/purchase_order_initialization.js"></script>

        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/echarts-all-english-v2.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/dashboard_initialization.js"></script>

        <link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/images/logo_icon_light.png"/>
        <!-- /theme JS files -->
        <style>
            /* pre loader*/
            .no-js #loader { display: none;  }
            .js #loader { display: block; position: absolute; left: 100px; top: 0; }
            .se-pre-con {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url('<?php echo base_url() ?>assets/images/preloader/Preloader_1.gif') center no-repeat #fff;
            }
        </style>
        <script>
            //pre loader
            $(window).load(function () {
                setTimeout(function () {
                    // Animate loader off screen
                    $(".se-pre-con").fadeOut("slow");
                    //$(".se-pre-con").hide();
                }, 1500);
            });
            //online-offline status
            setInterval(function () {
                var ajaxTime = new Date().getTime();
                $.get("<?php echo base_url() ?>index.php/web/master/online", function (data, status) {
                    if (data.trim().includes('success')) {
                        var totalTime = new Date().getTime() - ajaxTime;
                        if (totalTime < 400) {
                            document.getElementById('navbar-app-status').innerHTML = "ONLINE";
                            document.getElementById('navbar-app-status').className = "label bg-success";
                        } else if (totalTime < 2000) {
                            document.getElementById('navbar-app-status').innerHTML = "SLOW INTERNET DETECTED";
                            document.getElementById('navbar-app-status').className = "label bg-orange-400";
                        } else {
                            document.getElementById('navbar-app-status').innerHTML = "VERY SLOW INTERNET DETECTED";
                            document.getElementById('navbar-app-status').className = "label bg-danger";
                        }

                    } else {
                        window.location = 'login/';
                    }

                }).error(function (jqXHR, textStatus, errorThrown) {
                    document.getElementById('navbar-app-status').innerHTML = "OFFLINE";
                    document.getElementById('navbar-app-status').className = "label bg-danger";
                });
            }, 10000);
        </script>

    </head>	
    <body ng-app="merp" style = "background-color: #fcfcfc">
        <!-- Main navbar -->
        <div class="se-pre-con"></div>
        <div class="navbar navbar-default navbar-fixed-top header-highlight">
            <div class="navbar-header" style = "background-color:#263338">
                <a class="navbar-brand" href="http://quanterp.com" target = "blank_"><img src="<?php echo base_url() ?>assets/images/logo_6.png" alt="logo" style="margin-top: -7px;margin-left: -1px;height: 31px;"></a>

                <ul class="nav navbar-nav visible-xs-block"> 
                    <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                    <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
                </ul>
            </div>

            <div class="navbar-collapse collapse" id="navbar-mobile">

                <ul class="nav navbar-nav">
                    <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
                </ul>

                <p class="navbar-text"><span id = "navbar-app-status" class="label bg-success">Online</span></p>
                
                <ul class="nav navbar-nav navbar-right">

                    <li class="dropdown">
                        <a href="https://quanterp.com/page-support.html" class="dropdown-toggle" target = "blank_">
                            <span style="margin-right: 5px">Support</span>    
                            <i class="icon-comment-discussion"></i>                                
                                <!--<span class="badge bg-warning-400">2</span>-->
                        </a>
                    </li>
                    <li class="dropdown dropdown-user">
                        
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <!--<img src="<?php echo base_url() . $this->session->userdata('employee')['employee_image_path'] ?>" alt="">-->
                            <span><? echo $this->session->userdata('username'); ?></span>
                            <i class="caret"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <? if ($this->session->userdata('access_controller')->is_access_granted('settings', 'save')) { ?>
                                <li><a href="#!settings/profile"><i class="icon-cog5"></i>Preferences</a></li>
                                <?
                            }
                            if ($this->session->userdata('access_controller')->is_access_granted('employee_rights', 'save')) {
                                ?>
                                <li><a href="#!employee/rights/all"><i class="icon-user-plus"></i>Employee Rights</a></li>
                                <?
                            }
                            if ($this->session->userdata('access_controller')->is_access_granted('accounting_preferences', 'save')) {
                                ?>
                                <li><a href="#!settings/accounting"><i class="icon-calculator2"></i>Accounting Settings</a></li>
                            <li class="divider"></li> 
                            <? } ?>
                            
                            <li><a href="login/signout"><i class="icon-switch2"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /main navbar -->

        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main sidebar-fixed">
                    <div class="sidebar-content">

                        <!-- User menu -->
                        <div class="sidebar-user">
                            <div class="category-content">
                                <div class="media">
                                    <a href="#" class="media-left">
                                        <img src="<?php echo base_url() . $this->session->userdata('employee')['employee_image_path']; ?>" class="img-circle img-sm" alt="">
                                    </a>
                                    <div class="media-body">
                                        <span class="media-heading text-semibold"><? echo $this->session->userdata('employee')['employee_name']; ?></span>
                                        <div class="text-size-mini text-muted">
                                            <i class="icon-user text-size-small"></i> &nbsp;<? echo $this->session->userdata('employee')['employee_department']; ?>
                                        </div>
                                    </div>

                                    <div class="media-right media-middle">
                                        <ul class="icons-list">
                                            <? if ($this->session->userdata('access_controller')->is_access_granted('settings', 'save')) { ?>
                                                <li>
                                                    <a href="#!settings/profile"><i class="icon-cog3"></i></a>
                                                </li>
                                            <? } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /user menu -->


                        <!-- Main navigation -->
                        <div class="sidebar-category sidebar-category-visible">
                            <div class="category-content no-padding">
                                <ul class="navigation navigation-main navigation-accordion">

                                    <!-- Main -->
                                    <li><a href="#!dashboard/one"><i class="icon-home4"></i> <span>Dashboard</span></a></li>

                                    <?
                                    if ($this->session->userdata('access_controller')->is_access_granted('bank', 'view') ||
                                            $this->session->userdata('access_controller')->is_access_granted('broker', 'view') ||
                                            $this->session->userdata('access_controller')->is_access_granted('company', 'view') ||
                                            $this->session->userdata('access_controller')->is_access_granted('employee', 'view') ||
                                            $this->session->userdata('access_controller')->is_access_granted('location', 'view') ||
                                            $this->session->userdata('access_controller')->is_access_granted('product', 'view') ||
                                            $this->session->userdata('access_controller')->is_access_granted('transporter', 'view')) {
                                        ?>

                                        <!-- Forms -->
                                        <li class="navigation-header"><span>Master</span> <i class="icon-menu" title="Master"></i></li>

                                        <li>

                                            <a href="javascript: void(0);">
                                                <i class="icon-pencil3"><!-- --></i>
                                                Masters
                                            </a>

                                            <ul>
                                                <? 
                                                if ($this->session->userdata('access_controller')->is_access_granted('broker', 'view')) {
                                                    ?>
                                                    <li>
                                                        <a href="#!broker/all">
                                                            <i class="icon-man position-left"></i></i>Agent
                                                        </a>
                                                    </li>
                                                    <?
                                                }
                                                
                                                if ($this->session->userdata('access_controller')->is_access_granted('bank', 'view')) { ?>
                                                    <li>
                                                        <a href="#!bank/all">
                                                            <i class="fa fa-bank position-left"></i></i>Bank
                                                        </a>
                                                    </li>
                                                    <?
                                                }
                                                if ($this->session->userdata('access_controller')->is_access_granted('charge', 'view')) {
                                                    ?>
                                                    <li>
                                                        <a href="#!charge/all">
                                                            <i class="icon-cash4 position-left"></i></i>Charges
                                                        </a>
                                                    </li>
                                                    <?
                                                }
                                                if ($this->session->userdata('access_controller')->is_access_granted('company', 'view')) {
                                                    ?>
                                                    <li>
                                                        <a href="#!company/all">
                                                            <i class="icon-office position-left"></i></i>Company
                                                        </a>
                                                    </li>
                                                    <?
                                                }
                                                if ($this->session->userdata('access_controller')->is_access_granted('employee', 'view')) {
                                                    ?>
                                                    <li>
                                                        <a href="#!employee/all">
                                                            <i class="icon-user-tie position-left"></i></i>Employee
                                                        </a>
                                                    </li>
                                                    <?
                                                }
                                                if ($this->session->userdata('access_controller')->is_access_granted('location', 'view')) {
                                                    ?>
                                                    <li>
                                                        <a href="#!location/all">
                                                            <i class="icon-city position-left"></i></i>Location
                                                        </a>
                                                    </li>
                                                    <?
                                                }
                                                if ($this->session->userdata('access_controller')->is_access_granted('product', 'view')) {
                                                    ?>
                                                    <li>
                                                        <a href="#!product/all">
                                                            <i class="icon-cube3 position-left"></i></i>Product
                                                        </a>
                                                    </li>
                                                    <?
                                                }
                                                if ($this->session->userdata('access_controller')->is_access_granted('transporter', 'view')) {
                                                    ?>
                                                    <li>
                                                        <a href="#!transporter/all">
                                                            <i class="icon-truck position-left"></i></i>Transporter
                                                        </a>
                                                    </li>
                                                <? }
                                                ?>
                                            </ul>
                                        </li>
                                    <? } ?>

                                    <?
                                    if ($this->session->userdata('access_controller')->is_access_granted('advance_payment', 'view') ||
                                            $this->session->userdata('access_controller')->is_access_granted('purchase_order', 'view') ||
                                            $this->session->userdata('access_controller')->is_access_granted('purchase', 'view') ||
                                            $this->session->userdata('access_controller')->is_access_granted('debit_note', 'view') ||
                                            $this->session->userdata('access_controller')->is_access_granted('payment', 'view') ||
                                            $this->session->userdata('access_controller')->is_access_granted('expense', 'view')) {
                                        ?>
                                        <li class="navigation-header"><span>Sales & Purchase</span> <i class="icon-menu" title="Sales & Purchase"></i></li>
                                        <li>
                                            <a href="javascript: void(0);">
                                                <i class=" icon-cart2"><!-- --></i>
                                                Purchase
                                            </a>
                                            <ul>
                                                
                                                <? 
                                                
                                                if ($this->session->userdata('access_controller')->is_access_granted('advance_payment', 'view')) {
                                                    ?>
                                                    <li>
                                                        <a href="#!AdvancePayment/all">
                                                            <i class=" icon-credit-card2 position-left"></i></i>Advance Payment
                                                        </a>
                                                    </li>
                                                    <?
                                                }
                                                
                                                if ($this->session->userdata('access_controller')->is_access_granted('purchase_order', 'view')) { ?>
                                                    <li>
                                                        <a href="#!PurchaseOrder/all">
                                                            <i class="glyphicon glyphicon-list-alt  position-left"></i></i>Purchase Order
                                                        </a>
                                                    </li>
                                                    <?
                                                }
                                                if ($this->session->userdata('access_controller')->is_access_granted('purchase', 'view')) { ?>
                                                    <li>
                                                        <a href="#!purchase/all">
                                                            <i class="glyphicon glyphicon-list-alt  position-left"></i></i>Purchase Invoice
                                                        </a>
                                                    </li>
                                                    <?
                                                }
                                                if ($this->session->userdata('access_controller')->is_access_granted('debit_note', 'view')) {
                                                    ?>
                                                    <li>
                                                        <a href="#!DebitNote/all">
                                                            <i class=" icon-credit-card position-left"></i></i>Debit Note
                                                        </a>
                                                    </li>
                                                    <?
                                                }
                                                if ($this->session->userdata('access_controller')->is_access_granted('payment', 'view')) {
                                                    ?>
                                                    <li>
                                                        <a href="#!Payment/all">
                                                            <i class=" icon-credit-card2 position-left"></i></i>Payment
                                                        </a>
                                                    </li>
                                                    <?
                                                }
                                                if ($this->session->userdata('access_controller')->is_access_granted('expense', 'view')) {
                                                    ?>
                                                    <li>
                                                        <a href="#!Expense/all">
                                                            <i class="icon-cash4 position-left"></i></i>Expense
                                                        </a>
                                                    </li>
                                                <? } ?>
                                            </ul>
                                        </li>
                                    <? } ?>


                                    <?
                                    if ($this->session->userdata('access_controller')->is_access_granted('advance_receipt', 'view') ||
                                            $this->session->userdata('access_controller')->is_access_granted('quotation', 'view') ||
                                            $this->session->userdata('access_controller')->is_access_granted('pro_forma', 'view') ||
                                            $this->session->userdata('access_controller')->is_access_granted('invoice', 'view') ||
                                            $this->session->userdata('access_controller')->is_access_granted('credit_note', 'view') ||
                                            $this->session->userdata('access_controller')->is_access_granted('receipt', 'view')) {
                                        ?>

                                        <li>
                                            <a href="javascript: void(0);">
                                                &nbsp;<i class="fa fa-inr" style = "font-size:19px!important"><!-- --></i>
                                                Sales
                                            </a>
                                            <ul>
                                                
                                                <? 
                                                if ($this->session->userdata('access_controller')->is_access_granted('advance_receipt', 'view')) {
                                                    ?>
                                                    <li>
                                                        <a href="#!AdvanceReceipt/all">
                                                            <i class="icon-credit-card2 position-left"></i></i>Advance Receipt
                                                        </a>
                                                    </li>
                                                    <?
                                                }
                                                
                                                if ($this->session->userdata('access_controller')->is_access_granted('quotation', 'view')) { ?>
                                                    <li>
                                                        <a href="#!quotation/all">
                                                            <i class="glyphicon glyphicon-list-alt  position-left"></i></i>Quotation
                                                        </a>
                                                    </li>
                                                    <?
                                                } if ($this->session->userdata('access_controller')->is_access_granted('pro_forma', 'view')) { ?>
                                                    <li>
                                                        <a href="#!ProForma/all">
                                                            <i class="glyphicon glyphicon-list-alt  position-left"></i></i>Pro-Forma Invoice
                                                        </a>
                                                    </li>
                                                    <?
                                                }
                                                if ($this->session->userdata('access_controller')->is_access_granted('invoice', 'view')) { ?>
                                                    <li>
                                                        <a href="#!invoice/all">
                                                            <i class="glyphicon glyphicon-list-alt  position-left"></i></i>Sales Invoice
                                                        </a>
                                                    </li>
                                                    <?
                                                }
                                                if ($this->session->userdata('access_controller')->is_access_granted('credit_note', 'view')) {
                                                    ?>
                                                    <li>
                                                        <a href="#!CreditNote/all">
                                                            <i class=" icon-credit-card position-left"></i></i>Credit Note
                                                        </a>
                                                    </li>
                                                    <?
                                                }
                                                if ($this->session->userdata('access_controller')->is_access_granted('receipt', 'view')) {
                                                    ?>
                                                    <li>
                                                        <a href="#!Receipt/all">
                                                            <i class=" icon-credit-card2 position-left"></i></i>Receipt
                                                        </a>
                                                    </li>	
                                                <? } ?>
                                            </ul>
                                        </li>
                                    <? } ?>

                                    <?
                                    if ($this->session->userdata('access_controller')->is_access_granted('inventory_overview', 'view') ||
                                            $this->session->userdata('access_controller')->is_access_granted('inventory_adjustment', 'view')) {
                                        ?>
                                        <li>
                                            <a href="javascript: void(0);">
                                                <i class="icon-stack2"></i>
                                                Inventory
                                            </a> 
                                            <ul>
                                                <? if ($this->session->userdata('access_controller')->is_access_granted('inventory_overview', 'view')) { ?>
                                                    <li>
                                                        <a href="#!inventory/overview">
                                                            <i class="glyphicon glyphicon-list-alt  position-left"></i></i>Overview
                                                        </a>
                                                    </li>
                                                    <?
                                                }
                                                if ($this->session->userdata('access_controller')->is_access_granted('inventory_adjustment', 'view')) {
                                                    ?>
                                                    <li>
                                                        <a href="#!inventory/adjustment">
                                                            <i class="glyphicon glyphicon-resize-vertical position-left"></i></i>Adjustment
                                                        </a>
                                                    </li>
                                                <? } ?>
                                            </ul>
                                        </li>
                                    <? } ?>
                                        
                                        <?
                                    if ($this->session->userdata('access_controller')->is_access_granted('recipe', 'view') ||
                                            $this->session->userdata('access_controller')->is_access_granted('manufacturing_order', 'view')) {
                                        ?>
                                        <li class="navigation-header"><span>Production</span> <i class="icon-menu" title="Manufacturing"></i></li>
                                        <li>
                                            <a href="javascript: void(0);">
                                                <i class="fa fa-gavel"></i>
                                                Manufacturing
                                            </a> 
                                            <ul>
                                                <? if ($this->session->userdata('access_controller')->is_access_granted('recipe', 'view')) { ?>
                                                    <li>
                                                        <a href="#!recipe/all">
                                                            <i class="glyphicon glyphicon-list-alt  position-left"></i></i>Bill Of Material (Recipe)
                                                        </a>
                                                    </li>
                                                    <?
                                                }
                                                if ($this->session->userdata('access_controller')->is_access_granted('manufacturing_order', 'view')) {
                                                    ?>
                                                    <li>
                                                        <a href="#!ManufacturingOrder/all">
                                                            <i class="fa fa-gears position-left"></i></i>Manufacturing Order
                                                        </a>
                                                    </li>
                                                <? } ?>
                                            </ul>
                                        </li>
                                    <? } ?>

                                    <?
                                    if ($this->session->userdata('access_controller')->is_access_granted('chart_of_accounts', 'view') ||
                                            $this->session->userdata('access_controller')->is_access_granted('journal', 'view') ||
                                            $this->session->userdata('access_controller')->is_access_granted('system_transactions', 'view') ||
                                            $this->session->userdata('access_controller')->is_access_granted('ledger', 'view') ||
                                            $this->session->userdata('access_controller')->is_access_granted('trial_balance', 'view') ||
                                            $this->session->userdata('access_controller')->is_access_granted('profit_and_loss', 'view') ||
                                            $this->session->userdata('access_controller')->is_access_granted('balance_sheet', 'view') ||
                                            $this->session->userdata('access_controller')->is_access_granted('cash_flow', 'view')) {
                                        ?>
                                        <li class="navigation-header"><span>Accounts & Reports</span> <i class="icon-menu" title="Accounts & Reports"></i></li>
                                        <li>
                                            <a href="javascript: void(0);">
                                                <i class="icon-calculator2"></i>
                                                Accounts <span class="label bg-blue-400" style = "font-weight: 600"> βeta </span>
                                                <!--<span style="color:#29B6F6">( βeta )</span>-->
                                            </a> 
                                            <ul>
                                                <? if ($this->session->userdata('access_controller')->is_access_granted('chart_of_accounts', 'view')) { ?>
                                                    <li>
                                                        <a href="#!COA/all">
                                                            <i class="icon-book3  position-left"></i></i>Chart Of Accounts
                                                        </a>
                                                    </li>
                                                    <?
                                                }
                                                if ($this->session->userdata('access_controller')->is_access_granted('journal', 'view')) {
                                                    ?>
                                                    <li>
                                                        <a href="#!journal/all">
                                                            <i class="icon-book2  position-left"></i></i>Journal
                                                        </a>
                                                    </li>
                                                    <?
                                                }
                                                if ($this->session->userdata('access_controller')->is_access_granted('ledger', 'view')) {
                                                    ?>
                                                    <li>
                                                        <a href="#!SJ/ledger">
                                                            <i class="icon-books  position-left"></i></i>Ledger
                                                        </a>
                                                    </li>
                                                    <?
                                                }
                                                if ($this->session->userdata('access_controller')->is_access_granted('trial_balance', 'view')) {
                                                    ?>
                                                    <li>
                                                        <a href="#!SJ/trial_balance">
                                                            <i class="icon-checkmark  position-left"></i></i>Trial Balance
                                                        </a>
                                                    </li>
                                                    <?
                                                }
                                                if ($this->session->userdata('access_controller')->is_access_granted('system_transactions', 'view')) {
                                                    ?>
                                                    <li>
                                                        <a href="#!SJ/transactions">
                                                            <i class="icon-calculator3  position-left"></i></i>System Transactions
                                                        </a>
                                                    </li>
                                                    <?
                                                }
                                                if ($this->session->userdata('access_controller')->is_access_granted('profit_and_loss', 'view')) {
                                                    ?>
                                                    <li>
                                                        <a href="#!SJ/pnl">
                                                            <i class="icon-table  position-left"></i></i>Profit & Loss Statement
                                                        </a>
                                                    </li>
                                                    <?
                                                }
                                                if ($this->session->userdata('access_controller')->is_access_granted('balance_sheet', 'view')) {
                                                    ?>
                                                    <li>
                                                        <a href="#!SJ/bs">
                                                            <i class="icon-table2  position-left"></i></i>Balance Sheet
                                                        </a>
                                                    </li>
                                                    <?
                                                }
                                                if ($this->session->userdata('access_controller')->is_access_granted('cash_flow', 'view')) {
                                                    ?>
                                                    <!--<li>
                                                        <a href="#!SJ/cfs">
                                                            <i class="icon-cash4  position-left"></i></i>Cash Flow Statement
                                                        </a>
                                                    </li>-->
                                                <? } ?>
                                            </ul>
                                        </li>
                                    <? } ?>
                                    <? if ($this->session->userdata('access_controller')->is_access_granted('reports', 'view')) { ?>

                                        <li>
                                            <a href="javascript: void(0);">
                                                <i class="icon-stack4"><!-- --></i>
                                                Reports
                                            </a>
                                            <ul>
                                                <li>
                                                    <a href="#!report/all">
                                                        <i class="icon-stack-text  position-left"></i></i>Business Overview
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#!report/gst">
                                                        <i class="fa fa-google position-left"></i></i>GST <span class="label bg-blue-400" style = "font-weight: 600"> βeta </span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                    <? } ?>
                                    <!-- /page kits -->
                                    <li><a href="#!settings/changelog"><i class="icon-list-unordered"></i> <span>Version Tracker <span class="label bg-success-400">1.3</span></span></a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- /main navigation -->
                        <div ng-controller = "homePageController"ng-init="load()"></div>
                    </div>
                </div>
                <!-- /main sidebar -->

                <script>
                    var app = angular.module("merp", ["ngRoute"]);
                    app.config(function ($routeProvider) {
                        $routeProvider
                                .when("/:controller/:view", {
                                    templateUrl: function (params) {
                                        return '/index.php/web/' + params.controller + '/' + params.view;
                                    }
                                })
                                .when("/:controller/:view/:entity_id", {
                                    templateUrl: function (params) {
                                        return '/index.php/web/' + params.controller + '/' + params.view + '/' + params.entity_id;
                                    }
                                })
                                .when("/:controller/:view/:entity_id/:sub_view/:sub_entity_id", {
                                    templateUrl: function (params) {
                                        return '/index.php/web/' + params.controller + '/' + params.view + '/' + params.entity_id + '/' + params.sub_view + '/' + params.sub_entity_id;
                                    }
                                })
                                .when("/:controller/:subcontroller/:view/:entity_id", {
                                    templateUrl: function (params) {
                                        return '/index.php/web/' + params.controller + '/' + params.subcontroller + '/' + params.view + '/' + params.entity_id;
                                    }
                                })
                    });
                    app.controller("homePageController", function ($scope, $window, $templateCache) {
                        $scope.load = function () {
                            <? if($this->session->flashdata('error_notification')!=NULL){
                                echo 'showMessage("warning", "Subscription About To End", "'.$this->session->flashdata('error_notification').'")';
                            } ?>
                            
                            $templateCache.removeAll();
                        }
                    });
                    
                    app.controller("formController", function ($scope, $window, $templateCache) {
                        $scope.load = function (id) {
                            initializeFormElements(id);
                            initialize_uploader();
                            refreshLocationDistrictAndState();
                            refreshSecondaryLocationDistrictAndState();
                            $templateCache.removeAll();
                        };

                    });
                    app.controller("creditNoteController", function ($scope, $window, $templateCache) {
                        $scope.load = function () {
                            initialize_credit_note_form();
                            $templateCache.removeAll();
                        }
                    });
                    app.controller("debitNoteController", function ($scope, $window, $templateCache) {
                        $scope.load = function () {
                            initialize_debit_note_form();
                            $templateCache.removeAll();
                        }
                    });
                    app.controller("invoiceController", function ($scope, $window, $templateCache) {
                        $scope.load = function () {
                            initialize_invoice_form();
                            initializeMultiSelect();
                            initializeColoredRadio();
                            $templateCache.removeAll();
                        }
                    });
                    app.controller("proFormaController", function ($scope, $window, $templateCache) {
                        $scope.load = function () {
                            initialize_pro_forma_form();
                            $templateCache.removeAll();
                        }
                    });
                    app.controller("purchaseController", function ($scope, $window, $templateCache) {
                        $scope.load = function () {
                            initialize_purchase_form();
                            initializeMultiSelect();
                            initialize_uploader();
                            $templateCache.removeAll();
                        }
                    });
                    app.controller("purchaseOrderController", function ($scope, $window, $templateCache) {
                        $scope.load = function () {
                            initialize_purchase_order_form();
                            $templateCache.removeAll();
                        }
                    });
                    app.controller("quotationController", function ($scope, $window, $templateCache) {
                        $scope.load = function () {
                            initialize_quotation_form();
                            initialize_uploader();
                            $templateCache.removeAll();
                        }
                    });
                    app.controller("recipeController", function ($scope, $window, $templateCache) {
                        $scope.load = function () {
                            initialize_recipe_form();
                            $templateCache.removeAll();
                        }
                    });
                    app.controller("manufacturingOrderController", function ($scope, $window, $templateCache) {
                        $scope.load = function (id) {
                            $templateCache.removeAll();
                            initialize_manufacturing_order_form(id);
                        }
                    });
                    app.controller("receiptController", function ($scope, $window, $templateCache) {
                        $scope.load = function () {
                            initializeFormElements();
                            initializeReceiptForm();
                            $templateCache.removeAll();
                        }
                    });
                    
                    app.controller("paymentController", function ($scope, $window, $templateCache) {
                        $scope.load = function () {
                            initializeFormElements();
                            initializePaymentForm();
                            $templateCache.removeAll();
                        }
                    });
                    app.controller("advancePaymentController", function ($scope, $window, $templateCache) {
                        $scope.load = function () {
                            initializeFormElements();
                            initializeAdvancePaymentForm();
                            $templateCache.removeAll();
                        }
                    });
                    app.controller("advanceReceiptController", function ($scope, $window, $templateCache) {
                        $scope.load = function () {
                            initializeFormElements();
                            initializeAdvanceReceiptForm();
                            $templateCache.removeAll();
                        }
                    });
                    app.controller("dataTableController", function ($scope, $window, $templateCache) {
                        $scope.load = function (columnId) {
                            initialize_datatable(columnId);
                            initialize_datatable_no_grouping();
                            $templateCache.removeAll();
                        }
                    });
                    app.controller("reportController", function ($scope, $window, $templateCache) {
                        $scope.load = function (columnId) {
                            initialize_datatable_no_grouping();
                            $templateCache.removeAll();
                            initializeReportPage();
                        }
                    });
                    app.controller("gstReportController", function ($scope, $window, $templateCache) {
                        $scope.load = function () {
                            initializeGstReportPage();
                            $templateCache.removeAll();
                        }
                    });
                    app.controller("invoiceReportController", function ($scope, $window, $templateCache) {
                        $scope.load = function (columnId) {
                            initialize_datatable_no_grouping();
                            $templateCache.removeAll();
                            initializeInvoiceReportPage();
                        }
                    });
                    app.controller("noGroupDataTableController", function ($scope, $window, $templateCache) {
                        $scope.load = function () {
                            initialize_datatable_no_grouping();
                            $templateCache.removeAll();
                        }
                    });
                    app.controller("appSettingsController", function ($scope, $window, $templateCache) {
                        $scope.load = function () {
                            initialize_settings_form();
                            initializeFormElements();
                            initialize_uploader();
                            $templateCache.removeAll();
                        }
                    });
                    app.controller("inventory_adjustment", function ($scope, $window, $templateCache) {
                        $scope.load = function () {
                            initializeFormElements();
                            setupInventoryAdjustmentFormBehaviour();
                            $templateCache.removeAll();
                        }
                    });
                    app.controller("expenseFormController", function ($scope, $window, $templateCache) {
                        $scope.load = function () {
                            initializeFormElements();
                            initialize_expense_form();
                            initialize_uploader();
                            $templateCache.removeAll();
                        }
                    });
                    app.controller("journalController", function ($scope, $window, $templateCache) {
                        $scope.load = function () {
                            initializeJournalDataTable();
                            $templateCache.removeAll();
                        }
                    });
                    app.controller("coaObController", function ($scope, $window, $templateCache) {
                        $scope.load = function (coaArraySize) {
                            initializeCoaObForm();
                            disableSubmitIfNotBalanced(coaArraySize);
                            highLightOpeningBalanceAdjustment();
                            $templateCache.removeAll();
                        }
                    });
                    app.controller("pobController", function ($scope, $window, $templateCache) {
                        $scope.load = function () {
                            initializePobForm();
                            $templateCache.removeAll();
                        }
                    });
                    app.controller("dashboardController", function ($scope, $window, $templateCache) {
                        $scope.load = function () {
                            initializeCharts();
                            $templateCache.removeAll();
                        }
                    });
                </script>

                <p><a href="#/!"></a></p>


                <!-- Main content -->
                <div ng-view class="content-wrapper" style = "padding-top:40px">

                    

                </div>
                <!-- /content area -->
            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->





    <!-- Basic location modal  -->
    <div id="modal_default" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Add New Location</h5>
                </div>
                <?php
                //$attributes = array('class' => 'form-horizontal modal-form-validate-jquery', 'target' => 'votar', 'onsubmit' => 'return refreshLocationList();');
                $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id'=>'merpForm');
                echo form_open('web/location/save/', $attributes);
                ?>
                <div class="modal-body">

                    <fieldset class="content-group">
                        <legend class="text-bold">Basic inputs</legend>
                        <div class="form-group">
                            <label class="control-label col-lg-3">City Name <span class="text-danger">*</span></label>
                            <div class="col-lg-9">
                                <input class="form-control" tabindex="1001" name="city_name" id = "modal_city_name" type="text" required="required" placeholder="Enter City Name">
                            </div>
                        </div>							
                        <div class="form-group">
                            <label class="control-label col-lg-3">District</label>
                            <div class="col-lg-9">
                                <input class="form-control" id = "modal_district" placeholder="Example - Raigad, Thane" tabindex="1002" name="district" type="text">
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="content-group">
                        <legend class="text-bold">Additional Data</legend>
                        <div class="form-group">
                            <label class="control-label col-lg-3">State<span class="text-danger">*</span></label>
                            <div class="col-lg-9">
                                <select required class="select2 form-control" name="state_id" id = "modal_state" tabindex="1003">
                                    <option value = "">Choose a state</option>
                                    <optgroup label="States">
                                        <?php
                                        foreach ($states as $state) {
                                            ?>
                                            <option 
                                            <?php
                                            if (set_value('state_id') == $state['state_tin_number'])
                                                echo "selected";
                                            else if (isset($location)) {
                                                if ($location['state_id'] == $state['state_tin_number'])
                                                    echo "selected";
                                            }
                                            ?> value="<? echo $state['state_tin_number']; ?>"><? echo $state['state_name'] ?>
                                            </option>
                                            <?php
                                        }
                                        ?>
                                    </optgroup>
                                    <optgroup label = "Union Territories">
                                        <?php
                                        foreach ($union_territories as $union_territory) {
                                            ?>
                                            <option 
                                            <?php
                                            if (set_value('state_id') == $union_territory['state_tin_number'])
                                                echo "selected";
                                            else if (isset($location)) {
                                                if ($location['state_id'] == $union_territory['state_tin_number'])
                                                    echo "selected";
                                            }
                                            ?> value="<? echo $union_territory['state_tin_number']; ?>"><? echo $union_territory['state_name'] ?>
                                            </option>
                                            <?php
                                        }
                                        ?>

                                    </optgroup>
                                </select>
                            </div>
                        </div>

                    </fieldset>

                </div>

                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button>
                    <button onclick="submitForm('location');refreshLocationList();" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /basic modal -->

</body>
</html>