

<!-- Page header -->
<div class="page-header">
    <div ng-controller="formController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/employee/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Master</span> - Employee</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!/employee/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!"><i class="icon-home2 position-left"></i> Master</a></li>
            <li><a href="#!/employee/all"><i class="icon-user-tie position-left"></i> Employee</a></li>
            <li class="active"><?php
                if (isset($employee))
                    echo 'EMP' . str_pad($employee['employee_id'], 5, "0", STR_PAD_LEFT);
                else
                    echo "New";
                ?></li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Form validation -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Employee</h5>
            <div class="heading-elements">

            </div>
        </div>


        <div class="panel-body">
            <p class="content-group-lg">An Employee is a person employed with the company who can be provided with system access. Employee details entered here will be linked to Sales and Reports.</p>
            <?php echo validation_errors(); ?>
            <?php
            $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id' => 'merpForm');
            if (isset($employee))
                echo form_open_multipart('web/employee/save/' . $employee['employee_id'], $attributes);
            else
                echo form_open_multipart('web/employee/save', $attributes);
            ?>
            <fieldset class="content-group">
                <legend class="text-bold">Basic details</legend>	
                <div class="col-lg-12">							
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="employee_name">Employee Name <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" required name="employee_name" style="text-transform: capitalize;" type="text" value = "<?php if (isset($employee)) echo $employee['employee_name']; ?>" tabindex = "1" placeholder="Enter Employee Name">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="col-lg-3 control-label">Image</label>
                        <div class="col-lg-9">
                            <input type="file" name = "employee_image_path" id = "image_path" 
                                   class="file-input-preview" data-show-remove="false"  data-dismiss="fileupload" data-show-upload="false" 
                                   data-image-path = "<?php if (isset($employee)) echo $employee['employee_image_path'] ?>" 
                                   data-image-name = "<?php if (isset($employee)) echo $employee['employee_name'] ?>">
                            <span class="help-block">Maximum file upload size is <code>100 KB.</code> </span>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group col-lg-6">
                            <label class="control-label col-lg-3" for="employee_address">Address</label>
                            <div class="col-lg-9">
                                <input class="form-control" name="employee_address" type="text" value = "<?php if (isset($employee)) echo $employee['employee_address']; ?>" tabindex = "2" placeholder="Enter address">
                            </div>
                        </div>


                        <div class="form-group col-lg-6">
                            <label class="control-label col-lg-3" for="employee_area">Area</label>
                            <div class="col-lg-9">
                                <input class="form-control" name="employee_area" type="text" style="text-transform: capitalize;" value = "<?php if (isset($employee)) echo $employee['employee_area']; ?>" tabindex = "3" placeholder = "Enter area">
                            </div>
                        </div>					
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group col-lg-6">
                            <label class="control-label col-lg-3" for="employee_location_id">City <span class="text-danger">*</span></label>
                            <div class="col-lg-9">
                                <select required class="form-control select2" name="employee_location_id" id="city" tabindex = "4" onChange = "refreshLocationDistrictAndState()" >
                                            <option>Choose a location</option>
                                            <?php
                                            foreach ($locations as $location) {
                                                ?>
                                                <option district = "<?php echo $location['district'] ?>" state = "<?php echo $location['state_name'] ?>" 
                                                <?php
                                                if (set_value('employee_location_id') == $location['location_id'])
                                                    echo "selected";
                                                else if (isset($employee)) {
                                                    if ($employee['employee_location_id'] == $location['location_id'])
                                                        echo "selected";
                                                }
                                                ?> value="<? echo $location['location_id']; ?>"><? echo $location['city_name'] ?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                            </div>
                        </div>	
                        <div class="form-group col-lg-6">
                            <label class="control-label col-lg-3" for="district">District</label>
                            <div class="col-lg-9">
                                <input class="form-control" id="district" type="text" readonly>
                            </div>
                        </div>							
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group col-lg-6">
                            <label class="control-label col-lg-3" for="state">State</label>
                            <div class="col-lg-9">
                                <input class="form-control" id="state" type="text" readonly>
                            </div>
                        </div>
                        <div class="form-group col-lg-6">
                            <label class="control-label col-lg-3" for="employee_pincode">Pincode </label>
                            <div class="col-lg-9">
                                <input class="form-control" name="employee_pincode" type="text" value = "<?php if (isset($employee)) echo $employee['employee_pincode']; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' tabindex = "5">
                            </div>
                        </div>	

                    </div>
                    <div class="col-lg-12">
                        <div class="form-group col-lg-6">
                            <label class="control-label col-lg-3" for="employee_department">Department</label>
                            <div class="col-lg-9">
                                <input class="form-control" name="employee_department" type="text" value = "<?php if (isset($employee)) echo $employee['employee_department']; ?>" tabindex = "6" placeholder = "">
                            </div>
                        </div>		


                        <div class="form-group col-lg-6">
                            <label class="control-label col-lg-3" for="employee_designation">Designation</label>
                            <div class="col-lg-9">
                                <input class="form-control" name="employee_designation" type="text" value = "<?php if (isset($employee)) echo $employee['employee_designation']; ?>" tabindex = "7" placeholder = "">
                            </div>
                        </div>	
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group col-lg-6">
                            <label class="control-label col-lg-3" for="employee_email_address">Email Address</label><span id = "emailAddressError" style = "color:red; display:none;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Please check email address format !</span>
                            <div class="col-lg-9">
                                <input class="form-control" name="employee_email_address" type="text" value = "<?php if (isset($employee)) echo $employee['employee_email_address']; ?>" tabindex = "8">
                            </div>
                        </div>

                        <div class="form-group col-lg-6">
                            <label class="control-label col-lg-3" for="employee_contact_number">Contact Number </label>
                            <div class="col-lg-9">
                                <input class="form-control" name="employee_contact_number" type="text" value = "<?php if (isset($employee)) echo $employee['employee_contact_number']; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' tabindex = "9">
                            </div>
                        </div>	
                    </div>
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Banking & Compliance details</legend>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="employee_pan_number">PAN Number</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="employee_pan_number" type="text" value = "<?php if (isset($employee)) echo $employee['employee_pan_number']; ?>" tabindex = "10" placeholder = "Example - BANPP438DC">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="employee_esic_number">ESIC Number</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="employee_esic_number" type="text" value = "<?php if (isset($employee)) echo $employee['employee_esic_number']; ?>" tabindex = "11" placeholder = "">
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <!--<div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="employee_bank_id">Bank Name</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="employee_bank_id" type="text" value = "<?php if (isset($employee)) echo $employee['employee_bank_id']; ?>" tabindex = "12" placeholder = "Example - ICICI Bank, IDBI Bank">
                        </div>
                    </div>-->
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="employee_account_number">Account Number</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="employee_account_number" type="text" value = "<?php if (isset($employee)) echo $employee['employee_account_number']; ?>" tabindex = "13" placeholder = "">
                        </div>
                    </div>	
                </div>
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Application Credentials</legend>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="employee_username">Username</label>
                        <div class="col-lg-9">
                            <input class="form-control" 
                                   onblur ="checkIfUnique(this, 'employee', 'employee_username', '<? if (isset($employee)) echo $employee['employee_id']; ?>', 'employee_id')"
                                   name="employee_username" type="text" value = "<?php if (isset($employee)) echo $employee['employee_username']; ?>" tabindex = "14" placeholder = "">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="employee_password">Password</label>
                        <div class="input-group col-lg-9">
                            <input class="form-control" name="employee_password" id = "employee_password" type="password" value = "<?php if (isset($employee)) echo $employee['employee_password']; ?>" tabindex = "15" placeholder = "Enter a password">
                            <span class="input-group-addon"><i class="fontawesome icon-eye-blocked2"></i></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="employee_password_repeat">Re-enter Password</label>
                        <div class="col-lg-9">
                            <input class="form-control" name = "employee_password_repeat" type="password" value = "<?php if (isset($employee)) echo $employee['employee_password']; ?>" tabindex = "16" placeholder = "Re-enter password entered above">
                        </div>
                    </div>
                </div>
            </fieldset>

            <div class="text-right">
                <a href="#!/employee/all"><button class="btn btn-default" tabindex = "17">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                <? if ($this->session->userdata('access_controller')->is_access_granted('employee', 'save')) { ?>
                    <button onclick="submitForm('employee');" id ="buttonSubmit" class="btn btn-primary" tabindex = "19">Save<i class="icon-arrow-right14 position-right"></i></button>
                <? } ?>
            </div>
            </form>

        </div>

        </section>
    </div>	
    <!-- /form validation -->
    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->
