

<!-- Page header -->
<div class="page-header">

    <div ng-controller="dataTableController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Master</span> - Transporter</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!transporter/view" class="btn btn-link btn-float has-text"><i class="icon-truck text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Add Transporter</span></a>
                <a href="report/download/transporter" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-home2 position-left"></i>Master</li>
            <li class="active"><i class="icon-truck position-left"></i>Transporter</li>
        </ul>
    </div>
</div>
<!-- /page header -->



<!-- Content area -->
<div class="content">

    <!-- Invoice archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Transporter</h6>
            <div class="heading-elements">
            </div>
        </div>

        <table class="table masterDataTable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Transporter Name</th>
                    <th>Contact Person</th>
                    <th>City</th>
                    <th>Contact Number</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($transporters as $transporter): ?>
                    <tr>
                        <td>TRN<?php echo str_pad($transporter['transporter_id'], 5, "0", STR_PAD_LEFT); ?></td>
                        <td>
                            <h6 class="no-margin">
                                <a href="#!transporter/view/<?php echo $transporter['transporter_id']; ?>"><span><?php echo $transporter['transporter_name']; ?></span></a>
                                <small class="display-block text-muted">Created by <?php echo $transporter['employee_name']; ?> 
                                    on <?php echo $transporter['transporter_record_creation_time']; ?></small>
                            </h6>
                        </td>
                        <td><?php echo $transporter['transporter_contact_person_name']; ?></td>
                        <td><?php echo $transporter['city_name']; ?></td>
                        <td><?php echo $transporter['transporter_contact_number']; ?></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li><a href="#!transporter/view/<?php echo $transporter['transporter_id']; ?>"><i class="icon-file-eye"></i></a></li>
                                <? 
                                if($this->session->userdata('access_controller')->is_access_granted('transporter', 'delete')) { ?>
                                <li><a href='javascript: void(0)' onclick = 'showDeleteDialog("transporter", <?php echo $transporter['transporter_id']; ?>)'><i class="glyphicon glyphicon-trash"></i></a></li>
                                <? } ?>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td>ID</td>
                    <td>Transporter Name</td>
                    <td>City</td>
                    <td>Contact Person</td>
                    <td>Contact Number</td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->