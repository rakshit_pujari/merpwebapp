

<!-- Page header -->
<div class="page-header">

    <div ng-controller="noGroupDataTableController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Accounting</span> - Trial Balance</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-calculator2 position-left"></i>Accounting</li>
            <li class="active"><i class="icon-checkmark position-left"></i>Trial Balance</li>
        </ul>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Ledgers archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Trial Balance</h6>
            <div class="heading-elements">
                </ul>
            </div>
        </div>

        <table class="table noGroupDataTable">
            <thead>
                <tr>
                    <th></th>
                    <th>Account Name</th>
                    <th>Account Type</th>
                    <th>Debit</th>
                    <th>Credit</th>
                </tr>
            </thead>
            <tbody>
                <?
                    $debit_total = 0;
                    $credit_total = 0;
                ?>
                <tr><td>Assets</td></tr>
                <?php 
                    foreach ($ledger_entries as $ledger_entry): 
                    if($ledger_entry['at_name'] == 'Current Assets' || $ledger_entry['at_name'] == 'Non Current Assets')
                        add_row($ledger_entry, $debit_total, $credit_total);
                    endforeach;
                ?>
                
                <tr><td>Liabilities</td></tr>
                <?php 
                    foreach ($ledger_entries as $ledger_entry): 
                    if($ledger_entry['at_name'] == 'Current Liabilities' || $ledger_entry['at_name'] == 'Non Current Liabilities'
                            || $ledger_entry['at_name'] == 'Equity')
                        add_row($ledger_entry, $debit_total, $credit_total);
                    endforeach;
                ?>
                
                <tr><td>Revenue</td></tr>
                <?php 
                    foreach ($ledger_entries as $ledger_entry): 
                    if($ledger_entry['at_name'] == 'Revenue')
                        add_row($ledger_entry, $debit_total, $credit_total);
                    endforeach;
                ?>
                
                <tr><td>Expense</td></tr>
                <?php 
                    foreach ($ledger_entries as $ledger_entry): 
                    if($ledger_entry['at_name'] == 'Expense')
                        add_row($ledger_entry, $debit_total, $credit_total);
                    endforeach;
                ?>
                
            </tbody>
            <tfoot>
                <tr>
                    <th></th>
                    <th></th>
                    <th>Total</th>
                    <th><span style="color:<? if(number_format($debit_total, 2) == number_format($credit_total, 2)) echo '#00AA00'; else echo '#FF0000';?>"><? echo '₹ '. number_format($debit_total, 2); ?></span></th>
                    <th><span style="color:<? if(number_format($debit_total, 2) == number_format($credit_total, 2)) echo '#00AA00'; else echo '#FF0000';?>"><? echo '₹ '. number_format($credit_total, 2); ?></span></th>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->
    
    <?
    
    function add_row($ledger_entry, &$debit_total, &$credit_total){
        if($ledger_entry['credit_balance'] > 0 || $ledger_entry['debit_balance'] > 0){
        ?>
    
                   <tr>
                    <td></td>
                    <td>
                        <a href="#!SJ/ledger/<?php echo $ledger_entry['sj_account_coa_id']; ?>"><span>
                        <?
                        echo $ledger_entry['coa_account_name'];
                        ?>
                        </span></a>
                        
                    </td>
                    <td>
                        <?
                        echo $ledger_entry['at_name'];
                        ?>
                        
                    </td>
                    <td>
                        <?
                        if($ledger_entry['debit_balance'] > $ledger_entry['credit_balance']){
                            $debit_total+=($ledger_entry['debit_balance'] - $ledger_entry['credit_balance']);
                            echo '₹ '. number_format(($ledger_entry['debit_balance'] - $ledger_entry['credit_balance']), 2);
                        }
                        ?>
                    </td>
                    <td>
                        <?
                        if($ledger_entry['debit_balance'] < $ledger_entry['credit_balance']){
                            $credit_total+=($ledger_entry['credit_balance'] - $ledger_entry['debit_balance']);
                            echo '₹ '. number_format(($ledger_entry['credit_balance'] - $ledger_entry['debit_balance']), 2);
                        }
                        ?>
                    </td>
                    </tr>
        <? } 
    }
    
    ?>