

<!-- Page header -->
<div class="page-header">

    <div ng-controller="noGroupDataTableController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Inventory</span> - Overview</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="report/download/inventory_overview" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-stack2 position-left"></i>Inventory</li>
            <li class="active"><i class="icon-stack2 position-left"></i>Overview</li>
        </ul>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Inventory archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Inventory</h6>
            <div class="heading-elements">
                </ul>
            </div>
        </div>

        <table class="table noGroupDataTable">
            <thead>
                <tr>
                    <th class = "text-center">Sr No.</th>
                    <th class = "text-center">Image</th>
                    <th class = "text-center">Product Name</th>
                    <th class = "text-center">Product Type</th>
                    <th class = "text-center">Rate Per Unit</th>
                    <th class = "text-center">Available In Stock</th>
                    <th class = "text-center">Work In Progress</th>
                    <th class = "text-center">Inventory Value</th>
                    <th class = "text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $count = 1;
                foreach ($inventory_overview as $inventory_row):
                    ?>
                    <tr>    
                        <td class = "text-center"><?php echo $count; ?></td>
                        <td class = "text-center"><img width ="100px" src="<?php echo $inventory_row['product_image_path']; ?>"><span></span></td>
                        <td class = "text-center"><a href = "/index.php/web/master#!/inventory/product/<?php echo $inventory_row['product_id']; ?>"><span><?php echo $inventory_row['product_name']; ?></span></a></td>
                        <td class = "text-center"><? 
                            if($inventory_row['product_source'] == 'tradeable' || $inventory_row['product_source'] == 'other'){
                                echo ucwords($inventory_row['product_source']);
                            } else if($inventory_row['product_source'] == 'manufactured'){
                                if(in_array('purchase', explode(",", $inventory_row['transaction_type'])))
                                    echo 'Raw Material';
                                
                                if(in_array('purchase', explode(",", $inventory_row['transaction_type'])) && in_array('sales', explode(",", $inventory_row['transaction_type']))){
                                    echo ', ';
                                }
                                
                                if(in_array('sales', explode(",", $inventory_row['transaction_type'])))
                                    echo 'Finished Good';
                            }
                        ?></td>
                        <td class = "text-center">₹ <?php echo number_format($inventory_row['rate_per_unit'], 2); ?></td>
                        <td class = "text-center"><?php
                        
                        if(isset($inventory_row['product_quantity'])){
                            if ($inventory_row['product_quantity'] > 0)
                                echo '<span class="label bg-success"><span>'.$inventory_row['product_quantity'].' '. $inventory_row['uqc_text'].'</span></span>';
                            else
                                echo '<span class="label bg-danger"><span>'.$inventory_row['product_quantity'].' '. $inventory_row['uqc_text'].'</span></span>';
                        }
                            ?></td>
                        <td class = "text-center"><? echo $inventory_row['work_in_progress']; ?></td>
                        <td class = "text-center">₹ <?php echo number_format($inventory_row['inventory_asset_value'], 2); ?></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li><a href="/index.php/web/master#!/inventory/product/<?php echo $inventory_row['product_id']; ?>"><i class="icon-file-eye"></i></a></li>
                            </ul>
                        </td>
                    </tr>
                    <?php
                    $count++;
                endforeach;
                ?>
            </tbody>
            <tfoot>
                <tr>
                    <th>Sr No.</th>
                    <th>Image</th>
                    <th>Product Name</th>
                    <th>Product Type</th>
                    <th>Rate Per Unit</th>
                    <th>Available In Stock</th>
                    <th>Work In Progress</th>
                    <th>Inventory Value</th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->