
<div ng-controller="receiptController" ng-init="load()"></div>
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/receipt/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Sales</span> - Receipt</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!/receipt/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!"><i class="icon-home2 position-left"></i> Sales</a></li>
            <li><a href="#!/receipt/all"><i class="icon-city position-left"></i> Receipt</a></li>
            <li class="active"><?php
                if (isset($receipt))
                    echo 'REC' . str_pad($receipt['receipt_id'], 5, "0", STR_PAD_LEFT);
                else
                    echo "New";
                ?></li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Form validation -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Receipt</h5>

        </div>


        <div class="panel-body">
            <?php
            $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id'=>'merpDocForm');
            if (isset($receipt))
                echo form_open('web/Receipt/save/' . $receipt['receipt_id'], $attributes);
            else
                echo form_open('web/Receipt/save', $attributes);
            ?>
            <p class="content-group-lg">Receipt Voucher is a confirmation document recording the payments received during the course of business. Add all payments here to ensure accurate Receipt status.</p>

            <fieldset class="content-group">
                <legend class="text-bold">Customer Details</legend>
                <div class="col-lg-12">

                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Customer Name <span class="text-danger">*</span></label>
                        <div class="col-lg-9">

                            <?
                            if (isset($receipt)) {
                                ?>

                                <input class="form-control" type="text"
                                       value = "<? echo $receipt['receipt_linked_company_invoice_name']; ?>" readonly>
                                   <? } else {
                                       ?>

                                <select class="select2 form-control" name = "receipt_linked_company_id" id = "receipt_linked_company_id" onchange = "refreshInvoiceListForReceipt();">
                                    <option value = "">Choose an company</option>
                                    <? foreach ($invoice_customers as $invoice_customer) {
                                        ?>
                                        <option value = "<? echo $invoice_customer['invoice_linked_company_id'] ?>"

                                                ><? echo $invoice_customer['invoice_linked_company_display_name'] ?></option>
                                                <?
                                            }
                                            ?>

                                <? }
                                ?>

                            </select>													
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Invoice Number <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <?
                            if (isset($receipt)) {
                                ?>

                                <input class="form-control" type="text"
                                       value = "INV<?php echo str_pad($receipt['receipt_linked_invoice_id'], 5, "0", STR_PAD_LEFT); ?>" readonly>
                                   <? } else {
                                       ?>
                                <select class="select2 form-control" name="receipt_linked_invoice_id" id = "receipt_linked_invoice_id" onchange = "updateReceiptBalanceAmount(this);" required>		
                                </select>
                            <? } ?>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="form-group col-lg-6  has-feedback has-feedback-left">
                        <label class="control-label col-lg-3">Invoice Amount</label>
                        <div class="col-lg-9">
                            <input class="form-control" id = "" tabindex="2" name="receipt_linked_invoice_amount" type="text"
                                   value = "<?php
                                   if (isset($receipt))
                                       echo $receipt['receipt_linked_invoice_amount'];
                                   else
                                       echo "0";
                                   ?>" required readonly>
                            <div class="form-control-feedback" style = "left:10px">
                                <span style = "font-size:16px;"> ₹</span>
                            </div>
                        </div>
                    </div>	
                    <div class="form-group col-lg-6 has-feedback has-feedback-left">
                        <label class="control-label col-lg-3">Amount Received <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" id = "" tabindex="2" required name="receipt_amount" type="number" placeholder = "Should be less that or equal to balance amount"
                                   value = "<?php if (isset($receipt)) echo $receipt['receipt_amount']; ?>" <?php if (isset($receipt)) echo "receipt_balance='" . ($invoice_customer_map[0]['receipt_balance'] + $receipt['receipt_amount']) . "'"; ?> required step = 0.01 onchange ="limitToReceiptBalance(this)"  
                                   >
                            <div class="form-control-feedback" style = "left:10px">
                                <span style = "font-size:16px;"> ₹</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Payment Mode <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="form-control select2" id = "receipt_payment_mode" name="receipt_payment_mode" tabindex="3" required>
                                <option value = "">Choose a payment mode</option>
                                <option value = "cash" <?php
                                if (isset($receipt))
                                    if ($receipt['receipt_payment_mode'] == "cash") {
                                        echo " selected";
                                    };
                                ?>>Cash</option>
                                <option value = "cheque" <?php
                                if (isset($receipt))
                                    if ($receipt['receipt_payment_mode'] == "cheque") {
                                        echo " selected";
                                    };
                                ?>>Cheque</option>
                                <option value = "bank transfer" <?php
                                if (isset($receipt))
                                    if ($receipt['receipt_payment_mode'] == "bank transfer") {
                                        echo " selected";
                                    };
                                ?>>Bank Transfer</option>
                                <option value = "bank deposit" <?php
                                if (isset($receipt))
                                    if ($receipt['receipt_payment_mode'] == "bank deposit") {
                                        echo " selected";
                                    };
                                ?>>Bank Deposit</option>
                                <option value = "demand draft" <?php
                                if (isset($receipt))
                                    if ($receipt['receipt_payment_mode'] == "demand draft") {
                                        echo " selected";
                                    };
                                ?>>Demand Draft</option>
                            </select>
                        </div>
                    </div>		
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Date</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name="receipt_date" id = "datePicker"
                                   value = "<?php
                                   if (isset($receipt))
                                       echo explode(" ", $receipt['receipt_date'])[0];
                                   else
                                       echo date('Y-m-d');
                                   ?>" invoice-date = "<?php
                                   if (isset($invoice_customer_map))
                                       echo explode(" ", $invoice_customer_map[0]['invoice_date'])[0];
                                   ?>">
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">To Account <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="select2 form-control" name = "receipt_debit_coa_id" required>
                                <option value = "">Choose an account</option>
                                <? foreach ($coas as $coa) {
                                ?> 
                                <option value = "<? echo $coa['coa_id'] ?>" <?php
                                   if (isset($receipt)){
                                       if ($receipt['receipt_debit_coa_id'] == $coa[
                                               'coa_id']) {
                                           echo " selected";
                                       }
                                   } else if (isset($receipt_setting)){
                                       if ($receipt_setting['debit_account_coa_id'] == $coa[
                                               'coa_id']) {
                                           echo "selected";
                                       }
                                   }
                                ?>><? echo $coa['coa_account_name'] ?></option>
                                <?
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Transaction Reference #</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name = "receipt_transaction_reference" type="text"
                                   value = "<?php if (isset($receipt)) echo $receipt['receipt_transaction_reference']; ?>">
                        </div>
                    </div>	
                </div>


            </fieldset>

            <div class="text-right">

                <a href="#!/receipt/all"><button class="btn btn-default">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                <? if ($this->session->userdata('access_controller')->is_access_granted('receipt', 'save')) { ?>
                <button onclick="submitDocForm('receipt', 'previewConfirm')" id ="buttonSubmitReceipt" class="btn btn-primary">Confirm <i class="icon-arrow-right14 position-right"></i></button>
                <? } ?>
            </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->