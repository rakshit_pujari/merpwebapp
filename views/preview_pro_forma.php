<?php

function getIndianCurrency($number) {
    $decimal = round($number - ($no = floor($number)), 2) * 100;
    $hundred = null;
    $digits_length = strlen($no);
    $i = 0;
    $str = array();
    $words = array(0 => '', 1 => 'one', 2 => 'two',
        3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
        7 => 'seven', 8 => 'eight', 9 => 'nine',
        10 => 'ten', 11 => 'eleven', 12 => 'twelve',
        13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
        16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
        19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
        40 => 'forty', 50 => 'fifty', 60 => 'sixty',
        70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
    $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
    while ($i < $digits_length) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number] . ' ' . $digits[$counter] . $plural . ' ' . $hundred : $words[floor($number / 10) * 10] . ' ' . $words[$number % 10] . ' ' . $digits[$counter] . $plural . ' ' . $hundred;
        } else
            $str[] = null;
    }
    $Rupees = implode('', array_reverse($str));
    //$paise = ($decimal) ? " and " . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    $paise = ($decimal) ? " and " . ($words[((int) ($decimal/10)) * 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    return ($Rupees ? $Rupees . ' ' : '') . $paise;
}
?>

<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/ProForma/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Sales</span> - Pro-Forma Invoice</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                    <!--<a href="#!/ProForma/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>-->
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!"><i class="icon-home2 position-left"></i>Sales</a></li>
            <li><a href="#!/ProForma/all"><i class="icon-city position-left"></i> Pro-Forma Invoice</a></li>
            <? if ($pro_forma['pro_forma_status'] == 'confirm') {?>
            <li class="active"><?php if (isset($pro_forma)) echo 'PRF' . str_pad($pro_forma['pro_forma_id'], 5, "0", STR_PAD_LEFT); ?></li>
            <? } else {?>
            <li class="active"><?php if (isset($pro_forma)) echo 'DRAFT'; ?></li>
            <? } ?>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <a href="#!/ProForma/all"><button class="btn btn-default">Back<i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>

    <a href="#!/ProForma/view/<? echo $pro_forma['pro_forma_status'].'/'.$pro_forma['pro_forma_id']; ?>"><button class="btn btn-default" 
                                                                                 <? if(isset($linked_documents)) if (sizeof($linked_documents) > 0) echo "disabled title = 'Cannot be edited because this pro forma is linked to other documents'" ?>>Edit <i class="icon-pencil3 position-right"></i></button></a>

    <button onClick="window.print();" class="btn btn-primary">Print <i class="icon-arrow-right14 position-right"></i></button>

    <!-- Form validation -->
    <div class="panel-flat">



        <div class="panel panel-body col-md-offset-3" id = "section-to-print"  style="width: 210mm;min-height: 297mm;padding-left:40px;padding-right:40px;">

            <span id = "proFormaHeader">Pro-Forma Invoice</span>
            <hr>

            <table class = "proFormaTable">
                <tr style="vertical-align: top">
                    <td class = "logo_container" colspan = "2" rowspan = "2	">
                        <img class = "logo" src="<? echo $pro_forma['pro_forma_oc_logo_path']; ?>"></img>
                    </td>
                    <td>
                        <span class = "proFormaField">Pro-forma date</span>												
                        <br/>
                        <span class = "proFormaFieldValue"><? echo explode(" ", $pro_forma['pro_forma_date'])[0]; ?></span>
                    </td>
                    <td>
                        <span class = "proFormaField">Revision Number</span>												
                        <br/>
                        <span class = "proFormaFieldValue"><? echo empty(explode(" ", $pro_forma['pro_forma_revision_number'])[0]) ? ' - ' : explode(" ", $pro_forma['pro_forma_revision_number'])[0]; ?></span>											
                    </td>
                </tr>
                <tr style="vertical-align: top">
                    <td>
                        <span class = "proFormaField">Pro-forma No.</span>												
                        <br/>
                        <? if ($pro_forma['pro_forma_status'] == 'confirm') {?>
                        <span class = "proFormaFieldValue">PRF<? echo str_pad($pro_forma['pro_forma_id'], 5, "0", STR_PAD_LEFT); ?></span>
                        <? } else {?>
                        <span class = "proFormaFieldValue">DRAFT</span>
                        <? } ?>
                        
                    </td>
                    <td>
                        <span class = "proFormaField">Revision Date</span>												
                        <br/>
                        <span class = "proFormaFieldValue"><? echo empty(explode(" ", $pro_forma['pro_forma_revision_date'])[0]) ? ' - ' : explode(" ", $pro_forma['pro_forma_revision_date'])[0]; ?></span>
                    </td>
                </tr>
                <tr style="vertical-align: top">
                    <td colspan = "2" rowspan = "2	">
                        <span class = "proFormaFieldValue"><? echo $pro_forma['pro_forma_oc_name']; ?></span><br/>
                        <span class = "proFormaField">
                            <? echo $pro_forma['pro_forma_oc_address']; ?><br/>
                            <? echo $pro_forma['pro_forma_oc_city_name']; ?><br/>
                            <? echo $pro_forma['pro_forma_oc_district'].', '.$pro_forma['pro_forma_oc_state']; ?><br/>
                            GSTIN: <? echo $pro_forma['pro_forma_oc_gstin']; ?><br/>
                            PAN: <? echo $pro_forma['pro_forma_oc_pan_number']; ?>

                        </span>
                    </td>
                    <td>
                        <span class = "proFormaField">SO No.</span>												
                        <br/>
                        <span class = "proFormaFieldValue"><? echo $pro_forma['pro_forma_order_reference']; ?></span>
                    </td>
                    <td>
                        <span class = "proFormaField">Payment Terms</span>												
                        <br/>
                        <span class = "proFormaFieldValue"><? echo $pro_forma['pro_forma_linked_company_payment_term_name']; ?></span>
                    </td>
                </tr>
                <tr style="vertical-align: top">
                    <td>
                        <span class = "proFormaField">Transporter</span>												
                        <br/>
                        <span class = "proFormaFieldValue"><? echo $pro_forma['pro_forma_linked_transporter_name']; ?></span>
                    </td>
                    <td>
                        <span class = "proFormaField">Place Of Supply</span>												
                        <br/>
                        <span class = "proFormaFieldValue"><? echo $pro_forma['pro_forma_linked_company_gst_supply_state_id'].'-'.$pro_forma['pro_forma_place_of_supply']; ?></span>
                    </td>
                </tr>
            </table>
            <table class = "proFormaTable gray-border">

                <tr>
                    <td>Receiver details (Billed to)</td>
                    <td>Consignee details (Shipped to)</td>
                </tr>
                <tr>
                    <td>
                        Name:<span class = "proFormaFieldValue"><? echo $pro_forma['pro_forma_linked_company_invoice_name']; ?></span><br/>
                        Address:<? echo $pro_forma['pro_forma_linked_company_billing_address']; ?><br/>
                        State:<? echo $pro_forma['pro_forma_linked_company_billing_state_name']; ?><br/>
                        Contact details:<? echo $pro_forma['pro_forma_linked_company_contact_number']; ?><br/>
                        GST#:<? echo $pro_forma['pro_forma_linked_company_gstin']; ?><br/>
                    </td>
                    <td>
                        Name:<span class = "proFormaFieldValue"><? echo $pro_forma['pro_forma_linked_company_invoice_name']; ?></span><br/>
                        Address:<? echo $pro_forma['pro_forma_linked_company_shipping_address']; ?><br/>
                        State:<? echo $pro_forma['pro_forma_linked_company_shipping_state_name']; ?><br/>
                    </td>
                </tr>
            </table>
            <table style = "table-layout:fixed; width:160%;">
                <td>
                    <span>Reverse charge applicable : <?
                        if ($pro_forma['pro_forma_is_reverse_charge_applicable'] == "1")
                            echo "Y";
                        else 
                            echo "N";
                        ?></span>
                </td>
                <td>
                    <span>Tax Type : <? echo ucwords($pro_forma['pro_forma_tax_type']); ?></span>
                </td>
            </table>

            <hr>
            <table class = "productList proFormaTable">
                <thead class = "grayTBack">
                    <tr>
                        <td width = "30px">Sr</td>
                        <td width = "160px">Item Description</td>
                        <td>HSN / SAC code</td>
                        <td>Rate / Item</td>
                        <?
                        if ($pro_forma['pro_forma_tax_type'] == 'inclusive') {
                            ?>
                            <td>Rate / Item (Excl. Tax)</td>
                            <?
                        }
                        ?>
                        <td class = "discount">Discount / Item</td>
                        <td>Qty</td>
                        <td>Taxable Value</td>
                        <?
                        if ($pro_forma['pro_forma_oc_gst_supply_state_id'] == $pro_forma['pro_forma_linked_company_gst_supply_state_id']) {
                            echo "<td>SGST</td><td>CGST</td>";
                        } else {
                            echo "<td>IGST</td>";
                        }
                        ?>
                        <td width='75px'>Total</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $total_discount = 0;
                    $total_taxable_amount = 0;
                    $total_tax = 0;
                    $tax_amount_map = array();
                    foreach ($pro_forma_entries as $pro_forma_entry) {
                        ?>
                        <tr>
                            <td><? echo $pro_forma_entry['pfe_entry_id']; ?></td>

                            <td><? echo $pro_forma_entry['pfe_product_name']; ?></td>		

                            <td><? echo $pro_forma_entry['pfe_product_hsn_code']; ?></td>


                            <td><? echo number_format($pro_forma_entry['pfe_product_rate'], 2); ?></td> 

                            <?
                            if ($pro_forma['pro_forma_tax_type'] == 'inclusive') {
                                ?>
                                <td><? echo number_format(( $pro_forma_entry['pfe_product_rate'] ) / ( 1 + $pro_forma_entry['pfe_tax_percent_applicable'] / 100), 2) ?></td>
                                <?
                            }
                            ?>
                                <td class = "discount"><?
                                $discount = $pro_forma_entry['pfe_discount'] * $pro_forma_entry['pfe_product_rate'] / 100;
                                if ($pro_forma['pro_forma_tax_type'] == 'inclusive') {
                                    $discount = $discount / ( 1 + $pro_forma_entry['pfe_tax_percent_applicable'] / 100);
                                }
                                $total_discount = $total_discount + ( $discount * $pro_forma_entry['pfe_product_quantity']);
                                echo number_format($discount, 2);
                                ?></td>
                            <td><? echo number_format($pro_forma_entry['pfe_product_quantity'], 2) . ' ' . $pro_forma_entry['pfe_product_uqc_text'] ; ?></td>

                            <td>
                                <?
                                if ($pro_forma['pro_forma_tax_type'] == 'inclusive') {
                                    $taxable_amount = $pro_forma_entry['pfe_product_quantity'] * ((( $pro_forma_entry['pfe_product_rate'] ) / ( 1 + $pro_forma_entry['pfe_tax_percent_applicable'] / 100)) - $discount);
                                } else if ($pro_forma['pro_forma_tax_type'] == 'exclusive') {
                                    $taxable_amount = $pro_forma_entry['pfe_product_quantity'] * ( $pro_forma_entry['pfe_product_rate'] - $discount );
                                }

                                $total_taxable_amount = $total_taxable_amount + $taxable_amount;
                                echo number_format((float) $taxable_amount, 2);
                                ?>
                            </td>
                            <?
                            $tax = ( $taxable_amount * $pro_forma_entry['pfe_tax_percent_applicable'] / 100 ); // default to exclusive tax
                            //if($pro_forma['pro_forma_tax_type'] == 'inclusive'){
                            //$tax = $pro_forma_entry['pfe_product_quantity'] * ( $pro_forma_entry['pfe_product_rate'] - $discount ) * ( $pro_forma_entry['pfe_tax_percent_applicable']/100); 
                            //}
                            $total_tax = $total_tax + $tax;

                            $pfe_tax_percent_applicable = $pro_forma_entry['pfe_tax_percent_applicable'];
                            if (!isset($tax_amount_map[$pfe_tax_percent_applicable])) {
                                $tax_amount_map[$pfe_tax_percent_applicable] = $tax;
                            } else {
                                $stored_tax = $tax_amount_map[$pfe_tax_percent_applicable];
                                $tax_amount_map[$pfe_tax_percent_applicable] = $stored_tax + $tax;
                            }

                            if ($pro_forma['pro_forma_oc_gst_supply_state_id'] == $pro_forma['pro_forma_linked_company_gst_supply_state_id']) {
                                $cgst = $tax / 2;
                                $sgst = $tax / 2;
                                echo "<td>" . number_format((float) $sgst, 2) . "</td><td>" . number_format((float) $cgst, 2) . "</td>";
                            } else {
                                echo "<td>" . number_format((float) $tax, 2) . "</td>";
                            }
                            ?>
                            <td>
                                <?
                                //if($pro_forma['pro_forma_tax_type'] == 'inclusive'){
                                //	echo number_format((float) ($taxable_amount), 2);
                                //} else if ($pro_forma['pro_forma_tax_type'] == 'exclusive'){
                                echo number_format((float) ($taxable_amount + $tax), 2);
                                //}
                                ?>
                            </td>

                        </tr>

                        <?php
                    }
                    ?>
                        
                        <?
                    
                    foreach ($pro_forma_charge_entries as $pro_forma_charge_entry) {
                        ?>
                        <tr>
                            <?

                            if ($pro_forma['pro_forma_tax_type'] == 'inclusive') {
                                ?>
                                <td></td>
                                <?
                            }
                            ?> 
                            <td class = "discount"></td>
                            
                            <td colspan = "5" align="right"><span style = "font-style: italic"><? echo $pro_forma_charge_entry['pfce_charge_name']; ?></span></td>

                            <td>
                                <?
                                //$taxable_amount = $pro_forma_charge_entry['pfce_taxable_amount'];
                                if ($pro_forma['pro_forma_tax_type'] == 'inclusive') {
                                    $taxable_amount = ($pro_forma_charge_entry['pfce_taxable_amount'] ) / ( 1 + $pro_forma_charge_entry['pfce_tax_percent_applicable'] / 100);
                                } else if ($pro_forma['pro_forma_tax_type'] == 'exclusive') {
                                    $taxable_amount = $pro_forma_charge_entry['pfce_taxable_amount'];
                                }
                                $total_taxable_amount = $total_taxable_amount + $taxable_amount;
                                echo number_format((float) $taxable_amount, 2);
                                ?>
                            </td> 
                            <?
                            $tax = ( $taxable_amount * $pro_forma_charge_entry['pfce_tax_percent_applicable'] / 100 ); // default to exclusive tax
                            //if($pro_forma['pro_forma_tax_type'] == 'inclusive'){
                            //$tax = $pro_forma_charge_entry['pfce_product_quantity'] * ( $pro_forma_charge_entry['pfce_product_rate'] - $discount ) * ( $pro_forma_charge_entry['pfce_tax_percent_applicable']/100); 
                            //}
                            $total_tax = $total_tax + $tax;

                            $pfce_tax_percent_applicable = $pro_forma_charge_entry['pfce_tax_percent_applicable'];
                            if (!isset($tax_amount_map[$pfce_tax_percent_applicable])) {
                                $tax_amount_map[$pfce_tax_percent_applicable] = $tax;
                            } else {
                                $stored_tax = $tax_amount_map[$pfce_tax_percent_applicable];
                                $tax_amount_map[$pfce_tax_percent_applicable] = $stored_tax + $tax;
                            }

                            if ($pro_forma['pro_forma_oc_gst_supply_state_id'] == $pro_forma['pro_forma_linked_company_gst_supply_state_id']) {
                                $cgst = $tax / 2;
                                $sgst = $tax / 2;
                                echo "<td>" . number_format((float) $sgst, 2) . "</td><td>" . number_format((float) $cgst, 2) . "</td>";
                            } else {
                                echo "<td>" . number_format((float) $tax, 2) . "</td>";
                            }
                            ?>
                            <td>
                                <?
                                //if($pro_forma['pro_forma_tax_type'] == 'inclusive'){
                                  //  echo number_format((float) ($taxable_amount), 2);
                                //} else if ($pro_forma['pro_forma_tax_type'] == 'exclusive'){
                                    echo number_format((float) ($taxable_amount + $tax), 2);
                                //}
                                ?>
                            </td>

                        </tr>

                        <?php
                    }
                    ?>
                    
                </tbody>
                <tfoot class = "grayTBack">
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <?
                        if ($pro_forma['pro_forma_tax_type'] == 'inclusive') {
                            ?>
                            <td></td>
                            <?
                        }
                        ?>
                        <td class = "discount"></td>
                        <td>Total</td>
                        <td>₹ <? echo number_format((float) $total_taxable_amount, 2); ?></td>
                        <?
                        if ($pro_forma['pro_forma_oc_gst_supply_state_id'] == $pro_forma['pro_forma_linked_company_gst_supply_state_id']) {
                            echo "
																	<td>₹ " . number_format((float) ($total_tax / 2), 2) . "</td>
																	<td>₹ " . number_format((float) ($total_tax / 2), 2) . "</td>";
                        } else {
                            echo "
																	<td>₹ " . number_format((float) ($total_tax), 2) . "</td>";
                        }
                        ?>
                        <td>₹ <?
                            echo number_format((float) ($total_taxable_amount + $total_tax), 2);
                            ?></td>
                    </tr>
                </tfoot>
            </table>

            <table class = "proFormaTable">
                <tr class = "discount">
                    <td colspan = "6" rowspan = "2" width='450px'></td>
                    <td colspan = "2">Discount</td>
                    <td>₹ <? echo number_format($total_discount, 2); ?></td>
                </tr>
                <tr>
                    <?
                    if($total_discount == 0){
                        ?>
                        <td colspan = "6"></td>    
                            <?
                    }
                    ?>
                    <td colspan = "2">Taxable Amount</td>
                    <td>₹ <? echo number_format((float) $total_taxable_amount, 2); ?></td>
                </tr>


                <?
                foreach ($tax_amount_map as $tax_percent => $tax_amount) {
                    if ($pro_forma['pro_forma_oc_gst_supply_state_id'] == $pro_forma['pro_forma_linked_company_gst_supply_state_id']) {
                        echo "<tr>" .
                        "<td colspan = '6'></td>" .
                        "<td colspan = '2'>SGST@" . ($tax_percent / 2) . "%</td>" .
                        "<td>₹ " . number_format((float) ($tax_amount / 2), 2) . "</td>" .
                        "</tr>" .
                        "<tr>" .
                        "<td colspan = '6'></td>" .
                        "<td colspan = '2'>CGST@" . ($tax_percent / 2) . "%</td>" .
                        "<td>₹ " . number_format((float) ($tax_amount / 2), 2) . "</td>" .
                        "</tr>";
                    } else {
                        echo "<tr>" .
                        "<td colspan = '6'></td>" .
                        "<td colspan = '2'>IGST@" . $tax_percent . "%</td>" .
                        "<td>₹ " . number_format((float) ($tax_amount), 2) . "</td>" .
                        "</tr>";
                    }
                }
                ?>

                <tr style = "display:none">
                    <td colspan = "6"></td>
                    <td colspan = "2">Transportation Charges</td>
                    <td>₹ <?
                        $pro_forma_transport_charges = 0;
                        if ($pro_forma['pro_forma_transport_charges'] != '')
                            $pro_forma_transport_charges = $pro_forma['pro_forma_transport_charges'];

                        echo $pro_forma_transport_charges;
                        ?>
                    </td>
                </tr>

                <?
                    $pro_forma_adjustments = 0;
                    if ($pro_forma['pro_forma_adjustments'] != ''){
                        $pro_forma_adjustments = $pro_forma['pro_forma_adjustments'];
                    }
                ?>
                <? if($pro_forma_adjustments > 0){ ?>
                <tr>
                    <td colspan = "6" style = "text-align:left;">Total Pro-forma Amount (in words)</td>
                    <td colspan = "2">Adjustments</td>
                    <td>₹ <?
                        
                        echo $pro_forma_adjustments;
                        ?></td>
                </tr>
                <? } ?>
                <?
                
                        $total_pro_forma_amount = $total_taxable_amount + $total_tax + $pro_forma_adjustments + $pro_forma_transport_charges;
                ?>
                <tr class = "grayTBack">
                    <td colspan = "6" style = "text-align:left;">
                        <span style = "font-weight:500;">Rupees</span> <? echo ucwords(getIndianCurrency(round($total_pro_forma_amount, 2))); ?> <span style = "font-weight:500;">Only</span>
                    </td>
                    <td colspan = "2">Total Pro-forma Amount</td>
                    <td>₹ <?
                        echo number_format($total_pro_forma_amount, 2);
                        ?></td>
                </tr>
                <tr>
                    <td colspan = "6" style = "text-align:left;"><textarea style = "min-height: 12em;border:none;resize: none;overflow:hidden;width:300px;" readonly><? echo $pro_forma['pro_forma_oc_bank_details']; ?></textarea></td>
                    <td colspan = "3" style = "text-align:center;">Authorized Signatory<br/><br/><br/><br/><span style = "font-weight:800;"><? echo $pro_forma['pro_forma_oc_name']; ?></span></td>
                </tr>
            </table>
            <table>
                <textarea class = "termsText" readonly>Pro-forma Terms : &#13;&#10;<? echo $pro_forma['pro_forma_terms']; ?></textarea>
            </table>

            <footer style = "position:absolute;bottom:0;right:0;padding-right:20px;padding-bottom:15px;font-size:11px;font-style:italic;">
                Powered By Quant ERP
            </footer>
        </div>

        <?if(isset($linked_documents)) {?>
        <div class="breadcrumb-line breadcrumb-line-component">
            <ul class="breadcrumb">
                <li>Linked Documents</li>
                <?
                foreach ($linked_documents as $linked_document) {
                    $prefix = '';
                    $link = '';
                    if ($linked_document['document_type'] == 'credit_note') {
                        $prefix = 'CDN';
                        $link = '#!/CreditNote/preview/confirm/' . $linked_document['document_id'];
                    } else if ($linked_document['document_type'] == 'debit_note') {
                        $prefix = 'DBN';
                        $link = '#!/DebitNote/preview/confirm/' . $linked_document['document_id'];
                    } else if ($linked_document['document_type'] == 'payment') {
                        $prefix = 'PAY';
                        $link = '#!/Payment/preview/' . $linked_document['document_id'];
                    } else if ($linked_document['document_type'] == 'receipt') {
                        $prefix = 'REC';
                        $link = '#!/receipt/preview/' . $linked_document['document_id'];
                    }
                    echo '<li><a href = "' . $link . '">' . $prefix . str_pad($linked_document['document_id'], 5, "0", STR_PAD_LEFT) . '</a></li>';
                }
                ?>
            </ul>
        </div>
        <? } ?>
    </div>


    <style>
        @media print {
            body * {
                visibility: hidden;
            }
            #section-to-print, #section-to-print * {
                visibility: visible;
                overflow:hidden;
            }
            #section-to-print {
                position: absolute;
                left: 0;
                top: 0;
                size: auto;   /* auto is the initial value */
                margin: 0;
                border: initial;
                border-radius: initial;
                width: initial;
                min-height: initial;
                box-shadow: initial;
                background: initial;
                page-break-after: always;
            }
            .grayTBack {
                visibility: visible;
                background-color: #ccc!important;
                color: black;
                font-weight:700;
                -webkit-print-color-adjust: exact;
                border: solid #ccc!important;
            }
            
            .gray-border{
                border: 1px solid #ccc;
                overflow:visible!important;
                -webkit-print-color-adjust: exact;
            }
        }
        @page 
        {
            size:  auto;   /* auto is the initial value */
            margin: 0mm;  /* this affects the margin in the printer settings */
        }
        .termsText{
            font-size: 10px;
            min-height: 12em;
            border:none;
            resize: none;
            overflow:hidden;
            width:100px;
            width: 375px;
            text-align: justify;
        }
        <?
        if($total_discount == 0){
            ?>
               .discount {
                   display: none;
               }
                <?
        }
        ?>
        .productList{
            border: 1px solid #FFFFFF;
            text-align: center;
            font-size: 11px;
        }
        .whiteTBack {
            background-color: #FFFFFF;
            color: black;
            font-weight:700;
        }
        .grayTBack {
            background-color: #ccc;
            color: black;
            font-weight:700;
        }
        .proFormaTable {
            font-size: 11px;
            width: 100%;
            max-width: 100%;
            table-layout: fixed;
            font-family:"arial";
            margin-bottom:5px; 
            padding:10px;
        }
        .gray-border{
            border: 1px solid #ccc;
            border-collapse: collapse;
        }

        .logo {
            max-width:40mm; max-height: 20mm;
            text-align: center;
        }
        .logo_container {
            /*text-align: center;*/
        }
        #proFormaHeader {
            font-size: 20px;
            font:"arial";
            width:100%;
            text-align: center;
            font-family:"arial";
            font-weight: bold;
            display: block;
        }
        hr {
            border: 3px solid #000000;
            margin-top:3px;
        }
        .proFormaField {
            font-size: 11px;	
        }
        .proFormaFieldValue {
            font-weight: bold;
            font-size: 12px;
        }
        td {
            padding:3px;
        }
        hr {
            margin-bottom: 5px;
        }
    </style>