
<div ng-controller="expenseFormController" ng-init="load()"></div>
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/expense/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Purchase</span> - Expense</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!/expense/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!"><i class="icon-home2 position-left"></i>Purchase</a></li>
            <li><a href="#!/expense/all"><i class="icon-cash4 position-left"></i> Expense</a></li>
            <li class="active"><?php
                if (isset($expense))
                    echo 'EXP' . str_pad($expense['expense_id'], 5, "0", STR_PAD_LEFT);
                else
                    echo "New";
                ?></li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Form validation -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Expense</h5>

        </div>


        <div class="panel-body">
            <?php
            $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id'=>'merpForm');
            if (isset($expense))
                echo form_open_multipart('web/expense/save/' . $expense['expense_id'], $attributes);
            else
                echo form_open_multipart('web/expense/save', $attributes);
            ?>
            <p class="content-group-lg">Expense include day-to-day business expenses which are not booked under Purchases. Expenses recorded here will not be eligible for Input Tax Credit.</p>

            <fieldset class="content-group">
                <legend class="text-bold">Customer Details</legend>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Vendor Name <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control vendor_names" tabindex="1" name="expense_vendor_name" type="text" required onchange = "resetExpenseLinkedCompanyId();"
                                   value = "<?php if (isset($expense)) echo $expense['expense_vendor_name']; ?>">
                        </div>
                    </div>		
                    <div class="form-group col-lg-6 has-feedback has-feedback-left">
                        <label class="control-label col-lg-3">Company ID</label>
                        <div class="col-lg-9">
                            <input class="form-control" style = "font-size:12px;padding-left:40px;" name="expense_vendor_id" type="text" readonly
                                   value = "<?php if (isset($expense)) echo $expense['expense_vendor_id']; ?>">
                            <div class="form-control-feedback" style = "left:10px">
                                <span style = "font-size:12px;padding-left:7px;"> COMP</span>
                            </div>
                        </div>
                    </div>		
                </div>              
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Other Details</legend>	
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Date <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name="expense_date" id = "datePicker"
                                   value = "<?php if (isset($expense)) echo explode(" ", $expense['expense_date'])[0]; ?>" required>
                        </div>
                    </div>

                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Bill Reference</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="3" name="expense_bill_reference" type="text"
                                   value = "<?php if (isset($expense)) echo $expense['expense_bill_reference']; ?>">
                        </div>
                    </div>	
                </div>

                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Expense Amount <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="4" name = "expense_amount" type="number"
                                   value = "<?php if (isset($expense)) echo $expense['expense_amount']; ?>" required>

                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Expense Type <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="form-control select2" name="expense_type" tabindex="7" required>
                                <option value ="">Choose expense type</option>
                                <option value ="B2CS GST Paid" <? if(isset($expense)) if($expense['expense_type'] == 'B2CS GST Paid') echo 'selected';  ?>>B2CS GST Paid</option>
                                <option value ="Composition Taxable Supplies" <? if(isset($expense)) if($expense['expense_type'] == 'Composition Taxable Supplies') echo 'selected';  ?>>Composition Taxable Supplies</option>
                                <option value ="GST Exempt" <? if(isset($expense)) if($expense['expense_type'] == 'GST Exempt') echo 'selected';  ?>>GST Exempt</option>
                                <option value ="GST Not Applicable" <? if(isset($expense)) if($expense['expense_type'] == 'GST Not Applicable') echo 'selected';  ?>>GST Not Applicable</option>
                                <option value ="Non-GST" <? if(isset($expense)) if($expense['expense_type'] == 'Non-GST') echo 'selected';  ?>>Non-GST</option>
                                <option value ="Unregistered Dealer Purchase" <? if(isset($expense)) if($expense['expense_type'] == 'Unregistered Dealer Purchase') echo 'selected';  ?>>Unregistered Dealer Purchase</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Expense Account <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="form-control select2" name="expense_account_coa_id" tabindex="6" required>
                                <option value ="">Choose an account</option>
                                <? foreach ($expense_accounts as $expense_account) { ?>
                                    <option value = "<?php echo $expense_account['coa_id']; ?>" 
                                            <? if(isset($expense)) if($expense['expense_account_coa_id'] == $expense_account['coa_id']) echo " selected "; ?>><?php echo $expense_account['coa_account_name']; ?></option>
                                <? }
                                ?>

                            </select>
                        </div>
                    </div>

                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Payment Account <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="form-control select2" name="expense_payment_account_coa_id" tabindex="7" required>
                                <option value ="">Choose an account</option>
                                <? foreach ($current_accounts as $current_account) { ?>
                                    <option value = "<?php echo $current_account['coa_id']; ?>" 
                                            <? if(isset($expense)) if($expense['expense_payment_account_coa_id'] == $current_account['coa_id']) echo " selected "; ?>><?php echo $current_account['coa_account_name']; ?></option>
                                <? }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="col-lg-3 control-label">Attach Bill</label>
                        <div class="col-lg-9">
                            <input type="file" name = "expense_image_path" id = "expense_image_path" 
                                   class="file-input-preview" data-show-remove="false"  data-dismiss="fileupload" data-show-upload="false" 
                                   data-image-path = "<?php if (isset($expense)) echo $expense['expense_image_path'] ?>" 
                                   data-image-name = "<?php if (isset($expense)) echo $expense['expense_bill_reference'] ?>">
                            <span class="help-block">Maximum file upload size is <code>100 KB.</code> </span>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Expense Description / Narration <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name = "expense_narration" type="text" value = "<?php if (isset($expense)) echo $expense['expense_narration']; ?>" tabindex="12" required>

                        </div> 
                    </div>
                </div>
            </fieldset>

            <div class="text-right">
                <a href="#!/expense/all"><button class="btn btn-default">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                <? if($this->session->userdata('access_controller')->is_access_granted('expense', 'save')) { ?>
                <button onclick="submitForm('expense');" class="btn btn-primary">Save<i class="icon-arrow-right14 position-right"></i></button>
                <? } ?>
            </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->