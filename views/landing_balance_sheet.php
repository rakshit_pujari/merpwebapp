

<!-- Page header -->
<div class="page-header">

    <div ng-controller="noGroupDataTableController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Accounting</span> - Balance Sheet</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-calculator2 position-left"></i> Accounting</li>
            <li class="active"><i class="icon-table2 position-left"></i>Balance Sheet</li>
        </ul>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Ledgers archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Balance Sheet</h6>
            <div class="heading-elements">
                </ul>
            </div>
        </div>

        <?
        $net_income = 0;
        foreach ($ledger_entries as $ledger_entry):
            if ($ledger_entry['at_name'] == 'Revenue' || $ledger_entry['at_name'] == 'Expense')
                $net_income+=($ledger_entry['credit_balance'] - $ledger_entry['debit_balance']);
        endforeach;
        ?>
        <table class="table">
            <thead>
                <tr>
                    <th></th>
                    <th>Account Name</th>
                    <th>Account Type</th>
                    <th>Amount</th>
                </tr>
            </thead>
            <tbody>
                <?
                $assets_total = 0;
                $liabilities_total = 0;
                ?>
                <tr>
                    <th width="18%">I. Assets</th>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                
                <tr>
                    <td>Non Current Assets</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                
                <?php
                foreach ($ledger_entries as $ledger_entry):
                    if ($ledger_entry['at_name'] == 'Non Current Assets')
                        add_row($ledger_entry, 'debit', $assets_total);
                endforeach;
                ?>
                
                <tr>
                    <td>Current Assets</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                
                <?php
                foreach ($ledger_entries as $ledger_entry):
                    if ($ledger_entry['at_name'] == 'Current Assets')
                        add_row($ledger_entry, 'debit', $assets_total);
                endforeach;
                ?>                   
                <tr>
                    <td></td>
                    <th>Total Assets</th>
                    <td></td>
                    <th><? echo '₹ ' .number_format($assets_total, 2); ?></th>
                </tr>
                
                <tr>
                    <th>II. Liabilities</th>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                
                <tr>
                    <td>Non Current Liabilities</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                
                <?php
                foreach ($ledger_entries as $ledger_entry):
                    if ($ledger_entry['at_name'] == 'Non Current Liabilities' ) {
                            add_row($ledger_entry, 'credit', $liabilities_total);
                    }
                endforeach;
                ?>
                
                <tr>
                    <td>Current Liabilities</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                
                <?php
                foreach ($ledger_entries as $ledger_entry):
                    if ($ledger_entry['at_name'] == 'Current Liabilities') {
                        add_row($ledger_entry, 'credit', $liabilities_total);
                    }
                endforeach;
                ?>
                
                <tr>
                    <td>Equity</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                
                <?php
                foreach ($ledger_entries as $ledger_entry):
                    if ($ledger_entry['at_name'] == 'Equity') {
                        add_row($ledger_entry, 'credit', $liabilities_total);
                    }
                endforeach;
                ?>
                

                <tr>
                    <td></td> 
                    <td><a href = "#!/SJ/pnl"><? if($net_income >= 0) echo "Current Year Profit"; else echo "Current Year Loss"; ?></a></td> 
                    <td></td> 
                    <td><?
                        if($net_income < 0) echo "( ";
                        $liabilities_total += $net_income;
                        echo '₹ ' . number_format(abs($net_income), 2);
                        if($net_income < 0) echo " )";
                        ?></td> 
                </tr>

                
                <tr>
                    <td></td>
                    <th>Total Liabilities</th>
                    <td></td>
                    <th><? echo '₹ ' .number_format($liabilities_total, 2); ?></th> 
                </tr>
                
            </tbody>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->

    <?

    function add_row($ledger_entry, $account_type, &$section_total) {
        if($ledger_entry['credit_balance'] > 0 || $ledger_entry['debit_balance'] > 0){
        if($account_type == 'debit') 
            $amount = $ledger_entry['debit_balance'] - $ledger_entry['credit_balance'];
        else
            $amount = $ledger_entry['credit_balance'] - $ledger_entry['debit_balance'];
        $section_total = $section_total + $amount;
        ?>
        <tr>
            <td></td>
            <td>
                <a href="#!SJ/ledger/<?php echo $ledger_entry['sj_account_coa_id']; ?>"><span>
                        <?
                        echo $ledger_entry['coa_account_name'];
                        ?>
                    </span></a>

            </td>
            <td>
                <?
                echo $ledger_entry['at_name'];
                ?>

            </td>
            <td>
                <?
                if ($amount < 0)
                    echo '( ';
                echo '₹ ' . number_format(abs($amount), 2);
                if ($amount < 0)
                    echo ' )';
                ?>
            </td>
        </tr>
        <? }
    }
    ?>