<?php

function getIndianCurrency($number) {
    $decimal = round($number - ($no = floor($number)), 2) * 100;
    $hundred = null;
    $digits_length = strlen($no);
    $i = 0;
    $str = array();
    $words = array(0 => '', 1 => 'one', 2 => 'two',
        3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
        7 => 'seven', 8 => 'eight', 9 => 'nine',
        10 => 'ten', 11 => 'eleven', 12 => 'twelve',
        13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
        16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
        19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
        40 => 'forty', 50 => 'fifty', 60 => 'sixty',
        70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
    $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
    while ($i < $digits_length) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number] . ' ' . $digits[$counter] . $plural . ' ' . $hundred : $words[floor($number / 10) * 10] . ' ' . $words[$number % 10] . ' ' . $digits[$counter] . $plural . ' ' . $hundred;
        } else
            $str[] = null;
    }
    $Rupees = implode('', array_reverse($str));
    //$paise = ($decimal) ? " and " . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    $paise = ($decimal) ? " and " . ($words[((int) ($decimal / 10)) * 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    return ($Rupees ? $Rupees . ' ' : '') . $paise;
}
?>

<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/DebitNote/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Purchase</span> - Debit Note</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                    <!--<a href="#!/DebitNote/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>-->
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!"><i class="icon-home2 position-left"></i>Purchase</a></li>
            <li><a href="#!/DebitNote/all"><i class="icon-city position-left"></i> Debit Note</a></li>
            <? if ($debit_note['dn_status'] == 'confirm') { ?>
                <li class="active"><?php if (isset($debit_note)) echo 'DBN' . str_pad($debit_note['dn_id'], 5, "0", STR_PAD_LEFT); ?></li>
            <? } else { ?>
                <li class="active"><?php if (isset($debit_note)) echo 'DRAFT'; ?></li>
            <? } ?>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <a href="#!/DebitNote/all"><button class="btn btn-default">Back<i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
    <a href="#!/DebitNote/view/<? echo $debit_note['dn_status'].'/'.$debit_note['dn_id']; ?>"><button class="btn btn-default">Edit <i class="icon-pencil3 position-right"></i></button></a>
    <button onClick="window.print();" class="btn btn-primary">Print <i class="icon-arrow-right14 position-right"></i></button>

    <!-- Form validation -->
    <div class="panel-flat">



        <div class="panel panel-body col-md-offset-3"  id = "section-to-print" style="width: 210mm;min-height: 297mm;padding-left:40px;padding-right:40px;">

            <span id = "debitNoteHeader">Debit Note</span>
            <hr>

            <table class = "debitNoteTable">
                <tr style="vertical-align: top">
                    <td class = "logo_container" colspan = "2" rowspan = "2	">
                        <img class = "logo" src="<? echo $debit_note['dn_oc_logo_path']; ?>"></img>
                    </td>
                    <td>
                        <span class = "debitNoteField">Debit Note date</span>												
                        <br/>
                        <span class = "debitNoteFieldValue"><? echo explode(" ", $debit_note['dn_date'])[0]; ?></span>
                    </td>
                    <td>
                        <span class = "debitNoteField">Transporter</span>												
                        <br/>
                        <span class = "debitNoteFieldValue"><? echo $debit_note['dn_linked_transporter_name']; ?></span>											
                    </td>
                </tr>
                <tr style="vertical-align: top">
                    <td>
                        <span class = "debitNoteField">Debit Note No.</span>												
                        <br/>
                        <? if ($debit_note['dn_status'] == 'confirm') { ?>
                            <span class = "debitNoteFieldValue">DBN<? echo str_pad($debit_note['dn_id'], 5, "0", STR_PAD_LEFT); ?></span>
                        <? } else { ?>
                            <span class = "debitNoteFieldValue">DRAFT</span>
                        <? } ?>

                    </td>
                    <td>
                        <span class = "debitNoteField">Transport L.R No.</span>												
                        <br/>
                        <span class = "debitNoteFieldValue"><? echo $debit_note['dn_lr_number']; ?></span>
                    </td>
                </tr>
                <tr style="vertical-align: top">
                    <td colspan = "2" rowspan = "2">
                        <span class = "debitNoteFieldValue"><? echo $debit_note['dn_oc_name']; ?></span><br/>
                        <span class = "debitNoteField">
                            <? echo $debit_note['dn_oc_address']; ?><br/>
                            GSTIN: <? echo $debit_note['dn_oc_gstin']; ?><br/>
                            PAN: <? echo $debit_note['dn_oc_pan_number']; ?>

                        </span>
                    </td>
                    <td>
                        <?
                        if (isset($debit_note['dn_linked_invoice_id'])) {
                            echo '<span class = "debitNoteField">Invoice No.</span>												
																<br/>
															<span class = "debitNoteFieldValue">INV' . str_pad($debit_note['dn_linked_invoice_id'], 5, "0", STR_PAD_LEFT) . '</span>';
                        } else if (isset($debit_note['dn_linked_purchase_id'])) {
                            echo '<span class = "debitNoteField">Invoice No.</span>												
																<br/>
															<span class = "debitNoteFieldValue">PUR' . str_pad($debit_note['dn_linked_purchase_id'], 5, "0", STR_PAD_LEFT) . '</span>';
                        }
                        ?>

                    </td>
                    <td>
                        <span class = "debitNoteField">Payment Terms</span>												
                        <br/>
                        <span class = "debitNoteFieldValue"><? echo $debit_note['dn_linked_company_payment_term_name']; ?></span>
                    </td>
                </tr>
                <tr style="vertical-align: top">
                    <td>
                        <span class = "debitNoteField">Invoice Date.</span>												
                        <br/>
                        <span class = "debitNoteFieldValue"><? echo explode(" ", $debit_note['dn_linked_invoice_date'])[0]; ?></span>
                    </td>
                    <td>
                        <span class = "debitNoteField">Place Of Supply</span>												
                        <br/>
                        <span class = "debitNoteFieldValue"><? echo $debit_note['dn_linked_company_gst_supply_state_id'].'-'.$debit_note['dn_place_of_supply']; ?></span>
                    </td>
                </tr>
            </table>
            <table class = "debitNoteTable gray-border">

                <tr>
                    <td>Receiver details (Billed to)</td>
                    <td>
                        <? if (isset($debit_note['dn_linked_invoice_id'])) { ?>
                            Consignee details (Shipped to)
                        <? } ?>	
                    </td>

                </tr>
                <tr>
                    <td>
                        Name:<span class = "debitNoteFieldValue"><? echo $debit_note['dn_linked_company_invoice_name']; ?></span><br/>
                        Address:<? echo $debit_note['dn_linked_company_billing_address']; ?><br/>
                        State:<? echo $debit_note['dn_linked_company_billing_state_name']; ?><br/>
                        Contact details:<? echo $debit_note['dn_linked_company_contact_number']; ?><br/>
                        GST#:<? echo $debit_note['dn_linked_company_gstin']; ?><br/>
                        PAN#:<? echo $debit_note['dn_linked_company_pan_number']; ?><br/>
                    </td>
                    <td>
                        <? if (isset($debit_note['dn_linked_invoice_id'])) { ?>
                            Name:<? echo $debit_note['dn_linked_company_invoice_name']; ?><br/>
                            Address:<? echo $debit_note['dn_linked_company_shipping_address']; ?><br/>
                            State:<? echo $debit_note['dn_linked_company_shipping_state_name']; ?><br/>
                            GST#:<? echo $debit_note['dn_linked_company_gstin']; ?><br/>
                        <? } ?>
                    </td>
                </tr>
            </table>
            <table style = "table-layout:fixed; width:160%;">
                <td>
                    <span>Reverse charge applicable : <?
                        if ($debit_note['dn_is_reverse_charge_applicable'] == "0")
                            echo "N";
                        else if (
                                $debit_note['dn_is_reverse_charge_applicable'] == "1")
                            echo "Y";
                        ?></span>
                </td>

                <td>
                    <span>Tax Type : <? echo ucwords($debit_note['dn_tax_type']); ?></span>
                </td>
            </table>
            <hr>
            <table class = "productList debitNoteTable">
                <thead class = "grayTBack">
                    <tr>
                        <td width='25px'>Sr</td>
                        <td width='150px'>Item Description</td>
                        <td>HSN / SAC code</td>
                        <td>Rate / Item</td>
                        <?
                        if ($debit_note['dn_tax_type'] == 'inclusive') {
                            ?>
                            <td>Rate / Item (Excl. Tax)</td>
                            <?
                        }
                        ?>
                        <td>Discount / Item</td>
                        <td>Qty</td>
                        <td width='75px'>Taxable Value</td>
                        <?
                        if ($debit_note['dn_linked_company_billing_state_id'] == $debit_note['dn_linked_company_gst_supply_state_id']) {
                            echo "<td width='75px'>SGST</td><td width='75px'>CGST</td>";
                        } else {
                            echo "<td width='75px'>IGST</td>";
                        }
                        ?>
                        <td width='75px'>Total</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $total_discount = 0;
                    $total_taxable_amount = 0;
                    $total_tax = 0;
                    $tax_amount_map = array();
                    $counter = 1;
                    foreach ($debit_note_entries as $debit_note_entry) {
                        ?>
                        <tr>
                            <td><? echo $counter;
                            $counter++;
                            ?></td>

                            <td><? echo $debit_note_entry['dne_product_name']; ?></td>		

                            <td><? echo $debit_note_entry['dne_product_hsn_code']; ?></td>



                            <td><? echo number_format($debit_note_entry['dne_product_rate'], 2); ?></td>

                            <?
                            if ($debit_note['dn_tax_type'] == 'inclusive') {
                                ?>
                                <td><? echo number_format(($debit_note_entry['dne_product_rate'] ) / ( 1 + $debit_note_entry['dne_tax_percent_applicable'] / 100), 2) ?></td>
                                <?
                            }
                            ?>

                            <td><?
                                $discount = $debit_note_entry['dne_discount'] * $debit_note_entry['dne_product_rate'] / 100;
                                if ($debit_note['dn_tax_type'] == 'inclusive') {
                                    $discount = $discount / ( 1 + $debit_note_entry['dne_tax_percent_applicable'] / 100);
                                }
                                $total_discount = $total_discount + ( $discount * $debit_note_entry['dne_product_quantity'] );
                                echo number_format($discount, 2);
                                ?></td>
                            <td><? echo number_format($debit_note_entry['dne_product_quantity'], 2) . ' ' . $debit_note_entry['dne_product_uqc_text']; ?></td>

                            <td>
                                <?
                                if ($debit_note['dn_tax_type'] == 'inclusive') {
                                    $taxable_amount = $debit_note_entry['dne_product_quantity'] * ((( $debit_note_entry['dne_product_rate'] ) / ( 1 + $debit_note_entry['dne_tax_percent_applicable'] / 100)) - $discount);
                                } else if ($debit_note ['dn_tax_type'] == 'exclusive') {
                                    $taxable_amount = $debit_note_entry['dne_product_quantity'] * ( $debit_note_entry['dne_product_rate'] - $discount );
                                }

                                $total_taxable_amount = $total_taxable_amount + $taxable_amount;
                                echo number_format($taxable_amount, 2);
                                ?>   
                            </td>
                            <?
                            $tax = ( $taxable_amount * $debit_note_entry['dne_tax_percent_applicable'] / 100 ); // default to exclusive tax
                            //if($debit_note['dn_tax_type'] == 'inclusive'){
                            //taxAmount = (parseFloat(tax) * parseFloat(amount) ) / ( 100 + parseFloat(tax) );
                            //$tax = ($taxable_amount * $debit_note_entry['dne_tax_percent_applicable']) / ( 100 + $debit_note_entry['dne_tax_percent_applicable']);
                            //}
                            $total_tax = $total_tax + $tax;

                            $dne_tax_percent_applicable = $debit_note_entry['dne_tax_percent_applicable'];
                            if (!isset($tax_amount_map[$dne_tax_percent_applicable])) {
                                $tax_amount_map[$dne_tax_percent_applicable] = $tax;
                            } else {
                                $stored_tax = $tax_amount_map[$dne_tax_percent_applicable];
                                $tax_amount_map[$dne_tax_percent_applicable] = $stored_tax + $tax;
                            }

                            if ($debit_note['dn_linked_company_billing_state_id'] == $debit_note['dn_linked_company_gst_supply_state_id']) {
                                $cgst = $tax / 2;
                                $sgst = $tax / 2;
                                echo "<td>" . number_format((float) $sgst, 2) . "</td><td>" . number_format((float) $cgst, 2) . "</td>";
                            } else {
                                echo "<td>" . number_format((float) $tax, 2) . "</td>";
                            }
                            ?>
                            <td>
                                <?
                                //if($debit_note['dn_tax_type'] == 'inclusive'){
                                //echo number_format((float) ($taxable_amount), 2);
                                //} else if ($debit_note['dn_tax_type'] == 'exclusive'){
                                echo number_format((float) ($taxable_amount + $tax), 2);
                                //}
                                ?>
                            </td>

                        </tr>

                        <?php
                    }
                    ?>
                        
                    <?
                    
                    foreach ($debit_note_charge_entries as $debit_note_charge_entry) {
                        ?>
                        <tr>
                            <?

                            if ($debit_note['dn_tax_type'] == 'inclusive') {
                                ?>
                                <td></td>
                                <?
                            }
                            ?> 
                            <td class = "discount"></td>
                            
                            <td colspan = "5" align="right"><span style = "font-style: italic"><? echo $debit_note_charge_entry['dnce_charge_name']; ?></span></td>

                            <td>
                                <?
                                //$taxable_amount = $debit_note_charge_entry['dnce_taxable_amount'];
                                if ($debit_note['dn_tax_type'] == 'inclusive') {
                                    $taxable_amount = ($debit_note_charge_entry['dnce_taxable_amount'] ) / ( 1 + $debit_note_charge_entry['dnce_tax_percent_applicable'] / 100);
                                } else if ($debit_note['dn_tax_type'] == 'exclusive') {
                                    $taxable_amount = $debit_note_charge_entry['dnce_taxable_amount'];
                                }
                                $total_taxable_amount = $total_taxable_amount + $taxable_amount;
                                echo number_format((float) $taxable_amount, 2);
                                ?>
                            </td> 
                            <?
                            $tax = ( $taxable_amount * $debit_note_charge_entry['dnce_tax_percent_applicable'] / 100 ); // default to exclusive tax
                            //if($debit_note['debit_note_tax_type'] == 'inclusive'){
                            //$tax = $debit_note_charge_entry['dnce_product_quantity'] * ( $debit_note_charge_entry['dnce_product_rate'] - $discount ) * ( $debit_note_charge_entry['dnce_tax_percent_applicable']/100); 
                            //}
                            $total_tax = $total_tax + $tax;

                            $dnce_tax_percent_applicable = $debit_note_charge_entry['dnce_tax_percent_applicable'];
                            if (!isset($tax_amount_map[$dnce_tax_percent_applicable])) {
                                $tax_amount_map[$dnce_tax_percent_applicable] = $tax;
                            } else {
                                $stored_tax = $tax_amount_map[$dnce_tax_percent_applicable];
                                $tax_amount_map[$dnce_tax_percent_applicable] = $stored_tax + $tax;
                            }

                            if ($debit_note['dn_oc_gst_supply_state_id'] == $debit_note['dn_linked_company_gst_supply_state_id']) {
                                $cgst = $tax / 2;
                                $sgst = $tax / 2;
                                echo "<td>" . number_format((float) $sgst, 2) . "</td><td>" . number_format((float) $cgst, 2) . "</td>";
                            } else {
                                echo "<td>" . number_format((float) $tax, 2) . "</td>";
                            }
                            ?>
                            <td>
                                <?
                                //if($debit_note['dn_tax_type'] == 'inclusive'){
                                  //  echo number_format((float) ($taxable_amount), 2);
                                //} else if ($debit_note['dn_tax_type'] == 'exclusive'){
                                    echo number_format((float) ($taxable_amount + $tax), 2);
                                //}
                                ?>
                            </td>

                        </tr>

                        <?php
                    }
                    ?>
                    
                </tbody>
                <tfoot class = "grayTBack">
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <?
                        if ($debit_note['dn_tax_type'] == 'inclusive') {
                            ?>
                            <td></td>
                            <?
                        }
                        ?>
                        <td></td>
                        <td>Total</td>
                        <td>₹ <? echo number_format($total_taxable_amount, 2); ?></td>
                        <?
                        if ($debit_note['dn_linked_company_billing_state_id'] == $debit_note['dn_linked_company_gst_supply_state_id']) {
                            echo "
																	<td>₹ " . number_format((float) ($total_tax / 2), 2) . "</td>
																	<td>₹ " . number_format((float) ( $total_tax / 2), 2) . "</td>";
                        } else {
                            echo "
																	<td>₹ " . number_format((float) ($total_tax), 2) . "</td>";
                        }
                        ?>
                        <td>₹ <?
                            //if($debit_note['dn_tax_type'] == 'inclusive'){
                            //	echo number_format((float) ($total_taxable_amount), 2);
                            //} else if ($debit_note['dn_tax_type'] == 'exclusive'){
                            echo number_format((float) ($total_taxable_amount + $total_tax), 2);
                            //}
                            ?></td>
                    </tr>
                </tfoot>
            </table>

            <table class = "debitNoteTable">
                <tr>
                    <td colspan = "6" rowspan = "2" width='450px'></td>
                    <td colspan = "2">Discount Amount</td>
                    <td>₹ <? echo number_format($total_discount, 2); ?></td>
                </tr>
                <tr>
                    <td colspan = "2">Taxable Amount</td>
                    <td>₹    <? echo number_format($total_taxable_amount, 2); ?></td>
                </tr>


                <?
                foreach ($tax_amount_map as $tax_percent => $tax_amount) {
                    if ($debit_note['dn_linked_company_billing_state_id'] == $debit_note['dn_linked_company_gst_supply_state_id']) {
                        echo "<tr>" .
                        "<td colspan = '6'></td>" .
                        "<td colspan = '2'>SGST@" . ($tax_percent / 2) . "%</td>" .
                        "<td>₹ " . number_format((float) ( $tax_amount / 2), 2) . "</td>" .
                        "</tr>" . "<tr>" .
                        "<td colspan = '6'></td>" .
                        "<td colspan = '2'>CGST@" . ($tax_percent / 2) . "%</td>" .
                        "<td>₹ " . number_format((float) ( $tax_amount / 2), 2) . "</td>" .
                        "</tr>";
                    } else {
                        echo "<tr>" .
                        "<td colspan = '6'></td>" .
                        "<td colspan = '2'>IGST@" . $tax_percent . "%</td>" .
                        "<td>₹ " . number_format((float) ( $tax_amount ), 2) . "</td>" .
                        "</tr>";
                    }
                }
                ?>

                <tr style = "display:none">
                    <td colspan = "6"></td>
                    <td colspan = "2">Transportation Charges</td>
                    <td>₹ <?
                        $debit_note_transport_charges = 0;
                        if ($debit_note['dn_transport_charges'] != '')
                            $debit_note_transport_charges = $debit_note ['dn_transport_charges'];


                        echo $debit_note_transport_charges;
                        ?>
                    </td>
                </tr>

                <tr>
                    <td colspan = "6" style = "text-align:left;">Total Debit Note Amount (in words)</td>
                    <td colspan = "2">Adjustments</td>
                    <td>₹ <?
                        $debit_note_adjustments = 0;
                        if ($debit_note['dn_adjustments'] != '')
                            $debit_note_adjustments = $debit_note ['dn_adjustments'];


                        echo $debit_note_adjustments;
                        ?></td>
                </tr>
                <?
                $total_debit_note_amount = $total_taxable_amount + $total_tax + $debit_note_adjustments + $debit_note_transport_charges;
                //if($debit_note['dn_tax_type'] == 'inclusive'){
                //		$total_debit_note_amount = $total_taxable_amount + $debit_note_adjustments + $debit_note_transport_charges;
                //}
                ?>
                <tr class = "grayTBack">
                    <td colspan = "6" style = "text-align:left;">
                        <span style = "font-weight:500;">Rupees</span> <? echo ucwords(getIndianCurrency(round($total_debit_note_amount, 2))); ?> <span style = "font-weight:500;">Only</span>
                    </td>
                    <td colspan = "2">Total Debit Note Amount</td>
                    <td>₹    <?
                echo number_format($total_debit_note_amount, 2);
                ?>   </td>
                </tr>
                <tr>
                    <td colspan = "6" style = "text-align:left;"></td>
                    <td colspan = "2"><br/><br/><br/><br/><br/><br/>Authorized Signatory<br/><br/><br/><br/><span style = "font-weight:800;"><? echo $debit_note['dn_oc_name']; ?></span></td>
                    <td></td>
                </tr>
            </table>
            <!--<span class = "termsText">Debit Note Terms : <br/><? echo $debit_note['dn_terms']; ?><br/><? echo $debit_note['dn_additional_terms']; ?>
            </span>-->
<footer style = "position:absolute;bottom:0;right:0;padding-right:20px;padding-bottom:15px;font-size:11px;font-style:italic;">
                Powered By Quant ERP
            </footer>
        </div>

    </div>


    <style>
        @media print {
            body * {
                visibility: hidden;
            }
            #section-to-print, #section-to-print * {
                visibility: visible;
                overflow:hidden;
            }
            #section-to-print {
                position: absolute;
                left: 0;
                top: 0;
                size: auto;   /* auto is the initial value */
                margin: 0;
                border: initial;
                border-radius: initial;
                width: 100%;
                min-height: initial;
                box-shadow: initial;
                background: initial;
                page-break-after: always;
            }
            .grayTBack {
                visibility: visible;
                background-color: #ccc!important;
                color: black;
                font-weight:700;
                -webkit-print-color-adjust: exact;
                border: solid #ccc!important;
            }
            
            .gray-border{
                border: 1px solid #ccc;
                overflow:visible!important;
                -webkit-print-color-adjust: exact;
            }
        }
        @page 
        {
            size:  auto;   /* auto is the initial value */
            margin: 0mm;  /* this affects the margin in the printer settings */
        }

        .termsText{
            font-size: 10px;
        }
        .productList{
            border: 1px solid #FFFFFF;
            text-align: center;
            font-size: 11px;
        }
        .whiteTBack {
            background-color: #FFFFFF;
            color: black;
            font-weight:700;
        }
        .grayTBack {
            background-color: #ccc;
            color: black;
            font-weight:700;
        }
        .debitNoteTable {
            font-size: 11px;
            width: 100%;
            max-width: 100%;
            table-layout: fixed;
            font-family:"arial";
            margin-bottom:5px; 
            padding:10px;
        }
        .gray-border{
            border: 1px solid #ccc;
            border-collapse: collapse;
        }

        .logo {
            max-width:40mm; max-height: 20mm;
            text-align: center;
        }
        .logo_container {
            /*text-align: center;*/
        }
        #debitNoteHeader {
            font-size: 20px;
            font:"arial";
            width:100%;
            text-align: center;
            font-family:"arial";
            font-weight: bold;
            display: block;
        }
        hr {
            border: 3px solid #000000;
            margin-top:3px;
        }
        .debitNoteField {
            font-size: 11px;	
        }
        .debitNoteFieldValue {
            font-weight: bold;
            font-size: 12px;
        }
        td {
            padding:3px;
        }
        hr {
            margin-bottom: 5px;
        }
    </style>