
<div ng-controller="proFormaController" ng-init="load()"></div>
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/ProForma/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Sales</span> - Pro-forma Invoice</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!/ProForma/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-home2 position-left"></i>Sales</li>
            <li><a href="#!/ProForma/all"><i class="icon-city position-left"></i> Pro-forma Invoice</a></li>
            <li class="active"><?php
                if(isset($pro_forma)){                    
                    if($pro_forma['pro_forma_status'] == 'confirm'){
                        echo 'PRF' . str_pad($pro_forma['pro_forma_id'], 5, "0", STR_PAD_LEFT);
                    } else {
                        echo "DRAFT";
                    }
                } else {
                    echo "New";
                }
                ?></li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Form validation -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Pro-forma Invoice</h5>

        </div>


        <div class="panel-body">
            <?php
            $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id'=>'merpDocForm');
            if (isset($pro_forma))
                echo form_open('web/ProForma/save/' . $pro_forma['pro_forma_id'], $attributes);
            else
                echo form_open('web/ProForma/save', $attributes);
            ?>
            <p class="content-group-lg">A Pro-Forma Invoice is a preliminary Invoice shared with Customers for confirmation prior to Sales. It is a good practice to preview the Pro-Forma before confirming it.</p>

            <input name="pro_forma_status" type="text" readonly style = "display:none"
                                   value = "<?php if (isset($pro_forma)) echo $pro_forma['pro_forma_status']; else echo "new"; ?>">
                                   
            <fieldset class="content-group">
                <legend class="text-bold">Customer Details</legend>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Customer Name <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control customer_names" tabindex="1" name="pro_forma_linked_company_display_name" type="text" required onkeydown= "resetProFormaLinkedCompanyId();"
                                   value = "<?php if (isset($pro_forma)) echo $pro_forma['pro_forma_linked_company_display_name']; ?>">
                        </div>
                    </div>		
                    <div class="form-group col-lg-6 has-feedback has-feedback-left">
                        <label class="control-label col-lg-3">Company ID</label>
                        <div class="col-lg-9">
                            <input class="form-control" style = "font-size:12px;padding-left:40px;" name="pro_forma_linked_company_id" type="text" readonly
                                   value = "<?php if (isset($pro_forma)) echo $pro_forma['pro_forma_linked_company_id']; ?>">
                            <div class="form-control-feedback" style = "left:10px">
                                <span style = "font-size:12px;padding-left:7px;"> COMP</span>
                            </div>
                        </div>
                    </div>	
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Invoice Name <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" id = "" tabindex="2" name="pro_forma_linked_company_invoice_name" type="text"
                                   value = "<?php if (isset($pro_forma)) echo $pro_forma['pro_forma_linked_company_invoice_name']; ?>" required>
                        </div>
                    </div>	
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Date</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name="pro_forma_date" id = "datePicker"
                                   value = "<?php if (isset($pro_forma)) echo explode(" ", $pro_forma['pro_forma_date'])[0]; ?>">
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Billing Address</label>
                        <div class="col-lg-9">
                            <textarea rows = "3" class="form-control" tabindex="2" name="pro_forma_linked_company_billing_address" ><?php if (isset($pro_forma)) echo $pro_forma['pro_forma_linked_company_billing_address']; ?></textarea>
                        </div>
                    </div>	

                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Shipping Address</label>
                        <div class="col-lg-9">
                            <textarea rows = "3" class="form-control" tabindex="2" name="pro_forma_linked_company_shipping_address" ><?php if (isset($pro_forma)) echo $pro_forma['pro_forma_linked_company_shipping_address']; ?></textarea>
                        </div>
                    </div>	
                </div>
                <div class="col-lg-12">										
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Billing State <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="select2 form-control" name="pro_forma_linked_company_billing_state_id" id = "pro_forma_linked_company_billing_state_id" required>
                                <option value = "">Choose a state</option>
                                <optgroup label="States">
                                    <?php
                                    foreach ($states as $state) {
                                        ?>
                                        <option  
                                        <?php
                                        if (isset($pro_forma)) {
                                            if ($pro_forma['pro_forma_linked_company_billing_state_id'] == $state['state_tin_number'])
                                                echo " selected ";
                                        }
                                        ?> 
                                            value="<? echo $state['state_tin_number']; ?>"><? echo $state['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </optgroup>
                                <optgroup label = "Union Territories">
                                    <?php
                                    foreach ($union_territories as $union_territory) {
                                        ?>
                                        <option 
                                        <?php
                                        if (isset($pro_forma)) {
                                            if ($pro_forma['pro_forma_linked_company_billing_state_id'] == $union_territory['state_tin_number'])
                                                echo " selected ";
                                        }
                                        ?> 																	
                                            value="<? echo $union_territory['state_tin_number'] ?>"><? echo $union_territory['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>

                                </optgroup>
                            </select>
                        </div>
                    </div>	

                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Shipping State <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="select2 form-control" name="pro_forma_linked_company_shipping_state_id" id = "pro_forma_linked_company_shipping_state_id" required>
                                <option value = "">Choose a state</option>
                                <optgroup label="States">
                                    <?php
                                    foreach ($states as $state) {
                                        ?>
                                        <option  
                                        <?php
                                        if (isset($pro_forma)) {
                                            if ($pro_forma['pro_forma_linked_company_shipping_state_id'] == $state['state_tin_number'])
                                                echo "selected";
                                        }
                                        ?> 
                                            value="<? echo $state['state_tin_number'] ?>"><? echo $state['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </optgroup>
                                <optgroup label = "Union Territories">
                                    <?php
                                    foreach ($union_territories as $union_territory) {
                                        ?>
                                        <option 
                                        <?php
                                        if (isset($pro_forma)) {
                                            if ($pro_forma['pro_forma_linked_company_shipping_state_id'] == $union_territory['state_tin_number'])
                                                echo "selected";
                                        }
                                        ?> 																	
                                            value="<? echo $union_territory['state_tin_number'] ?>"><? echo $union_territory['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>

                                </optgroup>
                            </select>
                        </div>
                    </div>												
                </div>
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Other Details</legend>	
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">GST Supply State<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="select2 form-control" name="pro_forma_linked_company_gst_supply_state_id" id = "pro_forma_linked_company_gst_supply_state_id" onchange = "updateTaxesForProForma(); refreshAndReIndexProFormaTable(true); calculateProFormaNumbers();" required>
                                <option value = "">Choose a state</option>
                                <optgroup label="States">
                                    <?php
                                    foreach ($states as $state) {
                                        ?>
                                        <option 
                                        <?php
                                        if (isset($pro_forma)) {
                                            if ($pro_forma['pro_forma_linked_company_gst_supply_state_id'] == $state['state_tin_number'])
                                                echo "selected";
                                        }
                                        ?> value="<? echo $state['state_tin_number']; ?>"><? echo $state['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </optgroup>
                                <optgroup label = "Union Territories">
                                    <?php
                                    foreach ($union_territories as $union_territory) {
                                        ?>
                                        <option 
                                        <?php
                                        if (isset($pro_forma)) {
                                            if ($pro_forma['pro_forma_linked_company_gst_supply_state_id'] == $union_territory['state_tin_number'])
                                                echo "selected";
                                        }
                                        ?> value="<? echo $union_territory['state_tin_number']; ?>"><? echo $union_territory['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>

                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Tax Type</label>
                        <div class="col-lg-9">
                            <select class="form-control select" name="pro_forma_tax_type" id = "tax_type" onchange = "updateTaxesForProForma(); refreshAndReIndexProFormaTable(); calculateProFormaNumbers();">
                                <option value = "exclusive" 
                                    <? if (isset($pro_forma)) { 
                                        if ($pro_forma['pro_forma_tax_type'] == 'exclusive')
                                            echo "selected"; 
                                        } else if($oc_sales_default_tax_type == 'exclusive'){ 
                                            echo "selected";
                                        } ?>>Exclusive</option>
                                <option value = "inclusive" <? if (isset($pro_forma)) { 
                                    if ($pro_forma['pro_forma_tax_type'] == 'inclusive') 
                                        echo "selected"; 
                                    } else if($oc_sales_default_tax_type == 'inclusive'){ 
                                        echo "selected";
                                    } ?>>Inclusive</option>													
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Agent</label>
                        <div class="col-lg-9">
                            <select class="form-control select2" id = "pro_forma_linked_broker_id" name="pro_forma_linked_broker_id" tabindex="3" onchange = "updateBrokerCommissionForProForma()">
                                <option value = "">Choose a broker</option>
                                <?php foreach ($brokers as $broker): ?>
                                    <option data-commission = "<?php echo $broker['broker_commission'] ?>" value = "<? echo $broker['broker_id']; ?>" 
                                            <?php if (isset($pro_forma)) if ($pro_forma['pro_forma_linked_broker_id'] == $broker['broker_id']) echo "selected"; ?>><? echo $broker['broker_name']; ?></option>
                                        <?php endforeach; ?>
                            </select>
                        </div>
                    </div>	
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Agent Commission</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name = "pro_forma_linked_broker_commission" id="broker_commission" type="number" step = 0.01
                                   value = "<?php if (isset($pro_forma)) echo $pro_forma['pro_forma_linked_broker_commission']; ?>">
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Transporter</label> 
                        <div class="col-lg-9">
                            <select class="form-control select2" id = "transporter_id" name="pro_forma_linked_transporter_id">
                                <option value = "">Choose a transporter</option>
                                <?php foreach ($transporters as $transporter): ?>
                                    <option 
                                    <?php
                                    if (isset($pro_forma)) {
                                        if ($pro_forma['pro_forma_linked_transporter_id'] == $transporter['transporter_id'])
                                            echo "selected";
                                    }
                                    ?> 
                                        value = "<? echo $transporter['transporter_id']; ?>"><? echo $transporter['transporter_name']; ?></option>
                                    <?php endforeach; ?>
                            </select>
                        </div>
                    </div>	
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">LR Number</label>
                        <div class="col-lg-9">
                            <input class="form-control" name = "pro_forma_lr_number" type="text"
                                   value = "<?php if (isset($pro_forma)) echo $pro_forma['pro_forma_lr_number']; ?>">
                        </div>
                    </div>	
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Payment Terms</label>
                        <div class="col-lg-9">
                            <select class="form-control select" name="pro_forma_linked_company_payment_term_id" id = "pro_forma_linked_company_payment_term_id" tabindex = "10">
                                <?
                                foreach ($payment_terms as $payment_term) {
                                    ?>
                                    <option 
                                    <?php
                                    if (set_value('pro_forma_linked_company_payment_term_id') == $payment_term['payment_term_id'])
                                        echo "selected";
                                    else if (isset($pro_forma)) {
                                        if ($pro_forma['pro_forma_linked_company_payment_term_id'] == $payment_term['payment_term_id'])
                                            echo "selected";
                                    }
                                    ?> value="<? echo $payment_term['payment_term_id']; ?>"><? echo $payment_term['payment_term_display_text'] ?>
                                    </option>
                                    <?
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Order Reference</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name="pro_forma_order_reference" type="text"
                                   value = "<?php if (isset($pro_forma)) echo $pro_forma['pro_forma_order_reference']; ?>">
                        </div>
                    </div>	
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Notes</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name = "pro_forma_notes" type="text"
                                   value = "<?php if (isset($pro_forma)) echo $pro_forma['pro_forma_notes']; ?>">
                        </div>
                    </div>

                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Linked Employee</label>
                        <div class="col-lg-9">
                            <select class="form-control select2" name="pro_forma_linked_employee_id" tabindex="3">
                                <option value = "">Choose a employee</option>
                                <?php foreach ($employees as $employee): ?>
                                    <option  
                                    <?php
                                    if (isset($pro_forma)) {
                                        if ($pro_forma['pro_forma_linked_employee_id'] == $employee['employee_id'])
                                            echo "selected";
                                    }
                                    ?> value = "<? echo $employee['employee_id']; ?>"><? echo $employee['employee_name']; ?></option>
                                    <?php endforeach; ?>
                            </select>
                        </div>
                    </div>	
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Revision Number</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name="pro_forma_revision_number" type="text"
                                   value = "<?php if (isset($pro_forma)) echo $pro_forma['pro_forma_revision_number']; ?>">

                        </div>
                    </div>	
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Revision Date</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name="pro_forma_revision_date" id ="pro_forma_revision_date"
                                   value = "<?php if (isset($pro_forma)) echo explode(" ", $pro_forma['pro_forma_revision_date'])[0]; ?>">
                        </div>
                    </div>
                </div>
                <div class ="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Pro Forma Terms</label>
                        <div class="col-lg-9">
                            <textarea name = "pro_forma_terms" class="form-control" rows="5"><?php if (isset($pro_forma)) 
                                                     echo $pro_forma['pro_forma_terms']; 
                                                  else 
                                                      echo $oc_terms; ?>
                            </textarea>
                        </div>
                    </div>
                </div>
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Product Details</legend>
                <table class="table proFormaDatatable">
                    <thead>
                        <tr>
                            <th>Sr No</th>
                            <th>Product</th>
                            <th>HSN/SAC code</th>
                            <th>Tax</th>
                            <th>Rate</th>
                            <th>Quantity</th>
                            <th>Inventory</th>
                            <th>Discount ( % )</th>
                            <th>Amount</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($pro_forma_entries))
                            foreach ($pro_forma_entries as $pro_forma_entry) {
                                ?>
                                <tr>
                                    <td><? echo $pro_forma_entry['pfe_entry_id']; ?></td>

                                    <td>
                                        <select class="dtSelect2" onchange = "updateProductsForProForma(this, true);calculateProFormaNumbers();">
                                            <? foreach ($products as $product) { ?>
                                                <option value="<? echo $product['product_id']; ?>" <? if ($pro_forma_entry['pfe_linked_product_id'] == $product['product_id']) echo "selected"; ?> ><? echo $product['product_name']; ?></option>

                                            <? } ?>
                                        </select>
                                        <input name = "pro_forma_entries[<? echo $pro_forma_entry['pfe_entry_id']; ?>][<? echo $pro_forma_entry['pfe_linked_product_id']; ?>][0]" 
                                               value = "<? echo $pro_forma_entry['pfe_product_name']; ?>" style = "display:none" ></input>
                                    </td>		

                                    <td><a href = "master#!/product/hsn/<? echo $pro_forma_entry['pfe_product_hsn_code']; ?>"><? echo $pro_forma_entry['pfe_product_hsn_code']; ?></a>
                                        <input name = "pro_forma_entries[<? echo $pro_forma_entry['pfe_entry_id']; ?>][<? echo $pro_forma_entry['pfe_linked_product_id']; ?>][2]" 
                                               value = "<? echo $pro_forma_entry['pfe_product_hsn_code']; ?>" style = "display:none" ></input>
                                    </td>

                                    <td>
                                        <select name = "pro_forma_entries[<? echo $pro_forma_entry['pfe_entry_id']; ?>][<? echo $pro_forma_entry['pfe_linked_product_id']; ?>][1]"  class="form-control dtSelect"  onchange = "calculateProFormaNumbers()">
                                            <?
                                            $taxes = $intrastate_taxes;
                                            ;
                                            if ($oc_state_id != $pro_forma['pro_forma_linked_company_gst_supply_state_id']) {
                                                $taxes = $interstate_taxes;
                                            }

                                            foreach ($taxes as $tax) {
                                                ?>
                                                <option 
                                                <?php
                                                if ($pro_forma_entry['pfe_tax_id_applicable'] == $tax['tax_id'])
                                                    echo "selected";
                                                ?> value="<? echo $tax['tax_id']; ?>" tax_value = "<? echo $tax['tax_percent']; ?>"><? echo $tax['tax_name'] ?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>

                                    <td>
                                        <input name = "pro_forma_entries[<? echo $pro_forma_entry['pfe_entry_id']; ?>][<? echo $pro_forma_entry['pfe_linked_product_id']; ?>][3]"
                                               class = "form-control" type = "number" min = 0.01 step = 0.01 value = "<? echo $pro_forma_entry['pfe_product_rate']; ?>" onchange = "calculateProFormaNumbers()"></input>
                                    </td>

                                    <td>
                                        <input name = "pro_forma_entries[<? echo $pro_forma_entry['pfe_entry_id']; ?>][<? echo $pro_forma_entry['pfe_linked_product_id']; ?>][4]"
                                               id = "quantity" class = "form-control" type = "number" step = 0.01 value = "<? echo $pro_forma_entry['pfe_product_quantity']; ?>" min = 0  onchange = "calculateProFormaNumbers()"></input>
                                        <input name = "pro_forma_entries[<? echo $pro_forma_entry['pfe_entry_id']; ?>][<? echo $pro_forma_entry['pfe_linked_product_id']; ?>][5]" value = "<? echo $pro_forma_entry['pfe_product_uqc_id']; ?>" style = "display:none" ></input>
                                        <input name = "pro_forma_entries[<? echo $pro_forma_entry['pfe_entry_id']; ?>][<? echo $pro_forma_entry['pfe_linked_product_id']; ?>][6]" value = "<? echo $pro_forma_entry['pfe_product_uqc_text']; ?>" style = "display:none" ></input>
                                    </td>

                                    <td>NA</td>
                                    <td>
                                        <input name = "pro_forma_entries[<? echo $pro_forma_entry['pfe_entry_id']; ?>][<? echo $pro_forma_entry['pfe_linked_product_id']; ?>][7]"
                                               min = 0 max = 100 id = "discount" class = "form-control" type = "number" step = 0.01 value = "<? echo $pro_forma_entry['pfe_discount']; ?>" onchange = "calculateProFormaNumbers()"></input>
                                    </td>

                                    <td>
                                        0
                                    </td>

                                    <td>
                                        <?
                                        if ($pro_forma_entry['pfe_entry_id'] != 1)
                                            echo '<a href="javascript: void(0)" onclick = "deleteRowForProForma(this);refreshAndReIndexProFormaTable();calculateProFormaNumbers();"><i class="glyphicon glyphicon-trash"></i></a>';
                                        ?>
                                    </td>
                                </tr>

                                <?
                            }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>Sr No</td>
                            <td>Product</td>
                            <td>HSN/SAC code</td>
                            <td>Tax</td>
                            <td>Rate</td>
                            <td>Quantity</td>
                            <td>Inventory</td>
                            <td>Discount ( % )</td>
                            <td>Amount</td>
                            <td class="text-center">Actions</td>
                        </tr>
                    </tfoot>
                </table>
                <div class="form-group col-lg-12">
                    <a onclick = "updateProductsForProForma(null);calculateProFormaNumbers();" class="btn btn-link btn-float has-text text-success"><span>+Add New Row</span></a>
                </div>										
	
                <div class="form-group col-lg-8">
                    <fieldset class="content-group">
                    <legend class="text-bold">Additional Charges</legend>
                        <table class="table" id = "proFormaChargesDataTable">
                        <thead>
                            <tr>
                                <th>Charge</th>
                                <th>Taxable Amount</th>
                                <th>Tax</th>
                                <th>Amount</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody> 
                            <?php
                        if (isset($pro_forma_charge_entries))
                            foreach ($pro_forma_charge_entries as $pro_forma_charge_entry) {
                                ?>
                                <tr>
                                    <td>
                                        <select class="dtSelect2" onchange = "updateProFormaCharges(this);calculateProFormaNumbers();">
                                            <? foreach ($charges as $charge) { ?>
                                                <option value="<? echo $charge['charge_id']; ?>" <? 
                                                    if ($pro_forma_charge_entry['pfce_charge_id'] == $charge['charge_id']){
                                                        $charge_name = $charge['charge_name'];  //capture this in case charge name has changed
                                                        echo "selected"; 
                                                    } ?>>
                                                    <? echo $charge['charge_name']; ?></option>

                                            <? } ?>
                                        </select>
                                        <input name = "charge_entries[<? echo $pro_forma_charge_entry['pfce_entry_id']; ?>][<? echo $pro_forma_charge_entry['pfce_charge_id']; ?>][0]" 
                                               value = "<? 
                                               
                                               if(!empty($charge_name)) 
                                                        echo $charge_name; 
                                                    else 
                                                        echo $pro_forma_charge_entry['pfce_charge_name']; ?>" style = "display:none" ></input>
                                    </td>		

                                    <td>
                                        <input name = "charge_entries[<? echo $pro_forma_charge_entry['pfce_entry_id']; ?>][<? echo $pro_forma_charge_entry['pfce_charge_id']; ?>][1]" value = "<? echo $pro_forma_charge_entry['pfce_taxable_amount']; ?>" 
                                               class = "form-control" type = "number" min = "0.01" step ="0.01" style = "min-width:100px" onchange = "calculateProFormaNumbers();"></input>
                                    </td>
                                    <td>
                                        <select name = "charge_entries[<? echo $pro_forma_charge_entry['pfce_entry_id']; ?>][<? echo $pro_forma_charge_entry['pfce_charge_id']; ?>][2]"  
                                                class="form-control dtSelect"  onchange = "calculateProFormaNumbers()">
                                            <?
                                            $taxes = $intrastate_taxes;
                                            
                                            if ($oc_state_id != $pro_forma['pro_forma_linked_company_gst_supply_state_id']) {
                                                $taxes = $interstate_taxes;
                                            }

                                            foreach ($taxes as $tax) {
                                                ?>
                                                <option 
                                                <?php
                                                if ($pro_forma_charge_entry['pfce_tax_id_applicable'] == $tax['tax_id'])
                                                    echo "selected";
                                                ?> value="<? echo $tax['tax_id']; ?>" tax_value = "<? echo $tax['tax_percent']; ?>"><? echo $tax['tax_name'] ?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>


                                    <td style = "white-space: nowrap;">
                                        0
                                    </td>

                                    <td>
                                        <a href="javascript: void(0)" onclick = "deleteProFormaChargeRow(this);refreshAndReIndexEntireProFormaTable();calculateProFormaNumbers();"><i class="glyphicon glyphicon-trash"></i></a>
                                    </td>
                                </tr>

                                <?
                            }
                        ?>
                        </tbody>
                        </table>
                        <a onclick = "updateProFormaCharges(null);calculateProFormaNumbers();" class="btn btn-link btn-float has-text text-success"><span>+Add New Charge</span></a>
                    </fieldset>
                </div>
                <div class="form-group col-lg-4">
                    <div class="form-group col-lg-12">
                        <label class="control-label col-lg-6" id = "subTotalLabel">Error !</label>
                        <div class="col-lg-6">
                            <h6 class = "no-margin text-bold"><span id = "totalProFormaSubTotalAmount"></span><h6>
                                    </div>
                                    </div>	

                                    <div id = "taxDiv">

                                    </div>

                                    <div class="form-group col-lg-12 has-feedback has-feedback-left"  style = "display:none">
                                        <label class="control-label col-lg-6 ">Transport Charges</label>
                                        <div class="col-lg-6">
                                            <input class="form-control" name = "pro_forma_transport_charges" id = "transportationCharges" type="number" min = 0 step = 0.01 onchange = "calculateProFormaNumbers()" 
                                                   value = "<?php
                                                   if (isset($pro_forma) && trim($pro_forma['pro_forma_transport_charges']) != "") {
                                                       echo $pro_forma['pro_forma_transport_charges'];
                                                   } else {
                                                       echo "0";
                                                   }
                                                   ?>">
                                            <div class="form-control-feedback" style = "left:10px">
                                                <span style = "font-size:16px;"> ₹</span>
                                            </div>
                                        </div>
                                    </div>	

                                    <div class="form-group col-lg-12  has-feedback has-feedback-left" style = "margin-top:15px">
                                        <label class="control-label col-lg-6">Adjustments</label>
                                        <div class="col-lg-6">
                                            <input class="form-control" name = "pro_forma_adjustments" id = "adjustments" type="number" step = 0.01 onchange = "calculateProFormaNumbers()" 
                                                   value = "<?php
                                                   if (isset($pro_forma) && trim($pro_forma['pro_forma_adjustments']) != "") {
                                                       echo $pro_forma['pro_forma_adjustments'];
                                                   } else {
                                                       echo "0";
                                                   }
                                                   ?>">
                                            <div class="form-control-feedback" style = "left:10px">
                                                <span style = "font-size:16px;"> ₹</span>
                                            </div>
                                        </div>
                                    </div>	

                                    <div class="form-group col-lg-12">
                                        <label class="control-label col-lg-6">Pro-forma Total</label>
                                        <div class="col-lg-6">
                                            <h6 class = "no-margin text-bold"><span id = "proFormaTotal"></span><h6>
                                                    <input type = "text" name = "pro_forma_amount" id = "proFormaAmount" style = "display:none">
                                                    </div>
                                                    </div>	
                                                    </div>	
                                                    </fieldset>

                                                    <div class="text-right">
                                                        <a href="#!/ProForma/all"><button class="btn btn-default">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                                                        <? if ($this->session->userdata('access_controller')->is_access_granted('pro_forma', 'save')) { ?>
                                                        <? if(!isset($pro_forma) || $pro_forma['pro_forma_status'] == 'draft') {?>
                                                        <button onclick="submitDocForm('pro_forma', 'draft')" id = "buttonSaveDraftProForma" name="transaction" value="draft" class="btn btn-default">Save as draft <i class="icon-reload-alt position-right"></i></button>
                                                        <button onclick="submitDocForm('pro_forma', 'previewDraft')" name="transaction" value="previewDraft" class="btn btn-default">Preview<i class="icon-copy position-right"></i></button>
                                                        <? } ?>
                                                        <button onclick="confirmAndContinueForDocForm('pro_forma', 'confirm')" id = "buttonSaveConfirmProForma" name="transaction" value="confirm" class="btn btn-primary">Confirm <i class="icon-arrow-right14 position-right"></i></button>
                                                        <? } ?>
                                                    </div>
                                                    </form>
                                                    </div>
                                                    </div>
                                                    <!-- /form validation -->

                                                    <!-- Footer -->
                                                    <div class="footer text-muted">
                                                        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
                                                    </div>
                                                    <!-- /footer -->