
<div ng-controller="creditNoteController" ng-init="load()"></div>
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/creditNote/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Sales</span> - Credit Note</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!/creditNote/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!"><i class="icon-home2 position-left"></i>Sales</a></li>
            <li><a href="#!/creditNote/all"><i class="icon-city position-left"></i> Credit Note</a></li>
            <li class="active"><?php
                if(isset($credit_note)){                    
                    if($credit_note['cn_status'] == 'confirm'){
                        echo 'CDN' . str_pad($credit_note['cn_id'], 5, "0", STR_PAD_LEFT);
                    } else {
                        echo "DRAFT";
                    }
                } else {
                    echo "New";
                }
                ?></li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Form validation -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Credit Note</h5>

        </div>


        <div class="panel-body">
            <?php
            $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id'=>'merpDocForm');
            if (isset($credit_note))
                echo form_open('web/CreditNote/save/' . $credit_note['cn_id'], $attributes);
            else
                echo form_open('web/CreditNote/save', $attributes);
            ?>
            <p class="content-group-lg">A Credit Note is a formal document of return of sold goods received from Customers. Book all returns to ensure accurate Accounting & GST reporting.</p>

            <input name="cn_status" type="text" readonly style = "display:none"
                                   value = "<?php if (isset($credit_note)) echo $credit_note['cn_status']; else echo "new"; ?>">
            
            <fieldset class="content-group">
                <legend class="text-bold">Customer Details</legend>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Credit Against <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <label class="radio-inline">
                                <input type="radio" name="credit_against" id = "credit_against_sales" onchange = "clearCreditNoteForm();toggleElementsForCreditNote();refreshCustomerListForCreditNote(this);updateCustomerIdForCreditNoteForm();refreshInvoiceListForCreditNote();calculateCreditNoteNumbers()" value = "sales" <?php
                                if (isset($credit_note)) {
                                    if (isset($credit_note['cn_linked_invoice_id']))
                                        echo 'checked ';
                                    if (isset($credit_note['cn_linked_invoice_id']) || isset($credit_note['cn_linked_credit_note_id']))
                                        echo ' disabled';
                                } else
                                    echo "checked";
                                ?>>
                                Sales
                            </label>

                            <label class="radio-inline">
                                <input type="radio" value = "purchase" name="credit_against" id = "credit_against_purchase" onchange = "clearCreditNoteForm();toggleElementsForCreditNote();refreshCustomerListForCreditNote(this);updateCustomerIdForCreditNoteForm();refreshInvoiceListForCreditNote();calculateCreditNoteNumbers();" 
                                <?php
                                if (isset($credit_note)) {
                                    if (isset($credit_note['cn_linked_credit_note_id']))
                                        echo 'checked ';
                                    if (isset($credit_note['cn_linked_invoice_id']) || isset($credit_note['cn_linked_credit_note_id']))
                                        echo ' disabled';
                                }
                                ?> disabled>
                                Purchase
                            </label>
                            <?
                            if (isset($credit_note['cn_linked_invoice_id']) 
                                    || isset($credit_note['cn_linked_credit_note_id'])) {
                                if (isset($credit_note['cn_linked_invoice_id'])) {
                                    $credit_against = 'sales';
                                } else if (isset($credit_note['cn_linked_credit_note_id'])) {
                                    $credit_against = 'purchase';
                                }

                                echo '<input name="credit_against" style = "display:none" type="text" readonly value = "' . $credit_against . '">';
                            };
                            ?>
                        </div>		
                    </div>	
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Company Name <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <?
                            if (isset($credit_note)) {
                                echo '<input class="form-control" type="text" readonly value = "' . $credit_note['cn_linked_company_invoice_name'] . '">';
                            } else {
                                echo '<select class="select2 form-control" id = "credit_note_linked_company_name" 
                                    onchange = "clearCreditNoteForm();updateCustomerIdForCreditNoteForm();refreshInvoiceListForCreditNote();calculateCreditNoteNumbers();">
														<option value = "">Choose a company</option>
													</select>';
                            }
                            ?>

                        </div>
                    </div>

                </div>
                <div class = "col-lg-12">
                    <div class="form-group col-lg-6 has-feedback has-feedback-left">
                        <label class="control-label col-lg-3">Company ID</label>
                        <div class="col-lg-9">
                            <input class="form-control" style = "font-size:12px;padding-left:40px;" id = "credit_note_linked_company_id" name="cn_linked_company_id" type="text" readonly
                                   value = "<?php if (isset($credit_note)) echo $credit_note['cn_linked_company_id']; ?>">
                            <div class="form-control-feedback" style = "left:10px">
                                <span style = "font-size:12px;padding-left:7px;"> COMP</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Invoice Number <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <?
                            if (isset($credit_note)) {
                                if (isset($credit_note['cn_linked_invoice_id'])) {
                                    $cn_linked_supplementary_invoice_id = $credit_note['cn_linked_invoice_id'];
                                    $cn_linked_supplementary_invoice_id_text = 'INV' . str_pad($cn_linked_supplementary_invoice_id, 5, "0", STR_PAD_LEFT);
                                } else if (isset($credit_note['cn_linked_credit_note_id'])) {
                                    $cn_linked_supplementary_invoice_id = $credit_note['cn_linked_credit_note_id'];
                                    $cn_linked_supplementary_invoice_id_text = 'PUR' . str_pad($cn_linked_supplementary_invoice_id, 5, "0", STR_PAD_LEFT);
                                }

                                echo '<input class="form-control" type="text" readonly value = "' . $cn_linked_supplementary_invoice_id_text . '">
													<input name="cn_linked_supplementary_invoice_id" style = "display:none" type="text" readonly value = "' . $cn_linked_supplementary_invoice_id . '">';
                            } else {
                                echo '<select class="select2 form-control" name="cn_linked_supplementary_invoice_id" id = "credit_note_linked_invoice_id" onchange = "clearCreditNoteForm();loadInvoiceForCreditNoteForm();calculateCreditNoteNumbers();" required>
																
													</select>';
                            }
                            ?>
                        </div>
                    </div>


                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Invoice Name <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" id = "" tabindex="2" name="cn_linked_company_invoice_name" type="text"
                                   value = "<?php if (isset($credit_note)) echo $credit_note['cn_linked_company_invoice_name']; ?>" required readonly>
                        </div>
                    </div>	
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Date</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name="cn_date" id = "datePicker"
                                   value = "<?php if (isset($credit_note)) echo explode(" ", $credit_note['cn_date'])[0]; ?>">
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Billing Address</label>
                        <div class="col-lg-9">
                            <textarea rows = "3" class="form-control" tabindex="2" name="cn_linked_company_billing_address" readonly><?php if (isset($credit_note)) echo $credit_note['cn_linked_company_billing_address']; ?></textarea>
                        </div>
                    </div>	

                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Shipping Address</label>
                        <div class="col-lg-9">
                            <textarea rows = "3" class="form-control" tabindex="2" name="cn_linked_company_shipping_address" ><?php if (isset($credit_note)) echo $credit_note['cn_linked_company_shipping_address']; ?></textarea>
                        </div>
                    </div>	
                </div>
                <div class="col-lg-12">										
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Billing State</label>
                        <div class="col-lg-9">
                            <select class="select2 form-control" name="cn_linked_company_billing_state_id" id = "credit_note_linked_company_billing_state_id">
                                <option value = "">Choose a state</option>
                                <optgroup label="States">
                                    <?php
                                    foreach ($states as $state) {
                                        ?>
                                        <option  
                                        <?php
                                        if (isset($credit_note)) {
                                            if ($credit_note['cn_linked_company_billing_state_id'] == $state['state_tin_number'])
                                                echo "selected";
                                        }
                                        ?> 
                                            value="<? echo $state['state_tin_number']; ?>"><? echo $state['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </optgroup>
                                <optgroup label = "Union Territories">
                                    <?php
                                    foreach ($union_territories as $union_territory) {
                                        ?>
                                        <option 
                                        <?php
                                        if (isset($credit_note)) {
                                            if ($credit_note['cn_linked_company_billing_state_id'] == $union_territory['state_tin_number'])
                                                echo "selected";
                                        }
                                        ?> 																	
                                            value="<? echo $union_territory['state_tin_number']; ?>"><? echo $union_territory['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>

                                </optgroup>
                            </select>
                        </div>
                    </div>	

                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Shipping State</label>
                        <div class="col-lg-9">
                            <select class="select2 form-control" name="cn_linked_company_shipping_state_id" id = "credit_note_linked_company_shipping_state_id">
                                <option value = "">Choose a state</option>
                                <optgroup label="States">
                                    <?php
                                    foreach ($states as $state) {
                                        ?>
                                        <option  
                                        <?php
                                        if (isset($credit_note)) {
                                            if ($credit_note['cn_linked_company_shipping_state_id'] == $state['state_tin_number'])
                                                echo "selected";
                                        }
                                        ?> 
                                            value="<? echo $state['state_tin_number']; ?>"><? echo $state['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </optgroup>
                                <optgroup label = "Union Territories">
                                    <?php
                                    foreach ($union_territories as $union_territory) {
                                        ?>
                                        <option 
                                        <?php
                                        if (isset($credit_note)) {
                                            if ($credit_note['cn_linked_company_shipping_state_id'] == $union_territory['state_tin_number'])
                                                echo "selected";
                                        }
                                        ?> 																	
                                            value="<? echo $union_territory['state_tin_number']; ?>"><? echo $union_territory['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>

                                </optgroup>
                            </select>
                        </div>
                    </div>												
                </div>
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Other Details</legend>	
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">GST Supply State <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="select2 form-control" name="cn_linked_company_gst_supply_state_id" id = "credit_note_linked_company_gst_supply_state_id" required
                                    onchange = "updateTaxesInCreditNoteTable();refreshAndReIndexEntireCreditNoteTable(); calculateCreditNoteNumbers();">
                                <option value = "">Choose a state</option>
                                <optgroup label="States">
                                    <?php
                                    foreach ($states as $state) {
                                        ?>
                                        <option 
                                        <?php
                                        if (isset($credit_note)) {
                                            if ($credit_note['cn_linked_company_gst_supply_state_id'] == $state['state_tin_number'])
                                                echo "selected";
                                        }
                                        ?> value="<? echo $state['state_tin_number']; ?>"><? echo $state['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </optgroup>
                                <optgroup label = "Union Territories">
                                    <?php
                                    foreach ($union_territories as $union_territory) {
                                        ?>
                                        <option 
                                        <?php
                                        if (isset($credit_note)) {
                                            if ($credit_note['cn_linked_company_gst_supply_state_id'] == $union_territory['state_tin_number'])
                                                echo "selected";
                                        }
                                        ?> value="<? echo $union_territory['state_tin_number']; ?>"><? echo $union_territory['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>

                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Tax Type</label>
                        <div class="col-lg-9">
                            <select class="form-control select" name="cn_tax_type" id = "credit_note_tax_type" onchange = "updateTaxesInCreditNoteTable();refreshAndReIndexEntireCreditNoteTable(); calculateCreditNoteNumbers();">
                                <option value = "exclusive" <?php if (isset($credit_note)) if ($credit_note['cn_tax_type'] == 'exclusive') echo "selected"; ?>>Exclusive</option>
                                <option value = "inclusive" <?php if (isset($credit_note)) if ($credit_note['cn_tax_type'] == 'inclusive') echo "selected"; ?>>Inclusive</option>													
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Agent</label>
                        <div class="col-lg-9">
                            <select class="form-control select2" id = "credit_note_linked_broker_id" name="cn_linked_broker_id" tabindex="3" onchange = "updateBrokerCommissionForCreditNoteForm();">
                                <option value = "">Choose a broker</option>
                                <?php foreach ($brokers as $broker): ?>
                                    <option data-commission = "<?php echo $broker['broker_commission'] ?>" value = "<? echo $broker['broker_id']; ?>" 
                                            <?php if (isset($credit_note)) if ($credit_note['cn_linked_broker_id'] == $broker['broker_id']) echo "selected"; ?>><? echo $broker['broker_name']; ?></option>
                                        <?php endforeach; ?>
                            </select>
                        </div>
                    </div>	
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Agent Commission</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name = "cn_linked_broker_commission" id="broker_commission" type="number" step = 0.01
                                   value = "<?php if (isset($credit_note)) echo $credit_note['cn_linked_broker_commission']; ?>">
                        </div>
                    </div>	
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Transporter</label>
                        <div class="col-lg-9">
                            <select class="form-control select2" id = "transporter_id" name="cn_linked_transporter_id">
                                <option value = "">Choose a transporter</option>
                                <?php foreach ($transporters as $transporter): ?>
                                    <option 
                                    <?php
                                    if (isset($credit_note)) {
                                        if ($credit_note['cn_linked_transporter_id'] == $transporter['transporter_id'])
                                            echo "selected";
                                    }
                                    ?> 
                                        value = "<? echo $transporter['transporter_id']; ?>"><? echo $transporter['transporter_name']; ?></option>
                                    <?php endforeach; ?>
                            </select>
                        </div>
                    </div>	
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">LR Number</label>
                        <div class="col-lg-9">
                            <input class="form-control" name = "cn_lr_number" type="text"
                                   value = "<?php if (isset($credit_note)) echo $credit_note['cn_lr_number']; ?>">
                        </div>
                    </div>	
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Payment Terms</label>
                        <div class="col-lg-9">
                            <select class="form-control select" name="cn_linked_company_payment_term_id" id = "credit_note_linked_company_payment_term_id" tabindex = "10">
                                <?
                                foreach ($payment_terms as $payment_term) {
                                    ?>
                                    <option 
                                    <?php
                                    if (set_value('credit_note_linked_company_payment_term_id') == $payment_term['payment_term_id'])
                                        echo "selected";
                                    else if (isset($credit_note)) {
                                        if ($credit_note['cn_linked_company_payment_term_id'] == $payment_term['payment_term_id'])
                                            echo "selected";
                                    }
                                    ?> value="<? echo $payment_term['payment_term_id']; ?>"><? echo $payment_term['payment_term_display_text'] ?>
                                    </option>
                                    <?
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Order Reference</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name="cn_order_reference" type="text"
                                   value = "<?php if (isset($credit_note)) echo $credit_note['cn_order_reference']; ?>">
                        </div>
                    </div>	
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Notes</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name = "cn_notes" type="text"
                                   value = "<?php if (isset($credit_note)) echo $credit_note['cn_notes']; ?>">
                        </div>
                    </div>

                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Linked Employee</label>
                        <div class="col-lg-9">
                            <select class="form-control select2" name="cn_linked_employee_id" tabindex="3">
                                <option value = "">Choose a employee</option>
                                <?php foreach ($employees as $employee): ?>
                                    <option  
                                    <?php
                                    if (isset($credit_note)) {
                                        if ($credit_note['cn_linked_employee_id'] == $employee['employee_id'])
                                            echo "selected";
                                    }
                                    ?> value = "<? echo $employee['employee_id']; ?>"><? echo $employee['employee_name']; ?></option>
                                    <?php endforeach; ?>
                            </select>
                        </div>
                    </div>	
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Return Reason <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="form-control select2" name="cn_sir_id" required>
                                <option value = "">Choose Reason For Return</option>
                                <?php foreach ($supplementary_invoice_reasons as $supplementary_invoice_reason): ?>
                                    <option 
                                    <?php
                                    if (isset($credit_note)) {
                                        if ($credit_note['cn_sir_id'] == $supplementary_invoice_reason['sir_id'])
                                            echo "selected";
                                    }
                                    ?> 
                                        value = "<? echo $supplementary_invoice_reason['sir_id']; ?>"><? echo $supplementary_invoice_reason['sir_text']; ?></option>
                                    <?php endforeach; ?>
                            </select>
                        </div>
                    </div>	
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Write Off Stock</label>
                        <div class="col-lg-9">
                            <select class="form-control select" name="cn_write_off_stock">
                                <option value = "no" <? if (isset($credit_note)) {
                                        if ($credit_note['cn_write_off_stock'] == 0)
                                            echo "selected";
                                    } ?>>No</option>
                                <option value = "yes" <? if (isset($credit_note)) {
                                        if ($credit_note['cn_write_off_stock'] == 1)
                                            echo "selected";
                                    } ?> disabled>Yes</option>
                            </select>
                        </div>
                    </div>	
                </div>
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Product Details</legend>
                <table class="table creditNoteDatatable">
                    <thead>
                        <tr>
                            <th>Sr No</th>
                            <th>Product</th>
                            <th>HSN/SAC code</th>
                            <th>Tax</th>
                            <th>Rate</th>
                            <th>Quantity</th>
                            <th>Discount ( % )</th>
                            <th>Amount</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($credit_note_entries))
                            foreach ($credit_note_entries as $credit_note_entry) {
                                ?>
                                <tr>
                                    <td><? echo $credit_note_entry['cne_entry_id']; ?></td>

                                    <td>
                                        <? echo $credit_note_entry['cne_product_name']; ?>
                                        <input style = "display:none" name = "credit_note_entries[<? echo $credit_note_entry['cne_entry_id']; ?>][<? echo $credit_note_entry['cne_linked_product_id']; ?>][0]" type = "text" value = "<? echo $credit_note_entry['cne_product_name']; ?>" readonly></input>
                                    </td>		

                                    <td>
                                        <a href = "master#!/product/hsn/<? echo $credit_note_entry['cne_product_hsn_code']; ?>"><? echo $credit_note_entry['cne_product_hsn_code']; ?></a>
                                        <input name = "credit_note_entries[<? echo $credit_note_entry['cne_entry_id']; ?>][<? echo $credit_note_entry['cne_linked_product_id']; ?>][1]" 
                                               value = "<? echo $credit_note_entry['cne_product_hsn_code']; ?>" style = "display:none" readonly></input>
                                    </td>

                                    <td>
                                        <input style = "display:none" name = "credit_note_entries[<? echo $credit_note_entry['cne_entry_id']; ?>][<? echo $credit_note_entry['cne_linked_product_id']; ?>][2]" tax_value = '<? echo $credit_note_entry['cne_tax_percent_applicable']; ?>' class = "form-control" type = "text" value = "<? echo $credit_note_entry['cne_tax_id_applicable']; ?>" readonly></input>
                                        <input class = "form-control" type = "text" value = "<? echo $credit_note_entry['cne_tax_name_applicable']; ?>" readonly></input>

                                    </td>

                                    <td>
                                        <input name = "credit_note_entries[<? echo $credit_note_entry['cne_entry_id']; ?>][<? echo $credit_note_entry['cne_linked_product_id']; ?>][3]"
                                               class = "form-control" type = "number" step = 0.01 value = "<? echo $credit_note_entry['cne_product_rate']; ?>" onchange = "calculateCreditNoteNumbers()" readonly></input>
                                    </td>

                                    <td>
                                        <input name = "credit_note_entries[<? echo $credit_note_entry['cne_entry_id']; ?>][<? echo $credit_note_entry['cne_linked_product_id']; ?>][4]"
                                               id = "quantity" class = "form-control" type = "number" step = 0.01 value = "<? echo $credit_note_entry['cne_product_quantity']; ?>" min = 0  onchange = "calculateCreditNoteNumbers()"></input>
                                        <input name = "credit_note_entries[<? echo $credit_note_entry['cne_entry_id']; ?>][<? echo $credit_note_entry['cne_linked_product_id']; ?>][5]" value = "<? echo $credit_note_entry['cne_product_uqc_id']; ?>" style = "display:none" ></input>
                                        <input name = "credit_note_entries[<? echo $credit_note_entry['cne_entry_id']; ?>][<? echo $credit_note_entry['cne_linked_product_id']; ?>][6]" value = "<? echo $credit_note_entry['cne_product_uqc_text']; ?>" style = "display:none" ></input>
                                    </td>

                                    <td>
                                        <input name = "credit_note_entries[<? echo $credit_note_entry['cne_entry_id']; ?>][<? echo $credit_note_entry['cne_linked_product_id']; ?>][7]"
                                               min = 0 max = 100 id = "discount" class = "form-control" type = "number" step = 0.01 value = "<? echo $credit_note_entry['cne_discount']; ?>" onchange = "calculateCreditNoteNumbers()" readonly></input>
                                    </td>

                                    <td>
                                        0
                                    </td>

                                    <td>
                                        <?
                                        if($credit_against == 'sales'){
                                            $entry_id = $credit_note_entry['cne_linked_ipe_id'];
                                        } else if($credit_against == 'purchase'){
                                            $entry_id = $credit_note_entry['cne_linked_pe_id'];
                                        } 
                                        
                                        if(!isset($entry_id) || $entry_id == NULL || $entry_id == ""){
                                            throw new Exception('Entry ID not defined');
                                        }
                                        echo '<input style = "display:none" name = "credit_note_entries[' . $credit_note_entry['cne_entry_id'] . '][' . $credit_note_entry['cne_linked_product_id'] . '][8]" type = "text" '
                                                . 'value = "' . $entry_id . '" readonly></input><a href="javascript: void(0)" onclick = "deleteCreditNoteRow(this);calculateCreditNoteNumbers();"><i class="glyphicon glyphicon-trash"></i></a>';
                                        ?>
                                    </td>
                                </tr>

                                <?php
                            }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>Sr No</td>
                            <td>Product</td>
                            <td>HSN/SAC code</td>
                            <td>Tax</td>
                            <td>Rate</td>
                            <td>Quantity</td>
                            <td>Discount ( % )</td>
                            <td>Amount</td>
                            <td class="text-center">Actions</td>
                        </tr>
                    </tfoot>
                </table>

                <br/><br/>
                <div class="form-group col-lg-8">
                    <fieldset class="content-group">
                    <legend class="text-bold">Additional Charges</legend>
                        <table class="table" id = "creditNoteChargesDatatable">
                        <thead>
                            <tr>
                                <th>Charge</th>
                                <th>Taxable Amount</th>
                                <th>Tax</th>
                                <th>Amount</th>
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody> 
                        <?php
                        if (isset($credit_note_charge_entries))
                            foreach ($credit_note_charge_entries as $credit_note_charge_entry) {
                                ?>
                                <tr>
                                    <td>
                                        <select class="select2" onchange = "updateCreditNoteCharges(this);calculateCreditNoteNumbers();">
                                            <? foreach ($charges as $charge) { ?>
                                                <option value="<? echo $charge['charge_id']; ?>" <? 
                                                    if ($credit_note_charge_entry['cnce_charge_id'] == $charge['charge_id']){
                                                        $charge_name = $charge['charge_name'];  //capture this in case charge name has changed
                                                        echo "selected"; 
                                                    } ?>>
                                                    <? echo $charge['charge_name']; ?></option>

                                            <? } ?>
                                        </select>
                                        <input name = "charge_entries[<? echo $credit_note_charge_entry['cnce_entry_id']; ?>][<? echo $credit_note_charge_entry['cnce_charge_id']; ?>][0]" 
                                               value = "<? 
                                               
                                               if(!empty($charge_name)) 
                                                        echo $charge_name; 
                                                    else 
                                                        echo $credit_note_charge_entry['cnce_charge_name']; ?>" style = "display:none" ></input>
                                    </td>		

                                    <td>
                                        <input name = "charge_entries[<? echo $credit_note_charge_entry['cnce_entry_id']; ?>][<? echo $credit_note_charge_entry['cnce_charge_id']; ?>][1]" value = "<? echo $credit_note_charge_entry['cnce_taxable_amount']; ?>" 
                                               class = "form-control" type = "number" min = "0.01" step ="0.01" style = "min-width:100px" onchange = "calculateCreditNoteNumbers();"></input>
                                    </td>
                                    <td>
                                        <select name = "charge_entries[<? echo $credit_note_charge_entry['cnce_entry_id']; ?>][<? echo $credit_note_charge_entry['cnce_charge_id']; ?>][2]"  
                                                class="form-control dtSelect"  onchange = "calculateCreditNoteNumbers()">
                                            <?
                                            $taxes = $intrastate_taxes;
                                            
                                            if ($oc_state_id != $credit_note['credit_note_linked_company_gst_supply_state_id']) {
                                                $taxes = $interstate_taxes;
                                            }

                                            foreach ($taxes as $tax) {
                                                ?>
                                                <option 
                                                <?php
                                                if ($credit_note_charge_entry['cnce_tax_id_applicable'] == $tax['tax_id'])
                                                    echo "selected";
                                                ?> value="<? echo $tax['tax_id']; ?>" tax_value = "<? echo $tax['tax_percent']; ?>"><? echo $tax['tax_name'] ?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>


                                    <td style = "white-space: nowrap;">
                                        0
                                    </td>

                                    <td>
                                        <a href="javascript: void(0)" onclick = "deleteCreditNoteChargeRow(this);calculateCreditNoteNumbers();"><i class="glyphicon glyphicon-trash"></i></a>
                                    </td>
                                </tr>

                                <?
                            }
                        ?>
                        </tbody>
                        </table>
                        <a onclick = "updateCreditNoteCharges(null);calculateCreditNoteNumbers();" class="btn btn-link btn-float has-text text-success"><span>+Add New Charge</span></a>
                    </fieldset>
                </div>
                <div class="form-group col-lg-4">
                    <div class="form-group col-lg-12">
                        <label class="control-label col-lg-6" id = "subTotalLabel"></label>
                        <div class="col-lg-6">
                            <h6 class = "no-margin text-bold"><span id = "totalCreditNoteSubTotalAmount"></span><h6>
                                    </div>
                                    </div>	

                                    <div id = "taxDiv">

                                    </div>

                                    <div class="form-group col-lg-12 has-feedback has-feedback-left" style = "display:none">
                                        <label class="control-label col-lg-6 ">Transport Charges</label>
                                        <div class="col-lg-6">
                                            <input class="form-control" name = "cn_transport_charges" id = "transportationCharges" type="number" min = 0 step = 0.01 onkeyup = "calculateCreditNoteNumbers()" 
                                                   value = "<?php
                                                   if (isset($credit_note) && trim($credit_note['cn_transport_charges']) != "") {
                                                       echo $credit_note['cn_transport_charges'];
                                                   } else {
                                                       echo "0";
                                                   }
                                                   ?>">
                                            <div class="form-control-feedback" style = "left:10px">
                                                <span style = "font-size:16px;"> ₹</span>
                                            </div>
                                        </div>
                                    </div>	

                                    <div class="form-group col-lg-12  has-feedback has-feedback-left" style = "margin-top:15px;">
                                        <label class="control-label col-lg-6">Adjustments</label>
                                        <div class="col-lg-6">
                                            <input class="form-control" name = "cn_adjustments" id = "adjustments" type="number" step = 0.01 onkeyup = "calculateCreditNoteNumbers()" 
                                                   value = "<?php
                                                   if (isset($credit_note) && trim($credit_note['cn_adjustments']) != "") {
                                                       echo $credit_note['cn_adjustments'];
                                                   } else {
                                                       echo "0";
                                                   }
                                                   ?>">
                                            <div class="form-control-feedback" style = "left:10px">
                                                <span style = "font-size:16px;"> ₹</span>
                                            </div>
                                        </div>
                                    </div>	

                                    <div class="form-group col-lg-12">
                                        <label class="control-label col-lg-6">Credit Note Total</label>
                                        <div class="col-lg-6">
                                            <h6 class = "no-margin text-bold"><span id = "creditNoteTotal"></span><h6>
                                                    <input type = "text" name = "cn_amount" id = "creditNoteAmount" style = "display:none">
                                                    </div>
                                                    </div>	
                                                    </div>	
                                                    </fieldset>

                                                    <div class="text-right">
                                                        <a href="#!/CreditNote/all"><button class="btn btn-default">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                                                        <? if ($this->session->userdata('access_controller')->is_access_granted('credit_note', 'save')) { ?>
                                                        <? if(!isset($credit_note) || $credit_note['cn_status'] == 'draft') {?>
                                                        <button onclick="submitDocForm('credit_note', 'draft')" id = "buttonSaveDraftCn" name="transaction" value="draft" class="btn btn-default">Save as draft <i class="icon-reload-alt position-right"></i></button>
                                                        <button onclick="submitDocForm('credit_note', 'previewDraft')" id = "buttonPreviewDraftCn" name="transaction" value="previewDraft" class="btn btn-default">Preview<i class="icon-copy position-right"></i></button>
                                                        <? } ?>
                                                        <button onclick="confirmAndContinueForDocForm('credit_note', 'confirm')" id = "buttonSaveConfirmCn" name="transaction" value="confirm" class="btn btn-primary">Confirm <i class="icon-arrow-right14 position-right"></i></button>
                                                        <? } ?>
                                                    </div>
                                                    </form>
                                                    </div>
                                                    </div>
                                                    <!-- /form validation -->

                                                    <!-- Footer -->
                                                    <div class="footer text-muted">
                                                        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
                                                    </div>
                                                    <!-- /footer -->