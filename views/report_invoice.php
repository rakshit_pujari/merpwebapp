

<!-- Page header -->
<div class="page-header">

    <div ng-controller="invoiceReportController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Report</span> - Customer Sales</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="report/invoice" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Download Entire Report</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-stack4 position-left"></i>Report</li>
            <li class="active"><i class="icon-person position-left"></i>Customer Sales</li>
        </ul>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Invoice archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Customer Sales</h6>
            <div class="heading-elements">
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <div class="col-lg-12">
                <div class="form-group col-lg-9">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Start Date</label>
                        <div class="col-lg-9">
                            <input class="form-control datePicker" id = "startDate">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">End Date</label>
                        <div class="col-lg-9">
                            <input class="form-control datePicker" id = "endDate">
                        </div>
                    </div>


                </div>
                <div class="form-group col-lg-3">
                    <div class="text-right">
                        <button onclick = 'downloadReport("invoice")' class="btn btn-primary">Download Report For Selected Period<i class="glyphicon glyphicon-download-alt position-right"></i></button>
                    </div>
                </div>
            </div>


            <table class="table noGroupDataTable">
                <thead>
                    <tr>
                        <th>Sr no.</th>
                        <th>Customer Name</th>
                        <th>No. Of Invoices</th>
                        <th>Invoice Amount</th>
                        <th>Credit Note Amount</th>
                        <th>Debit Note Amount</th>
                        <th>Net Invoice Amount</th>
                        <th>COGS Amount</th>
                        <th>Gross Revenue</th>
                        <th>Gross Revenue</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <?
                        $row = 1;
                        foreach ($invoices as $invoice) {
                            ?>
                            <td><? echo ($row); ?></td>    
                            <td><? echo $invoice['invoice_linked_company_invoice_name']; ?></td>    
                            <td><? echo $invoice['number_of_invoices']; ?></td>    
                            <td>₹ <? echo number_format($invoice['total_invoice_amount'], 2); ?></td>    
                            <td>₹ <? echo number_format($invoice['total_cn_amount'], 2); ?></td>    
                            <td>₹ <? echo number_format($invoice['total_dn_amount'], 2); ?></td>    
                            <td>₹ <? echo number_format(($invoice['total_invoice_amount'] - $invoice['total_cn_amount'] + $invoice['total_dn_amount']), 2); ?></td>    
                            <td>₹ <? echo number_format(abs($invoice['cogs_amount']), 2); ?></td>    
                            <td>₹ <? echo number_format(($invoice['total_invoice_amount'] - $invoice['total_cn_amount'] + $invoice['total_dn_amount'] + $invoice['cogs_amount']), 2); ?></td>   
                            <td><? echo number_format(( 100 * ($invoice['total_invoice_amount'] - $invoice['total_cn_amount'] + $invoice['total_dn_amount'] + $invoice['cogs_amount']) / ($invoice['total_invoice_amount'] - $invoice['total_cn_amount'] + $invoice['total_dn_amount'])), 2);
                            ?> %</td>    

                        </tr>
                        <?
                        $row++;
                    }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Sr no.</th>
                        <th>Customer Name</th>
                        <th>No. Of Invoices</th>
                        <th>Invoice Amount</th>
                        <th>Credit Note Amount</th>
                        <th>Debit Note Amount</th>
                        <th>Net Invoice Amount</th>
                        <th>COGS Amount</th>
                        <th>Gross Revenue</th>
                        <th>Gross Revenue</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->