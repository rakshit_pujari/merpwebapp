

<!-- Page header -->
<div class="page-header">

    <div ng-controller="noGroupDataTableController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Accounting</span> - Cash Flow Statement</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!journal/all" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-calculator2 position-left"></i> Accounting</li>
            <li class="active"><i class="icon-cash4 position-left"></i>Cash Flow Statement</li>
        </ul>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Ledgers archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Cash Flow Statement</h6>
            <div class="heading-elements">
                </ul>
            </div>
        </div>

        <?
        $net_income = 0;
        foreach ($ledger_entries as $ledger_entry):
            if ($ledger_entry['at_name'] == 'Revenue' || $ledger_entry['at_name'] == 'Expense')
                $net_income += ($ledger_entry['credit_balance'] - $ledger_entry['debit_balance']);
        endforeach;
        ?>
        <table class="table">
            <thead>
                <tr>
                    <th></th>
                    <th>Account Name</th>
                    <th>Account Type</th>
                    <th>Amount</th>
                </tr>
            </thead>
            <tbody>
                <?
                $cash_flow_total = 0;
                $cash_opening_balance = 0;
                $cash_flow_from_current_assets = 0;
                $cash_flow_from_current_liabilities = 0;
                $cash_flow_from_investing = 0;
                $cash_flow_from_financing = 0;
                ?>
                
                <?php
                foreach ($ledger_entries as $ledger_entry):
                    if ($ledger_entry['at_name'] == 'Current Assets') {
                        if($ledger_entry['sj_account_coa_id'] == $cash_account_coa_id['debit_account_coa_id'] 
                                || $ledger_entry['coa_subaccount_of_id'] == $cash_account_coa_id['debit_account_coa_id'] ){
                            if($ledger_entry['coa_opening_balance_transaction_type'] == 'debit'){
                                $cash_opening_balance+= ($ledger_entry['coa_opening_balance']);
                            } else {
                                $cash_opening_balance-= ($ledger_entry['coa_opening_balance']);
                            }
                        }
                    }
                endforeach;
                ?>
                
                <tr>
                    <th>I. Opening Cash Balance</th>
                    <td></td>
                    <td></td>
                    <td><? 
                    if ($cash_opening_balance < 0)
                            echo '( ';
                    echo '₹ ' .number_format(abs($cash_opening_balance), 2); 
                    if ($cash_opening_balance < 0)
                            echo ' )';
                    ?></td>
                </tr>

                <tr>
                    <th>II. Cash Flow from Operating Activities</th>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                
                <tr>
                    <th></th> 
                    <td><a href = "#!SJ/pnl">Net Income</a></td> 
                    <td></td> 
                    <td><?
                        $cash_flow_total += $net_income;
                        if ($net_income < 0)
                            echo '( ';
                        echo '₹ ' . number_format(abs($net_income), 2);
                        if ($net_income < 0)
                            echo ' )';
                        ?>

                    </td> 
                </tr>
                
                <?php
                //Opening balance adjustment
                foreach ($ledger_entries as $ledger_entry):

                    if($ledger_entry['sj_account_coa_id'] == $coa_opening_balance_adjustment['debit_account_coa_id']
                            || $ledger_entry['sj_account_coa_id'] == $coa_opening_balance_adjustment['credit_account_coa_id']){
                        $amount = $ledger_entry['credit_balance'] - $ledger_entry['debit_balance'];
                        $cash_flow_total+= $amount;
                        ?>
                        <tr>
                            <td></td>
                            <td><span>
                                    <a href = "#!/COA/opening?highlight=adjustment"><?
                                    echo $ledger_entry['coa_account_name'];
                                    ?></a>
                                </span>
                            </td>
                            <td>
                                <?
                                //echo $ledger_entry['at_name'];
                                ?>

                            </td>
                            <td>
                                <?
                                if ($amount < 0)
                                    echo '( ';
                                echo '₹ ' . number_format(abs($amount), 2);
                                if ($amount < 0)
                                    echo ' )';
                                ?>
                            </td>
                        </tr> <?
                    }

                endforeach;
                ?>
                
                <?php
                foreach ($ledger_entries as $ledger_entry):
                    if ($ledger_entry['at_name'] == 'Current Assets') {
                        if($ledger_entry['sj_account_coa_id'] == $cash_account_coa_id['debit_account_coa_id'] 
                                || $ledger_entry['coa_subaccount_of_id'] == $cash_account_coa_id['debit_account_coa_id'] 
                                || $ledger_entry['sj_account_coa_id'] == $coa_opening_balance_adjustment['debit_account_coa_id']
                                || $ledger_entry['sj_account_coa_id'] == $coa_opening_balance_adjustment['credit_account_coa_id']){
                            continue;// do not consider cash & cash equivalent accounts & opening balance adjustment
                        }
                        
                        add_row($ledger_entry, 'credit', $cash_flow_from_current_assets, $cash_flow_total);
                    }
                endforeach;
                ?>

                <?php
                foreach ($ledger_entries as $ledger_entry):
                    if ($ledger_entry['at_name'] == 'Current Liabilities') {
                        if($ledger_entry['sj_account_coa_id'] == $coa_opening_balance_adjustment['debit_account_coa_id']
                                || $ledger_entry['sj_account_coa_id'] == $coa_opening_balance_adjustment['credit_account_coa_id']){
                            continue;// do not opening balance adjustment account
                        }
                        add_row($ledger_entry, 'credit', $cash_flow_from_current_liabilities, $cash_flow_total);
                    }
                endforeach;
                ?>
           
                <tr>
                    <th>III. Cash Flow from Investing</th>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <?php
                foreach ($ledger_entries as $ledger_entry):
                    if ($ledger_entry['at_name'] == 'Non Current Assets') {
                        add_row($ledger_entry, 'debit', $cash_flow_from_investing, $cash_flow_total);
                    }
                endforeach;
                ?>

                <tr>
                    <th>IV. Cash Flow from Financing</th>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                     
                <?php
                foreach ($ledger_entries as $ledger_entry):
                    if ($ledger_entry['at_name'] == 'Equity') {
                        add_row($ledger_entry, 'credit', $cash_flow_from_financing, $cash_flow_total);
                    }
                endforeach;
                ?>

                <?php
                foreach ($ledger_entries as $ledger_entry):
                    if ($ledger_entry['at_name'] == 'Non Current Liabilities') {
                        add_row($ledger_entry, 'credit', $cash_flow_from_financing, $cash_flow_total);
                    }
                endforeach;
                ?>

            </tbody>
            <tfoot>
                <tr>
                    <th width="15%">V. Net Cash Flow ( II + III + IV )</th>
                    <th></th>
                    <th></th>
                    <th><span style="color:<?
                        if ($cash_flow_total > 0)
                            echo '#00AA00';
                        else
                            echo '#FF0000';
                        ?>">

                            <?
                            if ($cash_flow_total < 0)
                                echo '( ';
                            echo '₹ ' . number_format(abs($cash_flow_total), 2);
                            if ($cash_flow_total < 0)
                                echo ' )';
                            ?>

                        </span></th>
                </tr>
                
                <tr>
                    <th width="15%">VI. Ending Cash Balance ( I + V  )</th>
                    <th></th>
                    <th></th>
                    <th><span style="color:<?
                        $ending_cash_balance = $cash_flow_total + $cash_opening_balance;
                        if ($ending_cash_balance > 0)
                            echo '#00AA00';
                        else
                            echo '#FF0000';
                        ?>">

                            <?
                            if ($ending_cash_balance < 0)
                                echo '( ';
                            echo '₹ ' . number_format(abs($ending_cash_balance), 2);
                            if ($ending_cash_balance < 0)
                                echo ' )';
                            ?>

                        </span></th>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->

    <?

    function add_row($ledger_entry, $account_type, &$section_total, &$total) {
        $opening_balance = $ledger_entry['coa_opening_balance'];
        if($ledger_entry['coa_opening_balance_transaction_type'] == 'debit'){
            $opening_balance = (-1) * $opening_balance;
        }
        $amount = $ledger_entry['credit_balance'] - $ledger_entry['debit_balance'] - $opening_balance;
        $total += $amount;
        $section_total = $section_total + $amount;
        ?>
        <tr>
            <td></td>
            <td>
                <a href="#!SJ/ledger/<?php echo $ledger_entry['sj_account_coa_id']; ?>"><span>
                        <?
                        echo $ledger_entry['coa_account_name'];
                        ?>
                    </span></a>

            </td>
            <td>
                <?
                echo $ledger_entry['at_name'];
                ?>

            </td>
            <td>
                <?
                if ($amount < 0)
                    echo '( ';
                echo '₹ ' . number_format(abs($amount), 2);
                if ($amount < 0)
                    echo ' )';
                ?>
            </td>
        </tr>
        <?
    }
    ?>