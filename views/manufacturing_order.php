
<div ng-controller="manufacturingOrderController" ng-init="load(<? if(isset($linked_recipe)) echo $linked_recipe['recipe_id']; ?>)"></div>
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/ManufacturingOrder/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Manufacturing</span> - Manufacturing Order</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!/ManufacturingOrder/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!"><i class="fa fa-gavel position-left"></i>Manufacturing</a></li>
            <li><a href="#!/ManufacturingOrder/all"><i class="fa fa-gears  position-left"></i>Manufacturing Order</a></li>
            <li class="active"><?php
                if (isset($manufacturing_order)) {
                    if ($manufacturing_order['manufacturing_order_status'] == 'confirm') {
                        echo 'MFG' . str_pad($manufacturing_order['manufacturing_order_id'], 5, "0", STR_PAD_LEFT);
                    } else {
                        echo "DRAFT";
                    }
                } else {
                    echo "New";
                }
                ?></li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Form validation -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Manufacturing Order</h5>
        </div>


        <div class="panel-body">
            <?php
            $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id' => 'merpDocForm');
            if (isset($manufacturing_order))
                echo form_open_multipart('web/ManufacturingOrder/save/' . $manufacturing_order['manufacturing_order_id'], $attributes);
            else
                echo form_open_multipart('web/ManufacturingOrder/save', $attributes);
            ?>
            <input name="manufacturing_order_status" type="text" readonly style = "display:none"
                   value = "<? if (isset($manufacturing_order))
                echo $manufacturing_order['manufacturing_order_status'];
            else
                echo "new";
            ?>">
            <p class="content-group-lg">A Manufacturing Order is a confirmation document required to manufacture products. It is a good practice to preview the Order before confirming it.</p>

            <fieldset class="content-group">
                <legend class="text-bold">Details</legend>	
                <div class="form-group col-lg-12">	
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Product <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="form-control select2" name="manufacturing_order_linked_product_id" id = "manufacturing_order_linked_product_id" tabindex = "1" 
                                    onchange="loadRecipeForManufacturing(this)" required>
                                <option value = "">Choose a recipe </option>
                                <?
                                foreach ($recipes as $recipe) {
                                    ?>
                                    <option 
                                    <?
                                    if (isset($manufacturing_order)) {
                                        if ($manufacturing_order['manufacturing_order_linked_product_id'] == $recipe['product_id'])
                                            echo "selected";
                                    }
                                    ?> value="<? echo $recipe['recipe_linked_product_id']; ?>" uqc-text ="<? echo $recipe['uqc_text']; ?>" recipe-id = "<? echo $recipe['recipe_id']; ?>"><? echo $recipe['product_name']. ' ( '. $recipe['uqc_text']. ' )' ?>
                                    </option>
                                    <?
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Order Date</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name = "manufacturing_order_date" type="text" id = "manufacturing_order_date_picker"
                                   value = "<?php if (isset($manufacturing_order)) echo $manufacturing_order['manufacturing_order_date']; ?>">

                        </div>
                    </div>
                </div>
                
                <div class="form-group col-lg-12">	                    
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Estimated Quantity (in lots)</label>
                        <div class="col-lg-9">
                            <input class="form-control" id ="max_quantity_estimate"
                                   readonly>

                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Order Quantity <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input min = 0.01 step = 0.01 class="form-control" tabindex="2" name = "manufacturing_order_product_quantity" type="number" min = "0" onchange="refreshForXUnits();calculateManufacturingNumbers();"
                                   value = "<?php if (isset($manufacturing_order)) echo $manufacturing_order['manufacturing_order_product_quantity']; else echo 0; ?>" required>

                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Batch Number</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name = "manufacturing_order_batch_number" type="text"
                                   value = "<?php if (isset($manufacturing_order)) echo $manufacturing_order['manufacturing_order_batch_number']; ?>">

                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Notes</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name = "manufacturing_order_notes" type="text"
                                   value = "<?php if (isset($manufacturing_order)) echo $manufacturing_order['manufacturing_order_notes']; ?>">

                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-12">
                    <div class="form-group col-lg-6 has-feedback has-feedback-left">
                        <label class="control-label col-lg-3">Estimated Cost </label>
                        <div class="col-lg-9">
                            <input class="form-control" type="text" id ="estimated_cost" readonly>
                            <div class="form-control-feedback">
                                <span style = "font-size:16px;">₹</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-lg-6 has-feedback has-feedback-left">
                        <label class="control-label col-lg-3">Estimated Cost Per Unit</label>
                        <div class="col-lg-9">
                            <input class="form-control" id ="estimated_cost_per_unit" type="text" readonly>
                            <div class="form-control-feedback">
                                <span style = "font-size:16px;">₹</span>
                            </div>
                        </div>
                    </div>
                </div>
                    
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Product Details</legend>
                <table class="table manufacturingOrderDatatable">
                    <thead>
                        <tr>
                            <th>Sr No</th>
                            <th>Product</th>
                            <th>Quantity</th>
                            <th>Available Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 0;
                        if (isset($manufacturing_order_entries))
                            foreach ($manufacturing_order_entries as $manufacturing_order_entry) {
                                $i++;
                                ?> 
                                <tr>
                                    <td><? echo $i; ?>
                                    <input value = '<? echo $manufacturing_order_entry['rate_per_unit']; ?>' readonly style = "display:none"></input>
                                    </td>

                                    <td>
                                        <input class="form-control" value = '<? echo $manufacturing_order_entry['product_name']; ?>' readonly></input>
                                    </td>		
                                    <td>
                                        <input name = "manufacturing_order_entries[<? echo $i; ?>][<? echo $manufacturing_order_entry['moe_linked_product_id']; ?>]" 
                                               min = "0" class = "form-control" type = "number" step = 0.01 value = "<? echo $manufacturing_order_entry['moe_linked_product_quantity']; ?>"
                                               onchange = "calculateManufacturingNumbers()"></input>
                                    </td>

                                    <td>
                                        <?
                                        echo $manufacturing_order_entry['inventory_quantity'] . ' ' . $manufacturing_order_entry['uqc_text'] . 
                                                '<input value = ' . $manufacturing_order_entry['inventory_quantity'].' readonly style = "display:none"></input>';
                                        ?>
                                    </td>
                                </tr>

                                    <?
                                }
                            ?>

                    </tbody>
                    <tfoot>
                        <tr>
                            <td>Sr No</td>
                            <td>Product</td>
                            <td>Quantity</td>
                            <td>Available Quantity</td>
                        </tr>
                    </tfoot>
                </table>	
            </fieldset>

            <div class="text-right">
                <a href="#!/ManufacturingOrder/all"><button class="btn btn-default">Cancel<i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                <? if ($this->session->userdata('access_controller')->is_access_granted('manufacturing_order', 'save')) { ?>
                    <? if (!isset($manufacturing_order) || $manufacturing_order['manufacturing_order_status'] == 'draft') { ?>
                        <button onclick="submitDocForm('manufacturing_order', 'draft')" id = "buttonSaveDraftQuotation" name="transaction" value="draft" class="btn btn-default">Save as draft <i class="icon-reload-alt position-right"></i></button>
                        <button onclick="submitDocForm('manufacturing_order', 'previewDraft')" name="transaction" value="previewDraft" class="btn btn-default">Preview<i class="icon-copy position-right"></i></button>
                    <? } ?>
                    <button onclick="confirmAndContinueForDocForm('manufacturing_order', 'confirm')" id = "buttonSaveConfirmQuotation" name="transaction" value="confirm" class="btn btn-primary">Confirm <i class="icon-arrow-right14 position-right"></i></button>
                <? } ?>
            </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->