<div style = "padding-top:20px;padding-left:20px;padding-right:20px;">
    <?php echo validation_errors('  <div style = "border-color: #ebccd1;background-color:#f2dede; color:#a94442" class="alert alert-danger alert-dismissable"><a style = "padding-bottom:10px" href="#" class="close" data-dismiss="alert" aria-label="close">×</a>', '</div>'); ?>
</div>
<div ng-controller="formController" ng-init="load('4')"></div>
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/location/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Master</span> - Location</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!/location/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!"><i class="icon-home2 position-left"></i> Master</a></li>
            <li><a href="#!/location/all"><i class="icon-city position-left"></i> Location</a></li>
            <li class="active"><?php
                if (isset($location))
                    echo 'LOC' . str_pad($location['location_id'], 5, "0", STR_PAD_LEFT);
                else
                    echo "New";
                ?></li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Form validation -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Location</h5>

        </div>


        <div class="panel-body">
            <p class="content-group-lg">A Location is the place of business operations. Location details entered here will be linked with Other Masters, Sales, Purchase, Accounts and Reports. </p>
            <?php echo validation_errors(); ?>
            <?php
            $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id'=>'merpForm');
            if (isset($location))
                echo form_open('web/location/save/' . $location['location_id'], $attributes);
            else
                echo form_open('web/location/save', $attributes);
            ?>
            <fieldset class="content-group">
                <legend class="text-bold">Basic inputs</legend>

                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-3">City Name <span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input class="form-control" tabindex="1" 
                               onblur ="checkIfUnique(this, 'location', 'city_name', '<? if(isset($location)) echo $location['location_id']; ?>', 'location_id')"
                               name="city_name" style="text-transform: capitalize;" type="text" required="required" value = "<?php if (isset($location)) echo $location['city_name']; ?>" placeholder="Enter City Name">
                    </div>
                </div>							
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-3">District</label>
                    <div class="col-lg-9">
                        <input class="form-control" placeholder="Example - Raigad, Thane" tabindex="2" name="district" style="text-transform: capitalize;" type="text"  value = "<?php if (isset($location)) echo $location['district']; ?>">
                    </div>
                </div>
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Additional Data</legend>
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-3">State<span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <select required class="select2 form-control" name="state_id" tabindex="3">
                            <optgroup label="States">
                                <?php
                                foreach ($states as $state) {
                                    ?>
                                    <option 
                                    <?php
                                    if (set_value('state_id') == $state ['state_tin_number'])
                                        echo
                                        "selected";
                                    else if (isset($location)) {
                                        if ($location['state_id'] == $state ['state_tin_number'])
                                            echo
                                            "selected";
                                    }
                                    ?> value="<? echo $state['state_tin_number']; ?>"><? echo $state['state_name'] ?>
                                    </option>
                                    <?php
                                }
                                ?>
                            </optgroup>
                            <optgroup label = "Union Territories">
                                <?php
                                foreach ($union_territories as $union_territory) {
                                    ?>
                                    <option 
                                    <?php
                                    if (set_value('state_id') == $union_territory ['state_tin_number'])
                                        echo
                                        "selected";
                                    else if (isset($location)) {
                                        if ($location['state_id'] == $union_territory ['state_tin_number'])
                                            echo
                                            "selected";
                                    }
                                    ?> value="<? echo $union_territory['state_tin_number']; ?>"><? echo $union_territory['state_name'] ?>
                                    </option>
                                    <?php
                                }
                                ?>

                            </optgroup>
                        </select>
                    </div>
                </div>

            </fieldset>

            <div class="text-right">
                <a href="#!/location/all"><button class="btn btn-default">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                <? 
                if($this->session->userdata('access_controller')->is_access_granted('location', 'save')) { ?>
                <button onclick="submitForm('location')" id = "buttonSubmit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                <? } ?>
            </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->