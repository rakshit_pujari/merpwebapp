
<div ng-controller="formController" ng-init="load()"></div>
<!-- Page header -->
<div class="page-header">

    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/bank/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Master</span> - Bank</h4>
        </div>
        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!/bank/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!"><i class="icon-home2 position-left"></i> Master</a></li>
            <li><a href="#!/bank/all"><i class="fa fa-bank position-left"></i> Bank</a></li>
            <li class="active"><?php
                if (isset($bank))
                    echo 'BNK' . str_pad($bank['bank_id'], 5, "0", STR_PAD_LEFT);
                else
                    echo "New";
                ?></li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Form validation -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Bank</h5>
        </div>


        <div class="panel-body">
            <p class="content-group-lg">Create Bank. Bank created here will be used while generating invoices etc</p>
            <?php echo validation_errors(); ?>
            <?php
            $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id'=>'merpForm');
            if (isset($bank))
                echo form_open('web/bank/save/' . $bank['bank_id'], $attributes);
            else
                echo form_open('web/bank/save', $attributes);
            ?>
            <fieldset class="content-group">
                <legend class="text-bold">Basic details</legend>					
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="bank_name">Bank Name <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input onblur ="checkIfUnique(this, 'bank', 'bank_name', '<? if(isset($bank)) echo $bank['bank_id']; ?>', 'bank_id')" class="form-control" name="bank_name" style="text-transform: capitalize;" type="text" value = "<?php if (isset($bank)) echo $bank['bank_name']; ?>" tabindex = "1" placeholder="Enter Bank Name" required>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="ifsc_code">IFSC Code</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="ifsc_code" type="text" value = "<?php if (isset($bank)) echo $bank['ifsc_code']; ?>" tabindex = "2" placeholder="Unique IFSC Code for Bank Branch">
                        </div>
                    </div>							
                </div>
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Account details</legend>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="bank_account_name">Account Name <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" onblur ="checkIfUnique(this, 'bank', 'bank_account_name', '<? if(isset($bank)) echo $bank['bank_id']; ?>', 'bank_id')" required name="bank_account_name" type="text" value = "<?php if (isset($bank)) echo $bank['bank_account_name']; ?>" tabindex = "3" placeholder = "" required>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="bank_account_number">Account Number <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" required onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="bank_account_number" type="text" value = "<?php if (isset($bank)) echo $bank['bank_account_number']; ?>" tabindex = "3" placeholder = "" required>
                        </div>
                    </div>				
                </div>
                <!-- refer bitbucket issue #140
                <div class="col-lg-12">
                    <div class="form-group col-lg-6 has-feedback has-feedback-left">
                        <label class="control-label col-lg-3">Opening Balance <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" required type="number" name="bank_opening_balance" value="<?php if (isset($bank)) echo $bank['bank_opening_balance']; ?>" required="required" tabindex = "4" placeholder="Opening Balance In Bank Account">
                            <div class="form-control-feedback">
                                <span style = "font-size:16px;">₹</span>
                            </div>
                        </div>
                    </div>
                </div>-->
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Location details</legend>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="bank_address">Address </label>
                        <div class="col-lg-9">
                            <input class="form-control" name="bank_address" type="text" value = "<?php if (isset($bank)) echo $bank['bank_address']; ?>" tabindex = "5" placeholder="Enter address">
                        </div>
                    </div>


                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="bank_area">Area</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="bank_area" type="text" value = "<?php if (isset($bank)) echo $bank['bank_area']; ?>" tabindex = "6" placeholder = "Enter area">
                        </div>
                    </div>						
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="bank_location_id">City <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select required class="form-control select2" name="bank_location_id" id="city" onChange = "refreshLocationDistrictAndState()" tabindex = "7">
                                <option>Choose a location</option>
                                <?php
                                foreach ($locations as $location) {
                                    ?>
                                    <option district = "<?php echo $location['district'] ?>" state = "<?php echo $location['state_name'] ?>" 
                                    <?php
                                    if (set_value('bank_location_id') == $location['location_id'])
                                        echo "selected";
                                    else if (isset($bank)) {
                                        if ($bank['bank_location_id'] == $location['location_id'])
                                            echo "selected";
                                    }
                                    ?> value="<? echo $location['location_id']; ?>"><? echo $location['city_name'] ?>
                                    </option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>	
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="district">District</label>
                        <div class="col-lg-9">
                            <input class="form-control" id="district" type="text" readonly>
                        </div>
                    </div>							
                </div>
                <div class="col-lg-12">							
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="state">State </label>
                        <div class="col-lg-9">
                            <input class="form-control" id="state" type="text" readonly>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="bank_pincode">Pincode </label>
                        <div class="col-lg-9">
                            <input class="form-control" name="bank_pincode" type="text" value = "<?php if (isset($bank)) echo $bank['bank_pincode']; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' tabindex = "7">
                        </div>
                    </div>	
                </div>
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Contact details</legend>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="bank_relationship_manager_name">Relationship Manager Name </label>
                        <div class="col-lg-9">
                            <input class="form-control" name="bank_relationship_manager_name" type="text" style="text-transform: capitalize;" value = "<?php if (isset($bank)) echo $bank['bank_relationship_manager_name']; ?>"  tabindex = "8">
                        </div>
                    </div>


                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="bank_email_address">Email Address</label><span id = "emailAddressError" style = "color:red; display:none;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Please check email address format !</span>
                        <div class="col-lg-9">
                            <input class="form-control" name="bank_email_address" type="text" value = "<?php if (isset($bank)) echo $bank['bank_email_address']; ?>" tabindex = "9">
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="bank_contact_number">Contact Number </label>
                        <div class="col-lg-9">
                            <input class="form-control format-phone-number" name="bank_contact_number" type="text" value = "<?php if (isset($bank)) echo $bank['bank_contact_number']; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' tabindex = "10">
                        </div>
                    </div>
                </div>
            </fieldset>

            <div class="text-right">
                <a href="#!/bank/all"><button class="btn btn-default">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                <? if($this->session->userdata('access_controller')->is_access_granted('bank', 'save')) { ?>
                <button onclick="submitForm('bank');" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                <? } ?>
            </div>
            </form>

        </div>

        </section>
    </div>	
    <!-- /form validation -->
    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->
