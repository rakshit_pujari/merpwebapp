

<!-- Page header -->
<div class="page-header">

    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Master</span> - Transporter</h4>
        </div>
        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!/transporter/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!"><i class="icon-home2 position-left"></i> Master</a></li>
            <li><a href="#!/transporter/all"><i class = "icon-truck position-left"></i> Transporter</a></li>
            <li class="active"><?php
                if (isset($transporter))
                    echo 'TRN' . str_pad($transporter['transporter_id'], 5, "0", STR_PAD_LEFT);
                else
                    echo "New";
                ?></li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Form validation -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Transporter</h5>

        </div>


        <div class="panel-body">
            <p class="content-group-lg">A Transporter is the agency associated with the company for transport of goods. Transporter details entered here will be linked with Sales, Purchase, and Accounts. </p>
            <?php echo validation_errors(); ?>
            <?php
            $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id'=>'merpForm');
            if (isset($transporter))
                echo form_open('web/transporter/save/' . $transporter['transporter_id'], $attributes);
            else
                echo form_open('web/transporter/save', $attributes);
            ?>
            <fieldset class="content-group">
                <legend class="text-bold">Basic Details</legend>					
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="transporter_name">Transporter Name <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" 
                                   onblur ="checkIfUnique(this, 'transporter', 'transporter_name', '<? if(isset($transporter)) echo $transporter['transporter_id']; ?>', 'transporter_id')"
                                   required name="transporter_name" type="text" style="text-transform: capitalize;" value = "<?php if (isset($transporter)) echo $transporter['transporter_name']; ?>" tabindex = "1" placeholder="Enter Transporter Name">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="transporter_invoice_name">Invoice Name <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" required name="transporter_invoice_name" type="text" style="text-transform: capitalize;" value = "<?php if (isset($transporter)) echo $transporter['transporter_invoice_name']; ?>" tabindex = "1" placeholder="Transporter Name to be displayed on Invoice">
                        </div>
                    </div>
                </div>
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Location Details</legend>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="transporter_billing_address">Billing Address </label>
                        <div class="col-lg-9">
                            <input class="form-control" name="transporter_billing_address" type="text" value = "<?php if (isset($transporter)) echo $transporter['transporter_billing_address']; ?>" tabindex = "2" placeholder="Enter Billing Address">
                        </div>
                    </div>

                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="transporter_correspondence_address">Correspondence Address </label>
                        <div class="col-lg-9">
                            <input class="form-control" name="transporter_correspondence_address" type="text" value = "<?php if (isset($transporter)) echo $transporter['transporter_correspondence_address']; ?>" tabindex = "2" placeholder="Enter Correspondence Address">
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="transporter_area">Area</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="transporter_area" type="text" value = "<?php if (isset($transporter)) echo $transporter['transporter_area']; ?>" tabindex = "3" placeholder = "Enter area">
                        </div>
                    </div>						

                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="transporter_location_id">City <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                        <select required class="form-control select2" name="transporter_location_id" id="city" onChange = "refreshLocationDistrictAndState()" tabindex = "4">
                                        <option>Choose a location</option>
                                        <?php
                                        foreach ($locations as $location) {
                                            ?>
                                            <option district = "<?php echo $location['district'] ?>" state = "<?php echo $location['state_name'] ?>" 
                                            <?php
                                            if (set_value('transporter_location_id') == $location['location_id'])
                                                echo "selected";
                                            else if (isset($transporter)) {
                                                if ($transporter['transporter_location_id'] == $location['location_id'])
                                                    echo "selected";
                                            }
                                            ?> value="<? echo $location['location_id']; ?>"><? echo $location['city_name'] ?>
                                            </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                        </div>
                    </div>	
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="district">District</label>
                        <div class="col-lg-9">
                            <input class="form-control" id="district" type="text" readonly>
                        </div>
                    </div>							

                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="state">State </label>
                        <div class="col-lg-9">
                            <input class="form-control" id="state" type="text" readonly>
                        </div>
                    </div>
                    
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="transporter_pincode">Pin Code </label>
                        <div class="col-lg-9">
                            <input class="form-control" name ="transporter_pincode" type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' tabindex = "5"
                                   value = "<?php if (isset($transporter)) echo $transporter['transporter_pincode'];?>">
                        </div>
                    </div>
                </div>
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Contact Details</legend>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="transporter_contact_person_name">Contact Person</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="transporter_contact_person_name" type="text" value = "<?php if (isset($transporter)) echo $transporter['transporter_contact_person_name']; ?>"  tabindex = "6">
                        </div>
                    </div>


                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="transporter_email_address">Email Address</label><span id = "emailAddressError" style = "color:red; display:none;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Please check email address format !</span>
                        <div class="col-lg-9">
                            <input class="form-control" name="transporter_email_address" type="text" value = "<?php if (isset($transporter)) echo $transporter['transporter_email_address']; ?>" tabindex = "7">
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="transporter_contact_number">Contact Number </label>
                        <div class="col-lg-9">
                            <input class="form-control format-phone-number" name="transporter_contact_number" type="text" value = "<?php if (isset($transporter)) echo $transporter['transporter_contact_number']; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' tabindex = "8">
                        </div>
                    </div>	
                </div>
            </fieldset>

            <fieldset class="content-group">
                <legend class="text-bold">Terms & Taxation Details</legend>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">GST Supply State<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select required class="select2 form-control" name="transporter_gst_supply_state_id" tabindex="9">
                                <option value = "">Choose a state</option>
                                <optgroup label="States">
                                    <?php
                                    foreach ($states as $state) {
                                        ?>
                                        <option 
                                        <?php
                                        if (set_value('transporter_gst_supply_state_id') == $state['state_tin_number'])
                                            echo "selected";
                                        else if (isset($transporter)) {
                                            if ($transporter['transporter_gst_supply_state_id'] == $state['state_tin_number'])
                                                echo "selected";
                                        }
                                        ?> value="<? echo $state['state_tin_number']; ?>"><? echo $state['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </optgroup>
                                <optgroup label = "Union Territories">
                                    <?php
                                    foreach ($union_territories as $union_territory) {
                                        ?>
                                        <option 
                                        <?php
                                        if (set_value('transporter_gst_supply_state_id') == $union_territory['state_tin_number'])
                                            echo "selected";
                                        else if (isset($transporter)) {
                                            if ($transporter['transporter_gst_supply_state_id'] == $union_territory['state_tin_number'])
                                                echo "selected";
                                        }
                                        ?> value="<? echo $union_territory['state_tin_number']; ?>"><? echo $union_territory['state_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>

                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="transporter_pan_number">PAN Number</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="transporter_pan_number" type="text" value = "<?php if (isset($transporter)) echo $transporter['transporter_pan_number']; ?>" tabindex = "10" placeholder = "Example - ABCC8787C">
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="transporter_gst_number">GST Number</label>
                        <div class="col-lg-9">
                            <input class="form-control" onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9@]+/, '')" name="transporter_gst_number" type="text" value = "<?php if (isset($transporter)) echo $transporter['transporter_gst_number']; ?>" tabindex = "11" placeholder = "Should be 15 characters">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Payment Terms </label>
                        <div class="col-lg-9">
                            <select class="form-control select" name="transporter_payment_term_id" tabindex = "12">
                                <?
                                foreach ($payment_terms as $payment_term) {
                                    ?>
                                    <option 
                                    <?php
                                    if (set_value('transporter_payment_term_id') == $payment_term['payment_term_id'])
                                        echo "selected";
                                    else if (isset($transporter)) {
                                        if ($transporter['transporter_payment_term_id'] == $payment_term['payment_term_id'])
                                            echo "selected";
                                    }
                                    ?> value="<? echo $payment_term['payment_term_id']; ?>"><? echo $payment_term['payment_term_display_text'] ?>
                                    </option>
                                    <?
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Reverse Charge </label>
                        <div class="col-lg-9">
                            <div class="checkbox checkbox-switch">
                                <label>
                                    <input type="checkbox" name="is_reverse_charge_applicable_for_transporter" data-on-text="Yes" data-off-text="No" class="switch" <?php if (isset($transporter)) if ($transporter['is_reverse_charge_applicable_for_transporter'] == 1) echo "checked"; ?>>
                                    Applicable
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>

            <div class="text-right">
                <a href="#!/transporter/all"><button class="btn btn-default">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                <? 
                if($this->session->userdata('access_controller')->is_access_granted('transporter', 'save')) { ?>
                <button onclick="submitForm('transporter')" id = "buttonSubmit" class="btn btn-primary">Save<i class="icon-arrow-right14 position-right"></i></button>
                <? } ?>
            </div>
            </form>

        </div>
        <div ng-controller="formController" ng-init="load()"></div>
        </section>
    </div>	
    <!-- /form validation -->
    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->
