

<!-- Page header -->
<div class="page-header">
    <div ng-controller="formController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/employee/rights/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Settings</span> - Employee Rights Allocation</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!"><i class="icon-home2 position-left"></i>Settings</a></li>
            <li><a href="#!/employee/rights/all"><i class="icon-user-tie position-left"></i> Employee Rights Allocation</a></li>
            <li class="active"><?php
                if (isset($employee))
                    echo 'EMP' . str_pad($employee['employee_id'], 5, "0", STR_PAD_LEFT);
                else
                    echo "New";
                ?></li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Form validation -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Employee Rights</h5>
            <div class="heading-elements">
                
            </div>
        </div>


        <div class="panel-body">
            <p> Name        : <? echo $employee['employee_name']; ?></p>
            <p> Department  : <? echo $employee['employee_department']; ?></p>
            <p> Designation : <? echo $employee['employee_designation']; ?></p><br/>
            <?php echo validation_errors(); ?>
            <?php
                $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id'=>'merpForm');
                echo form_open('web/employee/rights/save/' . $employee['employee_id'], $attributes);
            ?>

            <fieldset class="content-group">
                <legend class="text-bold">Employee Rights Allocation</legend>
                <div class="col-lg-12">
                    <table class="table">
                        <tr>
                            <th>Sr No</th>
                            <th>Particulars</th> 
                            <th class = "text-center">Create & Edit<br/><br/><input type = "checkbox" class = "control-success" onclick="toggleCheckboxSelection(this, 'save')"><br/></th>
                            <th class = "text-center">View<br/><br/><input type = "checkbox" class = "control-primary" onclick="toggleCheckboxSelection(this, 'view')"><br/></th>
                            <th class = "text-center">Delete<br/><br/><input type = "checkbox" class = "control-danger" onclick="toggleCheckboxSelection(this, 'delete')"><br/></th>
                        </tr>
                        <?
                        $row = 1;
                        foreach ($employee_rights as $key => $value) {
                            ?>
                            <tr>
                                <td><? echo $row++; ?></td>
                                <td><? echo ucwords(str_replace('_', ' ', $key)); ?></td>
                                <td class = "text-center"><input type = "checkbox" 
                                    <? if (isset($value['save'])) { ?>
                                                                     name = "employee_rights[<? echo $value['save']; ?>]" 
                                                                 <? } ?> 
                                                                 class = "<?
                                                                 if (isset($value['save']))
                                                                     echo "save control-success";
                                                                 else
                                                                     echo "styled";
                                                                 ?>" 
                                                                 <?
                                                                 if (isset($value['save'])) {
                                                                     if (in_array($value['save'], $employee_rights_assigned))
                                                                         echo "checked";
                                                                 } else
                                                                     echo "disabled";
                                                                 ?>></input></td>
                                <td class = "text-center"><input type = "checkbox" 
                                    <? if (isset($value['view'])) { ?>
                                                                     name = "employee_rights[<? echo $value['view']; ?>]" 
                                                                 <? } ?> 
                                                                 class = "<?
                                                                 if (isset($value['view']))
                                                                     echo "view control-primary";
                                                                 else
                                                                     echo "styled";
                                                                 ?>" 
                                                                 <?
                                                                 if (isset($value['view'])) {
                                                                     if (in_array($value['view'], $employee_rights_assigned))
                                                                         echo "checked";
                                                                 } else
                                                                     echo "disabled";
                                                                 ?>></input></td>
                                <td class = "text-center"><input type = "checkbox" 
                                    <? if (isset($value['delete'])) { ?>
                                                                     name = "employee_rights[<? echo $value['delete']; ?>]" 
                                                                 <? } ?> 
                                                                 class = "<?
                                                                 if (isset($value['delete']))
                                                                     echo "delete control-danger";
                                                                 else
                                                                     echo "styled";
                                                                 ?>" 
                                                                 <?
                                                                 if (isset($value['delete'])) {
                                                                     if (in_array($value['delete'], $employee_rights_assigned))
                                                                         echo "checked";
                                                                 } else
                                                                     echo "disabled";
                                                                 ?>></input></td>
                            </tr>
                        <? } ?>
                    </table>
                </div>
            </fieldset>
            <div class="text-right">
                <a href="#!/employee/rights/all"><button class="btn btn-default">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                <? if ($this->session->userdata('access_controller')->is_access_granted('employee_rights', 'save')) { ?>
                    <button onclick="submitForm('Employee Rights', 'employee/rights/all');" class="btn btn-primary">Save<i class="icon-arrow-right14 position-right"></i></button>
                <? } ?>
            </div>
            </form>

        </div>

        </section>
    </div>	
    <!-- /form validation -->
    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->
