<div class="content">

					<!-- Detached content -->
					<div class="container-detached">
						<div class="content-detached">

                                                    
                                                    
                                                    <div class="panel panel-flat" id="v_1_6">
								<div class="panel-heading">
									<h5 class="panel-title">Version 1.3</h5>
									<div class="heading-elements">
										<span class="text-muted heading-text">April 22, 2018</span>
										<span class="label bg-blue heading-text">v. 1.3</span>
					            	</div>
								</div>

								<div class="panel-body">
									<p class="content-group">Version 1.3 addresses commonly faced-issues pertaining to earlier builds. It involves new additions, improvements, enhancement and fixes.
</p>

<pre class="language-javascript"><code>
# New

[new] Charges Master added

[new] Sales & Purchase Charges can now be added separately while creating Proforma, Invoices, Credit Notes and Debit Notes

# Improvements

[improvement] Check sum digit validation added in Company Master for Company GSTIN

[improvement] New fields (GSTIN, PAN, Contact person and Number) added in Sales Invoice & Purchase Invoice creation

[improvement] Negative adjustment value now accepted in Purchase Invoice Creation

# Enhancements

[enhancement] GSTIN display in CAPS by default

[enhancement] Column width for Item description in Proforma increased

# Fixes

[fixes] Inventory valuation computation with new predated Manufacturing Order fixed

[fixes] GSTIN now mandatory for booking Purchase Invoices

[fixes] Minor aesthetic-related issues in Purchase

</code></pre>
								</div>
							</div>
                                                    
                                                    
                                                    <div class="panel panel-flat" id="v_1_6">
								<div class="panel-heading">
									<h5 class="panel-title">Version 1.2.2</h5>
									<div class="heading-elements">
										<span class="text-muted heading-text">April 08, 2018</span>
										<span class="label bg-blue heading-text">v. 1.2.2</span>
					            	</div>
								</div>

								<div class="panel-body">
									<p class="content-group">Version 1.2.2 involves improvements and enhancements.
</p>

<pre class="language-javascript"><code>
# Improvements

[improvement] System now allows inventory in decimals. Stock can now be purchased and Sold in part quantity. Manufacturing users can now process part quantity.

[improvement] UQC for products added in BOM (Recipe), Manufacturing Order and Opening Stock pages


# Enhancements

[enhancement] Document formatting enhanced to accommodate decimals

[enhancement] Rupee symbol added in Purchase Order and Quotation documents

</code></pre>
								</div>
							</div>
                                                    
                                                    
							<!-- Version 1.6 -->
							<div class="panel panel-flat" id="v_1_6">
								<div class="panel-heading">
									<h5 class="panel-title">Version 1.2.1</h5>
									<div class="heading-elements">
										<span class="text-muted heading-text">April 05, 2018</span>
										<span class="label bg-blue heading-text">v. 1.2.1</span>
					            	</div>
								</div>

								<div class="panel-body">
									<p class="content-group">Version 1.2.1 involves fixes and enhancements in Ver.1.2. The changes are mainly with respect to performance and aesthetic enhancements. 
</p>

<pre class="language-javascript"><code>
# Fixes

[fixes] Employee rights cancel button redirection issue resolved

[fixes] Minor fixes and enhancements in docs & reports


# Enhancements

[enhancement] Product UQC added in Product details table in Bill of Materials (Recipe) 

[enhancement] Customer & Vendor name in documents now displayed in bold 

[enhancement] Product description column width increased in documents 

[enhancement] Product UQC added in Product Master landing page 


</code></pre>
								</div>
							</div>
                                                        
                                                        
                                                    
                                                    <!-- Version 1.6 -->
							<div class="panel panel-flat" id="v_1_6">
								<div class="panel-heading">
									<h5 class="panel-title">Version 1.2</h5>
									<div class="heading-elements">
										<span class="text-muted heading-text">March 31, 2018</span>
										<span class="label bg-blue heading-text">v. 1.2</span>
					            	</div>
								</div>

								<div class="panel-body">
									<p class="content-group">Version 1.2 is the first major update with the addition of the manufacturing module, and GST-2 & 3B report creation. Quant Versions tracker is introduced to provide visibility to users about the changes in the system. Additionally, improvements to Version 1.1 have been added.
</p>

<pre class="language-javascript"><code># New

[new] Auto-GSTR-2 & 3B report generation now available 

[new] Manufacturing module with Bill of Materials (Recipe) Management added

[new] Quant Versions tracker added

# Improvements

[improvement] Purchase terms added in Purchase Invoice

[improvement] Proforma, Sales Invoice, Purchase Invoice terms can be edited from their respective creation pages

[improvement] Work-in-progress inventory display in Inventory Overview

[improvement] Default tax type (Inclusive / Exclusive) configuration from Company Settings

</code></pre>
								</div>
							</div>
							<!-- /version 1.6 -->


							<!-- Version 1.5 -->
							<div class="panel panel-flat" id="v_1_5">
								<div class="panel-heading">
									<h5 class="panel-title">Version 1.1</h5>
									<div class="heading-elements">
										<span class="text-muted heading-text">February 25, 2018</span>
										<span class="label bg-blue heading-text">v. 1.1</span>
					            	</div>
								</div>

								<div class="panel-body">
									<p class="content-group">Version 1.1 involves fixes and enhancements in the initial release. Most of the changes are with respect to aesthetic enhancements. 
</p>

<pre class="language-javascript"><code># Fixes

[fixes] Multi-line document terms display in PO and Quotations

[fixes] "Added to" account now displays the chosen account in Receipt Voucher

[fixes] Opening Receivables & Opening Payables cancel redirect reset

[fixes] Zero opening balance excluded from Transactions report 


# Enhancements

[enhancement] Line spacing improved in Payment, Receipt, Advance Payment & Advance Receipt amount

[enhancement] Fields renamed in Payment, Receipt, Advance Payment, Advance Receipt & Inventory Adjustment

[enhancement] Minor Dashboard enhancements

[enhancement] Improved GSTR-1 template format for download

[enhancement] Chart of Account hyperlink removed from Journal Landing

</code></pre>
								</div>
							</div>
							<!-- /version 1.5 -->


							<!-- Version 1.4 -->
							<div class="panel panel-flat" id="v_1_4">
								<div class="panel-heading">
									<h5 class="panel-title">Initial Release</h5>
									<div class="heading-elements">
										<span class="text-muted heading-text">February 01, 2018</span>
										<span class="label bg-blue heading-text">v. 1.0</span>
					            	</div>
								</div>

								<div class="panel-body">
									<p class="content-group">Quant is designed to provide a simple, intuitive experience to all the users. Based on real-time user feedbacks, we aim to constantly improve and optimise the product to offer a better, efficient system. Thus, Quant will be in active development with updates provided on a timely basis. Our updates will include: new additions, updates, improvements, enhancements, bug fixes, and code optimisations. Your feedback in improving the product is most welcome. 
</p>

<pre class="language-javascript"><code>The core application with the following modules has been deployed:
Masters
Purchase
Sales
Inventory
Accounts
Dashboard
Reports
Settings (Employee rights, Accounting Preferences & Company Preferences)

</code></pre>
								</div>
							</div>
							<!-- /version 1.4 -->
						</div>
					</div>
					<!-- /detached content -->

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->