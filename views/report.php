
<!-- Page header -->
<div class="page-header">

    <div ng-controller="reportController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Report</span> - <? if( isset($title)) echo $title; ?></h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <?if(isset($report_items[0])) { ?>
                <a href="report/download/<? if( isset($report_type)) echo $report_type; ?>" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Download Entire Report</span></a>
                <? } ?>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!/report/all"><i class="icon-stack4 position-left"></i> Report</a></li>
            <li class="active"><i class="<? if(isset($icon)) echo $icon; ?> position-left"></i><? if( isset($title)) echo $title; ?></li>
        </ul>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Invoice archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title"><? if(isset($title)) echo $title; ?></h6>
            <div class="heading-elements">
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <?if(isset($report_items[0])) { ?>
            <div class="col-lg-12">
                <div class="form-group col-lg-9">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Start Date</label>
                        <div class="col-lg-9">
                            <input class="form-control datePicker" id = "startDate">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">End Date</label>
                        <div class="col-lg-9">
                            <input class="form-control datePicker" id = "endDate">
                        </div>
                    </div>
                </div>
                <div class="form-group col-lg-3">
                    <div class="text-right">
                        <button onclick = 'downloadReport("<? if( isset($report_type)) echo $report_type; ?>")' class="btn btn-primary">Download Report For Selected Period<i class="glyphicon glyphicon-download-alt position-right"></i></button>
                    </div>
                </div>
            </div>

            <table class="table noGroupDataTable">
                <thead>
                    <tr>
                        <th class = "text-center">Sr no.</th>
                        <?
                        if(isset($report_items[0]))
                        foreach ($report_items[0] as $key => $value) {?>
                        <th class = "text-center"><? 
                        $key = str_replace('percent', ' %', $key);
                        $heading = ucwords(str_replace('_', ' ', $key));
                        echo $heading; ?></th>  
                        <?}
                        ?>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <?
                        $row = 1;
                        foreach ($report_items as $key => $report_item) {
                            ?>
                            <td class = "text-center"><? echo ($row); ?></td>    
                            <? foreach ($report_item as $report_item_key => $report_item_value) {
                            ?>
                            <td class = "text-center"><? echo $report_item_value; ?></td>    
                            <?
                            }
                            ?>

                        </tr>
                        <?
                        $row++;
                    }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th class = "text-center">Sr no.</th>
                        <?
                        if(isset($report_items[0]))
                        foreach ($report_items[0] as $key => $value) {?>
                        <th class = "text-center"><? 
                        $key = str_replace('percent', ' %', $key);
                        $heading = ucwords(str_replace('_', ' ', $key));
                        echo $heading; ?></th>  
                        <?}
                        ?>
                    </tr>
                </tfoot>
            </table>
            <? } else { ?>
            <span>No Data Available</span> 
            <? }
            ?>
        </div>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->