
<div ng-controller="paymentController" ng-init="load()"></div>
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/payment/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Purchase</span> - Payment</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!/payment/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!"><i class="icon-home2 position-left"></i> Purchase</a></li>
            <li><a href="#!/payment/all"><i class="icon-city position-left"></i> Payment</a></li>
            <li class="active"><?php
                if (isset($payment))
                    echo 'PAY' . str_pad($payment['payment_id'], 5, "0", STR_PAD_LEFT);
                else
                    echo "New";
                ?></li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Form validation -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Payment</h5>

        </div>


        <div class="panel-body">
            <?php
            $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id'=>'merpDocForm');
            if (isset($payment))
                echo form_open('web/Payment/save/' . $payment['payment_id'], $attributes);
            else
                echo form_open('web/Payment/save', $attributes);
            ?>
            <p class="content-group-lg">Payment Voucher is a confirmation document recording the payment made to vendors towards supplies received. Add all payments here to ensure accurate payment status.</p>

            <fieldset class="content-group">
                <legend class="text-bold">Vendor Details</legend>
                <div class="col-lg-12">

                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Vendor Name <span class="text-danger">*</span></label>
                        <div class="col-lg-9">

                            <?
                            if (isset($payment)) {
                                ?>

                                <input class="form-control" type="text"
                                       value = "<? echo $payment['payment_linked_company_invoice_name']; ?>" readonly>
                                       <? } else {
                                       ?>

                                <select class="select2 form-control" name = "payment_linked_company_id" id = "payment_linked_company_id" onchange = "refreshPurchaseListForPayment();">
                                    <option value = "">Choose an company</option>
                                    <? foreach ($purchase_customers as $purchase_customer) {
                                    ?>
                                    <option value = "<? echo $purchase_customer['purchase_linked_company_id'] ?>"

                                            ><? echo $purchase_customer['purchase_linked_company_display_name'] ?></option>
                                            <?
                                        }
                                        ?>

                                <? }
                                ?>

                            </select>													
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Purchase Invoice Number <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <?
                            if (isset($payment)) {
                                            
                            ?>

                            <input class="form-control" type="text"
                                   value = "PUR<?php echo str_pad($payment['payment_linked_purchase_id'], 5, "0", STR_PAD_LEFT); ?>   " readonly>
                                   <? } else {
                                   ?>
                            <select class="select2 form-control" name="payment_linked_purchase_id" id = "payment_linked_purchase_id" onchange = "updatePaymentBalanceAmount(this);" required>		
                            </select>
                        <? } ?>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="form-group col-lg-6  has-feedback has-feedback-left">
                        <label class="control-label col-lg-3">Purchase Amount</label>
                        <div class="col-lg-9">
                            <input class="form-control" id = "" tabindex="2" name="payment_linked_purchase_amount" type="text"
                                   value = "<?php
                                   if (isset($payment))
                                                   echo $payment['payment_linked_purchase_amount'];
                                       
                                       else
                                   echo "0";
                                   ?>" required readonly>
                            <div class="form-control-feedback" style = "left:10px">
                                <span style = "font-size:16px;"> ₹</span>
                            </div>
                        </div>
                    </div>	
                    <div class="form-group col-lg-6 has-feedback has-feedback-left">
                        <label class="control-label col-lg-3">Amount Paid <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" id = "" tabindex="2" required name="payment_amount" type="number" placeholder = "Should be less that or equal to balance amount"
                                   value = "<?php if (isset($payment)) echo $payment['payment_amount']; ?>" <?php if ( isset($payment)) echo "payment_balance='" . ($purchase_vendor_map [0]['payment_balance'] + $payment [ 'payment_amount']) . "'"; ?> required step = 0.01 onchange ="limitToPaymentBalance(this)">
                                <div class="form-control-feedback" style = "left:10px">
                                    <span style = "font-size:16px;"> ₹</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group col-lg-6">
                            <label class="control-label col-lg-3">Payment Mode <span class="text-danger">*</span></label>
                            <div class="col-lg-9">
                                <select class="form-control select2" id = "payment_payment_mode" name="payment_payment_mode" tabindex="3" required>
                                    <option value = "">Choose a payment mode</option>
                                    <option value = "cash" <?php
                                if (isset($payment))
                                                if ($payment['payment_payment_mode'] == "cash")
                                    {
                                            echo " selected";
                                    };
                                    ?>>Cash</option>
                                <option value = "cheque" <?php
                                        if (isset($payment))
                                                        if ($payment['payment_payment_mode'] == "cheque"
                                            ) {
                                                    echo " selected";
                                            };
                                            ?>>Cheque</option>
                                <option value = "bank transfer" <?php
                            if (isset($payment))
                                            if ($payment['payment_payment_mode'] ==
                                "bank transfer") {
                                        echo " selected";
                                };
                                            ?>>Bank Transfer</option>
                                <option value = "bank deposit" <?php
                                if (isset($payment))
                                                if ($payment['payment_payment_mode'] ==
                                    "bank deposit") {
                                            echo " selected";
                                    };
                                    ?>>Bank Deposit</option>
                                <option value = "demand draft" <?php
                                if (isset($payment))
                                                if ($payment['payment_payment_mode'] ==
                                    "demand draft") {
                                            echo " selected";
                                    };
                                    ?>>Demand Draft</option>
                            </select>
                        </div>
                    </div>		
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Date</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name="payment_date" id = "datePicker"
                                   value = "<?php
                                if (isset($payment))
                                                echo explode (" ", $payment['payment_date'])[0];
                                    
                                             else
                                    echo date ('Y-m-d');
                                    
                                    ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group col-lg-6">
                                    <label class="control-label col-lg-3">Pay From <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <select class="select2 form-control" name = "payment_credit_coa_id" required>
                                            <option value = "">Choose an account</option>
                                            <? foreach ($coas as $coa) {
                                            ?> 
                                            <option value = "<? echo $coa['coa_id'] ?>" <?php
                                               if (isset($payment)){
                                                   if ($payment['payment_credit_coa_id'] == $coa[
                                                           'coa_id']) {
                                                       echo "selected";
                                                   }
                                               } else if (isset($payment_setting)){
                                                   if ($payment_setting['credit_account_coa_id'] == $coa[
                                                           'coa_id']) {
                                                       echo "selected";
                                                   }
                                               }
                                            ?>><? echo $coa['coa_account_name'] ?></option>
                                            <?
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Transaction Reference #</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name = "payment_transaction_reference" type="text"
                                   value = "<?php if (isset($payment)) echo $payment['payment_transaction_reference']; ?>">
                        </div>
                    </div>	
                </div>


            </fieldset>

            <div class="text-right">

                <a href="#!/payment/all"><button class="btn btn-default">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                <? if($this->session->userdata('access_controller')->is_access_granted('payment', 'save')) { ?>
                <button onclick="submitDocForm('payment', 'previewConfirm')" id ="buttonSubmitPayment" class="btn btn-primary">Confirm <i class="icon-arrow-right14 position-right"></i></button>
                <? } ?>
            </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->