
<!-- NOTE -

CERTAIN FIELDS HAVE DELIBERATELY NOT BEEN GIVEN A NAME SINCE THOSE FIELDS ARE UNEDITABLE 
EXAMPLE - SUBSCRIBED MODULES, APPLICATION STATUS, RENEWAL DATE, SUBSCRIPTION DAYS REMAINING
THOSE FIELDS SHOULD BE EDITED FROM BACK END ONLY

-->
<div ng-controller="appSettingsController" ng-init="load()"></div> 
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/location/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Company Profile & Preferences</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                    <!--<a href="#!/location/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>-->
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-cog3 position-left"></i>Settings</li>
            <li class="active"><i class="icon-city position-left"></i>Company Profile & Preferences</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Form validation -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Profile</h5>

        </div>
        <div class="panel-body">
            <p class="content-group-lg">Company Profile & System Preferences include primary information of the business and its system preferences. The  details configured here will reflect in all documents and transactions.</p>
            <?php
            $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id'=>'merpForm');
            echo form_open_multipart('web/settings/profile/', $attributes);
            ?>
            <div class="tabbable" id = "homeTabs">
                <ul class="nav nav-tabs nav-tabs-highlight">
                    <li class="active" id = "0"><a href="#highlighted-tab1" data-toggle="tab" target="_self">Basic Details</a></li>
                    <li id = "1"><a href="#highlighted-tab2" data-toggle="tab" target="_self">Tax & Compliance Details</a></li>
                    <li id = "2"><a href="#highlighted-tab3" data-toggle="tab" target="_self">Terms</a></li>
                    <!--<li id = "3"><a href="#highlighted-tab4" data-toggle="tab" target="_self">Reports & Notifications</a></li>-->
                    <li id = "3"><a href="#highlighted-tab5" data-toggle="tab" target="_self">Subscription Details</a></li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="highlighted-tab1">
                        <div class="col-lg-12">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label col-lg-3">Company Type <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <select class="select form-control" name="oc_type" tabindex = "1" required>
                                            <option>Choose a business type</option>
                                            <option value = "trading" <?php if (isset($owner_company)) if ($owner_company['oc_type'] == 'trading') echo 'selected'; ?>>Trading</option>
                                            <option value = "manufacturing" <?php if (isset($owner_company)) if ($owner_company['oc_type'] == 'manufacturing') echo 'selected'; ?>>Manufacturing</option>
                                            <option value = "service" <?php if (isset($owner_company)) if ($owner_company['oc_type'] == 'service') echo 'selected'; ?>>Service</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-lg-3" for="oc_name">Company Name <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input class="form-control" required name="oc_name" type="text" style="text-transform: capitalize;" value = "<?php if (isset($owner_company)) echo $owner_company['oc_name']; ?>" tabindex = "2" placeholder="Name of your company/business">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-lg-3" for="oc_registered_address">Registered Address <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input class="form-control" required name="oc_registered_address" type="text" value = "<?php if (isset($owner_company)) echo $owner_company['oc_registered_address']; ?>" tabindex = "3" placeholder="Registered address of your company/business">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-lg-3" for="oc_billing_address">Billing Address <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input class="form-control" required name="oc_billing_address" type="text" value = "<?php if (isset($owner_company)) echo $owner_company['oc_billing_address']; ?>" tabindex = "4" placeholder = "Billing address of your company/business">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-lg-3" for="oc_billing_name">Billing Name <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input class="form-control" required name="oc_billing_name" type="text" value = "<?php if (isset($owner_company)) echo $owner_company['oc_billing_name']; ?>" tabindex = "5" placeholder="Enter billing name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-3" for="oc_pincode">Pin Code <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input class="form-control" required name="oc_pincode" type="text" value = "<?php if (isset($owner_company)) echo $owner_company['oc_pincode']; ?>" tabindex = "6" placeholder="">
                                    </div>
                                </div>												
                                <div class="form-group"> 
                                    <label class="control-label col-lg-3" for="city">City <span class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                       <select required class="form-control select2" name="oc_location_id" id="city" tabindex = "7" onChange = "refreshLocationDistrictAndState()" required>
                                                    <option value = ''>Choose a location</option>
                                                    <?php
                                                    foreach ($locations as $location) {
                                                        ?>
                                                        <option district = "<?php echo $location['district'] ?>" state = "<?php echo $location['state_name'] ?>" 
                                                        <?php
                                                        if (set_value('oc_location_id') == $location['location_id'])
                                                            echo "selected";
                                                        else if (isset($owner_company)) {
                                                            if ($owner_company['oc_location_id'] == $location['location_id'])
                                                                echo "selected";
                                                        }
                                                        ?> value="<? echo $location['location_id']; ?>"><? echo $location['city_name'] ?>
                                                        </option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>	
                                    </div>
                                </div>	

                                <div class="form-group">
                                    <label class="control-label col-lg-3" for="district">District</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" id="district" type="text" value = "<?php echo $owner_company['district']; ?>" readonly>
                                    </div>
                                </div>							

                                <div class="form-group">
                                    <label class="control-label col-lg-3" for="state">State </label>
                                    <div class="col-lg-9">
                                        <input class="form-control" id="state" type="text" value = "<?php echo $owner_company['billing_state_name']; ?>" readonly>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-lg-3" for="oc_bank_details">Bank Details</label>
                                    <div class="col-lg-9">
                                        <textarea class="form-control" name = "oc_bank_details" rows="10" cols="10" tabindex = "19">
                                            <?php if (!empty($owner_company['oc_bank_details'])) echo $owner_company['oc_bank_details']; 
                                            else
                                                echo 
'
Bank Name:  
Account Name:   
Account Number: 
Branch: 
IFSC Code:  
Swift Code: ';?>
                                        </textarea>
                                    </div>
                                </div>
                                <fieldset class="content-group">
                                    <legend class="text-bold">Contact Details</legend>
                                    <div class="form-group">
                                        <label class="control-label col-lg-3" for="oc_contact_person">Contact Person <span class="text-danger">*</span></label>
                                        <div class="col-lg-9">
                                            <input class="form-control" name="oc_contact_person" type="text" style="text-transform: capitalize;" value = "<?php if (isset($owner_company)) echo $owner_company['oc_contact_person']; ?>"  tabindex = "8">
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label col-lg-3" for="oc_email_address">Email Address <span class="text-danger">*</span></label>
                                        <div class="col-lg-9">
                                            <input class="form-control" name="oc_email_address" type="text" value = "<?php if (isset($owner_company)) echo $owner_company['oc_email_address']; ?>" tabindex = "9">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-lg-3" for="oc_contact_number">Contact Number <span class="text-danger">*</span></label>
                                        <div class="col-lg-9">
                                            <input class="form-control format-phone-number" name="oc_contact_number" type="text" value = "<?php if (isset($owner_company)) echo $owner_company['oc_contact_number']; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' tabindex = "10">
                                        </div>
                                    </div>							
                                </fieldset>

                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Company Logo</label>
                                    <div class="col-lg-9">
                                        <input type="file" name = "oc_logo_path" id = "oc_logo_path" class="file-input-preview" data-show-remove="false"  data-dismiss="fileupload" data-show-upload="false" data-image-path = "<?php if (isset($owner_company)) echo $owner_company['oc_logo_path'] ?>" data-image-name = "<?php if (isset($owner_company)) echo $owner_company['oc_logo_path'] ?>">
                                        <span class="help-block">Maximum file upload size is <code>500 KB.</code> </span>
                                    </div>
                                </div>		
                            </div>

                        </div>

                    </div>

                    <div class="tab-pane" id="highlighted-tab2">
                        <fieldset class="content-group">
                            <legend class="text-bold">Compliance</legend>


                            <div class="form-group">
                                <label class="control-label col-lg-3">GST State<span class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <select class="select2 form-control" name="oc_gst_supply_state_id" tabindex = "11">
                                        <option>Choose a state</option>
                                        <optgroup label="States">
                                            <?php
                                            foreach ($states as $state) {
                                                ?>
                                                <option 
                                                <?php
                                                if (set_value('oc_gst_supply_state_id') == $state['state_tin_number'])
                                                    echo "selected";
                                                else if (isset($owner_company)) {
                                                    if ($owner_company['oc_gst_supply_state_id'] == $state['state_tin_number'])
                                                        echo "selected";
                                                }
                                                ?> value="<? echo $state['state_tin_number']; ?>"><? echo $state['state_name'] ?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </optgroup>
                                        <optgroup label = "Union Territories">
                                            <?php
                                            foreach ($union_territories as $union_territory) {
                                                ?>
                                                <option 
                                                <?php
                                                if (set_value('oc_gst_supply_state_id') == $union_territory['state_tin_number'])
                                                    echo "selected";
                                                else if (isset($owner_company)) {
                                                    if ($owner_company['oc_gst_supply_state_id'] == $union_territory['state_tin_number'])
                                                        echo "selected";
                                                }
                                                ?> value="<? echo $union_territory['state_tin_number']; ?>"><? echo $union_territory['state_name'] ?>
                                                </option>
                                                <?php
                                            }
                                            ?>

                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-3" for="oc_gst_number">Company GST Number</label>
                                <div class="col-lg-9">
                                    <input class="form-control" onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9@]+/, '')" name="oc_gst_number" type="text" value = "<?php if (isset($owner_company)) echo $owner_company['oc_gst_number']; ?>" tabindex = "11" placeholder = "Should be 15 characters">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-3" for="oc_pan_number">Company PAN Number</label>
                                <div class="col-lg-9">
                                    <input class="form-control" name="oc_pan_number" type="text" value = "<?php if (isset($owner_company)) echo $owner_company['oc_pan_number']; ?>" tabindex = "12" placeholder = "Example - ABCC8787C">
                                </div>
                            </div>
                            <div class="form-group">
                                   <label class="control-label col-lg-3">Default Tax Type (Sales)</label>
                                   <div class="col-lg-9">
                                       <select class="form-control select" name="oc_sales_default_tax_type">
                                           <option value = "exclusive" <?php if (isset($owner_company)) if ($owner_company['oc_sales_default_tax_type'] == 'exclusive') echo "selected"; ?>>Exclusive</option>
                                           <option value = "inclusive" <?php if (isset($owner_company)) if ($owner_company['oc_sales_default_tax_type'] == 'inclusive') echo "selected"; ?>>Inclusive</option>													
                                       </select>
                                   </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-3">Default Tax Type (Purchase)</label>
                                <div class="col-lg-9">
                                    <select class="form-control select" name="oc_purchase_default_tax_type">
                                        <option value = "exclusive" <?php if (isset($owner_company)) if ($owner_company['oc_purchase_default_tax_type'] == 'exclusive') echo "selected"; ?>>Exclusive</option>
                                        <option value = "inclusive" <?php if (isset($owner_company)) if ($owner_company['oc_purchase_default_tax_type'] == 'inclusive') echo "selected"; ?>>Inclusive</option>													
                                    </select>
                                </div>
                            </div>
                        </fieldset>
                        <!--<fieldset class="content-group">
                            <legend class="text-bold">Payment Terms</legend>
                            <div class="form-group">
                                <label class="control-label col-lg-3" for="oc_payment_terms_1">Term 1</label>
                                <div class="col-lg-9">
                                    <textarea class="form-control" name = "oc_payment_terms_1" rows="10" cols="10"tabindex = "13"><?php if (isset($owner_company)) echo $owner_company['oc_payment_terms_1']; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-3" for="oc_payment_terms_2">Term 2</label>
                                <div class="col-lg-9">
                                    <textarea class="form-control" name = "oc_payment_terms_2" rows="10" cols="10" tabindex = "14"><?php if (isset($owner_company)) echo $owner_company['oc_payment_terms_2']; ?></textarea>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="content-group">
                            <legend class="text-bold">Taxation</legend>
                            <div class="form-group">
                                <label class="control-label col-lg-3">Tax Type</label>
                                <div class="col-lg-9">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name = "oc_tax_is_inclusive" class="control-primary" tabindex = "15" <?php if (isset($owner_company)) if ($owner_company['oc_tax_is_inclusive'] == '1') echo "checked"; ?>>
                                            Inclusive
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name = "oc_tax_is_exclusive" class="control-primary" tabindex = "16" <?php if (isset($owner_company)) if ($owner_company['oc_tax_is_exclusive'] == '1') echo "checked"; ?>>
                                            Exclusive
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-3" for="oc_gst_rate_applicable">GST Rate Applicable</label>
                                <div class="col-lg-9">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" class="control-primary" tabindex = "17"name = "oc_gst_rate_applicable">
                                            Interstate 18%
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" class="control-primary" tabindex = "18" name = "oc_gst_rate_applicable">
                                            Intrastate 18%
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>-->
                    </div>

                    <div class="tab-pane" id="highlighted-tab3"> 
                        <div class="form-group">
                            <label class="control-label col-lg-3" for="oc_invoice_terms">Invoice Terms</label>
                            <div class="col-lg-9">
                                <textarea class="form-control" name = "oc_invoice_terms" rows="10" cols="10" tabindex = "19"><?php if (isset($owner_company)) echo $owner_company['oc_invoice_terms']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3" for="oc_purchase_terms">Purchase Terms</label>
                            <div class="col-lg-9">
                                <textarea class="form-control" name = "oc_purchase_terms" rows="10" cols="10" tabindex = "20"><?php if (isset($owner_company)) echo $owner_company['oc_purchase_terms']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3" for="oc_quotation_terms">Quotation Terms</label>
                            <div class="col-lg-9">
                                <textarea class="form-control" name = "oc_quotation_terms" rows="10" cols="10" tabindex = "20"><?php if (isset($owner_company)) echo $owner_company['oc_quotation_terms']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3" for="oc_pro_forma_terms">Pro-forma Terms</label>
                            <div class="col-lg-9">
                                <textarea class="form-control" name = "oc_pro_forma_terms" rows="10" cols="10" tabindex = "19"><?php if (isset($owner_company)) echo $owner_company['oc_pro_forma_terms']; ?></textarea>
                            </div>
                        </div>
                        
                    </div>

                    <!--<div class="tab-pane" id="highlighted-tab4">
                        <div class="form-group">
                            <label class="control-label col-lg-3" for="oc_summary_report_frequency">Summary Report via email </label>
                            <div class="col-lg-9">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="oc_summary_report_frequency" value = "daily" class="control-primary" tabindex = "21" <?php if (isset($owner_company)) if ($owner_company['oc_summary_report_frequency'] == 'daily') echo "checked"; ?>>
                                        Daily
                                    </label>
                                </div>

                                <div class="radio">
                                    <label>
                                        <input type="radio" name="oc_summary_report_frequency" value = "weekly" class="control-primary" tabindex = "22" <?php if (isset($owner_company)) if ($owner_company['oc_summary_report_frequency'] == 'weekly') echo "checked"; ?>>
                                        Weekly
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="oc_summary_report_frequency" value = "monthly" class="control-primary" tabindex = "23" <?php if (isset($owner_company)) if ($owner_company['oc_summary_report_frequency'] == 'monthly') echo "checked"; ?>>
                                        Monthly
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3">Alert / Push Notifications </label>
                            <div class="col-lg-9">
                                <div class="checkbox checkbox-switch">
                                    <label>
                                        <input type="checkbox" name="oc_receive_push_notification" data-on-text="Yes" data-off-text="No" class="switch" <?php if (isset($owner_company)) if ($owner_company['oc_receive_push_notification'] == '1') echo "checked"; ?> tabindex = "24">
                                        Receive 
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>-->
                    <div class="tab-pane" id="highlighted-tab5">
                        <div class="form-group">
                            <label class="control-label col-lg-3" for="company_gst_number">Subscribed Modules </label>
                            <div class="col-lg-9">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="control-primary" tabindex = "25" <?php if (in_array('sales', explode(",", $owner_company['oc_subscribed_modules']))) echo "checked"; ?> onclick="this.checked = !this.checked;">
                                        Sales
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="control-primary" tabindex = "26" <?php if (in_array('purchase', explode(",", $owner_company['oc_subscribed_modules']))) echo "checked"; ?> onclick="this.checked = !this.checked;">
                                        Purchase
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="control-primary" tabindex = "27" <?php if (in_array('inventory', explode(",", $owner_company['oc_subscribed_modules']))) echo "checked"; ?> onclick="this.checked = !this.checked;">
                                        Inventory
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="control-primary" tabindex = "27" <?php if (in_array('manufacturing', explode(",", $owner_company['oc_subscribed_modules']))) echo "checked"; ?> onclick="this.checked = !this.checked;">
                                        Manufacturing
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="control-primary" tabindex = "28" <?php if (in_array('accounts', explode(",", $owner_company['oc_subscribed_modules']))) echo "checked"; ?> onclick="this.checked = !this.checked;">
                                        Accounts
                                    </label>
                                </div><div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="control-primary" tabindex = "29" <?php if (in_array('reports', explode(",", $owner_company['oc_subscribed_modules']))) echo "checked"; ?> onclick="this.checked = !this.checked;">
                                        Reports
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3">Status </label>
                            <div class="col-lg-9">
                                <div class="checkbox checkbox-switch">
                                    <label>
                                        <input type="checkbox" data-off-color="danger" data-on-text="Active" data-off-text="Inactive" class="switch" <?php if (isset($owner_company)) if ($owner_company['oc_application_status'] == 'active') echo "checked"; ?> readonly>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3">Subscription Start Date</label>
                            <div class="col-lg-9">
                                <input class="form-control" id = "datePicker" readonly tabindex="31" value = "<? if (isset($owner_company)) echo date("d F, Y", strtotime($owner_company['oc_subscription_start_date']));  ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3">Subscription End Date</label>
                            <div class="col-lg-9">
                                <input class="form-control" id = "datePicker" readonly tabindex="32" value = "<? if (isset($owner_company)) echo date("d F, Y", strtotime($owner_company['oc_subscription_end_date']));  ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3">Subscription Days Remaining</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="text" readonly tabindex="33" value = <?
                                if (isset($owner_company)) {
                                    date_default_timezone_set('Asia/Kolkata');
                                    $date_now = new DateTime("now");
                                    $date_end_subscription = new DateTime($owner_company['oc_subscription_end_date']);
                                    $date_diff = $date_now->diff($date_end_subscription)->format("%r%a"); 
                                    echo $date_diff; 
                                }
                                ?>>
                            </div>
                        </div>
                    </div>


                </div>
            </div>

            <div class="text-right">
                <a href="#!/dashboard/one"><button class="btn btn-default">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                <button onclick="submitForm('Preferences', 'settings/profile');" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
            </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->