<div style = "padding-top:20px;padding-left:20px;padding-right:20px;">
    <?php echo validation_errors('<div style = "border-color: #ebccd1;background-color:#f2dede; color:#a94442" class="alert alert-danger alert-dismissable"><a style = "padding-bottom:10px" href="#" class="close" data-dismiss="alert" aria-label="close">×</a>', '</div>'); ?>
</div>
<div ng-controller="formController" ng-init="load()"></div>
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/COA/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Accounts</span> - Chart Of Accounts</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!/COA/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!"><i class="icon-calculator2 position-left"></i> Accounts</a></li>
            <li><a href="#!/COA/all"><i class="icon-book3 position-left"></i> Chart Of Accounts</a></li>
            <li class="active"><?php
                if (isset($coa))
                    echo 'COA' . str_pad($coa['coa_id'], 5, "0", STR_PAD_LEFT);
                else
                    echo "New";
                ?></li>
        </ul>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">

    <!-- Form validation -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Chart Of Accounts</h5>

        </div>
        <div class="panel-body">
            <p class="content-group-lg">The Chart of Accounts comprises of all the ledger accounts of the company. Configure your Chart of Accounts to generate accurate Financial Statements.</p>
            <?php echo validation_errors(); ?>
            <?php
            $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id'=>'merpForm');
            if (isset($coa))
                echo form_open('web/COA/save/' . $coa['coa_id'], $attributes);
            else
                echo form_open('web/COA/save', $attributes);
            ?>
            <fieldset class="content-group">
                <legend class="text-bold">Basic inputs</legend>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Account Type <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select required class="select2 form-control" name="coa_at_id" tabindex="1" id = "coa_at_id" 
                                    <?if(isset($coa['coa_subaccount_of_id'])){
                                        echo " disabled";
                                    } ?>>
                                <option value = "">Choose an account type</option>
                                <optgroup label="Accounts">
                                    <?php
                                    foreach ($account_types as $account_type) {
                                        ?>
                                        <option 
                                        <?php
                                        if (set_value('coa_at_id') == $account_type['at_id'])
                                            echo "selected";
                                        else if (isset($coa)) {
                                            if ($coa['coa_at_id'] == $account_type['at_id'])
                                                echo "selected";
                                        }
                                        ?> value="<? echo $account_type['at_id'] ?>">
                                            <? echo $account_type['at_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?> 
                                </optgroup>
                            </select>
                        </div>
                    </div>							
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Account Name<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" required style="text-transform: capitalize;" type="text" name = "coa_account_name" 
                                   onblur ="checkIfUnique(this, 'chart_of_accounts', 'coa_account_name', '<? if(isset($coa)) echo $coa['coa_id']; ?>', 'coa_id')"
                                   value="<? if(isset($coa)) echo $coa['coa_account_name']; ?>" tabindex="2"
                                   placeholder = "Enter name for account">
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Sub-account</label>
                        <div class="col-lg-9">
                            <div class="checkbox checkbox-switch">
                                <label>
                                    <input type="checkbox" data-on-text="Yes" data-off-text="No" class="switch" <?php
                                    if(isset($coa['coa_subaccount_of_id'])){
                                        echo " checked";
                                    } 
                                    ?> tabindex="3" onchange="document.getElementById('coa_subaccount_of_id').disabled = !this.checked;
                                                                document.getElementById('coa_at_id').disabled = this.checked">
                                    
                                </label>
                            </div> 
                        </div>		
                    </div>

                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Parent Account</label>
                        <div class="col-lg-9">
                            <select required class="select2 form-control" name="coa_subaccount_of_id" tabindex="4" id = "coa_subaccount_of_id"
                                    <?if(!isset($coa['coa_subaccount_of_id'])){
                                        echo " disabled";
                                    } ?>>
                                <option value =''>Choose Parent Account</option>
                                <?php
                                    foreach ($parent_accounts as $parent_account) {
                                        if($parent_account['coa_id'] == $coa['coa_id'])
                                            continue;
                                        ?>
                                        <option 
                                        <?php
                                        if (set_value('coa_subaccount_of_id') == $parent_account['coa_id'])
                                            echo "selected";
                                        else if (isset($coa)) {
                                            if ($coa['coa_subaccount_of_id'] == $parent_account['coa_id'])
                                                echo "selected";
                                        }
                                        ?> value="<? echo $parent_account['coa_id'] ?>">
                                            <? echo $parent_account['coa_account_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Account Code</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="5" type="text" name = "coa_account_code"
                                   value="<?php
                                    if(isset($coa))
                                            echo $coa['coa_account_code'];
                                    ?>">
                        </div>
                    </div>	
                    <div class="form-group col-lg-6" id = "ia_stock_quantity_container">
                        <label class="control-label col-lg-3">Account Description </label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="6" type="text" name ="coa_account_description"
                                   value="<? if(isset($coa)) echo $coa['coa_account_description']; ?>">
                        </div>
                    </div>	
                </div>
                
            </fieldset>

            <div class="text-right">
                <a href="#!/COA/all"><button class="btn btn-default">Cancel<i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                <? 
                if($this->session->userdata('access_controller')->is_access_granted('chart_of_accounts', 'save')) { ?>
                <button onclick="submitForm('Chart Of Account', 'COA/all');" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
                <? } ?>
            </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
