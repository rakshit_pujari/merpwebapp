

<!-- Page header -->
<div class="page-header">

    <div ng-controller="noGroupDataTableController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Accounting</span> - Transactions</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!SJ/ledger/<? echo $coa_id; ?>/company/all" class="btn btn-link btn-float has-text"><i class="icon-office position-left" style = "font-size:22px;color:#26A69A !important"></i> <span>Company Wise</span></a>

                <!--<a href="report/download/ledger/<? echo $transactions[0]['sj_account_coa_id']; ?>" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>-->
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-calculator2 position-left"></i> Accounting</li>
            <li><a href="#!/SJ/ledger/"><i class="icon-book2 position-left"></i>Ledger</a></li>
            <li <? if(!isset($entity_id)) echo 'class="active"'; ?>>
                <? if(isset($entity_id)) echo '<a href="#!/SJ/ledger/'.$coa_id.'">'; ?>
                    <? echo $transactions[0]['coa_account_name']; ?>
                <? if(isset($entity_id)) echo '</a>'; ?>
            </li>
            <?
            if (!empty($transactions[0]['sj_linked_broker_id'])){ 
                echo '<li class="active">'.$transactions[0]['broker_name'].'</li>'; 
            } else if (!empty($transactions[0]['sj_linked_company_id'])){ 
               echo '<li class="active">'.$transactions[0]['company_display_name'].'</li>'; 
            } else if (!empty($transactions[0]['sj_linked_employee_id'])){ 
               echo '<li class="active">'.$transactions[0]['employee_name'].'</li>'; 
            } else if (!empty($transactions[0]['sj_linked_transporter_id'])){ 
               echo '<li class="active">'.$transactions[0]['transporter_name'].'</li>'; 
            }                        
            ?>
        </ul>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Transactions archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title"><? if(!empty($transactions[0]['coa_account_name'])){
                echo $transactions[0]['coa_account_name'];                 
            } else {
                echo 'Transactions';
            } ?></h6>
            <div class="heading-elements">
                </ul>
            </div>
        </div>

        <table class="table noGroupDataTable">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Transactions ID</th>
                    <th>Entity</th>
                    <th>Debit</th>
                    <th>Credit</th>
                    <th>Running Balance</th>
                    <th>Description</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $current_journal_id = '';
                    $debit_total = 0;
                    $credit_total = 0;
                    $running_balance = 0;
                    foreach ($transactions as $transaction): ?>
                    
                <tr>
                    <td>
                        <?
                        echo $transaction['sj_date'];
                        ?>
                    </td>
                    <td>
                        <?
                        //echo $transaction['sj_document_id_text'];
                        if ($transaction['sj_document_type'] == 'invoice') {
                                echo 'Tax Invoice - <a href = "master#!/invoice/preview/confirm/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'credit_note') {
                                echo 'Credit Note - <a href = "master#!/CreditNote/view/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'purchase') {
                                echo 'Purchase - <a href = "master#!/purchase/view/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'debit_note') {
                                echo 'Debit Note - <a href = "master#!/DebitNote/view/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'journal') {
                                echo 'Journal - <a href = "master#!/journal/view/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'coa_opening_balance') {
                                echo 'Opening Balance - <a href = "master#!/COA/view/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'receipt') {
                                echo 'Receipt - <a href = "master#!/receipt/view/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'advance_receipt') {
                                echo 'Advance Receipt - <a href = "master#!/AdvanceReceipt/preview/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'payment') {
                                echo 'Payment - <a href = "master#!/payment/view/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'advance_payment') {
                                echo 'Advance Payment - <a href = "master#!/AdvancePayment/preview/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'expense') {
                                echo 'Expense - <a href = "master#!/expense/view/confirm/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'inventory_adjustment') {
                                echo 'Inventory Adjustment - <a href = "master#!/Inventory/adjustment/view/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'broker') {
                                echo 'Opening Balance for Broker - <a href = "master#!/broker/view/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'company') {
                                echo 'Opening Balance for Company - <a href = "master#!/company/view/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'employee') {
                                echo 'Opening Balance for Employee - <a href = "master#!/employee/view/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else if ($transaction['sj_document_type'] == 'transporter') {
                                echo 'Opening Balance for Transporter - <a href = "master#!/transporter/view/' . $transaction['sj_document_id'] . '">' . $transaction['sj_document_id_text'] . '</a>';
                            } else {
                                echo '<span style="color:#4CAF50">' . $transaction['sj_document_type'] .' - '.$transaction['sj_document_id_text'] . '</span>';
                            }
                        ?>
                    </td>
                    <td>
                        <?
                        if (isset($transaction['sj_linked_broker_id'])){ ?>
                            Broker - <a href = "master#!/SJ/ledger/<? echo $transaction['sj_account_coa_id']; ?>/broker/<? echo $transaction['sj_linked_broker_id']; ?>"><? echo $transaction['broker_name']; ?></a>
                        <? } else if (isset($transaction['sj_linked_company_id'])){ ?>
                            Company - <a href = "master#!/SJ/ledger/<? echo $transaction['sj_account_coa_id']; ?>/company/<? echo $transaction['sj_linked_company_id']; ?>"><? echo $transaction['company_display_name']; ?></a>
                        <? } else if (isset($transaction['sj_linked_employee_id'])){ ?>
                            Employee - <a href = "master#!/SJ/ledger/<? echo $transaction['sj_account_coa_id']; ?>/employee/<? echo $transaction['sj_linked_employee_id']; ?>"><? echo $transaction['employee_name']; ?></a>
                        <? } else if (isset($transaction['sj_linked_transporter_id'])){ ?>
                            Transporter - <a href = "master#!/SJ/ledger/<? echo $transaction['sj_account_coa_id']; ?>/transporter/<? echo $transaction['sj_linked_transporter_id']; ?>"><? echo $transaction['transporter_name']; ?></a>
                        <? }                        
                        ?>
                        
                    </td>
                    <td>
                        <?
                        if($transaction['sj_transaction_type'] == 'debit'){
                            $debit_total+=$transaction['sj_amount'];
                            echo '₹ '. number_format($transaction['sj_amount'], 2);
                        }
                        ?>
                    </td>
                    <td>
                        <?
                        if($transaction['sj_transaction_type'] == 'credit'){
                            $credit_total+=$transaction['sj_amount'];
                            echo '₹ '.number_format($transaction['sj_amount'], 2);
                        }
                        ?>
                    </td>
                    <td><? $running_balance = $debit_total - $credit_total; 
                            echo '₹ '.number_format(abs($running_balance), 2); 
                            if($running_balance < 0)
                                echo ' (CR)';
                            else if ($running_balance > 0)
                                echo ' (DR)';?></td>
                    <td>
                        <? echo $transaction['sj_narration']; ?>
                    </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th><? echo '₹ '.number_format($debit_total, 2); ?></th>
                    <th><? echo '₹ '.number_format($credit_total, 2); ?></th>
                    <th></th>
                    <th></th>
                </tr>
                <tr>
                    <th></th>
                    <th></th>
                    <th>Balance</th>
                    <th><? if($debit_total > $credit_total){
                        echo '₹ '.number_format(abs($debit_total - $credit_total), 2).' (DR)';
                    } ?></th>
                    <th><? if($debit_total < $credit_total) {
                        echo '₹ '.number_format(abs($debit_total - $credit_total), 2).' (CR)';
                    } ?></th>
                    <th></th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->