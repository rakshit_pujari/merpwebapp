

<!-- Page header -->
<div class="page-header">

    <div ng-controller="dataTableController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Master</span> - Company</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!company/view" class="btn btn-link btn-float has-text"><i class="icon-office text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Add Company</span></a>
                <a href="report/download/company" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-home2 position-left"></i>Master</li>
            <li class="active"><i class="icon-office position-left"></i>Company</li>
        </ul>
    </div>
</div>
<!-- /page header -->



<!-- Content area -->
<div class="content">

    <!-- Invoice archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Company</h6>
            <div class="heading-elements">
            </div>
        </div>

        <table class="table masterDataTable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Company Name</th>
                    <th>Type</th>
                    <th>City</th>
                    <th>Contact Person</th>
                    <th>Contact Number</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($companies as $company): ?>
                    <tr>
                        <td>COM<?php echo str_pad($company['company_id'], 5, "0", STR_PAD_LEFT); ?></td>
                        <td>
                            <h6 class="no-margin">
                                <a href="#!company/view/<?php echo $company['company_id']; ?>"><span><?php echo $company['company_display_name']; ?></span></a>
                                <small class="display-block text-muted">Created by <?php echo $company['employee_name']; ?> 
                                    on <?php echo $company['company_record_creation_time']; ?></small>
                            </h6>
                        </td>
                        <td>
                            <?php
                            if ($company['company_type'] == 'customer')
                                echo '<span class="label bg-danger"><span>' . $company['company_type'] . '</span></span>';
                            else
                                echo '<span class="label bg-blue"><span>' . $company['company_type'] . '</span></span>';
                            ?>
                        </td>
                        <td><?php echo $company['billing_city_name']; ?></td>
                        <td><?php echo $company['company_contact_person_name']; ?></td>
                        <td><?php echo $company['company_contact_number']; ?></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li><a href="#!company/view/<?php echo $company['company_id']; ?>"><i class="icon-file-eye"></i></a></li>
                                <? 
                                if($this->session->userdata('access_controller')->is_access_granted('company', 'delete')) { ?>
                                <li><a href='javascript: void(0)' onclick = 'showDeleteDialog("company", <?php echo $company['company_id']; ?>)'><i class="glyphicon glyphicon-trash"></i></a></li>
                                <? } ?>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td>ID</td>
                    <td>Company Name</td>
                    <td>City</td>
                    <td>Type</td>
                    <td>Contact Person</td>
                    <td>Contact Number</td>
                    <td></th>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->