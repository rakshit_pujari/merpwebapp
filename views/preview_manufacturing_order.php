<?php

function getIndianCurrency($number) {
    $decimal = round($number - ($no = floor($number)), 2) * 100;
    $hundred = null;
    $digits_length = strlen($no);
    $i = 0;
    $str = array();
    $words = array(0 => '', 1 => 'one', 2 => 'two',
        3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
        7 => 'seven', 8 => 'eight', 9 => 'nine',
        10 => 'ten', 11 => 'eleven', 12 => 'twelve',
        13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
        16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
        19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
        40 => 'forty', 50 => 'fifty', 60 => 'sixty',
        70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
    $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
    while ($i < $digits_length) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number] . ' ' . $digits[$counter] . $plural . ' ' . $hundred : $words[floor($number / 10) * 10] . ' ' . $words[$number % 10] . ' ' . $digits[$counter] . $plural . ' ' . $hundred;
        } else
            $str[] = null;
    }
    $Rupees = implode('', array_reverse($str));
    //$paise = ($decimal) ? " and " . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    $paise = '';
    if ($decimal < 100) {
        $paise = ($decimal) ? " and " . ($words[((int) ($decimal / 10)) * 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    }
    return ($Rupees ? $Rupees . ' ' : '') . $paise;
}
?>

<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/ManufacturingOrder/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Manufacturing</span> - Manufacturing Order</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                    <!--<a href="#!/ManufacturingOrder/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>-->
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="fa fa-gavel position-left"></i>Manufacturing</li>
            <li><a href="#!/ManufacturingOrder/all"><i class="fa fa-gears position-left"></i>Manufacturing Order</a></li>
            <? if ($manufacturing_order['manufacturing_order_status'] == 'confirm' || $manufacturing_order['manufacturing_order_status'] == 'complete') { ?>
                <li class="active"><?php if (isset($manufacturing_order)) echo 'MFG' . str_pad($manufacturing_order['manufacturing_order_id'], 5, "0", STR_PAD_LEFT); ?></li>
            <? } else { ?>
                <li class="active"><?php if (isset($manufacturing_order)) echo 'DRAFT'; ?></li>
            <? } ?>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <a href="#!/ManufacturingOrder/all"><button class="btn btn-default">Back<i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
    <a href="#!/ManufacturingOrder/view/<? echo $manufacturing_order['manufacturing_order_status']; ?>/<? echo $manufacturing_order['manufacturing_order_id']; ?>">
    <button class="btn btn-default" <? if($manufacturing_order['manufacturing_order_status'] == 'complete') echo " disabled title = 'Cannot be edited because this document has been marked as complete'"; ?>>Edit <i class="icon-pencil3 position-right"></i></button></a>
    </a>
    <button onClick="window.print();" class="btn btn-primary">Print <i class="icon-arrow-right14 position-right"></i></button>
    <? if($manufacturing_order['manufacturing_order_status'] == 'confirm') { ?>
    <button type="button" class="btn bg-teal-400 btn-labeled btn-labeled-right" onclick="markManufacturingOrderAsComplete(<? echo $manufacturing_order['manufacturing_order_id']; ?>)"><b><i class="glyphicon glyphicon-ok"></i></b>Mark As Complete</button>
    <? } ?>
    <br/><br/><br/>
    <!-- Form validation -->
    <div class="panel-flat">
        <div class="panel panel-body col-md-offset-3"  id = "section-to-print" style="width: 210mm;min-height: 157mm;padding-left:40px;padding-right:40px;">

            <span id = "manufacturingOrderHeader">Manufacturing Order</span>
            <hr>

            <table class = "manufacturingOrderTable">
                <tr>
                    <td class = "logo_container">
                        <img class = "logo" src="<? echo $manufacturing_order['manufacturing_order_oc_logo_path']; ?>"></img>
                        <br/><br/>
                        <span class = "manufacturingOrderFieldValue"><? echo $manufacturing_order['manufacturing_order_oc_name']; ?></span><br/>
                        <span class = "manufacturingOrderField">
                            <? echo $manufacturing_order['manufacturing_order_oc_address']; ?><br/>
                            <? echo $manufacturing_order['manufacturing_order_oc_city_name']; ?><br/>
                            <? echo $manufacturing_order['manufacturing_order_oc_district'].', '.$manufacturing_order['manufacturing_order_oc_state']; ?><br/>
                            GSTIN: <? echo $manufacturing_order['manufacturing_order_oc_gstin']; ?><br/>
                            PAN: <? echo $manufacturing_order['manufacturing_order_oc_pan_number']; ?>

                        </span>
                    </td>
                    <td style="vertical-align: top">
                        <span class = "manufacturingOrderField">Manufacturing Order No.</span>												
                        <br/>
                        <span class = "manufacturingOrderFieldValue">
                            <? if ($manufacturing_order['manufacturing_order_status'] == 'confirm' || $manufacturing_order['manufacturing_order_status'] == 'complete') { ?>
                                <? if (isset($manufacturing_order)) echo 'MFG' . str_pad($manufacturing_order['manufacturing_order_id'], 5, "0", STR_PAD_LEFT); ?>
                            <? } else { ?>
                                <? if (isset($manufacturing_order)) echo 'DRAFT'; ?>
                            <? } ?>
                        </span>
                        <br/>
                        <span class = "manufacturingOrderField">Manufacturing Order date</span>												
                        <br/>
                        <span class = "manufacturingOrderFieldValue"><? echo $manufacturing_order['manufacturing_order_date']; ?></span>
                        <br/>
                        <span class = "manufacturingOrderField">Product</span>												
                        <br/>
                        <span class = "manufacturingOrderFieldValue"><? echo $manufacturing_order['manufacturing_order_linked_product_name']; ?></span>
                        <br/>
                        <span class = "manufacturingOrderField">Order Quantity</span>												
                        <br/>
                        <span class = "manufacturingOrderFieldValue"><? echo $manufacturing_order['manufacturing_order_product_quantity']. ' '. $manufacturing_order['manufacturing_order_uqc_text']; ?></span><br/>
                        <span class = "manufacturingOrderField">Batch Number</span>												
                        <br/>
                        <span class = "manufacturingOrderFieldValue"><? echo $manufacturing_order['manufacturing_order_batch_number']; ?></span>
                    </td>
                </tr>
            </table>
            <hr>
            <table class = "productList manufacturingOrderTable">
                <thead class = "grayTBack">
                    <tr>
                        <td width='25px'>Sr</td>
                        <td>Raw Material</td>
                        <td>Consumption Quantity</td>
                        <td>Note / Remarks</td>
                    </tr>
                </thead>
                <tbody>
                    <?
                    $i = 1;
                    foreach ($manufacturing_order_entries as $manufacturing_order_entry) {
                        ?>
                        <tr>
                            <td><? echo $i++; ?></td>

                            <td><? echo $manufacturing_order_entry['moe_linked_product_name']; ?></td>		


                            <td><? echo $manufacturing_order_entry['moe_linked_product_quantity']. ' '. $manufacturing_order_entry['moe_uqc_text'];; ?></td>
                            <td></td>						  
                        </tr>

                        <?php
                    }
                    ?>
                </tbody>
                <tfoot class = "grayTBack">
                    <tr style="height:20px">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>

            <table class = "manufacturingOrderTable">
                <tr>
                    <td colspan = "6" style = "text-align:center;">
                    <td colspan = "3" style = "text-align:center;"><br/><br/>Authorized Signatory<br/><br/><br/><br/><span style = "font-weight:800;"><? echo $manufacturing_order['manufacturing_order_oc_name']; ?></span></td>
                </tr>
            </table>
            <table>
                
                <? if (isset($manufacturing_order)) { ?>
                <div class="ribbon-container">
                    <? if ($manufacturing_order['manufacturing_order_status'] == 'complete') { ?>
                        <div class="ribbon bg-success-400">COMPLETE</div>
                    <? } else if ($manufacturing_order['manufacturing_order_status'] == 'confirm') { ?>
                        <div class="ribbon bg-blue">CONFIRM</div>
                    <? } else { ?>
                        <div class="ribbon bg-danger">DRAFT</div>
                    <? }
                    ?>
                </div>
                <? } ?>
            </table>
            <footer style = "position:absolute;bottom:0;right:0;padding-right:20px;padding-bottom:15px;font-size:11px;font-style:italic;">
                Powered By Quant ERP
            </footer>
        </div>

    </div>


    <style>
        @media print {
            body * {
                visibility: hidden;
            }
            #section-to-print, #section-to-print * {
                visibility: visible;
                overflow:hidden;
            }
            #section-to-print {
                position: absolute;
                left: 0;
                top: 0;
                size: auto;   /* auto is the initial value */
                margin: 0;
                border: initial;
                border-radius: initial;
                width: initial;
                min-height: initial;
                box-shadow: initial;
                background: initial;
                page-break-after: always;
            }
            .grayTBack {
                visibility: visible;
                background-color: #ccc!important;
                color: black;
                font-weight:700;
                -webkit-print-color-adjust: exact;
                border: solid #ccc!important;
            }
            
            .gray-border{
                border: 1px solid #ccc;
                overflow:visible!important;
                -webkit-print-color-adjust: exact;
            }
        }
        @page 
        {
            size:  auto;   /* auto is the initial value */
            margin: 0mm;  /* this affects the margin in the printer settings */
        }
        .termsText{
            font-size: 10px;
        }
        .productList{
            border: 1px solid #FFFFFF;
            text-align: center;
            font-size: 11px;
        }
        .termsText{
            font-size: 10px;
            min-height: 12em;
            border:none;
            resize: none;
            overflow:hidden;
            width:100px;
            width: 375px;
            text-align: justify;
        }
        .whiteTBack {
            background-color: #FFFFFF;
            color: black;
            font-weight:700;
        }
        .grayTBack {
            background-color: #ccc;
            color: black;
            font-weight:700;
        }
        .manufacturingOrderTable {
            font-size: 11px;
            width: 100%;
            max-width: 100%;
            table-layout: fixed;
            font-family:"arial";
            margin-bottom:5px; 
            padding:10px;
        }
        .gray-border{
            border: 1px solid #ccc;
            border-collapse: collapse;
        }

        .logo {
            max-width:40mm; max-height: 20mm;
            text-align: center;
        }
        .logo_container {
            /*text-align: center;*/
        }
        #manufacturingOrderHeader {
            font-size: 20px;
            font:"arial";
            width: 100%;
            text-align: center;
            font-family:"arial";
            font-weight: bold;
            display: block;
        }
        hr {
            border: 3px solid #000000;
            margin-top:3px;
        }
        .manufacturingOrderField {
            font-size: 11px;	
        }
        .manufacturingOrderFieldValue {
            font-weight: bold;
            font-size: 12px;
        }
        td {
            padding:3px;
        }
        hr {
            margin-bottom: 5px;
        }
    </style>