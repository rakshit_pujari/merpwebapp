

<!-- Page header -->
<div class="page-header">

    <div ng-controller="dataTableController" ng-init="load(1)"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Inventory</span> - Adjustment</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!Inventory/adjustment/view" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-resize-vertical text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Adjust Inventory</span></a>
                <a href="report/download/inventory_adjustment" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-home2 position-left"></i>Inventory</li>
            <li class="active"><i class="glyphicon glyphicon-resize-vertical position-left"></i>Adjustment</li>
        </ul>

    </div>
</div>
<!-- /page header -->



<!-- Content area -->
<div class="content">

    <!-- Invoice archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Inventory Adjustment</h6>
            <div class="heading-elements">
                <!--<ul class="icons-list">
        <li><a data-action="collapse"></a></li>
        <li><a data-action="reload"></a></li>
        <li><a data-action="close"></a></li>-->
                </ul>
            </div>
        </div>

        <table class="table masterDataTable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Product Name</th>
                    <th>Adjustment Type</th>
                    <th>Post Adjustment Qty</th>
                    <th>Post Adjustment Amount</th>
                    <th>Reason</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($inventory_adjustments as $inventory_adjustment): ?>
                    <tr>
                        <td>IAD<?php echo str_pad($inventory_adjustment['ia_id'], 5, "0", STR_PAD_LEFT); ?></td>
                        <td><?php echo explode(" ", $inventory_adjustment['ia_record_creation_time'])[0]; ?></td>
                        <td><a href="#!Inventory/adjustment/view/<?php echo $inventory_adjustment['ia_id']; ?>"><span><?php echo $inventory_adjustment['product_name']; ?></span></a>
                                <small class="display-block text-muted">Created by <?php echo $inventory_adjustment['employee_username']; ?> 
                                    on <?php echo $inventory_adjustment['ia_record_creation_time']; ?></small></td>
                        <td><?php echo ucfirst($inventory_adjustment['ia_type']); ?></td>
                        <td><?php if(isset($inventory_adjustment['ia_quantity'])) echo number_format($inventory_adjustment['ia_quantity']). ' '.$inventory_adjustment['uqc_text']; else echo '-'; ?></td>
                        <td><?php if(isset($inventory_adjustment['ia_value'])) echo '₹ '. number_format($inventory_adjustment['ia_value']); else echo '-'; ?></td>
                        <td><?php echo $inventory_adjustment['ia_reason']; ?></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li><a href="#!Inventory/adjustment/view/<?php echo $inventory_adjustment['ia_id']; ?>"><i class="icon-file-eye"></i></a></li>
                                <? 
                                if($this->session->userdata('access_controller')->is_access_granted('inventory_adjustment', 'delete')) { ?>
                                <li><a href='javascript: void(0)' onclick = 'showDeleteDialog("inventory_adjustment", <?php echo $inventory_adjustment['ia_id']; ?>)'><i class="glyphicon glyphicon-trash"></i></a></li>
                                <? } ?>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Product Name</th>
                    <th>Adjustment Type</th>
                    <th>Post Adjustment Qty</th>
                    <th>Post Adjustment Amount</th>
                    <th>Reason</th>
                    <th class="text-center"></th>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->