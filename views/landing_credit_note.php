

<!-- Page header -->
<div class="page-header">

    <div ng-controller="dataTableController" ng-init="load(6)"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Sales</span> - Credit Note</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!CreditNote/view/new" class="btn btn-link btn-float has-text"><i class="icon-credit-card text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Add Credit Note</span></a>
                <a href="report/download/credit_note" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-home2 position-left"></i>Sales</li>
            <li class="active"><i class="icon-city position-left"></i>Credit Note</li>
        </ul>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Credit Note archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Credit Note</h6>
            <div class="heading-elements">

            </div>
        </div>

        <table class="table masterDataTable">
            <thead>
                <tr>
                    <th>Credit Note ID</th>
                    <th>Company Name</th>
                    <th>Invoice Number</th>
                    <th>Invoice Amount</th>
                    <th>Status</th>
                    <th>Amount</th>
                    <th>Date</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($credit_notes as $credit_note): ?>
                    <tr>
                        <td>
                            <? if ($credit_note['cn_status'] == 'confirm') { ?>
                                CDN<?php echo str_pad($credit_note['cn_id'], 5, "0", STR_PAD_LEFT); ?>
                            <? } else { ?>
                                DRAFT
                            <? } ?>
                        </td>
                        <td>
                            <h6 class="no-margin">
                                 <? if ($credit_note['cn_status'] == 'confirm') { ?>
                                    <a href="#!CreditNote/preview/confirm/<? echo $credit_note['cn_id']; ?>">
                                    <? } else { ?>
                                        <a href="#!CreditNote/view/<? echo $credit_note['cn_status']; ?>/<? echo $credit_note['cn_id']; ?>">
                                        <? } ?><span><?php echo $credit_note['cn_linked_company_invoice_name']; ?></span></a>
                                <small class="display-block text-muted">Created by <?php echo $credit_note['employee_username']; ?> 
                                    on <?php echo $credit_note['cn_record_creation_time']; ?></small>
                            </h6>
                        </td>
                        <td><?php
                            if (isset($credit_note['cn_linked_invoice_id'])) {
                                $cn_linked_supplementary_invoice_id = $credit_note['cn_linked_invoice_id'];
                                $cn_linked_supplementary_invoice_id_text = 'INV' . str_pad($cn_linked_supplementary_invoice_id, 5, "0", STR_PAD_LEFT);
                            } else if (isset($credit_note['cn_linked_purchase_id'])) {
                                $cn_linked_supplementary_invoice_id = $credit_note['cn_linked_purchase_id'];
                                $cn_linked_supplementary_invoice_id_text = 'PUR' . str_pad($cn_linked_supplementary_invoice_id, 5, "0", STR_PAD_LEFT);
                            }

                            echo $cn_linked_supplementary_invoice_id_text;
                            ?></td>
                        <td><?php echo '₹ '.number_format($credit_note['invoice_amount'], 2); ?></td>
                        <td><?php
                            if ($credit_note['cn_status'] == 'confirm')
                                echo '<span class="label bg-blue"><span>' . $credit_note['cn_status'] . '</span></span>';
                            else
                                echo '<span class="label bg-danger"><span>' . $credit_note['cn_status'] . '</span></span>';
                            ?></td>
                        <td><?php echo '₹ '.number_format($credit_note['cn_amount'], 2); ?></td>
                        <td><?php echo explode(" ", $credit_note['cn_date'])[0]; ?></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li><? if ($credit_note['cn_status'] == 'confirm') { ?>
                                        <a href="#!CreditNote/preview/confirm/<? echo $credit_note['cn_id']; ?>">
                                        <? } else { ?>
                                            <a href="#!CreditNote/view/<? echo $credit_note['cn_status']; ?>/<? echo $credit_note['cn_id']; ?>">
                                            <? } ?>
                                            <i class="icon-file-eye"></i></a>
                                            <? if ($this->session->userdata('access_controller')->is_access_granted('credit_note', 'delete')
                                                    && $credit_note['cn_status'] == 'draft') { 
                                                ?>
                                                <li><a href='javascript: void(0)' onclick = 'showDeleteDialog("CreditNote", <?php echo $credit_note['cn_id']; ?>)'><i class="glyphicon glyphicon-trash"></i></a></li>
                                            <? } ?>
                                </li>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th>Credit Note ID</th>
                    <th>Company Name</th>
                    <th>Invoice Number</th>
                    <th>Invoice Amount</th>
                    <th>Status</th>
                    <th>Amount</th>
                    <th>Date</th>
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->