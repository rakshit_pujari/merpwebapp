

<!-- Page header -->
<div class="page-header">

    <div ng-controller="dataTableController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Master</span> - Charge</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!charge/view" class="btn btn-link btn-float has-text"><i class="icon-cash4 text-primary" style = "font-size:22px;color:#26A69A !important"></i><span>Add Charge</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-home2 position-left"></i>Master</li>
            <li class="active"><i class="icon-cash4 position-left"></i>Charge</li>
        </ul>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Charge</h6>
            <div class="heading-elements">
            </div>
        </div>

        <table class="table masterDataTable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Description</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($charges as $charge): ?>
                    <tr>
                        <td>CRG<?php echo str_pad($charge['charge_id'], 5, "0", STR_PAD_LEFT); ?></td>
                        <td>
                            <h6 class="no-margin">
                                <a href="#!charge/view/<?php echo $charge['charge_id']; ?>"><span><?php echo $charge['charge_name']; ?></span></a>
                                <small class="display-block text-muted">Created by <?php echo $charge['employee_username']; ?> 
                                    on <?php echo $charge['charge_record_creation_time']; ?></small>
                            </h6>
                        </td>
                        <td><?php echo $charge['charge_type']; ?></td>
                        <td><?php echo $charge['charge_description']; ?></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li><a href="#!charge/view/<?php echo $charge['charge_id']; ?>"><i class="icon-file-eye"></i></a></li>
                                <? 
                                if($this->session->userdata('access_controller')->is_access_granted('charge', 'delete')) { ?>
                                <li><a href='javascript: void(0)' onclick = 'showDeleteDialog("charge", <?php echo $charge['charge_id']; ?>)'><i class="glyphicon glyphicon-trash"></i></a></li>
                                <? } 
                                ?>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td>ID</td>
                    <td>Name</td>
                    <td>Type</td>
                    <td>Description</td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->