<?php

function getIndianCurrency($number) {
    $decimal = round($number - ($no = floor($number)), 2) * 100;
    $hundred = null;
    $digits_length = strlen($no);
    $i = 0;
    $str = array();
    $words = array(0 => '', 1 => 'one', 2 => 'two',
        3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
        7 => 'seven', 8 => 'eight', 9 => 'nine',
        10 => 'ten', 11 => 'eleven', 12 => 'twelve',
        13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
        16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
        19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
        40 => 'forty', 50 => 'fifty', 60 => 'sixty',
        70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
    $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
    while ($i < $digits_length) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number] . ' ' . $digits[$counter] . $plural . ' ' . $hundred : $words[floor($number / 10) * 10] . ' ' . $words[$number % 10] . ' ' . $digits[$counter] . $plural . ' ' . $hundred;
        } else
            $str[] = null;
    }
    $Rupees = implode('', array_reverse($str));
    //$paise = ($decimal) ? " and " . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    $paise = ($decimal) ? " and " . ($words[((int) ($decimal/10)) * 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    return ($Rupees ? $Rupees . ' ' : '') . $paise;
}
?>
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/Receipt/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Sales</span> - Receipt</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                    <!--<a href="#!/Receipt/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>-->
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!"><i class="icon-home2 position-left"></i>Sales</a></li>
            <li><a href="#!/Receipt/all"><i class="icon-city position-left"></i> Receipt</a></li>
            <li class="active">REC<?php if (isset($receipt)) echo str_pad($receipt['receipt_id'], 5, "0", STR_PAD_LEFT); ?></li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <a href="#!/Receipt/all"><button class="btn btn-default">Back<i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
    <a href="#!/Receipt/view/<?php if (isset($receipt)) echo $receipt['receipt_id']; ?>"><button class="btn btn-default">Edit <i class="glyphicon  glyphicon-pencil position-right"></i></button></a>
    <button onClick="window.print();" class="btn btn-primary">Print <i class="icon-arrow-right14 position-right"></i></button>

    <!-- Form validation -->
    <div class="panel-flat">



        <div class="panel panel-body col-md-offset-3"  id = "section-to-print" style="width: 210mm;min-height: 297mm;padding-left:40px;padding-right:40px;">

            <span id = "receiptHeader">Receipt Voucher</span>
            <hr>

            <table class = "receiptTable">
                <tr>
                    <td class = "logo_container" colspan = "2" rowspan = "2	">
                        <img class = "logo" src="<? echo $receipt['receipt_oc_logo_path']; ?>"></img>
                    </td>
                    <td>
                        <span class = "receiptField">Receipt date</span>												
                        <br/>
                        <span class = "receiptFieldValue"><? echo explode(" ", $receipt['receipt_date'])[0]; ?></span>
                    </td>
                    <td>
                        <span class = "receiptField">Payment Terms</span>												
                        <br/>
                        <span class = "receiptFieldValue"><? echo $receipt['receipt_linked_company_payment_term_text']; ?></span>											
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class = "receiptField">Receipt No.</span>												
                        <br/>
                        <span class = "receiptFieldValue">REC<? echo str_pad($receipt['receipt_id'], 5, "0", STR_PAD_LEFT); ?></span>
                    </td>
                    <td>
                        <span class = "receiptField">Place Of Supply</span>												
                        <br/>
                        <span class = "receiptFieldValue"><? echo $receipt['receipt_linked_company_gst_supply_state_id'].'-'.$receipt['receipt_place_of_supply']; ?></span>
                    </td>
                </tr>
                <tr>
                    <td colspan = "2" rowspan = "2	">
                        <span class = "receiptFieldValue"><? echo $receipt['receipt_oc_name']; ?></span><br/>
                        <span class = "receiptField">
                            <? echo $receipt['receipt_oc_address']; ?><br/>
                            GSTIN: <? echo $receipt['receipt_oc_gstin']; ?><br/>
                            PAN: <? echo $receipt['receipt_oc_pan_number']; ?>

                        </span>
                    </td>
                    <td>
                        <span class = "receiptField">Invoice Due Date.</span>												
                        <br/>
                        <span class = "receiptFieldValue"><? echo explode(" ", $receipt['receipt_linked_invoice_due_date'])[0]; ?></span>
                    </td>
                    <td>
                    </td>
                </tr>

            </table>
            <table class = "receiptTable gray-border">

                <tr>
                    <td>Receiver details (Billed to)</td>
                    <td>
                        <? if (isset($receipt['receipt_linked_invoice_id'])) { ?>
                            Consignee details (Shipped to)
                        <? } ?>	
                    </td>

                </tr>
                <tr>
                    <td>
                        Name:<span class = "receiptFieldValue"><? echo $receipt['receipt_linked_company_invoice_name']; ?></span><br/>
                        Address:<? echo $receipt['receipt_linked_company_billing_address']; ?><br/>
                        State:<? echo $receipt['receipt_linked_company_billing_state_name']; ?><br/>
                        Contact details:<? echo $receipt['receipt_linked_company_contact_number']; ?><br/>
                        GST#:<? echo $receipt['receipt_linked_company_gstin']; ?><br/>
                    </td>
                    <td>
                        <? if (isset($receipt['receipt_linked_invoice_id'])) { ?>
                            Name:<? echo $receipt['receipt_linked_company_invoice_name']; ?><br/>
                            Address:<? echo $receipt['receipt_linked_company_shipping_address']; ?><br/>
                            State:<? echo $receipt['receipt_linked_company_shipping_state_name']; ?><br/>
                            GST#:<? echo $receipt['receipt_linked_company_gstin']; ?><br/>
                        <? } ?>
                    </td>
                </tr>
            </table>

            <hr>
            <table class = "productList receiptTable">
                <thead class = "grayTBack">
                    <tr>
                        <td>Sr</td>
                        <td>Invoice Number</td>
                        <td>Invoice Date</td>
                        <td>Invoice Amount</td>
                        <td>Amount Received</td>
                        <td>Payment Mode</td>
                        <td>Transaction Reference</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>INV<?php echo str_pad($receipt['receipt_linked_invoice_id'], 5, "0", STR_PAD_LEFT); ?></td>
                        <td><? echo explode(" ", $receipt['receipt_linked_invoice_date'])[0]; ?></td>
                        <td>₹ <? echo number_format($receipt['receipt_linked_invoice_amount'], 2); ?></td>
                        <td>₹ <? echo number_format($receipt['receipt_amount'], 2); ?></td>
                        <td><? echo ucwords($receipt['receipt_payment_mode']); ?></td>
                        <td><? echo $receipt['receipt_transaction_reference']; ?></td>
                    </tr>
                </tbody>
                <tfoot class = "grayTBack">
                    <tr style="height:20px">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>

            <table class = "receiptTable">
                <tr>
                    <td colspan = "6" rowspan = "2"></td>
                    <td colspan = "2"></td>
                    <td> </td>
                </tr>
                <tr>
                    <td colspan = "2"></td>
                    <td> </td>
                </tr>




                <tr>
                    <td colspan = "6"></td>
                    <td colspan = "2"></td>
                    <td>
                    </td>
                </tr>

                <tr>
                    <td colspan = "6" style = "text-align:left;">
                        Total Receipt Amount (in words)
                        <br>
                        <span style = "font-weight:500;">Rupees</span> <span style = "font-weight:800;"><? echo ucwords(getIndianCurrency($receipt['receipt_amount'])); ?> </span>Only
                    </td>
                    <td colspan = "2"></td>
                    <td> </td>
                </tr>

                <tr>
                    <td colspan = "6" style = "text-align:left;">
                        
                    </td>
                    <td colspan = "2">Authorized Signatory<br/><br/><br/><span style = "font-weight:800;"><? echo $receipt['receipt_oc_name']; ?></span></td>
                    <td> </td>
                </tr>


                <table>
                    <span class = "termsText">Added to :<? echo $receipt['receipt_debit_coa_name']; ?><br/>
                    </span>
                    <!--<div class="ribbon-container">
                            <div class="ribbon bg-success-400">PAID</div>
                    </div>-->
            <footer style = "position:absolute;bottom:0;right:0;padding-right:20px;padding-bottom:15px;font-size:11px;font-style:italic;">
                Powered By Quant ERP
            </footer>
                    </div>

                    </div>


                    <style>
                        @media print {
                            body * {
                                visibility: hidden;
                            }
                            #section-to-print, #section-to-print * {
                                visibility: visible;
                                overflow:hidden;
                            }
                            #section-to-print {
                                position: absolute;
                                left: 0;
                                top: 0;
                                size: auto;   /* auto is the initial value */
                                margin: 0;
                                border: initial;
                                border-radius: initial;
                                width: 100%;
                                min-height: initial;
                                box-shadow: initial;
                                background: initial;
                                page-break-after: always;
                            }
                            .grayTBack {
                                visibility: visible;
                                background-color: #ccc!important;
                                color: black;
                                font-weight:700;
                                -webkit-print-color-adjust: exact;
                                border: solid #ccc!important;
                            }

                            .gray-border{
                                border: 1px solid #ccc;
                                overflow:visible!important;
                                -webkit-print-color-adjust: exact;
                            }
                        }
                        @page 
                        {
                            size:  auto;   /* auto is the initial value */
                            margin: 0mm;  /* this affects the margin in the printer settings */
                        }

                        .termsText{
                            font-size: 10px;
                        }
                        .productList{
                            border: 1px solid #FFFFFF;
                            text-align: center;
                            font-size: 11px;
                        }
                        .whiteTBack {
                            background-color: #FFFFFF;
                            color: black;
                            font-weight:700;
                        }
                        .grayTBack {
                            background-color: #ccc;
                            color: black;
                            font-weight:700;
                        }
                        .receiptTable {
                            font-size: 11px;
                            width: 100%;
                            max-width: 100%;
                            table-layout: fixed;
                            font-family:"arial";
                            margin-bottom:5px; 
                            padding:10px;
                        }
                        .gray-border{
                            border: 1px solid #ccc;
                            border-collapse: collapse;
                        }

                        .logo {
                            max-width:40mm; max-height: 20mm;
                            text-align: center;
                        }
                        .logo_container {
                            /*text-align: center;*/
                        }
                        #receiptHeader {
                            font-size: 20px;
                            font:"arial";
                            width:100%;
                            text-align: center;
                            font-family:"arial";
                            font-weight: bold;
                            display: block;
                        }
                        hr {
                            border: 3px solid #000000;
                            margin-top:3px;
                        }
                        .receiptField {
                            font-size: 11px;	
                        }
                        .receiptFieldValue {
                            font-weight: bold;
                            font-size: 12px;
                        }
                        td {
                            padding:3px;
                        }
                        hr {
                            margin-bottom: 5px;
                        }
                    </style>