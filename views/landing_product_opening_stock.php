

<!-- Page header -->
<div class="page-header">

    <div ng-controller="pobController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Chart Of Accounts</span> - Opening Stock</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <!--<a href="#!COA/view" class="btn btn-link btn-float has-text"><i class="icon-book3 text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Add New Account</span></a>
                <a href="#!COA/" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>-->
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-calculator2 position-left"></i>Accounting</li>
            <li><a href="#!/COA/all"><i class="icon-book3 position-left"></i> Chart Of Accounts</a></li>
            <li class="active">Opening Stock</li>
        </ul>

    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">

    <!-- Invoice archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Products</h6>
            <div class="heading-elements">
            </div>
        </div>
        
        <div class="panel-body">
            <?php
            $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id'=>'merpForm');
            echo form_open('web/product/opening/save', $attributes);
            ?>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Opening Balance Date</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="1"
                                   value = "<?php if (isset($oc_data)) echo explode(" ", $oc_data['oc_opening_balance_date'])[0]; ?>" readonly>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <div class="text-right">
                            <a href="#!/COA/opening"><button class="btn btn-default">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                            <button onclick="submitForm('Opening Stock', 'product/opening');" class="btn btn-primary" >Save <i class="icon-arrow-right14 position-right"></i></button>
                        </div>
                    </div>
                </div>
        

        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Product Name</th>
                    <th>Product For</th>
                    <th>Opening Stock Quantity</th>
                    <th>Opening Stock Value</th>
                    <th>Average Rate Per Unit</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($products as $product): ?>
                    <tr>
                        <td>PRD<?php echo str_pad($product['product_id'], 5, "0", STR_PAD_LEFT); ?></td>
                        <td><a href="#!product/view/<?php echo $product['product_id']; ?>">
                            <span><?php echo $product['product_name']; ?></span>
                            </a>
                        </td>
                        <td><?php echo $product['transaction_type']; ?></td>
                        <td>
                            <div class="has-feedback has-feedback-right">
                                <input min ="0" name = "pob[<? echo $product['product_id'] ?>][0]" class = "form-control" 
                                       type = "number" onchange ="calculateProductBalanceFields(this, <? echo $product['product_id'] ?>);"
                                       value = "<?php echo $product['pob_opening_balance']; ?>" required></input>
                                <div class="form-control-feedback">
                                    <span style = "padding-right:5px;"><? echo $product['uqc_text']; ?></span>
                                </div>
                             
                            </div>
                        </td>
                        <td>
                            <div class="has-feedback has-feedback-left">
                            <input min ="0" step = 0.01  name = "pob[<? echo $product['product_id'] ?>][1]" class = "form-control" 
                                   type = "number" onchange ="calculateProductBalanceFields(this, <? echo $product['product_id'] ?>);"
                                   value = "<?php echo $product['pob_opening_balance'] * $product['pob_unit_rate']; ?>" required></input>
                               <div class="form-control-feedback">
                                    <span style = "padding-right:5px;">₹ </span>
                                </div>
                             
                            </div>
                        </td>
                        <td>
                            <div class="has-feedback has-feedback-right">
                                <input min ="0" step = 0.01 name = "pob[<? echo $product['product_id'] ?>][2]" class = "form-control" 
                                       type = "number" onchange ="calculateProductBalanceFields(this, <? echo $product['product_id'] ?>);" 
                                       value = "<?php echo $product['pob_unit_rate']; ?>"></input>
                                <div class="form-control-feedback">
                                    <span style = "font-size:10px;">₹/<? echo $product['uqc_text']; ?> </span>
                                </div>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
        <div class="text-right">
            <a href="#!/COA/opening"><button class="btn btn-default">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
            <button onclick="submitForm('Opening Stock', 'product/opening');" class="btn btn-primary" >Save <i class="icon-arrow-right14 position-right"></i></button>
        </div>
        </form>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->