

<!-- Page header -->
<div class="page-header">

    <div ng-controller="dataTableController" ng-init="load(6)"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Purchase</span> - Advance Payment</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!AdvancePayment/view" class="btn btn-link btn-float has-text"><i class="icon-credit-card2 text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Add Advance Payment</span></a>
                <a href="report/download/advance_payment" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-home2 position-left"></i>Purchase</li>
            <li class="active"><i class="icon-city position-left"></i>Advance Payment</li>
        </ul>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Payment archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Advance Payment</h6>
            <div class="heading-elements">
                </ul>
            </div>
        </div>

        <table class="table masterDataTable">
            <thead>
                <tr>
                    <th>Advance Payment no.</th>
                    <th>Customer Name</th>
                    <th>Amount</th>
                    <th>Linked Purchases</th>
                    <th>Balance</th>
                    <th>Payment Mode</th>
                    <th>Date</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($advance_payments as $advance_payment): ?>
                    <tr>
                        <td>ADP<?php echo str_pad($advance_payment['advance_payment_id'], 5, "0", STR_PAD_LEFT); ?></td>
                        <td>
                            <h6 class="no-margin">
                                <a href="#!AdvancePayment/preview/<?php echo $advance_payment['advance_payment_id']; ?>"><span><?php echo $advance_payment['advance_payment_linked_company_display_name']; ?></span></a>
                                <small class="display-block text-muted">Created by <?php echo $advance_payment['employee_username']; ?> 
                                    on <?php echo $advance_payment['advance_payment_record_creation_time']; ?></small>
                            </h6>
                        </td>
                        <td><? echo '₹ '.number_format($advance_payment['advance_payment_amount'], 2) ?></td>
                        <td><?
                            $net_utilized_amount = 0;
                            foreach ($advance_payment['linked_purchases'] as $linked_purchase) {
                                if($linked_purchase['pap_allocated_amount'] < 0){
                                    echo '(Less) ';
                                }
                                if(!empty($linked_purchase['pap_purchase_id'])){
                                ?>
                            <a href="#!purchase/preview/confirm/<? echo $linked_purchase['pap_purchase_id']; ?>">
                                <?
                                echo '<span>PUR'.str_pad($linked_purchase['pap_purchase_id'], 5, "0", STR_PAD_LEFT).'</span>';
                                ?></a><?
                                }
                                else {
                                    ?>
                            <a href="#!CreditNote/preview/confirm/<? echo $linked_purchase['pap_debit_note_id']; ?>">
                                <?
                                echo '<span>DBN'.str_pad($linked_purchase['pap_debit_note_id'], 5, "0", STR_PAD_LEFT).'</span>';
                                ?></a><?
                                }
                                echo ' - ';
                                echo '₹ '.number_format(abs($linked_purchase['pap_allocated_amount']), 2).'<br/>';
                                
                                $net_utilized_amount+=$linked_purchase['pap_allocated_amount'];
                            }
                            ?></td>
                        <td><?
                            if($net_utilized_amount == $advance_payment['advance_payment_amount']){
                                echo '<span class="label bg-danger"><span>'.'₹ '.number_format($advance_payment['balance_advance_payment_amount'], 2).'</span></span>'; 
                            } else {
                                echo '<span class="label bg-success"><span>'.'₹ '.number_format($advance_payment['balance_advance_payment_amount'], 2).'</span></span>'; 
                            }
                        ?></td>
                        <td><?php echo ucfirst($advance_payment['advance_payment_payment_mode']); ?></td>
                        <td><?php echo explode(" ", $advance_payment['advance_payment_date'])[0]; ?></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li><a href="#!AdvancePayment/preview/<?php echo $advance_payment['advance_payment_id']; ?>"><i class="icon-file-eye"></i></a></li>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td>Advance Payment no.</td>
                    <td>Customer Name</td>
                    <th>Amount</th>
                    <th>Linked Purchases</th>
                    <td>Balance</td>
                    <th>Payment Mode</th>
                    <td>Date</td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->