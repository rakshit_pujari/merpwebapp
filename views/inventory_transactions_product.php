

<!-- Page header -->
<div class="page-header">

    <div ng-controller="dataTableController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Inventory</span> - Product Transactions</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="report/download/inventory_overview/<? echo $product['product_id']; ?>" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!/inventory/overview"><i class="icon-stack2 position-left"></i> Inventory</a></li>
            <li class="active"><i class="icon-city position-left"></i>Product Transactions</li>
        </ul>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Invoice archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Inventory Valuation for <? echo $product['product_name']; ?></h6>
            <div class="heading-elements">
                </ul>
            </div>
        </div>

        <table class="table">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Transaction Details</th>
                    <th>Quantity</th>
                    <th>Unit Cost</th>
                    <th>FIFO Reference</th>
                    <th>Stock On Hand</th>
                    <th>Inventory Asset Value</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($inventory_products as $inventory_product):
                    ?>
                    <tr>    
                        <td><?php
                            echo date('jS F y', strtotime($inventory_product['ie_it_entry_datetime']));
                            //$inventory_product['ie_it_entry_datetime'];
                            ?></td>
                        <td><?php
                            $document_id = $inventory_product['ie_it_document_id_text'];
                            if (strpos($inventory_product['ie_it_document_id_text'], 'INV') !== false) {
                                echo 'Tax Invoice - <a href = "master#!/invoice/preview/confirm/' . substr($document_id, 3) . '">' . $inventory_product['ie_it_document_id_text'] . '</a>';
                            } else if (strpos($inventory_product['ie_it_document_id_text'], 'CDN') !== false) {
                                echo 'Credit Note - <a href = "master#!/CreditNote/preview/confirm/' . substr($document_id, 3) . '">' . $inventory_product['ie_it_document_id_text'] . '</a>';
                            } else if (strpos($inventory_product['ie_it_document_id_text'], 'PUR') !== false) {
                                echo 'Purchase - <a href = "master#!/purchase/preview/confirm/' . substr($document_id, 3) . '">' . $inventory_product['ie_it_document_id_text'] . '</a>';
                            } else if (strpos($inventory_product['ie_it_document_id_text'], 'DBN') !== false) {
                                echo 'Debit Note - <a href = "master#!/DebitNote/preview/confirm/' . substr($document_id, 3) . '">' . $inventory_product['ie_it_document_id_text'] . '</a>';
                            } else if (strpos($inventory_product['ie_it_document_id_text'], 'IAD') !== false) {
                                echo 'Inventory Adjustment - <a href = "master#!/Inventory/adjustment/view/' . substr($document_id, 3) . '">' . $inventory_product['ie_it_document_id_text'] . '</a>';
                            }  else if (strpos($inventory_product['ie_it_document_id_text'], 'MFG') !== false) {
                                echo 'Manufacturing Order - <a href = "master#!/ManufacturingOrder/preview/complete/' . substr($document_id, 3) . '">' . $inventory_product['ie_it_document_id_text'] . '</a>';
                            } else {
                                echo '<span style="color:#4CAF50">***' . $inventory_product['ie_it_document_id_text'] . '***</span>';
                            }
                            ?></td> 
                        <td><?php
                            if ($inventory_product['ie_it_product_quantity'] < 0) {
                                echo '<span class="label bg-danger">';
                            } else {
                                echo '<span class="label bg-success">';
                            }
                            echo $inventory_product['ie_it_product_quantity']. ' '. $inventory_product['uqc_text'];
                            ?></span></td>
                        <td>₹ <?php echo number_format($inventory_product['ie_it_product_rate'], 2); ?></td>
                        <td><?php echo $inventory_product['ie_fifo_reference']; ?></td>
                        <td><?php
                            if ($inventory_product['ie_stock_on_hand'] < 0) {
                                echo '<span style="color:#FF0000">';
                            }
                            echo number_format($inventory_product['ie_stock_on_hand'], 2). ' '. $inventory_product['uqc_text'];
                            ?></td>
                        <td>₹ <?php
                            if ($inventory_product['ie_inventory_asset_value'] < 0) {
                                echo '<span style="color:#FF0000">';
                            }
                            echo number_format($inventory_product['ie_inventory_asset_value'], 2);
                            ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th>Date</th>
                    <th>Transaction Details</th>
                    <th>Quantity</th>
                    <th>Unit Cost</th>
                    <th>FIFO Reference</th>
                    <th>Stock On Hand</th>
                    <th>Inventory Asset Value</th>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->