
<div ng-controller="advancePaymentController" ng-init="load()"></div>
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/AdvancePayment/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Purchase</span> - Advance Payment</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!/AdvancePayment/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!"><i class="icon-home2 position-left"></i>Purchase</a></li>
            <li><a href="#!/AdvancePayment/all"><i class="icon-city position-left"></i>Advance Payment</a></li>
            <li class="active"><?php
                if (isset($advance_payment))
                    echo 'ADP' . str_pad($advance_payment['advance_payment_id'], 5, "0", STR_PAD_LEFT);
                else
                    echo "New";
                ?></li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Form validation -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Payment</h5>

        </div>


        <div class="panel-body">
            <?php
            $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id' => 'merpDocForm');
            if (isset($advance_payment))
                echo form_open('web/AdvancePayment/save/' . $advance_payment['advance_payment_id'], $attributes);
            else
                echo form_open('web/AdvancePayment/save', $attributes);
            ?>
            <p class="content-group-lg">Payment Voucher is a confirmation document recording the payment made to vendors towards supplies received. Add all payments here to ensure accurate payment status.</p>

            <fieldset class="content-group">
                <legend class="text-bold">Vendor Details</legend>
                <div class="col-lg-12">

                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Vendor Name <span class="text-danger">*</span></label>
                        <div class="col-lg-9">

                            <?
                            if (isset($advance_payment)) {
                            ?>

                            <input class="form-control" type="text"
                                   value = "<? echo $advance_payment['advance_payment_linked_company_invoice_name']; ?>" readonly>
                            <input class="form-control" type="text" name = "advance_payment_linked_company_id"
                                   value = "<? echo $advance_payment['advance_payment_linked_company_id']; ?>" readonly style = "display: none">
                            <? } else {
                            ?>

                            <select class="select2 form-control" name = "advance_payment_linked_company_id" onchange = "updateAdvancePaymentCompanyId();">
                                <option value = "">Choose an company</option>
                                <? foreach ($vendors as $vendor) {
                                ?>
                                <option value = "<? echo $vendor['company_id'] ?>">
                                    <? echo $vendor['company_display_name'] ?></option>
                                <?
                                }
                                ?>

                                <? }
                                ?>

                            </select>													
                        </div>
                    </div>
                    <div class="form-group col-lg-6 has-feedback has-feedback-left">
                        <label class="control-label col-lg-3">Company ID</label>
                        <div class="col-lg-9">
                            <input class="form-control" style = "font-size:12px;padding-left:40px;" id = "advance_payment_linked_company_id_display"  type="text" readonly
                                   value = "<?php if (isset($advance_payment)) echo $advance_payment['advance_payment_linked_company_id']; ?>">
                            <div class="form-control-feedback" style = "left:10px">
                                <span style = "font-size:12px;padding-left:7px;"> COMP</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <!--<div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Document Date</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name="advance_payment_document_date" id = "datePicker"
                                   value = "<?php
                                if (isset($advance_payment))
                                    echo explode(" ", $advance_payment['advance_payment_document_date'])[0];
                                //else
                                  //  echo date('Y-m-d');
                                ?>">
                        </div>
                    </div>-->		
                </div>
                <div class="col-lg-12">
	
                    <div class="form-group col-lg-6 has-feedback has-feedback-left">
                        <label class="control-label col-lg-3">Advance Paid <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" id = "" tabindex="2" required name="advance_payment_amount" type="number" placeholder = "Should be less that or equal to balance amount" 
                                   <?
                                   if(isset($advance_payment)){
                                   ?>
                                   min = <? echo $advance_payment['advance_payment_total_allocated_amount']; ?>
                                   <?
                                   }
                                   ?>
                                    value = "<?php if (isset($advance_payment)) echo $advance_payment['advance_payment_amount']; ?>" required>
                            <div class="form-control-feedback" style = "left:10px">
                                <span style = "font-size:16px;"> ₹</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Payment Date</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name="advance_payment_date" id = "datePicker"
                                   value = "<?php
                                if (isset($advance_payment))
                                    echo explode(" ", $advance_payment['advance_payment_date'])[0];
                                else
                                    echo date('Y-m-d');
                                ?>">
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Pay From <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="select2 form-control" name = "advance_payment_credit_coa_id" required>
                                <option value = "">Choose an account</option>
                                <? foreach ($chart_of_accounts as $chart_of_account) {
                                ?> 
                                <option value = "<? echo $chart_of_account['coa_id'] ?>" <?php
                                   if (isset($advance_payment)){
                                       if ($advance_payment['advance_payment_credit_coa_id'] == $chart_of_account[
                                               'coa_id']) {
                                           echo "selected";
                                       }
                                   } else if (isset($advance_payment_setting)){
                                       if ($advance_payment_setting['credit_account_coa_id'] == $chart_of_account[
                                               'coa_id']) {
                                           echo "selected";
                                       }
                                   }
                                ?>><? echo $chart_of_account['coa_account_name'] ?></option>
                                <?
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Transaction Reference #</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name = "advance_payment_transaction_reference" type="text"
                                   value = "<?php if (isset($advance_payment)) echo $advance_payment['advance_payment_transaction_reference']; ?>">
                        </div>
                    </div>	
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Payment Mode <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="form-control select2" id = "advance_payment_payment_mode" name="advance_payment_payment_mode" tabindex="3" required>
                                <option value = "">Choose a payment mode</option>
                                <option value = "cash" <?php
                                if (isset($advance_payment))
                                    if ($advance_payment['advance_payment_payment_mode'] == "cash") {
                                        echo " selected";
                                    };
                                ?>>Cash</option>
                                <option value = "cheque" <?php
                                if (isset($advance_payment))
                                    if ($advance_payment['advance_payment_payment_mode'] == "cheque"
                                    ) {
                                        echo " selected";
                                    };
                                ?>>Cheque</option>
                                <option value = "bank transfer" <?php
                                if (isset($advance_payment))
                                    if ($advance_payment['advance_payment_payment_mode'] == "bank transfer") {
                                        echo " selected";
                                    };
                                ?>>Bank Transfer</option>
                                <option value = "bank deposit" <?php
                                        if (isset($advance_payment))
                                            if ($advance_payment['advance_payment_payment_mode'] == "bank deposit") {
                                                echo " selected";
                                            };
                                        ?>>Bank Deposit</option>
                                <option value = "demand draft" <?php
                                if (isset($advance_payment))
                                    if ($advance_payment['advance_payment_payment_mode'] == "demand draft") {
                                        echo " selected";
                                    };
                                ?>>Demand Draft</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Notes</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="2" name = "advance_payment_notes" type="text"
                                   value = "<?php if (isset($advance_payment)) echo $advance_payment['advance_payment_notes']; ?>">
                        </div>
                    </div>	
                </div>


            </fieldset>

            <div class="text-right">

                <a href="#!/AdvancePayment/all"><button class="btn btn-default">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                <? if($this->session->userdata('access_controller')->is_access_granted('payment', 'save')) { ?>
                <button onclick="submitDocForm('Advance_Payment', 'previewConfirm')" id ="buttonSubmitPayment" class="btn btn-primary">Confirm <i class="icon-arrow-right14 position-right"></i></button>
                <? } ?>
            </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->