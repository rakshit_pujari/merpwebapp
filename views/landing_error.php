

<!-- Page header -->
<div class="page-header">

    <div ng-controller="dataTableController" ng-init="load(0)"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold"></span>Error</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">

            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li class="active"><i class="fa fa-bank position-left"></i>Error</li>
        </ul>

    </div>
</div>
<!-- /page header -->



<!-- Content area -->
<div class="content">

    <!-- Invoice archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Error</h6>
            <div class="heading-elements">
            </div>
        </div>

        <table class="table masterDataTable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Error Text</th>
                    <th>Error Type</th>
                    <th>URL</th>
                    <th>Payload</th>
                    <th>User</th>
                    <th>Time</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($errors as $error): ?>
                    <tr>
                        <td>ERR<?php echo str_pad($error['error_id'], 5, "0", STR_PAD_LEFT); ?></td>
                        <td><?php echo $error['error_text']; ?></td>
                        <td><?php echo $error['error_type']; ?></td>
                        <td><?php echo $error['error_url']; ?></td>
                        <td><?php echo $error['error_payload']; ?></td>
                        <td><?php echo $error['employee_name']; ?></td>
                        <td><?php echo $error['error_record_creation_time']; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Error Text</th>
                    <th>Error Type</th>
                    <th>URL</th>
                    <th>Payload</th>
                    <th>User</th>
                    <th>Time</th>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->