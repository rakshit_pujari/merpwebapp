

<!-- Page header -->
<div class="page-header">

    <div ng-controller="dataTableController" ng-init="load(6)"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Sales</span> - Advance Receipt</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!AdvanceReceipt/view" class="btn btn-link btn-float has-text"><i class="icon-credit-card2 text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Add Advance Receipt</span></a>
                <a href="report/download/advance_receipt" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-home2 position-left"></i>Sales</li>
            <li class="active"><i class="icon-city position-left"></i>Advance Receipt</li>
        </ul>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Receipt archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Advance Receipt</h6>
            <div class="heading-elements">
                </ul>
            </div>
        </div>

        <table class="table masterDataTable">
            <thead>
                <tr>
                    <th>Advance Receipt no.</th>
                    <th>Customer Name</th>
                    <th>Amount</th>
                    <th>Linked Invoices</th>
                    <th>Balance</th>
                    <th>Receipt Mode</th>
                    <th>Date</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($advance_receipts as $advance_receipt): ?>
                    <tr>
                        <td>ADR<?php echo str_pad($advance_receipt['advance_receipt_id'], 5, "0", STR_PAD_LEFT); ?></td>
                        <td>
                            <h6 class="no-margin">
                                <a href="#!AdvanceReceipt/preview/<?php echo $advance_receipt['advance_receipt_id']; ?>"><span><?php echo $advance_receipt['advance_receipt_linked_company_display_name']; ?></span></a>
                                <small class="display-block text-muted">Created by <?php echo $advance_receipt['employee_username']; ?> 
                                    on <?php echo $advance_receipt['advance_receipt_record_creation_time']; ?></small>
                            </h6>
                        </td>
                        <td><? echo '₹ '.number_format($advance_receipt['advance_receipt_amount'], 2) ?></td>
                        <td><?
                            $net_utilized_amount = 0;
                            foreach ($advance_receipt['linked_invoices'] as $linked_invoice) {
                                if($linked_invoice['iar_allocated_amount'] < 0){
                                    echo '(Less) ';
                                }
                                if(!empty($linked_invoice['iar_invoice_id'])){
                                ?>
                            <a href="#!invoice/preview/confirm/<? echo $linked_invoice['iar_invoice_id']; ?>">
                                <?
                                echo '<span>INV'.str_pad($linked_invoice['iar_invoice_id'], 5, "0", STR_PAD_LEFT).'</span>';
                                ?></a><?
                                }
                                else {
                                    ?>
                            <a href="#!CreditNote/preview/confirm/<? echo $linked_invoice['iar_credit_note_id']; ?>">
                                <?
                                echo '<span>CDN'.str_pad($linked_invoice['iar_credit_note_id'], 5, "0", STR_PAD_LEFT).'</span>';
                                ?></a><?
                                }
                                echo ' - ';
                                echo '₹ '.number_format(abs($linked_invoice['iar_allocated_amount']), 2).'<br/>';
                                
                                $net_utilized_amount+=$linked_invoice['iar_allocated_amount'];
                            }
                            ?></td>
                        <td><?
                            if($net_utilized_amount == $advance_receipt['advance_receipt_amount']){
                                echo '<span class="label bg-danger"><span>'.'₹ '.number_format($advance_receipt['balance_advance_receipt_amount'], 2).'</span></span>'; 
                            } else {
                                echo '<span class="label bg-success"><span>'.'₹ '.number_format($advance_receipt['balance_advance_receipt_amount'], 2).'</span></span>'; 
                            }
                        ?></td>
                        <td><?php echo ucfirst($advance_receipt['advance_receipt_receipt_mode']); ?></td>
                        <td><?php echo explode(" ", $advance_receipt['advance_receipt_date'])[0]; ?></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li><a href="#!AdvanceReceipt/preview/<?php echo $advance_receipt['advance_receipt_id']; ?>"><i class="icon-file-eye"></i></a></li>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td>Advance Receipt no.</td>
                    <td>Customer Name</td>
                    <th>Amount</th>
                    <th>Linked Invoices</th>
                    <td>Balance</td>
                    <th>Receipt Mode</th>
                    <td>Date</td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->