

<!-- Page header -->
<div class="page-header">

    <div ng-controller="prObController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Chart Of Accounts</span> - Opening <? echo ucwords($title); ?></h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <!--<a href="#!COA/view" class="btn btn-link btn-float has-text"><i class="icon-book3 text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Add New Account</span></a>
                <a href="#!COA/" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>-->
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-calculator2 position-left"></i>Accounting</li>
            <li><a href="#!COA/all"><i class="icon-book3 position-left"></i>Chart Of Accounts</a></li>
            <li class="active">Opening <? echo ucwords($title); ?></li>
        </ul>

    </div>
</div>
<!-- /page header -->



<!-- Content area -->
<div class="content">

    <!-- Invoice archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title"><? echo ucwords($title); ?></h6>
            <div class="heading-elements">
            </div>
        </div>

        <div class="panel-body">
            <?php
            $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id'=>'merpForm');
            echo form_open('web/OC/opening/save', $attributes);
            ?>
            <div class="col-lg-12">
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-3">Opening Balance Date</label>
                    <div class="col-lg-9">
                        <input class="form-control"
                               value = "<?php if (isset($oc_data)) echo explode(" ", $oc_data['oc_opening_balance_date'])[0]; ?>" readonly>
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <div class="text-right">
                        <a href="#!/COA/opening"><button class="btn btn-default">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                        <button onclick="submitForm('Opening Balance', 'COA/opening');" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>


            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Account Type</th>
                        <th>Debit</th>
                        <th>Credit</th>
                    </tr>
                </thead>
                <tbody>
                    <?
                    foreach ($accounts as $account): ?>
                       <tr>
                           <td><? echo $account['pr_entity_prefix'].str_pad($account['pr_entity_id'], 5, "0", STR_PAD_LEFT); ?></td>
                           <td><a href="#!<? echo $account['pr_entity_type']; ?>/view/<?php echo $account['pr_entity_id']; ?>">
                                   <span><? echo $account['pr_entity_name']; ?></span>
                               </a>
                           </td>
                           <td><? echo ucwords($account['pr_entity_type_display']); ?></td>
                           <td>
                               <input min ="0" name = "pr_ob[<? echo $account['pr_entity_class_id'] ?>][<? echo $account['pr_entity_id'] ?>][0]" class = "form-control" 
                                      type = "number" onchange="invalidatePRField(this, <? echo $account['pr_entity_class_id'] ?>, <? echo $account['pr_entity_id'] ?>, 0);"
                                      value = "<?
                                      if (isset($account))
                                          if ($account['pr_opening_balance_transaction_type'] == 'debit')
                                              if ($account['pr_opening_balance'] > 0)                                               
                                                   echo $account['pr_opening_balance'];
                                      ?>" 
                                      <?
                                      if (isset($account))
                                          if ($account['pr_opening_balance_transaction_type'] != 'debit')
                                              if ($account['pr_opening_balance'] > 0)   
                                                   echo "disabled";
                                      ?>></input> 
                           </td>
                           <td>
                               <input min ="0" name = "pr_ob[<? echo $account['pr_entity_class_id'] ?>][<? echo $account['pr_entity_id'] ?>][1]" class = "form-control" 
                                      type = "number" onchange="invalidatePRField(this, <? echo $account['pr_entity_class_id'] ?>, <? echo $account['pr_entity_id'] ?>, 1);"
                                      value = "<?
                                      if (isset($account))
                                          if ($account['pr_opening_balance_transaction_type'] == 'credit')
                                              if ($account['pr_opening_balance'] > 0)
                                                   echo $account['pr_opening_balance'];
                                      ?>" 
                                      <?
                                      if (isset($account))
                                          if ($account['pr_opening_balance_transaction_type'] != 'credit')
                                              if ($account['pr_opening_balance'] > 0)
                                                   echo "disabled";
                                      ?>></input>
                           </td>
                       </tr>
                   <? endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Account Type</th>
                        <th><span id = "balanceDebit"></span></th>
                        <th><span id = "balanceCredit"></span>
                        </th>
                    </tr>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>
                            <div class="text-right">
                                <a href="#!/COA/opening"><button class="btn btn-default">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                                <button onclick="submitForm('Opening Balance', 'COA/opening');" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                            </div>
                        </th>
                    </tr>
                </tfoot>
            </table>
            </form>
        </div>

        <!-- Footer -->
        <div class="footer text-muted">
            2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
        </div>
        <!-- /footer -->
        
        