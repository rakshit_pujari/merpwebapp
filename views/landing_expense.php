

<!-- Page header -->
<div class="page-header">

    <div ng-controller="dataTableController" ng-init="load(2)"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Purchase</span> - Expense</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!expense/view/new" class="btn btn-link btn-float has-text"><i class="icon-cash4 text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Add Expense</span></a>
                <a href="report/download/expense_1" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-home2 position-left"></i>Purchase</li>
            <li class="active"><i class="icon-cash4 position-left"></i>Expense</li>
        </ul>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Expense archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Expense</h6>
            <div class="heading-elements">
                </ul>
            </div>
        </div>

        <table class="table masterDataTable">
            <thead>
                <tr>
                    <th>Expense ID</th>
                    <th>Vendor Name</th>
                    <th>Date</th>
                    <th>Expense Amount</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($expenses as $expense): ?>
                    <tr>
                        <td>EXP<?php echo str_pad($expense['expense_id'], 5, "0", STR_PAD_LEFT); ?></td>
                        <td>
                            <h6 class="no-margin">
                                <a href="#!expense/view/confirm/<?php echo $expense['expense_id']; ?>"><span><?php echo $expense['expense_vendor_name']; ?></span></a>
                                <small class="display-block text-muted">Created by <?php echo $expense['employee_username']; ?> 
                                    on <?php echo $expense['expense_record_creation_time']; ?></small>
                            </h6>
                        </td>
                        <td><?php echo explode(" ", $expense['expense_date'])[0]; ?></td>
                        <td><?php echo '₹ '.number_format($expense['expense_amount'], 2); ?></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li><a href="#!expense/view/confirm/<?php echo $expense['expense_id']; ?>"><i class="icon-file-eye"></i></a></li>
                                <? if($this->session->userdata('access_controller')->is_access_granted('expense', 'delete')) { ?>
                                <li><a href='javascript: void(0)' onclick = 'showDeleteDialog("expense", <?php echo $expense['expense_id']; ?>)'><i class="glyphicon glyphicon-trash"></i></a></li>
                                <? } ?>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th>Expense ID</th>
                    <th>Vendor Name</th>
                    <th>Date</th>
                    <th>Expense Amount</th>
                    <th class="text-center"></th>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->