

<!-- Page header -->
<div class="page-header">

    <div ng-controller="coaObController" ng-init="load(<? echo sizeOf($chart_of_accounts); ?>)"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Chart Of Accounts</span> - Opening Balance</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!product/opening" class="btn btn-link btn-float has-text"><i class="icon-cube3 text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Opening Stock For Products</span></a>
                <a href="#!OC/opening/receivables" class="btn btn-link btn-float has-text"><i class="icon-drawer-in text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Opening Receivables</span></a>
                <a href="#!OC/opening/payables" class="btn btn-link btn-float has-text"><i class="icon-drawer-out text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Opening Payables</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-calculator2 position-left"></i>Accounting</li>
            <li><a href="#!COA/all"><i class="icon-book3 position-left"></i>Chart Of Accounts</a></li>
            <li class="active">Opening Balance</li>
        </ul>

    </div>
</div>
<!-- /page header -->



<!-- Content area -->
<div class="content">

    <!-- Invoice archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Chart Of Accounts</h6>
            <div class="heading-elements">
            </div>
        </div>

        <div class="panel-body">
            <?php
            $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id'=>'merpForm');
            echo form_open('web/COA/opening/save', $attributes);
            ?>
            <div class="col-lg-12">
                <div class="form-group col-lg-6">
                    <label class="control-label col-lg-3">Opening Balance Date</label>
                    <div class="col-lg-9">
                        <input class="form-control" tabindex="1" name="coa_opening_balance_date" id = "datePicker" required="required"
                               value = "<?php if (isset($oc_data)) echo explode(" ", $oc_data['oc_opening_balance_date'])[0]; ?>" >
                    </div>
                </div>
                <div class="form-group col-lg-6">
                    <div class="text-right">
                        <a href="#!/COA/all"><button class="btn btn-default">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                        <button onclick="submitForm('Opening Balance', 'COA/opening');" id ="submitCoaOb1" class="btn btn-primary" disabled>Save <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>


            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Account Name</th>
                        <th>Account Type</th>
                        <th>Debit</th>
                        <th>Credit</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($chart_of_accounts as $chart_of_account): ?>
                        <tr>
                            <td>COA<?php echo str_pad($chart_of_account['coa_id'], 5, "0", STR_PAD_LEFT); ?></td>
                            <td><a href="#!COA/view/<?php echo $chart_of_account['coa_id']; ?>">
                                    <span><?php echo $chart_of_account['coa_account_name']; ?></span>
                                </a>
                            </td>
                            <td><?php echo $chart_of_account['at_name']; ?></td>
                            <td>
                                <input min ="0" name = "coa_ob[<? echo $chart_of_account['coa_id'] ?>][0]" class = "form-control" 
                                       type = "number" onchange="invalidateCoaField(this, <? echo $chart_of_account['coa_id'] ?>, 0); disableSubmitIfNotBalanced(<? echo sizeOf($chart_of_accounts); ?>);"
                                       value = "<?
                                       if (isset($chart_of_account))
                                           if ($chart_of_account['coa_opening_balance_transaction_type'] == 'debit')
                                               if ($chart_of_account['coa_opening_balance'] > 0)                                               
                                                    echo $chart_of_account['coa_opening_balance'];
                                       ?>" 
                                       <?
                                       if (isset($chart_of_account))
                                           if ($chart_of_account['coa_opening_balance_transaction_type'] != 'debit')
                                               if ($chart_of_account['coa_opening_balance'] > 0)   
                                                    echo "disabled";
                                       ?>></input>
                            </td>
                            <td>
                                <input min ="0" name = "coa_ob[<? echo $chart_of_account['coa_id'] ?>][1]" class = "form-control" 
                                       type = "number" onchange="invalidateCoaField(this, <? echo $chart_of_account['coa_id'] ?>, 1); disableSubmitIfNotBalanced(<? echo sizeOf($chart_of_accounts); ?>);"
                                       value = "<?
                                       if (isset($chart_of_account))
                                           if ($chart_of_account['coa_opening_balance_transaction_type'] == 'credit')
                                               if ($chart_of_account['coa_opening_balance'] > 0)
                                                    echo $chart_of_account['coa_opening_balance'];
                                       ?>" 
                                       <?
                                       if (isset($chart_of_account))
                                           if ($chart_of_account['coa_opening_balance_transaction_type'] != 'credit')
                                               if ($chart_of_account['coa_opening_balance'] > 0)
                                                    echo "disabled";
                                       ?>></input>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Account Name</th>
                        <th>Account Type</th>
                        <th><span id = "balanceDebit"></span></th>
                        <th><span id = "balanceCredit"></span>
                        </th>
                    </tr>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>
                            <div class="text-right">
                                <a href="#!/COA/all"><button class="btn btn-default">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                                <button onclick="submitForm('Opening Balance', 'COA/opening');" id ="submitCoaOb2" class="btn btn-primary" disabled>Save <i class="icon-arrow-right14 position-right"></i></button>
                            </div>
                        </th>
                    </tr>
                </tfoot>
            </table>
            </form>
        </div>

        <!-- Footer -->
        <div class="footer text-muted">
            2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
        </div>
        <!-- /footer -->