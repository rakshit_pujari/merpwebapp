

<!-- Page header -->
<div class="page-header">
    <div ng-controller="formController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/product/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Master</span> - Product</h4>
        </div>
        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!/product/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!"><i class="icon-home2 position-left"></i> Master</a></li>
            <li><a href="#!/product/all"><i class = "icon-cube3 position-left"></i> Product</a></li>
            <li class="active"><?php
                if (isset($product))
                    echo 'PRO' . str_pad($product['product_id'], 5, "0", STR_PAD_LEFT);
                else
                    echo "New";
                ?></li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Form validation -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Product</h5>
        </div>


        <div class="panel-body">
            <p class="content-group-lg">A Product can be a Raw Material, Manufactured Good or a Tradable item. Product details entered here will be linked with Sales, Purchase, Inventory, Accounts and Reports.</p>
            <?php
            $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id'=>'merpForm');
            if (isset($product))
                echo form_open_multipart('web/product/save/' . $product['product_id'], $attributes);
            else
                echo form_open_multipart('web/product/save', $attributes);
            ?>
            <fieldset class="content-group">
                <legend class="text-bold">Basic details</legend>					
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="product_name">Product Name <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input required class="form-control" 
                                   onblur ="checkIfUnique(this, 'product', 'product_name', '<? if(isset($product)) echo $product['product_id']; ?>', 'product_id')"
                                   name="product_name" type="text" value = "<?php if (isset($product)) echo $product['product_name']; ?>" tabindex = "1" placeholder="Enter Product Name">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Product Type <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select class="form-control select" name="product_type" tabindex = "4" 
                                    onchange="
                                    document.getElementById('product_source_tradeable').disabled = false;
                                    document.getElementById('product_source_manufactured').disabled = false;    
                                    if(this.value == 'service'){
                                        document.getElementById('sales_label').innerHTML = 'Sales';
                                        document.getElementById('purchase_label').innerHTML = 'Purchase'
                                        document.getElementById('product_source_other').checked = true;
                                        document.getElementById('product_source_tradeable').disabled = true;
                                        document.getElementById('product_source_manufactured').disabled = true;
                                    }">
                                <option value = "goods" <? if (isset($product)) if ($product['product_type'] == 'goods') echo 'selected'; ?>>Goods</option>
                                <option value = "service" <? if (isset($product)) if ($product['product_type'] == 'service') echo 'selected'; ?>>Service</option>
                            </select>
                        </div>
                    </div>	
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Product Is <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <label class="radio-inline">
                                <input type = "radio" name = "product_source" value = "tradeable" id = "product_source_tradeable"
                                       onclick = "document.getElementById('sales_label').innerHTML = 'Sales';
                                           document.getElementById('purchase_label').innerHTML = 'Purchase'" 
                                <?
                                if (isset($product)) {
                                    if ($product['product_type'] == 'service') 
                                        echo 'disabled';
                                    else if ($product['product_source'] == 'tradeable')
                                        echo 'checked ';
                                } else
                                    echo "checked";
                                ?>>
                                For Trading
                            </label>

                            <label class="radio-inline">
                                <input type = "radio" name = "product_source" value = "manufactured" id = "product_source_manufactured" 
                                       onclick = "document.getElementById('sales_label').innerHTML = 'Sales ( Finished Good )';
                                           document.getElementById('purchase_label').innerHTML = 'Purchase ( Raw Material )'" 
                                <?
                                if (isset($product)) {
                                    if ($product['product_type'] == 'service') 
                                        echo 'disabled';
                                    else if ($product['product_source'] == 'manufactured')
                                        echo 'checked ';
                                }
                                ?>>
                                For Manufacturing
                            </label>
                            
                            <label class="radio-inline">
                                <input type = "radio" name = "product_source" value = "other" id = "product_source_other"
                                       onclick = "document.getElementById('sales_label').innerHTML = 'Sales';
                                           document.getElementById('purchase_label').innerHTML = 'Purchase'"
                                <?
                                if (isset($product)) {
                                    if ($product['product_source'] == 'other' 
                                            || $product['product_type'] == 'service')
                                        echo 'checked ';
                                }
                                ?>>
                                Others
                            </label>
                            
                        </div>		
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Transaction Type <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <label class="radio-inline">
                                <input type="checkbox" class ="styled" name="transaction_type[]" required value = "sales" <?php if (isset($product)) if (in_array('sales', explode(",", $product['transaction_type']))) echo "checked"; ?> 
                                       onclick="document.getElementsByName('selling_price')[0].disabled = !this.checked">
                                &nbsp;&nbsp;<span id = "sales_label"><? 
                                if(isset($product)){
                                    if ($product['product_source'] == 'manufactured') {
                                            echo "Sales ( Finished Good )";
                                        } else {
                                            echo "Sales";
                                        } 
                                } else {
                                    echo "Sales";
                                } ?></span>
                            </label>

                            <label class="radio-inline" style="padding-left:14px">
                                <input type="checkbox" class ="styled" name="transaction_type[]" required value = "purchase"  <?php if (isset($product)) if (in_array('purchase', explode(",", $product['transaction_type']))) echo "checked"; ?>
                                        onclick="document.getElementsByName('purchase_price')[0].disabled = !this.checked">
                                &nbsp;&nbsp;<span id = "purchase_label">
                                    <? 
                                    if(isset($product)){
                                        if ($product['product_source'] == 'manufactured') {
                                            echo "Purchase ( Raw Material )";
                                        } else {
                                            echo "Purchase";
                                        } 
                                    } else {
                                        echo "Purchase";
                                    }  ?>
                                </span>
                            </label>
                        </div>
                    </div>	

                </div>
                    
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Product Category </label>
                        <div class="col-lg-9">
                            <select class="form-control select2" name="product_category" tabindex = "10">
                                <option value = "">Choose a category</option>
                                <option <?php if (isset($product)) if ("Sauces" == $product['product_category']) echo "selected" ?>>Sauces</option>
                                <option <?php if (isset($product)) if ("Noodles" == $product['product_category']) echo "selected" ?>>Noodles</option>
                                <option <?php if (isset($product)) if ("Jams" == $product['product_category']) echo "selected" ?>>Jams</option>
                                <option <?php if (isset($product)) if ("Spices" == $product['product_category']) echo "selected" ?>>Spices</option>
                                <option <?php if (isset($product)) if ("Pickles" == $product['product_category']) echo "selected" ?>>Pickles</option>
                                <option <?php if (isset($product)) if ("Paste" == $product['product_category']) echo "selected" ?>>Paste</option>
                                <option <?php if (isset($product)) if ("Chutney" == $product['product_category']) echo "selected" ?>>Chutney</option>
                                <option <?php if (isset($product)) if ("Liquid" == $product['product_category']) echo "selected" ?>>Liquid</option>
                                <option <?php if (isset($product)) if ("Others" == $product['product_category']) echo "selected" ?>>Others</option>
                            </select>
                        </div>
                    </div>	`
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="product_code">Product Code</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="product_code" type="text" value = "<?php if (isset($product)) echo $product['product_code']; ?>" tabindex = "1" placeholder="Any short code for this product">
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="hsn_code">HSN / SAC Code <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="hsn_code" type="text" value = "<?php if (isset($product)) echo $product['hsn_code']; ?>" tabindex = "1" placeholder="Enter HSN / SAC Code" required>
                        </div>
                    </div>							
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="product_uqc_id">Unique Quantity Code <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                           <select class="form-control select2" name="product_uqc_id" tabindex = "2" required>
                               <option value ="">Choose an option</option>
                                <?
                                foreach ($unique_quantity_codes as $unique_quantity_code) {
                                    ?>
                                    <option 
                                    <?php
                                    if(set_value('product_uqc_id') == $unique_quantity_code['uqc_id'])
                                        echo
                                        "selected";
                                    else if (isset($product)) {
                                        if ($product['product_uqc_id'] == $unique_quantity_code['uqc_id'])
                                            echo
                                            "selected";
                                    }
                                    ?> value="<? echo $unique_quantity_code['uqc_id']; ?>"><? echo $unique_quantity_code['uqc_short']; ?>
                                    </option>
                                    <?
                                }
                                ?>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="col-lg-3 control-label">Product Image</label>
                        <div class="col-lg-9">
                            <input type="file" name = "product_image_path" id = "product_image_path" 
                                   class="file-input-preview" data-show-remove="false"  data-dismiss="fileupload" data-show-upload="false" 
                                   data-image-path = "<?php if (isset($product)) echo $product['product_image_path'] ?>" 
                                   data-image-name = "<?php if (isset($product)) echo $product['product_name'] ?>">
                            <span class="help-block">Maximum file upload size is <code>100 KB.</code> </span>
                        </div>
                    </div>		
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Product Status </label>
                        <div class="col-lg-9">
                            <div class="checkbox checkbox-switch">
                                <label>
                                    <input type="checkbox" name="product_status" data-on-text="Yes" data-off-text="No" class="switch" <?php
                                    if (isset($product)) {
                                        if ($product['product_status'] == 'active')
                                            echo "checked";
                                    } else {
                                        echo "checked";
                                    }
                                    ?>>
                                    Active
                                </label>
                            </div>
                        </div>
                    </div>
                   
                </div>

            </fieldset>
            <fieldset class="content-group">
                <legend class="text-bold">Pricing</legend>	
                <div class="col-lg-12">
                    <div class="form-group col-lg-6 has-feedback has-feedback-left">
                        <label class="control-label col-lg-3" for="purchase_price">Purchase Price </label>
                        <div class="col-lg-9">
                            <input class="form-control" name="purchase_price" type="number" 
                                <?php if(!isset($product) || !in_array('purchase', explode(",", $product['transaction_type']))) 
                                            echo "disabled"; ?> 
                                   value = "<?php if (isset($product)) echo $product['purchase_price']; ?>" tabindex = "1" placeholder="Enter Purchase Price in INR">
                            <div class="form-control-feedback">
                                <span style = "font-size:16px;">₹</span>
                            </div>

                        </div>
                    </div>	
                    <div class="form-group col-lg-6 has-feedback has-feedback-left">
                        <label class="control-label col-lg-3" for="selling_price">Selling Price </label>
                        <div class="col-lg-9">
                            <input class="form-control" name="selling_price" type="number" 
                                    <?php if(!isset($product) || !in_array('sales', explode(",", $product['transaction_type']))) 
                                            echo "disabled"; ?>  value = "<?php if (isset($product)) echo $product['selling_price']; ?>" tabindex = "1" placeholder="Enter Selling Price in INR">
                            <div class="form-control-feedback">
                                <span style = "font-size:16px;">₹</span>
                            </div>
                        </div>
                    </div>	
                </div>
            </fieldset>
            <fieldset class="content-group">
                <legend class="text-bold">Taxation</legend>	
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Intra-State GST <span class="text-danger">*</span></label>
                        <div class="col-lg-9">

                            <select class="form-control select" name="intra_state_tax_id" tabindex = "4">
                                <?
                                foreach ($intrastate_taxes as $intrastate_tax) {
                                    ?>
                                    <option 
                                    <?php
                                    if(set_value('intra_state_tax_id') == $intrastate_tax['tax_id'])
                                        echo
                                        "selected";
                                    else if (isset($product)) {
                                        if ($product['intra_state_tax_id'] == $intrastate_tax['tax_id'])
                                            echo
                                            "selected";
                                    }
                                    ?> value="<? echo $intrastate_tax['tax_id']; ?>"><? echo $intrastate_tax['tax_name']; ?>
                                    </option>
                                    <?
                                }
                                ?>

                            </select>

                        </div>
                    </div>	
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Inter-State GST <span class="text-danger">*</span></label>
                        <div class="col-lg-9">

                            <select class="form-control select" name="inter_state_tax_id" tabindex = "4">
                                <?
                                foreach ($interstate_taxes as $interstate_tax) {
                                    ?>
                                    <option 
                                    <?php
                                    if (set_value('inter_state_tax_id') == $interstate_tax ['tax_id'])
                                        echo " selected";
                                    else if (isset($product)) {
                                        if ($product['inter_state_tax_id'] == $interstate_tax ['tax_id'])
                                            echo " selected";
                                    }
                                    ?> value="<? echo $interstate_tax['tax_id'] ?>"><? echo $interstate_tax['tax_name'] ?>
                                    </option>
                                    <?
                                }
                                ?>
                            </select>

                        </div>
                    </div>	
                </div>
            </fieldset>
            <!--<fieldset class="content-group">
                <legend class="text-bold">Stocks</legend>	
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3" for="opening_stock">Opening Stock </label>
                        <div class="col-lg-9">
                            <input class="form-control" name="opening_stock" type="text" value = "<?php if (isset($product_opening_balance)) echo $product_opening_balance['pob_opening_balance']; ?>" tabindex = "1" placeholder="Enter Opening Stock">
                        </div>
                    </div>	
                    <div class="form-group col-lg-6 has-feedback has-feedback-left">
                        <label class="control-label col-lg-3" for="rate_per_unit">Rate Per Unit</label>
                        <div class="col-lg-9">
                            <input class="form-control" name="rate_per_unit" type="text" value = "<?php if (isset($product_opening_balance)) echo $product_opening_balance['pob_unit_rate']; ?>" tabindex = "1" placeholder="Rate in INR">
                            <div class="form-control-feedback">
                                <span style = "font-size:16px;">₹</span>
                            </div>
                        </div>
                    </div>	
                </div>
            </fieldset>-->
            <div class="text-right">
                <a href="#!/product/all"><button class="btn btn-default">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                <? 
                if($this->session->userdata('access_controller')->is_access_granted('product', 'save')) { ?>
                <button onclick="submitForm('product');" id = "buttonSubmit" class="btn btn-primary">Save<i class="icon-arrow-right14 position-right"></i></button>
                <? } ?>
            </div>
            </form>

        </div>

        </section>
    </div>	
    <!-- /form validation -->
    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->