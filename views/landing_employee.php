

<!-- Page header -->
<div class="page-header">

    <div ng-controller="dataTableController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Master</span> - Employee</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!employee/view" class="btn btn-link btn-float has-text"><i class=" icon-user-tie text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Add Employee</span></a>
                <a href="report/download/employee" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-home2 position-left"></i>Master</li>
            <li class="active"><i class="icon-user-tie position-left"></i>Employee</li>
        </ul>
    </div>
</div>
<!-- /page header -->



<!-- Content area -->
<div class="content">

    <!-- Invoice archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Employee</h6>
            <div class="heading-elements">

            </div>
        </div>

        <table class="table masterDataTable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Employee Name</th>
                    <th>City</th>
                    <th>Department</th>
                    <th>Designation</th>
                    <th>Contact</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($employees as $employee): ?>
                    <tr>
                        <td>EMP<?php echo str_pad($employee['employee_id'], 5, "0", STR_PAD_LEFT); ?></td>
                        <td>
                            <h6 class="no-margin">
                                <a href="#!employee/view/<?php echo $employee['employee_id']; ?>"><span><?php echo $employee['employee_name']; ?></span></a>
                                <small class="display-block text-muted">Created by <?php echo $employee['employee_record_created_by_name']; ?> 
                                    on <?php echo $employee['employee_record_creation_time']; ?></small>
                            </h6>
                        </td>
                        <td><a href="#!location/view/<?php echo $employee['location_id']; ?>"><span><?php echo $employee['city_name']; ?></span></a></td>
                        <td><?php echo $employee['employee_department']; ?></td>
                        <td><?php echo $employee['employee_designation']; ?></td>
                        <td><?php echo $employee['employee_contact_number']; ?></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li><a href="#!employee/view/<?php echo $employee['employee_id']; ?>"><i class="icon-file-eye"></i></a></li>
                                <? 
                                if($this->session->userdata('access_controller')->is_access_granted('employee', 'delete')) { 
                                    if($employee['employee_id']!=1){?>
                                <li><a href='javascript: void(0)' onclick = 'showDeleteDialog("employee", <?php echo $employee['employee_id']; ?>)'><i class="glyphicon glyphicon-trash"></i></a></li>
                                    <? }
                                    
                                    } ?>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td>ID</td>
                    <td>Employee Name</td>
                    <td>City</td>
                    <td>Department</td>
                    <td>Designation</td>
                    <td>Contact</td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->