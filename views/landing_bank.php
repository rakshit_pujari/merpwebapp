

<!-- Page header -->
<div class="page-header">

    <div ng-controller="dataTableController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Master</span> - Bank</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!bank/view" class="btn btn-link btn-float has-text"><i class="fa fa-bank text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Add Bank</span></a>
                <a href="report/download/bank" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-home2 position-left"></i> Master</li>
            <li class="active"><i class="fa fa-bank position-left"></i>Bank</li>
        </ul>

    </div>
</div>
<!-- /page header -->



<!-- Content area -->
<div class="content">

    <!-- Invoice archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Bank</h6>
            <div class="heading-elements">
            </div>
        </div>

        <table class="table masterDataTable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Bank Name</th>
                    <th>Account</th>
                    <th>City</th>
                    <th>Relationship Manager Name</th>
                    <th>Contact</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($banks as $bank): ?>
                    <tr>
                        <td>BNK<?php echo str_pad($bank['bank_id'], 5, "0", STR_PAD_LEFT); ?></td>
                        <td>
                            <h6 class="no-margin">
                                <a href="#!bank/view/<?php echo $bank['bank_id']; ?>"><span><?php echo $bank['bank_name']; ?></span></a>
                                <small class="display-block text-muted">Created by <?php echo $bank['employee_name']; ?> 
                                    on <?php echo $bank['bank_record_creation_time']; ?></small>
                            </h6>
                        </td>
                        <td><?php echo $bank['bank_account_name']; ?></td>
                        <td><?php echo $bank['city_name']; ?></td>
                        <td><?php echo $bank['bank_relationship_manager_name']; ?></td>
                        <td><?php echo $bank['bank_contact_number']; ?></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li><a href="#!bank/view/<?php echo $bank['bank_id']; ?>"><i class="icon-file-eye"></i></a></li>
                                <? 
                                if($this->session->userdata('access_controller')->is_access_granted('bank', 'delete')) { ?>
                                <li><a href='javascript: void(0)' onclick = 'showDeleteDialog("bank", <?php echo $bank['bank_id']; ?>)'><i class="glyphicon glyphicon-trash"></i></a></li>
                                <? } ?>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td>ID</td>
                    <td>Bank Name</td>
                    <td>Account</td>
                    <td>City</td>
                    <td>Relationship Manager Name</td>
                    <td>Contact</td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->