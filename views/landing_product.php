

<!-- Page header -->
<div class="page-header">

    <div ng-controller="dataTableController" ng-init="load(4)"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Master</span> - Product</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!product/view" class="btn btn-link btn-float has-text"><i class="icon-cube3 text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Add Product</span></a>
                <a href="report/download/product" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-home2 position-left"></i>Master</li>
            <li class="active"><i class="icon-cube3 position-left"></i>Product</li>
        </ul>

    </div>
</div>
<!-- /page header -->



<!-- Content area -->
<div class="content">

    <!-- Invoice archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Product</h6>
            <div class="heading-elements">
            </div>
        </div>

        <table class="table masterDataTable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Image</th>
                    <th>Product Name</th>
                    <th>Transaction Type</th>
                    <th>Category</th>
                    <th>Status</th>
                    <th>Inventory</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($products as $product): ?>
                    <tr>
                        <td>PRD<?php echo str_pad($product['product_id'], 5, "0", STR_PAD_LEFT); ?></td>
                        <td><img width ="150px" src="<?php echo $product['product_image_path']; ?>"><span></span></td>
                        <td>
                            <h6 class="no-margin">
                                <a href="#!product/view/<?php echo $product['product_id']; ?>"><span><?php echo $product['product_name']; ?></span></a>
                                <small class="display-block text-muted">Created by <?php echo $product['employee_name']; ?> 
                                    on <?php echo $product['product_record_creation_time']; ?></small>
                            </h6>
                        </td>
                        <td><?php echo $product['transaction_type']; ?></td>
                        <td><?php echo $product['product_category']; ?></td>
                        <td><?php echo $product['product_status']; ?></td>
                        <td><?php
                        
                        if(isset($product['product_quantity'])){
                            if ($product['product_quantity'] > 0)
                                echo '<span class="label bg-success"><span>'.$product['product_quantity'].' '. $product['uqc_text'].'</span></span>';
                            else
                                echo '<span class="label bg-danger"><span>'.$product['product_quantity'].' '. $product['uqc_text'].'</span></span>';      
                        }
                        ?></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li><a href="#!product/view/<?php echo $product['product_id']; ?>"><i class="icon-file-eye"></i></a></li>
                                <? 
                                if($this->session->userdata('access_controller')->is_access_granted('product', 'delete')) { ?>
                                <li><a href='javascript: void(0)' onclick = 'showDeleteDialog("product", <?php echo $product['product_id']; ?>)'><i class="glyphicon glyphicon-trash"></i></a></li>
                                <? } ?>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td>ID</td>
                    <th>Image</th>
                    <td>Product Name</td>
                    <td>Transaction Type</td>
                    <td>Category</td>
                    <td>Status</td>
                    <td>Inventory</td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->