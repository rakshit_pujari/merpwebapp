
<div ng-controller="appSettingsController" ng-init="load()"></div> 
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/dashboard/one"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Accounting Preferences</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                    <!--<a href="#!/location/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>-->
                <a href="#!COA/opening" class="btn btn-link btn-float has-text"><i class="icon-plus3 text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Opening Balance</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-cog3 position-left"></i>Settings</li>
            <li class="active"><i class="icon-city position-left"></i>Accounting Preferences</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Form validation -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Accounting Preferences</h5>

        </div>
        <div class="panel-body">
            <p class="content-group-lg">Accounting Preferences include the configuration for all system-generated accounting transactions. Accounting transaction entries will be linked to the accounts configured here.</p>
            <?php
            $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id'=>'merpForm');
            echo form_open_multipart('web/settings/accounting/save', $attributes);
            ?>
            <div class="tabbable" id = "homeTabs">
                <ul class="nav nav-tabs nav-tabs-highlight">
                    <li class="active" id = "0"><a href="#subhighlighted-tab1" data-toggle="tab" target="_self">General</a></li>
                    <li id = "1"><a href="#subhighlighted-tab2" data-toggle="tab" target="_self">Sales</a></li>
                    <li id = "2"><a href="#subhighlighted-tab3" data-toggle="tab" target="_self">Credit Note</a></li>
                    <li id = "3"><a href="#subhighlighted-tab4" data-toggle="tab" target="_self">Purchase</a></li>
                    <li id = "4"><a href="#subhighlighted-tab5" data-toggle="tab" target="_self">Debit Note</a></li>
                    <li id = "5"><a href="#subhighlighted-tab6" data-toggle="tab" target="_self">Payments</a></li>
                    <li id = "6"><a href="#subhighlighted-tab7" data-toggle="tab" target="_self">Receipts</a></li>
                    <li id = "7"><a href="#subhighlighted-tab8" data-toggle="tab" target="_self">Manufacturing</a></li>
                </ul>
                <div class="tab-content">

                    <div class="tab-pane active" id="subhighlighted-tab1">
                        <div class="col-lg-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Sr No.</th>
                                        <th>Field Name</th>
                                        <th>Linked Chart Of Account</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?
                                    $count = 1;
                                    foreach ($general_settings as $general_setting):
                                        ?>
                                        <tr>
                                            <td width="10%"><? echo $count++; ?></td>
                                            <td width="20%"><? echo $general_setting['as_field_display_name']; ?></td>
                                            <td width="35%">
                                                <select name = "accounting_settings[<? echo $general_setting['as_id']; ?>][0]" 
                                                        required class = "form-control select2">
                                                            <?
                                                            foreach ($chart_of_accounts as $chart_of_account) {
                                                                ?>
                                                        <option value = "<? echo $chart_of_account['coa_id']; ?>"
                                                        <?
                                                        if ($general_setting['debit_account_coa_id'] == $chart_of_account['coa_id'])
                                                            echo " selected ";
                                                        ?>>
                                                                    <? echo $chart_of_account['coa_account_name']; ?> 
                                                        </option>  
                                                        <?
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <?
                                    endforeach;
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Sr No.</th>
                                        <th>Field Name</th>
                                        <th>Linked Chart Of Account</th>
                                    </tr>
                                </tfoot>
                            </table>


                        </div>
                    </div>

                    <? generateTable('subhighlighted-tab2', $invoice_accounting_settings, $chart_of_accounts); ?>
                    <? generateTable('subhighlighted-tab3', $credit_note_accounting_settings, $chart_of_accounts); ?>
                    <? generateTable('subhighlighted-tab4', $purchase_accounting_settings, $chart_of_accounts); ?>
                    <? generateTable('subhighlighted-tab5', $debit_note_accounting_settings, $chart_of_accounts); ?>
                    <? generateTable('subhighlighted-tab6', $payment_accounting_settings, $chart_of_accounts); ?>
                    <? generateTable('subhighlighted-tab7', $receipt_accounting_settings, $chart_of_accounts); ?>
                    <? generateTable('subhighlighted-tab8', $manufacturing_accounting_settings, $chart_of_accounts); ?>
                </div>


            </div>

            <div class="text-right">
                <a href="#!/dashboard/one"><button class="btn btn-default">Cancel <i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                <button onclick="submitForm('Accounting Settings', 'settings/accounting');" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
            </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->

    <?

    function generateTable($tab_id, $items, $chart_of_accounts) { ?> 
        <div class="tab-pane" id="<? echo $tab_id; ?>">
            <div class="col-lg-12">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Sr No.</th>
                            <th>Field Name</th>
                            <th>Debit(Dr)</th>
                            <th>Credit(Cr)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?
                        $count = 1;
                        foreach ($items as $item):
                            ?>
                            <tr>
                                <td width="10%"><? echo $count++; ?></td>
                                <td width="20%"><? echo $item['as_field_display_name']; ?></td>
                                <td width="35%">
                                    <select name = "accounting_settings[<? echo $item['as_id']; ?>][0]" 
                                            required class = "form-control select2">
                                                <?
                                                foreach ($chart_of_accounts as $chart_of_account) {
                                                    ?>
                                            <option value = "<? echo $chart_of_account['coa_id']; ?>"
                                            <?
                                            if ($item['debit_account_coa_id'] == $chart_of_account['coa_id'])
                                                echo " selected ";
                                            ?>>
                                                        <? echo $chart_of_account['coa_account_name']; ?> 
                                            </option>  
                                            <?
                                        }
                                        ?>
                                    </select>
                                </td>
                                <td width="35%">
                                    <select name = "accounting_settings[<? echo $item['as_id']; ?>][1]" 
                                            required class = "form-control select2">
                                        <?
                                        foreach ($chart_of_accounts as $chart_of_account) {
                                            ?>
                                            <option value = "<? echo $chart_of_account['coa_id']; ?>"
                                            <?
                                            if ($item['credit_account_coa_id'] == $chart_of_account['coa_id'])
                                                echo " selected ";
                                            ?>>
                                                        <? echo $chart_of_account['coa_account_name']; ?> 
                                            </option>  
                                            <?
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <?
                        endforeach;
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Sr No.</th>
                            <th>Field Name</th>
                            <th>Debit(Dr)</th>
                            <th>Credit(Cr)</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    <? }
    ?>