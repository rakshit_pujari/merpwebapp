

<!-- Page header -->
<div class="page-header">

    <div ng-controller="dataTableController" ng-init="load(2)"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Sales</span> - Pro-forma Invoice</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!ProForma/view/new" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-list-alt  position-left" style = "font-size:22px;color:#26A69A !important"></i> <span>Add Pro-forma Invoice</span></a>
                <a href="report/download/pro_forma" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="fa fa-inr" style = "font-size:12px!important"></i> Sales</li>
            <li class="active"><i class="glyphicon glyphicon-list-alt position-left"></i>Pro-forma Invoice</li>
        </ul>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Pro Forma archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Pro-forma Invoice</h6>
            <div class="heading-elements">

            </div>
        </div>

        <table class="table masterDataTable">
            <thead>
                <tr>
                    <th>Pro-forma no.</th>
                    <th>Customer Name</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th>Amount</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($pro_formas as $pro_forma): ?>
                    <tr>
                        <td>
                            <? if ($pro_forma['pro_forma_status'] == 'confirm') { ?>
                                PRF<?php echo str_pad($pro_forma['pro_forma_id'], 5, "0", STR_PAD_LEFT); ?>
                            <? } else { ?>
                                DRAFT
                            <? } ?>
                        </td>
                        <td>
                            <h6 class="no-margin">
                                <? if ($pro_forma['pro_forma_status'] == 'confirm') { ?>
                                    <a href="#!ProForma/preview/confirm/<? echo $pro_forma['pro_forma_id']; ?>">
                                    <? } else { ?>
                                        <a href="#!ProForma/view/<? echo $pro_forma['pro_forma_status']; ?>/<? echo $pro_forma['pro_forma_id']; ?>">
                                        <? } ?>
                                        <span><?php echo $pro_forma['pro_forma_linked_company_display_name']; ?></span>
                                    </a>
                                    <small class="display-block text-muted">Created by <?php echo $pro_forma['employee_name']; ?> 
                                        on <?php echo $pro_forma['pro_forma_record_creation_time']; ?></small>
                            </h6>
                        </td>
                        <td><?php echo explode(" ", $pro_forma['pro_forma_date'])[0]; ?></td>
                        <td>
                            <?php
                            if ($pro_forma['pro_forma_status'] == 'confirm')
                                echo '<span class="label bg-blue"><span>' . $pro_forma['pro_forma_status'] . '</span></span>';
                            else
                                echo '<span class="label bg-danger"><span>' . $pro_forma['pro_forma_status'] . '</span></span>';
                            ?>

                        </td>
                        <td>₹ <?php echo number_format($pro_forma['pro_forma_amount'], 2); ?></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li><? if ($pro_forma['pro_forma_status'] == 'confirm') { ?>
                                        <a href="#!ProForma/preview/confirm/<? echo $pro_forma['pro_forma_id']; ?>">
                                        <? } else { ?>
                                            <a href="#!ProForma/view/<? echo $pro_forma['pro_forma_status']; ?>/<? echo $pro_forma['pro_forma_id']; ?>">
                                            <? } ?>
                                            <i class="icon-file-eye"></i></a>
                                        <? if ($this->session->userdata('access_controller')->is_access_granted('pro_forma', 'delete')
                                                && $pro_forma['pro_forma_status'] == 'draft') { 
                                            ?>
                                            <li><a href='javascript: void(0)' onclick = 'showDeleteDialog("ProForma", <?php echo $pro_forma['pro_forma_id']; ?>)'><i class="glyphicon glyphicon-trash"></i></a></li>
                                        <? } ?>
                                </li>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td>Pro-forma no.</td>
                    <td>Date</td>
                    <td>Customer Name</td>
                    <th>Status</th>
                    <td>Amount</td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->