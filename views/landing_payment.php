

<!-- Page header -->
<div class="page-header">

    <div ng-controller="dataTableController" ng-init="load(2)"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Purchase</span> - Payment</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!Payment/view" class="btn btn-link btn-float has-text"><i class="icon-credit-card2 text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Add Payment</span></a>
                <a href="report/download/payment" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-home2 position-left"></i>Purchase</li>
            <li class="active"><i class="icon-city position-left"></i>Payment</li>
        </ul>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Payment archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Payment</h6>
            <div class="heading-elements">
                
            </div>
        </div>

        <table class="table masterDataTable">
            <thead>
                <tr>
                    <th>Payment no.</th>
                    <th>Customer Name</th>
                    <th>Date</th>
                    <th>Purchase Invoice</th>
                    <th>Amount</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($payments as $payment): ?>
                    <tr>
                        <td>PAY<?php echo str_pad($payment['payment_id'], 5, "0", STR_PAD_LEFT); ?></td>
                        <td>
                            <h6 class="no-margin">
                                <a href="#!Payment/preview/<?php echo $payment['payment_id']; ?>"><span><?php echo $payment['payment_linked_company_invoice_name']; ?></span></a>
                                <small class="display-block text-muted">Created by <?php echo $payment['employee_username']; ?> 
                                    on <?php echo $payment['payment_record_creation_time']; ?></small>
                            </h6>
                        </td>
                        <td><?php echo explode(" ", $payment['payment_date'])[0]; ?></td>
                        <td><a href="#!purchase/view/<?php echo $payment['payment_linked_purchase_id']; ?>"><span>PUR<?php echo str_pad($payment['payment_linked_purchase_id'], 5, "0", STR_PAD_LEFT); ?></span></a></td>
                        <td><?php echo '₹ '.number_format($payment['payment_amount'], 2); ?></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li><a href="#!Payment/preview/<?php echo $payment['payment_id']; ?>"><i class="icon-file-eye"></i></a></li>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td>Payment no.</td>
                    <td>Date</td>
                    <td>Customer Name</td>
                    <th>Purchase Invoice</th>
                    <td>Amount</td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->