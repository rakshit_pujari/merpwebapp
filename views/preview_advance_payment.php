<?php

function getIndianCurrency($number) {
    $decimal = round($number - ($no = floor($number)), 2) * 100;
    $hundred = null;
    $digits_length = strlen($no);
    $i = 0;
    $str = array();
    $words = array(0 => '', 1 => 'one', 2 => 'two',
        3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
        7 => 'seven', 8 => 'eight', 9 => 'nine',
        10 => 'ten', 11 => 'eleven', 12 => 'twelve',
        13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
        16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
        19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
        40 => 'forty', 50 => 'fifty', 60 => 'sixty',
        70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
    $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
    while ($i < $digits_length) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number] . ' ' . $digits[$counter] . $plural . ' ' . $hundred : $words[floor($number / 10) * 10] . ' ' . $words[$number % 10] . ' ' . $digits[$counter] . $plural . ' ' . $hundred;
        } else
            $str[] = null;
    }
    $Rupees = implode('', array_reverse($str));
    //$paise = ($decimal) ? " and " . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    $paise = ($decimal) ? " and " . ($words[((int) ($decimal/10)) * 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    return ($Rupees ? $Rupees . ' ' : '') . $paise;
}
?>
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/AdvancePayment/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Purchase</span> - Advance Payment</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                    <!--<a href="#!/AdvancePayment/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>-->
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!"><i class="icon-home2 position-left"></i> Purchase</a></li>
            <li><a href="#!/AdvancePayment/all"><i class="icon-city position-left"></i> Advance Payment</a></li>
            <li class="active">ADP<?php if (isset($advance_payment)) echo str_pad($advance_payment['advance_payment_id'], 5, "0", STR_PAD_LEFT); ?></li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <a href="#!/AdvancePayment/all"><button class="btn btn-default">Back<i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
    <a href="#!/AdvancePayment/view/<?php if (isset($advance_payment)) echo $advance_payment['advance_payment_id']; ?>"><button class="btn btn-default">Edit <i class="glyphicon  glyphicon-pencil position-right"></i></button></a>
    <button onClick="window.print();" class="btn btn-primary">Print <i class="icon-arrow-right14 position-right"></i></button>

    <!-- Form validation -->
    <div class="panel-flat">



        <div class="panel panel-body col-md-offset-3"  id = "section-to-print" style="width: 210mm;min-height: 297mm;padding-left:40px;padding-right:40px;">

            <span id = "paymentHeader">Payment Voucher</span>
            <hr>

            <table class = "paymentTable">
                <tr>
                    <td class = "logo_container" colspan = "2" rowspan = "2	">
                        <img class = "logo" src="<? echo $advance_payment['advance_payment_oc_logo_path']; ?>"></img>
                    </td>
                    <td>
                        <span class = "paymentField">Payment date</span>												
                        <br/>
                        <span class = "paymentFieldValue"><? echo explode(" ", $advance_payment['advance_payment_date'])[0]; ?></span>
                    </td>
                    <td>
                        <span class = "paymentField">Place Of Supply</span>												
                        <br/>
                        <span class = "paymentFieldValue"><? echo $advance_payment['advance_payment_place_of_supply']; ?></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class = "paymentField">Payment No.</span>												
                        <br/>
                        <span class = "paymentFieldValue">ADP<? echo str_pad($advance_payment['advance_payment_id'], 5, "0", STR_PAD_LEFT); ?></span>
                    </td>
                    <td>
                        <span class = "paymentField">Payment Type</span>												
                        <br/>
                        <span class = "paymentFieldValue">ADVANCE</span>
                    </td>
                </tr>
                <tr>
                    <td colspan = "2" rowspan = "2">
                        <span class = "paymentFieldValue"><? echo $advance_payment['advance_payment_oc_name']; ?></span><br/>
                        <span class = "paymentField">
                            <? echo $advance_payment['advance_payment_oc_address']; ?><br/>
                            GSTIN: <? echo $advance_payment['advance_payment_oc_gstin']; ?><br/>
                            PAN: <? echo $advance_payment['advance_payment_oc_pan_number']; ?>

                        </span>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>

            </table>
            <table class = "paymentTable gray-border">

                <tr>
                    <td>Receiver details (Billed to)</td>
                    <td>

                    </td>

                </tr>
                <tr>
                    <td>
                        Name:<span class = "paymentFieldValue"><? echo $advance_payment['advance_payment_linked_company_invoice_name']; ?></span><br/>
                        Address:<? echo $advance_payment['advance_payment_linked_company_billing_address']; ?><br/>
                        State:<? echo $advance_payment['advance_payment_linked_company_billing_state_name']; ?><br/>
                        Contact details:<? echo $advance_payment['advance_payment_linked_company_contact_number']; ?><br/>
                        GST#:<? echo $advance_payment['advance_payment_linked_company_gstin']; ?><br/>
                    </td>
                    <td>

                    </td>
                </tr>
            </table>

            <hr>
            <table class = "productList paymentTable">
                <thead class = "grayTBack">
                    <tr>
                        <td>Sr</td>
                        <td>Payment Mode</td>
                        <td>Advance Amount</td>
                        <td>Transaction Reference</td>
                        <td>Notes</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                       <td>1</td>
                       <td><? echo ucwords($advance_payment['advance_payment_payment_mode']); ?></td>
                       <td><? echo '₹ '.number_format($advance_payment['advance_payment_amount'], 2); ?></td>
                       <td><? echo $advance_payment['advance_payment_transaction_reference']; ?></td>
                       <td><? echo $advance_payment['advance_payment_notes']; ?></td>
                    </tr>
                </tbody>
                <tfoot class = "grayTBack">
                    <tr style="height:20px">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>

            <table class = "paymentTable">
                <tr>
                    <td colspan = "6" rowspan = "2"></td>
                    <td colspan = "2"></td>
                    <td> </td>
                </tr>
                <tr>
                    <td colspan = "2"></td>
                    <td> </td>
                </tr>
                <tr>
                    <td colspan = "6"></td>
                    <td colspan = "2"></td>
                    <td>
                    </td>
                </tr>

                <tr>
                    <td colspan = "6" style = "text-align:left;">
                        Total Advance Payment Amount (in words)
                        <br>
                        <span style = "font-weight:500;">Rupees</span> <span style = "font-weight:800;"><? echo ucwords(getIndianCurrency($advance_payment['advance_payment_amount'])); ?> </span>Only
                    </td>
                    <td colspan = "2"></td>
                    <td> </td>
                </tr>

                <tr>
                    <td colspan = "6" style = "text-align:left;">
                        
                    </td>
                    <td colspan = "2">Authorized Signatory<br/><br/><br/><span style = "font-weight:800;"><? echo $advance_payment['advance_payment_oc_name']; ?></span></td>
                    <td> </td>
                </tr>


                <table>
                    <span class = "termsText">Paid From :
                        <? echo $advance_payment['advance_payment_credit_coa_name']; ?>
                    </span>
                    <!--<div class="ribbon-container">
                            <div class="ribbon bg-success-400">PAID</div>
                    </div>-->

                    </div>

            <footer style = "position:absolute;bottom:0;right:0;padding-right:20px;padding-bottom:15px;font-size:11px;font-style:italic;">
                Powered By Quant ERP
            </footer>
                    </div>


                    <style>
                        @media print {
                            body * {
                                visibility: hidden;
                            }
                            #section-to-print, #section-to-print * {
                                visibility: visible;
                                overflow:hidden;
                            }
                            #section-to-print {
                                position: absolute;
                                left: 0;
                                top: 0;
                                size: auto;   /* auto is the initial value */
                                margin: 0;
                                border: initial;
                                border-radius: initial;
                                width: 100%;
                                min-height: initial;
                                box-shadow: initial; 
                                background: initial;
                                page-break-after: always;
                            }
                            .grayTBack {
                                visibility: visible;
                                background-color: #ccc!important;
                                color: black;
                                font-weight:700;
                                -webkit-print-color-adjust: exact;
                                border: solid #ccc!important;
                            }

                            .gray-border{
                                border: 1px solid #ccc;
                                overflow:visible!important;
                                -webkit-print-color-adjust: exact;
                            }
                        }
                        @page 
                        {
                            size:  auto;   /* auto is the initial value */
                            margin: 0mm;  /* this affects the margin in the printer settings */
                        }

                        .termsText{
                            font-size: 10px;
                        }
                        .productList{
                            border: 1px solid #FFFFFF;
                            text-align: center;
                            font-size: 11px;
                        }
                        .whiteTBack {
                            background-color: #FFFFFF;
                            color: black;
                            font-weight:700;
                        }
                        .grayTBack {
                            background-color: #ccc;
                            color: black;
                            font-weight:700;
                        }
                        .paymentTable {
                            font-size: 11px;
                            width: 100%;
                            max-width: 100%;
                            table-layout: fixed;
                            font-family:"arial";
                            margin-bottom:5px; 
                            padding:10px;
                        }
                        .gray-border{
                            border: 1px solid #ccc;
                            border-collapse: collapse;
                        }

                        .logo {
                            max-width:40mm; max-height: 20mm;
                            text-align: center;
                        }
                        .logo_container {
                            /*text-align: center;*/
                        }
                        #paymentHeader {
                            font-size: 20px;
                            font:"arial";
                            width:100%;
                            text-align: center;
                            font-family:"arial";
                            font-weight: bold;
                            display: block;
                        }
                        hr {
                            border: 3px solid #000000;
                            margin-top:3px;
                        }
                        .paymentField {
                            font-size: 11px;	
                        }
                        .paymentFieldValue {
                            font-weight: bold;
                            font-size: 12px;
                        }
                        
                        td {
                            padding:3px;
                        }
                        
                        hr {
                            margin-bottom: 5px;
                        }
                        
                    </style>