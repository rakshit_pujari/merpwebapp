

<!-- Page header -->
<div class="page-header">

    <div ng-controller="noGroupDataTableController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Accounting</span> - Journal</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!journal/view" class="btn btn-link btn-float has-text"><i class="icon-book2 text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Add Journal</span></a>
                <a href="report/download/journal" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-calculator2 position-left"></i>Accounting</li>
            <li class="active"><i class="icon-book2 position-left"></i>Journal</li>
        </ul>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Journal archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Journal</h6>
            <div class="heading-elements">
                </ul>
            </div>
        </div>

        <table class="table">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Journal ID</th>
                    <th>Account Name</th>
                    <th>Sub Account</th>
                    <th>Debit</th>
                    <th>Credit</th>
                    <th>Description</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $current_journal_id = '';
                    foreach ($journals as $journal): ?>
                <tr>
                        <? if($journal['journal_id'] != $current_journal_id) {  ?>
                            <td rowspan="<? echo $journal['journal_row_count']; ?>">
                                <?php 
                                echo explode(" ", $journal['journal_date'])[0]; ?>
                            </td>
                        <? } else { ?>
                            <td style="display: none;"></td>
                        <? } ?>
                         
                        <? if($journal['journal_id'] != $current_journal_id) { ?>
                        <td rowspan="<? echo $journal['journal_row_count']; ?>">
                            <a href="#!journal/view/<?php echo $journal['journal_id']; ?>"><span>JRN<?php 
                            echo str_pad($journal['journal_id'], 5, "0", STR_PAD_LEFT); ?></span></a>
                        </td>
                        <? } else { ?>
                            <td style="display: none;"></td>
                        <? } ?>
                        <td>
                            <?
                            if(isset($journal['possible_parent_name'])){
                                echo //'<a href="#!COA/view/'.$journal['possible_parent_coa_id'].'">'
                                         '<span>'.$journal['possible_parent_name'].'</span>';
                                        //. '</a>';
                            } else {
                                echo //'<a href="#!COA/view/'.$journal['account_coa_id'].'">'
                                         '<span>'.$journal['account_name'].'</span>';
                                        //. '</a>';
                            }
                            ?>
                        </td>
                        <td>
                            <?
                            if(isset($journal['possible_parent_name'])){
                                echo //'<a href="#!COA/view/'.$journal['account_coa_id'].'">'
                                        '<span>'.$journal['account_name'].'</span>';
                                        //. '</a>';
                            } else {
                                echo "None";
                            }
                            ?>
                        </td>
                        <td>
                            <?
                            if($journal['je_transaction_type'] == 'debit'){
                                echo '₹ '. $journal['je_amount'];
                            }
                            ?>
                        </td>
                        <td>
                            <?
                            if($journal['je_transaction_type'] == 'credit'){
                                echo '₹ '.$journal['je_amount'];
                            }
                            ?>
                        </td>
                        <td>
                            <? echo $journal['je_description']; ?>
                        </td>
                        <? if($journal['journal_id'] != $current_journal_id) {  
                            $current_journal_id = $journal['journal_id']; ?>
                            <td class="text-center" rowspan="<? echo $journal['journal_row_count']; ?>">
                            <ul class="icons-list">
                                <li><a href="#!journal/view/<?php echo $journal['journal_id']; ?>"><i class="icon-file-eye"></i></a></li>
                                <? 
                                if($this->session->userdata('access_controller')->is_access_granted('journal', 'delete')) { ?>
                                <li><a href='javascript: void(0)' onclick = 'showDeleteDialog("journal", <?php echo $journal['journal_id']; ?>)'><i class="glyphicon glyphicon-trash"></i></a></li>
                                <? } ?>
                            </ul>
                            </td>
                        <? } else { ?>
                            <td style="display: none;"></td>
                        <? } ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th>Date</th>
                    <th>Journal ID</th>
                    <th>Account Name</th>
                    <th>Sub Account</th>
                    <th>Debit</th>
                    <th>Credit</th>
                    <th>Description</th>
                    <th class="text-center">Actions</th>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->