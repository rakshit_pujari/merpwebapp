

<!-- Page header -->
<div class="page-header">

    <div ng-controller="dataTableController" ng-init="load(2)"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Sales</span> - Receipt</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!Receipt/view" class="btn btn-link btn-float has-text"><i class="icon-credit-card2 text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Add Receipt</span></a>
                <a href="report/download/receipt" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-home2 position-left"></i>Sales</li>
            <li class="active"><i class="icon-city position-left"></i>Receipt</li>
        </ul>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Receipt archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Receipt</h6>
            <div class="heading-elements">
                </ul>
            </div>
        </div>

        <table class="table masterDataTable">
            <thead>
                <tr>
                    <th>Receipt no.</th>
                    <th>Customer Name</th>
                    <th>Date</th>
                    <th>Sales Invoice</th>
                    <th>Amount</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($receipts as $receipt): ?>
                    <tr>
                        <td>REC<?php echo str_pad($receipt['receipt_id'], 5, "0", STR_PAD_LEFT); ?></td>
                        <td>
                            <h6 class="no-margin">
                                <a href="#!Receipt/preview/<?php echo $receipt['receipt_id']; ?>"><span><?php echo $receipt['receipt_linked_company_invoice_name']; ?></span></a>
                                <small class="display-block text-muted">Created by <?php echo $receipt['employee_username']; ?> 
                                    on <?php echo $receipt['receipt_record_creation_time']; ?></small>
                            </h6>
                        </td>
                        <td><?php echo explode(" ", $receipt['receipt_date'])[0]; ?></td>
                        <td><a href="#!invoice/preview/confirm/<?php echo $receipt['receipt_linked_invoice_id']; ?>"><span>INV<?php echo str_pad($receipt['receipt_linked_invoice_id'], 5, "0", STR_PAD_LEFT); ?></span></a></td>
                        <td>₹ <?php echo number_format($receipt['receipt_amount'], 2); ?></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li><a href="#!Receipt/preview/<?php echo $receipt['receipt_id']; ?>"><i class="icon-file-eye"></i></a></li>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th>Receipt no.</th>
                    <th>Customer Name</th>
                    <th>Date</th>
                    <th>Sales Invoice</th>
                    <th>Amount</th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->