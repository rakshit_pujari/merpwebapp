

<!-- Page header -->
<div class="page-header">

    <div ng-controller="dataTableController" ng-init="load(5)"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Purchase</span> - Debit Note</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!DebitNote/view/new" class="btn btn-link btn-float has-text"><i class="icon-credit-card text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Add Debit Note</span></a>
                <a href="report/download/debit_note" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-home2 position-left"></i>Purchase</li>
            <li class="active"><i class="icon-city position-left"></i>Debit Note</li>
        </ul>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Debit Note archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Debit Note</h6>
            <div class="heading-elements">
                </ul>
            </div>
        </div>

        <table class="table masterDataTable">
            <thead>
                <tr>
                    <th>Debit Note ID</th>
                    <th>Customer Name</th>
                    <th>Invoice ID</th>
                    <th>Status</th>
                    <th>Amount</th>
                    <th>Date</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($debit_notes as $debit_note): ?>
                    <tr>
                        <td><? if ($debit_note['dn_status'] == 'confirm') { ?>
                                DBN<?php echo str_pad($debit_note['dn_id'], 5, "0", STR_PAD_LEFT); ?>
                            <? } else { ?>
                                DRAFT
                            <? } ?></td>
                        <td>
                            <h6 class="no-margin">
                                <? if ($debit_note['dn_status'] == 'confirm') { ?>
                                    <a href="#!DebitNote/preview/confirm/<? echo $debit_note['dn_id']; ?>">
                                    <? } else { ?>
                                        <a href="#!DebitNote/view/<? echo $debit_note['dn_status']; ?>/<? echo $debit_note['dn_id']; ?>">
                                        <? } ?><span><?php echo $debit_note['dn_linked_company_invoice_name']; ?></span></a>
                                <small class="display-block text-muted">Created by <?php echo $debit_note['employee_username']; ?> 
                                    on <?php echo $debit_note['dn_record_creation_time']; ?></small>
                            </h6>
                        </td>
                        <td><?php
                            if (isset($debit_note['dn_linked_invoice_id'])) {
                                $dn_linked_supplementary_invoice_id = $debit_note['dn_linked_invoice_id'];
                                $dn_linked_supplementary_invoice_id_text = 'INV' . str_pad($dn_linked_supplementary_invoice_id, 5, "0", STR_PAD_LEFT);
                            } else if (isset($debit_note['dn_linked_purchase_id'])) {
                                $dn_linked_supplementary_invoice_id = $debit_note['dn_linked_purchase_id'];
                                $dn_linked_supplementary_invoice_id_text = 'PUR' . str_pad($dn_linked_supplementary_invoice_id, 5, "0", STR_PAD_LEFT);
                            }

                            echo $dn_linked_supplementary_invoice_id_text;
                            ?>
                        </td>
 
                        <td><?php
                            if ($debit_note['dn_status'] == 'confirm')
                                echo '<span class="label bg-blue"><span>' . $debit_note['dn_status'] . '</span></span>';
                            else
                                echo '<span class="label bg-danger"><span>' . $debit_note['dn_status'] . '</span></span>';
                            ?></td>
                        <td><?php echo '₹ '.number_format($debit_note['dn_amount'], 2); ?></td>
                        <td><?php echo explode(" ", $debit_note['dn_date'])[0]; ?></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li><? if ($debit_note['dn_status'] == 'confirm') { ?>
                                        <a href="#!DebitNote/preview/confirm/<? echo $debit_note['dn_id']; ?>">
                                        <? } else { ?>
                                            <a href="#!DebitNote/view/<? echo $debit_note['dn_status']; ?>/<? echo $debit_note['dn_id']; ?>">
                                            <? } ?><i class="icon-file-eye"></i></a></li>
                                        <? if ($this->session->userdata('access_controller')->is_access_granted('debit_note', 'delete')
                                                    && $debit_note['dn_status'] == 'draft') { 
                                                ?>
                                                <li><a href='javascript: void(0)' onclick = 'showDeleteDialog("DebitNote", <?php echo $debit_note['dn_id']; ?>)'><i class="glyphicon glyphicon-trash"></i></a></li>
                                            <? } ?>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td>Debit Note ID</td>
                    <td>Customer Name</td>
                    <th>Invoice Number</th>
                    <th>Status</th>
                    <td>Amount</td>
                    <td>Date</td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->