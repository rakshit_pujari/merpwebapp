<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Quant</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>assets/css/core.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>assets/css/components.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>assets/css/colors.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/loaders/pace.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/core/libraries/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/core/libraries/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/loaders/blockui.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/plugins/forms/styling/uniform.min.js"></script>

        <script type="text/javascript" src="<?php echo base_url() ?>assets/js/core/app.js"></script>
        <!--<script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/login.js"></script>-->
        <!-- /theme JS files -->
        <link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/images/logo_icon_light.png"/>
       
    </head>

    <body class="login-container" style="background-image: url('<?php echo base_url() ?>assets/images/230.jpg'); background-size: cover">
        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Content area -->
                    <div class="content pb-20">

                        <!-- Advanced login -->
                        <?php echo validation_errors(); ?>
                        <?php
                        $attributes = array('class' => 'form-horizontal form-validate-jquery');
                        echo form_open('web/login/authentication', $attributes);
                        ?>
                        <div class="panel panel-body login-form" style="opacity: 0.85">
                            <div class="text-center">
                                <!--<div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>-->
                                <div class="border-slate-300 text-slate-300"><img src = "<?php echo base_url() ?>assets/images/logo_icon_light.png" ><!--<i class="icon-reading"></i>--></div>
                                <h5 class="content-group-lg">Login to Quant<small class="display-block">Enter your credentials</small></h5>
                            </div>

                            <div class="form-group has-feedback has-feedback-left">
                                <input type="text" name = "username" class="form-control" placeholder="Username" required>
                                <div class="form-control-feedback">
                                    <i class="icon-user text-muted"></i>
                                </div>
                            </div>

                            <div class="form-group has-feedback has-feedback-left">
                                <input type="password" name ="password" class="form-control" placeholder="Password"required>
                                <div class="form-control-feedback">
                                    <i class="icon-lock2 text-muted"></i>
                                </div>
                            </div>

                            <!--<div class="form-group login-options">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" class="styled" checked="checked">
                                            Remember
                                        </label>
                                    </div>
                                </div>
                            </div>-->
                            <? if($this->session->flashdata('error_notification')!=NULL)
                            {?>
                            <div class="form-group">
                                <b><span id = "userNotification" style = "color:#FF0000; font-size:12px;"><?php echo $this->session->flashdata('error_notification'); ?><br/></span></b>
                            </div>
                            <? } ?>
                            <div class="form-group">
                                <button type="submit" class="btn bg-blue btn-block">Login <i class="icon-arrow-right14 position-right"></i></button>
                            </div>
                            <span class="help-block text-center no-margin">By continuing, you're confirming that you've read our <a href="#">Terms &amp; Conditions</a> and <a href="#">Cookie Policy</a></span>
                        </div>
                        </form>
                        <!-- /advanced login -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>
</html>
