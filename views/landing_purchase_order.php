

<!-- Page header -->
<div class="page-header">

    <div ng-controller="dataTableController" ng-init="load(2)"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Purchase </span> - Purchase Order</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!PurchaseOrder/view/new" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-list-alt  position-left" style = "font-size:22px;color:#26A69A !important"></i> <span>Add Purchase Order</span></a>
                <a href="report/download/purchase_order" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-home2 position-left"></i>Purchase</li>
            <li class="active"><i class="icon-city position-left"></i>Purchase Order</li>
        </ul>
    </div> 
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Purchase Order archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Purchase Order</h6>
            <div class="heading-elements">
                </ul>
            </div>
        </div>

        <table class="table masterDataTable">
            <thead>
                <tr>
                    <th>Purchase Order ID</th>
                    <th>Vendor Name</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($purchase_orders as $purchase_order): ?>
                    <tr>
                        <td>
                            <? if ($purchase_order['purchase_order_status'] == 'confirm') { ?>
                                POR<?php echo str_pad($purchase_order['purchase_order_id'], 5, "0", STR_PAD_LEFT); ?>
                            <? } else { ?>
                                DRAFT
                            <? } ?>
                        </td>
                        <td>
                            <h6 class="no-margin">
                                <? if ($purchase_order['purchase_order_status'] == 'confirm') { ?>
                                    <a href="#!PurchaseOrder/preview/confirm/<? echo $purchase_order['purchase_order_id']; ?>">
                                    <? } else { ?>
                                        <a href="#!PurchaseOrder/view/<? echo $purchase_order['purchase_order_status']; ?>/<? echo $purchase_order['purchase_order_id']; ?>">
                                        <? } ?><span><?php echo $purchase_order['purchase_order_linked_company_display_name']; ?></span></a>
                                    <small class="display-block text-muted">Created by <?php echo $purchase_order['employee_username']; ?> 
                                        on <?php echo $purchase_order['purchase_order_record_creation_time']; ?></small>
                            </h6>
                        </td>
                        <td><?php echo explode(" ", $purchase_order['purchase_order_date'])[0]; ?></td>
                        <td><?php
                            if ($purchase_order['purchase_order_status'] == 'confirm')
                                echo '<span class="label bg-blue"><span>' . $purchase_order['purchase_order_status'] . '</span></span>';
                            else
                                echo '<span class="label bg-danger"><span>' . $purchase_order['purchase_order_status'] . '</span></span>';
                            ?></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li><? if ($purchase_order['purchase_order_status'] == 'confirm') { ?>
                                        <a href="#!PurchaseOrder/preview/confirm/<? echo $purchase_order['purchase_order_id']; ?>">
                                        <? } else { ?>
                                            <a href="#!PurchaseOrder/view/<? echo $purchase_order['purchase_order_status']; ?>/<? echo $purchase_order['purchase_order_id']; ?>">
                                            <? } ?>
                                            <i class="icon-file-eye"></i></a></li>
                                            <? if ($this->session->userdata('access_controller')->is_access_granted('purchase_order', 'delete')
                                                && $purchase_order['purchase_order_status'] == 'draft') { 
                                            ?>
                                            <li><a href='javascript: void(0)' onclick = 'showDeleteDialog("PurchaseOrder", <?php echo $purchase_order['purchase_order_id']; ?>)'><i class="glyphicon glyphicon-trash"></i></a></li>
                                        <? } ?>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td>Purchase Order ID</td>
                    <td>Date</td>
                    <td>Vendor Name</td>
                    <th>Status</th>
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->