

<!-- Page header -->
<div class="page-header">

    <div ng-controller="noGroupDataTableController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Accounting</span> - Profit & Loss Statement</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-calculator2 position-left"></i>Accounting</li>
            <li class="active"><i class="icon-table position-left"></i>Profit & Loss Statement</li>
        </ul>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Ledgers archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Profit & Loss Statement</h6>
            <div class="heading-elements">
                </ul>
            </div>
        </div>

        <table class="table">
            <thead>
                <tr>
                    <th></th>
                    <th>Account Name</th>
                    <th>Account Type</th>
                    <th>Amount</th>
                </tr>
            </thead>
            <tbody>
                <?
                $total = 0;
                $revenue_total = 0;
                $expense_total = 0;
                $tax_total = 0;
                ?>
                <tr>
                    <th  style="width:130px">I. Revenue</th>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <?php
                foreach ($ledger_entries as $ledger_entry):
                    if ($ledger_entry['at_name'] == 'Revenue')
                        add_row($ledger_entry, 'credit', $revenue_total, $total);
                endforeach;
                ?>

                <tr>
                    <td></td>
                    <th>Total Revenue</th>
                    <td></td>
                    <th><? echo '₹ ' .number_format(abs($revenue_total), 2); ?></th>
                </tr>
                
                <tr>
                    <th>II. Expenses</th>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <?php
                foreach ($ledger_entries as $ledger_entry):
                    if ($ledger_entry['at_name'] == 'Expense') {
                        if ($ledger_entry['sj_account_coa_id'] != $corporate_tax['debit_account_coa_id'])
                            add_row($ledger_entry, 'debit', $expense_total, $total);
                    }
                endforeach;
                ?>
                
                <tr>
                    <td></td>
                    <th>Total Expense</th>
                    <td></td>
                    <th><? echo '₹ ' .number_format(abs($expense_total), 2); ?></th> 
                </tr>
                
                <tr>
                    <th>III. Profit Before Tax ( I - II )</th>
                    <td></td>
                    <td></td>
                    <th><span style="color:<?
                        if ($total > 0)
                            echo '#00AA00';
                        else
                            echo '#FF0000';
                        ?>"><? 
                        if (($revenue_total + $expense_total) < 0)
                            echo '( ';
                        echo '₹ ' .number_format(abs($revenue_total + $expense_total), 2); 
                        if (($revenue_total + $expense_total) < 0)
                            echo ' )'; ?></span></th>
                </tr>
                <?php
                foreach ($ledger_entries as $ledger_entry):
                    if ($ledger_entry['at_name'] == 'Expense') {
                        if ($ledger_entry['sj_account_coa_id'] == $corporate_tax['debit_account_coa_id'])
                            add_row($ledger_entry, 'debit', $tax_total, $total);
                    }
                endforeach;
                ?>
            </tbody>
            <tfoot>
                <tr>
                    <th width="15%"></th>
                    <th></th>
                    <th><?
                        if ($total >= 0)
                            echo 'Net Profit';
                        else 
                            echo 'Net Loss';
                    ?></th>
                    <th><span style="color:<?
                        if ($total >= 0)
                            echo '#00AA00';
                        else
                            echo '#FF0000';
                        ?>"><? 
                        if ($total < 0)
                            echo '( ';
                        echo '₹ ' . number_format(abs($total), 2); 
                        if ($total < 0)
                            echo ' )';?></span></th>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->

    <?

    function add_row($ledger_entry, $account_type, &$section_total, &$total) {
        if($ledger_entry['credit_balance'] > 0 || $ledger_entry['debit_balance'] > 0){
        $amount = $ledger_entry['credit_balance'] - $ledger_entry['debit_balance'];
        $total+=$amount;
        $section_total = $section_total + $amount;
        ?>
        <tr style="border: none;border: 0px #CCC;border-collapse: collapse;">
            <td></td>
            <td>
                <?
                if (($account_type == 'credit' && $amount < 0) || ($account_type == 'debit' && $amount > 0))
                    echo 'Less :';
                ?>
                <a href="#!SJ/ledger/<?php echo $ledger_entry['sj_account_coa_id']; ?>"><span>
                        <?
                        echo $ledger_entry['coa_account_name'];
                        ?>
                    </span></a>

            </td>
            <td>
                <?
                echo $ledger_entry['at_name'];
                ?>

            </td>
            <td>
                <?
                echo '₹ ' . number_format(abs($ledger_entry['credit_balance'] - $ledger_entry['debit_balance']), 2);
                ?>
            </td>
        </tr>
        <?
        }
    }
    ?>