

<!-- Page header -->
<div class="page-header">

    <div ng-controller="dataTableController" ng-init="load(2)"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Sales</span> - Invoice</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!invoice/view/new" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-list-alt  position-left" style = "font-size:22px;color:#26A69A !important"></i> <span>Add Invoice</span></a>
                <a href="report/download/sales_invoice" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="fa fa-inr" style = "font-size:12px!important"></i> Sales</li>
            <li class="active"><i class="glyphicon glyphicon-list-alt position-left"></i>Sales Invoice</li>
        </ul>
    </div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">

    <!-- Invoice archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Sales Invoice</h6>
            <div class="heading-elements">

            </div>
        </div>

        <table class="table masterDataTable">
            <thead>
                <tr>
                    <th>Invoice no.</th>
                    <th>Customer Name</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th>Payment Status</th>
                    <th>Amount</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($invoices as $invoice): ?>
                    <tr>
                        <td>
                            <? if ($invoice['invoice_status'] == 'confirm') { ?>
                                INV<?php echo str_pad($invoice['invoice_id'], 5, "0", STR_PAD_LEFT); ?>
                            <? } else { ?>
                                DRAFT
                            <? } ?>
                        </td>
                        <td>
                            <h6 class="no-margin">
                                <? if ($invoice['invoice_status'] == 'confirm') { ?>
                                    <a href="#!invoice/preview/confirm/<? echo $invoice['invoice_id']; ?>">
                                    <? } else { ?>
                                        <a href="#!invoice/view/<? echo $invoice['invoice_status']; ?>/<? echo $invoice['invoice_id']; ?>">
                                        <? } ?>
                                        <span><?php echo $invoice['invoice_linked_company_display_name']; ?></span>
                                    </a>
                                    <small class="display-block text-muted">Created by <?php echo $invoice['employee_name']; ?> 
                                        on <?php echo $invoice['invoice_record_creation_time']; ?></small>
                            </h6>
                        </td>
                        <td><?php echo explode(" ", $invoice['invoice_date'])[0]; ?></td>
                        <td>
                            <?php
                            if ($invoice['invoice_status'] == 'confirm')
                                echo '<span class="label bg-blue"><span>' . $invoice['invoice_status'] . '</span></span>';
                            else
                                echo '<span class="label bg-danger"><span>' . $invoice['invoice_status'] . '</span></span>';
                            ?>

                        </td>
                        <td><?php
                            if (isset($invoice['receipt_balance'])) {
                                if ($invoice['receipt_balance'] <= 0)
                                    echo '<span class="label bg-success"><span>PAID</span></span>';
                                else if ($invoice['receipt_balance'] >= $invoice['invoice_amount'])
                                    echo '<span class="label bg-danger"><span>UNPAID</span></span>';
                                //else if ($invoice['receipt_balance'] < 0)
                                  //  echo '<span class="label bg-success"><span>₹ ' . number_format(abs($invoice['receipt_balance'])) . ' OVERPAID</span></span>';
                                else
                                    echo '<span class="label bg-orange"><span>₹ ' . number_format($invoice['receipt_balance']) . ' PENDING</span></span>';
                            }
                            ?></td>
                        <td>₹ <?php echo number_format($invoice['invoice_amount'], 2); ?></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li><? if ($invoice['invoice_status'] == 'confirm') { ?>
                                        <a href="#!invoice/preview/confirm/<? echo $invoice['invoice_id']; ?>">
                                        <? } else { ?>
                                            <a href="#!invoice/view/<? echo $invoice['invoice_status']; ?>/<? echo $invoice['invoice_id']; ?>">
                                            <? } ?>
                                            <i class="icon-file-eye"></i></a>
                                            <? if ($this->session->userdata('access_controller')->is_access_granted('invoice', 'delete')
                                                    && $invoice['invoice_status'] == 'draft') { 
                                                ?>
                                                <li><a href='javascript: void(0)' onclick = 'showDeleteDialog("invoice", <?php echo $invoice['invoice_id']; ?>)'><i class="glyphicon glyphicon-trash"></i></a></li>
                                            <? } ?>
                                </li>
                            </ul>
                        </td>
                    </tr>
            <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td>Invoice no.</td>
                    <td>Date</td>
                    <td>Customer Name</td>
                    <th>Status</th>
                    <th>Payment Status</th>
                    <td>Amount</td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->