<?php

function getIndianCurrency($number) {
    $decimal = round($number - ($no = floor($number)), 2) * 100;
    $hundred = null;
    $digits_length = strlen($no);
    $i = 0;
    $str = array();
    $words = array(0 => '', 1 => 'one', 2 => 'two',
        3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
        7 => 'seven', 8 => 'eight', 9 => 'nine',
        10 => 'ten', 11 => 'eleven', 12 => 'twelve',
        13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
        16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
        19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
        40 => 'forty', 50 => 'fifty', 60 => 'sixty',
        70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
    $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
    while ($i < $digits_length) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number] . ' ' . $digits[$counter] . $plural . ' ' . $hundred : $words[floor($number / 10) * 10] . ' ' . $words[$number % 10] . ' ' . $digits[$counter] . $plural . ' ' . $hundred;
        } else
            $str[] = null;
    }
    $Rupees = implode('', array_reverse($str));
    //$paise = ($decimal) ? " and " . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    $paise = '';
    if ($decimal < 100) {
        $paise = ($decimal) ? " and " . ($words[((int) ($decimal / 10)) * 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    }
    return ($Rupees ? $Rupees . ' ' : '') . $paise;
}
?>

<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/purchase/all"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Purchase</span> - Purchase Invoice</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                    <!--<a href="#!/purchase/all" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>-->
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-city position-left"></i> Purchase</li>
            <li><a href="#!/purchase/all"><i class="icon-city position-left"></i> Purchase Invoice</a></li>
            <? if ($purchase['purchase_status'] == 'confirm') { ?>
                <li class="active"><?php if (isset($purchase)) echo 'PUR' . str_pad($purchase['purchase_id'], 5, "0", STR_PAD_LEFT); ?></li>
            <? } else { ?>
                <li class="active"><?php if (isset($purchase)) echo 'DRAFT'; ?></li>
            <? } ?>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <a href="#!/purchase/all"><button class="btn btn-default">Back<i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
    <a href="#!/purchase/view/<? echo $purchase['purchase_status'] . '/' . $purchase['purchase_id']; ?>"><button class="btn btn-default" 
                                                                                                             <? if (isset($linked_documents)) if (sizeof($linked_documents) > 0) echo "disabled title = 'Cannot be edited because this invoice is linked to other documents'" ?>>Edit <i class="icon-pencil3 position-right"></i></button></a>

    <button onClick="window.print();" class="btn btn-primary">Print <i class="icon-arrow-right14 position-right"></i></button>
    <!-- Form validation -->
    <div class="panel-flat">
        <div class="panel panel-body col-md-offset-3"  id = "section-to-print" style="width: 210mm;min-height: 297mm;padding-left:40px;padding-right:40px;">

            <span id = "purchaseHeader">Purchase Invoice</span>
            <hr>

            <table class = "purchaseTable">
                <tr>
                    <td class = "logo_container" colspan = "2" rowspan = "2">
                        <img class = "logo" src="<? echo $purchase['purchase_oc_logo_path']; ?>"></img>
                    </td>
                    <td style="vertical-align: top">
                        <span class = "purchaseField">Purchase date</span>												
                        <br/>
                        <span class = "purchaseFieldValue"><? echo explode(" ", $purchase['purchase_date'])[0]; ?></span>
                    </td>
                    <td style="vertical-align: top">
                        <span class = "purchaseField">Payment Terms</span>												
                        <br/>
                        <span class = "purchaseFieldValue"><? echo $purchase['purchase_linked_company_payment_term_name']; ?></span>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top">
                        <span class = "purchaseField">Purchase No.</span>												
                        <br/>
                        <? if ($purchase['purchase_status'] == 'confirm') { ?>
                            <span class = "purchaseFieldValue">PUR<? echo str_pad($purchase['purchase_id'], 5, "0", STR_PAD_LEFT); ?></span>
                        <? } else { ?>
                            <span class = "purchaseFieldValue">DRAFT</span>
                        <? } ?>

                    </td>
                    <td style="vertical-align: top">
                        <span class = "purchaseField">Place Of Supply</span>												
                        <br/>
                        <span class = "purchaseFieldValue"><? echo $purchase['purchase_linked_company_gst_supply_state_id'].'-'.$purchase['purchase_place_of_supply']; ?></span>
                    </td>
                </tr>
                <tr>
                    <td colspan = "2" rowspan = "2">
                        <span class = "purchaseFieldValue"><? echo $purchase['purchase_oc_name']; ?></span><br/>
                        <span class = "purchaseField">
                            <? echo $purchase['purchase_oc_address']; ?><br/>
                            GSTIN: <? echo $purchase['purchase_oc_gstin']; ?><br/>
                            PAN: <? echo $purchase['purchase_oc_pan_number']; ?>

                        </span>
                    </td>
                    <td style="vertical-align: top">
                        <span class = "purchaseField">Vendor Bill Number</span>												
                        <br/>
                        <span class = "purchaseFieldValue"><? echo $purchase['purchase_bill_reference']; ?></span>
                    </td>
                    <td style="vertical-align: top">
                        <span class = "purchaseField">Due Date</span>												
                        <br/>
                        <span class = "purchaseFieldValue"><? echo explode(" ", $purchase['purchase_due_date'])[0]; ?></span>
                    </td>
                </tr>
                <tr>

                    <td style="vertical-align: top">
                        <span class = "purchaseField">Vendor Bill Date</span>												
                        <br/>
                        <span class = "purchaseFieldValue"><? echo explode(" ", $purchase['purchase_bill_date'])[0]; ?></span>
                    </td>
                </tr>
            </table>
            <table class = "purchaseTable gray-border">
                <tr>
                    <td>Vendor Billing Details</td>
                </tr>
                <tr>
                    <td>
                        Name:<span class = "purchaseFieldValue"><? echo $purchase['purchase_linked_company_invoice_name']; ?></span><br/>
                        Address:<? echo $purchase['purchase_linked_company_billing_address']; ?><br/>
                        State:<? echo $purchase['purchase_linked_company_billing_state_name']; ?><br/>
                        <? if (!empty($purchase['purchase_billing_contact_person_name'])) { ?>
                        Contact Person:<? echo $purchase['purchase_billing_contact_person_name']; ?><br/>
                        <? } ?>
                        <? if (!empty($purchase['purchase_billing_contact_number'])) { ?>
                        Contact Details:<? echo $purchase['purchase_billing_contact_number']; ?><br/>
                        <? } ?>
                        GST#:<? echo strtoupper($purchase['purchase_linked_company_gstin']); ?><br/>
                        PAN#:<? echo $purchase['purchase_linked_company_pan_number']; ?><br/>
                    </td>
                </tr>
            </table>
            <table style = "table-layout:fixed; width:160%;">
                <td>
                    <span>Reverse charge applicable : <?
                        if ($purchase['purchase_is_reverse_charge_applicable'] == "1")
                            echo "Y";
                        else
                            echo "N";
                        ?>
                    </span>
                </td>
                <td>
                    <span>Tax Type : <? echo ucwords($purchase['purchase_tax_type']); ?></span>
                </td>
            </table>
            <hr>
            <table class = "productList purchaseTable">
                <thead class = "grayTBack">
                    <tr>
                        <td width='25px'>Sr</td>
                        <td width='120px'>Item Description</td>
                        <td>HSN / SAC code</td>

                        <td>Rate / Item</td>
                        <?
                        if ($purchase['purchase_tax_type'] == 'inclusive') {
                            ?>
                            <td>Rate / Item (Excl. Tax)</td>
                            <?
                        }
                        ?>
                        <td class = "discount">Discount / Item</td>
                        <td>Qty</td>
                        <td width='90px'>Taxable Value</td>
                        <?
                        if ($purchase['purchase_linked_company_billing_state_id'] == $purchase['purchase_linked_company_gst_supply_state_id']) {
                            echo "<td>SGST</td><td>CGST</td>";
                        } else {
                            echo "<td>IGST</td>";
                        }
                        ?>
                        <td width="90px">Total</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $total_discount = 0;
                    $total_taxable_amount = 0;
                    $total_tax = 0;
                    $tax_amount_map = array();
                    foreach ($purchase_entries as $purchase_entry) {
                        ?>
                        <tr>
                            <td><? echo $purchase_entry['pe_entry_id']; ?></td>

                            <td><? echo $purchase_entry['pe_product_name']; ?></td>		

                            <td><? echo $purchase_entry['pe_product_hsn_code']; ?></td>

                            <td><? echo number_format($purchase_entry['pe_product_rate'], 2); ?></td>
                            <?
                            if ($purchase['purchase_tax_type'] == 'inclusive') {
                                ?>
                                <td><? echo number_format(( $purchase_entry['pe_product_rate'] ) / ( 1 + $purchase_entry['pe_tax_percent_applicable'] / 100), 2) ?></td>
                                <?
                            }
                            ?>

                            <td class = "discount"><?
                                $discount = $purchase_entry['pe_discount'] * $purchase_entry['pe_product_rate'] / 100;
                                if ($purchase['purchase_tax_type'] == 'inclusive') {
                                    $discount = $discount / ( 1 + $purchase_entry['pe_tax_percent_applicable'] / 100);
                                }
                                $total_discount = $total_discount + ( $discount * $purchase_entry ['pe_product_quantity'] );
                                echo number_format($discount, 2);
                                ?></td>
                            <td><? echo number_format($purchase_entry['pe_product_quantity'], 2) . ' ' . $purchase_entry['pe_product_uqc_text']; ?></td>						  

                            <td>
                                <?
                                if ($purchase['purchase_tax_type'] == 'inclusive') {
                                    $taxable_amount = $purchase_entry['pe_product_quantity'] * ((( $purchase_entry['pe_product_rate'] ) / ( 1 + $purchase_entry['pe_tax_percent_applicable'] / 100)) - $discount);
                                } else if ($purchase['purchase_tax_type'] == 'exclusive') {
                                    $taxable_amount = $purchase_entry['pe_product_quantity'] * ( $purchase_entry['pe_product_rate'] - $discount );
                                }
                                $total_taxable_amount = $total_taxable_amount + $taxable_amount;
                                echo number_format($taxable_amount, 2);
                                ?>
                            </td>
                            <?
                            $tax = ( $taxable_amount * $purchase_entry['pe_tax_percent_applicable'] / 100 ); // default to exclusive tax
                            //if($purchase['purchase_tax_type'] == 'inclusive'){
                            //taxAmount = (parseFloat(tax) * parseFloat(amount) ) / ( 100 + parseFloat(tax) );
                            //$tax = ($taxable_amount * $purchase_entry['pe_tax_percent_applicable']) / ( 100 + $purchase_entry['pe_tax_percent_applicable']);
                            //}
                            $total_tax = $total_tax + $tax;

                            $pe_tax_percent_applicable = $purchase_entry['pe_tax_percent_applicable'];
                            if (!isset($tax_amount_map[$pe_tax_percent_applicable])) {
                                $tax_amount_map[$pe_tax_percent_applicable] = $tax;
                            } else {
                                $stored_tax = $tax_amount_map[$pe_tax_percent_applicable];
                                $tax_amount_map[$pe_tax_percent_applicable] = $stored_tax + $tax;
                            }

                            if ($purchase['purchase_linked_company_billing_state_id'] == $purchase['purchase_linked_company_gst_supply_state_id']) {
                                $cgst = $tax / 2;
                                $sgst = $tax / 2;
                                echo "<td>" . number_format((float) $sgst, 2) . "</td><td>" . number_format((float) $cgst, 2) . "</td>";
                            } else {
                                echo "<td>" . number_format((float) $tax, 2) . "</td>";
                            }
                            ?>
                            <td>
                                <? echo number_format((float) ($taxable_amount + $tax), 2) ?>
                            </td>

                        </tr>

                        <?php
                    }
                    ?>
                    <?
                    
                    foreach ($purchase_charge_entries as $purchase_charge_entry) {
                        ?>
                        <tr>
                            <?

                            if ($purchase['purchase_tax_type'] == 'inclusive') {
                                ?>
                                <td></td>
                                <?
                            }
                            ?> 
                            <td class = "discount"></td>
                            
                            <td colspan = "5" align="right"><span style = "font-style: italic"><? echo $purchase_charge_entry['pce_charge_name']; ?></span></td>

                            <td>
                                <?
                                //$taxable_amount = $purchase_charge_entry['pce_taxable_amount'];
                                if ($purchase['purchase_tax_type'] == 'inclusive') {
                                    $taxable_amount = ($purchase_charge_entry['pce_taxable_amount'] ) / ( 1 + $purchase_charge_entry['pce_tax_percent_applicable'] / 100);
                                } else if ($purchase['purchase_tax_type'] == 'exclusive') {
                                    $taxable_amount = $purchase_charge_entry['pce_taxable_amount'];
                                }
                                $total_taxable_amount = $total_taxable_amount + $taxable_amount;
                                echo number_format((float) $taxable_amount, 2);
                                ?>
                            </td> 
                            <?
                            $tax = ( $taxable_amount * $purchase_charge_entry['pce_tax_percent_applicable'] / 100 ); // default to exclusive tax
                            //if($purchase['purchase_tax_type'] == 'inclusive'){
                            //$tax = $purchase_charge_entry['pce_product_quantity'] * ( $purchase_charge_entry['pce_product_rate'] - $discount ) * ( $purchase_charge_entry['pce_tax_percent_applicable']/100); 
                            //}
                            $total_tax = $total_tax + $tax;

                            $pce_tax_percent_applicable = $purchase_charge_entry['pce_tax_percent_applicable'];
                            if (!isset($tax_amount_map[$pce_tax_percent_applicable])) {
                                $tax_amount_map[$pce_tax_percent_applicable] = $tax;
                            } else {
                                $stored_tax = $tax_amount_map[$pce_tax_percent_applicable];
                                $tax_amount_map[$pce_tax_percent_applicable] = $stored_tax + $tax;
                            }

                            if ($purchase['purchase_oc_gst_supply_state_id'] == $purchase['purchase_linked_company_gst_supply_state_id']) {
                                $cgst = $tax / 2;
                                $sgst = $tax / 2;
                                echo "<td>" . number_format((float) $sgst, 2) . "</td><td>" . number_format((float) $cgst, 2) . "</td>";
                            } else {
                                echo "<td>" . number_format((float) $tax, 2) . "</td>";
                            }
                            ?>
                            <td>
                                <?
                                //if($purchase['purchase_tax_type'] == 'inclusive'){
                                  //  echo number_format((float) ($taxable_amount), 2);
                                //} else if ($purchase['purchase_tax_type'] == 'exclusive'){
                                    echo number_format((float) ($taxable_amount + $tax), 2);
                                //}
                                ?>
                            </td>

                        </tr>

                        <?php
                    }
                    ?>
                    
                </tbody>
                <tfoot class = "grayTBack">
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <?
                        if ($purchase['purchase_tax_type'] == 'inclusive') {
                            ?>
                            <td></td>
                            <?
                        }
                        ?>
                        <td class = "discount"></td>
                        <td>Total</td>
                        <td>₹ <? echo number_format($total_taxable_amount, 2); ?></td>
                        <?
                        if ($purchase['purchase_linked_company_billing_state_id'] == $purchase['purchase_linked_company_gst_supply_state_id']) {
                            echo "<td>₹ " . number_format((float) ($total_tax / 2), 2) . "</td>
                                  <td>₹ " . number_format((float) ($total_tax / 2), 2) . "</td>";
                        } else {
                            echo "<td>₹ " . number_format((float) ($total_tax), 2) . "</td>";
                        }
                        ?>
                        <td>₹ <? echo number_format((float) ($total_taxable_amount + $total_tax), 2) ?></td>
                    </tr>
                </tfoot>
            </table>

            <table class = "purchaseTable">
                <tr class = "discount">
                    <td colspan = "6" rowspan = "2" width='470px'></td>
                    <td colspan = "2">Discount Amount</td>
                    <td>₹    <? echo number_format($total_discount, 2); ?></td>
                </tr>
                <tr>
                    <?
                    if($total_discount == 0){
                        ?>
                        <td colspan = "6"></td>    
                            <?
                    }
                    ?>
                    <td colspan = "2">Taxable Amount</td>
                    <td>₹    <? echo number_format($total_taxable_amount, 2); ?></td>
                </tr>


                <?
                foreach ($tax_amount_map as $tax_percent => $tax_amount) {

                    if ($purchase ['purchase_linked_company_billing_state_id'] == $purchase['purchase_linked_company_gst_supply_state_id']) {
                        echo "<tr>" .
                        "<td colspan = '6'></td>" .
                        "<td colspan = '2'>SGST@" . ($tax_percent / 2) . "%</td>" .
                        "<td>₹ " . number_format((float) ( $tax_amount / 2), 2) . "</td>" . "</tr>" .
                        "<tr>" .
                        "<td colspan = '6'></td>" .
                        "<td colspan = '2'>CGST@" . ($tax_percent / 2) . "%</td>" .
                        "<td>₹ " . number_format((float) ( $tax_amount / 2), 2) . "</td>" . "</tr>";
                    } else {
                        echo "<tr>" .
                        "<td colspan = '6'></td>" .
                        "<td colspan = '2'>IGST@" . $tax_percent . "%</td>" .
                        "<td>₹ " . number_format((float) ( $tax_amount), 2) . "</td>" .
                        "</tr>";
                    }
                }
                ?>

                <tr style = "display:none">
                    <td colspan = "6"></td>
                    <td colspan = "2">Transportation Charges</td>
                    <td>₹ <?
                        $purchase_transport_charges = 0;
                        if ($purchase['purchase_transport_charges'] != '')
                            $purchase_transport_charges = $purchase[
                                    'purchase_transport_charges'];

                        echo $purchase_transport_charges;
                        ?>
                    </td>
                </tr>

                
                <?
                $purchase_adjustments = 0;
                if ($purchase['purchase_adjustments'] != ''){
                    $purchase_adjustments = $purchase['purchase_adjustments'];
                }

                ?>
                <? if($purchase_adjustments > 0){ ?>
                <tr>
                    <td colspan = "6" style = "text-align:left;">Total Purchase Amount (in words)</td>
                    <td colspan = "2">Adjustments</td>
                    <td>₹ <?
                        
                        echo $purchase_adjustments;
                        ?></td>
                </tr>
                <? } ?>
                <?
                    $total_purchase_amount = $total_taxable_amount + $total_tax + $purchase_adjustments + $purchase_transport_charges;
                ?>
                <tr class = "grayTBack">
                    <td colspan = "6" style = "text-align:left;">
                        <span style = "font-weight:500;">Rupees</span> <? echo ucwords(getIndianCurrency(round(($total_purchase_amount), 2))); ?> <span style = "font-weight:500;">Only</span>
                    </td>
                    <td colspan = "2">Total Purchase Amount</td>
                    <td>₹    <?
                        echo number_format(($total_purchase_amount), 2);
                        ?>   </td>
                </tr>
                <tr>
                    <td colspan = "6" style = "text-align:left;">
                        <br/><br/>
                        <textarea class = "termsText" readonly>Purchase Terms : &#13;&#10;<? echo $purchase['purchase_terms']; ?><? echo $purchase['purchase_additional_terms']; ?></textarea>
                    </td>
                    <td colspan = "3">Authorized Signatory<br/><br/><br/><br/><span style = "font-weight:800;"><? echo $purchase['purchase_oc_name']; ?></span></td>
                </tr>
            </table>
            <footer style = "position:absolute;bottom:0;right:0;padding-right:20px;padding-bottom:15px;font-size:11px;font-style:italic;">
                Powered By Quant ERP
            </footer>
            <? if (isset($payment_balance)) { ?>
                <div class="ribbon-container">
                    <? if ($payment_balance <= 0) { ?>
                        <div class="ribbon bg-success-400">PAID</div>
                    <? } else if ($payment_balance >= ($total_purchase_amount - 0.001)) { // cannot compare int to double. So 20000.00 >= 20000 returns false. Hence subtracting a small amount. ?>
                        <div class="ribbon bg-danger">UNPAID</div>
                    <? } else { ?>
                        <div class="ribbon bg-orange">PENDING</div>
                    <? }
                    ?>
                </div>
            <? } ?>
        </div>

        <? if ($purchase['purchase_image_path'] != '') { ?>
            <div class="breadcrumb-line breadcrumb-line-component">
                <ul class="breadcrumb">
                    <li>Uploaded Bill</li>
                    <li><a href = '<? echo $purchase['purchase_image_path']; ?>'><? echo $purchase['purchase_bill_reference'] != '' ? $purchase['purchase_bill_reference'] : "View"; ?></a></li>
                </ul>
            </div><br/>
        <? } ?>

        <? if (isset($linked_documents)) { ?>
            <div class="breadcrumb-line breadcrumb-line-component">
                <ul class="breadcrumb">
                    <li>Linked Documents</li>
                    <?
                    foreach ($linked_documents as $linked_document) {
                        $prefix = '';
                        $link = '';
                        if ($linked_document['document_type'] == 'credit_note') {
                            $prefix = 'CDN';
                            $link = '#!/CreditNote/preview/confirm/' . $linked_document['document_id'];
                        } else if ($linked_document['document_type'] == 'debit_note') {
                            $prefix = 'DBN';
                            $link = '#!/DebitNote/preview/confirm/' . $linked_document['document_id'];
                        } else if ($linked_document['document_type'] == 'payment') {
                            $prefix = 'PAY';
                            $link = '#!/Payment/preview/' . $linked_document['document_id'];
                        } else if ($linked_document['document_type'] == 'receipt') {
                            $prefix = 'REC';
                            $link = '#!/receipt/preview/' . $linked_document['document_id'];
                        } else if ($linked_document['document_type'] == 'advance_payment') {
                            $prefix = 'ADP';
                            $link = '#!/AdvancePayment/preview/' . $linked_document['document_id'];
                        }
                        echo '<li><a href = "' . $link . '">' . $prefix . str_pad($linked_document['document_id'], 5, "0", STR_PAD_LEFT) . '</a></li>';
                    }
                    ?>
                </ul>
            </div>
        <? } ?>
    </div>


    <style>
        @media print {
            body * {
                visibility: hidden;
            }
            #section-to-print, #section-to-print * {
                visibility: visible;
                overflow:hidden;
            }
            #section-to-print .ribbon-container .ribbon {
                visibility: hidden;
            }
            #section-to-print {
                position: absolute;
                left: 0;
                top: 0;
                size: auto;   /* auto is the initial value */
                margin: 0;
                border: initial;
                border-radius: initial;
                width: initial;
                min-height: initial;
                box-shadow: initial;
                background: initial;
                page-break-after: always;
            }
            .grayTBack {
                visibility: visible;
                background-color: #ccc!important;
                color: black;
                font-weight:700;
                -webkit-print-color-adjust: exact;
                border: solid #ccc!important;
            }
            
            .gray-border{
                border: 1px solid #ccc;
                overflow:visible!important;
                -webkit-print-color-adjust: exact;
            }
        }
        @page 
        {
            size:  auto;   /* auto is the initial value */
            margin: 0mm;  /* this affects the margin in the printer settings */
        }
        
        <?
        if($total_discount == 0){
            ?>
               .discount {
                   display: none;
               }
                <?
        }
        ?>
        .termsText{
            font-size: 10px;
            min-height: 12em;
            border:none;
            resize: none;
            overflow:hidden;
            width:100px;
            width: 375px;
            text-align: justify;
        }
        .productList{
            border: 1px solid #FFFFFF;
            text-align: center;
            font-size: 11px;
        }
        .whiteTBack {
            background-color: #FFFFFF;
            color: black;
            font-weight:700;
        }
        .grayTBack {
            background-color: #ccc;
            color: black;
            font-weight:700;
        }
        .purchaseTable {
            font-size: 11px;
            width: 100%;
            max-width: 100%;
            table-layout: fixed;
            font-family:"arial";
            margin-bottom:5px; 
            padding:10px;
        }
        .gray-border{
            border: 1px solid #ccc;
            border-collapse: collapse;
        }

        .logo {
            max-width:40mm; max-height: 20mm;
            text-align: center;
        }
        .logo_container {
            /*text-align: center;*/
        }
        #purchaseHeader {
            font-size: 20px;
            font:"arial";
            width: 100%;
            text-align: center;
            font-family:"arial";
            font-weight: bold;
            display: block;
        }
        hr {
            border: 3px solid #000000;
            margin-top:3px;
        }
        .purchaseField {
            font-size: 11px;	
        }
        .purchaseFieldValue {
            font-weight: bold;
            font-size: 12px;
        }
        td {
            padding:3px;
        }
        hr {
            margin-bottom: 5px;
        }
    </style>