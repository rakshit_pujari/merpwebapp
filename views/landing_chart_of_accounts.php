

<!-- Page header -->
<div class="page-header">

    <div ng-controller="dataTableController" ng-init="load(3)"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Master</span> - Chart Of Accounts</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!COA/view" class="btn btn-link btn-float has-text"><i class="icon-book3 text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Add New Account</span></a>
                <!--<a href="#!COA/opening" class="btn btn-link btn-float has-text"><i class="icon-plus3 text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Opening Balance</span></a>-->
                <a href="report/download/chart_of_accounts" class="btn btn-link btn-float has-text"><i class="glyphicon glyphicon-download-alt text-primary" style = "font-size:22px;color:#26A69A !important"></i> <span>Export</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-calculator2 position-left"></i>Accounting</li>
            <li class="active"><i class="icon-book3 position-left"></i>Chart Of Accounts</li>
        </ul>

    </div>
</div>
<!-- /page header -->

<style>
    tr.active{background-color:yellow;}
</style>

<!-- Content area -->
<div class="content">

    <!-- Invoice archive -->
    <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Chart Of Accounts</h6>
            <div class="heading-elements">
                <!--<ul class="icons-list">
        <li><a data-action="collapse"></a></li>
        <li><a data-action="reload"></a></li>
        <li><a data-action="close"></a></li>-->
                </ul>
            </div>
        </div>

        <table class="table masterDataTable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Account Name</th>
                    <th>Account Code</th>
                    <th>Account Type</th>
                    <th>Account Description</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($chart_of_accounts as $chart_of_account): ?>
                    <tr>
                        <td>COA<?php echo str_pad($chart_of_account['coa_id'], 5, "0", STR_PAD_LEFT); ?></td>
                        <td><a href="#!COA/view/<?php echo $chart_of_account['coa_id']; ?>">
                            <span><?php echo $chart_of_account['coa_account_name']; ?></span>
                            </a>
                            <small class="display-block text-muted">Created by <?php echo $chart_of_account['employee_username']; ?> 
                                    on <?php echo $chart_of_account['coa_record_creation_time']; ?></small>
                        </td>
                        <td><?php echo $chart_of_account['coa_account_code']; ?></td>
                        <td><?php echo $chart_of_account['at_name']; ?></td>
                        <td><?php echo $chart_of_account['coa_account_description']; ?></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li><a href="#!COA/view/<?php echo $chart_of_account['coa_id']; ?>"><i class="icon-file-eye"></i></a></li>
                                <? if($this->session->userdata('access_controller')->is_access_granted('chart_of_accounts', 'delete')) { ?>
                                <li><a href='javascript: void(0)' onclick = 'showDeleteDialog("COA", <?php echo $chart_of_account['coa_id']; ?>)'><i class="glyphicon glyphicon-trash"></i></a></li>
                                <? } ?>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Account Name</th>
                    <th>Account Code</th>
                    <th>Account Type</th>
                    <th>Account Description</th>
                    <th class="text-center">Actions</th>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->