
<!-- Page header -->
<div class="page-header">
    <div ng-controller="reportController" ng-init="load()"></div>
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Report - Business Overview</span></h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">

            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><i class="icon-stack4 position-left"></i> Report</li>
            <li class="active"><i class="icon-stack-text position-left"></i>Business Overview</li>
        </ul>
    </div>
</div>

<!-- Content area -->
<div class="content">
    <h4 class="media-heading text-bold">&nbsp;Sales Reports</h4><br/>
    <div class="row">
        <a href="#!report/view/invoice">
            <div class="col-md-4">
                <div class="panel panel-body">
                    <div class="media no-margin stack-media-on-mobile">
                        <div class="media-left media-middle">
                            <i class="icon-meter-fast text-success-400 icon-3x no-edge-top"></i>
                        </div>
                        <div class="media-body">
                            <h6 class="media-heading text-semibold">Customer Performance</h6>
                            Customer-wise summary of Net Sales from all Customers
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a href="#!report/view/invoice_summary">
            <div class="col-md-4">
                <div class="panel panel-body">
                    <div class="media no-margin stack-media-on-mobile">
                        <div class="media-left media-middle">
                            <i class="icon-file-stats text-success-400 icon-3x no-edge-top"></i>
                        </div>
                        <div class="media-body">
                            <h6 class="media-heading text-semibold">Invoice Summary</h6>
                            Status of all Sales Invoices generated during the period
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a href="#!report/view/product_sales">
            <div class="col-md-4">
                <div class="panel panel-body">
                    <div class="media no-margin stack-media-on-mobile">
                        <div class="media-left media-middle">
                            <i class="icon-cube3 text-success-400 icon-3x no-edge-top"></i>
                        </div>
                        <div class="media-body">
                            <h6 class="media-heading text-semibold">Product Sales</h6>
                            Details of Product-wise Sales booked during the period
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="row">
        <a href="#!report/view/customer_ageing">
            <div class="col-md-4">
                <div class="panel panel-body">
                    <div class="media no-margin stack-media-on-mobile">
                        <div class="media-left media-middle">
                            <i class="icon-users4 text-success-400 icon-3x no-edge-top"></i>
                        </div>
                        <div class="media-body">
                            <h6 class="media-heading text-semibold">Customer Ageing</h6>
                            Customer-wise summary of Sales Invoice outstanding
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <!--<a href="#">
            <div class="col-md-4">
                <div class="panel panel-body">
                    <div class="media no-margin stack-media-on-mobile">
                        <div class="media-left media-middle">
                            <i class="icon-user-tie text-success-400 icon-3x no-edge-top"></i>
                        </div>
                        <div class="media-body">
                            <h6 class="media-heading text-semibold">Employee Sales</h6>
                            Employee-wise Sales Performance during the period
                        </div>
                    </div>
                </div>
            </div>
        </a>-->
    </div>

    <h4 class="media-heading text-bold">&nbsp;Purchase Reports</h4><br/>
    <div class="row">
        <a href="#!report/view/purchase_summary">
            <div class="col-md-4">
                <div class="panel panel-body">
                    <div class="media no-margin stack-media-on-mobile">
                        <div class="media-left media-middle">
                            <i class="icon-cart-add2 text-danger-400 icon-3x no-edge-top"></i>
                        </div>
                        <div class="media-body">
                            <h6 class="media-heading text-semibold">Purchase Summary</h6>
                            Snapshot of the status of Purchase Invoices generated
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <!--<a href="#!report/view/product_purchase">
            <div class="col-md-4">
                <div class="panel panel-body">
                    <div class="media no-margin stack-media-on-mobile">
                        <div class="media-left media-middle">
                            <i class="icon-cube3 text-danger-400 icon-3x no-edge-top"></i>
                        </div>
                        <div class="media-body">
                            <h6 class="media-heading text-semibold">Product Purchases</h6>
                            Details of Product-wise Registered Vendors Purchases
                        </div>
                    </div>
                </div>
            </div>
        </a>-->
        <a href="#!report/view/vendor_purchase">
            <div class="col-md-4">
                <div class="panel panel-body">
                    <div class="media no-margin stack-media-on-mobile">
                        <div class="media-left media-middle">
                            <i class="icon-file-text2 text-danger-400 icon-3x no-edge-top"></i>
                        </div>
                        <div class="media-body">
                            <h6 class="media-heading text-semibold">Vendor Purchases</h6>
                            Vendor-wise summary of Registered Vendor Purchases
                        </div>
                    </div>
                </div>
            </div>
        </a>
    <!--</div>
    <div class="row">-->
        <a href="#!report/view/vendor_ageing">
            <div class="col-md-4">
                <div class="panel panel-body">
                    <div class="media no-margin stack-media-on-mobile">
                        <div class="media-left media-middle">
                            <i class="icon-users4 text-danger-400 icon-3x no-edge-top"></i>
                        </div>
                        <div class="media-body">
                            <h6 class="media-heading text-semibold">Vendor Ageing</h6>
                            Vendor-wise summary of Purchase Invoices outstanding
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <h4 class="media-heading text-bold">&nbsp;Other Reports</h4><br/>
    <div class="row">
        <a href="#!report/view/inventory">
            <div class="col-md-4">
                <div class="panel panel-body">
                    <div class="media no-margin stack-media-on-mobile">
                        <div class="media-left media-middle">
                            <i class="icon-stack2 text-blue-400 icon-3x no-edge-top"></i>
                        </div>
                        <div class="media-body">
                            <h6 class="media-heading text-semibold">Inventory Summary</h6>
                            Product-wise snapshot view of inventory transactions
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a href="#!report/view/expense">
            <div class="col-md-4">
                <div class="panel panel-body">
                    <div class="media no-margin stack-media-on-mobile">
                        <div class="media-left media-middle">
                            <i class="icon-file-text2 text-blue-400 icon-3x no-edge-top"></i>
                        </div>
                        <div class="media-body">
                            <h6 class="media-heading text-semibold">Expense Summary</h6>
                            Vendor-wise Unregistered/Exempt/Non-GST Purchases
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a href="#!report/view/broker_summary">
            <div class="col-md-4">
                <div class="panel panel-body">
                    <div class="media no-margin stack-media-on-mobile">
                        <div class="media-left media-middle">
                            <i class="icon-file-text2 text-blue-400 icon-3x no-edge-top"></i>
                        </div>
                        <div class="media-body">
                            <h6 class="media-heading text-semibold">Agent Summary</h6>
                            Agent-wise summary of Trade Commission payable
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="row">
        <a href="#!report/view/broker_commission">
            <div class="col-md-4">
                <div class="panel panel-body">
                    <div class="media no-margin stack-media-on-mobile">
                        <div class="media-left media-middle">
                            <i class="icon-man text-blue-400 icon-3x no-edge-top"></i>
                        </div>
                        <div class="media-body">
                            <h6 class="media-heading text-semibold">Agent Commission</h6>
                            Invoice-wise account of Commission payable to Agents 
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a href="#!report/view/transactions">
            <div class="col-md-4">
                <div class="panel panel-body">
                    <div class="media no-margin stack-media-on-mobile">
                        <div class="media-left media-middle">
                            <i class="icon-calculator3 text-blue-400 icon-3x no-edge-top"></i>
                        </div>
                        <div class="media-body">
                            <h6 class="media-heading text-semibold">Transaction History</h6>
                            Summary of all Accounting Transactions in the system
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
    <!-- /footer -->