<div style = "padding-top:20px;padding-left:20px;padding-right:20px;">
    <?php echo validation_errors('<div style = "border-color: #ebccd1;background-color:#f2dede; color:#a94442" class="alert alert-danger alert-dismissable"><a style = "padding-bottom:10px" href="#" class="close" data-dismiss="alert" aria-label="close">×</a>', '</div>'); ?>
</div>
<div ng-controller="inventory_adjustment" ng-init="load()"></div>
<!-- Page header -->
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="#!/Inventory/adjustment"><i class="icon-arrow-left52 position-left"></i></a><span class="text-semibold">Master</span> - Inventory Adjustment</h4>
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#!/Inventory/adjustment" class="btn btn-link btn-float has-text"><i class="icon-backward text-primary"></i> <span>Go Back</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="#!"><i class="icon-home2 position-left"></i> Master</a></li>
            <li><a href="#!/Inventory/adjustment"><i class="glyphicon glyphicon-resize-vertical position-left"></i> Inventory Adjustment</a></li>
            <li class="active"><?php
                if (isset($inventory_adjustment))
                    echo 'IAD' . str_pad($inventory_adjustment['ia_id'], 5, "0", STR_PAD_LEFT);
                else
                    echo "New";
                ?></li>
        </ul>
    </div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">

    <!-- Form validation -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Inventory Adjustment</h5>

        </div>
        <div class="panel-body">
            <p class="content-group-lg">Inventory Shrinkage or Excesses can be adjusted in the system with Inventory Adjustments. Review physical inventory periodically to ensure accurate Inventory & Valuation status.</p>
            <?php echo validation_errors(); ?>
            <?php
            $attributes = array('class' => 'form-horizontal form-validate-jquery', 'id'=>'merpForm');
            if (isset($inventory_adjustment))
                echo form_open('web/Inventory/adjustment/save/' . $inventory_adjustment['ia_id'], $attributes);
            else
                echo form_open('web/Inventory/adjustment/save', $attributes);
            ?>
            <fieldset class="content-group">
                <legend class="text-bold">Basic inputs</legend>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Product Name <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select required class="select2 form-control" name="ia_product_id" id = "ia_product_id" tabindex="3" 
                                    onchange="refreshInventoryQuantityAndValuation();">
                                <option value = "">Choose a Product</option>
                                <optgroup label="Products">
                                    <?php
                                    foreach ($products as $product) {
                                        ?>
                                        <option 
                                        <?php
                                        if (set_value('ia_product_id') == $product['product_id'])
                                            echo "selected";
                                        else if (isset($inventory_adjustment)) {
                                            if ($inventory_adjustment['ia_product_id'] == $product['product_id'])
                                                echo "selected";
                                        }
                                        ?> value="<? echo $product['product_id'] ?>" 
                                            <? echo 'product_quantity =' . $product['product_quantity'] ?> 
                                            <? echo 'inventory_asset_value =' . $product['inventory_asset_value'] ?>><? echo $product['product_name'] ?>
                                        </option>
                                        <?php
                                    }
                                    ?>
                                </optgroup>
                            </select>
                        </div>
                    </div>							
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Stock Type</label>
                        <div class="col-lg-9">
                            <select required class="select form-control" name="ia_stock_type" tabindex="3">
                                <option>Good Stock</option>
                                <!--<option <?php
                                if (isset($inventory_adjustment))
                                    if ($inventory_adjustment['ia_stock_type'] == 'Damaged / Defective')
                                        echo "selected";
                                    ?> disabled>Damaged / Defective</option>
                                <option <?
                                if (isset($inventory_adjustment))
                                    if ($inventory_adjustment['ia_stock_type'] == 'Production')
                                        echo "selected";
                                    ?> disabled>Production</option>--> 
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Adjustment Type <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <label class="radio-inline">
                                <input type="radio" name="ia_type" value = "quantity" onchange="toggleIaFormElementsVisibility()" <?php
                                if (isset($inventory_adjustment)) {
                                    echo " disabled ";
                                    if ($inventory_adjustment['ia_type'] == 'quantity')
                                        echo 'checked';
                                } else
                                    echo "checked";
                                ?>>
                                By Quantity
                            </label>

                            <label class="radio-inline">
                                <input type="radio" name="ia_type" value = "value" onchange="toggleIaFormElementsVisibility()" 
                                <?php
                                if (isset($inventory_adjustment)) {
                                    echo " disabled ";
                                    if ($inventory_adjustment['ia_type'] == 'value')
                                        echo 'checked';
                                }
                                ?>>
                                By Value
                            </label>
                        </div>		
                    </div>

                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Adjustment Reason <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select required class="select form-control" name="ia_reason" tabindex="3" id = "ia_reason_select" onchange="refreshReasonElement();" required>
                                <option value =''>Choose adjustment reason</option>
                                <option value = 'Damaged Goods' <?
                                
                                if (isset($inventory_adjustment)){
                                    $custom_reason = true;
                                    if ($inventory_adjustment['ia_reason'] == 'Damaged Goods'){
                                        $custom_reason = false;
                                        echo "selected";
                                    }
                                }
                                ?>>Damaged Goods</option>
                                <option value = 'Defective / Expired Goods' <?
                                if (isset($inventory_adjustment))
                                    if ($inventory_adjustment['ia_reason'] == 'Defective / Expired Goods'){
                                        $custom_reason = false;
                                        echo "selected";
                                    }
                                ?>>Defective / Expired Goods</option>
                                <option value = 'Inventory Audit' <?
                                if (isset($inventory_adjustment))
                                    if ($inventory_adjustment['ia_reason'] == 'Inventory Audit'){
                                        $custom_reason = false;
                                        echo "selected";
                                    }
                                ?>>Inventory Audit</option>
                                <option value = 'Production adjustments' <?
                                if (isset($inventory_adjustment))
                                    if ($inventory_adjustment['ia_reason'] == 'Production adjustments'){
                                        $custom_reason = false;
                                        echo "selected";
                                    }
                                ?>>Production adjustments</option>
                                <option value = 'Stock Take' <?
                                if (isset($inventory_adjustment))
                                    if ($inventory_adjustment['ia_reason'] == 'Stock Take'){
                                        $custom_reason = false;
                                        echo "selected";
                                    }
                                ?>>Stock Take</option>
                                <option value = 'Write-off' <?
                                if (isset($inventory_adjustment))
                                    if ($inventory_adjustment['ia_reason'] == 'Write-off'){
                                        $custom_reason = false;
                                        echo "selected";
                                    }
                                ?>>Write-off</option>
                                
                                <option value = 'other' <? if (isset($inventory_adjustment)) if($custom_reason) echo "selected"; ?>>Other</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">If Others, Please Specify <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input required class="form-control" tabindex="1" type="text" id = "ia_reason_text"
                                   value="<?
                                    if(isset($inventory_adjustment))
                                    if ($custom_reason)
                                            echo $inventory_adjustment['ia_reason'];
                                    ?>">
                        </div>
                    </div>	
                    <div class="form-group col-lg-6" id = "ia_stock_quantity_container">
                        <label class="control-label col-lg-3">Stock Quantity <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" type="text" readonly id = "ia_stock_quantity" 
                                   value="<? if(isset($stock))
                                                    echo $stock['ie_stock_on_hand']; ?>">
                        </div>
                    </div>
                    <div class="form-group col-lg-6" id = "ia_stock_value_container">
                        <label class="control-label col-lg-3">Stock Value </label>
                        <div class="col-lg-9">
                            <input class="form-control" type="text" readonly id = "ia_stock_value" 
                                   value="<? if(isset($stock))
                                                    echo $stock['ie_inventory_asset_value']; ?>">
                        </div>
                    </div>	
                </div>
                <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Difference</label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="1" id = "ia_difference" onkeyup="calculateIaNumbers('difference')" type = "number" step = 0.01 
                                   value = "<?
                                   if(!empty($stock['ie_stock_on_hand']) && !empty($inventory_adjustment['ia_quantity'])){
                                       echo $inventory_adjustment['ia_quantity'] - $stock['ie_stock_on_hand'];
                                   } else if(!empty($stock['ie_inventory_asset_value']) && !empty($inventory_adjustment['ia_value'])){
                                       echo $inventory_adjustment['ia_value'] - $stock['ie_inventory_asset_value'];
                                   }
                                   
                                   
                                   ?>">
                        </div>
                    </div>
                    <div class="form-group col-lg-6" id = "ia_quantity_container">
                        <label class="control-label col-lg-3">Final Stock Quantity <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="1" name="ia_quantity" 
                                   type="number" required="required" step = 0.01
                                   placeholder="Enter Stock Quantity" id = "ia_quantity"
                                   value="<? if (isset($inventory_adjustment['ia_quantity']))
                                        echo $inventory_adjustment['ia_quantity']; ?>" onchange="if(this.value == 0) this.value = 1" onkeyup="calculateIaNumbers('total')">
                        </div>
                    </div>
                    
                    <div class="form-group col-lg-6" id = "ia_value_container">
                        <label class="control-label col-lg-3">Final Stock Value <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="1" name="ia_value" type="number"  step = 0.01
                                   required="required" placeholder="Enter Stock Value" 
                                   id = "ia_value" 
                                   value="<? if (isset($inventory_adjustment['ia_value']))
                                        echo $inventory_adjustment['ia_value']; ?>" onkeyup="calculateIaNumbers('total')">
                        </div>
                    </div>
                    
                    </div>
                    <div class="col-lg-12">
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-3">Notes <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input class="form-control" tabindex="1" name="ia_notes" type="text" required="required" placeholder="Enter Notes" 
                                   value="<? if (isset($inventory_adjustment['ia_notes']))
                                        echo $inventory_adjustment['ia_notes']; ?>">
                        </div>
                    </div>
                </div>
                
            </fieldset>

            <div class="text-right">
                <a href="#!/Inventory/adjustment/"><button class="btn btn-default">Cancel<i class="glyphicon glyphicon-fast-backward position-right"></i></button></a>
                <? 
                if($this->session->userdata('access_controller')->is_access_granted('inventory_adjustment', 'save')) { ?>
                <button onclick="submitForm('inventory_adjustment', 'Inventory/adjustment');" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
                <? } ?>
            </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->

    <!-- Footer -->
    <div class="footer text-muted">
        2017 <a href="http://www.quanterp.com" target="blank_">Quant</a> by <a href="http://1qubit.com" target="_blank">1Qubit Technologies</a>
    </div>
