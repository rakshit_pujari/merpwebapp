
var quotationDataTable;
var quotationProductList = [];
var idQuotationProductMap = {};

function initialize_quotation_form() {

    var quotationDate = new Date();
    var quotationDateString = $('#quotation_date_picker').val();
    if (quotationDateString != null && quotationDateString != "") {
        try {
            quotationDate = $.datepicker.parseDate("yy-mm-dd", quotationDateString);
        } catch (Err) {

        }
    }
    
    var quotationRevisionDate = new Date();
    var quotationRevisionDateString = $('#quotation_revision_date').val();
    if (quotationRevisionDateString != null && quotationRevisionDateString != "") {
        try {
            quotationRevisionDate = $.datepicker.parseDate("yy-mm-dd", quotationRevisionDateString);
            $('#quotation_revision_date').datepicker({
                dateFormat: 'yy-mm-dd'
            }).datepicker("setDate", quotationRevisionDate);
        } catch (Err) {

        }
    } else {
        $('#quotation_revision_date').datepicker({
            dateFormat: 'yy-mm-dd'
        });
    }
    
    $('#quotation_date_picker').datepicker({
            dateFormat: 'yy-mm-dd'
    }).datepicker("setDate", quotationDate);

    initializeFormValidator();
    initializeQuotationDataTable();
    initializeQuotationSelect2();
    loadCustomersForQuotation();
    loadQuotationProductsList();
}

function resetQuotationLinkedCompanyId() {
    $('[name="quotation_linked_company_id"]').val('');
}

function initializeQuotationSelect2() {
    // Select with search
    $('.select2').select2();
    $('.select').select2({//keep this below $('.select2').select2();
        minimumResultsForSearch: Infinity
    });
}

function initializeQuotationDataTableSelect2() {
    $('.dtSelect2').select2();
    $('.dtSelect').select2({
        minimumResultsForSearch: Infinity
    });
}


function initializeQuotationDataTable() {
    quotationDataTable = $('.quotationDatatable').DataTable({
        autoWidth: false,
        columnDefs: [
            {
                className: "row text-center",
                targets: [4]
            }
        ],
        order: [[0, "asc"]],
        searching: false,
        paging: false,
        bInfo: false,
        bSort: false
    });

}

function loadCustomersForQuotation() {

    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            var data = JSON.parse(request.responseText).companies;

            $('.customer_names').typeahead(
                    {
                        hint: true,
                        highlight: true,
                        minLength: 0
                    },
                    {
                        source: function (query, process) {
                            objects = [];
                            map = {};
                            //data = [{"id":1,"label":"machin ltd"},{"id":2,"label":"truc"}] // Or get your JSON dynamically and load it into this variable
                            $.each(data, function (i, object) {
                                var company = (object.company_display_name).toLowerCase();
                                if (company.includes(query.toLowerCase())) {
                                    map[object.company_display_name] = object;	// using company_display_name because company_display_name is unique key in company table
                                    objects.push(object.company_display_name);
                                }
                            });
                            process(objects);
                        }
                    });
            $('.customer_names').on('typeahead:selected', function (e, datum) {
                //alert(datum.value);
                //alert(map[datum].company_id);
                $('[name="quotation_linked_company_invoice_name"]').val(map[datum].company_contact_person_name);
                $('[name="quotation_linked_company_id"]').val(map[datum].company_id);
                var address = '';
                if ((map[datum].company_billing_address)) {
                    address += map[datum].company_billing_address;
                    if (!address.trim().endsWith(",")) {
                        address = address + ', ';
                        ;
                    }
                }
                if ((map[datum].company_billing_area)) {
                    address += map[datum].company_billing_area + ', ';
                }
                if ((map[datum].city_name)) {
                    address += map[datum].city_name + ', ';
                }
                if ((map[datum].district)) {
                    address += map[datum].district + ', ';
                }
                if ((map[datum].company_billing_pincode)) {
                    address += map[datum].company_billing_pincode;
                }

                if (address.trim().endsWith(",")) {
                    address = address.trim().slice(0, -1);
                }
                //map[datum].company_billing_address + ", " + map[datum].company_billing_area  + ", " + map[datum].city_name + ", " + map[datum].district + ", " + map[datum].company_billing_pincode
                $('[name="quotation_linked_company_billing_address"]').val(address);
                $('#quotation_linked_company_billing_state_id').val(map[datum].company_billing_state_id).trigger("change");
                //$('#quotation_linked_company_gst_supply_state_id').val(map[datum].company_gst_supply_state_id).trigger("change");
                $('#quotation_linked_company_payment_term_id').val(map[datum].company_payment_term_id).trigger("change");
                $('[name="quotation_linked_company_pincode"]').val(map[datum].company_pincode);
            });

        }
    }

    request.open("GET", "/index.php/web/company/json/customer", true);

    request.setRequestHeader("content-type", "application/json");
    request.send();
}

function deleteQuotationRow(element) {
    quotationDataTable.row($(element).parents('tr'))
            .remove()
            .draw();
    renumberQuotationTable();
}

function renumberQuotationTable() {
    quotationDataTable.column(0).nodes().each(function (cell, i) {
        cell.innerHTML = i + 1;
    });
}

function loadQuotationProductsList() {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            var productsArray = JSON.parse(request.responseText).products;
            quotationProductList = [];
            for (var i = 0; i < productsArray.length; i++) {
                var product = productsArray[i];
                quotationProductList.push(product);
                idQuotationProductMap[product.product_id] = product;
            }

            if (!quotationDataTable.data().count())
                updateQuotationProducts();//add 1 row by default if empty
            else 
                initializeQuotationDataTableSelect2();
        }
    };
    request.open("GET", "/index.php/web/product/json/sales", true);
    request.send();
}


/*
 0 - Product ID
 1 - rate
 2 - quantity
 3- UQC ID
 4 - UQC text
 */
function updateQuotationProducts(element, updateFields) {

    var productsListSize = 0;
    quotationDataTable.column(0).nodes().each(function (cell, i) {
        productsListSize++;
    });

    var rowIdx = productsListSize;
    var productId = quotationProductList[0].product_id;
    var quantity = 1;
    var quotation_price = 0;
    if(idQuotationProductMap[productId].selling_price){
        quotation_price = idQuotationProductMap[productId].selling_price;
    }
    if (element != null) {
        rowIdx = $(element).parents('tr')[0].rowIndex - 1;
        productId = element.selectedOptions[0].value;	//$('#priorityList2 option:selected').val()	
        if (!updateFields && quotationDataTable.cell(rowIdx, 2).nodes().to$().find('input').val()){
            quotation_price = quotationDataTable.cell(rowIdx, 2).nodes().to$().find('input').val();
        } else if(idQuotationProductMap[productId].selling_price){
            quotation_price = idQuotationProductMap[productId].selling_price;
        } else {
            quotation_price = 0;
        }

        quantity = quotationDataTable.cell(rowIdx, 3).nodes().to$().find('input').val();
    }

    //PRODUCT
    var productSelectList = document.createElement("select");
    productSelectList.setAttribute("onchange", "updateQuotationProducts(this, true);");
    productSelectList.classList.add("form-control");
    productSelectList.classList.add("dtSelect2");
    //Create and append the options
    for (var i = 0; i < quotationProductList.length; i++) {
        var option = document.createElement("option");
        option.value = quotationProductList[i].product_id;
        option.text = quotationProductList[i].product_name;
        productSelectList.appendChild(option);

        if (productId == quotationProductList[i].product_id) {
            option.selected = true;
            option.setAttribute("selected", "selected");
        }
    }
    var productStr = productSelectList.outerHTML;

    var data = [];
    data[0] = rowIdx + 1;
    data[1] = productStr + '<input name = "quotation_entries[' + data[0] + '][' + productId + '][0]" value = "' + idQuotationProductMap[productId].product_name + '" style = "display:none" ></input>';
    data[2] = '<input name = "quotation_entries[' + data[0] + '][' + productId + '][1]" class = "form-control" type = "number" step = 0.01 min = 0.01 value = "' + quotation_price + '"></input>';
    data[3] = '<input name = "quotation_entries[' + data[0] + '][' + productId + '][2]" id = "quantity" class = "form-control" type = "number" step = 0.01 min = 0.01 value = "' + quantity + '"></input>'
            + '<input name = "quotation_entries[' + data[0] + '][' + productId + '][3]" value = "' + idQuotationProductMap[productId].product_uqc_id + '" style = "display:none" ></input>'
            + '<input name = "quotation_entries[' + data[0] + '][' + productId + '][4]" value = "' + idQuotationProductMap[productId].uqc_text + '" style = "display:none" ></input>';
    if (rowIdx == 0)
        data[4] = '';
    else
        data[4] = '<a href="javascript: void(0)" onclick = "deleteQuotationRow(this);refreshAndReIndexEntireQuotationTable();"><i class="glyphicon glyphicon-trash"></i></a>';

    if (element == null) {
        quotationDataTable.row.add(data).draw(true);
    } else {
        quotationDataTable.row(rowIdx).data(data).draw(true);
    }
    initializeQuotationDataTableSelect2();
}
