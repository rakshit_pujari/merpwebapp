
// Initialize chart
function initializeCharts() {

    $(".datepicker").datepicker();

    var line_bar_sales = null;
    var line_bar_purchase = null;
    var tornado_bars_negative = null;
    
    if(document.getElementById('line_bar_sales')!=null){
        line_bar_sales = echarts.init(document.getElementById('line_bar_sales'));
    }
    if(document.getElementById('line_bar_purchase')!=null){
        line_bar_purchase = echarts.init(document.getElementById('line_bar_purchase'));
    }
    
    if(document.getElementById('tornado_bars_negative')!=null){
        tornado_bars_negative = echarts.init(document.getElementById('tornado_bars_negative'));
    }
    
    var formatUtil = echarts.format;
    
    $.get("/index.php/web/dashboard/profit_history", function (data, status) {
        if (status == 'success') {
            var revenue_history = JSON.parse(data).profit_history.revenue_history;
            var expense_history = JSON.parse(data).profit_history.expense_history;
            var net_profit_history = JSON.parse(data).profit_history.net_profit_history;

//
            // Tornado with negative stacks options
            //

            tornado_bars_negative_options = {

                toolbox: {
                    show: true,
                    feature: {
                        dataView: {show: true, readOnly: false},
                        saveAsImage: {show: true}
                    }
                },
                // Setup grid
                grid: {
                    x: 65,
                    x2: 65,
                    y: 35,
                    y2: 25
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow'
                    }
                },

                // Add legend
                legend: {
                    data: ['Revenue', 'Expense', 'Profit']
                },

                // Enable drag recalculate
                calculable: true,

                // Horizontal axis
                xAxis: [{
                        type: 'value',
                        name: 'Rupees',
                        axisLabel: {
                            //formatter: '{value} ₹'
                            formatter: function(params){
                                return '₹ ' + params;//echarts.format.addCommas(params);
                            }
                        }
                    }],

                // Vertical axis
                yAxis: [{
                        type: 'category',
                        axisTick: {
                            show: false
                        },
                        data: ['Week 1', 'Week 2', 'Week 3', 'Week 4', 'Week 5', 'Week 6', 'Week 7'/*, 'Week 8', 'Week 9', 'Week 10', 'Week 11', 'Week 12'*/]
                    }],

                // Add series
                series: [
                    {
                        name: 'Revenue',
                        type: 'bar',
                        stack: 'Total',
                        barWidth: 5,
                        itemStyle: {
                            normal: {
                                color: '#29B6F6',
                                shadowBlur: 200,
                                shadowColor: 'rgba(0, 0, 0, 0)',
                                label: {
                                    show: true,
                                    position: 'right'
                                }
                            }
                        },
                        data: revenue_history
                    },
                    {
                        name: 'Expense',
                        type: 'bar',
                        stack: 'Total',
                        barWidth: 5,
                        itemStyle: {
                            normal: {
                                color: '#B6A2DE',
                                shadowBlur: 200,
                                shadowColor: 'rgba(0, 0, 0, 0)',
                                label: {
                                    show: true,
                                    position: 'left',
                                }
                            }
                        },
                        data: expense_history
                    },
                    {
                        name: 'Profit',
                        type: 'bar',
                        barWidth: 25,
                        itemStyle: {
                            normal: {
                                color: '#55C7C9',
                                shadowBlur: 200,
                                shadowColor: 'rgba(0, 0, 0, 0)',
                                label: {
                                    show: true,
                                    position: 'inside'
                                }
                            }
                        },
                        data: net_profit_history
                    }
                ]
            };

            if(tornado_bars_negative!=null)
                tornado_bars_negative.setOption(tornado_bars_negative_options);
        }
    });



    /**
     * Sales history graph
     */

    $.get("/index.php/web/dashboard/sales_history", function (data, status) {
        if (status == 'success') {
            var sales_amount_history = JSON.parse(data).sales_history.sales_amount_history;
            var cn_amount_history = JSON.parse(data).sales_history.cn_amount_history;
            var net_sales_amount_history = JSON.parse(data).sales_history.net_sales_amount_history;
            line_bar_options = {

                toolbox: {
                    show: true,
                    feature: {
                        dataView: {show: true, readOnly: false},
                        magicType: {show: true, type: ['line', 'bar']},
                        restore: {show: true},
                        saveAsImage: {show: true}
                    }
                },

                // Setup grid
                grid: {
                    x: 80,
                    x2: 45,
                    y: 35,
                    y2: 25
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                // Enable drag recalculate
                calculable: true,

                // Add legend
                legend: {
                    data: ['Sales', 'Credit Note', 'Net Sales']
                },

                // Horizontal axis
                xAxis: [{
                        type: 'category',
                        data: ['Week 1', 'Week 2', 'Week 3', 'Week 4', 'Week 5', 'Week 6', 'Week 7', 'Week 8', 'Week 9', 'Week 10', 'Week 11', 'Week 12']
                    }],

                // Vertical axis
                yAxis: [
                    {
                        type: 'value',
                        name: 'Rupees',
                        axisLabel: {
                            formatter: '₹ {value}'
                        }
                    }
                ],

                // Add series
                series: [
                    {
                        name: 'Sales',
                        type: 'bar',
                        data: sales_amount_history,
                        itemStyle: {
                            normal: {
                                color: '#55C7C9',
                                shadowBlur: 200,
                                shadowColor: 'rgba(0, 0, 0, 0)'
                            }
                        }
                    },
                    {
                        name: 'Credit Note',
                        type: 'bar',
                        data: cn_amount_history,
                        itemStyle: {
                            normal: {
                                color: '#B6A2DE',
                                shadowBlur: 200,
                                shadowColor: 'rgba(0, 0, 0, 0)'
                            }
                        }

                    },
                    {
                        name: 'Net Sales',
                        type: 'line',
                        data: net_sales_amount_history,
                        itemStyle: {
                            normal: {
                                color: '#29B6F6',
                                shadowBlur: 200,
                                shadowColor: 'rgba(0, 0, 0, 0)'
                            }
                        }

                    }
                ]
            };


            // use configuration item and data specified to show chart
            if(line_bar_sales!=null)
                line_bar_sales.setOption(line_bar_options);
        }
    });


    $.get("/index.php/web/dashboard/purchase_history", function (data, status) {
        if (status == 'success') {
            var purchase_amount_history = JSON.parse(data).purchase_history.purchase_amount_history;
            var dn_amount_history = JSON.parse(data).purchase_history.dn_amount_history;
            var net_purchase_amount_history = JSON.parse(data).purchase_history.net_purchase_amount_history;
            line_bar_options = {

                toolbox: {
                    show: true,
                    feature: {
                        dataView: {show: true, readOnly: false},
                        magicType: {show: true, type: ['line', 'bar']},
                        restore: {show: true},
                        saveAsImage: {show: true}
                    }
                },

                // Setup grid
                grid: {
                    x: 80,
                    x2: 45,
                    y: 35,
                    y2: 25
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                // Enable drag recalculate
                calculable: true,

                // Add legend
                legend: {
                    data: ['Purchase', 'Debit Note', 'Net Purchase']
                },

                // Horizontal axis
                xAxis: [{
                        type: 'category',
                        data: ['Week 1', 'Week 2', 'Week 3', 'Week 4', 'Week 5', 'Week 6', 'Week 7', 'Week 8', 'Week 9', 'Week 10', 'Week 11', 'Week 12']
                    }],

                // Vertical axis
                yAxis: [
                    {
                        type: 'value',
                        name: 'Rupees',
                        axisLabel: {
                            formatter: '₹ {value}'
                        }
                    }
                ],

                // Add series
                series: [
                    {
                        name: 'Purchase',
                        type: 'bar',
                        data: purchase_amount_history,
                        itemStyle: {
                            normal: {
                                color: '#55C7C9',
                                shadowBlur: 200,
                                shadowColor: 'rgba(0, 0, 0, 0)'
                            }
                        }
                    },
                    {
                        name: 'Debit Note',
                        type: 'bar',
                        data: dn_amount_history,
                        itemStyle: {
                            normal: {
                                color: '#B6A2DE',
                                shadowBlur: 200,
                                shadowColor: 'rgba(0, 0, 0, 0)'
                            }
                        }

                    },
                    {
                        name: 'Net Purchase',
                        type: 'line',
                        data: net_purchase_amount_history,
                        itemStyle: {
                            normal: {
                                color: '#29B6F6',
                                shadowBlur: 200,
                                shadowColor: 'rgba(0, 0, 0, 0)'
                            }
                        }

                    }
                ]
            };


            // use configuration item and data specified to show chart
            if(line_bar_purchase!=null)
                line_bar_purchase.setOption(line_bar_options);
        }
    });


    window.onresize = function () {
        setTimeout(function () {
            if(line_bar_sales!=null) line_bar_sales.resize();
            if(line_bar_purchase!=null) line_bar_purchase.resize();
            if(tornado_bars_negative!=null) tornado_bars_negative.resize();
        }, 200);
    }
}

