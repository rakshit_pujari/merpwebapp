function initialize_settings_form() {
    // Primary
    $(".control-primary").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-primary-600 text-primary-800'
    });

    // Bootstrap switch
    $(".switch").bootstrapSwitch();

/*    $('.select2').select2();
    $('.select').select2({
        minimumResultsForSearch: Infinity
    });
*/
    var currentTabIndex = 0;
    $("#btnBack").click(function () {
        var currentTab = $("#homeTabs li.active");
        currentTabIndex = parseInt(currentTab[0].id);
        $('.tabbable li:eq(' + (currentTabIndex - 1) + ') a').tab('show'); 				//http://getbootstrap.com/javascript/#tabs
        if (currentTabIndex > 0) {
            currentTabIndex = currentTabIndex - 1;
        }
    });


    $("#btnNext").click(function () {
        var currentTab = $("#homeTabs li.active");
        currentTabIndex = parseInt(currentTab[0].id);
        $('.tabbable li:eq(' + (currentTabIndex + 1) + ') a').tab('show'); 				//http://getbootstrap.com/javascript/#tabs
        if (currentTabIndex < ($('.tabbable >ul >li').length - 1)) {
            currentTabIndex = currentTabIndex + 1;
        }
    });
}