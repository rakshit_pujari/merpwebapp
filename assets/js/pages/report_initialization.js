
function downloadInvoiceReport(type){
    var startDate = $('#startDate').val();
    var endDate = $('#endDate').val();
    window.location.href = 'report/'+ type + '/' + startDate + '/' + endDate;
}


function initializeReportPage(){
    var currentDate = new Date();
    
    $('#endDate').datepicker({
        dateFormat: 'yy-mm-dd'
    }).datepicker("setDate", currentDate);
    // Select with search
    currentDate.setDate(1);
    //currentDate.setMonth(currentDate.getMonth() - 1);
    
    $('#startDate').datepicker({
        dateFormat: 'yy-mm-dd'
    }).datepicker("setDate", currentDate);
}

function initializeGstReportPage(){
    $('.select2').select2();

    $('.select').select2({//keep this below $('.select2').select2();
        minimumResultsForSearch: Infinity
    });
}

function downloadReport(type){
    var startDate = $('#startDate').val();
    var endDate = $('#endDate').val();
    window.location.href = 'report/download/'+ type + '/' + startDate + '/' + endDate;
}

function downloadAllGSTReports(){
    downloadGSTR1();
    downloadGSTR2A();
    downloadGSTR3B();
}

function downloadGSTR1(){
    var gstrReportMonth = $('#gstrReportMonth').val();
    var gstrReportYear = $('#gstrReportYear').val();
    window.location.href = 'report/gstr1_xlsx/1.4/' + gstrReportMonth + '/' + gstrReportYear;
}

function downloadGSTR2A(){
    var gstrReportMonth = $('#gstrReportMonth').val();
    var gstrReportYear = $('#gstrReportYear').val();
    window.location.href = 'report/gstr2_xlsx/1.1/' + gstrReportMonth + '/' + gstrReportYear;
}

function downloadGSTR3B(){
    var gstrReportMonth = $('#gstrReportMonth').val();
    var gstrReportYear = $('#gstrReportYear').val();
    window.location.href = 'report/gstr3_xlsx/3.0/' + gstrReportMonth + '/' + gstrReportYear;
}

