
var journalDatatable;
var parentCoaList = null;

var brokerList, companyList, employeeList, transporterList;
/**
 * 
 * @returns
 */
function initializeJournalDataTable() {
    journalDatatable = $('.journalDatatable').DataTable({
        autoWidth: false,
        columnDefs: [{
                width: '250px',
                targets: [1, 2, 3, 6]
            },
            {
                width: '150px',
                targets: [4, 5]
            },
            {
                width: '50px',
                targets: [0, 7]
            },
            {
                className: "row text-center",
                targets: [7]
            }
        ],
        order: [[0, "asc"]],
        searching: false,
        paging: false,
        bInfo: false,
        bSort: false
    });
    $('.select2').select2();
    // Set today's date
    Date.prototype.toDateInputValue = (function () {
        var local = new Date(this);
        local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
        return local.toJSON().slice(0, 10);
    });

    $('#datePicker').val(new Date().toDateInputValue());
    loadChartOfAccountsForJournalForm();
    loadJournalEntities();
    initializeDataTableSelect2();
    validateJournalEntry();
}


function loadJournalEntities(){
    $.get("/index.php/web/journal/json/entities", function (data, status) {
        if (status == 'success') {
            brokerList = JSON.parse(data).brokers;   
            companyList = JSON.parse(data).companies;   
            employeeList = JSON.parse(data).employees;   
            transporterList = JSON.parse(data).transporters;   
        }
    });
}
/**
 * 
 * @returns 
 */
function loadChartOfAccountsForJournalForm() {
    $.get("/index.php/web/COA/json/parent", function (data, status) {
        if (status == 'success') {
            parentCoaList = JSON.parse(data).coa;   
            $.get("/index.php/web/COA/json/subaccount", function (data, status) {
                if (status == 'success') {
                    var subAccountCoaList = JSON.parse(data).coa;   
                    for (var i = 0; i < parentCoaList.length; i++) {
                        var subaccounts = [];
                        
                        var mainAccount = Object();
                        mainAccount.coa_id = parentCoaList[i].coa_id;
                        mainAccount.coa_account_name = "None";
                        mainAccount.coa_subaccount_of_id = parentCoaList[i].coa_id;
                        subaccounts.push(mainAccount);
                        for (var j = 0; j < subAccountCoaList.length; j++) {
                           subAccountCoaList[j].displayRowId = -1;
                           if(subAccountCoaList[j].coa_subaccount_of_id == parentCoaList[i].coa_id){
                               subaccounts.push(subAccountCoaList[j]);
                           }
                        }   
                        parentCoaList[i].subaccounts = subaccounts; 
                    }
                    
                }
            });
        }
    });
}

var rowCounter = 0;
function updateJournalRows(currentRow) {

    var selectedParentCoaId = null;
    if(parentCoaList == null
            || brokerList == null
            || companyList == null
            || employeeList == null
            || transporterList == null){
        alert('Error. Please check your internet connection.');
        return;
    }
    
    var rowId = 0;
    journalDatatable.column(0).nodes().each(function (cell, i) {
        rowId++;
    });

    rowCounter = rowId;
    
    var rowIdx = rowCounter;
    var data = [];

    var selectedParentCoaId = parentCoaList[0].coa_id;
    var selectedEntity;
    if(currentRow != null){
        rowIdx = $(currentRow).parents('tr')[0].rowIndex - 1;
        selectedParentCoaId = journalDatatable.cell(rowIdx, 1).nodes().to$().find('select').val();
        selectedEntity = journalDatatable.cell(rowIdx, 3).nodes().to$().find('select').val();
    } 
    
    
    var parentCoaSelectList = document.createElement("select");
    parentCoaSelectList.setAttribute("onchange", "updateJournalRows(this);validateJournalEntry();");
    parentCoaSelectList.setAttribute("required", "required");
    //parentCoaSelectList.setAttribute("name", "journal_entries[" + rowCounter + "][0]");
    parentCoaSelectList.classList.add("form-control");
    parentCoaSelectList.classList.add("dtSelect2");   

    //Create and append the options
    var subAccountCoaList = [];
    for (var i = 0; i < parentCoaList.length; i++) {
        var option = document.createElement("option");
        option.value = parentCoaList[i].coa_id;
        option.text = parentCoaList[i].coa_account_name;
        parentCoaSelectList.appendChild(option);

        if (selectedParentCoaId == parentCoaList[i].coa_id) {
            option.selected = true;
            option.setAttribute("selected", "selected");
            subAccountCoaList = parentCoaList[i].subaccounts;
        }
    }
 
    var subaccountCoaSelectList = document.createElement("select");
    subaccountCoaSelectList.setAttribute("name", "journal_entries[" + rowIdx + "][1]");
    subaccountCoaSelectList.setAttribute("onchange", "validateJournalEntry()");
    subaccountCoaSelectList.classList.add("form-control");
    subaccountCoaSelectList.classList.add("dtSelect2");

    //Create and append the options
    for (var i = 0; i < subAccountCoaList.length; i++) {
        if(parseInt(selectedParentCoaId) != parseInt(subAccountCoaList[i].coa_subaccount_of_id)){
            continue;
        }    
        var option = document.createElement("option");
        option.value = subAccountCoaList[i].coa_id;
        option.text = subAccountCoaList[i].coa_account_name;
        subaccountCoaSelectList.appendChild(option);
               
    }
    
    var entitySelectList = document.createElement("select");
    entitySelectList.setAttribute("name", "journal_entries[" + rowIdx + "][5]");
    entitySelectList.classList.add("form-control");
    entitySelectList.classList.add("dtSelect2");   

    var blankOption = document.createElement("option");
    blankOption.value = '';
    blankOption.text = 'None';
    entitySelectList.appendChild(blankOption);

    var brokerOptGroup = document.createElement("optGroup");
    brokerOptGroup.setAttribute("label", "Brokers");
    for (var i = 0; i < brokerList.length; i++) {
        var option = document.createElement("option");
        option.value = 'broker_' + brokerList[i].broker_id;
        option.text = brokerList[i].broker_name;
        brokerOptGroup.appendChild(option);
        
        if (option.value == selectedEntity) {
            option.selected = true;
            option.setAttribute("selected", "selected");
        }
    }
    entitySelectList.appendChild(brokerOptGroup);
    
    var companyOptGroup = document.createElement("optGroup");
    companyOptGroup.setAttribute("label", "Companies");
    for (var i = 0; i < companyList.length; i++) {
        var option = document.createElement("option");
        option.value = 'company_' + companyList[i].company_id;
        option.text = companyList[i].company_display_name;
        companyOptGroup.appendChild(option);
        
        if (option.value == selectedEntity) {
            option.selected = true;
            option.setAttribute("selected", "selected");
        }
    }
    entitySelectList.appendChild(companyOptGroup);
    
    var employeeOptGroup = document.createElement("optGroup");
    employeeOptGroup.setAttribute("label", "Employees");
    for (var i = 0; i < employeeList.length; i++) {
        var option = document.createElement("option");
        option.value = 'employee_' + employeeList[i].employee_id;
        option.text = employeeList[i].employee_name;
        employeeOptGroup.appendChild(option);
        
        if (option.value == selectedEntity) {
            option.selected = true;
            option.setAttribute("selected", "selected");
        }
    }
    entitySelectList.appendChild(employeeOptGroup);
    
    var transporterOptGroup = document.createElement("optGroup");
    transporterOptGroup.setAttribute("label", "Transporters");
    for (var i = 0; i < transporterList.length; i++) {
        var option = document.createElement("option");
        option.value = 'transporter_' + transporterList[i].transporter_id;
        option.text = transporterList[i].transporter_name;
        transporterOptGroup.appendChild(option);
        
        if (option.value == selectedEntity) {
            option.selected = true;
            option.setAttribute("selected", "selected");
        }
    }
    entitySelectList.appendChild(transporterOptGroup);

    var parentCoaStr = parentCoaSelectList.outerHTML;
    var subaccountCoaStr = subaccountCoaSelectList.outerHTML; 
    var entitySelectStr = entitySelectList.outerHTML; 
    
    data[0] = (rowIdx + 1);
    data[1] = parentCoaStr;
    data[2] = subaccountCoaStr;
    data[3] = '<input name = "journal_entries[' + rowIdx + '][2]" class = "form-control" type = "text"></input>';
    data[4] = '<input name = "journal_entries[' + rowIdx + '][3]" class = "form-control" type = "number" step = 0.01 onchange = "invalidate(this, 3);validateJournalEntry()"></input>';
    data[5] = '<input name = "journal_entries[' + rowIdx + '][4]" class = "form-control" type = "number" step = 0.01 onchange = "invalidate(this, 4);validateJournalEntry()"></input>';
    data[6] = entitySelectStr;
    data[7] = '<a href="javascript: void(0)" onclick = "deleteJournalRow(this);"><i class="glyphicon glyphicon-trash"></i></a>';

    if (currentRow == null) {
        journalDatatable.row.add(data).draw(true);     
    } else {
        journalDatatable.row(rowIdx).data(data).draw(true);
    }
    initializeDataTableSelect2();
}

function invalidate(element, transaction_type){
    var invalidElement;
    var rowIdx = $(element).parents('tr')[0].rowIndex - 1;
    if(transaction_type == 3){
        invalidElement = journalDatatable.cell(rowIdx, 5).nodes().to$().find('input')[0];
    } else if(transaction_type == 4){
        invalidElement = journalDatatable.cell(rowIdx, 4).nodes().to$().find('input')[0];
    }
    if(element.value != 0)
        invalidElement.disabled = true;
    else 
        invalidElement.disabled = false;
}

function deleteJournalRow(element) {
    //var element = journalDatatable.cell(currentRowId, 1).nodes().to$().find('select')[0];
    journalDatatable.row($(element).parents('tr')).remove().draw();
    renumberJournalTable();
    validateJournalEntry();
}


function renumberJournalTable() {
    journalDatatable.column(0).nodes().each(function (cell, i) {
        cell.innerHTML = (i + 1);
    });
}

function validateJournalEntry(){
    document.getElementById('buttonSaveConfirmJournal').disabled = true;
    
    var debitTotal = parseFloat(0);
    var creditTotal = parseFloat(0);
    
    var debitCoa = [];
    var creditCoa = [];
    journalDatatable.column(0).nodes().each(function (cell, i) {
        var debitAmount = journalDatatable.cell(i, 4).nodes().to$().find('input')[0];
        var account = journalDatatable.cell(i, 2).nodes().to$().find('select')[0];
        if(debitAmount!=null){
            if(!debitAmount.disabled){
                if(debitAmount.value > 0){
                    debitTotal+=parseFloat(debitAmount.value);
                    debitCoa.push(account.value);
                } else {
                    debitAmount.value = '';
                }
            }
        }
        
        var creditAmount = journalDatatable.cell(i, 5).nodes().to$().find('input')[0];
        if(creditAmount!=null){
            if(!creditAmount.disabled){
                if(creditAmount.value > 0){
                    creditTotal+=parseFloat(creditAmount.value);
                    creditCoa.push(account.value);
                } else {
                    creditAmount.value = '';
                }
            }
        }
    });
    
    if(debitTotal == creditTotal 
            && intersect(debitCoa, creditCoa).length == 0){
        if(debitTotal > 0)
            document.getElementById('buttonSaveConfirmJournal').disabled = false;
    }
    
}

function intersect(a, b) {
    var t;
    if (b.length > a.length) t = b, b = a, a = t; // indexOf to loop over shorter
    return a.filter(function (e) {
        return b.indexOf(e) > -1;
    });
}

