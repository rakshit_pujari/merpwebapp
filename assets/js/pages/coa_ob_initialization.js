function highLightOpeningBalanceAdjustment(){
    
    //check if redirected from other page for highlighting adjustment
    
    if(getParameterByName('highlight') == 'adjustment'){
        var w = $(window);
        var table = $('table');
        var row = table.find('tr')
            .removeClass('active')
            .eq( 69 )
            .addClass('active');

        if (row.length){
            $('html,body').animate({scrollTop: row.offset().top - (w.height()/2)}, 1000 );
        }
    }
}
function initializeCoaObForm(){
    var currentDate = new Date();
    var currentDateString = $('#datePicker').val();
    if(currentDateString != null && currentDateString != ""){
        try{
            currentDate = $.datepicker.parseDate( "yy-mm-dd", currentDateString);
        } catch(Err){
            
        }
    }
    $('#datePicker').datepicker({
        dateFormat: 'yy-mm-dd'
    }).datepicker("setDate", currentDate);
}

function invalidateCoaField(element, coa_id, transaction_type){
    var name;
    if(transaction_type == 0){
        name = 'coa_ob[' + coa_id + '][1]';
    } else if(transaction_type == 1){
        name = 'coa_ob[' + coa_id + '][0]';
    }
    if(element.value != 0)
        document.getElementsByName(name)[0].disabled = true;
    else 
        document.getElementsByName(name)[0].disabled = false;
}

function disableSubmitIfNotBalanced(coaLength){
    var coaCounter = 0;
    var coaId = 0;
    var debitTotal = 0;
    var creditTotal = 0;
    
    document.getElementById('submitCoaOb1').disabled = true;
    document.getElementById('submitCoaOb2').disabled = true;
    document.getElementById('balanceDebit').innerHTML = '';
    document.getElementById('balanceCredit').innerHTML = '';
    
    while(coaCounter < coaLength){
        var debitElement = 'coa_ob[' + coaId + '][0]';
        var creditElement = 'coa_ob[' + coaId + '][1]';
        if(document.getElementsByName(debitElement)[0]!=null 
                && !document.getElementsByName(debitElement)[0].disabled){
            if(parseFloat(document.getElementsByName(debitElement)[0].value) > 0){
                debitTotal+=  parseFloat(document.getElementsByName(debitElement)[0].value);
            }
            coaCounter++;
        } else if(document.getElementsByName(creditElement)[0]!=null
                && !document.getElementsByName(creditElement)[0].disabled){
            if(parseFloat(document.getElementsByName(creditElement)[0].value) > 0){
                creditTotal+=  parseFloat(document.getElementsByName(creditElement)[0].value); 
            }
            coaCounter++;
        } 
        coaId++;
    }
    
    if(debitTotal == creditTotal){        
        document.getElementById('submitCoaOb1').disabled = false;
        document.getElementById('submitCoaOb2').disabled = false;
        return;
    } else if(debitTotal > creditTotal){
        document.getElementById('balanceDebit').innerHTML = '₹ ' + (debitTotal - creditTotal);
    } else if(debitTotal < creditTotal){
        document.getElementById('balanceCredit').innerHTML = '₹ ' + (creditTotal - debitTotal);
    }
}