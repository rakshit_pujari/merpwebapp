

var EXP_OC_GST_STATE_ID = -1;
var interstate_taxes_for_expense = null;
var intrastate_taxes_for_expense = null;

function initialize_expense_form() {
    //loadOcGstStateIdForExpense();
    loadVendorsForExpenseForm();
    var currentDate = new Date();
    var currentDateString = $('#datePicker').val();
    if (currentDateString != null && currentDateString != "") {
        try {
            currentDate = $.datepicker.parseDate("yy-mm-dd", currentDateString)
        } catch (Err) {

        }
    }
    $('#datePicker').datepicker({
        dateFormat: 'yy-mm-dd'
    }).datepicker("setDate", currentDate);
}

function resetExpenseLinkedCompanyId() {
    $('[name="expense_vendor_id"]').val('');
}

function initializeExpenseSelect2() {
    // Select with search
    $('.select2').select2();
    $('.select').select2({//keep this below $('.select2').select2();
        minimumResultsForSearch: Infinity
    });
}

function initializeExpenseDataTableSelect2() {
    $('.dtSelect2').select2();
    $('.dtSelect').select2({
        minimumResultsForSearch: Infinity
    });
}

function loadVendorsForExpenseForm() {

    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            var data = JSON.parse(request.responseText).companies;

            $('.vendor_names').typeahead(
                    {
                        hint: true,
                        highlight: true,
                        minLength: 0
                    },
                    {
                        source: function (query, process) {
                            objects = [];
                            map = {};
                            //data = [{"id":1,"label":"machin ltd"},{"id":2,"label":"truc"}] // Or get your JSON dynamically and load it into this variable
                            $.each(data, function (i, object) {
                                var company = (object.company_display_name).toLowerCase();
                                if (company.includes(query.toLowerCase())) {
                                    map[object.company_display_name] = object;	// using company_display_name because company_display_name is unique key in company table
                                    objects.push(object.company_display_name);
                                }
                            });
                            process(objects);
                        }
                    });
            $('.vendor_names').on('typeahead:selected', function (e, datum) {
                $('[name="expense_vendor_id"]').val(map[datum].company_id);
            });

        }
    }

    request.open("GET", "/index.php/web/company/json/vendor", true);

    request.setRequestHeader("content-type", "application/json");
    request.send();
}

function calculateTaxAmountForExpenseForm() {
    var expense_tax_type = document.getElementById('expense_tax_type').value;
    var expense_tax_rate_applicable = document.getElementById('expense_tax_rate_applicable');
    var expense_amount_paid = document.getElementById('expense_amount_paid').value;
    var tax_rate = expense_tax_rate_applicable.options[expense_tax_rate_applicable.selectedIndex].getAttribute('tax_percent');

    var expense_tax_amount = document.getElementById('expense_tax_amount');
    if (expense_tax_type == 'exclusive') {
        expense_tax_amount.value = expense_amount_paid * tax_rate / 100;
    } else if (expense_tax_type == 'inclusive') {
        expense_tax_amount.value = ((parseFloat(tax_rate) * parseFloat(expense_amount_paid)) / (100 + parseFloat(tax_rate))).toFixed(2);
    }
}

function loadTaxesForExpense() {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            interstate_taxes_for_expense = JSON.parse(request.responseText).interstate_taxes;
            intrastate_taxes_for_expense = JSON.parse(request.responseText).intrastate_taxes;
            loadVendorsForExpenseForm();
        }
    };
    request.open("GET", "/index.php/web/tax/json", true);
    request.send();
}


function loadOcGstStateIdForExpense() {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            var stateId = JSON.parse(request.responseText).state_id;
            EXP_OC_GST_STATE_ID = stateId;
            if (EXP_OC_GST_STATE_ID > 0) {	// continue only if GST ID is valid
                loadTaxesForExpense();
                calculateTaxAmountForExpenseForm();
            } else {
                alert('Failed to load OC gst state ID. Please contact system admin');
            }
        }
    };
    request.open("GET", "/index.php/web/OC/state", true);
    request.send();
}

