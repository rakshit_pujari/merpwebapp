
function initializeFormElements(id) {
    //alert(id);
    // Form components
    // ------------------------------

    // Switchery toggles
    var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
    elems.forEach(function (html) {
        var switchery = new Switchery(html);
    });


    // Bootstrap switch
    $(".switch").bootstrapSwitch();
    
    
    //
    // Contextual colors
    //

    // Primary
    $(".control-primary").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-primary-600 text-primary-800'
    });

    // Danger
    $(".control-danger").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-danger-600 text-danger-800'
    });

    // Success
    $(".control-success").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-success-600 text-success-800'
    });

    // Warning
    $(".control-warning").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-warning-600 text-warning-800'
    });

    // Info
    $(".control-info").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-info-600 text-info-800'
    });

    // Custom color
    $(".control-custom").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-indigo-600 text-indigo-800'
    });


    // Bootstrap multiselect
    $('.multiselect').multiselect({
        checkboxName: 'vali'
    });


    // Touchspin
    $(".touchspin-postfix").TouchSpin({
        min: 0,
        max: 100,
        step: 0.1,
        decimals: 2,
        postfix: '%'
    });

    // Select with search
    $('.select2').select2();

    $('.select').select2({//keep this below $('.select2').select2();
        minimumResultsForSearch: Infinity
    });
    // Styled checkboxes, radios
    $(".styled, .multiselect-container input").uniform({radioClass: 'choice'});


    // Styled file input
    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-blue'
    });

    //Format
    // Phone #
    $('.format-phone-number').formatter({
        pattern: '({{999}}) {{999}} - {{9999}}'
    });

    jQuery.validator.addMethod("lettersonlywithspace", function (value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
    }, "Only alphabetical characters");

    // Setup validation
    // ------------------------------

    // Initialize
    var validator = $(".form-validate-jquery").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function (error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent().parent().parent());
                } else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function (label) {
            label.addClass("validation-valid-label").text("Valid")
        },
        rules: {
            city_name: {
                minlength: 2,
                lettersonlywithspace: true
            },
            bank_account_number: {
                min: 4
            },
            employee_name: {
                minlength: 2,
                lettersonlywithspace: true
            },
            employee_location_id: {
                min: 1
            },
            employee_contact_number: {
                number: true,
                minlength: 5
            },
            employee_pincode: {
                number: true,
                minlength: 5
            },
            employee_password: {
                minlength: 5
            },
            employee_password_repeat: {
                minlength: 5,
                equalTo: "#employee_password"
            },
            employee_email_address: {
                email: true
            },
            bank_location_id: {
                min: 1
            },
            bank_email_address: {
                email: true
            },
            broker_location_id: {
                min: 1
            },
            broker_email_address: {
                email: true
            },
            broker_gst_number: {
                minlength: 15
            },
            company_location_id: {
                min: 1
            },
            company_gst_number: {
                minlength: 15
            },
            state: {
                minlength: 1
            },
            transporter_location_id: {
                min: 1
            },
            transporter_email_address: {
                email: true
            },
            transporter_gst_number: {
                minlength: 15
            },
            repeat_email: {
                equalTo: "#email"
            },
            minimum_characters: {
                minlength: 10
            },
            maximum_characters: {
                maxlength: 10
            },
            minimum_number: {
                min: 10
            },
            maximum_number: {
                max: 10
            },
            number_range: {
                range: [10, 20]
            },
            url: {
                url: true
            },
            date: {
                date: true
            },
            date_iso: {
                dateISO: true
            },
            numbers: {
                number: true
            },
            digits: {
                digits: true
            },
            creditcard: {
                creditcard: true
            },
            basic_checkbox: {
                minlength: 2
            },
            styled_checkbox: {
                minlength: 2
            },
            switchery_group: {
                minlength: 2
            },
            switch_group: {
                minlength: 2
            }
        },
        messages: {
            custom: {
                required: "This is a custom error message",
            },
            bank_account_number:"Please enter a valid account number",
            bank_location_id: "Please choose a location",
            broker_location_id: "Please choose a location",
            company_location_id: "Please choose a location",
            employee_location_id: "Please choose a location",
            transporter_location_id: "Please choose a location",
            employee_pincode: "Please enter a valid pin code",
            employee_contact_number: "Please enter a valid contact number"
        }
    });

    // Reset form
    $('#reset').on('click', function () {
        validator.resetForm();
    });



    var mvalidator = $(".modal-form-validate-jquery").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function (error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent().parent().parent());
                } else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function (label) {
            label.addClass("validation-valid-label").text("Valid")
        },
        rules: {
            modal_city_name: {
                minlength: 2,
                lettersonlywithspace: true
            },
            modal_state: {
                minlength: 1
            }
        },
        messages: {
            custom: {
                required: "This is a custom error message",
            },
            bank_location_id: "Please choose a location",
            broker_location_id: "Please choose a location",
            company_location_id: "Please choose a location",
            employee_location_id: "Please choose a location",
            transporter_location_id: "Please choose a location",
            employee_pincode: "Please enter a valid pin code",
            employee_contact_number: "Please enter a valid contact number"
        }
    });
    
    
    $('#employee_password').hover(function functionName() {
            //Change the attribute to text
            $('#employee_password').attr('type', 'text');
            $('.fontawesome').removeClass('icon-eye-blocked2').addClass('icon-eye2');
        }, function () {
            //Change the attribute back to password
            $('#employee_password').attr('type', 'password');
            $('.fontawesome').removeClass('icon-eye2').addClass('icon-eye-blocked2');
        }
    );
};


function refreshLocationDistrictAndState() {
    if (document.getElementById('city') == null || document.getElementById('state') == null
            || document.getElementById('district') == null)
        return;
    var district = document.getElementById('district');
    var state = document.getElementById('state');
    var city = document.getElementById('city');

    var selectedOption = city.options[city.selectedIndex];

    var districtValue = selectedOption.getAttribute("district");
    var stateValue = selectedOption.getAttribute("state");

    district.value = '';
    state.value = '';

    district.value = districtValue;
    state.value = stateValue;
}



function refreshSecondaryLocationDistrictAndState() {
    if (document.getElementById('secondaryFieldCity') == null || document.getElementById('secondaryFieldState') == null
            || document.getElementById('secondaryFieldDistrict') == null)
        return;
    var district = document.getElementById('secondaryFieldDistrict');
    var state = document.getElementById('secondaryFieldState');
    var city = document.getElementById('secondaryFieldCity');

    var selectedOption = city.options[city.selectedIndex];

    var districtValue = selectedOption.getAttribute("district");
    var stateValue = selectedOption.getAttribute("state");

    district.value = '';
    state.value = '';

    district.value = districtValue;
    state.value = stateValue;
}
function pad(n, c) {
    var p = 5;
    var pad_char = typeof c !== 'undefined' ? c : '0';
    var pad = new Array(1 + p).join(pad_char);
    return (pad + n).slice(-pad.length);
}

function removeOptions(selectbox) {
    var i;
    for (i = selectbox.options.length - 1; i >= 0; i--)
    {
        selectbox.remove(i);
    }
}

function refreshLocationList() {
    var modal_state = document.getElementById('modal_state');
    if (document.getElementById('modal_city_name').value == '' || modal_state.options[modal_state.selectedIndex].text == '') {
        return;
    }
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            var locationsArray = JSON.parse(request.responseText).locations;
            var city = document.getElementById('city');
            removeOptions(city);

            var chooseOption = document.createElement('option');
            chooseOption.value = "";
            chooseOption.innerHTML = "Choose a location";
            city.appendChild(chooseOption);

            var addedLocationIndex = 0;
            for (var i = 0; i < locationsArray.length; i++) {
                var location = locationsArray[i];
                var opt = document.createElement('option');
                opt.value = location.location_id;
                opt.innerHTML = location.city_name;
                opt.setAttribute("district", location.district);
                opt.setAttribute("state", location.state_name);
                city.appendChild(opt);

            }

            for (i = city.options.length - 1; i >= 0; i--) {
                var selectedOption = city.options[i];

                if (document.getElementById('modal_city_name').value == selectedOption.text &&
                        document.getElementById('modal_district').value == selectedOption.getAttribute("district") &&
                        modal_state.options[modal_state.selectedIndex].text == selectedOption.getAttribute("state")) {
                    addedLocationIndex = i;
                }
            }

            city.selectedIndex = addedLocationIndex;
            refreshLocationDistrictAndState();

            document.getElementById('modal_city_name').value = '';
            document.getElementById('modal_district').value = '';
            document.getElementById('modal_state').selectedIndex = 0;

            $('#modal_default').modal('hide');
        }
    }

    request.open("GET", "/index.php/web/location/json", true);

    request.setRequestHeader("content-type", "application/json");
    setTimeout(function () {
        request.send();
    }, 200);

}
