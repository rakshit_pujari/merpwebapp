
var manufacturingOrderDataTable;
var manufacturingOrderProductList = [];
var idManufacturingOrderProductMap = {};

function initialize_manufacturing_order_form(recipeId) {

    var manufacturingOrderDate = new Date();
    var manufacturingOrderDateString = $('#manufacturing_order_date_picker').val();
    if (manufacturingOrderDateString != null && manufacturingOrderDateString != "") {
        try {
            manufacturingOrderDate = $.datepicker.parseDate("yy-mm-dd", manufacturingOrderDateString);
        } catch (Err) {

        }
    }
    
    $('#manufacturing_order_date_picker').datepicker({
            dateFormat: 'yy-mm-dd'
    }).datepicker("setDate", manufacturingOrderDate);

    initializeFormValidator();
    initializeManufacturingOrderDataTable();
    initializeManufacturingOrderSelect2();
    
    if(recipeId!=null && recipeId!=''){
        getRecipeForManufacturing(recipeId, false);
    } else {
        calculateManufacturingNumbers();
    }
}

var recipe;
var recipe_entries;
function loadRecipeForManufacturing(element){
    
    manufacturingOrderDataTable.clear().draw();
    var recipeId = $('option:selected', element).attr('recipe-id');
    if(recipeId == '')
        return;
    getRecipeForManufacturing(recipeId, true);
}

function getRecipeForManufacturing(recipeId, loadEntries){    
    $.get("/index.php/web/recipe/json/" + recipeId, function (data, status) {
        if (status == 'success') {
            recipe = JSON.parse(data).recipe;
            
            recipe_entries = JSON.parse(data).recipe_entries;
            if(loadEntries){
                $('[name="manufacturing_order_product_quantity"]').val(recipe.recipe_product_quantity);
                for (var i = 0; i < recipe_entries.length; i++) {
                    addProductRowToManufacutringOrderTable(i, recipe_entries[i].re_linked_product_id, recipe_entries[i].product_name, 
                                recipe_entries[i].rate_per_unit, recipe_entries[i].re_linked_product_quantity, 
                                    recipe_entries[i].inventory_quantity, recipe_entries[i].uqc_text);
                }
            
                initializeManufacturingOrderDataTableSelect2();
            }
            calculateManufacturingNumbers();
        }
    });
}

function refreshForXUnits(){
    var manufacturing_order_product_quantity = parseFloat($('[name="manufacturing_order_product_quantity"]').val());
    manufacturingOrderDataTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
        manufacturingOrderDataTable.cell(rowIdx, 0).nodes().to$().find('input').val(recipe_entries[rowIdx].rate_per_unit);
        if(recipe!=null){
            manufacturingOrderDataTable.cell(rowIdx, 2).nodes().to$().find('input').val((recipe_entries[rowIdx].re_linked_product_quantity 
                    * manufacturing_order_product_quantity / recipe.recipe_product_quantity).toFixed(2));
        }
        manufacturingOrderDataTable.cell(rowIdx, 3).nodes().to$().find('input').val(recipe_entries[rowIdx].inventory_quantity);
    });    
}


function calculateManufacturingNumbers(){
    var max_quantity_estimate = -999999;
    var manufacturing_order_product_quantity = parseInt($('[name="manufacturing_order_product_quantity"]').val());
    var estimate_cost = 0;
    var manufacturing_order_linked_product_id = document.getElementById('manufacturing_order_linked_product_id');
    var uqc_text = $('option:selected', manufacturing_order_linked_product_id).attr('uqc-text');
    manufacturingOrderDataTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var rate_per_unit = manufacturingOrderDataTable.cell(rowIdx, 0).nodes().to$().find('input').val();
        var required_quantity = manufacturingOrderDataTable.cell(rowIdx, 2).nodes().to$().find('input').val();
        var inventory_quantity = manufacturingOrderDataTable.cell(rowIdx, 3).nodes().to$().find('input').val();
        if(max_quantity_estimate == -999999 || max_quantity_estimate > (inventory_quantity / recipe_entries[rowIdx].re_linked_product_quantity)){
            max_quantity_estimate = inventory_quantity / recipe_entries[rowIdx].re_linked_product_quantity;
        }
        
        estimate_cost+= (rate_per_unit * required_quantity);
    });
    
    if(max_quantity_estimate > 0) {
        document.getElementById("max_quantity_estimate").value = Math.floor(max_quantity_estimate) + ' ( ' + recipe.recipe_product_quantity + ' ' + uqc_text + ' per lot )';
    } else {
        if(recipe!=null && uqc_text!=null){
            document.getElementById("max_quantity_estimate").value = 0 + ' ( ' + recipe.recipe_product_quantity + ' ' + uqc_text + ' per lot )';
        }
    }
    
    document.getElementById("estimated_cost_per_unit").value = (estimate_cost / manufacturing_order_product_quantity).toFixed(2);;
    document.getElementById("estimated_cost").value = (estimate_cost).toFixed(2);
    //fill estimate cost and rate per unit
}

function initializeManufacturingOrderSelect2() {
    // Select with search
    $('.select2').select2();
    $('.select').select2({//keep this below $('.select2').select2();
        minimumResultsForSearch: Infinity
    });
}

function initializeManufacturingOrderDataTableSelect2() {
    $('.dtSelect2').select2();
    $('.dtSelect').select2({
        minimumResultsForSearch: Infinity
    });
}


function initializeManufacturingOrderDataTable() {
    manufacturingOrderDataTable = $('.manufacturingOrderDatatable').DataTable({
        autoWidth: true,
        order: [[0, "asc"]],
        searching: false,
        paging: false,
        bInfo: false,
        bSort: false
    });

}


function addProductRowToManufacutringOrderTable(rowIdx, productId, productName, rate_per_unit, quantity, inventory_quantity, uqc_text){
    var data = [];
    data[0] = rowIdx + 1 + '<input value = ' + rate_per_unit + ' readonly style = "display:none"></input>';
    data[1] = '<input class="form-control" value = "' + productName + '" readonly></input>';
    data[2] = '<input name = "manufacturing_order_entries[' + (rowIdx + 1) + '][' + productId + ']" min = "0" class = "form-control" type = "number" step = 0.01 value = ' + quantity +
                    ' onchange = "calculateManufacturingNumbers();"></input>';
    data[3] = inventory_quantity + ' ' + uqc_text + '<input value = ' + inventory_quantity + ' readonly style = "display:none"></input>';
    manufacturingOrderDataTable.row.add(data).draw(true);
    
}

function markManufacturingOrderAsComplete(manufacturing_order_id){
    
    swal({
        title: "Are you sure ?",
        text: "Are you sure you want to mark this order as complete ?",
        showCancelButton: true,
        confirmButtonColor: "#166dba",
        cancelButtonColor: "#166dba",
        confirmButtonText: "Yes",
        cancelButtonText: "Cancel",
        closeOnConfirm: true,
        closeOnCancel: true
    },
    function (isConfirm) {
        if (isConfirm) {
            setTimeout($.get("/index.php/web/ManufacturingOrder/complete/" + manufacturing_order_id, function (data, status) {
                swal({
                    title: "Manufacturing Order Completed",
                    text: "Manufacturing Order Completed Successfully",
                    confirmButtonColor: "#2196F3",
                    type: "success"
                }, function () {
                    window.location = '#!/ManufacturingOrder/preview/complete/' + manufacturing_order_id;
                });
            }), 1000);
        }
    });
}
