
var dataTable;
var chargesDataTable;
var counter = 1;
var taxIsExclusive = true;
var interStateTaxIsApplicable = false;
var OC_GST_STATE_ID = -1;
var advanceReceipts;

function initialize_invoice_form() {
    loadOcGstStateId();

    var currentDate = new Date();
    var currentDateString = $('#datePicker').val();
    if (currentDateString != null && currentDateString != "") {
        try {
            currentDate = $.datepicker.parseDate("yy-mm-dd", currentDateString)
        } catch (Err) {

        }
    }
    $('#datePicker').datepicker({
        dateFormat: 'yy-mm-dd'
    }).datepicker("setDate", currentDate);


    initializeFormValidator();
    
}

function initializeFormValidator() {

    // Initialize
    var validator = $(".form-validate-jquery").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function (error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent().parent().parent());
                } else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function (label) {
            label.addClass("validation-valid-label").text("Valid")
        },
        rules: {
            city_name: {
                minlength: 2,
                lettersonlywithspace: true
            },
            employee_name: {
                minlength: 2,
                lettersonlywithspace: true
            },
            employee_location_id: {
                min: 1
            },
            employee_contact_number: {
                number: true,
                min: 11111111,
                max: 9999999999
            },
            employee_pincode: {
                number: true,
                min: 111111,
                max: 999999
            },
            employee_password: {
                minlength: 5
            },
            employee_password_repeat: {
                equalTo: "#employee_password"
            },
            employee_email_address: {
                email: true
            },
            bank_location_id: {
                min: 1
            },
            bank_email_address: {
                email: true
            },
            broker_location_id: {
                min: 1
            },
            broker_email_address: {
                email: true
            },
            broker_gst_number: {
                minlength: 15
            },
            company_location_id: {
                min: 1
            },
            company_gst_number: {
                minlength: 15
            },
            state: {
                minlength: 1
            },
            transporter_location_id: {
                min: 1
            },
            transporter_email_address: {
                email: true
            },
            repeat_email: {
                equalTo: "#email"
            },
            minimum_characters: {
                minlength: 10
            },
            maximum_characters: {
                maxlength: 10
            },
            minimum_number: {
                min: 10
            },
            maximum_number: {
                max: 10
            },
            number_range: {
                range: [10, 20]
            },
            url: {
                url: true
            },
            date: {
                date: true
            },
            date_iso: {
                dateISO: true
            },
            numbers: {
                number: true
            },
            digits: {
                digits: true
            },
            creditcard: {
                creditcard: true
            },
            basic_checkbox: {
                minlength: 2
            },
            styled_checkbox: {
                minlength: 2
            },
            switchery_group: {
                minlength: 2
            },
            switch_group: {
                minlength: 2
            }
        },
        messages: {
            custom: {
                required: "This is a custom error message",
            },
            bank_location_id: "Please choose a location",
            broker_location_id: "Please choose a location",
            company_location_id: "Please choose a location",
            employee_location_id: "Please choose a location",
            transporter_location_id: "Please choose a location",
            employee_pincode: "Please enter a valid pin code",
            employee_contact_number: "Please enter a valid contact number"
        }
    });
}

function resetInvoiceLinkedCompanyId() {
    $('[name="invoice_linked_company_id"]').val('');
    rebuildAdvanceReceiptMultiselect();
}

function initializeSelect2() {
    // Select with search
    $('.select2').select2();
    $('.select').select2({//keep this below $('.select2').select2();
        minimumResultsForSearch: Infinity
    });
}

function initializeDataTableSelect2() {
    $('.dtSelect2').select2();
    $('.dtSelect').select2({
        minimumResultsForSearch: Infinity,
    });
}


function initializeDataTable() {
    dataTable = $('.spDatatable').DataTable({
        autoWidth: false,
        columnDefs: [{
                width: '150px',
                targets: [3, 4, 5, 6]
            },
            {
                className: "row text-center",
                targets: [8]
            }
        ],
        order: [[0, "asc"]],
        searching: false,
        paging: false,
        bInfo: false,
        bSort: false
    });

    chargesDataTable = $('#chargesDatatable').DataTable({
        autoWidth: false,
        columnDefs: [
            {
                width: '200px',
                targets: [0]
            },
            {
                className: "row text-center",
                targets: [4]
            }
        ],
        order: [[0, "asc"]],
        searching: false,
        paging: false,
        bInfo: false,
        bSort: false
    });
}

function loadAdvanceReceipts(){
    $.get("/index.php/web/AdvanceReceipt/json", function (data, status) {
        if (status == 'success') {
            advanceReceipts = JSON.parse(data).advance_receipts;
        }
    });
}

function loadCustomers() {

    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            var data = JSON.parse(request.responseText).companies;

            $('.customer_names').typeahead(
                    {
                        hint: true,
                        highlight: true,
                        minLength: 0
                    },
                    {
                        limit:100,
                        source: function (query, process) {
                            objects = [];
                            map = {};
                            //data = [{"id":1,"label":"machin ltd"},{"id":2,"label":"truc"}] // Or get your JSON dynamically and load it into this variable
                            $.each(data, function (i, object) {
                                var company = (object.company_display_name).toLowerCase();
                                if (company.includes(query.toLowerCase())) {
                                    map[object.company_display_name] = object;	// using company_display_name because company_display_name is unique key in company table
                                    objects.push(object.company_display_name);
                                }
                            });
                            process(objects);
                        }
                    });
            $('.customer_names').on('typeahead:selected', function (e, datum) {
                //alert(datum.value);
                //alert(map[datum].company_id);
                $('[name="invoice_linked_company_invoice_name"]').val(map[datum].company_invoice_name);
                $('[name="invoice_linked_company_id"]').val(map[datum].company_id);
                var address = '';
                if ((map[datum].company_billing_address)) {
                    address += map[datum].company_billing_address;
                    if (!address.trim().endsWith(",")) {
                        address = address + ', ';
                    }
                }
                if ((map[datum].company_billing_area)) {
                    address += map[datum].company_billing_area + ', ';
                }
                if ((map[datum].billing_city_name)) {
                    address += map[datum].billing_city_name + ', ';
                }
                if ((map[datum].billing_district)) {
                    address += map[datum].billing_district + ', ';
                }
                if ((map[datum].company_billing_pincode)) {
                    address += map[datum].company_billing_pincode;
                }

                if (address.trim().endsWith(",")) {
                    address = address.trim().slice(0, -1);
                }
                $('[name="invoice_linked_company_billing_address"]').val(address);
                var address = '';
                if ((map[datum].company_shipping_address)) {
                    address += map[datum].company_shipping_address;
                    if (!address.trim().endsWith(",")) {
                        address = address + ', ';
                        ;
                    }
                }
                if ((map[datum].company_shipping_area)) {
                    address += map[datum].company_shipping_area + ', ';
                }
                if ((map[datum].shipping_city_name)) {
                    address += map[datum].shipping_city_name + ', ';
                }
                if ((map[datum].shipping_district)) {
                    address += map[datum].shipping_district + ', ';
                }
                if ((map[datum].company_shipping_pincode)) {
                    address += map[datum].company_shipping_pincode;
                }

                if (address.trim().endsWith(",")) {
                    address = address.trim().slice(0, -1);
                }
                $('[name="invoice_linked_company_shipping_address"]').val(address);
                
                $('[name="invoice_billing_contact_person_name"]').val(map[datum].company_contact_person_name);
                $('[name="invoice_shipping_contact_person_name"]').val(map[datum].company_contact_person_name);
                $('[name="invoice_billing_contact_number"]').val(map[datum].company_contact_number);
                $('[name="invoice_shipping_contact_number"]').val(map[datum].company_contact_number);
                $('[name="invoice_linked_company_gstin"]').val(map[datum].company_gst_number);
                $('[name="invoice_linked_company_pan_number"]').val(map[datum].company_pan_number);
                
                //$('[name="invoice_linked_company_area"]').val(map[datum].company_area);
                //$('[name="invoice_linked_company_city"]').val(map[datum].city_name);
                //$('[name="invoice_linked_company_district"]').val(map[datum].district);
                //$('[name="invoice_linked_company_pincode"]').val(map[datum].company_pincode); 
                $('#invoice_linked_company_billing_state_id').val(map[datum].company_billing_state_id).trigger("change");
                $('#invoice_linked_company_shipping_state_id').val(map[datum].company_shipping_state_id).trigger("change");
                $('#invoice_linked_company_gst_supply_state_id').val(map[datum].company_gst_supply_state_id).trigger("change");
                $('#invoice_linked_company_payment_term_id').val(map[datum].company_payment_term_id).trigger("change");
                $('[name="invoice_linked_company_pincode"]').val(map[datum].company_pincode);
                
                rebuildAdvanceReceiptMultiselect(map[datum].company_id);
            });

        }
    }

    request.open("GET", "/index.php/web/company/json/customer", true);

    request.setRequestHeader("content-type", "application/json");
    request.send();
}

function rebuildAdvanceReceiptMultiselect(companyId) {
    var options = [];
    for (var i = 0; i < advanceReceipts.length; i++) {
        if(advanceReceipts[i].advance_receipt_linked_company_id == companyId){
            var option = {label: 'ADR' + String("00000" + advanceReceipts[i].advance_receipt_id).slice(-5) + ' - ( Balance -  ₹'+ advanceReceipts[i].balance_advance_receipt_amount + ' )', value: advanceReceipts[i].advance_receipt_id};
            options.push(option);
        }
    }
    $('#invoice_advance_receipt_ids').multiselect('dataprovider', options);
    //$('#invoice_advance_receipt_ids').multiselect('rebuild');
    $(".styled, .multiselect-container input").uniform({ radioClass: 'choice'});
}

function updateBrokerCommission() {
    var invoice_linked_broker_id = document.getElementById('invoice_linked_broker_id');
    var selectedOption = invoice_linked_broker_id.options[invoice_linked_broker_id.selectedIndex];
    var broker_commission = selectedOption.getAttribute("data-commission");
    document.getElementById('broker_commission').value = broker_commission;
}

function deleteRow(element) {
    dataTable.row($(element).parents('tr'))
            .remove()
            .draw();
    renumberTable();
    //refreshAndReIndexEntireTable();
    //calculateInvoiceNumbers();
}

function deleteChargeRow(element) {
    chargesDataTable.row($(element).parents('tr'))
            .remove()
            .draw();
    //refreshAndReIndexEntireTable();
    //calculateInvoiceNumbers();
}

function renumberTable() {
    dataTable.column(0).nodes().each(function (cell, i) {
        cell.innerHTML = i + 1;
    });
}

var productList = [];

var idProductMap = {};

function loadProductsList() {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            var productsArray = JSON.parse(request.responseText).products;
            productList = [];
            for (var i = 0; i < productsArray.length; i++) {
                var product = productsArray[i];
                productList.push(product);
                idProductMap[product.product_id] = product;
            }

            if (!dataTable.data().count()){
                updateProducts();//add 1 row by default if empty
                
                //add default charges
                for (var i = 0; i < chargeList.length; i++) {
                    if(chargeList[i].charge_is_default_type == 1){
                        updateCharges(null, chargeList[i].charge_id);
                    }
                }
            }

            updateTaxes();
            calculateInvoiceNumbers();
        }
    };
    request.open("GET", "/index.php/web/product/json/sales", true);
    request.send();
}

var interstate_taxes = null;
var intrastate_taxes = null;
function loadTaxes() {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            interstate_taxes = JSON.parse(request.responseText).interstate_taxes;
            intrastate_taxes = JSON.parse(request.responseText).intrastate_taxes;
            loadProductsList();
            loadCustomers();
            loadAdvanceReceipts();
        }
    };
    request.open("GET", "/index.php/web/tax/json", true);
    request.send();
}

var chargeList = null;
var idChargeMap = {};
function loadCharges() {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            chargeList = JSON.parse(request.responseText).charges;
            for (var i = 0; i < chargeList.length; i++) {
                var charge = chargeList[i];
                idChargeMap[charge.charge_id] = charge;
            }
        }
    };
    request.open("GET", "/index.php/web/charge/json/sales", true);
    request.send();
}


function loadOcGstStateId() {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            var stateId = JSON.parse(request.responseText).state_id;
            OC_GST_STATE_ID = stateId;
            if (OC_GST_STATE_ID > 0) {	// continue only if GST ID is valid
                initializeDataTable();
                loadTaxes();
                loadCharges();
                initializeSelect2();
                initializeDataTableSelect2();
            } else {
                alert('Failed to load OC gst state ID. Please contact system admin');
            }
        }
    };
    request.open("GET", "/index.php/web/OC/state", true);
    request.send();
}

function updateCharges(element, defaultChargeId){
    
    var chargesListSize = 0;
    chargesDataTable.column(0).nodes().each(function (cell, i) {
        chargesListSize++;
    });

    var rowIdx = chargesListSize;
    var chargeId = chargeList[0].charge_id;
    if(defaultChargeId){
        chargeId = defaultChargeId;
    }
    var applicableTaxId = parseInt(productList[0].intra_state_tax_id);
    var taxes = intrastate_taxes;
    if (interStateTaxIsApplicable) {
        applicableTaxId = parseInt(productList[0].inter_state_tax_id);
        taxes = interstate_taxes;
    }
    
    
    var taxableAmount = 0.01;
    var totalAmount = 0;
    //var tax_percent = 0;
    if (element != null) {
        rowIdx = $(element).parents('tr')[0].rowIndex - 1;
        chargeId = element.selectedOptions[0].value;	//$('#priorityList2 option:selected').val()
        taxableAmount = chargesDataTable.cell(rowIdx, 1).nodes().to$().find('input').val();
        applicableTaxId = chargesDataTable.cell(rowIdx, 2).nodes().to$().find('select').val();
        //tax_percent = parseFloat(chargesDataTable.cell(rowIdx, 2).nodes().to$().find('select')[0].selectedOptions[0].attributes['tax_value'].value);
        //total_amount = (1 + tax_percent/100) * taxableAmount;
    }

    //PRODUCT
    var chargeSelectList = document.createElement("select");
    chargeSelectList.setAttribute("onchange", "updateCharges(this);calculateInvoiceNumbers();");
    chargeSelectList.classList.add("form-control");
    chargeSelectList.classList.add("dtSelect2");
    //Create and append the options
    for (var i = 0; i < chargeList.length; i++) {
        var option = document.createElement("option");
        option.value = chargeList[i].charge_id;
        option.text = chargeList[i].charge_name;
        chargeSelectList.appendChild(option);

        if (chargeId == chargeList[i].charge_id) {
            option.selected = true;
            option.setAttribute("selected", "selected");
        }
    }
    
    var chargeStr = chargeSelectList.outerHTML;

    //TAXES
    var taxSelectList = document.createElement("select");
    taxSelectList.setAttribute("onchange", "calculateInvoiceNumbers();");
    taxSelectList.setAttribute("name", "charge_entries[" + (rowIdx + 1) + "][" + chargeId + "][2]");
    taxSelectList.classList.add("form-control");
    taxSelectList.classList.add("dtSelect");
    //var taxes = [0, 5, 12, 18, 28];
    //Create and append the options
    for (var i = 0; i < taxes.length; i++) {
        var option = document.createElement("option");
        option.value = taxes[i].tax_id;
        option.setAttribute("tax_value", taxes[i].tax_percent);
        option.text = taxes[i].tax_name;
        taxSelectList.appendChild(option);

        if (applicableTaxId == taxes[i].tax_id) {
            option.selected = true;
            option.setAttribute("selected", "selected");
        }
    }
    
    var taxStr = taxSelectList.outerHTML;

    var data = [];
    data[0] = chargeStr + '<input name = "charge_entries[' + (rowIdx + 1) + '][' + chargeId + '][0]" value = "' + idChargeMap[chargeId].charge_name + '" style = "display:none" ></input>';
    data[1] = '<input style = "min-width:100px" name = "charge_entries[' + (rowIdx + 1) + '][' + chargeId + '][1]" value = "' + taxableAmount + '" class = "form-control" min = "0.01" step ="0.01" type = "number" onchange = "calculateInvoiceNumbers();"></input>';
    data[2] = taxStr;
    data[3] = '';

    data[4] = '<a href="javascript: void(0)" onclick = "deleteChargeRow(this);refreshAndReIndexEntireTable();calculateInvoiceNumbers();"><i class="glyphicon glyphicon-trash"></i></a>';
    //console.log(data);
    if (element == null) {
        chargesDataTable.row.add(data).draw(true);
    } else {
        chargesDataTable.row(rowIdx).data(data).draw(true);
    } 

    //calculateInvoiceNumbers();
    initializeDataTableSelect2();
} 


/*
 0 - Product Name
 1 - Tax
 2 - HSN
 3 - rate
 4 - quantity
 5 - discount
 */
function updateProducts(element, updateFields) {

    var productsListSize = 0;
    dataTable.column(0).nodes().each(function (cell, i) {
        productsListSize++;
    });

    var rowIdx = productsListSize;
    var productId = productList[0].product_id;
    var applicable_tax_id = parseInt(productList[0].intra_state_tax_id);
    var taxes = intrastate_taxes;
    if (interStateTaxIsApplicable) {
        applicable_tax_id = parseInt(productList[0].inter_state_tax_id);
        taxes = interstate_taxes;
    }
    
    var quantity = 1;
    var discount = 0;
    var selling_price = idProductMap[productId].selling_price;
    if (element != null) {
        rowIdx = $(element).parents('tr')[0].rowIndex - 1;
        productId = element.selectedOptions[0].value;	//$('#priorityList2 option:selected').val()
        if (updateFields) {
            applicable_tax_id = parseInt(idProductMap[productId].intra_state_tax_id);
            if (interStateTaxIsApplicable) {
                applicable_tax_id = parseInt(idProductMap[productId].inter_state_tax_id);
            }
        } else {
            applicable_tax_id = dataTable.cell(rowIdx, 3).nodes().to$().find('select').val();
        }

        if (!updateFields && dataTable.cell(rowIdx, 4).nodes().to$().find('input').val())
            selling_price = dataTable.cell(rowIdx, 4).nodes().to$().find('input').val();
        else
            selling_price = idProductMap[productId].selling_price;
        quantity = dataTable.cell(rowIdx, 5).nodes().to$().find('input').val();
        discount = dataTable.cell(rowIdx, 7).nodes().to$().find('input').val();
    }

    //PRODUCT
    var productSelectList = document.createElement("select");
    productSelectList.setAttribute("onchange", "updateProducts(this, true);calculateInvoiceNumbers();");
    productSelectList.classList.add("form-control");
    productSelectList.classList.add("dtSelect2");
    //Create and append the options
    for (var i = 0; i < productList.length; i++) {
        var option = document.createElement("option");
        option.value = productList[i].product_id;
        option.text = productList[i].product_name;
        productSelectList.appendChild(option);

        if (productId == productList[i].product_id) {
            option.selected = true;
            option.setAttribute("selected", "selected");
        }
    }
    
    var productStr = productSelectList.outerHTML;

    //TAXES
    var taxSelectList = document.createElement("select");
    taxSelectList.setAttribute("onchange", "calculateInvoiceNumbers();");
    taxSelectList.setAttribute("name", "invoice_entries[" + (rowIdx + 1) + "][" + productId + "][1]");
    taxSelectList.classList.add("form-control");
    taxSelectList.classList.add("dtSelect");
    //var taxes = [0, 5, 12, 18, 28];
    //Create and append the options
    for (var i = 0; i < taxes.length; i++) {
        var option = document.createElement("option");
        option.value = taxes[i].tax_id;
        option.setAttribute("tax_value", taxes[i].tax_percent);
        option.text = taxes[i].tax_name;
        taxSelectList.appendChild(option);

        if (applicable_tax_id == taxes[i].tax_id) {
            option.selected = true;
            option.setAttribute("selected", "selected");
        }
    }
    
    var taxStr = taxSelectList.outerHTML;

    var data = [];
    data[0] = rowIdx + 1;
    data[1] = productStr + '<input name = "invoice_entries[' + data[0] + '][' + productId + '][0]" value = "' + idProductMap[productId].product_name + '" style = "display:none" ></input>';
    data[2] = '<a href = "master#!/product/hsn/' + idProductMap[productId].hsn_code + '">' + idProductMap[productId].hsn_code
            + '</a><input name = "invoice_entries[' + data[0] + '][' + productId + '][2]" value = "' + idProductMap[productId].hsn_code + '" style = "display:none" ></input>';
    data[3] = taxStr;
    data[4] = '<input name = "invoice_entries[' + data[0] + '][' + productId + '][3]" class = "form-control" type = "number" step = 0.01 min = 0.01 value = "' + selling_price + '" onchange = "calculateInvoiceNumbers()"></input>';
    data[5] = '<input name = "invoice_entries[' + data[0] + '][' + productId + '][4]" id = "quantity" class = "form-control" type = "number" step = 0.01 min = 0.01  value = "' + quantity + '" onchange = "calculateInvoiceNumbers()"></input>'
            + '<input name = "invoice_entries[' + data[0] + '][' + productId + '][5]" value = "' + idProductMap[productId].product_uqc_id + '" style = "display:none" ></input>'
            + '<input name = "invoice_entries[' + data[0] + '][' + productId + '][6]" value = "' + idProductMap[productId].uqc_text + '" style = "display:none" ></input>';
    data[6] = '';//idProductMap[productId].inventory_quantity + ' ' + idProductMap[productId].uqc_text!='null'? idProductMap[productId].uqc_text : '';
    data[7] = '<input name = "invoice_entries[' + data[0] + '][' + productId + '][7]" min = 0 max = 100 id = "discount" class = "form-control" type = "number" step = 0.01 value = "' + discount + '" onchange = "calculateInvoiceNumbers()"></input>';
    data[8] = 0;
    if (rowIdx == 0)
        data[9] = '';
    else
        data[9] = '<a href="javascript: void(0)" onclick = "deleteRow(this);refreshAndReIndexEntireTable();calculateInvoiceNumbers();"><i class="glyphicon glyphicon-trash"></i></a>';

    if (element == null) {
        dataTable.row.add(data).draw(true);
    } else {
        dataTable.row(rowIdx).data(data).draw(true);
    }

    //calculateInvoiceNumbers();
    initializeDataTableSelect2();
}

function updateTaxes() {
    var invoice_linked_company_gst_supply_state_id = document.getElementById('invoice_linked_company_gst_supply_state_id');
    var selectedStateId = (invoice_linked_company_gst_supply_state_id.options[invoice_linked_company_gst_supply_state_id.selectedIndex]).value;
    if (selectedStateId > 0) {
        interStateTaxIsApplicable = !(selectedStateId == OC_GST_STATE_ID);
    } else {
        interStateTaxIsApplicable = false;
    }

    var tax_type = document.getElementById('tax_type');
    var selectedOption = tax_type.options[tax_type.selectedIndex];
    if (selectedOption.value == 'inclusive') {
        taxIsExclusive = false;
    } else {
        taxIsExclusive = true;
    }
}

function refreshAndReIndexEntireTable(updateFields) {
    dataTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var element = dataTable.cell(rowIdx, 1).nodes().to$().find('select')[0];
        if (element == null) {
            alert("ERROR");
            return;
        }
        updateProducts(element, updateFields);
    });
    chargesDataTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var element = chargesDataTable.cell(rowIdx, 0).nodes().to$().find('select')[0];
        if (element == null) {
            alert("ERROR");
            return;
        }
        updateCharges(element);
    });
}

function calculateInvoiceNumbers() {
    if (taxIsExclusive) {
        $('#subTotalLabel').text("Sub Total ( Exclusive Of Tax )");
    } else {
        $('#subTotalLabel').text("Sub Total ( Inclusive Of Tax )");
    }
    var totalAmount = parseFloat(0);
    var totalTaxAmount = parseFloat(0);
    var postTotalTaxAmount = parseFloat(0);
    var taxAmountMap = {};
    dataTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var data = this.data();
        var product_id = dataTable.cell(rowIdx, 1).nodes().to$().find('select')[0].selectedOptions[0].value;
        var tax_select = dataTable.cell(rowIdx, 3).nodes().to$().find('select');	// GET ATTRIBUTE AND NOT VALUE
        var tax = $('option:selected', tax_select).attr('tax_value');
        var cost = dataTable.cell(rowIdx, 4).nodes().to$().find('input').val();
        var quantity = dataTable.cell(rowIdx, 5).nodes().to$().find('input').val();
        var inventory_text = 'NA';
        if (idProductMap[product_id].product_type == 'goods') {
            inventory_text = idProductMap[product_id].inventory_quantity + ' ' + (idProductMap[product_id].uqc_text != null ? idProductMap[product_id].uqc_text : '')
        }
        dataTable.cell(rowIdx, 6).data(inventory_text); //set inventory quantity
        var discount = dataTable.cell(rowIdx, 7).nodes().to$().find('input').val();

        var amount = ((cost * quantity) * (1 - (discount / 100))).toFixed(2);
        totalAmount = parseFloat(totalAmount) + parseFloat(amount);

        var taxAmount = parseFloat(0);
        if (taxIsExclusive) {
            taxAmount = amount * (tax / 100);
        } else {
            //(tax * amount) / ( 100 + tax )
            taxAmount = (parseFloat(tax) * parseFloat(amount)) / (100 + parseFloat(tax))
        }

        if (taxAmountMap[tax] == null) {
            taxAmountMap[tax] = taxAmount;
        } else {
            taxAmountMap[tax] = taxAmountMap[tax] + taxAmount;
        }

        totalTaxAmount = (parseFloat(totalTaxAmount) + parseFloat(taxAmount)).toFixed(2);
        dataTable.cell(rowIdx, 8).data('₹ ' + amount);
    });

    chargesDataTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var data = this.data();
        //var charge_id = chargesDataTable.cell(rowIdx, 0).nodes().to$().find('select')[0].selectedOptions[0].value;
        var taxableAmount = chargesDataTable.cell(rowIdx, 1).nodes().to$().find('input').val();
        var tax = chargesDataTable.cell(rowIdx, 2).nodes().to$().find('select')[0].selectedOptions[0].attributes['tax_value'].value;
        var taxAmountOnCharge = parseFloat(0);
        var totalChargeAmount = parseFloat(0);
        if (taxIsExclusive) {
            taxAmountOnCharge = taxableAmount * (tax / 100);
            totalChargeAmount = (parseFloat(taxableAmount) + parseFloat(taxAmountOnCharge)).toFixed(2);
        } else {
            //(tax * amount) / ( 100 + tax )
            taxAmountOnCharge = (parseFloat(tax) * parseFloat(taxableAmount)) / (100 + parseFloat(tax));
            totalChargeAmount = taxableAmount;
        } 

        if (taxAmountMap[tax] == null) {
            taxAmountMap[tax] = taxAmountOnCharge;
        } else {
            taxAmountMap[tax] = taxAmountMap[tax] + taxAmountOnCharge;
        }
 
        totalTaxAmount = parseFloat(totalTaxAmount) + parseFloat(taxAmountOnCharge);
        totalAmount = parseFloat(totalAmount) + parseFloat(taxableAmount);
        chargesDataTable.cell(rowIdx, 3).data('<span style = "white-space: nowrap;">₹ ' + totalChargeAmount + '</span>');
    });
    
    document.getElementById('totalInvoiceSubTotalAmount').innerHTML = '₹ ' + parseFloat(totalAmount).toFixed(2);

    var taxStr = '';

    for (var taxPercent in taxAmountMap) {
        //if(taxAmountMap[taxPercent]!=0){
        if (interStateTaxIsApplicable) {

            taxStr += '<div class="form-group col-lg-12">' +
                    '<label class="control-label col-lg-6">IGST@' + taxPercent + '%</label>' +
                    '<div class="col-lg-6">' +
                    '<h6 class = "no-margin text-bold">₹ ' + taxAmountMap[taxPercent].toFixed(2) + '<h6>' +
                    '</div>' +
                    '</div>';

        } else {
            taxStr += '<div class="form-group col-lg-12">' +
                    '<label class="control-label col-lg-6">SGST@' + (taxPercent / 2) + '%</label>' +
                    '<div class="col-lg-6">' +
                    '<h6 class = "no-margin text-bold">₹ ' + (taxAmountMap[taxPercent] / 2).toFixed(2) + '<h6>' +
                    '</div>' +
                    '</div>' +
                    '<div class="form-group col-lg-12">' +
                    '<label class="control-label col-lg-6">CGST@' + (taxPercent / 2) + '%</label>' +
                    '<div class="col-lg-6">' +
                    '<h6 class = "no-margin text-bold">₹ ' + (taxAmountMap[taxPercent] / 2).toFixed(2) + '<h6>' +
                    '</div>' +
                    '</div>';
            ;

        }
        //}
    }

    document.getElementById('taxDiv').innerHTML = taxStr;

    var transportationCharges = 0;
    if (document.getElementById('transportationCharges').value != "") {
        transportationCharges = document.getElementById('transportationCharges').value;
    }
    var adjustments = 0;
    if (document.getElementById('adjustments').value != "") {
        adjustments = document.getElementById('adjustments').value;
    }

    if (taxIsExclusive) {
        postTotalTaxAmount = parseFloat(totalTaxAmount) + parseFloat(totalAmount);
    } else {
        postTotalTaxAmount = totalAmount;
    }

    var invoiceTotal = parseFloat(postTotalTaxAmount) + parseFloat(transportationCharges) + parseFloat(adjustments);

    document.getElementById('invoiceTotal').innerHTML = '₹ ' + parseFloat(invoiceTotal).toFixed(2);
    document.getElementById('invoiceAmount').value = parseFloat(invoiceTotal).toFixed(2);

    if (invoiceTotal <= 0) {
        if (document.getElementById('buttonSaveDraftInvoice') != null)
            document.getElementById('buttonSaveDraftInvoice').disabled = true;

        document.getElementById('buttonSaveConfirmInvoice').disabled = true;
    } else {
        if (document.getElementById('buttonSaveDraftInvoice') != null)
            document.getElementById('buttonSaveDraftInvoice').disabled = false;

        document.getElementById('buttonSaveConfirmInvoice').disabled = false;
    }
}