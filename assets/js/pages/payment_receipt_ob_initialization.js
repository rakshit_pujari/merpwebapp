/**
 * 0 - broker
 * 1 - company
 * 2 - employee
 * 3 - transporter
 * 
 * @param {type} element
 * @param {type} coa_id
 * @param {type} transaction_type
 * @returns {undefined}
 */
function invalidatePRField(element, entity_id, pr_id, transaction_type){
    var name;
    if(transaction_type == 0){
        name = 'pr_ob[' + entity_id + '][' + pr_id + '][1]';
    } else if(transaction_type == 1){
        name = 'pr_ob[' + entity_id + '][' + pr_id + '][0]';
    }
    
    if(element.value != 0)
        document.getElementsByName(name)[0].disabled = true;
    else 
        document.getElementsByName(name)[0].disabled = false;
}