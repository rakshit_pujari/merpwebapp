function showDeleteDialog(entityClass, id) {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this record",
        showCancelButton: true,
        confirmButtonColor: "#166dba",
        cancelButtonColor: "#166dba",
        confirmButtonText: "Yes",
        cancelButtonText: "Cancel",
        closeOnConfirm: true,
        closeOnCancel: true
    },
    function (isConfirm) {
        if (isConfirm) {
            var request = new XMLHttpRequest();
            //NProgress.start();
            request.onreadystatechange = function () {
                //NProgress.inc();
                if (request.readyState == 4) {
                    var response = request.response;
                    if (request.status == 200) {
                        if (response.trim() == "success") {
                            //location.reload(); 
                            swal({
                                title: "Deleted",
                                text: "Record deleted successfully",
                                confirmButtonColor: "#2196F3",
                                type: "success"
                            });
                            
                            if(entityClass == 'inventory_adjustment'){
                                window.location = "#!Inventory/adjustment";
                            } else {
                                window.location = "#!" + entityClass + "/all";
                            }
                            return; 
                        }
                    }
                    //show error dialog box
                    swal({
                        title: "Error",
                        text: "This item could not be deleted since it is linked to other records in the system. Please delete those records first.",
                        confirmButtonColor: "#2196F3",
                        type: "error"
                    });
                }
            };
            
            $prefix = entityClass;
            if(entityClass == 'inventory_adjustment'){
                $prefix = 'Inventory/adjustment';
            }
            var url = $prefix + "/delete/" + id;
            if(entityClass == 'invoice'){
                url = "invoice/delete/draft/" + id;
            }
            if(entityClass == 'ProForma'){
                url = "ProForma/delete/draft/" + id;
            }
            if(entityClass == 'purchase'){
                url = "purchase/delete/draft/" + id;
            }
            if(entityClass == 'DebitNote'){
                url = "DebitNote/delete/draft/" + id;
            }
            if(entityClass == 'CreditNote'){
                url = "CreditNote/delete/draft/" + id;
            }
            request.open("DELETE", url, true);
            request.send();
        } else {

        }
    });
}