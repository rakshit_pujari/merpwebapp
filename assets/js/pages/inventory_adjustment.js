

function setupInventoryAdjustmentFormBehaviour(){
    toggleIaFormElementsVisibility();
    refreshReasonElement();
}

function calculateIaNumbers(calculateUsing){
    var ia_product_id = document.getElementById('ia_product_id');
    var selectedOption = ia_product_id.options[ia_product_id.selectedIndex];
    var adjustmentType = $('input[name=ia_type]:checked').val();
    
    //var inventory_asset_value = parseFloat(selectedOption.getAttribute("inventory_asset_value"));
    //var product_quantity = parseFloat(selectedOption.getAttribute("product_quantity"));
    var inventory_asset_value = document.getElementById('ia_stock_value').value;
    var product_quantity = document.getElementById('ia_stock_quantity').value;
    
    
    var ia_difference = document.getElementById('ia_difference');
    var ia_quantity = document.getElementById('ia_quantity');
    var ia_value = document.getElementById('ia_value');
    
    if(calculateUsing == 'difference'){
        ia_quantity.value = parseFloat(product_quantity) + parseFloat(ia_difference.value);
        ia_value.value = parseFloat(inventory_asset_value) + parseFloat(ia_difference.value);
    } else if (calculateUsing == 'total'){
        
        if(adjustmentType == 'quantity'){
            ia_difference.value = parseFloat(product_quantity) - parseFloat(ia_quantity.value);
        }  else if(adjustmentType == 'value'){
            ia_difference.value = parseFloat(inventory_asset_value) - parseFloat(ia_value.value);
        }
        
    }
}


function refreshReasonElement(){
    var ia_reason_select = document.getElementById('ia_reason_select');
    var selectedOption = ia_reason_select.options[ia_reason_select.selectedIndex];
    
    if(selectedOption.value == 'other'){
        document.getElementById('ia_reason_text').disabled = false;
        document.getElementById('ia_reason_select').name = '';
        document.getElementById('ia_reason_text').name = 'ia_reason';
    } else {
        document.getElementById('ia_reason_text').disabled = true;
        document.getElementById('ia_reason_select').name = 'ia_reason';
        document.getElementById('ia_reason_text').name = '';
    }
}

function toggleIaFormElementsVisibility(){
    
    var adjustmentType = $('input[name=ia_type]:checked').val();
    
    if(adjustmentType == 'quantity'){

        document.getElementById('ia_quantity').disabled = false;
        document.getElementById('ia_value').disabled = true;

        document.getElementById('ia_stock_quantity_container').style.display = 'block';
        document.getElementById('ia_quantity_container').style.display = 'block';
        document.getElementById('ia_stock_value_container').style.display = 'none';
        document.getElementById('ia_value_container').style.display = 'none';

    } else if(adjustmentType == 'value'){
        
        document.getElementById('ia_quantity').disabled = true;
        document.getElementById('ia_value').disabled = false;
        
        document.getElementById('ia_stock_quantity_container').style.display = 'none';
        document.getElementById('ia_quantity_container').style.display = 'none';
        document.getElementById('ia_stock_value_container').style.display = 'block';
        document.getElementById('ia_value_container').style.display = 'block';
    }
}

function refreshInventoryQuantityAndValuation() {
    if (document.getElementById('ia_product_id') == null || document.getElementById('ia_stock_value') == null
            || document.getElementById('ia_stock_quantity') == null)
        return;
    var ia_product_id = document.getElementById('ia_product_id');
    var ia_stock_value = document.getElementById('ia_stock_value');
    var ia_stock_quantity = document.getElementById('ia_stock_quantity');

    var selectedOption = ia_product_id.options[ia_product_id.selectedIndex];
    
    var inventory_asset_value = selectedOption.getAttribute("inventory_asset_value");
    var product_quantity = selectedOption.getAttribute("product_quantity");

    ia_stock_value.value = '';
    ia_stock_quantity.value = '';

    ia_stock_value.value = inventory_asset_value;
    ia_stock_quantity.value = product_quantity;
    
    //document.getElementById('ia_difference').value = '';
    document.getElementById('ia_quantity').value = '';
    document.getElementById('ia_value').value = ''; 
}