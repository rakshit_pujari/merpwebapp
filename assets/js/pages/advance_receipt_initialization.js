function initializeAdvanceReceiptForm() {
    var currentDate = new Date();
    var currentDateString = $('#datePicker').val();
    if (currentDateString != null && currentDateString != "") {
        try {
            currentDate = $.datepicker.parseDate("yy-mm-dd", currentDateString)
        } catch (Err) {

        }
    }
    $('#datePicker').datepicker({
        dateFormat: 'yy-mm-dd'
    }).datepicker("setDate", currentDate);
    initializeSelect2();
}

function updateAdvanceReceiptCompanyId(){
    $('#advance_receipt_linked_company_id_display').val($('[name="advance_receipt_linked_company_id"]').val());
}