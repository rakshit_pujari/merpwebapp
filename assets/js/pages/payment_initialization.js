function initializePaymentForm() {
    var currentDate = new Date();
    var currentDateString = $('#datePicker').val();
    if (currentDateString != null && currentDateString != "") {
        try {
            currentDate = $.datepicker.parseDate("yy-mm-dd", currentDateString)
        } catch (Err) {

        }
    }
    $('#datePicker').datepicker({
        dateFormat: 'yy-mm-dd'
    }).datepicker("setDate", currentDate);
    initializeSelect2();
    loadPurchaseCustomerMapForPaymentForm();
}

function loadPurchaseCustomerMapForPaymentForm() {
    $.get("/index.php/web/purchase/json/purchase_vendor_map", function (data, status) {
        if (status === 'success') {
            purchaseVendorMapForPaymentForm = JSON.parse(data).purchase_vendor_map;
            refreshPurchaseListForPayment();
        }
    });
}

function limitToPaymentBalance(element) {
    if (parseFloat($(element).val()) > parseFloat($(element).attr("payment_balance"))) {
        $(element).val($(element).attr("payment_balance"));
    }
    if (parseFloat($(element).val()) <= 0) {
        $(element).val(0);
        swal({
            title: "Invalid payment amount",
            text: "Payment amount has to be more than zero",
            confirmButtonColor: "#2196F3",
            type: "error"
        });
        if(document.getElementById('buttonSubmitPayment')!=null) 
            document.getElementById('buttonSubmitPayment').disabled = true;
    } else {
        if(document.getElementById('buttonSubmitPayment')!=null) 
            document.getElementById('buttonSubmitPayment').disabled = false;
    }
}
function updatePaymentBalanceAmount(element) {
    if(document.getElementById('buttonSubmitPayment')!=null) 
        document.getElementById('buttonSubmitPayment').disabled = true;

    var purchase_amount = element.selectedOptions[0].getAttribute("purchase_amount");
    var payment_balance = element.selectedOptions[0].getAttribute("payment_balance");
    var cn_amount = element.selectedOptions[0].getAttribute("cn_amount");
    var dn_amount = element.selectedOptions[0].getAttribute("dn_amount");
    var payment_amount = element.selectedOptions[0].getAttribute("payment_amount");
    var advance_payment_amount = element.selectedOptions[0].getAttribute("advance_payment_amount");

    $("input[name=payment_linked_purchase_amount]").val(purchase_amount);
    $("input[name=payment_amount]").val(payment_balance);
    $("input[name=payment_amount]").attr({
        "max": payment_balance,
        "min": 0,
        "payment_balance": payment_balance
    });

    $('input[name=payment_amount]').popover('destroy');
    setTimeout(function () {
        var popOverContent = 'Purchase Amount : ₹ ' + purchase_amount
                + '<br />(Less) Advance Payment : ₹ ' + advance_payment_amount
                + '<br />(Less) Debit Note Amount : ₹ ' + dn_amount + '<br />(Less) Amount Paid Till Date : ₹ ' + payment_amount
                //+ '<br />(Add) Credit Note Amount : ₹ ' + cn_amount
                + '<br/>_________________________________<br />Net Payable : ₹ ' + payment_balance;

        $('input[name=payment_amount]').popover({
            title: 'Amount Break Up',
            content: popOverContent,
            placement: 'top',
            html: true
        });
        $('input[name=payment_amount]').on('mouseenter', function () {
            $(this).popover('show')
        });
        $('input[name=payment_amount]').on('mouseleave', function () {
            $(this).popover('hide')
        });
    }, 200);

    if (payment_balance <= 0) {
        swal({
            title: "Purchase Amount Paid",
            text: "Total purchase amount has been paid by the customer. Cannot create payment.",
            confirmButtonColor: "#2196F3",
            type: "warning"
        });
    } else {
        document.getElementById('buttonSubmitPayment').disabled = false;
    }
}

var purchaseVendorMapForPaymentForm = new Array();
function refreshPurchaseListForPayment() {
    var payment_linked_company_name = document.getElementById('payment_linked_company_id');
    var selected_company_id = null;
    var selected_company_name = null;
    var selectedOption = payment_linked_company_name.options[payment_linked_company_name.selectedIndex];

    selected_company_id = selectedOption.value;
    selected_company_name = selectedOption.text;

    var payment_linked_purchase_id = document.getElementById('payment_linked_purchase_id');
    $(payment_linked_purchase_id).empty();
    addPlaceholder(payment_linked_purchase_id, 'a Purchase Invoice');

    if (purchaseVendorMapForPaymentForm !== null) {
        var purchaseOptGroup = document.createElement("OPTGROUP");
        purchaseOptGroup.label = 'Please select a vendor to load purchase invoice';

        for (var i = 0; i < purchaseVendorMapForPaymentForm.length; i++) {
            if(purchaseVendorMapForPaymentForm[i].payment_balance <= 0)    //show only purchase pending receipts
                continue;
            
            var option = document.createElement("option");
            option.value = purchaseVendorMapForPaymentForm[i].purchase_id;
            option.text = 'PUR' + pad(purchaseVendorMapForPaymentForm[i].purchase_id, 5);
            option.setAttribute("company_id", purchaseVendorMapForPaymentForm[i].purchase_linked_company_id);
            option.setAttribute("company_name", purchaseVendorMapForPaymentForm[i].purchase_linked_company_invoice_name);
            option.setAttribute("purchase_amount", purchaseVendorMapForPaymentForm[i].purchase_amount);
            option.setAttribute("payment_balance", purchaseVendorMapForPaymentForm[i].payment_balance);
            option.setAttribute("cn_amount", purchaseVendorMapForPaymentForm[i].cn_amount);
            option.setAttribute("dn_amount", purchaseVendorMapForPaymentForm[i].dn_amount);
            option.setAttribute("payment_amount", purchaseVendorMapForPaymentForm[i].payment_amount);
            option.setAttribute("advance_payment_amount", purchaseVendorMapForPaymentForm[i].advance_payment_amount);

            purchaseOptGroup.label = 'Purchase Invoice For Selected Vendor';
            if (selected_company_id === purchaseVendorMapForPaymentForm[i].purchase_linked_company_id
                    && purchaseVendorMapForPaymentForm[i].purchase_linked_company_id != null) {
                purchaseOptGroup.appendChild(option);
            } else if (selected_company_name.trim() === purchaseVendorMapForPaymentForm[i].purchase_linked_company_display_name.trim()) {
                purchaseOptGroup.appendChild(option);
            }
        }

        payment_linked_purchase_id.appendChild(purchaseOptGroup);
    }
}
