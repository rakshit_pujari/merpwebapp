function initializeReceiptForm() {
    var currentDate = new Date();
    var currentDateString = $('#datePicker').val();
    if (currentDateString != null && currentDateString != "") {
        try {
            currentDate = $.datepicker.parseDate("yy-mm-dd", currentDateString)
        } catch (Err) {

        }
    }
    $('#datePicker').datepicker({
        dateFormat: 'yy-mm-dd'
    }).datepicker("setDate", currentDate);

    var invoice_date = $('input[name=receipt_date]').attr('invoice-date');
    $('input[name=receipt_date]').datepicker("option", "minDate", new Date(invoice_date));
    initializeSelect2();
    loadInvoiceCustomerMapForReceiptForm();
}

function loadInvoiceCustomerMapForReceiptForm() {
    $.get("/index.php/web/invoice/json/invoice_customer_map", function (data, status) {
        if (status === 'success') {
            invoiceCustomerMapForReceiptForm = JSON.parse(data).invoice_customer_map;
            refreshInvoiceListForReceipt();
        }
    });
}

function limitToReceiptBalance(element) {
    if (parseFloat($(element).val()) > parseFloat($(element).attr("receipt_balance"))) {
        $(element).val($(element).attr("receipt_balance"));
    }
    if (parseFloat($(element).val()) <= 0) {
        $(element).val(0);
        swal({
            title: "Invalid receipt amount",
            text: "Receipt amount has to be more than zero",
            confirmButtonColor: "#2196F3",
            type: "error"
        });
        document.getElementById('buttonSubmitReceipt').disabled = true;
    } else {
        document.getElementById('buttonSubmitReceipt').disabled = false;
    }
}
function updateReceiptBalanceAmount(element) {

    document.getElementById('buttonSubmitReceipt').disabled = true;

    var invoice_amount = element.selectedOptions[0].getAttribute("invoice_amount");
    var invoice_date = element.selectedOptions[0].getAttribute("invoice_date");
    var receipt_balance = element.selectedOptions[0].getAttribute("receipt_balance");
    var cn_amount = element.selectedOptions[0].getAttribute("cn_amount");
    var dn_amount = element.selectedOptions[0].getAttribute("dn_amount");
    var receipt_amount = element.selectedOptions[0].getAttribute("receipt_amount");
    var advance_receipt_amount = element.selectedOptions[0].getAttribute("advance_receipt_amount");

    $("input[name=receipt_linked_invoice_amount]").val(invoice_amount);
    $("input[name=receipt_amount]").val(receipt_balance);
    $("input[name=receipt_amount]").attr({
        "max": receipt_balance,
        "min": 0,
        "receipt_balance": receipt_balance
    });

    $('input[name=receipt_amount]').popover('destroy');
    setTimeout(function () {
        var popOverContent = 'Invoice Amount : ₹ ' + invoice_amount
                + '<br />(Less) Advance Receipt : ₹ ' + advance_receipt_amount
                + '<br />(Less) Credit Note Amount : ₹ ' + cn_amount
                //+ '<br />(Add) Debit Note Amount : ₹ ' + dn_amount
                + '<br />(Less) Amount Received Till Date : ₹ ' + receipt_amount
                + '<br/>_________________________________<br />Net Receivable : ₹ ' + receipt_balance;

        $('input[name=receipt_amount]').popover({
            title: 'Amount Break Up',
            content: popOverContent,
            placement: 'top',
            html: true
        });
        $('input[name=receipt_amount]').on('mouseenter', function () {
            $(this).popover('show')
        });
        $('input[name=receipt_amount]').on('mouseleave', function () {
            $(this).popover('hide')
        });

        $('input[name=receipt_date]').datepicker("option", "minDate", new Date(invoice_date));
    }, 200);

    if (receipt_balance <= 0) {
        swal({
            title: "Invoice Amount Paid",
            text: "Total invoice amount has been paid by the customer. Cannot create receipt.",
            confirmButtonColor: "#2196F3",
            type: "warning"
        });
    } else {
        document.getElementById('buttonSubmitReceipt').disabled = false;
    }
}

var invoiceCustomerMapForReceiptForm = new Array();
function refreshInvoiceListForReceipt() {
    var receipt_linked_company_name = document.getElementById('receipt_linked_company_id');
    var selected_company_id = null;
    var selected_company_name = null;
    var selectedOption = receipt_linked_company_name.options[receipt_linked_company_name.selectedIndex];

    selected_company_id = selectedOption.value;
    selected_company_name = selectedOption.text;


    var receipt_linked_invoice_id = document.getElementById('receipt_linked_invoice_id');
    $(receipt_linked_invoice_id).empty();
    addPlaceholder(receipt_linked_invoice_id, 'an Invoice');

    if (invoiceCustomerMapForReceiptForm !== null) {
        var invoiceOptGroup = document.createElement("OPTGROUP");
        invoiceOptGroup.label = 'Please Select a customer to load invoices';

        for (var i = 0; i < invoiceCustomerMapForReceiptForm.length; i++) {
            if(invoiceCustomerMapForReceiptForm[i].receipt_balance <= 0)    //show only invoices pending receipts
                continue;
            
            var option = document.createElement("option");
            option.value = invoiceCustomerMapForReceiptForm[i].invoice_id;
            option.text = 'INV' + pad(invoiceCustomerMapForReceiptForm[i].invoice_id, 5);
            option.setAttribute("company_id", invoiceCustomerMapForReceiptForm[i].invoice_linked_company_id);
            option.setAttribute("company_name", invoiceCustomerMapForReceiptForm[i].invoice_linked_company_invoice_name);
            option.setAttribute("invoice_amount", invoiceCustomerMapForReceiptForm[i].invoice_amount);
            option.setAttribute("invoice_date", invoiceCustomerMapForReceiptForm[i].invoice_date);
            option.setAttribute("receipt_balance", invoiceCustomerMapForReceiptForm[i].receipt_balance);
            option.setAttribute("cn_amount", invoiceCustomerMapForReceiptForm[i].cn_amount);
            option.setAttribute("dn_amount", invoiceCustomerMapForReceiptForm[i].dn_amount);
            option.setAttribute("receipt_amount", invoiceCustomerMapForReceiptForm[i].receipt_amount);
            option.setAttribute("advance_receipt_amount", invoiceCustomerMapForReceiptForm[i].advance_receipt_amount);

            if (selected_company_id === invoiceCustomerMapForReceiptForm[i].invoice_linked_company_id
                    && invoiceCustomerMapForReceiptForm[i].invoice_linked_company_id!=null) {
                invoiceOptGroup.appendChild(option);
            } else if (selected_company_name.trim() === invoiceCustomerMapForReceiptForm[i].invoice_linked_company_display_name.trim()) {
                invoiceOptGroup.appendChild(option);
            }
        }

        receipt_linked_invoice_id.appendChild(invoiceOptGroup);
    }
}
