
function initialize_datatable(columnId) {

    // Table setup
    // ------------------------------
    var sorting_order = 'desc';
    if(columnId == null){
        columnId = 3;
        sorting_order = 'asc';
    }
    columnId = parseInt(columnId);
    // Initialize
    $('.masterDataTable').DataTable({
        autoWidth: true,

        // Individual column searching with selects
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var select = $('<select class="filter-select" data-placeholder="Filter"><option value=""></option></select>')
                        .appendTo($(column.footer()).not(':last-child').empty())
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                    );
                            //alert($(this).val());
                            column
                                    //.search( val ? '^'+val+'$' : '', true, false )
                                    .search(val)
                                    .draw();
                        });

                column.data().unique().sort().each(function (d, j) {
                    //alert(d);
                    var start = d.indexOf("<span>");
                    var end = d.indexOf("</span>");

                    var str = d.substring(start + 6, end);
                    if (start > 0 && end > 0) {
                        select.append('<option value="' + str + '">' + str + '</option>')
                    } else {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    }
                });
            });
        }, 

        buttons: [
            /*{
             extend: 'selectAll', 
             text: '<i class="icon-checkmark"></i></span>',
             className: 'btn bg-blue btn-icon'
             },
             {
             extend: 'selectNone',
             text: '<i class="icon-cross"></i></span>',
             className: 'btn bg-blue btn-icon'
             },*/
            {
                extend: 'colvis',
                text: '<i class="icon-three-bars"></i> <span class="caret"></span>',
                className: 'btn btn-icon',
                collectionLayout: 'fixed two-column'
            }
        ],

        order: [[columnId, sorting_order]], 
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
        },
        aLengthMenu: [
        [25, 50, 100, 200, -1],
        [25, 50, 100, 200, "All"]
        ],
        iDisplayLength: -1,
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            

            api.column(columnId, {page: 'current'}).data().each(function (group, i) {
                
                var start = group.indexOf("<span>");
                var end = group.indexOf("</span>");

                var str = group.substring(start + 6, end);
                if (start > 0 && end > 0) {
                    group = str;
                } 
                
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr class="active border-double"><td colspan="8" class="text-semibold">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });

            $('.select').select2({
                width: '150px',
                minimumResultsForSearch: Infinity
            });

            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function (settings) {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
            $('.select').select2().select2('destroy');
        },
        scrollY: 419, // messes up column search
        scrollCollapse: true
                /*select: {
                 style: 'multi'
                 }*/
    });

    /*// Individual column searching with text inputs
     $('.masterDataTable tfoot td').not(':last-child').each(function () {
     var title = $('.masterDataTable thead th').eq($(this).index()).text();
     $(this).html('<input type="text" class="form-control input-sm" placeholder="Search '+title+'" />');
     });
     var table = $('.masterDataTable').DataTable();
     table.columns().every( function () {
     var that = this;
     $('input', this.footer()).on('keyup change', function () {
     that.search(this.value).draw();
     });
     });*/



    // External table additions
    // ------------------------------

    // Enable Select2 select for the length option
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: 'auto'
    });

    // Enable Select2 select for individual column searching
    $('.filter-select').select2();

}


function initialize_datatable_no_grouping() {

    // Initialize
    $('.noGroupDataTable').DataTable({
        autoWidth: true,
        buttons: [
            {
                extend: 'colvis',
                text: '<i class="icon-three-bars"></i> <span class="caret"></span>',
                className: 'btn btn-icon',
                collectionLayout: 'fixed two-column'
            }
        ],
        order:[],
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
        },
        aLengthMenu: [
            [25, 50, 100, 200, -1],
            [25, 50, 100, 200, "All"]
        ],
        iDisplayLength: -1,
        preDrawCallback: function (settings) {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
            $('.select').select2().select2('destroy');
        }
    });



    // External table additions
    // ------------------------------

    // Enable Select2 select for the length option
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: 'auto'
    });

    // Enable Select2 select for individual column searching
    $('.filter-select').select2({
        allowClear: true
    });

}

