
var customerListForDebitNoteForm, vendorListForDebitNoteForm;
var debitNoteChargesDataTable;
var invoice_customer_map_for_debit_note_form, purchase_vendor_map_for_debit_note_form;
var debitNoteDataTable;
var DN_OC_GST_STATE_ID = -1;
var taxIsExclusiveForDebitNote = true;
var interStateTaxIsApplicableForDebitNote = false;

function initialize_debit_note_form() {
    loadOcGstStateIdForDebitNote();
    var currentDate = new Date();
    var currentDateString = $('#datePicker').val();
    if (currentDateString != null && currentDateString != "") {
        try {
            currentDate = $.datepicker.parseDate("yy-mm-dd", currentDateString)
        } catch (Err) {

        }
    }
    $('#datePicker').datepicker({
        dateFormat: 'yy-mm-dd'
    }).datepicker("setDate", currentDate);
    
    currentDate = new Date();
    var vendorCnDateString = $('#vendorCnDatePicker').val();
    if (vendorCnDateString != null && vendorCnDateString != "") {
        try {
            currentDate = $.datepicker.parseDate("yy-mm-dd", vendorCnDateString)
            $('#vendorCnDatePicker').datepicker({
                dateFormat: 'yy-mm-dd'
            }).datepicker("setDate", currentDate);
        } catch (Err) {

        }
    } else {
        $('#vendorCnDatePicker').datepicker({dateFormat: 'yy-mm-dd'});
    }
    
    
    initializeFormValidator();
    
}


var debitNoteChargeList = null;
var idDebitNoteChargeMap = {};
function loadDebitNoteCharges() {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            debitNoteChargeList = JSON.parse(request.responseText).charges;
            for (var i = 0; i < debitNoteChargeList.length; i++) {
                var charge = debitNoteChargeList[i];
                idDebitNoteChargeMap[charge.charge_id] = charge;
            }
        }
    };
    request.open("GET", "/index.php/web/charge/json/purchase", true);
    request.send();
}


function loadOcGstStateIdForDebitNote() {
    $.get("/index.php/web/OC/state", function (data, status) {
        if (status == 'success') {
            var stateId = JSON.parse(data).state_id;
            DN_OC_GST_STATE_ID = stateId;
            if (DN_OC_GST_STATE_ID > 0) {	// continue only if GST ID is valid
                initializeDebitNoteDataTable();
                if (!debitNoteDataTable.data().count()) {
                    loadCustomersAndVendorsForDebitNoteForm();
                    loadInvoiceCustomerAndPurchaseVendorMapForDebitNoteForm();
                    
                }

                loadTaxesForDebitNoteForm();
                initializeSelect2();
                loadDebitNoteCharges();
                updateTaxesInDebitNoteTable();
                calculateDebitNoteNumbers();
            } else {
                alert('Failed to load OC gst state ID. Please contact system admin');
            }

        }
    });
}

function pad(str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}

var interstate_taxes_for_debit_note = null;
var intrastate_taxes_for_debit_note = null;
function loadTaxesForDebitNoteForm() {
    $.get("/index.php/web/tax/json", function (data, status) {
        if (status == 'success') {
            var productsArray = JSON.parse(data).products;
            interstate_taxes_for_debit_note = JSON.parse(data).interstate_taxes;
            intrastate_taxes_for_debit_note = JSON.parse(data).intrastate_taxes;
            loadProductsListForDebitNoteForm();
        }
    });
}

function initializeDebitNoteDataTableSelect2() {
    // Select with search
    $('.dtSelect2').select2();
    $('.dtSelect').select2({//keep this below $('.select2').select2();
        minimumResultsForSearch: Infinity
    });
}

var productListForDebitNoteForm;
var idProductMapForDebitNoteForm = {};
function loadProductsListForDebitNoteForm() {
    $.get("/index.php/web/product/json/sales", function (data, status) {
        if (status == 'success') {
            var productsArray = JSON.parse(data).products;
            productListForDebitNoteForm = [];
            for (var i = 0; i < productsArray.length; i++) {
                var product = productsArray[i];
                productListForDebitNoteForm.push(product);
                idProductMapForDebitNoteForm[product.product_id] = product;
            }
        }
    });
}


function removeOptionsInDebitNoteForm(selectbox)
{
    $(selectbox).empty();
}

function updateTaxesInDebitNoteTable() {
    var debit_note_linked_company_gst_supply_state_id = document.getElementById('debit_note_linked_company_gst_supply_state_id');
    var debit_note_linked_company_billing_state_id = document.getElementById('debit_note_linked_company_billing_state_id');
    var selectedStateId = (debit_note_linked_company_gst_supply_state_id.options[debit_note_linked_company_gst_supply_state_id.selectedIndex]).value;
    var billingStateId = (debit_note_linked_company_billing_state_id.options[debit_note_linked_company_billing_state_id.selectedIndex]).value;
    if (selectedStateId > 0) {
        //interStateTaxIsApplicableForDebitNote = !(selectedStateId == DN_OC_GST_STATE_ID);
        interStateTaxIsApplicableForDebitNote = !(selectedStateId == billingStateId);
    } else {
        interStateTaxIsApplicableForDebitNote = false;
    }

    var tax_type = document.getElementById('debit_note_tax_type');
    var selectedOption = tax_type.options[tax_type.selectedIndex];
    if (selectedOption.value == 'inclusive') {
        taxIsExclusiveForDebitNote = false;
    } else {
        taxIsExclusiveForDebitNote = true;
    }
}

function loadInvoiceCustomerAndPurchaseVendorMapForDebitNoteForm() {
    $.get("/index.php/web/invoice/json/invoice_customer_map", function (data, status) {
        if (status == 'success') {
            invoice_customer_map_for_debit_note_form = JSON.parse(data).invoice_customer_map;
            refreshInvoiceListForDebitNote();
        }
    });

    $.get("/index.php/web/purchase/json/purchase_vendor_map", function (data, status) {
        if (status == 'success') {
            purchase_vendor_map_for_debit_note_form = JSON.parse(data).purchase_vendor_map;
            refreshInvoiceListForDebitNote();
        }
    });
}

function updateBrokerCommissionForDebitNoteForm() {
    var debit_note_linked_broker_id = document.getElementById('debit_note_linked_broker_id');
    var selectedOption = debit_note_linked_broker_id.options[debit_note_linked_broker_id.selectedIndex];
    var broker_commission = selectedOption.getAttribute("data-commission");
    document.getElementById('broker_commission').value = broker_commission;
}

function deleteDebitNoteRow(element) {
    debitNoteDataTable.row($(element).parents('tr'))
            .remove()
            .draw();
    renumberDebitNoteTable();
}

function deleteDebitNoteChargeRow(element) {
    debitNoteChargesDataTable.row($(element).parents('tr'))
            .remove()
            .draw();
}

function renumberDebitNoteTable() {
    debitNoteDataTable.column(0).nodes().each(function (cell, i) {
        cell.innerHTML = i + 1;
    });
}

function clearDebitNoteForm() {
    purchase_invoice_amount_for_debit_note_form = -1;
    $("input[name=dn_linked_company_invoice_name]").val('');
    //$("input[name=dn_date]").val('');
    $("textarea[name=dn_linked_company_billing_address]").val('');
    $("textarea[name=dn_linked_company_shipping_address]").val('');

    $("select[name=dn_linked_company_billing_state_id]").val('').trigger("change");
    $("select[name=dn_linked_company_shipping_state_id]").val('').trigger("change");

    $("select[name=dn_linked_company_gst_supply_state_id]").val('').trigger("change");

    $("select[name=dn_tax_type]").val('').trigger("change");
    ;
    $("select[name=dn_linked_broker_id]").val('').trigger("change");
    ;
    $("input[name=dn_linked_broker_commission]").val('');
    $("select[name=dn_linked_transporter_id]").val('').trigger("change");
    ;
    $("input[name=dn_lr_number]").val('');

    $("select[name=dn_linked_company_payment_term_id]").val('').trigger("change");

    $("input[name=dn_order_reference]").val('');
    $("input[name=dn_notes]").val('');

    $("input[name=dn_linked_employee_id]").val('');

    debitNoteDataTable.clear().draw();
}

function minMaxAmountForDebitNoteForm(element, min, max)
{
    if (parseInt(element.value) < min || isNaN(parseInt(element.value)))
        return min;
    else if (parseInt(element.value) > max)
        return max;
    else
        return element.value;
}

/*
 0 - Product Name
 1 - Tax
 2 - HSN
 3 - rate
 4 - quantity
 5 - discount
 */

var purchase_invoice_amount_for_debit_note_form = -1;
function loadInvoiceForDebitNoteForm() {
    purchase_invoice_amount_for_debit_note_form = -1;
    var debit_note_linked_invoice_id = document.getElementById('debit_note_linked_invoice_id');
    var selectedInvoice = debit_note_linked_invoice_id.options[debit_note_linked_invoice_id.selectedIndex];
    var invoiceId = selectedInvoice.value;
    clearDebitNoteForm();
    if (invoiceId != null) {
        if (document.getElementById('debit_against_sales').checked) {
            $.get("/index.php/web/invoice/json/invoice/" + invoiceId, function (data, status) {
                if (status == 'success') {
                    //alert(data);
                    var invoice = JSON.parse(data).invoice;
                    $("input[name=dn_linked_company_invoice_name]").val(invoice.invoice_linked_company_invoice_name);
                    //$("input[name=dn_date]").val(invoice.invoice_date.split(' ')[0]);
                    $("textarea[name=dn_linked_company_billing_address]").val(invoice.invoice_linked_company_billing_address);
                    $("textarea[name=dn_linked_company_shipping_address]").val(invoice.invoice_linked_company_shipping_address);

                    $("select[name=dn_linked_company_billing_state_id]").val(invoice.invoice_linked_company_billing_state_id).trigger("change");
                    $("select[name=dn_linked_company_shipping_state_id]").val(invoice.invoice_linked_company_shipping_state_id).trigger("change");

                    $("select[name=dn_linked_company_gst_supply_state_id]").val(invoice.invoice_linked_company_gst_supply_state_id).trigger("change");

                    $("select[name=dn_tax_type]").val(invoice.invoice_tax_type).trigger("change");
                    ;
                    $("select[name=dn_linked_broker_id]").val(invoice.invoice_linked_broker_id).trigger("change");
                    ;
                    $("input[name=dn_linked_broker_commission]").val(invoice.invoice_linked_broker_commission);
                    $("select[name=dn_linked_transporter_id]").val(invoice.invoice_linked_transporter_id).trigger("change");
                    ;
                    $("input[name=dn_lr_number]").val(invoice.invoice_lr_number);

                    $("select[name=dn_linked_company_payment_term_id]").val(invoice.invoice_linked_company_payment_term_id).trigger("change");

                    $("input[name=dn_order_reference]").val(invoice.invoice_order_reference);
                    $("input[name=dn_notes]").val(invoice.invoice_notes);

                    $("input[name=dn_linked_employee_id]").val(invoice.invoice_linked_employee_id);


                    var invoice_entries = JSON.parse(data).invoice_entries;
                    debitNoteDataTable.clear().draw();

                    for (var i = 0; i < invoice_entries.length; i++) {

                        var invoice_entry = invoice_entries[i];

                        var data = [];
                        data[0] = invoice_entry.ipe_entry_id;
                        data[1] = invoice_entry.ipe_product_name + '<input style = "display:none" name = "debit_note_entries[' + data[0] + '][' + invoice_entry.ipe_linked_product_id + '][0]" type = "text" value = "' + invoice_entry.ipe_product_name + '" readonly></input>';
                        data[2] = invoice_entry.ipe_product_hsn_code + '<input style = "display:none" name = "debit_note_entries[' + data[0] + '][' + invoice_entry.ipe_linked_product_id + '][1]" type = "text" value = "' + invoice_entry.ipe_product_hsn_code + '" readonly></input>';
                        data[3] = '<input name = "debit_note_entries[' + data[0] + '][' + invoice_entry.ipe_linked_product_id + '][2]" tax_value = ' + invoice_entry.ipe_tax_percent_applicable + ' style = "display:none" type = "text" value = "' + invoice_entry.ipe_tax_id_applicable + '" readonly></input>'
                                +
                                '<input class = "form-control" type = "text" value = "' + invoice_entry.ipe_tax_name_applicable + '" readonly></input>';
                        data[4] = '<input name = "debit_note_entries[' + data[0] + '][' + invoice_entry.ipe_linked_product_id + '][3]" readonly class = "form-control" type = "number" step = 0.01 value = "' + invoice_entry.ipe_product_rate + '" onchange = "calculateDebitNoteNumbers()"></input>';

                        data[5] = '<input name = "debit_note_entries[' + data[0] + '][' + invoice_entry.ipe_linked_product_id + '][4]" id = "quantity" class = "form-control" min = 0  type = "number" step = 0.01 value = "' + invoice_entry.ipe_product_quantity + '" onchange = "calculateDebitNoteNumbers()"></input>'
                                + '<input name = "debit_note_entries[' + data[0] + '][' + invoice_entry.ipe_linked_product_id + '][5]" value = "' + invoice_entry.ipe_product_uqc_id + '" style = "display:none" ></input>'
                                + '<input name = "debit_note_entries[' + data[0] + '][' + invoice_entry.ipe_linked_product_id + '][6]" value = "' + invoice_entry.ipe_product_uqc_text + '" style = "display:none" ></input>';

                        data[6] = '<input name = "debit_note_entries[' + data[0] + '][' + invoice_entry.ipe_linked_product_id + '][7]" readonly min = 0 max = 100 id = "discount" class = "form-control" type = "number" step = 0.01 value = "' + invoice_entry.ipe_discount + '" onchange = "calculateDebitNoteNumbers()"></input>';
                        data[7] = 0;
                        data[8] = '<input style = "display:none" name = "debit_note_entries[' + data[0] + '][' + invoice_entry.ipe_linked_product_id + '][8]" type = "text" value = "' + invoice_entry.ipe_id + '" readonly></input><a href="javascript: void(0)" onclick = "deleteDebitNoteRow(this);calculateDebitNoteNumbers();"><i class="glyphicon glyphicon-trash"></i></a>';

                        debitNoteDataTable.row.add(data).draw(true);

                    }
                    calculateDebitNoteNumbers();
                }
            });
        } else if (document.getElementById('debit_against_purchase').checked) {
            $.get("/index.php/web/purchase/json/purchase/" + invoiceId, function (data, status) {
                if (status == 'success') {
                    var purchase = JSON.parse(data).purchase;
                    $("input[name=dn_linked_company_invoice_name]").val(purchase.purchase_linked_company_invoice_name);
                    //$("input[name=dn_date]").val(purchase.purchase_date.split(' ')[0]);
                    $("textarea[name=dn_linked_company_billing_address]").val(purchase.purchase_linked_company_billing_address);
                    $("textarea[name=dn_linked_company_shipping_address]").val(purchase.purchase_linked_company_shipping_address);

                    $("select[name=dn_linked_company_billing_state_id]").val(purchase.purchase_linked_company_billing_state_id).trigger("change");
                    $("select[name=dn_linked_company_shipping_state_id]").val(purchase.purchase_linked_company_shipping_state_id).trigger("change");

                    $("select[name=dn_linked_company_gst_supply_state_id]").val(purchase.purchase_linked_company_gst_supply_state_id).trigger("change");

                    $("select[name=dn_tax_type]").val(purchase.purchase_tax_type).trigger("change");
                    ;
                    $("select[name=dn_linked_broker_id]").val(purchase.purchase_linked_broker_id).trigger("change");
                    $("input[name=dn_linked_broker_commission]").val(purchase.purchase_linked_broker_commission);
                    $("select[name=dn_linked_transporter_id]").val(purchase.purchase_linked_transporter_id).trigger("change");
                    ;
                    $("input[name=dn_lr_number]").val(purchase.purchase_lr_number);

                    $("select[name=dn_linked_company_payment_term_id]").val(purchase.purchase_linked_company_payment_term_id).trigger("change");

                    $("input[name=dn_order_reference]").val(purchase.purchase_order_reference);
                    $("input[name=dn_notes]").val(purchase.purchase_notes);

                    $("input[name=dn_linked_employee_id]").val(purchase.purchase_linked_employee_id);
                    purchase_invoice_amount_for_debit_note_form = purchase.purchase_amount;


                    var purchase_entries = JSON.parse(data).purchase_entries;
                    debitNoteDataTable.clear().draw();
                    var total_debit_note_balance = 0;
                    for (var i = 0; i < purchase_entries.length; i++) {

                        var purchase_entry = purchase_entries[i];

                        var data = [];
                        data[0] = purchase_entry.pe_entry_id;
                        data[1] = purchase_entry.pe_product_name + '<input style = "display:none" name = "debit_note_entries[' + data[0] + '][' + purchase_entry.pe_linked_product_id + '][0]" type = "text" value = "' + purchase_entry.pe_product_name + '" readonly></input>';
                        data[2] = purchase_entry.pe_product_hsn_code + '<input style = "display:none" name = "debit_note_entries[' + data[0] + '][' + purchase_entry.pe_linked_product_id + '][1]" type = "text" value = "' + purchase_entry.pe_product_hsn_code + '" readonly></input>';
                        data[3] = '<input name = "debit_note_entries[' + data[0] + '][' + purchase_entry.pe_linked_product_id + '][2]" tax_value = ' + purchase_entry.pe_tax_percent_applicable + ' style = "display:none" type = "text" value = "' + purchase_entry.pe_tax_id_applicable + '" readonly></input>'
                                +
                                '<input class = "form-control" type = "text" value = "' + purchase_entry.pe_tax_name_applicable + '" readonly></input>';
                        data[4] = '<input name = "debit_note_entries[' + data[0] + '][' + purchase_entry.pe_linked_product_id + '][3]" class = "form-control" readonly type = "number" step = 0.01 value = "' + purchase_entry.pe_product_rate + '" onchange = "calculateDebitNoteNumbers()"></input>';

                        var quantity_value = purchase_entry.pe_product_quantity;
                        if (parseInt(quantity_value) > parseInt(purchase_entry.debit_note_balance)) {
                            quantity_value = purchase_entry.debit_note_balance;
                        }

                        total_debit_note_balance = parseInt(total_debit_note_balance) + parseInt(quantity_value);

                        data[5] = '<input name = "debit_note_entries[' + data[0] + '][' + purchase_entry.pe_linked_product_id + '][4]" id = "quantity"  min = "0" max = "' + purchase_entry.debit_note_balance + '" onblur = "this.value = minMaxAmountForDebitNoteForm(this, 0, ' + purchase_entry.debit_note_balance + ')" class = "form-control" type = "number" step = 0.01 value = "' + quantity_value + '" onchange = "calculateDebitNoteNumbers()"></input>'
                                + '<input name = "debit_note_entries[' + data[0] + '][' + purchase_entry.pe_linked_product_id + '][5]" value = "' + purchase_entry.pe_product_uqc_id + '" style = "display:none" ></input>'
                                + '<input name = "debit_note_entries[' + data[0] + '][' + purchase_entry.pe_linked_product_id + '][6]" value = "' + purchase_entry.pe_product_uqc_text + '" style = "display:none" ></input>';


                        data[6] = '<input name = "debit_note_entries[' + data[0] + '][' + purchase_entry.pe_linked_product_id + '][7]" readonly min = 0 max = 100 id = "discount" class = "form-control" type = "number" step = 0.01 value = "' + purchase_entry.pe_discount + '" onchange = "calculateDebitNoteNumbers()"></input>';
                        data[7] = 0;
                        data[8] = '<input style = "display:none" name = "debit_note_entries[' + data[0] + '][' + purchase_entry.pe_linked_product_id + '][8]" type = "text" value = "' + purchase_entry.pe_id + '" readonly></input><a href="javascript: void(0)" onclick = "deleteDebitNoteRow(this);calculateDebitNoteNumbers();"><i class="glyphicon glyphicon-trash"></i></a>';

                        debitNoteDataTable.row.add(data).draw(true);

                    }
                    calculateDebitNoteNumbers();
                    if (total_debit_note_balance <= 0) {
                        swal({
                            title: "Error",
                            text: "No balance quantity left",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                        clearDebitNoteForm();
                    }
                }
            });

        }
    }
}

function refreshInvoiceListForDebitNote() {

    var debit_note_linked_company_name = document.getElementById('debit_note_linked_company_name');
    var selected_company_id = null;
    var selected_company_name = null;
    var selectedOption = debit_note_linked_company_name.options[debit_note_linked_company_name.selectedIndex];
    var preselectedValue = $(debit_note_linked_company_name).val();

    if (selectedOption.value !== '') {
        selected_company_id = selectedOption.value;
        selected_company_name = selectedOption.text;
    }


    var debit_note_linked_invoice_id = document.getElementById('debit_note_linked_invoice_id');
    removeOptionsInDebitNoteForm(debit_note_linked_invoice_id);
    addPlaceholder(debit_note_linked_invoice_id, 'an Invoice');

    if (invoice_customer_map_for_debit_note_form != null && document.getElementById('debit_against_sales').checked) {
        var invoiceOptGroup = document.createElement("OPTGROUP");
        invoiceOptGroup.label = 'Please Select a customer to load invoices';
        var invoiceAdded = false;
        for (var i = 0; i < invoice_customer_map_for_debit_note_form.length; i++) {
            var option = document.createElement("option");
            option.value = invoice_customer_map_for_debit_note_form[i].invoice_id;
            option.text = 'INV' + pad(invoice_customer_map_for_debit_note_form[i].invoice_id, 5);
            option.setAttribute("company_id", invoice_customer_map_for_debit_note_form[i].invoice_linked_company_id);
            option.setAttribute("company_name", invoice_customer_map_for_debit_note_form[i].invoice_linked_company_invoice_name);

            invoiceOptGroup.label = 'Invoice For Selected Customer';
            if (selected_company_id == invoice_customer_map_for_debit_note_form[i].invoice_linked_company_id
                    && invoice_customer_map_for_debit_note_form[i].invoice_linked_company_id!=null) {
                invoiceOptGroup.appendChild(option);
            } else if (selected_company_name.trim() == invoice_customer_map_for_debit_note_form[i].invoice_linked_company_display_name.trim()) {
                invoiceOptGroup.appendChild(option);
            }
        }

        debit_note_linked_invoice_id.appendChild(invoiceOptGroup);
    }

    if (purchase_vendor_map_for_debit_note_form != null && document.getElementById('debit_against_purchase').checked) {
        var purchaseOptGroup = document.createElement("OPTGROUP");
        purchaseOptGroup.label = 'Please Select a customer to load purchase invoices';
        for (var i = 0; i < purchase_vendor_map_for_debit_note_form.length; i++) {
            var option = document.createElement("option");
            option.value = purchase_vendor_map_for_debit_note_form[i].purchase_id;
            option.text = 'PUR' + pad(purchase_vendor_map_for_debit_note_form[i].purchase_id, 5);
            option.setAttribute("company_id", purchase_vendor_map_for_debit_note_form[i].purchase_linked_company_id);
            option.setAttribute("company_name", purchase_vendor_map_for_debit_note_form[i].purchase_linked_company_invoice_name);

            purchaseOptGroup.label = 'Purchase For Selected Vendor';
            if (selected_company_id == purchase_vendor_map_for_debit_note_form[i].purchase_linked_company_id
                    && purchase_vendor_map_for_debit_note_form[i].purchase_linked_company_id!=null) {
                purchaseOptGroup.appendChild(option);
            } else if (selected_company_name.trim() == purchase_vendor_map_for_debit_note_form[i].purchase_linked_company_display_name.trim()) {
                purchaseOptGroup.appendChild(option);
            }
        }
        debit_note_linked_invoice_id.appendChild(purchaseOptGroup);
    }
}

function initializeDebitNoteDataTable() {
    debitNoteDataTable = $('.debitNoteDatatable').DataTable({
        autoWidth: false,
        columnDefs: [{
                width: '150px',
                targets: [3, 4, 5, 6]
            },
            {
                className: "row text-center",
                targets: [8]
            }
        ],
        order: [[0, "asc"]],
        searching: false,
        paging: false,
        bInfo: false,
        bSort: false
    });

    debitNoteChargesDataTable = $('#debitNoteChargesDatatable').DataTable({
        autoWidth: false,
        columnDefs: [
            {
                width: '200px',
                targets: [0]
            },
            {
                className: "row text-center",
                targets: [4]
            }
        ],
        order: [[0, "asc"]],
        searching: false,
        paging: false,
        bInfo: false,
        bSort: false
    });
}


function loadCustomersAndVendorsForDebitNoteForm() {

    $.get("/index.php/web/invoice/json/customer", function (data, status) {
        if (status == 'success') {
            customerListForDebitNoteForm = JSON.parse(data).customers;
            refreshCustomerListForDebitNote();
        }
    });

    $.get("/index.php/web/purchase/json/vendor", function (data, status) {
        if (status == 'success') {
            vendorListForDebitNoteForm = JSON.parse(data).vendors;
            refreshCustomerListForDebitNote();
        }
    });
}

function addPlaceholder(element, company_type) {
    var option = document.createElement("option");
    option.value = '';
    option.text = 'Choose ' + company_type;
    option.setAttribute("company_id", '');
    element.appendChild(option);
}

function refreshAndReIndexEntireDebitNoteTable(updateFields) {
    /*debitNoteDataTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var element = purchaseDataTable.cell(rowIdx, 1).nodes().to$().find('select')[0];
        if (element == null) {
            alert("ERROR");
            return;
        }
        updatePurchaseProducts(element, updateFields);
    });
    */
    debitNoteChargesDataTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var element = debitNoteChargesDataTable.cell(rowIdx, 0).nodes().to$().find('select')[0];
        if (element == null) {
            alert("ERROR");
            return;
        }
        updateDebitNoteCharges(element);
    });
}


function refreshCustomerListForDebitNote() {

    var debit_note_linked_company_name = document.getElementById('debit_note_linked_company_name');
    removeOptionsInDebitNoteForm(debit_note_linked_company_name);
    addPlaceholder(debit_note_linked_company_name, 'a Company');
    if (document.getElementById('debit_against_sales').checked) {
        if (customerListForDebitNoteForm != null) {
            addPlaceholder(document.getElementById('debit_note_linked_company_id'), 'a Customer');
            for (var i = 0; i < customerListForDebitNoteForm.length; i++) {
                var option = document.createElement("option");
                option.value = customerListForDebitNoteForm[i].invoice_linked_company_id;
                option.text = customerListForDebitNoteForm[i].invoice_linked_company_display_name;
                debit_note_linked_company_name.appendChild(option);

            }
        }
    } else if (document.getElementById('debit_against_purchase').checked) {
        if (vendorListForDebitNoteForm != null) {
            addPlaceholder(document.getElementById('debit_note_linked_company_id'), 'a Vendor');
            for (var i = 0; i < vendorListForDebitNoteForm.length; i++) {
                var option = document.createElement("option");
                option.value = vendorListForDebitNoteForm[i].purchase_linked_company_id;
                option.text = vendorListForDebitNoteForm[i].purchase_linked_company_display_name;
                debit_note_linked_company_name.appendChild(option);
            }
        }
    }
}

function toggleElementsForDebitNote() {
    if (document.getElementById('debit_against_sales').checked) {
        $("select[name=debit_note_linked_broker_id]").prop('disabled', false);
        $("input[name=debit_note_linked_broker_commission]").prop('disabled', false);
    } else if (document.getElementById('debit_against_purchase').checked) {
        $("select[name=debit_note_linked_broker_id]").prop('disabled', true);
        $("input[name=debit_note_linked_broker_commission]").prop('disabled', true);
    } else {
        alert("Error");
    }
}

function updateCustomerIdForDebitNoteForm() {
    var debit_note_linked_company_name = document.getElementById('debit_note_linked_company_name');
    var selectedOption = debit_note_linked_company_name.options[debit_note_linked_company_name.selectedIndex];
    document.getElementById('debit_note_linked_company_id').value = selectedOption.value;
}

function updateDebitNoteCharges(element, defaultDebitNoteChargeId){
    
    var debitNoteChargesListSize = 0;
    debitNoteChargesDataTable.column(0).nodes().each(function (cell, i) {
        debitNoteChargesListSize++;
    });

    var rowIdx = debitNoteChargesListSize;
    var debitNoteChargeId = debitNoteChargeList[0].charge_id;
    if(defaultDebitNoteChargeId){
        debitNoteChargeId = defaultDebitNoteChargeId;
    }
    var applicableTaxId = parseInt(productListForDebitNoteForm[0].intra_state_tax_id);
    var taxes = intrastate_taxes_for_debit_note;
    if (interStateTaxIsApplicableForDebitNote) {
        applicableTaxId = parseInt(productListForDebitNoteForm[0].inter_state_tax_id);
        taxes = interstate_taxes_for_debit_note;
    }
    
    
    var taxableAmount = 0.01;
    var totalAmount = 0;
    //var tax_percent = 0;
    if (element != null) {
        rowIdx = $(element).parents('tr')[0].rowIndex - 1;
        debitNoteChargeId = element.selectedOptions[0].value;	//$('#priorityList2 option:selected').val()
        taxableAmount = debitNoteChargesDataTable.cell(rowIdx, 1).nodes().to$().find('input').val();
        applicableTaxId = debitNoteChargesDataTable.cell(rowIdx, 2).nodes().to$().find('select').val();
        //tax_percent = parseFloat(debitNoteChargesDataTable.cell(rowIdx, 2).nodes().to$().find('select')[0].selectedOptions[0].attributes['tax_value'].value);
        //total_amount = (1 + tax_percent/100) * taxableAmount;
    }

    //PRODUCT
    var debitNoteChargeSelectList = document.createElement("select");
    debitNoteChargeSelectList.setAttribute("onchange", "updateDebitNoteCharges(this);calculateDebitNoteNumbers();");
    debitNoteChargeSelectList.classList.add("form-control");
    debitNoteChargeSelectList.classList.add("dtSelect2");
    //Create and append the options
    for (var i = 0; i < debitNoteChargeList.length; i++) {
        var option = document.createElement("option");
        option.value = debitNoteChargeList[i].charge_id;
        option.text = debitNoteChargeList[i].charge_name;
        debitNoteChargeSelectList.appendChild(option);

        if (debitNoteChargeId == debitNoteChargeList[i].charge_id) {
            option.selected = true;
            option.setAttribute("selected", "selected");
        }
    }
    
    var debitNoteChargeStr = debitNoteChargeSelectList.outerHTML;

    //TAXES
    var taxSelectList = document.createElement("select");
    taxSelectList.setAttribute("onchange", "calculateDebitNoteNumbers();");
    taxSelectList.setAttribute("name", "charge_entries[" + (rowIdx + 1) + "][" + debitNoteChargeId + "][2]");
    taxSelectList.classList.add("form-control");
    taxSelectList.classList.add("dtSelect");
    //var taxes = [0, 5, 12, 18, 28];
    //Create and append the options
    for (var i = 0; i < taxes.length; i++) {
        var option = document.createElement("option");
        option.value = taxes[i].tax_id;
        option.setAttribute("tax_value", taxes[i].tax_percent);
        option.text = taxes[i].tax_name;
        taxSelectList.appendChild(option);

        if (applicableTaxId == taxes[i].tax_id) {
            option.selected = true;
            option.setAttribute("selected", "selected");
        }
    }
    
    var taxStr = taxSelectList.outerHTML;

    var data = [];
    data[0] = debitNoteChargeStr + '<input name = "charge_entries[' + (rowIdx + 1) + '][' + debitNoteChargeId + '][0]" value = "' + idDebitNoteChargeMap[debitNoteChargeId].charge_name + '" style = "display:none" ></input>';
    data[1] = '<input style = "min-width:100px" name = "charge_entries[' + (rowIdx + 1) + '][' + debitNoteChargeId + '][1]" value = "' + taxableAmount + '" class = "form-control" min = "0.01" step ="0.01" type = "number" onchange = "calculateDebitNoteNumbers();"></input>';
    data[2] = taxStr;
    data[3] = '';

    data[4] = '<a href="javascript: void(0)" onclick = "deleteDebitNoteChargeRow(this);calculateDebitNoteNumbers();"><i class="glyphicon glyphicon-trash"></i></a>';
    //console.log(data);
    if (element == null) {
        debitNoteChargesDataTable.row.add(data).draw(true);
    } else {
        debitNoteChargesDataTable.row(rowIdx).data(data).draw(true);
    } 

    //calculateDebitNoteNumbers();
    initializeDebitNoteDataTableSelect2();
} 


function calculateDebitNoteNumbers() {
    if (taxIsExclusiveForDebitNote) {
        $('#subTotalLabel').text("Sub Total ( Exclusive Of Tax )");
    } else {
        $('#subTotalLabel').text("Sub Total ( Inclusive Of Tax )");
    }
    var totalAmount = parseFloat(0);
    var totalTaxAmount = parseFloat(0);
    var postTotalTaxAmount = parseFloat(0);
    var taxAmountMap = {};
    debitNoteDataTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var data = this.data();

        var tax_input = debitNoteDataTable.cell(rowIdx, 3).nodes().to$().find('input');	// GET ATTRIBUTE AND NOT VALUE
        var tax = $(tax_input).attr('tax_value');
        var cost = debitNoteDataTable.cell(rowIdx, 4).nodes().to$().find('input').val();
        var quantity = debitNoteDataTable.cell(rowIdx, 5).nodes().to$().find('input').val();
        var discount = debitNoteDataTable.cell(rowIdx, 6).nodes().to$().find('input').val();

        var amount = ((cost * quantity) * (1 - (discount / 100))).toFixed(2);
        totalAmount = parseFloat(totalAmount) + parseFloat(amount);

        var taxAmount = parseFloat(0);
        if (taxIsExclusiveForDebitNote) {
            taxAmount = amount * (tax / 100);
        } else {
            //(tax * amount) / ( 100 + tax )
            taxAmount = (parseFloat(tax) * parseFloat(amount)) / (100 + parseFloat(tax))
        }

        if (taxAmountMap[tax] == null) {
            taxAmountMap[tax] = taxAmount;
        } else {
            taxAmountMap[tax] = taxAmountMap[tax] + taxAmount;
        }

        totalTaxAmount = (parseFloat(totalTaxAmount) + parseFloat(taxAmount)).toFixed(2);
        debitNoteDataTable.cell(rowIdx, 7).data('₹ ' + amount);
    });
    
    debitNoteChargesDataTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var data = this.data();
        //var charge_id = chargesDataTable.cell(rowIdx, 0).nodes().to$().find('select')[0].selectedOptions[0].value;
        var taxableAmount = debitNoteChargesDataTable.cell(rowIdx, 1).nodes().to$().find('input').val();
        var tax = debitNoteChargesDataTable.cell(rowIdx, 2).nodes().to$().find('select')[0].selectedOptions[0].attributes['tax_value'].value;
        var taxAmountOnCharge = parseFloat(0);
        var totalChargeAmount = parseFloat(0);
        if (taxIsExclusiveForDebitNote) {
            taxAmountOnCharge = taxableAmount * (tax / 100);
            totalChargeAmount = (parseFloat(taxableAmount) + parseFloat(taxAmountOnCharge)).toFixed(2);
        } else {
            //(tax * amount) / ( 100 + tax )
            taxAmountOnCharge = (parseFloat(tax) * parseFloat(taxableAmount)) / (100 + parseFloat(tax));
            totalChargeAmount = taxableAmount;
        } 

        if (taxAmountMap[tax] == null) {
            taxAmountMap[tax] = taxAmountOnCharge;
        } else {
            taxAmountMap[tax] = taxAmountMap[tax] + taxAmountOnCharge;
        }
 
        totalTaxAmount = parseFloat(totalTaxAmount) + parseFloat(taxAmountOnCharge);
        totalAmount = parseFloat(totalAmount) + parseFloat(taxableAmount);
        debitNoteChargesDataTable.cell(rowIdx, 3).data('<span style = "white-space: nowrap;">₹ ' + totalChargeAmount + '</span>');
    });

    document.getElementById('totalDebitNoteSubTotalAmount').innerHTML = '₹ ' + parseFloat(totalAmount).toFixed(2);

    var taxStr = '';

    for (var taxPercent in taxAmountMap) {
        //if(taxAmountMap[taxPercent]!=0){
        if (interStateTaxIsApplicableForDebitNote) {

            taxStr += '<div class="form-group col-lg-12">' +
                    '<label class="control-label col-lg-6">IGST@' + taxPercent + '%</label>' +
                    '<div class="col-lg-6">' +
                    '<h6 class = "no-margin text-bold">₹ ' + taxAmountMap[taxPercent].toFixed(2) + '<h6>' +
                    '</div>' +
                    '</div>';

        } else {
            taxStr += '<div class="form-group col-lg-12">' +
                    '<label class="control-label col-lg-6">SGST@' + (taxPercent / 2) + '%</label>' +
                    '<div class="col-lg-6">' +
                    '<h6 class = "no-margin text-bold">₹ ' + (taxAmountMap[taxPercent] / 2).toFixed(2) + '<h6>' +
                    '</div>' +
                    '</div>' +
                    '<div class="form-group col-lg-12">' +
                    '<label class="control-label col-lg-6">CGST@' + (taxPercent / 2) + '%</label>' +
                    '<div class="col-lg-6">' +
                    '<h6 class = "no-margin text-bold">₹ ' + (taxAmountMap[taxPercent] / 2).toFixed(2) + '<h6>' +
                    '</div>' +
                    '</div>';
            ;

        }
        //}
    }

    document.getElementById('taxDiv').innerHTML = taxStr;

    var transportationCharges = 0;
    if (document.getElementById('transportationCharges').value != "") {
        transportationCharges = document.getElementById('transportationCharges').value;
    }
    var adjustments = 0;
    if (document.getElementById('adjustments').value != "") {
        adjustments = document.getElementById('adjustments').value;
    }

    if (taxIsExclusiveForDebitNote) {
        postTotalTaxAmount = parseFloat(totalTaxAmount) + parseFloat(totalAmount);
    } else {
        postTotalTaxAmount = totalAmount;
    }

    var debitNoteTotal = parseFloat(postTotalTaxAmount) + parseFloat(transportationCharges) + parseFloat(adjustments);

    document.getElementById('debitNoteTotal').innerHTML = '₹ ' + parseFloat(debitNoteTotal).toFixed(2);
    document.getElementById('debitNoteAmount').value = parseFloat(debitNoteTotal).toFixed(2);

    if (debitNoteTotal <= 0) {
        if (document.getElementById('buttonSaveDraftDn') != null)
            document.getElementById('buttonSaveDraftDn').disabled = true;
        if (document.getElementById('buttonPreviewDraftDn') != null)
            document.getElementById('buttonPreviewDraftDn').disabled = true;
        if (document.getElementById('buttonSaveConfirmDn') != null)
            document.getElementById('buttonSaveConfirmDn').disabled = true;
    } else {
        if (parseFloat(purchase_invoice_amount_for_debit_note_form) >= 0) {
            if (debitNoteTotal > parseFloat(purchase_invoice_amount_for_debit_note_form)) {
                swal({
                    title: "Invoice Amount Exceeded",
                    text: "Debit note total has exceeded Invoice amount - ₹ " + purchase_invoice_amount_for_debit_note_form,
                    confirmButtonColor: "#2196F3",
                    type: "warning"
                });
            }
        }
        if (document.getElementById('buttonSaveDraftDn') != null) {
            document.getElementById('buttonSaveDraftDn').disabled = false;
        }

        if (document.getElementById('buttonPreviewDraftDn') != null) {
            document.getElementById('buttonPreviewDraftDn').disabled = false;
        }

        if (document.getElementById('buttonSaveConfirmDn') != null) {
            document.getElementById('buttonSaveConfirmDn').disabled = false;
        }

    }
}