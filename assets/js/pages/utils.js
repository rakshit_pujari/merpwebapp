 
 
function verifyGstin(element, entityClassName, fieldName, entityId, entityIdFieldName){
    document.getElementById('buttonSubmit').disabled = false;
    document.getElementById('buttonSubmit').innerHTML = 'Save<i class="icon-arrow-right14 position-right"></i>';
    
    var gstin = element.value;
    if(gstin.trim() != ''){
        
        if(document.getElementById('buttonSubmit')!=null) {
            document.getElementById('buttonSubmit').innerHTML = 'Validating GSTIN <i class="fa fa-clock-o position-right">';
            document.getElementById('buttonSubmit').disabled = true;
        }
        
        $.get("tax/gst/validate/" + gstin, function(result){
            document.getElementById('buttonSubmit').innerHTML = 'Save<i class="icon-arrow-right14 position-right"></i>';
            if(result.trim() == 'valid'){
                checkIfUnique(element, entityClassName, fieldName, entityId, entityIdFieldName)
            } else {
                showMessage("warning", "Invalid GSTIN", "You have entered an invalid GSTIN - " + gstin);
                document.getElementById('buttonSubmit').disabled = true;
            }
        });
    }
}

function showMessage(type, title, text){
    swal({
        title: title,
        text: text,
        confirmButtonColor: "#2196F3",
        type: type
    }, function () {
        //window.location = '#!/' + docType + '/all';
    });
}

/**
 * 
 * @param {type} entityClass
 * @param {type} finalDestination
 * @returns {undefined}
 */
function submitForm(entityClass, finalDestination) {
    $("#merpForm").on("submit", function (e) {
        e.preventDefault();
    });
    
    if ($('#merpForm').valid()) {
        $("#merpForm").find(':submit').prop( "disabled", true);
        var formData = new FormData($('#merpForm')[0]);
        $.ajax({
            type: 'POST',
            url: $('#merpForm')[0].action,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                var docId = parseInt(response);
                if(isNaN(docId)){
                    docId = '';
                }
                swal({
                    title: toTitleCase(entityClass.replace('_', ' ')) + " Saved",
                    text: toTitleCase(entityClass.replace('_', ' ')) + " Saved Successfully",
                    confirmButtonColor: "#2196F3",
                    type: "success"
                }, function () {
                    if (finalDestination) {
                        window.location = '#!/' + finalDestination + docId;
                    } else {
                        window.location = '#!/' + entityClass + '/all';
                    }
                });

            },
            error: function (data) {
                $("#merpForm").find(':submit').prop("disabled", false);
                $.post("master/error", {error_text: data.responseText, error_type: 'master', error_url: $('#merpForm')[0].action}, function(result){
                    //alert(result);
                });
                swal({
                    title: "Error Saving " + toTitleCase(entityClass.replace('_', ' ')),
                    text: "Couldn't Save " + toTitleCase(entityClass.replace('_', ' ')) + ". Please contact system admin.",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                }, function () {
                    //window.location = '#!/' + entityClass + '/all';
                });
            }
        }).done(function (response) {
            $("#merpForm").find(':submit').prop("disabled", false);
        });
    }
}

/**
 * 
 * @param {type} docType
 * @param {type} action
 * @returns {undefined}
 */
function confirmAndContinueForDocForm(docType, action) {
    $("#merpDocForm").on("submit", function (e) {
        e.preventDefault();
    });
    if ($('#merpDocForm').valid()) {
        swal({
            title: "Are you sure ?",
            text: "Are you sure you want to continue ?",
            showCancelButton: true,
            confirmButtonColor: "#166dba",
            cancelButtonColor: "#166dba",
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function (isConfirm) {
            if (isConfirm) {
                submitDocForm(docType, action);
            }
        });
    }
}
/**
 * 
 * @param {type} docType
 */
function submitDocForm(docType, action) {
    $("#merpDocForm").on("submit", function (e) {
        e.preventDefault();
    });

    if ($('#merpDocForm').valid()) {
        //var payload = $('#merpDocForm').serialize();
        //payload+= "&transaction="+action;
        $("#merpDocForm").find(':submit').prop("disabled", true);
        var formData = new FormData($('#merpDocForm')[0]);
        if(action!=null)
            formData.append('transaction', action);

        $.ajax({
            type: 'post',
            url: $('#merpDocForm')[0].action,
            //data: payload,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
            },
            error: function (data) {
                $("#merpDocForm").find(':submit').prop("disabled", false);
                //alert(data.responseText);
                $.post("master/error", {error_text: data.responseText, error_type: 'doc', error_url: $('#merpDocForm')[0].action}, function(result){
                    //alert(result);
                });
                swal({
                    title: "Error connecting to Server ",
                    text: "Couldn't reach server. Please contact system admin.",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                }, function () {
                    //window.location = '#!/' + docType + '/all';
                });
            }
        }).done(function (response) {
            $("#merpDocForm").find(':submit').prop("disabled", false);
            var docId = parseInt(response);
            if (docId) {
                if (action == 'draft') {
                    swal({
                        title: toTitleCase(docType.replace('_', ' ')) + " Saved",
                        text: toTitleCase(docType.replace('_', ' ')) + " Saved Successfully",
                        confirmButtonColor: "#2196F3",
                        type: "success"
                    }, function () {
                        window.location = '#!/' + toTitleCase(docType.replace('_', ' ')).replace(' ', '') + '/all';
                    });
                } else if (action == 'previewDraft') {
                    window.location = '#!/' + toTitleCase(docType.replace('_', ' ')).replace(' ', '') + '/preview/draft/' + docId;
                }  else if (action == 'previewConfirm') {
                    window.location = '#!/' + toTitleCase(docType.replace('_', ' ')).replace(' ', '') + '/preview/' + docId;
                } else if (action == 'confirm') {
                    swal({
                        title: toTitleCase(docType.replace('_', ' ')) + " Confirmed",
                        text: toTitleCase(docType.replace('_', ' ')) + " has been confirmed",
                        confirmButtonColor: "#2196F3",
                        type: "success"
                    }, function () {
                        window.location = '#!/' + toTitleCase(docType.replace('_', ' ')).replace(' ', '') + '/preview/confirm/' + docId;
                    });
                }
            } else {
                swal({
                    title: "Error Saving " + toTitleCase(docType.replace('_', ' ')),
                    text: "Couldn't Save " + toTitleCase(docType.replace('_', ' ')) + ". Please contact system admin.",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                }, function () {
                    //window.location = '#!/' + docType + '/all';
                });
            }
        });

    }
}

/**
 * 
 * @param {type} name
 * @param {type} url
 * @returns {String}
 */
function getParameterByName(name, url) {
    if (!url)
        url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
    if (!results)
        return null;
    if (!results[2])
        return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

/**
 * Used in employee rights as of now
 * @param {type} source
 * @param {type} children
 * @returns {undefined}
 */
function toggleCheckboxSelection(source, children) {
    checkboxes = document.getElementsByClassName(children);
    //for(var child_checkbox in checkboxes){
    Array.prototype.forEach.call(checkboxes, function (child_checkbox) {
        $(child_checkbox).prop("checked", source.checked);
    });
    $.uniform.update();
}

/**
 * Used in masters to check if field value is unique
 * @param {type} element
 * @param {type} entityClassName
 * @param {type} fieldName
 * @returns {undefined}
 */
function checkIfUnique(element, entityClassName, fieldName, entityId, entityIdFieldName) {
    if(element.value.trim() == '')
        return;
    if(document.getElementById('buttonSubmit')!=null) {
        document.getElementById('buttonSubmit').innerHTML = 'Verifying<i class="fa fa-clock-o position-right">';
        document.getElementById('buttonSubmit').disabled = true;
    }
    
    $.post("master/unique", {user_entered_data: element.value, entity_class_name: entityClassName, field_name: fieldName, entity_id: entityId, entity_id_field_name: entityIdFieldName}, function (result) {
        document.getElementById('buttonSubmit').innerHTML = 'Save<i class="icon-arrow-right14 position-right"></i>';
        if (result.trim() == 'success') {
           if(document.getElementById('buttonSubmit')!=null) {
               document.getElementById('buttonSubmit').disabled = false;
           } 
        } else {
            swal({
                title: "Error",
                text: titleCase(fieldName.replace("_", " ").replace("_", " ").replace("_", " ")) + " '" + element.value + "' already exists",
                confirmButtonColor: "#2196F3",
                type: "warning"
            });
            if(document.getElementById('buttonSubmit')!=null) {
                document.getElementById('buttonSubmit').disabled = true;
            }
        }
    });
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function toTitleCase(str) {
    return str.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

function titleCase(str) {
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
        // You do not need to check if i is larger than splitStr length, as your for does that for you
        // Assign it back to the array
        splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    // Directly return the joined string
    return splitStr.join(' ');
}

function initializeMultiSelect() {

     // Select All and Filtering features
    $('.multiselect-select-all-filtering').multiselect({
        includeSelectAllOption: true,
        enableFiltering: true,
        templates: {
            filter: '<li class="multiselect-item multiselect-filter"><i class="icon-search4"></i> <input class="form-control" type="text"></li>'
        },
        onSelectAll: function() {
            $.uniform.update();
        }
    });


    // Related plugins
    // ------------------------------
    
    // Styled checkboxes and radios
    $(".styled, .multiselect-container input").uniform({ radioClass: 'choice'});

}


function initializeColoredRadio(){
        //
    // Contextual colors
    //

    // Primary
    $(".control-primary").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-primary-600 text-primary-800'
    });

    // Danger
    $(".control-danger").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-danger-600 text-danger-800'
    });

    // Success
    $(".control-success").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-success-600 text-success-800'
    });

    // Warning
    $(".control-warning").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-warning-600 text-warning-800'
    });

    // Info
    $(".control-info").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-info-600 text-info-800'
    });

    // Custom color
    $(".control-custom").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-indigo-600 text-indigo-800'
    });

}

function updateCopyType(copyType){
    document.getElementById('copyType').innerHTML = copyType;
}
