
function initializePobForm(){
    var currentDate = new Date();
    var currentDateString = $('#datePicker').val();
    if(currentDateString != null && currentDateString != ""){
        try{
            currentDate = $.datepicker.parseDate( "yy-mm-dd", currentDateString)
        } catch(Err){
            
        }
    }
    $('#datePicker').datepicker({
        dateFormat: 'yy-mm-dd'
    }).datepicker("setDate", currentDate);
}

function calculateProductBalanceFields(element, productId){
    
    if(document.getElementsByName('pob[' + productId + '][0]')[0].value == '') document.getElementsByName('pob[' + productId + '][0]')[0].value = 0;    //quantity
    if(document.getElementsByName('pob[' + productId + '][1]')[0].value == '') document.getElementsByName('pob[' + productId + '][1]')[0].value = 0;    //opening stock
    if(document.getElementsByName('pob[' + productId + '][2]')[0].value == '') document.getElementsByName('pob[' + productId + '][2]')[0].value = 0;    //arpu
    
    var quantity = document.getElementsByName('pob[' + productId + '][0]')[0].value;
    var openingStockValue = document.getElementsByName('pob[' + productId + '][1]')[0].value;
    var arpu = document.getElementsByName('pob[' + productId + '][2]')[0].value;
  
    if(element==document.getElementsByName('pob[' + productId + '][2]')[0])
        if(quantity > 0 
            && arpu > 0){
            document.getElementsByName('pob[' + productId + '][1]')[0].value = (parseFloat(arpu) * parseInt(quantity)).toFixed(2);
        }
    
    if(element==document.getElementsByName('pob[' + productId + '][1]')[0])
        if(quantity > 0 
            && openingStockValue > 0){
            document.getElementsByName('pob[' + productId + '][2]')[0].value = parseFloat(parseFloat(openingStockValue) / parseInt(quantity)).toFixed(2);
        }
}