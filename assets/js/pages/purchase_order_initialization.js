
var purchaseOrderDataTable;
var purchaseOrderProductList = [];
var idPurchaseOrderProductMap = {};

function initialize_purchase_order_form() {

    var purchaseOrderDate = new Date();
    var purchaseOrderDateString = $('#purchase_order_date_picker').val();
    if (purchaseOrderDateString != null && purchaseOrderDateString != "") {
        try {
            purchaseOrderDate = $.datepicker.parseDate("yy-mm-dd", purchaseOrderDateString);
        } catch (Err) {

        }
    }
    $('#purchase_order_date_picker').datepicker({
        dateFormat: 'yy-mm-dd'
    }).datepicker("setDate", purchaseOrderDate);

    var purchaseOrderRevisionDateString = $('#purchase_order_revision_date').val();
    if (purchaseOrderRevisionDateString != null && purchaseOrderRevisionDateString != "") {
        try {
            var purchaseOrderBillDate = $.datepicker.parseDate("yy-mm-dd", purchaseOrderRevisionDateString);
            $('#purchase_order_revision_date').datepicker({
                dateFormat: 'yy-mm-dd'
            }).datepicker("setDate", purchaseOrderBillDate);
        } catch (Err) {

        }
    } else {
        $('#purchase_order_revision_date').datepicker({
                dateFormat: 'yy-mm-dd'
            })
    }
    
    initializeFormValidator();
    initializePurchaseOrderDataTable();
    initializePurchaseOrderSelect2();
    loadCustomersForPurchaseOrder();
    loadPurchaseOrderProductsList();
}

function resetPurchaseOrderLinkedCompanyId() {
    $('[name="purchase_order_linked_company_id"]').val('');
}

function initializePurchaseOrderSelect2() {
    // Select with search
    $('.select2').select2();
    $('.select').select2({//keep this below $('.select2').select2();
        minimumResultsForSearch: Infinity
    });
}

function initializePurchaseOrderDataTableSelect2() {
    $('.dtSelect2').select2();
    $('.dtSelect').select2({
        minimumResultsForSearch: Infinity
    });
}


function initializePurchaseOrderDataTable() {
    purchaseOrderDataTable = $('.purchaseOrderDatatable').DataTable({
        autoWidth: false,
        columnDefs: [
            {
                className: "row text-center",
                targets: [4]
            }
        ],
        order: [[0, "asc"]],
        searching: false,
        paging: false,
        bInfo: false,
        bSort: false
    });

}

function loadCustomersForPurchaseOrder() {

    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            var data = JSON.parse(request.responseText).companies;

            $('.vendor_names').typeahead(
                    {
                        hint: true,
                        highlight: true,
                        minLength: 0
                    },
                    {
                        source: function (query, process) {
                            objects = [];
                            map = {};
                            //data = [{"id":1,"label":"machin ltd"},{"id":2,"label":"truc"}] // Or get your JSON dynamically and load it into this variable
                            $.each(data, function (i, object) {
                                var company = (object.company_display_name).toLowerCase();
                                if (company.includes(query.toLowerCase())) {
                                    map[object.company_display_name] = object;	// using company_display_name because company_display_name is unique key in company table
                                    objects.push(object.company_display_name);
                                }
                            });
                            process(objects);
                        }
                    });
            $('.vendor_names').on('typeahead:selected', function (e, datum) {
                //alert(datum.value);
                //alert(map[datum].company_id); 
                $('[name="purchase_order_linked_company_invoice_name"]').val(map[datum].company_contact_person_name);
                $('[name="purchase_order_linked_company_id"]').val(map[datum].company_id);
                var address = '';
                if ((map[datum].company_billing_address)) {
                    address += map[datum].company_billing_address;
                    if (!address.trim().endsWith(",")) {
                        address = address + ', ';
                        ;
                    }
                }
                if ((map[datum].company_billing_area)) {
                    address += map[datum].company_billing_area + ', ';
                }
                if ((map[datum].billing_city_name)) {
                    address += map[datum].billing_city_name + ', ';
                }
                if ((map[datum].billing_district)) { 
                    address += map[datum].billing_district + ', ';
                }
                if ((map[datum].company_billing_pincode)) {
                    address += map[datum].company_billing_pincode;
                }

                if (address.trim().endsWith(",")) {
                    address = address.trim().slice(0, -1);
                }
                //map[datum].company_billing_address + ", " + map[datum].company_billing_area  + ", " + map[datum].city_name + ", " + map[datum].district + ", " + map[datum].company_billing_pincode
                $('[name="purchase_order_linked_company_billing_address"]').val(address);
                $('#purchase_order_linked_company_billing_state_id').val(map[datum].company_billing_state_id).trigger("change");
                //$('#purchase_order_linked_company_gst_supply_state_id').val(map[datum].company_gst_supply_state_id).trigger("change");
                $('#purchase_order_linked_company_payment_term_id').val(map[datum].company_payment_term_id).trigger("change");
                $('[name="purchase_order_linked_company_pincode"]').val(map[datum].company_pincode);
            });

        }
    }

    request.open("GET", "/index.php/web/company/json/vendor", true);

    request.setRequestHeader("content-type", "application/json");
    request.send();
}

function deletePurchaseOrderRow(element) {
    purchaseOrderDataTable.row($(element).parents('tr'))
            .remove()
            .draw();
    renumberPurchaseOrderTable();
}

function renumberPurchaseOrderTable() {
    purchaseOrderDataTable.column(0).nodes().each(function (cell, i) {
        cell.innerHTML = i + 1;
    });
}

function loadPurchaseOrderProductsList() {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            var productsArray = JSON.parse(request.responseText).products;
            purchaseOrderProductList = [];
            for (var i = 0; i < productsArray.length; i++) {
                var product = productsArray[i];
                purchaseOrderProductList.push(product);
                idPurchaseOrderProductMap[product.product_id] = product;
            }

            if (!purchaseOrderDataTable.data().count())
                updatePurchaseOrderProducts();//add 1 row by default if empty
            else 
                initializePurchaseOrderDataTableSelect2();
        }
    };
    request.open("GET", "/index.php/web/product/json/purchase", true);
    request.send();
}


/*
 0 - Product ID
 1 - rate
 2 - quantity
 3- UQC ID
 4 - UQC text
 */
function updatePurchaseOrderProducts(element, updateFields) {

    var productsListSize = 0;
    purchaseOrderDataTable.column(0).nodes().each(function (cell, i) {
        productsListSize++;
    });

    var rowIdx = productsListSize;
    var productId = purchaseOrderProductList[0].product_id;
    var quantity = 1;
    var purchase_order_price = 0;
    if(idPurchaseOrderProductMap[productId].selling_price){
        purchase_order_price = idPurchaseOrderProductMap[productId].selling_price;
    }
    
    if (element != null) {
        rowIdx = $(element).parents('tr')[0].rowIndex - 1;
        productId = element.selectedOptions[0].value;	//$('#priorityList2 option:selected').val()	
        if (!updateFields && purchaseOrderDataTable.cell(rowIdx, 2).nodes().to$().find('input').val()){
            purchase_order_price = purchaseOrderDataTable.cell(rowIdx, 2).nodes().to$().find('input').val();
        } else if(idPurchaseOrderProductMap[productId].selling_price){
            purchase_order_price = idPurchaseOrderProductMap[productId].selling_price;
        } else {
            purchase_order_price = 0;
        }
            //purchase_order_price = idPurchaseOrderProductMap[productId].selling_price;

        quantity = purchaseOrderDataTable.cell(rowIdx, 3).nodes().to$().find('input').val();
    }

    //PRODUCT
    var productSelectList = document.createElement("select");
    productSelectList.setAttribute("onchange", "updatePurchaseOrderProducts(this, true);");
    productSelectList.classList.add("form-control");
    productSelectList.classList.add("dtSelect2");
    //Create and append the options
    for (var i = 0; i < purchaseOrderProductList.length; i++) {
        var option = document.createElement("option");
        option.value = purchaseOrderProductList[i].product_id;
        option.text = purchaseOrderProductList[i].product_name;
        productSelectList.appendChild(option);

        if (productId == purchaseOrderProductList[i].product_id) {
            option.selected = true;
            option.setAttribute("selected", "selected");
        }
    }
    var productStr = productSelectList.outerHTML;

    var data = [];
    data[0] = rowIdx + 1;
    data[1] = productStr + '<input name = "purchase_order_entries[' + data[0] + '][' + productId + '][0]" value = "' + idPurchaseOrderProductMap[productId].product_name + '" style = "display:none" ></input>';
    data[2] = '<input name = "purchase_order_entries[' + data[0] + '][' + productId + '][1]" class = "form-control" type = "number" step = 0.01  min = 0.01 value = "' + purchase_order_price + '"></input>';
    data[3] = '<input name = "purchase_order_entries[' + data[0] + '][' + productId + '][2]" id = "quantity" class = "form-control" type = "number" step = 0.01 min = 0.01 value = "' + quantity + '"></input>'
            + '<input name = "purchase_order_entries[' + data[0] + '][' + productId + '][3]" value = "' + idPurchaseOrderProductMap[productId].product_uqc_id + '" style = "display:none" ></input>'
            + '<input name = "purchase_order_entries[' + data[0] + '][' + productId + '][4]" value = "' + idPurchaseOrderProductMap[productId].uqc_text + '" style = "display:none" ></input>';
    if (rowIdx == 0)
        data[4] = '';
    else
        data[4] = '<a href="javascript: void(0)" onclick = "deletePurchaseOrderRow(this);refreshAndReIndexEntirePurchaseOrderTable();"><i class="glyphicon glyphicon-trash"></i></a>';

    if (element == null) {
        purchaseOrderDataTable.row.add(data).draw(true);
    } else {
        purchaseOrderDataTable.row(rowIdx).data(data).draw(true);
    }
    initializePurchaseOrderDataTableSelect2();
}
