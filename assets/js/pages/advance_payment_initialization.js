function initializeAdvancePaymentForm() {
    var currentDate = new Date();
    var currentDateString = $('#datePicker').val();
    if (currentDateString != null && currentDateString != "") {
        try {
            currentDate = $.datepicker.parseDate("yy-mm-dd", currentDateString)
        } catch (Err) {

        }
    }
    $('#datePicker').datepicker({
        dateFormat: 'yy-mm-dd'
    }).datepicker("setDate", currentDate);
    initializeSelect2();
}

function updateAdvancePaymentCompanyId(){
    $('#advance_payment_linked_company_id_display').val($('[name="advance_payment_linked_company_id"]').val());
}