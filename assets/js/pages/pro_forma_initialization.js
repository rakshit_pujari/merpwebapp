
var proFormaDataTable;
var proFormaChargesDataTable;
var taxIsExclusiveForProForma = true;
var interStateTaxIsApplicableForProForma = false;
var PF_OC_GST_STATE_ID = -1;


function initialize_pro_forma_form() {
    loadOcGstStateIdForProForma();

    var currentDate = new Date();
    var currentDateString = $('#datePicker').val();
    if (currentDateString != null && currentDateString != "") {
        try {
            currentDate = $.datepicker.parseDate("yy-mm-dd", currentDateString)
        } catch (Err) {

        }
    }
    $('#datePicker').datepicker({
        dateFormat: 'yy-mm-dd'
    }).datepicker("setDate", currentDate);
    
    var proFormaRevisionDateString = $('#pro_forma_revision_date').val();
    if (proFormaRevisionDateString != null && proFormaRevisionDateString != "") {
        try {
            var proFormaBillDate = $.datepicker.parseDate("yy-mm-dd", proFormaRevisionDateString);
            $('#pro_forma_revision_date').datepicker({
                dateFormat: 'yy-mm-dd'
            }).datepicker("setDate", proFormaBillDate);
        } catch (Err) {

        }
    } else {
        $('#pro_forma_revision_date').datepicker({
                dateFormat: 'yy-mm-dd'
            })
    }
    
    initializeProFormaValidator();
}

function initializeProFormaValidator() {

    // Initialize
    var validator = $(".form-validate-jquery").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function (error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent().parent().parent());
                } else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function (label) {
            label.addClass("validation-valid-label").text("Valid")
        },
        rules: {
            city_name: {
                minlength: 2,
                lettersonlywithspace: true
            },
            employee_name: {
                minlength: 2,
                lettersonlywithspace: true
            },
            employee_location_id: {
                min: 1
            },
            employee_contact_number: {
                number: true,
                min: 11111111,
                max: 9999999999
            },
            employee_pincode: {
                number: true,
                min: 111111,
                max: 999999
            },
            employee_password: {
                minlength: 5
            },
            employee_password_repeat: {
                equalTo: "#employee_password"
            },
            employee_email_address: {
                email: true
            },
            bank_location_id: {
                min: 1
            },
            bank_email_address: {
                email: true
            },
            broker_location_id: {
                min: 1
            },
            broker_email_address: {
                email: true
            },
            broker_gst_number: {
                minlength: 15
            },
            company_location_id: {
                min: 1
            },
            company_gst_number: {
                minlength: 15
            },
            state: {
                minlength: 1
            },
            transporter_location_id: {
                min: 1
            },
            transporter_email_address: {
                email: true
            },
            repeat_email: {
                equalTo: "#email"
            },
            minimum_characters: {
                minlength: 10
            },
            maximum_characters: {
                maxlength: 10
            },
            minimum_number: {
                min: 10
            },
            maximum_number: {
                max: 10
            },
            number_range: {
                range: [10, 20]
            },
            url: {
                url: true
            },
            date: {
                date: true
            },
            date_iso: {
                dateISO: true
            },
            numbers: {
                number: true
            },
            digits: {
                digits: true
            },
            creditcard: {
                creditcard: true
            },
            basic_checkbox: {
                minlength: 2
            },
            styled_checkbox: {
                minlength: 2
            },
            switchery_group: {
                minlength: 2
            },
            switch_group: {
                minlength: 2
            }
        },
        messages: {
            custom: {
                required: "This is a custom error message",
            },
            bank_location_id: "Please choose a location",
            broker_location_id: "Please choose a location",
            company_location_id: "Please choose a location",
            employee_location_id: "Please choose a location",
            transporter_location_id: "Please choose a location",
            employee_pincode: "Please enter a valid pin code",
            employee_contact_number: "Please enter a valid contact number"
        }
    });
}
function resetProFormaLinkedCompanyId() {
    $('[name="pro_forma_linked_company_id"]').val('');
}

function initializeProFormaSelect2() {
    // Select with search
    $('.select2').select2();
    $('.select').select2({//keep this below $('.select2').select2();
        minimumResultsForSearch: Infinity
    });
}

function initializeProFormaDataTableSelect2() {
    $('.dtSelect2').select2();
    $('.dtSelect').select2({
        minimumResultsForSearch: Infinity
    });
}


function initializeProFormaDataTable() {
    proFormaDataTable = $('.proFormaDatatable').DataTable({
        autoWidth: false,
        columnDefs: [{
                width: '150px',
                targets: [3, 4, 5, 6]
            },
            {
                className: "row text-center",
                targets: [8]
            }
        ],
        order: [[0, "asc"]],
        searching: false,
        paging: false,
        bInfo: false,
        bSort: false
    });

    proFormaChargesDataTable = $('#proFormaChargesDataTable').DataTable({
        autoWidth: false,
        columnDefs: [
            {
                width: '200px',
                targets: [0]
            },
            {
                className: "row text-center",
                targets: [4]
            }
        ],
        order: [[0, "asc"]],
        searching: false,
        paging: false,
        bInfo: false,
        bSort: false
    });
}

function loadCustomersForProForma() {

    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            var data = JSON.parse(request.responseText).companies;

            $('.customer_names').typeahead(
                    {
                        hint: true,
                        highlight: true,
                        minLength: 0
                    },
                    {
                        source: function (query, process) {
                            objects = [];
                            map = {};
                            //data = [{"id":1,"label":"machin ltd"},{"id":2,"label":"truc"}] // Or get your JSON dynamically and load it into this variable
                            $.each(data, function (i, object) {
                                var company = (object.company_display_name).toLowerCase();
                                if (company.includes(query.toLowerCase())) {
                                    map[object.company_display_name] = object;	// using company_display_name because company_display_name is unique key in company table
                                    objects.push(object.company_display_name);
                                }
                            });
                            process(objects);
                        }
                    });
            $('.customer_names').on('typeahead:selected', function (e, datum) {
                //alert(datum.value);
                //alert(map[datum].company_id);
                $('[name="pro_forma_linked_company_invoice_name"]').val(map[datum].company_invoice_name);
                $('[name="pro_forma_linked_company_id"]').val(map[datum].company_id);
                var address = '';
                if ((map[datum].company_billing_address)) {
                    address += map[datum].company_billing_address;
                    if (!address.trim().endsWith(",")) {
                        address = address + ', ';
                    }
                }
                if ((map[datum].company_billing_area)) {
                    address += map[datum].company_billing_area + ', ';
                }
                if ((map[datum].billing_city_name)) {
                    address += map[datum].billing_city_name + ', ';
                }
                if ((map[datum].billing_district)) {
                    address += map[datum].billing_district + ', ';
                }
                if ((map[datum].company_billing_pincode)) {
                    address += map[datum].company_billing_pincode;
                }

                if (address.trim().endsWith(",")) {
                    address = address.trim().slice(0, -1);
                }
                $('[name="pro_forma_linked_company_billing_address"]').val(address);
                var address = '';
                if ((map[datum].company_shipping_address)) {
                    address += map[datum].company_shipping_address;
                    if (!address.trim().endsWith(",")) {
                        address = address + ', ';
                        ;
                    }
                }
                if ((map[datum].company_shipping_area)) {
                    address += map[datum].company_shipping_area + ', ';
                }
                if ((map[datum].shipping_city_name)) {
                    address += map[datum].shipping_city_name + ', ';
                }
                if ((map[datum].shipping_district)) {
                    address += map[datum].shipping_district + ', ';
                }
                if ((map[datum].company_shipping_pincode)) {
                    address += map[datum].company_shipping_pincode;
                }

                if (address.trim().endsWith(",")) {
                    address = address.trim().slice(0, -1);
                }
                $('[name="pro_forma_linked_company_shipping_address"]').val(address);
                //$('[name="pro_forma_linked_company_area"]').val(map[datum].company_area);
                //$('[name="pro_forma_linked_company_city"]').val(map[datum].city_name);
                //$('[name="pro_forma_linked_company_district"]').val(map[datum].district);
                //$('[name="pro_forma_linked_company_pincode"]').val(map[datum].company_pincode); 
                $('#pro_forma_linked_company_billing_state_id').val(map[datum].company_billing_state_id).trigger("change");
                $('#pro_forma_linked_company_shipping_state_id').val(map[datum].company_shipping_state_id).trigger("change");
                $('#pro_forma_linked_company_gst_supply_state_id').val(map[datum].company_gst_supply_state_id).trigger("change");
                $('#pro_forma_linked_company_payment_term_id').val(map[datum].company_payment_term_id).trigger("change");
                $('[name="pro_forma_linked_company_pincode"]').val(map[datum].company_pincode);
            });

        }
    }

    request.open("GET", "/index.php/web/company/json/customer", true);

    request.setRequestHeader("content-type", "application/json");
    request.send();
}

function updateBrokerCommissionForProForma() {
    var pro_forma_linked_broker_id = document.getElementById('pro_forma_linked_broker_id');
    var selectedOption = pro_forma_linked_broker_id.options[pro_forma_linked_broker_id.selectedIndex];
    var broker_commission = selectedOption.getAttribute("data-commission");
    document.getElementById('broker_commission').value = broker_commission;
}


function deleteProFormaChargeRow(element) {
    proFormaChargesDataTable.row($(element).parents('tr'))
            .remove()
            .draw();
}

function deleteRowForProForma(element) {
    proFormaDataTable.row($(element).parents('tr'))
            .remove()
            .draw();
    renumberProFormaTable();
    //refreshAndReIndexProFormaTable();
    //calculateProFormaNumbers();
}

function renumberProFormaTable() {
    proFormaDataTable.column(0).nodes().each(function (cell, i) {
        cell.innerHTML = i + 1;
    });
}

var productListForProForma = [];

var idProductMapForProForma = {};

function loadProductsListForProForma() {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            var productsArray = JSON.parse(request.responseText).products;
            productListForProForma = [];
            for (var i = 0; i < productsArray.length; i++) {
                var product = productsArray[i];
                productListForProForma.push(product);
                idProductMapForProForma[product.product_id] = product;
            }

            if (!proFormaDataTable.data().count()){
                updateProductsForProForma();//add 1 row by default if empty
                
                                //add default charges
                for (var i = 0; i < proFormaChargeList.length; i++) {
                    if(proFormaChargeList[i].charge_is_default_type == 1){
                        updateProFormaCharges(null, proFormaChargeList[i].charge_id);
                    }
                }
            }
            updateTaxesForProForma();
            calculateProFormaNumbers();
        }
    };
    request.open("GET", "/index.php/web/product/json/sales", true);
    request.send();
}

var interstate_taxes_for_pro_forma = null;
var intrastate_taxes_for_pro_forma = null;
function loadTaxesForProForma() {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            interstate_taxes_for_pro_forma = JSON.parse(request.responseText).interstate_taxes;
            intrastate_taxes_for_pro_forma = JSON.parse(request.responseText).intrastate_taxes;
            loadProductsListForProForma();
            loadCustomersForProForma();
        }
    };
    request.open("GET", "/index.php/web/tax/json", true);
    request.send();
}

var proFormaChargeList = null;
var idProFormaChargeMap = {};
function loadProFormaCharges() {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            proFormaChargeList = JSON.parse(request.responseText).charges;
            for (var i = 0; i < proFormaChargeList.length; i++) {
                var charge = proFormaChargeList[i];
                idProFormaChargeMap[charge.charge_id] = charge;
            }
        }
    };
    request.open("GET", "/index.php/web/charge/json/sales", true);
    request.send();
}

function loadOcGstStateIdForProForma() {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            var stateId = JSON.parse(request.responseText).state_id;
            PF_OC_GST_STATE_ID = stateId;
            if (PF_OC_GST_STATE_ID > 0) {	// continue only if GST ID is valid
                initializeProFormaDataTable();
                loadTaxesForProForma();
                loadProFormaCharges();
                initializeProFormaSelect2();
                initializeProFormaDataTableSelect2();
            } else {
                alert('Failed to load OC gst state ID. Please contact system admin');
            }
        }
    };
    request.open("GET", "/index.php/web/OC/state", true);
    request.send();
}


function updateProFormaCharges(element, defaultProFormaChargeId){
    
    var proFormaChargesListSize = 0;
    proFormaChargesDataTable.column(0).nodes().each(function (cell, i) {
        proFormaChargesListSize++;
    });

    var rowIdx = proFormaChargesListSize;
    var proFormaChargeId = proFormaChargeList[0].charge_id;
    if(defaultProFormaChargeId){
        proFormaChargeId = defaultProFormaChargeId;
    }
    var applicableTaxId = parseInt(productListForProForma[0].intra_state_tax_id);
    var taxes = intrastate_taxes_for_pro_forma;
    if (interStateTaxIsApplicableForProForma) {
        applicableTaxId = parseInt(productListForProForma[0].inter_state_tax_id);
        taxes = interstate_taxes_for_pro_forma;
    }
    
    
    var taxableAmount = 0.01;
    var totalAmount = 0;
    //var tax_percent = 0;
    if (element != null) {
        rowIdx = $(element).parents('tr')[0].rowIndex - 1;
        proFormaChargeId = element.selectedOptions[0].value;	//$('#priorityList2 option:selected').val()
        taxableAmount = proFormaChargesDataTable.cell(rowIdx, 1).nodes().to$().find('input').val();
        applicableTaxId = proFormaChargesDataTable.cell(rowIdx, 2).nodes().to$().find('select').val();
        //tax_percent = parseFloat(proFormaChargesDataTable.cell(rowIdx, 2).nodes().to$().find('select')[0].selectedOptions[0].attributes['tax_value'].value);
        //total_amount = (1 + tax_percent/100) * taxableAmount;
    }

    //PRODUCT
    var proFormaChargeSelectList = document.createElement("select");
    proFormaChargeSelectList.setAttribute("onchange", "updateProFormaCharges(this);calculateProFormaNumbers();");
    proFormaChargeSelectList.classList.add("form-control");
    proFormaChargeSelectList.classList.add("dtSelect2");
    //Create and append the options
    for (var i = 0; i < proFormaChargeList.length; i++) {
        var option = document.createElement("option");
        option.value = proFormaChargeList[i].charge_id;
        option.text = proFormaChargeList[i].charge_name;
        proFormaChargeSelectList.appendChild(option);

        if (proFormaChargeId == proFormaChargeList[i].charge_id) {
            option.selected = true;
            option.setAttribute("selected", "selected");
        }
    }
    
    var proFormaChargeStr = proFormaChargeSelectList.outerHTML;

    //TAXES
    var taxSelectList = document.createElement("select");
    taxSelectList.setAttribute("onchange", "calculateProFormaNumbers();");
    taxSelectList.setAttribute("name", "charge_entries[" + (rowIdx + 1) + "][" + proFormaChargeId + "][2]");
    taxSelectList.classList.add("form-control");
    taxSelectList.classList.add("dtSelect");
    //var taxes = [0, 5, 12, 18, 28];
    //Create and append the options
    for (var i = 0; i < taxes.length; i++) {
        var option = document.createElement("option");
        option.value = taxes[i].tax_id;
        option.setAttribute("tax_value", taxes[i].tax_percent);
        option.text = taxes[i].tax_name;
        taxSelectList.appendChild(option);

        if (applicableTaxId == taxes[i].tax_id) {
            option.selected = true;
            option.setAttribute("selected", "selected");
        }
    }
    
    var taxStr = taxSelectList.outerHTML;

    var data = [];
    data[0] = proFormaChargeStr + '<input name = "charge_entries[' + (rowIdx + 1) + '][' + proFormaChargeId + '][0]" value = "' + idProFormaChargeMap[proFormaChargeId].charge_name + '" style = "display:none" ></input>';
    data[1] = '<input style = "min-width:100px" name = "charge_entries[' + (rowIdx + 1) + '][' + proFormaChargeId + '][1]" value = "' + taxableAmount + '" class = "form-control" min = "0.01" step ="0.01" type = "number" onchange = "calculateProFormaNumbers();"></input>';
    data[2] = taxStr;
    data[3] = '';

    data[4] = '<a href="javascript: void(0)" onclick = "deleteChargeRow(this);refreshAndReIndexEntireTable();calculateProFormaNumbers();"><i class="glyphicon glyphicon-trash"></i></a>';
    //console.log(data);
    if (element == null) {
        proFormaChargesDataTable.row.add(data).draw(true);
    } else {
        proFormaChargesDataTable.row(rowIdx).data(data).draw(true);
    } 

    //calculateProFormaNumbers();
    initializeDataTableSelect2();
} 


/*
 0 - Product Name
 1 - Tax
 2 - HSN
 3 - rate
 4 - quantity
 5 - discount
 */
function updateProductsForProForma(element, updateFields) {

    var productsListSize = 0;
    proFormaDataTable.column(0).nodes().each(function (cell, i) {
        productsListSize++;
    });

    var rowIdx = productsListSize;
    var productId = productListForProForma[0].product_id;
    var applicable_tax_id = parseInt(productListForProForma[0].intra_state_tax_id);
    var taxes = intrastate_taxes_for_pro_forma;
    if (interStateTaxIsApplicableForProForma) {
        applicable_tax_id = parseInt(productListForProForma[0].inter_state_tax_id);
        taxes = interstate_taxes_for_pro_forma;
    }
    var quantity = 1;
    var discount = 0;
    var selling_price = idProductMapForProForma[productId].selling_price;
    if (element != null) {
        rowIdx = $(element).parents('tr')[0].rowIndex - 1;
        productId = element.selectedOptions[0].value;	//$('#priorityList2 option:selected').val()
        if (updateFields) {
            applicable_tax_id = parseInt(idProductMapForProForma[productId].intra_state_tax_id);
            if (interStateTaxIsApplicableForProForma) {
                applicable_tax_id = parseInt(idProductMapForProForma[productId].inter_state_tax_id);
            }
        } else {
            applicable_tax_id = proFormaDataTable.cell(rowIdx, 3).nodes().to$().find('select').val();
        }

        if (!updateFields && proFormaDataTable.cell(rowIdx, 4).nodes().to$().find('input').val())
            selling_price = proFormaDataTable.cell(rowIdx, 4).nodes().to$().find('input').val();
        else
            selling_price = idProductMapForProForma[productId].selling_price;
        quantity = proFormaDataTable.cell(rowIdx, 5).nodes().to$().find('input').val();
        discount = proFormaDataTable.cell(rowIdx, 7).nodes().to$().find('input').val();
    }

    //PRODUCT
    var productSelectList = document.createElement("select");
    productSelectList.setAttribute("onchange", "updateProductsForProForma(this, true);calculateProFormaNumbers();");
    productSelectList.classList.add("form-control");
    productSelectList.classList.add("dtSelect2");
    //Create and append the options
    for (var i = 0; i < productListForProForma.length; i++) {
        var option = document.createElement("option");
        option.value = productListForProForma[i].product_id;
        option.text = productListForProForma[i].product_name;
        productSelectList.appendChild(option);

        if (productId == productListForProForma[i].product_id) {
            option.selected = true;
            option.setAttribute("selected", "selected");
        }
    }
    var productStr = productSelectList.outerHTML;

    //TAXES
    var taxSelectList = document.createElement("select");
    taxSelectList.setAttribute("onchange", "calculateProFormaNumbers();");
    taxSelectList.setAttribute("name", "pro_forma_entries[" + (rowIdx + 1) + "][" + productId + "][1]");
    taxSelectList.classList.add("form-control");
    taxSelectList.classList.add("dtSelect");
    //var taxes = [0, 5, 12, 18, 28];
    //Create and append the options
    for (var i = 0; i < taxes.length; i++) {
        var option = document.createElement("option");
        option.value = taxes[i].tax_id;
        option.setAttribute("tax_value", taxes[i].tax_percent);
        option.text = taxes[i].tax_name;
        taxSelectList.appendChild(option);

        if (applicable_tax_id == taxes[i].tax_id) {
            option.selected = true;
            option.setAttribute("selected", "selected");
        }
    }
    var taxStr = taxSelectList.outerHTML;

    var data = [];
    data[0] = rowIdx + 1;
    data[1] = productStr + '<input name = "pro_forma_entries[' + data[0] + '][' + productId + '][0]" value = "' + idProductMapForProForma[productId].product_name + '" style = "display:none" ></input>';
    data[2] = '<a href = "master#!/product/hsn/' + idProductMapForProForma[productId].hsn_code + '">' + idProductMapForProForma[productId].hsn_code
            + '</a><input name = "pro_forma_entries[' + data[0] + '][' + productId + '][2]" value = "' + idProductMapForProForma[productId].hsn_code + '" style = "display:none" ></input>';
    data[3] = taxStr;
    data[4] = '<input name = "pro_forma_entries[' + data[0] + '][' + productId + '][3]" class = "form-control" type = "number" step = 0.01 min = 0.01 value = "' + selling_price + '" onchange = "calculateProFormaNumbers()"></input>';
    data[5] = '<input name = "pro_forma_entries[' + data[0] + '][' + productId + '][4]" id = "quantity" class = "form-control" type = "number" step = 0.01 min = 0.01  value = "' + quantity + '" onchange = "calculateProFormaNumbers()"></input>'
            + '<input name = "pro_forma_entries[' + data[0] + '][' + productId + '][5]" value = "' + idProductMapForProForma[productId].product_uqc_id + '" style = "display:none" ></input>'
            + '<input name = "pro_forma_entries[' + data[0] + '][' + productId + '][6]" value = "' + idProductMapForProForma[productId].uqc_text + '" style = "display:none" ></input>';
    data[6] = '';//idProductMapForProForma[productId].inventory_quantity + ' ' + idProductMapForProForma[productId].uqc_text!='null'? idProductMapForProForma[productId].uqc_text : '';
    data[7] = '<input name = "pro_forma_entries[' + data[0] + '][' + productId + '][7]" min = 0 max = 100 id = "discount" class = "form-control" type = "number" step = 0.01 value = "' + discount + '" onchange = "calculateProFormaNumbers()"></input>';
    data[8] = 0;
    if (rowIdx == 0)
        data[9] = '';
    else
        data[9] = '<a href="javascript: void(0)" onclick = "deleteRowForProForma(this);refreshAndReIndexProFormaTable();calculateProFormaNumbers();"><i class="glyphicon glyphicon-trash"></i></a>';

    if (element == null) {
        proFormaDataTable.row.add(data).draw(true);
    } else {
        proFormaDataTable.row(rowIdx).data(data).draw(true);
    }

    //calculateProFormaNumbers();
    initializeProFormaDataTableSelect2();
}

function updateTaxesForProForma() {
    var pro_forma_linked_company_gst_supply_state_id = document.getElementById('pro_forma_linked_company_gst_supply_state_id');
    var selectedStateId = (pro_forma_linked_company_gst_supply_state_id.options[pro_forma_linked_company_gst_supply_state_id.selectedIndex]).value;
    if (selectedStateId > 0) {
        interStateTaxIsApplicableForProForma = !(selectedStateId == PF_OC_GST_STATE_ID);
    } else {
        interStateTaxIsApplicableForProForma = false;
    }

    var tax_type = document.getElementById('tax_type');
    var selectedOption = tax_type.options[tax_type.selectedIndex];
    if (selectedOption.value == 'inclusive') {
        taxIsExclusiveForProForma = false;
    } else {
        taxIsExclusiveForProForma = true;
    }
}

function refreshAndReIndexProFormaTable(updateFields) {
    proFormaDataTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var element = proFormaDataTable.cell(rowIdx, 1).nodes().to$().find('select')[0];
        if (element == null) {
            alert("ERROR");
            return;
        }
        updateProductsForProForma(element, updateFields);
    });
    
    proFormaChargesDataTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var element = proFormaChargesDataTable.cell(rowIdx, 0).nodes().to$().find('select')[0];
        if (element == null) {
            alert("ERROR");
            return;
        }
        updateProFormaCharges(element);
    });
}

function calculateProFormaNumbers() {
    if (taxIsExclusiveForProForma) {
        $('#subTotalLabel').text("Sub Total ( Exclusive Of Tax )");
    } else {
        $('#subTotalLabel').text("Sub Total ( Inclusive Of Tax )");
    }
    var totalAmount = parseFloat(0);
    var totalTaxAmount = parseFloat(0);
    var postTotalTaxAmount = parseFloat(0);
    var taxAmountMap = {};
    proFormaDataTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var data = this.data();
        var product_id = proFormaDataTable.cell(rowIdx, 1).nodes().to$().find('select')[0].selectedOptions[0].value;
        var tax_select = proFormaDataTable.cell(rowIdx, 3).nodes().to$().find('select');	// GET ATTRIBUTE AND NOT VALUE
        var tax = $('option:selected', tax_select).attr('tax_value');
        var cost = proFormaDataTable.cell(rowIdx, 4).nodes().to$().find('input').val();
        var quantity = proFormaDataTable.cell(rowIdx, 5).nodes().to$().find('input').val();
        var inventory_text = 'NA';
        if (idProductMapForProForma[product_id].product_type == 'goods') {
            inventory_text = idProductMapForProForma[product_id].inventory_quantity + ' ' + (idProductMapForProForma[product_id].uqc_text != null ? idProductMapForProForma[product_id].uqc_text : '')
        }
        proFormaDataTable.cell(rowIdx, 6).data(inventory_text); //set inventory quantity
        var discount = proFormaDataTable.cell(rowIdx, 7).nodes().to$().find('input').val();

        var amount = ((cost * quantity) * (1 - (discount / 100))).toFixed(2);
        totalAmount = parseFloat(totalAmount) + parseFloat(amount);

        var taxAmount = parseFloat(0);
        if (taxIsExclusiveForProForma) {
            taxAmount = amount * (tax / 100);
        } else {
            //(tax * amount) / ( 100 + tax )
            taxAmount = (parseFloat(tax) * parseFloat(amount)) / (100 + parseFloat(tax))
        }

        if (taxAmountMap[tax] == null) {
            taxAmountMap[tax] = taxAmount;
        } else {
            taxAmountMap[tax] = taxAmountMap[tax] + taxAmount;
        }

        totalTaxAmount = (parseFloat(totalTaxAmount) + parseFloat(taxAmount)).toFixed(2);
        proFormaDataTable.cell(rowIdx, 8).data('₹ ' + amount);
    });
    
    proFormaChargesDataTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var data = this.data();
        //var charge_id = chargesDataTable.cell(rowIdx, 0).nodes().to$().find('select')[0].selectedOptions[0].value;
        var taxableAmount = proFormaChargesDataTable.cell(rowIdx, 1).nodes().to$().find('input').val();
        var tax = proFormaChargesDataTable.cell(rowIdx, 2).nodes().to$().find('select')[0].selectedOptions[0].attributes['tax_value'].value;
        var taxAmountOnCharge = parseFloat(0);
        var totalChargeAmount = parseFloat(0);
        if (taxIsExclusiveForProForma) {
            taxAmountOnCharge = taxableAmount * (tax / 100);
            totalChargeAmount = (parseFloat(taxableAmount) + parseFloat(taxAmountOnCharge)).toFixed(2);
        } else {
            //(tax * amount) / ( 100 + tax )
            taxAmountOnCharge = (parseFloat(tax) * parseFloat(taxableAmount)) / (100 + parseFloat(tax));
            totalChargeAmount = taxableAmount;
        } 

        if (taxAmountMap[tax] == null) {
            taxAmountMap[tax] = taxAmountOnCharge;
        } else {
            taxAmountMap[tax] = taxAmountMap[tax] + taxAmountOnCharge;
        }
 
        totalTaxAmount = parseFloat(totalTaxAmount) + parseFloat(taxAmountOnCharge);
        totalAmount = parseFloat(totalAmount) + parseFloat(taxableAmount);
        proFormaChargesDataTable.cell(rowIdx, 3).data('<span style = "white-space: nowrap;">₹ ' + totalChargeAmount + '</span>');
    });


    document.getElementById('totalProFormaSubTotalAmount').innerHTML = '₹ ' + parseFloat(totalAmount).toFixed(2);

    var taxStr = '';

    for (var taxPercent in taxAmountMap) {
        //if(taxAmountMap[taxPercent]!=0){
        if (interStateTaxIsApplicableForProForma) {

            taxStr += '<div class="form-group col-lg-12">' +
                    '<label class="control-label col-lg-6">IGST@' + taxPercent + '%</label>' +
                    '<div class="col-lg-6">' +
                    '<h6 class = "no-margin text-bold">₹ ' + taxAmountMap[taxPercent].toFixed(2) + '<h6>' +
                    '</div>' +
                    '</div>';

        } else {
            taxStr += '<div class="form-group col-lg-12">' +
                    '<label class="control-label col-lg-6">SGST@' + (taxPercent / 2) + '%</label>' +
                    '<div class="col-lg-6">' +
                    '<h6 class = "no-margin text-bold">₹ ' + (taxAmountMap[taxPercent] / 2).toFixed(2) + '<h6>' +
                    '</div>' +
                    '</div>' +
                    '<div class="form-group col-lg-12">' +
                    '<label class="control-label col-lg-6">CGST@' + (taxPercent / 2) + '%</label>' +
                    '<div class="col-lg-6">' +
                    '<h6 class = "no-margin text-bold">₹ ' + (taxAmountMap[taxPercent] / 2).toFixed(2) + '<h6>' +
                    '</div>' +
                    '</div>';
            ;

        }
        //}
    }

    document.getElementById('taxDiv').innerHTML = taxStr;

    var transportationCharges = 0;
    if (document.getElementById('transportationCharges').value != "") {
        transportationCharges = document.getElementById('transportationCharges').value;
    }
    var adjustments = 0;
    if (document.getElementById('adjustments').value != "") {
        adjustments = document.getElementById('adjustments').value;
    }

    if (taxIsExclusiveForProForma) {
        postTotalTaxAmount = parseFloat(totalTaxAmount) + parseFloat(totalAmount);
    } else {
        postTotalTaxAmount = totalAmount;
    }

    var proFormaTotal = parseFloat(postTotalTaxAmount) + parseFloat(transportationCharges) + parseFloat(adjustments);

    document.getElementById('proFormaTotal').innerHTML = '₹ ' + parseFloat(proFormaTotal).toFixed(2);
    document.getElementById('proFormaAmount').value = parseFloat(proFormaTotal).toFixed(2);

    if (proFormaTotal <= 0) {
        if (document.getElementById('buttonSaveDraftProForma') != null)
            document.getElementById('buttonSaveDraftProForma').disabled = true;

        document.getElementById('buttonSaveConfirmProForma').disabled = true;
    } else {
        if (document.getElementById('buttonSaveDraftProForma') != null)
            document.getElementById('buttonSaveDraftProForma').disabled = false;

        document.getElementById('buttonSaveConfirmProForma').disabled = false;
    }
}