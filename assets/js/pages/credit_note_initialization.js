
var customerListForCreditNoteForm, vendorListForCreditNoteForm;
var creditNoteChargesDataTable;
var invoice_customer_map, purchase_vendor_map;
var creditNoteDataTable;
var CN_OC_GST_STATE_ID = -1;
var taxIsExclusiveForCreditNote = true;
var interStateTaxIsApplicableForCreditNote = false;

function initialize_credit_note_form() {
    loadOcGstStateIdForCreditNote();

    var currentDate = new Date();
    var currentDateString = $('#datePicker').val();
    if (currentDateString != null && currentDateString != "") {
        try {
            currentDate = $.datepicker.parseDate("yy-mm-dd", currentDateString)
        } catch (Err) {

        }
    }
    $('#datePicker').datepicker({
        dateFormat: 'yy-mm-dd'
    }).datepicker("setDate", currentDate);

    initializeFormValidator();
    //initializeCreditNoteDataTableSelect2();
}

var creditNoteChargeList = null;
var idcreditNoteChargeMap = {};
function loadCreditNoteCharges() {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            creditNoteChargeList = JSON.parse(request.responseText).charges;
            for (var i = 0; i < creditNoteChargeList.length; i++) {
                var charge = creditNoteChargeList[i];
                idcreditNoteChargeMap[charge.charge_id] = charge;
            }
        }
    };
    request.open("GET", "/index.php/web/charge/json/sales", true);
    request.send();
}


function loadOcGstStateIdForCreditNote() {
    $.get("/index.php/web/OC/state", function (data, status) {
        if (status == 'success') {
            var stateId = JSON.parse(data).state_id;
            CN_OC_GST_STATE_ID = stateId;
            if (CN_OC_GST_STATE_ID > 0) {	// continue only if GST ID is valid
                initializeCreditNoteDataTable();
                if (!creditNoteDataTable.data().count()) {
                    loadCustomersAndVendorsForCreditNoteForm();
                    loadInvoiceCustomerAndPurchaseVendorMap();
                    
                }
                
                loadTaxesForCreditNoteForm();
                initializeSelect2();
                loadCreditNoteCharges();
                updateTaxesInCreditNoteTable();
                calculateCreditNoteNumbers();
            } else {
                alert('Failed to load OC gst state ID. Please contact system admin');
            }

        }
    });
}

function pad(str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}

var interstate_taxes_for_credit_note = null;
var intrastate_taxes_for_credit_note = null;
function loadTaxesForCreditNoteForm() {
    $.get("/index.php/web/tax/json", function (data, status) {
        if (status == 'success') {
            var productsArray = JSON.parse(data).products;
            interstate_taxes_for_credit_note = JSON.parse(data).interstate_taxes;
            intrastate_taxes_for_credit_note = JSON.parse(data).intrastate_taxes;
            loadProductsListForCreditNoteForm();
        }
    });
}

function initializeCreditNoteDataTableSelect2() {
    // Select with search
    $('.dtSelect2').select2();
    $('.dtSelect').select2({//keep this below $('.select2').select2();
        minimumResultsForSearch: Infinity
    });
}

var productListForCreditNoteForm;
var idProductMapForCreditNoteForm = {};
function loadProductsListForCreditNoteForm() {
    $.get("/index.php/web/product/json/sales", function (data, status) {
        if (status == 'success') {
            var productsArray = JSON.parse(data).products;
            productListForCreditNoteForm = [];
            for (var i = 0; i < productsArray.length; i++) {
                var product = productsArray[i];
                productListForCreditNoteForm.push(product);
                idProductMapForCreditNoteForm[product.product_id] = product;
            }
        }
    });
}


function removeOptionsInCreditNoteForm(selectbox)
{
    $(selectbox).empty();
}

function updateTaxesInCreditNoteTable() {
    var credit_note_linked_company_gst_supply_state_id = document.getElementById('credit_note_linked_company_gst_supply_state_id');
    var selectedStateId = (credit_note_linked_company_gst_supply_state_id.options[credit_note_linked_company_gst_supply_state_id.selectedIndex]).value;
    if (selectedStateId > 0) {
        interStateTaxIsApplicableForCreditNote = !(selectedStateId == CN_OC_GST_STATE_ID);
    } else {
        interStateTaxIsApplicableForCreditNote = false;
    }

    var tax_type = document.getElementById('credit_note_tax_type');
    var selectedOption = tax_type.options[tax_type.selectedIndex];
    if (selectedOption.value == 'inclusive') {
        taxIsExclusiveForCreditNote = false;
    } else {
        taxIsExclusiveForCreditNote = true;
    }
}

function loadInvoiceCustomerAndPurchaseVendorMap() {
    $.get("/index.php/web/invoice/json/invoice_customer_map", function (data, status) {
        if (status == 'success') {
            invoice_customer_map = JSON.parse(data).invoice_customer_map;
            refreshInvoiceListForCreditNote();
        }
    });

    $.get("/index.php/web/purchase/json/purchase_vendor_map", function (data, status) {
        if (status == 'success') {
            purchase_vendor_map = JSON.parse(data).purchase_vendor_map;
            refreshInvoiceListForCreditNote();
        }
    });
}

function refreshAndReIndexEntireCreditNoteTable(updateFields) {
    /*creditNoteDataTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var element = purchaseDataTable.cell(rowIdx, 1).nodes().to$().find('select')[0];
        if (element == null) {
            alert("ERROR");
            return;
        }
        updatePurchaseProducts(element, updateFields);
    });
    */
    creditNoteChargesDataTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var element = creditNoteChargesDataTable.cell(rowIdx, 0).nodes().to$().find('select')[0];
        if (element == null) {
            alert("ERROR");
            return;
        }
        updateCreditNoteCharges(element);
    });
}

function syncCompanyName() {
    var credit_note_linked_company_name = document.getElementById('credit_note_linked_company_name');
    var credit_note_linked_invoice_id = document.getElementById('credit_note_linked_invoice_id');

    var selectedCompanyName = credit_note_linked_company_name.options[credit_note_linked_company_name.selectedIndex];
    var selectedInvoice = credit_note_linked_invoice_id.options[credit_note_linked_invoice_id.selectedIndex];

    if (selectedCompanyName != null
            && selectedInvoice != null) {
        if (selectedInvoice.getAttribute('company_id') != '') {
            $(credit_note_linked_company_name).val(selectedInvoice.getAttribute('company_id')).trigger("change");

        } else if (selectedInvoice.getAttribute('company_name') != '') {
            for (var i = 0; i < $('select#credit_note_linked_company_name option').length; i++) {
                if ($('select#credit_note_linked_company_name option')[i].val() == selectedInvoice.getAttribute('company_name')) {
                    var val = $('select#credit_note_linked_company_name option')[i].val();
                    $(credit_note_linked_company_name).val(selectedInvoice.getAttribute(val)).trigger("change");
                }
            }
        } else {
            alert('Error');
        }
    }
}

function updateBrokerCommissionForCreditNoteForm() {
    var credit_note_linked_broker_id = document.getElementById('credit_note_linked_broker_id');
    var selectedOption = credit_note_linked_broker_id.options[credit_note_linked_broker_id.selectedIndex];
    var broker_commission = selectedOption.getAttribute("data-commission");
    document.getElementById('broker_commission').value = broker_commission;
}


function deleteCreditNoteChargeRow(element) {
    creditNoteChargesDataTable.row($(element).parents('tr'))
            .remove()
            .draw();
}

function deleteCreditNoteRow(element) {
    creditNoteDataTable.row($(element).parents('tr'))
            .remove()
            .draw();
    renumberCreditNoteTable();
}

function renumberCreditNoteTable() {
    creditNoteDataTable.column(0).nodes().each(function (cell, i) {
        cell.innerHTML = i + 1;
    });
}

function clearCreditNoteForm() {
    invoice_amount = -1;
    $("input[name=cn_linked_company_invoice_name]").val('');
    //$("input[name=cn_date]").val('');
    $("textarea[name=cn_linked_company_billing_address]").val('');
    $("textarea[name=cn_linked_company_shipping_address]").val('');

    $("select[name=cn_linked_company_billing_state_id]").val('').trigger("change");
    $("select[name=cn_linked_company_shipping_state_id]").val('').trigger("change");

    $("select[name=cn_linked_company_gst_supply_state_id]").val('').trigger("change");

    $("select[name=cn_tax_type]").val('').trigger("change");
    ;
    $("select[name=cn_linked_broker_id]").val('').trigger("change");
    ;
    $("input[name=cn_linked_broker_commission]").val('');
    $("select[name=cn_linked_transporter_id]").val('').trigger("change");
    ;
    $("input[name=cn_lr_number]").val('');

    $("select[name=cn_linked_company_payment_term_id]").val('').trigger("change");

    $("input[name=cn_order_reference]").val('');
    $("input[name=cn_notes]").val('');

    $("input[name=cn_linked_employee_id]").val('');

    creditNoteDataTable.clear().draw();
}

function minMaxAmountForCreditNoteForm(element, min, max)
{
    if (parseInt(element.value) < min || isNaN(parseInt(element.value)))
        return min;
    else if (parseInt(element.value) > max)
        return max;
    else
        return element.value;
}

/*
 0 - Product Name
 1 - Tax
 2 - HSN
 3 - rate
 4 - quantity
 5 - discount
 */

var invoice_amount = -1;
function loadInvoiceForCreditNoteForm() {
    invoice_amount = -1;
    var credit_note_linked_invoice_id = document.getElementById('credit_note_linked_invoice_id');
    var selectedInvoice = credit_note_linked_invoice_id.options[credit_note_linked_invoice_id.selectedIndex];
    var invoiceId = selectedInvoice.value;
    clearCreditNoteForm();
    if (invoiceId != null) {
        if (document.getElementById('credit_against_sales').checked) {
            $.get("/index.php/web/invoice/json/invoice/" + invoiceId, function (data, status) {
                if (status == 'success') {
                    //alert(data);
                    var invoice = JSON.parse(data).invoice;
                    $("input[name=cn_linked_company_invoice_name]").val(invoice.invoice_linked_company_invoice_name);
                    //$("input[name=cn_date]").val(invoice.invoice_date.split(' ')[0]);
                    $("textarea[name=cn_linked_company_billing_address]").val(invoice.invoice_linked_company_billing_address);
                    $("textarea[name=cn_linked_company_shipping_address]").val(invoice.invoice_linked_company_shipping_address);

                    $("select[name=cn_linked_company_billing_state_id]").val(invoice.invoice_linked_company_billing_state_id).trigger("change");
                    $("select[name=cn_linked_company_shipping_state_id]").val(invoice.invoice_linked_company_shipping_state_id).trigger("change");

                    $("select[name=cn_linked_company_gst_supply_state_id]").val(invoice.invoice_linked_company_gst_supply_state_id).trigger("change");

                    $("select[name=cn_tax_type]").val(invoice.invoice_tax_type).trigger("change");

                    $("select[name=cn_linked_broker_id]").val(invoice.invoice_linked_broker_id).trigger("change");

                    $("input[name=cn_linked_broker_commission]").val(invoice.invoice_linked_broker_commission);
                    $("select[name=cn_linked_transporter_id]").val(invoice.invoice_linked_transporter_id).trigger("change");

                    $("input[name=cn_lr_number]").val(invoice.invoice_lr_number);

                    $("select[name=cn_linked_company_payment_term_id]").val(invoice.invoice_linked_company_payment_term_id).trigger("change");

                    $("input[name=cn_order_reference]").val(invoice.invoice_order_reference);
                    $("input[name=cn_notes]").val(invoice.invoice_notes);

                    $("input[name=cn_linked_employee_id]").val(invoice.invoice_linked_employee_id);

                    invoice_amount = invoice.invoice_amount;

                    var invoice_entries = JSON.parse(data).invoice_entries;
                    creditNoteDataTable.clear().draw();
                    var total_credit_note_balance = 0;
                    for (var i = 0; i < invoice_entries.length; i++) {

                        var invoice_entry = invoice_entries[i];

                        var data = [];
                        data[0] = invoice_entry.ipe_entry_id;
                        data[1] = invoice_entry.ipe_product_name + '<input style = "display:none" name = "credit_note_entries[' + data[0] + '][' + invoice_entry.ipe_linked_product_id + '][0]" type = "text" value = "' + invoice_entry.ipe_product_name + '" readonly></input>';
                        data[2] = invoice_entry.ipe_product_hsn_code + '<input style = "display:none" name = "credit_note_entries[' + data[0] + '][' + invoice_entry.ipe_linked_product_id + '][1]" type = "text" value = "' + invoice_entry.ipe_product_hsn_code + '" readonly></input>';
                        data[3] = '<input name = "credit_note_entries[' + data[0] + '][' + invoice_entry.ipe_linked_product_id + '][2]" tax_value = ' + invoice_entry.ipe_tax_percent_applicable + ' style = "display:none" type = "text" value = "' + invoice_entry.ipe_tax_id_applicable + '" readonly></input>'
                                +
                                '<input class = "form-control" type = "text" value = "' + invoice_entry.ipe_tax_name_applicable + '" readonly></input>';
                        data[4] = '<input name = "credit_note_entries[' + data[0] + '][' + invoice_entry.ipe_linked_product_id + '][3]" class = "form-control" type = "number" step = 0.01 value = "' + invoice_entry.ipe_product_rate + '" onchange = "calculateCreditNoteNumbers()" readonly></input>';

                        var quantity_value = invoice_entry.ipe_product_quantity;
                        if (parseInt(quantity_value) > parseInt(invoice_entry.credit_note_balance)) {
                            quantity_value = invoice_entry.credit_note_balance;
                        }

                        total_credit_note_balance = parseInt(total_credit_note_balance) + parseInt(quantity_value);

                        data[5] = '<input name = "credit_note_entries[' + data[0] + '][' + invoice_entry.ipe_linked_product_id + '][4]" id = "quantity"  min = "0" max = "' + invoice_entry.credit_note_balance + '" onblur = "this.value = minMaxAmountForCreditNoteForm(this, 0, ' + invoice_entry.credit_note_balance + ')" class = "form-control" type = "number" step = 0.01 value = "' + quantity_value + '" onchange = "calculateCreditNoteNumbers()"></input>'
                                + '<input name = "credit_note_entries[' + data[0] + '][' + invoice_entry.ipe_linked_product_id + '][5]" value = "' + invoice_entry.ipe_product_uqc_id + '" style = "display:none" ></input>'
                                + '<input name = "credit_note_entries[' + data[0] + '][' + invoice_entry.ipe_linked_product_id + '][6]" value = "' + invoice_entry.ipe_product_uqc_text + '" style = "display:none" ></input>';
                        data[6] = '<input name = "credit_note_entries[' + data[0] + '][' + invoice_entry.ipe_linked_product_id + '][7]" readonly min = 0 max = 100 id = "discount" class = "form-control" type = "number" step = 0.01 value = "' + invoice_entry.ipe_discount + '" onchange = "calculateCreditNoteNumbers()" readonly></input>';
                        data[7] = 0;
                        data[8] = '<input style = "display:none" name = "credit_note_entries[' + data[0] + '][' + invoice_entry.ipe_linked_product_id + '][8]" type = "text" value = "' + invoice_entry.ipe_id + '" readonly></input><a href="javascript: void(0)" onclick = "deleteCreditNoteRow(this);calculateCreditNoteNumbers();"><i class="glyphicon glyphicon-trash"></i></a>';

                        creditNoteDataTable.row.add(data).draw(true);

                    }
                    calculateCreditNoteNumbers();
                    if (total_credit_note_balance <= 0) {
                        swal({
                            title: "Error",
                            text: "No balance quantity left",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                        clearCreditNoteForm();
                    }
                }
            });
        } else if (document.getElementById('credit_against_purchase').checked) {
            $.get("/index.php/web/purchase/json/purchase/" + invoiceId, function (data, status) {
                if (status == 'success') {
                    var purchase = JSON.parse(data).purchase;
                    $("input[name=cn_linked_company_invoice_name]").val(purchase.purchase_linked_company_invoice_name);
                    //$("input[name=cn_date]").val(purchase.purchase_date.split(' ')[0]);
                    $("textarea[name=cn_linked_company_billing_address]").val(purchase.purchase_linked_company_billing_address);
                    $("textarea[name=cn_linked_company_shipping_address]").val(purchase.purchase_linked_company_shipping_address);

                    $("select[name=cn_linked_company_billing_state_id]").val(purchase.purchase_linked_company_billing_state_id).trigger("change");
                    $("select[name=cn_linked_company_shipping_state_id]").val(purchase.purchase_linked_company_shipping_state_id).trigger("change");

                    $("select[name=cn_linked_company_gst_supply_state_id]").val(purchase.purchase_linked_company_gst_supply_state_id).trigger("change");

                    $("select[name=cn_tax_type]").val(purchase.purchase_tax_type).trigger("change");
                    ;
                    $("select[name=cn_linked_broker_id]").val(purchase.purchase_linked_broker_id).trigger("change");
                    $("input[name=cn_linked_broker_commission]").val(purchase.purchase_linked_broker_commission);
                    $("select[name=cn_linked_transporter_id]").val(purchase.purchase_linked_transporter_id).trigger("change");
                    ;
                    $("input[name=cn_lr_number]").val(purchase.purchase_lr_number);

                    $("select[name=cn_linked_company_payment_term_id]").val(purchase.purchase_linked_company_payment_term_id).trigger("change");

                    $("input[name=cn_order_reference]").val(purchase.purchase_order_reference);
                    $("input[name=cn_notes]").val(purchase.purchase_notes);

                    $("input[name=cn_linked_employee_id]").val(purchase.purchase_linked_employee_id);

                    var purchase_entries = JSON.parse(data).purchase_entries;
                    creditNoteDataTable.clear().draw();
                    for (var i = 0; i < purchase_entries.length; i++) {

                        var purchase_entry = purchase_entries[i];

                        var data = [];
                        data[0] = purchase_entry.pe_entry_id;
                        data[1] = purchase_entry.pe_product_name + '<input style = "display:none" name = "credit_note_entries[' + data[0] + '][' + purchase_entry.pe_linked_product_id + '][0]" type = "text" value = "' + purchase_entry.pe_product_name + '" readonly></input>';
                        data[2] = purchase_entry.pe_product_hsn_code + '<input style = "display:none" name = "credit_note_entries[' + data[0] + '][' + purchase_entry.pe_linked_product_id + '][1]" type = "text" value = "' + purchase_entry.pe_product_hsn_code + '" readonly></input>';
                        data[3] = '<input name = "credit_note_entries[' + data[0] + '][' + purchase_entry.pe_linked_product_id + '][2]" tax_value = ' + purchase_entry.pe_tax_percent_applicable + ' style = "display:none" type = "text" value = "' + purchase_entry.pe_tax_id_applicable + '" readonly></input>'
                                +
                                '<input class = "form-control" type = "text" value = "' + purchase_entry.pe_tax_name_applicable + '" readonly></input>';
                        data[4] = '<input name = "credit_note_entries[' + data[0] + '][' + purchase_entry.pe_linked_product_id + '][3]" class = "form-control" type = "number" step = 0.01 value = "' + purchase_entry.pe_product_rate + '" onchange = "calculateCreditNoteNumbers()"></input>';
                        data[5] = '<input name = "credit_note_entries[' + data[0] + '][' + purchase_entry.pe_linked_product_id + '][4]" id = "quantity" class = "form-control" type = "number" step = 0.01 value = "' + purchase_entry.pe_product_quantity + '" onchange = "calculateCreditNoteNumbers()"></input>'
                                + '<input name = "credit_note_entries[' + data[0] + '][' + purchase_entry.pe_linked_product_id + '][5]" value = "' + purchase_entry.pe_product_uqc_id + '" style = "display:none" ></input>'
                                + '<input name = "credit_note_entries[' + data[0] + '][' + purchase_entry.pe_linked_product_id + '][6]" value = "' + purchase_entry.pe_product_uqc_text + '" style = "display:none" ></input>';

                        data[6] = '<input name = "credit_note_entries[' + data[0] + '][' + purchase_entry.pe_linked_product_id + '][7]" readonly min = 0 max = 100 id = "discount" class = "form-control" type = "number" step = 0.01 value = "' + purchase_entry.pe_discount + '" onchange = "calculateCreditNoteNumbers()"></input>';
                        data[7] = 0;
                        data[8] = '<input style = "display:none" name = "credit_note_entries[' + data[0] + '][' + purchase_entry.pe_linked_product_id + '][8]" type = "text" value = "' + purchase_entry.pe_id + '" readonly></input><a href="javascript: void(0)" onclick = "deleteCreditNoteRow(this);calculateCreditNoteNumbers();"><i class="glyphicon glyphicon-trash"></i></a>';

                        creditNoteDataTable.row.add(data).draw(true);

                    }
                    calculateCreditNoteNumbers();
                }
            });

        }
    }
}

function refreshInvoiceListForCreditNote() {

    var credit_note_linked_company_name = document.getElementById('credit_note_linked_company_name');
    //var selected_company_id = -1;
    var selected_company_id = null;
    var selected_company_name = null;
    var selectedOption = credit_note_linked_company_name.options[credit_note_linked_company_name.selectedIndex];

    selected_company_id = selectedOption.value;
    selected_company_name = selectedOption.text;

    var credit_note_linked_invoice_id = document.getElementById('credit_note_linked_invoice_id');
    removeOptionsInCreditNoteForm(credit_note_linked_invoice_id);
    addPlaceholder(credit_note_linked_invoice_id, 'an Invoice');

    if (invoice_customer_map != null && document.getElementById('credit_against_sales').checked) {
        var invoiceOptGroup = document.createElement("OPTGROUP");
        invoiceOptGroup.label = 'Please Select a customer to load invoices';
        var invoiceAdded = false;
        for (var i = 0; i < invoice_customer_map.length; i++) {
            var option = document.createElement("option");
            option.value = invoice_customer_map[i].invoice_id;
            option.text = 'INV' + pad(invoice_customer_map[i].invoice_id, 5);
            option.setAttribute("company_id", invoice_customer_map[i].invoice_linked_company_id);
            option.setAttribute("company_name", invoice_customer_map[i].invoice_linked_company_invoice_name);

            invoiceOptGroup.label = 'Invoice For Selected Customer';
            if (selected_company_id == invoice_customer_map[i].invoice_linked_company_id
                    && invoice_customer_map[i].invoice_linked_company_id != null) {
                invoiceOptGroup.appendChild(option);
            } else if (selected_company_name.trim() == invoice_customer_map[i].invoice_linked_company_display_name.trim()) {
                invoiceOptGroup.appendChild(option);
            }
        }

        credit_note_linked_invoice_id.appendChild(invoiceOptGroup);
    }

    if (purchase_vendor_map != null && document.getElementById('credit_against_purchase').checked) {
        var purchaseOptGroup = document.createElement("OPTGROUP");
        purchaseOptGroup.label = 'Please Select a customer to load purchase invoices';
        for (var i = 0; i < purchase_vendor_map.length; i++) {
            var option = document.createElement("option");
            option.value = purchase_vendor_map[i].purchase_id;
            option.text = 'PUR' + pad(purchase_vendor_map[i].purchase_id, 5);
            option.setAttribute("company_id", purchase_vendor_map[i].purchase_linked_company_id);
            option.setAttribute("company_name", purchase_vendor_map[i].purchase_linked_company_invoice_name);

            //if (selected_company_id == "0") {
            if (selected_company_id == null && selected_company_name == null) {
                //if(purchase_vendor_map[i].purchase_linked_company_id == null) purchaseOptGroup.appendChild(option);
            } else {
                purchaseOptGroup.label = 'Purchase For Selected Vendor';
                if (selected_company_id == purchase_vendor_map[i].purchase_linked_company_id
                        && purchase_vendor_map[i].purchase_linked_company_id!=null) {
                    purchaseOptGroup.appendChild(option);
                } else if (selected_company_name.trim() == purchase_vendor_map[i].purchase_linked_company_display_name.trim()) {
                    purchaseOptGroup.appendChild(option);
                }
            }
        }
        credit_note_linked_invoice_id.appendChild(purchaseOptGroup);
    }
}

function initializeCreditNoteDataTable() {
    creditNoteDataTable = $('.creditNoteDatatable').DataTable({
        autoWidth: false,
        columnDefs: [{
                width: '150px',
                targets: [3, 4, 5, 6]
            },
            {
                className: "row text-center",
                targets: [8]
            }
        ],
        order: [[0, "asc"]],
        searching: false,
        paging: false,
        bInfo: false,
        bSort: false
    });
    
    creditNoteChargesDataTable = $('#creditNoteChargesDatatable').DataTable({
        autoWidth: false,
        columnDefs: [
            {
                width: '200px',
                targets: [0]
            },
            {
                className: "row text-center",
                targets: [4]
            }
        ],
        order: [[0, "asc"]],
        searching: false,
        paging: false,
        bInfo: false,
        bSort: false
    });
}


function loadCustomersAndVendorsForCreditNoteForm() {

    $.get("/index.php/web/invoice/json/customer", function (data, status) {
        if (status == 'success') {
            customerListForCreditNoteForm = JSON.parse(data).customers;
            refreshCustomerListForCreditNote();
        }
    });

    $.get("/index.php/web/purchase/json/vendor", function (data, status) {
        if (status == 'success') {
            vendorListForCreditNoteForm = JSON.parse(data).vendors;
            refreshCustomerListForCreditNote();
        }
    });
}

function addPlaceholder(element, company_type) {
    var option = document.createElement("option");
    option.value = '';
    option.text = 'Choose ' + company_type;
    option.setAttribute("company_id", '');
    element.appendChild(option);
}

function refreshCustomerListForCreditNote() {

    var credit_note_linked_company_name = document.getElementById('credit_note_linked_company_name');
    removeOptionsInCreditNoteForm(credit_note_linked_company_name);
    addPlaceholder(credit_note_linked_company_name, 'a Company');
    if (document.getElementById('credit_against_sales').checked) {
        if (customerListForCreditNoteForm != null) {
            addPlaceholder(document.getElementById('credit_note_linked_company_id'), 'a Customer');
            for (var i = 0; i < customerListForCreditNoteForm.length; i++) {
                var option = document.createElement("option");
                option.value = customerListForCreditNoteForm[i].invoice_linked_company_id;
                option.text = customerListForCreditNoteForm[i].invoice_linked_company_display_name;
                credit_note_linked_company_name.appendChild(option);

            }
        }
    } else if (document.getElementById('credit_against_purchase').checked) {
        if (vendorListForCreditNoteForm != null) {
            addPlaceholder(document.getElementById('credit_note_linked_company_id'), 'a Vendor');
            for (var i = 0; i < vendorListForCreditNoteForm.length; i++) {
                var option = document.createElement("option");
                option.value = vendorListForCreditNoteForm[i].purchase_linked_company_id;
                option.text = vendorListForCreditNoteForm[i].purchase_linked_company_display_name;
                credit_note_linked_company_name.appendChild(option);
            }
        }
    }
}

function toggleElementsForCreditNote() {
    if (document.getElementById('credit_against_sales').checked) {
        $("select[name=credit_note_linked_broker_id]").prop('disabled', false);
        $("input[name=credit_note_linked_broker_commission]").prop('disabled', false);
    } else if (document.getElementById('credit_against_purchase').checked) {
        $("select[name=credit_note_linked_broker_id]").prop('disabled', true);
        $("input[name=credit_note_linked_broker_commission]").prop('disabled', true);
    } else {
        alert("Error");
    }
}

function updateCustomerIdForCreditNoteForm() {
    var credit_note_linked_company_name = document.getElementById('credit_note_linked_company_name');
    var selectedOption = credit_note_linked_company_name.options[credit_note_linked_company_name.selectedIndex];
    document.getElementById('credit_note_linked_company_id').value = selectedOption.value;
}

function updateCreditNoteCharges(element, defaultCreditNoteChargeId){
    
    var creditNoteChargesListSize = 0;
    creditNoteChargesDataTable.column(0).nodes().each(function (cell, i) {
        creditNoteChargesListSize++;
    });

    var rowIdx = creditNoteChargesListSize;
    var creditNoteChargeId = creditNoteChargeList[0].charge_id;
    if(defaultCreditNoteChargeId){
        creditNoteChargeId = defaultCreditNoteChargeId;
    }
    var applicableTaxId = parseInt(productListForCreditNoteForm[0].intra_state_tax_id);
    var taxes = intrastate_taxes_for_credit_note;
    if (interStateTaxIsApplicableForCreditNote) {
        applicableTaxId = parseInt(productListForCreditNoteForm[0].inter_state_tax_id);
        taxes = interstate_taxes_for_credit_note;
    }
    
    
    var taxableAmount = 0.01;
    var totalAmount = 0;
    //var tax_percent = 0;
    if (element != null) {
        rowIdx = $(element).parents('tr')[0].rowIndex - 1;
        creditNoteChargeId = element.selectedOptions[0].value;	//$('#priorityList2 option:selected').val()
        taxableAmount = creditNoteChargesDataTable.cell(rowIdx, 1).nodes().to$().find('input').val();
        applicableTaxId = creditNoteChargesDataTable.cell(rowIdx, 2).nodes().to$().find('select').val();
        //tax_percent = parseFloat(creditNoteChargesDataTable.cell(rowIdx, 2).nodes().to$().find('select')[0].selectedOptions[0].attributes['tax_value'].value);
        //total_amount = (1 + tax_percent/100) * taxableAmount;
    }

    //PRODUCT
    var creditNoteChargeSelectList = document.createElement("select");
    creditNoteChargeSelectList.setAttribute("onchange", "updateCreditNoteCharges(this);calculateCreditNoteNumbers();");
    creditNoteChargeSelectList.classList.add("form-control");
    creditNoteChargeSelectList.classList.add("dtSelect2");
    //Create and append the options
    for (var i = 0; i < creditNoteChargeList.length; i++) {
        var option = document.createElement("option");
        option.value = creditNoteChargeList[i].charge_id;
        option.text = creditNoteChargeList[i].charge_name;
        creditNoteChargeSelectList.appendChild(option);

        if (creditNoteChargeId == creditNoteChargeList[i].charge_id) {
            option.selected = true;
            option.setAttribute("selected", "selected");
        }
    }
    
    var creditNoteChargeStr = creditNoteChargeSelectList.outerHTML;

    //TAXES
    var taxSelectList = document.createElement("select");
    taxSelectList.setAttribute("onchange", "calculateCreditNoteNumbers();");
    taxSelectList.setAttribute("name", "charge_entries[" + (rowIdx + 1) + "][" + creditNoteChargeId + "][2]");
    taxSelectList.classList.add("form-control");
    taxSelectList.classList.add("dtSelect");
    //var taxes = [0, 5, 12, 18, 28];
    //Create and append the options
    for (var i = 0; i < taxes.length; i++) {
        var option = document.createElement("option");
        option.value = taxes[i].tax_id;
        option.setAttribute("tax_value", taxes[i].tax_percent);
        option.text = taxes[i].tax_name;
        taxSelectList.appendChild(option);

        if (applicableTaxId == taxes[i].tax_id) {
            option.selected = true;
            option.setAttribute("selected", "selected");
        }
    }
    
    var taxStr = taxSelectList.outerHTML;

    var data = [];
    data[0] = creditNoteChargeStr + '<input name = "charge_entries[' + (rowIdx + 1) + '][' + creditNoteChargeId + '][0]" value = "' + idcreditNoteChargeMap[creditNoteChargeId].charge_name + '" style = "display:none" ></input>';
    data[1] = '<input style = "min-width:100px" name = "charge_entries[' + (rowIdx + 1) + '][' + creditNoteChargeId + '][1]" value = "' + taxableAmount + '" class = "form-control" min = "0.01" step ="0.01" type = "number" onchange = "calculateCreditNoteNumbers();"></input>';
    data[2] = taxStr;
    data[3] = '';

    data[4] = '<a href="javascript: void(0)" onclick = "deleteCreditNoteChargeRow(this);calculateCreditNoteNumbers();"><i class="glyphicon glyphicon-trash"></i></a>';
    //console.log(data);
    if (element == null) {
        creditNoteChargesDataTable.row.add(data).draw(true);
    } else {
        creditNoteChargesDataTable.row(rowIdx).data(data).draw(true);
    } 

    //calculateCreditNoteNumbers();
    initializeDataTableSelect2();
} 


function calculateCreditNoteNumbers() {
    if (taxIsExclusiveForCreditNote) {
        $('#subTotalLabel').text("Sub Total ( Exclusive Of Tax )");
    } else {
        $('#subTotalLabel').text("Sub Total ( Inclusive Of Tax )");
    }
    var totalAmount = parseFloat(0);
    var totalTaxAmount = parseFloat(0);
    var postTotalTaxAmount = parseFloat(0);
    var taxAmountMap = {};
    var productTotalForCreditNote = parseFloat(0);
    creditNoteDataTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var data = this.data();

        var tax_input = creditNoteDataTable.cell(rowIdx, 3).nodes().to$().find('input');	// GET ATTRIBUTE AND NOT VALUE
        var tax = $(tax_input).attr('tax_value');
        var cost = creditNoteDataTable.cell(rowIdx, 4).nodes().to$().find('input').val();
        var quantity = creditNoteDataTable.cell(rowIdx, 5).nodes().to$().find('input').val();
        var discount = creditNoteDataTable.cell(rowIdx, 6).nodes().to$().find('input').val();

        var amount = ((cost * quantity) * (1 - (discount / 100))).toFixed(2);
        totalAmount = parseFloat(totalAmount) + parseFloat(amount);
        productTotalForCreditNote = parseFloat(productTotalForCreditNote) + parseFloat(amount);
        var taxAmount = parseFloat(0);
        if (taxIsExclusiveForCreditNote) {
            taxAmount = amount * (tax / 100);
        } else {
            //(tax * amount) / ( 100 + tax )
            taxAmount = (parseFloat(tax) * parseFloat(amount)) / (100 + parseFloat(tax))
        }

        if (taxAmountMap[tax] == null) {
            taxAmountMap[tax] = taxAmount;
        } else {
            taxAmountMap[tax] = taxAmountMap[tax] + taxAmount;
        }

        totalTaxAmount = (parseFloat(totalTaxAmount) + parseFloat(taxAmount)).toFixed(2);
        creditNoteDataTable.cell(rowIdx, 7).data('₹ ' + amount);
    });
    
    
    creditNoteChargesDataTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var data = this.data();
        //var charge_id = chargesDataTable.cell(rowIdx, 0).nodes().to$().find('select')[0].selectedOptions[0].value;
        var taxableAmount = creditNoteChargesDataTable.cell(rowIdx, 1).nodes().to$().find('input').val();
        var tax = creditNoteChargesDataTable.cell(rowIdx, 2).nodes().to$().find('select')[0].selectedOptions[0].attributes['tax_value'].value;
        var taxAmountOnCharge = parseFloat(0);
        var totalChargeAmount = parseFloat(0);
        if (taxIsExclusiveForCreditNote) {
            taxAmountOnCharge = taxableAmount * (tax / 100);
            totalChargeAmount = (parseFloat(taxableAmount) + parseFloat(taxAmountOnCharge)).toFixed(2);
        } else {
            //(tax * amount) / ( 100 + tax )
            taxAmountOnCharge = (parseFloat(tax) * parseFloat(taxableAmount)) / (100 + parseFloat(tax));
            totalChargeAmount = taxableAmount;
        } 

        if (taxAmountMap[tax] == null) {
            taxAmountMap[tax] = taxAmountOnCharge;
        } else {
            taxAmountMap[tax] = taxAmountMap[tax] + taxAmountOnCharge;
        }
 
        totalTaxAmount = parseFloat(totalTaxAmount) + parseFloat(taxAmountOnCharge);
        totalAmount = parseFloat(totalAmount) + parseFloat(taxableAmount);
        creditNoteChargesDataTable.cell(rowIdx, 3).data('<span style = "white-space: nowrap;">₹ ' + totalChargeAmount + '</span>');
    });

    document.getElementById('totalCreditNoteSubTotalAmount').innerHTML = '₹ ' + parseFloat(totalAmount).toFixed(2);

    var taxStr = '';

    for (var taxPercent in taxAmountMap) {
        //if(taxAmountMap[taxPercent]!=0){
        if (interStateTaxIsApplicableForCreditNote) {

            taxStr += '<div class="form-group col-lg-12">' +
                    '<label class="control-label col-lg-6">IGST@' + taxPercent + '%</label>' +
                    '<div class="col-lg-6">' +
                    '<h6 class = "no-margin text-bold">₹ ' + taxAmountMap[taxPercent].toFixed(2) + '<h6>' +
                    '</div>' +
                    '</div>';

        } else {
            taxStr += '<div class="form-group col-lg-12">' +
                    '<label class="control-label col-lg-6">SGST@' + (taxPercent / 2) + '%</label>' +
                    '<div class="col-lg-6">' +
                    '<h6 class = "no-margin text-bold">₹ ' + (taxAmountMap[taxPercent] / 2).toFixed(2) + '<h6>' +
                    '</div>' +
                    '</div>' +
                    '<div class="form-group col-lg-12">' +
                    '<label class="control-label col-lg-6">CGST@' + (taxPercent / 2) + '%</label>' +
                    '<div class="col-lg-6">' +
                    '<h6 class = "no-margin text-bold">₹ ' + (taxAmountMap[taxPercent] / 2).toFixed(2) + '<h6>' +
                    '</div>' +
                    '</div>';
            ;

        }
        //}
    }

    document.getElementById('taxDiv').innerHTML = taxStr;

    var transportationCharges = 0;
    if (document.getElementById('transportationCharges').value != "") {
        transportationCharges = document.getElementById('transportationCharges').value;
    }
    var adjustments = 0;
    if (document.getElementById('adjustments').value != "") {
        adjustments = document.getElementById('adjustments').value;
    }

    if (taxIsExclusiveForCreditNote) {
        postTotalTaxAmount = parseFloat(totalTaxAmount) + parseFloat(totalAmount);
    } else {
        postTotalTaxAmount = totalAmount;
    }

    var creditNoteTotal = parseFloat(postTotalTaxAmount) + parseFloat(transportationCharges) + parseFloat(adjustments);

    document.getElementById('creditNoteTotal').innerHTML = '₹ ' + parseFloat(creditNoteTotal).toFixed(2);
    document.getElementById('creditNoteAmount').value = parseFloat(creditNoteTotal).toFixed(2);

    if (productTotalForCreditNote <= 0) {
        if (document.getElementById('buttonSaveDraftCn') != null)
            document.getElementById('buttonSaveDraftCn').disabled = true;
        if (document.getElementById('buttonPreviewDraftCn') != null)
            document.getElementById('buttonPreviewDraftCn').disabled = true;
        if (document.getElementById('buttonSaveConfirmCn') != null)
            document.getElementById('buttonSaveConfirmCn').disabled = true;
    } else {
        if (parseFloat(invoice_amount) > 0) {
            if (creditNoteTotal > parseFloat(invoice_amount)) {
                swal({
                    title: "Invoice Amount Exceeded",
                    text: "Credit note total has exceeded Invoice amount - ₹ " + invoice_amount,
                    confirmButtonColor: "#2196F3",
                    type: "warning"
                });
            }
        }

        if (document.getElementById('buttonSaveDraftCn') != null)
            document.getElementById('buttonSaveDraftCn').disabled = false;
        if (document.getElementById('buttonPreviewDraftCn') != null)
            document.getElementById('buttonPreviewDraftCn').disabled = false;
        if (document.getElementById('buttonSaveConfirmCn') != null)
            document.getElementById('buttonSaveConfirmCn').disabled = false;
    }
}