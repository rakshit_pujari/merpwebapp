
var purchaseDataTable;
var purchaseChargesDataTable;
var taxIsExclusiveForPurchase = true;
var interStateTaxIsApplicableForPurchase = false;
var P_OC_GST_STATE_ID = -1;
var interstate_taxes_for_purchase = null;
var intrastate_taxes_for_purchase = null;

var purchaseProductList = [];

var idPurchaseProductMap = {};

var advancePayments;

function initialize_purchase_form() {
    loadOcGstStateIdForPurchase();

    var purchaseDate = new Date();
    var purchaseDateString = $('#purchase_date_picker').val();
    if (purchaseDateString != null && purchaseDateString != "") {
        try {
            purchaseDate = $.datepicker.parseDate("yy-mm-dd", purchaseDateString);
        } catch (Err) {

        }
    }
    $('#purchase_date_picker').datepicker({
        dateFormat: 'yy-mm-dd'
    }).datepicker("setDate", purchaseDate);

    var purchaseBillDate = new Date();
    var purchaseBillDateString = $('#purchase_bill_date_picker').val();
    if (purchaseBillDateString != null && purchaseBillDateString != "") {
        try {
            purchaseBillDate = $.datepicker.parseDate("yy-mm-dd", purchaseBillDateString);
        } catch (Err) {

        }
    }
    $('#purchase_bill_date_picker').datepicker({
        dateFormat: 'yy-mm-dd'
    }).datepicker("setDate", purchaseBillDate);

    initializeFormValidator();
}

function resetPurchaseLinkedCompanyId() {
    $('[name="purchase_linked_company_id"]').val('');
    rebuildAdvancePaymentMultiselect();
}

function initializePurchaseSelect2() {
    // Select with search
    $('.select2').select2();
    $('.select').select2({//keep this below $('.select2').select2();
        minimumResultsForSearch: Infinity
    });
}

function initializePurchaseDataTableSelect2() {
    $('.dtSelect2').select2();
    $('.dtSelect').select2({
        minimumResultsForSearch: Infinity
    });
}


function initializePurchaseDataTable() {
    purchaseDataTable = $('.purchaseDatatable').DataTable({
        autoWidth: false,
        columnDefs: [{
                width: '150px',
                targets: [3, 4, 5, 6]
            },
            {
                className: "row text-center",
                targets: [8]
            }
        ],
        order: [[0, "asc"]],
        searching: false,
        paging: false,
        bInfo: false,
        bSort: false
    });

    purchaseChargesDataTable = $('#purchaseChargesDatatable').DataTable({
        autoWidth: false,
        columnDefs: [
            {
                width: '200px',
                targets: [0]
            },
            {
                className: "row text-center",
                targets: [4]
            }
        ],
        order: [[0, "asc"]],
        searching: false,
        paging: false,
        bInfo: false,
        bSort: false
    });
}

function loadAdvancePayments(){
    $.get("/index.php/web/AdvancePayment/json", function (data, status) {
        if (status == 'success') {
            advancePayments = JSON.parse(data).advance_payments;
        }
    });
}

function loadVendors() {

    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            var data = JSON.parse(request.responseText).companies;

            $('.vendor_names').typeahead(
                    {
                        hint: true,
                        highlight: true,
                        minLength: 0
                    },
                    {
                        limit:100,
                        source: function (query, process) {
                            objects = [];
                            map = {};
                            //data = [{"id":1,"label":"machin ltd"},{"id":2,"label":"truc"}] // Or get your JSON dynamically and load it into this variable
                            $.each(data, function (i, object) {
                                var company = (object.company_display_name).toLowerCase();
                                if (company.includes(query.toLowerCase())) {
                                    map[object.company_display_name] = object;	// using company_display_name because company_display_name is unique key in company table
                                    objects.push(object.company_display_name);
                                }
                            });
                            process(objects);
                        }
                    });
            $('.vendor_names').on('typeahead:selected', function (e, datum) {
                //alert(datum.value);
                //alert(map[datum].company_id);
                $('[name="purchase_linked_company_invoice_name"]').val(map[datum].company_invoice_name);
                $('[name="purchase_linked_company_id"]').val(map[datum].company_id);
                var address = '';
                if ((map[datum].company_billing_address)) {
                    address += map[datum].company_billing_address;
                    if (!address.trim().endsWith(",")) {
                        address = address + ', ';
                        ;
                    }
                }
                if ((map[datum].company_billing_area)) {
                    address += map[datum].company_billing_area + ', ';
                }
                if ((map[datum].city_name)) {
                    address += map[datum].billing_city_name + ', ';
                }
                if ((map[datum].district)) {
                    address += map[datum].billing_district + ', ';
                }
                if ((map[datum].company_billing_pincode)) {
                    address += map[datum].company_billing_pincode;
                }

                if (address.trim().endsWith(",")) {
                    address = address.trim().slice(0, -1);
                }
                
                $('[name="purchase_billing_contact_person_name"]').val(map[datum].company_contact_person_name);
                $('[name="purchase_billing_contact_number"]').val(map[datum].company_contact_number);
                $('[name="purchase_linked_company_gstin"]').val(map[datum].company_gst_number);
                $('[name="purchase_linked_company_pan_number"]').val(map[datum].company_pan_number);
                //map[datum].company_billing_address + ", " + map[datum].company_billing_area  + ", " + map[datum].city_name + ", " + map[datum].district + ", " + map[datum].company_billing_pincode
                $('[name="purchase_linked_company_billing_address"]').val(address);
                $('#purchase_linked_company_billing_state_id').val(map[datum].company_billing_state_id).trigger("change");
                //$('#purchase_linked_company_gst_supply_state_id').val(map[datum].company_gst_supply_state_id).trigger("change");
                $('#purchase_linked_company_payment_term_id').val(map[datum].company_payment_term_id).trigger("change");
                $('[name="purchase_linked_company_pincode"]').val(map[datum].company_pincode);
                
                rebuildAdvancePaymentMultiselect(map[datum].company_id);
            });

        }
    }

    request.open("GET", "/index.php/web/company/json/vendor", true);

    request.setRequestHeader("content-type", "application/json");
    request.send();
}

function rebuildAdvancePaymentMultiselect(companyId) {
    var options = [];
    for (var i = 0; i < advancePayments.length; i++) {
        if(advancePayments[i].advance_payment_linked_company_id == companyId){
            var option = {label: 'ADP' + String("00000" + advancePayments[i].advance_payment_id).slice(-5) + ' - ( Balance -  ₹'+ advancePayments[i].balance_advance_payment_amount + ' )', value: advancePayments[i].advance_payment_id};
            options.push(option);
        }
    }
    $('#purchase_advance_payment_ids').multiselect('dataprovider', options);
    //$('#invoice_advance_payment_ids').multiselect('rebuild');
    $(".styled, .multiselect-container input").uniform({ radioClass: 'choice'});
}

function deletePurchaseRow(element) {
    purchaseDataTable.row($(element).parents('tr'))
            .remove()
            .draw();
    renumberPurchaseTable();
    //refreshAndReIndexEntirePurchaseTable();
    //calculatePurchaseNumbers();
}

function deletePurchaseChargeRow(element) {
    purchaseChargesDataTable.row($(element).parents('tr'))
            .remove()
            .draw();
}

function renumberPurchaseTable() {
    purchaseDataTable.column(0).nodes().each(function (cell, i) {
        cell.innerHTML = i + 1;
    });
}

function loadPurchaseProductsList() {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            var productsArray = JSON.parse(request.responseText).products;
            purchaseProductList = [];
            for (var i = 0; i < productsArray.length; i++) {
                var product = productsArray[i];
                purchaseProductList.push(product);
                idPurchaseProductMap[product.product_id] = product;
            }

            if (!purchaseDataTable.data().count()){
                updatePurchaseProducts();//add 1 row by default if empty
                
                //add default charges
                for (var i = 0; i < purchaseChargeList.length; i++) {
                    if(purchaseChargeList[i].charge_is_default_type == 1){
                        updatePurchaseCharges(null, purchaseChargeList[i].charge_id);
                    }
                }
            }
                
            updateTaxesInPurchaseTable();
            calculatePurchaseNumbers();
        }
    };
    request.open("GET", "/index.php/web/product/json/purchase", true);
    request.send();
}


function loadTaxesForPurchase() {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            interstate_taxes_for_purchase = JSON.parse(request.responseText).interstate_taxes;
            intrastate_taxes_for_purchase = JSON.parse(request.responseText).intrastate_taxes;
            loadPurchaseProductsList();
            loadVendors();
            loadAdvancePayments();
        }
    };
    request.open("GET", "/index.php/web/tax/json", true);
    request.send();
}

var purchaseChargeList = null;
var idPurchaseChargeMap = {};
function loadPurchaseCharges() {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            purchaseChargeList = JSON.parse(request.responseText).charges;
            for (var i = 0; i < purchaseChargeList.length; i++) {
                var charge = purchaseChargeList[i];
                idPurchaseChargeMap[charge.charge_id] = charge;
            }
        }
    };
    request.open("GET", "/index.php/web/charge/json/purchase", true);
    request.send();
}


function loadOcGstStateIdForPurchase() {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            var stateId = JSON.parse(request.responseText).state_id;
            P_OC_GST_STATE_ID = stateId;
            if (P_OC_GST_STATE_ID > 0) {	// continue only if GST ID is valid
                initializePurchaseDataTable();
                loadTaxesForPurchase();
                loadPurchaseCharges();
                initializePurchaseSelect2();
                initializePurchaseDataTableSelect2();
            } else {
                alert('Failed to load OC gst state ID. Please contact system admin');
            }
        }
    };
    request.open("GET", "/index.php/web/OC/state", true);
    request.send();
}

function updatePurchaseCharges(element, defaultPurchaseChargeId){
    
    var purchaseChargesListSize = 0;
    purchaseChargesDataTable.column(0).nodes().each(function (cell, i) {
        purchaseChargesListSize++;
    });

    var rowIdx = purchaseChargesListSize;
    var purchaseChargeId = purchaseChargeList[0].charge_id;
    if(defaultPurchaseChargeId){
        purchaseChargeId = defaultPurchaseChargeId;
    }
    var applicableTaxId = parseInt(purchaseProductList[0].intra_state_tax_id);
    var taxes = intrastate_taxes_for_purchase;
    if (interStateTaxIsApplicableForPurchase) {
        applicableTaxId = parseInt(purchaseProductList[0].inter_state_tax_id);
        taxes = interstate_taxes_for_purchase;
    }
    
    
    var taxableAmount = 0.01;
    var totalAmount = 0;
    //var tax_percent = 0;
    if (element != null) {
        rowIdx = $(element).parents('tr')[0].rowIndex - 1;
        purchaseChargeId = element.selectedOptions[0].value;	//$('#priorityList2 option:selected').val()
        taxableAmount = purchaseChargesDataTable.cell(rowIdx, 1).nodes().to$().find('input').val();
        applicableTaxId = purchaseChargesDataTable.cell(rowIdx, 2).nodes().to$().find('select').val();
        //tax_percent = parseFloat(purchaseChargesDataTable.cell(rowIdx, 2).nodes().to$().find('select')[0].selectedOptions[0].attributes['tax_value'].value);
        //total_amount = (1 + tax_percent/100) * taxableAmount;
    }

    //PRODUCT
    var purchaseChargeSelectList = document.createElement("select");
    purchaseChargeSelectList.setAttribute("onchange", "updatePurchaseCharges(this);calculatePurchaseNumbers();");
    purchaseChargeSelectList.classList.add("form-control");
    purchaseChargeSelectList.classList.add("dtSelect2");
    //Create and append the options
    for (var i = 0; i < purchaseChargeList.length; i++) {
        var option = document.createElement("option");
        option.value = purchaseChargeList[i].charge_id;
        option.text = purchaseChargeList[i].charge_name;
        purchaseChargeSelectList.appendChild(option);

        if (purchaseChargeId == purchaseChargeList[i].charge_id) {
            option.selected = true;
            option.setAttribute("selected", "selected");
        }
    }
    
    var purchaseChargeStr = purchaseChargeSelectList.outerHTML;

    //TAXES
    var taxSelectList = document.createElement("select");
    taxSelectList.setAttribute("onchange", "calculatePurchaseNumbers();");
    taxSelectList.setAttribute("name", "charge_entries[" + (rowIdx + 1) + "][" + purchaseChargeId + "][2]");
    taxSelectList.classList.add("form-control");
    taxSelectList.classList.add("dtSelect");
    //var taxes = [0, 5, 12, 18, 28];
    //Create and append the options
    for (var i = 0; i < taxes.length; i++) {
        var option = document.createElement("option");
        option.value = taxes[i].tax_id;
        option.setAttribute("tax_value", taxes[i].tax_percent);
        option.text = taxes[i].tax_name;
        taxSelectList.appendChild(option);

        if (applicableTaxId == taxes[i].tax_id) {
            option.selected = true;
            option.setAttribute("selected", "selected");
        }
    }
    
    var taxStr = taxSelectList.outerHTML;

    var data = [];
    data[0] = purchaseChargeStr + '<input name = "charge_entries[' + (rowIdx + 1) + '][' + purchaseChargeId + '][0]" value = "' + idPurchaseChargeMap[purchaseChargeId].charge_name + '" style = "display:none" ></input>';
    data[1] = '<input style = "min-width:100px" name = "charge_entries[' + (rowIdx + 1) + '][' + purchaseChargeId + '][1]" value = "' + taxableAmount + '" class = "form-control" min = "0.01" step ="0.01" type = "number" onchange = "calculatePurchaseNumbers();"></input>';
    data[2] = taxStr;
    data[3] = '';

    data[4] = '<a href="javascript: void(0)" onclick = "deleteChargeRow(this);refreshAndReIndexEntireTable();calculatePurchaseNumbers();"><i class="glyphicon glyphicon-trash"></i></a>';
    //console.log(data);
    if (element == null) {
        purchaseChargesDataTable.row.add(data).draw(true);
    } else {
        purchaseChargesDataTable.row(rowIdx).data(data).draw(true);
    } 

    //calculatePurchaseNumbers();
    initializeDataTableSelect2();
} 



/*
 0 - Product Name
 1 - Tax
 2 - HSN
 3 - rate
 4 - quantity
 5 - discount
 */
function updatePurchaseProducts(element, updateFields) {

    var productsListSize = 0;
    purchaseDataTable.column(0).nodes().each(function (cell, i) {
        productsListSize++;
    });

    var rowIdx = productsListSize;
    var productId = purchaseProductList[0].product_id;
    var applicable_tax_id = parseInt(purchaseProductList[0].intra_state_tax_id);
    var taxes = intrastate_taxes_for_purchase;
    if (interStateTaxIsApplicableForPurchase) {
        applicable_tax_id = parseInt(purchaseProductList[0].inter_state_tax_id);
        taxes = interstate_taxes_for_purchase;
    }
    var quantity = 1;
    var discount = 0;
    var purchase_price = idPurchaseProductMap[productId].purchase_price;
    if (element != null) {
        rowIdx = $(element).parents('tr')[0].rowIndex - 1;
        productId = element.selectedOptions[0].value;	//$('#priorityList2 option:selected').val()	
        if (updateFields) {
            applicable_tax_id = parseInt(idPurchaseProductMap[productId].intra_state_tax_id);
            if (interStateTaxIsApplicableForPurchase) {
                applicable_tax_id = parseInt(idPurchaseProductMap[productId].inter_state_tax_id);
            }
        } else {
            applicable_tax_id = purchaseDataTable.cell(rowIdx, 3).nodes().to$().find('select').val();
        }
        if (!updateFields && purchaseDataTable.cell(rowIdx, 4).nodes().to$().find('input').val())
            purchase_price = purchaseDataTable.cell(rowIdx, 4).nodes().to$().find('input').val();
        else
            purchase_price = idPurchaseProductMap[productId].purchase_price;

        quantity = purchaseDataTable.cell(rowIdx, 5).nodes().to$().find('input').val();
        discount = purchaseDataTable.cell(rowIdx, 6).nodes().to$().find('input').val();
    }

    //PRODUCT
    var productSelectList = document.createElement("select");
    productSelectList.setAttribute("onchange", "updatePurchaseProducts(this, true);calculatePurchaseNumbers();");
    productSelectList.classList.add("form-control");
    productSelectList.classList.add("dtSelect2");
    //Create and append the options
    for (var i = 0; i < purchaseProductList.length; i++) {
        var option = document.createElement("option");
        option.value = purchaseProductList[i].product_id;
        option.text = purchaseProductList[i].product_name;
        productSelectList.appendChild(option);

        if (productId == purchaseProductList[i].product_id) {
            option.selected = true;
            option.setAttribute("selected", "selected");
        }
    }
    var productStr = productSelectList.outerHTML;

    //TAXES
    var taxSelectList = document.createElement("select");
    taxSelectList.setAttribute("onchange", "calculatePurchaseNumbers();");
    taxSelectList.setAttribute("name", "purchase_entries[" + (rowIdx + 1) + "][" + productId + "][1]");
    taxSelectList.classList.add("form-control");
    taxSelectList.classList.add("dtSelect");
    //var taxes = [0, 5, 12, 18, 28];
    //Create and append the options
    for (var i = 0; i < taxes.length; i++) {
        var option = document.createElement("option");
        option.value = taxes[i].tax_id;
        option.setAttribute("tax_value", taxes[i].tax_percent);
        option.text = taxes[i].tax_name;
        taxSelectList.appendChild(option);

        if (applicable_tax_id == taxes[i].tax_id) {
            option.selected = true;
            option.setAttribute("selected", "selected");
        }
    }
    var taxStr = taxSelectList.outerHTML;

    var data = [];
    data[0] = rowIdx + 1;
    data[1] = productStr + '<input name = "purchase_entries[' + data[0] + '][' + productId + '][0]" value = "' + idPurchaseProductMap[productId].product_name + '" style = "display:none" ></input>';
    data[2] = '<a href = "master#!/product/hsn/' + idPurchaseProductMap[productId].hsn_code + '">' + idPurchaseProductMap[productId].hsn_code
            + '</a><input name = "purchase_entries[' + data[0] + '][' + productId + '][2]" value = "' + idPurchaseProductMap[productId].hsn_code + '" style = "display:none" ></input>';
    data[3] = taxStr;
    data[4] = '<input name = "purchase_entries[' + data[0] + '][' + productId + '][3]" class = "form-control" type = "number" step = 0.01 value = "' + purchase_price + '" onchange = "calculatePurchaseNumbers()"></input>';
    data[5] = '<input name = "purchase_entries[' + data[0] + '][' + productId + '][4]" id = "quantity" class = "form-control" type = "number" step = 0.01 min = 0 value = "' + quantity + '" onchange = "calculatePurchaseNumbers()"></input>'
            + '<input name = "purchase_entries[' + data[0] + '][' + productId + '][5]" value = "' + idPurchaseProductMap[productId].product_uqc_id + '" style = "display:none" ></input>'
            + '<input name = "purchase_entries[' + data[0] + '][' + productId + '][6]" value = "' + idPurchaseProductMap[productId].uqc_text + '" style = "display:none" ></input>';
    data[6] = '<input name = "purchase_entries[' + data[0] + '][' + productId + '][7]" readonly min = 0 max = 100 id = "discount" class = "form-control" type = "number" step = 0.01 value = "' + discount + '" onchange = "calculatePurchaseNumbers()"></input>';
    data[7] = 0;
    if (rowIdx == 0)
        data[8] = '';
    else
        data[8] = '<a href="javascript: void(0)" onclick = "deletePurchaseRow(this);refreshAndReIndexEntirePurchaseTable();calculatePurchaseNumbers();"><i class="glyphicon glyphicon-trash"></i></a>';

    if (element == null) {
        purchaseDataTable.row.add(data).draw(true);
    } else {
        purchaseDataTable.row(rowIdx).data(data).draw(true);
    }

    //calculatePurchaseNumbers();
    initializePurchaseDataTableSelect2();
}

function updateTaxesInPurchaseTable() {
    var purchase_linked_company_gst_supply_state_id = document.getElementById('purchase_linked_company_gst_supply_state_id');
    var purchase_linked_company_billing_state_id = document.getElementById('purchase_linked_company_billing_state_id');
    var selectedStateId = (purchase_linked_company_gst_supply_state_id.options[purchase_linked_company_gst_supply_state_id.selectedIndex]).value;

    var billingStateId = (purchase_linked_company_billing_state_id.options[purchase_linked_company_billing_state_id.selectedIndex]).value;
    if (selectedStateId > 0 && billingStateId > 0) {
        //interStateTaxIsApplicableForPurchase = !(selectedStateId == P_OC_GST_STATE_ID);
        interStateTaxIsApplicableForPurchase = !(selectedStateId == billingStateId);
    } else {
        interStateTaxIsApplicableForPurchase = false;
    }

    var tax_type = document.getElementById('tax_type');
    var selectedOption = tax_type.options[tax_type.selectedIndex];
    if (selectedOption.value == 'inclusive') {
        taxIsExclusiveForPurchase = false;
    } else {
        taxIsExclusiveForPurchase = true;
    }
}

function refreshAndReIndexEntirePurchaseTable(updateFields) {
    purchaseDataTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var element = purchaseDataTable.cell(rowIdx, 1).nodes().to$().find('select')[0];
        if (element == null) {
            alert("ERROR");
            return;
        }
        updatePurchaseProducts(element, updateFields);
    });
    
    purchaseChargesDataTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var element = purchaseChargesDataTable.cell(rowIdx, 0).nodes().to$().find('select')[0];
        if (element == null) {
            alert("ERROR");
            return;
        }
        updatePurchaseCharges(element);
    });
}

function calculatePurchaseNumbers() {
    if (taxIsExclusiveForPurchase) {
        $('#subTotalLabel').text("Sub Total ( Exclusive Of Tax )");
    } else {
        $('#subTotalLabel').text("Sub Total ( Inclusive Of Tax )");
    }
    var totalAmount = parseFloat(0);
    var totalTaxAmount = parseFloat(0);
    var postTotalTaxAmount = parseFloat(0);
    var taxAmountMap = {};
    purchaseDataTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var data = this.data();

        var tax_select = purchaseDataTable.cell(rowIdx, 3).nodes().to$().find('select');	// GET ATTRIBUTE AND NOT VALUE
        var tax = $('option:selected', tax_select).attr('tax_value');
        var cost = purchaseDataTable.cell(rowIdx, 4).nodes().to$().find('input').val();
        var quantity = purchaseDataTable.cell(rowIdx, 5).nodes().to$().find('input').val();
        var discount = purchaseDataTable.cell(rowIdx, 6).nodes().to$().find('input').val();

        var amount = ((cost * quantity) * (1 - (discount / 100))).toFixed(2);
        totalAmount = parseFloat(totalAmount) + parseFloat(amount);

        var taxAmount = parseFloat(0);
        if (taxIsExclusiveForPurchase) {
            taxAmount = amount * (tax / 100);
        } else {
            //(tax * amount) / ( 100 + tax )
            taxAmount = (parseFloat(tax) * parseFloat(amount)) / (100 + parseFloat(tax))
        }

        if (taxAmountMap[tax] == null) {
            taxAmountMap[tax] = taxAmount;
        } else {
            taxAmountMap[tax] = taxAmountMap[tax] + taxAmount;
        }

        totalTaxAmount = (parseFloat(totalTaxAmount) + parseFloat(taxAmount)).toFixed(2);
        purchaseDataTable.cell(rowIdx, 7).data('₹ ' + amount);
    });
    
    purchaseChargesDataTable.rows().every(function (rowIdx, tableLoop, rowLoop) {
        var data = this.data();
        //var charge_id = chargesDataTable.cell(rowIdx, 0).nodes().to$().find('select')[0].selectedOptions[0].value;
        var taxableAmount = purchaseChargesDataTable.cell(rowIdx, 1).nodes().to$().find('input').val();
        var tax = purchaseChargesDataTable.cell(rowIdx, 2).nodes().to$().find('select')[0].selectedOptions[0].attributes['tax_value'].value;
        var taxAmountOnCharge = parseFloat(0);
        var totalChargeAmount = parseFloat(0);
        if (taxIsExclusiveForPurchase) {
            taxAmountOnCharge = taxableAmount * (tax / 100);
            totalChargeAmount = (parseFloat(taxableAmount) + parseFloat(taxAmountOnCharge)).toFixed(2);
        } else {
            //(tax * amount) / ( 100 + tax )
            taxAmountOnCharge = (parseFloat(tax) * parseFloat(taxableAmount)) / (100 + parseFloat(tax));
            totalChargeAmount = taxableAmount;
        } 

        if (taxAmountMap[tax] == null) {
            taxAmountMap[tax] = taxAmountOnCharge;
        } else {
            taxAmountMap[tax] = taxAmountMap[tax] + taxAmountOnCharge;
        }
 
        totalTaxAmount = parseFloat(totalTaxAmount) + parseFloat(taxAmountOnCharge);
        totalAmount = parseFloat(totalAmount) + parseFloat(taxableAmount);
        purchaseChargesDataTable.cell(rowIdx, 3).data('<span style = "white-space: nowrap;">₹ ' + totalChargeAmount + '</span>');
    });

    document.getElementById('totalPurchaseSubTotalAmount').innerHTML = '₹ ' + parseFloat(totalAmount).toFixed(2);

    var taxStr = '';

    for (var taxPercent in taxAmountMap) {
        //if(taxAmountMap[taxPercent]!=0){
        if (interStateTaxIsApplicableForPurchase) {

            taxStr += '<div class="form-group col-lg-12">' +
                    '<label class="control-label col-lg-6">IGST@' + taxPercent + '%</label>' +
                    '<div class="col-lg-6">' +
                    '<h6 class = "no-margin text-bold">₹ ' + taxAmountMap[taxPercent].toFixed(2) + '<h6>' +
                    '</div>' +
                    '</div>';

        } else {
            taxStr += '<div class="form-group col-lg-12">' +
                    '<label class="control-label col-lg-6">SGST@' + (taxPercent / 2) + '%</label>' +
                    '<div class="col-lg-6">' +
                    '<h6 class = "no-margin text-bold">₹ ' + (taxAmountMap[taxPercent] / 2).toFixed(2) + '<h6>' +
                    '</div>' +
                    '</div>' +
                    '<div class="form-group col-lg-12">' +
                    '<label class="control-label col-lg-6">CGST@' + (taxPercent / 2) + '%</label>' +
                    '<div class="col-lg-6">' +
                    '<h6 class = "no-margin text-bold">₹ ' + (taxAmountMap[taxPercent] / 2).toFixed(2) + '<h6>' +
                    '</div>' +
                    '</div>';
            ;

        }
        //}
    }

    document.getElementById('taxDiv').innerHTML = taxStr;

    var transportationCharges = 0;
    if (document.getElementById('transportationCharges').value != "") {
        transportationCharges = document.getElementById('transportationCharges').value;
    }
    var adjustments = 0;
    if (document.getElementById('adjustments').value != "") {
        adjustments = document.getElementById('adjustments').value;
    }

    if (taxIsExclusiveForPurchase) {
        postTotalTaxAmount = parseFloat(totalTaxAmount) + parseFloat(totalAmount);
    } else {
        postTotalTaxAmount = totalAmount;
    }

    var purchaseTotal = parseFloat(postTotalTaxAmount) + parseFloat(transportationCharges) + parseFloat(adjustments);

    document.getElementById('purchaseTotal').innerHTML = '₹ ' + parseFloat(purchaseTotal).toFixed(2);
    document.getElementById('purchaseAmount').value = parseFloat(purchaseTotal).toFixed(2);

    if (purchaseTotal <= 0) {
        if (document.getElementById('buttonSaveDraftPurchase') != null)
            document.getElementById('buttonSaveDraftPurchase').disabled = true;
        document.getElementById('buttonSaveConfirmPurchase').disabled = true;
    } else {
        if (document.getElementById('buttonSaveDraftPurchase') != null)
            document.getElementById('buttonSaveDraftPurchase').disabled = false;
        document.getElementById('buttonSaveConfirmPurchase').disabled = false;
    }
}