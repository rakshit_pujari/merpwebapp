
var recipeDataTable;
var recipeProductList = [];
var idRecipeProductMap = {};

function initialize_recipe_form() {

    var recipeDate = new Date();
    var recipeDateString = $('#recipe_date_picker').val();
    if (recipeDateString != null && recipeDateString != "") {
        try {
            recipeDate = $.datepicker.parseDate("yy-mm-dd", recipeDateString);
        } catch (Err) {

        }
    }
    
    var recipeRevisionDate = new Date();
    var recipeRevisionDateString = $('#recipe_revision_date').val();
    if (recipeRevisionDateString != null && recipeRevisionDateString != "") {
        try {
            recipeRevisionDate = $.datepicker.parseDate("yy-mm-dd", recipeRevisionDateString);
            $('#recipe_revision_date').datepicker({
                dateFormat: 'yy-mm-dd'
            }).datepicker("setDate", recipeRevisionDate);
        } catch (Err) {

        }
    } else {
        $('#recipe_revision_date').datepicker({
            dateFormat: 'yy-mm-dd'
        });
    }
    
    $('#recipe_date_picker').datepicker({
            dateFormat: 'yy-mm-dd'
    }).datepicker("setDate", recipeDate);

    initializeFormValidator();
    initializeRecipeDataTable();
    initializeRecipeSelect2();
    loadRecipeProductsList();
}

function initializeRecipeSelect2() {
    // Select with search
    $('.select2').select2();
    $('.select').select2({//keep this below $('.select2').select2();
        minimumResultsForSearch: Infinity
    });
}

function initializeRecipeDataTableSelect2() {
    $('.dtSelect2').select2();
    $('.dtSelect').select2({
        minimumResultsForSearch: Infinity
    });
}


function initializeRecipeDataTable() {
    recipeDataTable = $('.recipeDatatable').DataTable({
        autoWidth: true,
        columnDefs: [
            {
                className: "row text-center",
                targets: [3]
            }
        ],
        order: [[0, "asc"]],
        searching: false,
        paging: false,
        bInfo: false,
        bSort: false
    });
}


function deleteRecipeRow(element) {
    recipeDataTable.row($(element).parents('tr'))
            .remove()
            .draw();
    renumberRecipeTable();
}

function renumberRecipeTable() {
    recipeDataTable.column(0).nodes().each(function (cell, i) {
        cell.innerHTML = i + 1;
    });
}

function loadRecipeProductsList() {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            var productsArray = JSON.parse(request.responseText).products;
            recipeProductList = [];
            for (var i = 0; i < productsArray.length; i++) {
                var product = productsArray[i];
                recipeProductList.push(product);
                idRecipeProductMap[product.product_id] = product;
            }

            if (!recipeDataTable.data().count())
                updateRecipeProducts();//add 1 row by default if empty
            else 
                initializeRecipeDataTableSelect2();
        }
    };
    request.open("GET", "/index.php/web/product/json/purchase", true);
    request.send();
}


/*
 0 - Product ID
 1 - rate
 2 - quantity
 3- UQC ID
 4 - UQC text
 */
function updateRecipeProducts(element, updateFields) {

    var productsListSize = 0;
    recipeDataTable.column(0).nodes().each(function (cell, i) {
        productsListSize++;
    });

    var rowIdx = productsListSize;
    var productId = recipeProductList[0].product_id;
    var quantity = 0;
    if (element != null) {
        rowIdx = $(element).parents('tr')[0].rowIndex - 1;
        productId = element.selectedOptions[0].value;
        quantity = recipeDataTable.cell(rowIdx, 2).nodes().to$().find('input').val();
    }
    
    //PRODUCT
    var productSelectList = document.createElement("select");
    productSelectList.setAttribute("onchange", "updateRecipeProducts(this, true);");
    productSelectList.classList.add("form-control");
    productSelectList.classList.add("dtSelect2");
    
    var uqc_text = '';
    //Create and append the options
    for (var i = 0; i < recipeProductList.length; i++) {
        var option = document.createElement("option");
        option.value = recipeProductList[i].product_id;
        option.text = recipeProductList[i].product_name;
        productSelectList.appendChild(option);

        if (productId == recipeProductList[i].product_id) {
            option.selected = true;
            option.setAttribute("selected", "selected");
            uqc_text = recipeProductList[i].uqc_text;
        }
    }
    
    var productStr = productSelectList.outerHTML;

    var data = [];
    data[0] = rowIdx + 1;
    data[1] = productStr;
    data[2] = '<input name = "recipe_entries[' + data[0] + '][' + productId + ']" class = "form-control" type = "number" min = 0 step = 0.01 value = ' + quantity + '></input>';
    data[3] = uqc_text;
    if (rowIdx == 0)
        data[4] = '';
    else
        data[4] = '<a href="javascript: void(0)" onclick = "deleteRecipeRow(this);refreshAndReIndexEntireRecipeTable();"><i class="glyphicon glyphicon-trash"></i></a>';

    if (element == null) {
        recipeDataTable.row.add(data).draw(true);
    } else {
        recipeDataTable.row(rowIdx).data(data).draw(true);
    }
    initializeRecipeDataTableSelect2();
}
