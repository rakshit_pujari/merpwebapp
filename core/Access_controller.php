<?php

class Access_controller extends Auth_controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url_helper');
        $this->load->model('employee_model');
        $this->load->model('owner_company_model');
        
        $this->session->set_userdata('access_controller', $this);
    }

    /**
     * Check if employee is granted access
     * @param type $class
     * @param type $action
     * @return boolean
     */
    public function is_access_granted($class, $action){
        $employee_rights_list = $this->employee_model->get_employee_rights_list();
        $module = $this->employee_model->get_class_module($class, $action);
        $employee_assigned_rights = $this->employee_model->get_rights_assigned_to_employee($this->session->userdata('employee')['employee_id']);
        
        $owner_company = $this->owner_company_model->get_owner_company();
        
        /**
         * Check if module has been subscribed
         */
        if (!in_array($module, explode(",", $owner_company['oc_subscribed_modules'])) 
                && $module != 'master'){    //master is subscribed by default for all
            throw new Exception($module. ' for '.$class.' '.$action.' not found in '.$owner_company['oc_subscribed_modules']);
            return false;
        }
                
        if(isset($employee_rights_list[$class][$action]) 
                && isset($employee_assigned_rights)){
            $required_right = $employee_rights_list[$class][$action];
            return(in_array($required_right, $employee_assigned_rights));
        } else {
            return false;
        }
    }
}
?>

