<?php

class Auth_controller extends Parent_controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url_helper');
        $this->load->model('auth_model');
        
        if (!$this->auth_model->is_session_valid()) {
            $this->session->set_flashdata('error_notification', 'Your Session Has Expired.');
            redirect('web/login/', 'refresh');
        } 
    }

}
?>

